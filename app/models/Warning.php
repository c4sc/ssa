<?php

class Warning extends Eloquent {

	protected $fillable = [];
    public $timestamps = false;
    const ERROR = 'ER';
    const WARNING = 'WN';

    public function schools()
    {
        return $this->belongsToMany('School', 'school_warnings', 'warning_id', 'schcd');
    }

    public function scopeErrros()
    {
        return $query->where('warnings.type', '=', Warning::ERROR);
    }

}
