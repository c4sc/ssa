<?php

class Student extends Elegant
{
    protected $fillable = [];
    protected $table = 'students';
    public $timestamps = true;
    public $incrementing = true;


    public static $GENDER = [
        '' => 'Select Gender',
        1 => '1 - Male',
        2 => '2 - Female',
        ];

    public static $SOCIAL_CATEGORY = [
        '' => '....Select....',
        1 => '1 - General',
        2 => '2 - SC',
        3 => '3 - ST',
        4 => '4 - OBC',
        5 => '5 - ORC',
        6 => '6 - Others',
        ];

    public static $RELIGION = array (
        '' => '....Select....',
        1 => '1 - Hindu',
        2 => '2 - Muslim',
        3 => '3 - Christian',
        4 => '4 - Sikh',
        5 => '5 - Buddhist',
        6 => '6 - Jain',
        7 => '7 - Others',
    );

    public static function findRecord($studentId, $year = AcademicYear::CURRENT_YEAR, $columns = array('*'))
    {
        return self::where('id', '=', $studentId)
            ->first($columns);
    }


    public static function updateRecord($data, $studentId, $year=AcademicYear::CURRENT_YEAR)
    {
        return self::where('id', '=', $studentId)
            ->update($data);
    }

}
