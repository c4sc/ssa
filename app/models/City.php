<?php

class City extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_CITY';
    public $primaryKey = 'CITYCD';
    public $timestamps = false;
    public $incrementing = false;

    public static function getList($block)
    {
        $cities = self::where('block_cities.BLKCD', '=', $block)
            ->join('block_cities', 'STEPS_CITY.CITYCD', '=', 'block_cities.CITYCD')
            ->select(DB::raw('CONCAT(STEPS_CITY.CITYNAME, " - ", STEPS_CITY.CITYCD) as name'), DB::raw('STEPS_CITY.CITYCD as id'))
            ->lists('name', 'id');

        if(count($cities)) {
            return $cities;
        }
        $district = substr($block, 0, 4);
        return self::where('CITYCD', 'LIKE', "{$district}%")->orderBy('CITYNAME')
            ->select(DB::raw('CONCAT(CITYNAME, " - ", CITYCD) as name'), 'CITYCD')
            ->lists('name', 'CITYCD');
    }
}
