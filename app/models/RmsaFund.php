<?php

class RmsaFund extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_FUNDS';
    public $incrementing = false;
    public $timestamps = false;

    public static $HEADS = array(
        1 => 'Civil works',
        2 => 'Annual School Grants',
        3 => 'Others',
    );

    public static $SCHOOL_GRANT_HEADS = array(
        2 => 'Minor repair/maintenance',
        3 => 'Repair and replacement of laboratory equipments, purchase of laboratory consumables and articles, etc.',
        4 => 'Purchase of books, periodicals, newspapers, etc.',
        5 => 'Grant for meeting water, telephone and electricity charges',
    );

    public static function updateFund($data, $itemId,  $schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', $itemId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            1  => array(1),
            2  => range(2, 5),
            3  => array(6),
        );

        // insert null/default values
        foreach($groups as $groupId => $groupItems) {

            foreach($groupItems as $item) {

                $newItem = new self();
                $newItem->ITEMID = $item;
                $newItem->ITEMIDGROUP = $groupId;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }
    }

}
