<?php

class Cwsn extends AcademicYear
{

	protected $fillable = [];
    public $table = 'STEPS_DISABILITY09';
    public $incrementing = false;
    public $timestamps = false;

    public static $DISABILITIES = [
        1 => '1 - Visual Impairment (Blindness)',
        2 => '2 - Visual Impairment (Low vision)',
        3 => '3 - Hearing impairment',
        4 => '4 - speech impairment',
        5 => '5 - Loco motor impairment',
        6 => '6 - Mental Retardation',
        7 => '7 - Learning disability',
        8 => '8 - Cerebral Palsy',
        9 => '9 - Autism',
        10 => '10 - Multiple disability',
    ];

    public static function updateCwsn($data, $disabilityId,  $schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('DISABILITY_TYPE', '=', $disabilityId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        // insert null/default values
        foreach(range(1, 10) as $itemId) {

            $newItem = new self();
            $newItem->DISABILITY_TYPE = $itemId;
            $newItem->SCHCD = $schoolCode;
            $newItem->AC_YEAR = self::CURRENT_YEAR;
            $newItem->save();
        }
    }

}
