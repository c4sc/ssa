<?php


class School extends Elegant
{

    /*
    protected $fillable = ['SCHCD' ,'SCHNAME' ,'SCHMGT' ,'SCHCAT' ,'SCHTYPE' ,'RURURB' ,'DISTCD' ,
        'BLKCD' ,'VILCD' ,'CLUCD' ,'PANCD' ,'SCHTYPES' ,'SCHTYPEHS' ,'SCHMGTS' ,'SCHMGTHS'];
     */
    protected $fillable = [];
    protected $table = 'STEPS_SCHOOL';
    protected $primaryKey = 'SCHCD';
    public $timestamps = false;
    public $incrementing = false;

    public static $CHOICE = [
        '' => '.... Select ....',
        2 => '2 - No',
        1 => '1 - Yes',
    ];
    public static $CHOICE_NA = [
        2 => '2 - No',
        1 => '1 - Yes',
    ];
    public static $CHOICE_NF = [
        '' => '.... Select ....',
        2 => '2 - No',
        1 => '1 - Yes',
        3 => '3 - Yes, but not functional',
    ];

    protected $rules = array(
        'schoolCode'   => 'integer|digits:11|unique:STEPS_SCHOOL,SCHCD',
        'name'   => 'required|min:3|max:128',
        'rural_or_urban'   => 'required',
        'district'   => 'required',
        'block'   => 'required',
        'village'   => 'required',
        'cluster'   => 'required',
        );

    public static $TYPES = array(
        '' => '.... Select ....',
        1 => '1 - Boys',
        2 => '2 - Girls',
        3 => '3 - Co-educational',
    );

    public static $AREA = array(
        '' => '.... Select ....',
        1 => '1 - Rural',
        2 => '2 - Urban',
    );

    public static $CATEGORIES = array(
        '' => '.... Select ....',
        1 => '1 - Primary',
        2 => '2 - Primary with Upper Primary',
        3 => '3 - Primary, Uppper Primary, Secondary and HigherSecondary.',
        4 => '4 - Upper Primary only',
        5 => '5 - Upper Primary, Secondary and Higher Secondary',
        6 => '6 - Primary, Uppper Primary, and Secondary Only',
        7 => '7 - Upper Primary and Secondary',
        8 => '8 - Secondary Only',
        10 => '10 - Secondary with Higher Secondary',
        11 => '11 - Higher Secondary only/Jr. College',
    );

    public static $MANAGEMENTS = array(
        '' => '.... Select ....',
        1 => '1 - Department of Education',
        2 => '2 - Tribal/Social Welfare Dept.',
        3 => '3 - Local body',
        4 => '4 - Pvt. Aided',
        5 => '5 - Pvt. Unaided',
        6 => '6 - Others',
        7 => '7 - Central Govt.',
        8 => '8 - Un-Recognised',
        10 => '10 - CBSE',
        11 => '11 - MGLC',
        12 => '12 - Technical School',
        97 => '97 - Madarsa recognized (by Wakf board/Madarsa Board)',
        98 => '98 - Madarsa unrecognized',
    );

    public static $UDISE_ROUTE_GROUPS = array(
        'particulars' => 'Particulars',
        'enrolment' => 'Enrolment',
        'enrolmentAge' => 'Enrolment by Age',
        'repeaters' => 'Repeaters',
        'cwsn' => 'Children with Special Needs',
        'facility' => 'Facilities Provided &amp; Results',
        'secondary' => 'Secondary &amp; Higher Secondary',
        'fund' => 'Fund and Staff',
    );

    // order of the defenition matters. I am trying to use this to generate menu in layouts
    // key should be the route name.
    public static $UDISE_ROUTES = array(
        'particulars-one' => array('group'=>'particulars', 'title'=>'School Particulars', 'required'=>true),
        'particulars-two' => array('group'=>'particulars', 'title'=>'Particulars - Schools with Pre Primary', 'required'=>true),
        'particulars-three' => array('group'=>'particulars', 'title'=>'Particulars - Elementary', 'required'=>true),
        'particulars-four' => array('group'=>'particulars', 'title'=>'SMDC Details', 'required'=>true),
        'facilities-two' => array('group'=>'particulars', 'title'=>'Facilities - Secondary & Higher Secondary', 'required'=>true),
        'facilities-one' => array('group'=>'particulars', 'title'=>'Physical facilities and Equipment', 'required'=>true),
        'mdm' => array('group'=>'particulars', 'title'=>'Mid day meal', 'required'=>true),
        'teachers' => array('group'=>'particulars', 'title'=>'Teachers', 'required'=>false),

        'first-grade-admission' => array('group'=>'enrolment', 'title'=>'First Grade Admission', 'required'=>true),
        'enrolment-by-category' => array('group'=>'enrolment', 'title'=>'Enrolment by Social Category', 'required'=>true),
        'enrolment-minority-category' => array('group'=>'enrolment', 'title'=>'Enrolment by Social Category - Minority', 'required'=>true),
        'enrolment-by-medium' => array('group'=>'enrolment', 'title'=>'Enrolment by Medium of Instruction', 'required'=>true),

        'enrolment-by-age' => array('group'=>'enrolmentAge', 'title'=>'LP Section', 'required'=>true),
        'enrolment-by-age-up' => array('group'=>'enrolmentAge', 'title'=>'UP Section', 'required'=>true),
        'enrolment-by-age-hs' => array('group'=>'enrolmentAge', 'title'=>'Secondary', 'required'=>true),
        'enrolment-by-age-ss' => array('group'=>'enrolmentAge', 'title'=>'Higher Secondary', 'required'=>true),

        'repeaters-by-category' => array('group'=>'repeaters', 'title'=>'By Social Category', 'required'=>true),
        'repeaters-minority-category' => array('group'=>'repeaters', 'title'=>'Minority Category', 'required'=>true),

        'children-with-special-needs' => array('group'=>'cwsn', 'title'=>'LP Section', 'required'=>true),
        'children-with-special-needs-up' => array('group'=>'cwsn', 'title'=>'UP Section', 'required'=>true),
        'children-with-special-needs-hs' => array('group'=>'cwsn', 'title'=>'Secondary', 'required'=>true),
        'children-with-special-needs-hss' => array('group'=>'cwsn', 'title'=>'Higher Secondary', 'required'=>true),

        'facilities-cwsn' => array('group'=>'facility', 'title'=>'To (CWSN)-Children with Special Needs', 'required'=>true),
        'facilities-to-children-primary' => array('group'=>'facility', 'title'=>'To Primary Classes', 'required'=>true),
        'facilities-to-children-up' => array('group'=>'facility', 'title'=>'To Upper Primary Classes', 'required'=>true),
        'exam-results' => array('group'=>'facility', 'title'=>'Examination Results', 'required'=>true),

        'x-result-by-category' => array('group'=>'secondary', 'title'=>'X Results by category', 'required'=>true),
        'result-by-score-x' => array('group'=>'secondary', 'title'=>'X Results by score', 'required'=>true),
        'streams-available' => array('group'=>'secondary', 'title'=>'Streams available', 'required'=>true),
        'enrolment-by-stream' => array('group'=>'secondary', 'title'=>'Enrolment by Stream', 'required'=>true),
        'repeaters-by-stream' => array('group'=>'secondary', 'title'=>'Repeaters by Stream', 'required'=>true),
        'result-by-score-xii' => array('group'=>'secondary', 'title'=>'XII Results by score', 'required'=>true),
        'result-by-stream' => array('group'=>'secondary', 'title'=>'XII Results by stream', 'required'=>true),

        'rmsa-fund' => array('group'=>'fund', 'title'=>'RMSA Fund', 'required'=>true),
        'staff' => array('group'=>'fund', 'title'=>'Staff Details', 'required'=>true),
    );

    /**
     * Generate school code for new schools
     *
     * @param int villageCode
     * @return int schoolCode
     */
    public static function generateSchoolCode($villageCode)
    {
        $schoolCode = self::where('SCHCD', 'LIKE', "$villageCode%")->max('SCHCD');
        if($schoolCode == 0) {
            return $villageCode .= '01';
        }
        return ++$schoolCode;
    }

    public function warnings()
    {
        return $this->belongsToMany('Warning', 'school_warnings', 'schcd', 'warning_id');
    }

    public static function updateFromDetails($schoolCode)
    {
        $details = SchoolDetails::findRecord($schoolCode);

        $current = self::find($schoolCode);
        if ( ! isset($details->SCHCD)) {
            return;
        }
        if ( ! isset($current->SCHCD)) {
            $current = new self();
        }

        // save
        $current->SCHNAME = $details->SCHNAME;
        $current->BLKCD = $details->BLKCD;
        $current->VILCD = $details->VILCD;
        $current->DISTCD = $details->DISTCD;
        $current->SCHCD = $schoolCode;
        $current->SCHVAR2 = null;
        $current->save();
    }

    /**
     * set confirmation flag
     * @param $status
     * @return void
     */
    public function confirmation($status = null)
    {
        if($status !== null) {
            $this->SCHVAR2 = $status;
            return $this->save();
        }
        return $this->SCHVAR2 == 1;
    }

}
