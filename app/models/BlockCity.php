<?php

class BlockCity extends Eloquent {

	protected $fillable = ['CITYCD', 'BLKCD'];
    public $timestamps = false;

}
