<?php

class Habitation extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_HABITATION';
    public $primaryKey = 'HABCD';
    protected $timestamp = false;

    public static function getList($block)
    {
        $habitations = self::where('block_habitations.BLKCD', '=', $block)
            ->join('block_habitations', 'STEPS_HABITATION.HABCD', '=', 'block_habitations.HABCD')
            ->select(DB::raw('CONCAT(STEPS_HABITATION.HABNAME, " - ", STEPS_HABITATION.HABCD) as name'),
            DB::raw('STEPS_HABITATION.HABCD as id'))
            ->lists('name', 'id');

        if(count($habitations)) {
            return $habitations;
        }
        return self::where('HABCD', 'LIKE', "{$block}%")->orderBy('HABNAME')
            ->select(DB::raw('CONCAT(STEPS_HABITATION.HABNAME, " - ", STEPS_HABITATION.HABCD) as name'), 'HABCD')
            ->lists('name', 'HABCD');
    }
}
