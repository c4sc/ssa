<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryGroupModel;

class Group extends SentryGroupModel {

	protected $fillable = [];
	protected $table = 'groups';

}
