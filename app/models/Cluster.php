<?php

class Cluster extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_CLUSTER';
    protected $primaray_key = 'CLUCD';

    public static function getList($block)
    {
        $clusters = self::where('block_clusters.BLKCD', '=', $block)
            ->join('block_clusters', 'STEPS_CLUSTER.CLUCD', '=', 'block_clusters.CLUCD')
            ->select(DB::raw('CONCAT(STEPS_CLUSTER.CLUNAME, " - ", STEPS_CLUSTER.CLUCD) as name'),
            DB::raw('STEPS_CLUSTER.CLUCD as id'))
            ->lists('name', 'id');

        if(count($clusters)) {
            return $clusters;
        }
        return self::where('CLUCD', 'LIKE', "{$block}%")->orderBy('CLUNAME')
            ->select(DB::raw('CONCAT(CLUNAME, " - ", CLUCD) as name'), 'CLUCD')
            ->lists('name', 'CLUCD');
    }
}
