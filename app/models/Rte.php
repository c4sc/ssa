<?php

class Rte extends AcademicYear
{

	protected $fillable = [];
    public $table = 'STEPS_RTEINFO';
    public $timestamps = false;

    public static $SPLTRG_BY = array(
        1 => '1 - School teachers',
        2 => '2 - specially engaged teachers',
        3 => '3 - both 1 &amp; 2',
        4 => '4 - NGO',
        5 => '5 - Others ',
    );
    public static $SPLTRG_PLACE = array(
        1 => '1 - School premises',
        2 => '2 - Other than school premises',
        3 => '3 - both 1 &amp; 2',
    );
    public static $SPLTRG_TYPE = array(
        1 => '1 - Residential',
        2 => '2 - non-residential',
        3 => '3 - both 1 &amp; 2',
    );
    public static $MONTH = array(
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    );

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previous = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', self::PREVIOUS_YEAR)->first();

        $current = new self();

        if(isset($previous->SCHCD)) {

            $current->WORKDAYS_PR = $previous->WORKDAYS_PR;
            $current->WORKDAYS_UPR = $previous->WORKDAYS_UPR;
            $current->SCHHRSCHILD_PR = $previous->SCHHRSCHILD_PR;
            $current->SCHHRSCHILD_UPR = $previous->SCHHRSCHILD_UPR;
            $current->SCHHRSTCH_PR = $previous->SCHHRSTCH_PR;
            $current->SCHHRSTCH_UPR = $previous->SCHHRSTCH_UPR;
            $current->CCE_YN = $previous->CCE_YN;
            $current->PCR_MAINTAINED = $previous->PCR_MAINTAINED;
            $current->PCR_SHARED = $previous->PCR_SHARED;
            $current->WSEC25P_APPLIED = $previous->WSEC25P_APPLIED;
            $current->WSEC25P_ENROLLED = $previous->WSEC25P_ENROLLED;
            $current->SMC_YN = $previous->SMC_YN;
            $current->SMCMEM_M = $previous->SMCMEM_M;
            $current->SMCMEM_F = $previous->SMCMEM_F;
            $current->SMSPARENTS_M = $previous->SMSPARENTS_M;
            $current->SMSPARENTS_F = $previous->SMSPARENTS_F;
            $current->SMCNOMLOCAL_M = $previous->SMCNOMLOCAL_M;
            $current->SMCNOMLOCAL_F = $previous->SMCNOMLOCAL_F;
            $current->SMCMEETINGS = $previous->SMCMEETINGS;
            $current->SMCSDP_YN = $previous->SMCSDP_YN;
            $current->SMCBANKAC_YN = $previous->SMCBANKAC_YN;
            $current->SMCBANK = $previous->SMCBANK;
            $current->SMCBANKBRANCH = $previous->SMCBANKBRANCH;
            $current->SMCACNO = $previous->SMCACNO;
            $current->SMCACNAME = $previous->SMCACNAME;
            $current->IFSCCODE = $previous->IFSCCODE;
            $current->SPLTRNG_YN = $previous->SPLTRNG_YN;
            $current->SPLTRG_CY_PROVIDED_B = $previous->SPLTRG_CY_PROVIDED_B;
            $current->SPLTRG_CY_PROVIDED_G = $previous->SPLTRG_CY_PROVIDED_G;
            $current->SPLTRG_PY_ENROLLED_B = $previous->SPLTRG_PY_ENROLLED_B;
            $current->SPLTRG_PY_ENROLLED_G = $previous->SPLTRG_PY_ENROLLED_G;
            $current->SPLTRG_PY_PROVIDED_B = $previous->SPLTRG_PY_PROVIDED_B;
            $current->SPLTRG_PY_PROVIDED_G = $previous->SPLTRG_PY_PROVIDED_G;
            $current->SPLTRG_BY = $previous->SPLTRG_BY;
            $current->SPLTRG_PLACE = $previous->SPLTRG_PLACE;
            $current->SPLTRG_TYPE = $previous->SPLTRG_TYPE;
            $current->ACSTARTMNTH = $previous->ACSTARTMNTH;
            $current->TXTBKRECD_YN = $previous->TXTBKRECD_YN;
            $current->TXTBKMNTH = $previous->TXTBKMNTH;
            $current->TXTBKYEAR = $previous->TXTBKYEAR;
        }

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }
}
