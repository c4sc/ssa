<?php

class AcademicYear extends Eloquent {

    public $schoolCode;
    public $year;

    const CURRENT_YEAR = '2014-15';
    const PREVIOUS_YEAR = '2013-14';

    /**
	 * Find model by composite key
	 *
	 * @param  mixed $schoolCode
	 * @param  string $year
	 * @param  array  $columns
	 * @return \Illuminate\Database\Eloquent\Model|static|null
	 */
    public static function findRecord($schoolCode = false, $year = self::CURRENT_YEAR, $columns = array('*'))
    {
        if( ! $schoolCode) {
            $schoolCode = App::make('master')->SCHCD;
        }
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->first($columns);
    }

    /**
	 * Find model by composite key or throw exception
	 *
	 * @param  mixed $schoolCode
	 * @param  string $year
	 * @param  array  $columns
	 * @return \Illuminate\Database\Eloquent\Model|static|null
	 */
    public static function findRecordOrFail($schoolCode = false, $year = self::CURRENT_YEAR, $columns = array('*'))
    {
        if ( ! is_null($model = static::findRecord($schoolCode, $year, $columns))) return $model;
        throw new Illuminate\Database\Eloquent\ModelNotFoundException;
    }


	/**
	 * Find matching by composite key
	 *
	 * @param  mixed $schoolCode
	 * @param  string $year
	 * @return \Illuminate\Database\Eloquent\Model|static|null
	 */
    public static function findRecords($schoolCode = false, $year = self::CURRENT_YEAR)
    {
        if( ! $schoolCode) {
            $schoolCode = App::make('master')->SCHCD;
        }
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year);
    }

    /**
     * scope for handling composite key
     *
     * @param Object Query
     * @param string $year
     * @return Query
     */
    public function scopeYear($query, $schoolCode = false, $year = false)
    {
        if( ! $schoolCode) {
            $schoolCode = $this->SCHCD;
        }
        if( ! $year) {
            $year = $this->AC_YEAR;
        }
        return $query->where('AC_YEAR', '=', $year)->where('SCHCD', '=', $schoolCode);
    }

    public static function updateRecord($data, $schoolCode = false, $year = self::CURRENT_YEAR)
    {
        if( ! $schoolCode) {
            $schoolCode = App::make('master')->SCHCD;
        }
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->update($data);
    }

}
