<?php

class Teacher extends Eloquent {

	protected $fillable = ['*'];
    public $table = 'STEPS_TEACHER';
    public $timestamps = false;
    public $incrementing = false;

    public static $GENDER = [
        1 => '1 - Male',
        2 => '2 - Female',
    ];

    public static $SOCIAL_CATEGORY = [
        1 => '1 - General',
        2 => '2 - SC',
        3 => '3 - ST',
        4 => '4 - OBC',
        5 => '5 - ORC',
        6 => '6 - Others',
    ];

    public static $APPOINTMENT_NATURE = [
        1 => '1 - Regular',
        2 => '2 - Contract',
        3 => '3 - Part-Time',
    ];

    public static $ACADEMIC_QUALIFICATION= [
        1 => '1 - Below Secondary',
        2 => '2 - Secondary',
        3 => '3 - Higher Secondary',
        4 => '4 - Graduate',
        5 => '5 - Post Graduate',
        6 => '6 - M.Phil.',
        7 => '7 - Ph.D.',
        8 => '8 - Post-Doctoral',
    ];

    public static $CLASSES_TAUGHT = [
        1 => '1 - Primary Only',
        2 => '2 - Upper Primary Only',
        3 => '3 - Primary and Upper Primary',
        5 => '5 - Secondary only',
        6 => '6 - Hr. Secondary only',
        7 => '7 - Upper Primary and Secondary',
        8 => '8 - Secondary and Hr. Secondary',
    ];

    public static $DISABILITY = [
        1 => '1 - Not Applicable',
        2 => '2 - Locomotor',
        3 => '3 - Visual',
        4 => '4 - Others',
    ];

    public static $TRAINING_SOURCE = [
        'BRC',
        'CRC',
        'DIET',
        'OTHER'
    ];

    public static $TYPE_CATEGORY = [
        1 => '1 - Head teacher',
        2 => '2 - Acting head teacher',
        3 => '3 - Teacher',
        5 => '5 - Instructor positioned as per RTE',
        6 => '6 - Principal',
        7 => '7 - Vice-Principal',
        8 => '8 - Lecturer',
    ];

    public static $CHOICE = [
        '' => '.... Select ....',
        1 => '1 - Yes',
        2 => '2 - No',
    ];

    public static $PROF_QUALIFICATION = [
        1 => '1 - D.Ed',
        3 => '3 - B.Ed. or equivalent',
        4 => '4 - M. Ed. or equivalent',
        5 => '5 - Others',
        7 => '6 - Diploma/degree in special education',
    ];

    public static function generateSerial($schoolCode) {
        $serial = self::where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->where('SCHCD', '=', $schoolCode)->max('SLNO');
        if( ! $serial) {
            $serial = 0;
        }
        return ++$serial;
    }

    public static function findRecord($teacherId, $year = AcademicYear::CURRENT_YEAR, $columns = array('*'))
    {
        return self::where('TCHCD1', '=', $teacherId)
            ->where('AC_YEAR', '=', $year)
            ->first($columns);
    }


    public static function deleteRecord($teacherId, $year = AcademicYear::CURRENT_YEAR)
    {
        return self::where('TCHCD1', '=', $teacherId)
            ->where('AC_YEAR', '=', $year)
            ->delete();

    }


    public static function updateRecord($data, $teacherId, $year = AcademicYear::CURRENT_YEAR) {

        return self::where('TCHCD1', '=', $teacherId)
            ->where('AC_YEAR', '=', $year)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previousData = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)->get();

        if(count($previousData) == 0) {
            return;
        }

        $teachers = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)
            ->chunk(100, function($teachers) use($schoolCode) {
                foreach($teachers as $previousTeacher) {

                    $teacher = new self();
                    $teacher->SLNO = $previousTeacher->SLNO;
                    $teacher->SCHCD = $previousTeacher->SCHCD;
                    $teacher->TCHCD = $previousTeacher->TCHCD;
                    $teacher->TCHCD1 = $previousTeacher->TCHCD1;
                    $teacher->PENNO = $previousTeacher->PENNO;
                    $teacher->AADHAAR = $previousTeacher->AADHAAR;
                    $teacher->TCHNAME = $previousTeacher->TCHNAME;
                    $teacher->SEX = $previousTeacher->SEX;
                    $teacher->DOB = $previousTeacher->DOB;
                    $teacher->CASTE = $previousTeacher->CASTE;
                    $teacher->CATEGORY = $previousTeacher->CATEGORY;
                    $teacher->POST_STATUS = $previousTeacher->POST_STATUS;
                    $teacher->YOJ = $previousTeacher->YOJ;
                    $teacher->QUAL_ACAD = $previousTeacher->QUAL_ACAD;
                    $teacher->QUAL_PROF = $previousTeacher->QUAL_PROF;
                    $teacher->CLS_TAUGHT = $previousTeacher->CLS_TAUGHT;
                    $teacher->APPOINTSUB = $previousTeacher->APPOINTSUB;
                    $teacher->MAIN_TAUGHT1 = $previousTeacher->MAIN_TAUGHT1;
                    $teacher->MAIN_TAUGHT2 = $previousTeacher->MAIN_TAUGHT2;
                    $teacher->NONTCH_ASS = $previousTeacher->NONTCH_ASS;
                    $teacher->MATH_UPTO = $previousTeacher->MATH_UPTO;
                    $teacher->ENG_UPTO = $previousTeacher->ENG_UPTO;
                    $teacher->SCIENCEUPTO = $previousTeacher->SCIENCEUPTO;
                    $teacher->WORKINGSINCE = $previousTeacher->WORKINGSINCE;
                    $teacher->DISABILITY_TYPE = $previousTeacher->DISABILITY_TYPE;
                    $teacher->CWSNTRAINED_YN = $previousTeacher->CWSNTRAINED_YN;
                    $teacher->MOBILE = $previousTeacher->MOBILE;
                    $teacher->EMAIL = $previousTeacher->EMAIL;

                    foreach(Teacher::$TRAINING_SOURCE as $source) {
                        $teacher->{"TRN_$source"} = $previousTeacher->{"TRN_$source"};
                    }
                    $teacher->SCHCD = $schoolCode;
                    $teacher->AC_YEAR = AcademicYear::CURRENT_YEAR;
                    $teacher->save();
                }
            });
    }

    public static function getAllowedClassesTaught($master)
    {
        $classesTaught = array();
        if ($master->hasPrimary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[1];
        }
        if ($master->hasUpperPrimary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[2];
        }
        if ($master->hasPrimary() && $master->hasUpperPrimary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[3];
        }
        if ($master->hasSecondary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[5];
        }
        if ($master->hasHigherSecondary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[6];
        }
        if ($master->hasUpperPrimary()  && $master->hasSecondary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[7];
        }
        if ($master->hasSecondary()  && $master->hasHigherSecondary()) {
            $classesTaught[] = static::$CLASSES_TAUGHT[8];
        }
        return $classesTaught;
    }

}

