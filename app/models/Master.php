<?php

class Master extends AcademicYear
{

	protected $fillable = [];
    protected $table = 'STEPS_MASTER';
    public $timestamps = false;

    public static $RESIDENTIAL_TYPES = array(
        1 => '1 - Ashram (Govt.)',
        2 => '2 - Non-Ashram (Govt.)',
        3 => '3 - Private',
        4 => '4 - Others',
        6 => '6 - KGBV',
        7 => '7 - Model School',
    );

    public static $INSTRUCTION_MEDIUMS = array(
        8 => '8 - Malayalam',
        19 => '19 - English',
        5 => '5 - Kannada',
        16 => '16 - Tamil',
        98 => '98 - Not-Applicable',
    );

    public static $INSTRUCTION_MEDIUMS_MANDATORY = array(
        8 => '8 - Malayalam',
        19 => '19 - English',
        5 => '5 - Kannada',
        16 => '6 - Tamil',
    );


    public static $BOARDS = array(
        1 => '1 - CBSE',
        2 => '2 - State Board',
        3 => '3 - ICSE',
        4 => '4 - International Board',
        5 => '5 - Others ',
    );

    public static $RELIGIOUS_MINORITIES = array(
        1 => '1 - Muslim',
        2 => '2 - Sikh',
        3 => '3 - Jain',
        4 => '4 - Christain',
        5 => '5 - Parsi',
        6 => '6 - Budhist',
        7 => '7 - Any other',
    );

    public static $GOV_MANAGEMENTS = array(1, 2, 3, 12);

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previous = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)->first();

        $current = new Master();

        if(isset($previous->SCHCD)) {
            // insert values from previous year data
            $current->PINCD = ($previous->PINCD != 0)?$previous->PINCD:null;
            $current->DISTHQ = ($previous->DISTHQ != 0)?$previous->DISTHQ:null;
            $current->DISTCRC = ($previous->DISTCRC != 0)?$previous->DISTCRC:null;
            $current->ESTDYEAR = ($previous->ESTDYEAR != 0)?$previous->ESTDYEAR:null;
            $current->LOWCLASS = ($previous->LOWCLASS != 0)?$previous->LOWCLASS:null;
            $current->HIGHCLASS = ($previous->HIGHCLASS != 0)?$previous->HIGHCLASS:null;
            $current->PPSEC_YN = ($previous->PPSEC_YN != 0)?$previous->PPSEC_YN:null;
            $current->PPSTUDENT = ($previous->PPSTUDENT != 0)?$previous->PPSTUDENT:null;
            $current->PPTEACHER = ($previous->PPTEACHER != 0)?$previous->PPTEACHER:null;
            $current->WORKDAYS = ($previous->WORKDAYS != 0)?$previous->WORKDAYS:null;
            $current->SCHRES_YN = ($previous->SCHRES_YN != 0)?$previous->SCHRES_YN:null;
            $current->RESITYPE = ($previous->RESITYPE != 0)?$previous->RESITYPE:null;
            $current->SCHSHI_YN = ($previous->SCHSHI_YN != 0)?$previous->SCHSHI_YN:null;
            $current->NOINSPECT = ($previous->NOINSPECT != 0)?$previous->NOINSPECT:null;
            $current->VISITSCRC = ($previous->VISITSCRC != 0)?$previous->VISITSCRC:null;
            $current->VISITSBRC = ($previous->VISITSBRC != 0)?$previous->VISITSBRC:null;
            $current->CONTI_R = $previous->CONTI_R;
            $current->CONTI_E = $previous->CONTI_E;
            $current->TLM_R = $previous->TLM_R;
            $current->TLM_E = $previous->TLM_E;
            $current->FUNDS_E = $previous->FUNDS_E;
            $current->FUNDS_R = $previous->FUNDS_R;
            $current->OSRC_E = $previous->OSRC_E;
            $current->OSRC_R = $previous->OSRC_R;
            $current->TCHSAN = $previous->TCHSAN;
            $current->TCHPOS = $previous->TCHPOS;
            $current->PARAPOS = $previous->PARAPOS;
            $current->NTPOS = $previous->NTPOS;
            $current->MEDINSTR1 = $previous->MEDINSTR1;
            $current->MEDINSTR2 = $previous->MEDINSTR2;
            $current->MEDINSTR3 = $previous->MEDINSTR3;
            $current->MEDINSTR4 = $previous->MEDINSTR4;
            $current->EDUBLKCD = $previous->EDUBLKCD;
            $current->HABITCD = $previous->HABITCD;
            $current->ACONSTCD = $previous->ACONSTCD;
            $current->MUNICIPALCD = $previous->MUNICIPALCD;
            $current->CITY = $previous->CITY;
            $current->SCHMNTCGRANT_R = $previous->SCHMNTCGRANT_R;
            $current->SCHMNTCGRANT_E = $previous->SCHMNTCGRANT_E;
            $current->TLE_R = $previous->TLE_R;
            $current->TLE_E = $previous->TLE_E;
            $current->SCHSTATUS = ($previous->SCHSTATUS != 0)?$previous->SCHSTATUS:null;
            $current->SCHMGT = ($previous->SCHMGT != 0)?$previous->SCHMGT:null;
            $current->SCHCAT = ($previous->SCHCAT != 0)?$previous->SCHCAT:null;
            $current->SCHTYPE = ($previous->SCHTYPE != 0)?$previous->SCHTYPE:null;
            $current->RURURB = ($previous->RURURB != 0)?$previous->RURURB:null;
            $current->CLUCD = ($previous->CLUCD != 0)?$previous->CLUCD:null;
            $current->PANCD = ($previous->PANCD != 0)?$previous->PANCD:null;
            $current->SCHTYPES = ($previous->SCHTYPES != 0)?$previous->SCHTYPES:null;
            $current->SCHTYPEHS = ($previous->SCHTYPEHS != 0)?$previous->SCHTYPEHS:null;
            $current->SCHMGTS = ($previous->SCHMGTS != 0)?$previous->SCHMGTS:null;
            $current->SCHMGTHS = ($previous->SCHMGTHS != 0)?$previous->SCHMGTHS:null;
            $current->POSTALADDR = ($previous->POSTALADDR != 0)?$previous->POSTALADDR:null;
            $current->STDCODE1 = ($previous->STDCODE1 != 0)?$previous->STDCODE1:null;
            $current->PHONE1 = ($previous->PHONE1 != 0)?$previous->PHONE1:null;
            $current->MOBILE1 = ($previous->MOBILE1 != 0)?$previous->MOBILE1:null;
            $current->STDCODE2 = ($previous->STDCODE2 != 0)?$previous->STDCODE2:null;
            $current->PHONE2 = ($previous->PHONE2 != 0)?$previous->PHONE2:null;
            $current->MOBILE2 = ($previous->MOBILE2 != 0)?$previous->MOBILE2:null;
            $current->EMAIL = ($previous->EMAIL != 0)?$previous->EMAIL:null;
            $current->WEBSITE = ($previous->WEBSITE != 0)?$previous->WEBSITE:null;
            $current->PARASANCT_ELE = ($previous->PARASANCT_ELE != 0)?$previous->PARASANCT_ELE:null;
            $current->TCHSANCT_SEC = ($previous->TCHSANCT_SEC != 0)?$previous->TCHSANCT_SEC:null;
            $current->TCHPOSN_SEC = ($previous->TCHPOSN_SEC != 0)?$previous->TCHPOSN_SEC:null;
            $current->PARASANCT_SEC = ($previous->PARASANCT_SEC != 0)?$previous->PARASANCT_SEC:null;
            $current->PARAPOSN_SEC = ($previous->PARAPOSN_SEC != 0)?$previous->PARAPOSN_SEC:null;
            $current->TCHSANCT_HSEC = ($previous->TCHSANCT_HSEC != 0)?$previous->TCHSANCT_HSEC:null;
            $current->TCHPOSN_HSEC = ($previous->TCHPOSN_HSEC != 0)?$previous->TCHPOSN_HSEC:null;
            $current->PARASANCT_HSEC = ($previous->PARASANCT_HSEC != 0)?$previous->PARASANCT_HSEC:null;
            $current->PARAPOSN_HSEC = ($previous->PARAPOSN_HSEC != 0)?$previous->PARAPOSN_HSEC:null;
            $current->NTSANCT_SEC = ($previous->NTSANCT_SEC != 0)?$previous->NTSANCT_SEC:null;
            $current->NTPOSN_SEC = ($previous->NTPOSN_SEC != 0)?$previous->NTPOSN_SEC:null;
            $current->NTSANCT_HSEC = ($previous->NTSANCT_HSEC != 0)?$previous->NTSANCT_HSEC:null;
            $current->NTPOSN_HSEC = ($previous->NTPOSN_HSEC != 0)?$previous->NTPOSN_HSEC:null;
            $current->ANGANWADI_YN = ($previous->ANGANWADI_YN != 0)?$previous->ANGANWADI_YN:null;
            $current->ANGANWADI_STU = ($previous->ANGANWADI_STU != 0)?$previous->ANGANWADI_STU:null;
            $current->ANGANWADI_TCH = ($previous->ANGANWADI_TCH != 0)?$previous->ANGANWADI_TCH:null;
            $current->LATDEG = ($previous->LATDEG != 0)?$previous->LATDEG:null;
            $current->LATMIN = ($previous->LATMIN != 0)?$previous->LATMIN:null;
            $current->LATSEC = ($previous->LATSEC != 0)?$previous->LATSEC:null;
            $current->LONDEG = ($previous->LONDEG != 0)?$previous->LONDEG:null;
            $current->LONMIN = ($previous->LONMIN != 0)?$previous->LONMIN:null;
            $current->LONSEC = ($previous->LONSEC != 0)?$previous->LONSEC:null;
            $current->APPROACHBYROAD = ($previous->APPROACHBYROAD != 0)?$previous->APPROACHBYROAD:null;
            $current->YEARRECOG = ($previous->YEARRECOG != 0)?$previous->YEARRECOG:null;
            $current->YEARUPGRD = ($previous->YEARUPGRD != 0)?$previous->YEARUPGRD:null;
            $current->CWSNSCH_YN = ($previous->CWSNSCH_YN != 0)?$previous->CWSNSCH_YN:null;
            $current->VISITSRTCWSN = ($previous->VISITSRTCWSN != 0)?$previous->VISITSRTCWSN:null;
            $current->TCHREGU_UPR = ($previous->TCHREGU_UPR != 0)?$previous->TCHREGU_UPR:null;
            $current->TCHPARA_UPR = ($previous->TCHPARA_UPR != 0)?$previous->TCHPARA_UPR:null;
            $current->TCHPART_UPR = ($previous->TCHPART_UPR != 0)?$previous->TCHPART_UPR:null;
            $current->TCHSANCT_PR = ($previous->TCHSANCT_PR != 0)?$previous->TCHSANCT_PR:null;
            $current->PARASANCT_PR = ($previous->PARASANCT_PR != 0)?$previous->PARASANCT_PR:null;
            $current->DISTP = ($previous->DISTP != 0)?$previous->DISTP:null;
            $current->DISTU = ($previous->DISTU != 0)?$previous->DISTU:null;
            $current->DISTS = ($previous->DISTS != 0)?$previous->DISTS:null;
            $current->DISTH = ($previous->DISTH != 0)?$previous->DISTH:null;
            $current->MEDS1 = ($previous->MEDS1 != 0)?$previous->MEDS1:null;
            $current->MEDS2 = ($previous->MEDS2 != 0)?$previous->MEDS2:null;
            $current->MEDS3 = ($previous->MEDS3 != 0)?$previous->MEDS3:null;
            $current->MEDS4 = ($previous->MEDS4 != 0)?$previous->MEDS4:null;
            $current->MEDH1 = ($previous->MEDH1 != 0)?$previous->MEDH1:null;
            $current->MEDH2 = ($previous->MEDH2 != 0)?$previous->MEDH2:null;
            $current->MEDH3 = ($previous->MEDH3 != 0)?$previous->MEDH3:null;
            $current->MEDH4 = ($previous->MEDH4 != 0)?$previous->MEDH4:null;
            $current->BOARDSEC = ($previous->BOARDSEC != 0)?$previous->BOARDSEC:null;
            $current->BOARDHSEC = ($previous->BOARDHSEC != 0)?$previous->BOARDHSEC:null;
            $current->YEARRECOGS = ($previous->YEARRECOGS != 0)?$previous->YEARRECOGS:null;
            $current->YEARRECOGH = ($previous->YEARRECOGH != 0)?$previous->YEARRECOGH:null;
            $current->YEARUPGRDS = ($previous->YEARUPGRDS != 0)?$previous->YEARUPGRDS:null;
            $current->YEARUPGRDH = ($previous->YEARUPGRDH != 0)?$previous->YEARUPGRDH:null;
            $current->SCHRESUPR_YN = ($previous->SCHRESUPR_YN != 0)?$previous->SCHRESUPR_YN:null;
            $current->SCHRESSEC_YN = ($previous->SCHRESSEC_YN != 0)?$previous->SCHRESSEC_YN:null;
            $current->SCHRESHSEC_YN  = ($previous->SCHRESHSEC_YN != 0)?$previous->SCHRESHSEC_YN:null;
        }

        // save
        $school = SchoolDetails::findRecord($schoolCode);

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->SCHMGT = $school->SCHMGT;
        $current->SCHCAT = $school->SCHCAT;
        $current->SCHTYPE = $school->SCHTYPE;
        $current->RURURB = $school->RURURB;
        $current->CLUCD = $school->CLUCD;
        $current->BLKCD = $school->BLKCD;
        $current->VILCD = $school->VILCD;
        $current->DISTCD = $school->DISTCD;
        $current->PANCD = $school->PANCD;
        $current->SCHSTATUS = $school->SCHSTATUS;
        $current->SCHTYPES = $school->SCHTYPES;
        $current->SCHTYPEHS = $school->SCHTYPEHS;
        $current->SCHMGTS = $school->SCHMGTS;
        $current->SCHMGTHS = $school->SCHMGTHS;
        $current->save();

    }

    /**
     * check the school has elementary secion
     *
     * @return boolean
     */
    function hasElementary()
    {
        return self::isElementary($this->LOWCLASS);
    }

    /**
     * check the school has secondary secion
     *
     * @return boolean
     */
    function hasSecondary()
    {
        if( ! isset($this->LOWCLASS) ||  ! isset($this->HIGHCLASS)) {
            return false;
        }
        return self::isSecondary($this->LOWCLASS, $this->HIGHCLASS);
    }

    /**
     * check the school has higher-secondary secion
     *
     * @return Boolean
     */
    function hasHigherSecondary()
    {
        return self::isHigherSecondary($this->HIGHCLASS);
    }

    /**
     * check the school has Primary section
     *
     * @return Boolean
     */

    function hasPrimary()
    {
        if( ! isset($this->LOWCLASS)) {
            return false;
        }

        return self::isPrimary($this->LOWCLASS >= 5);
    }


    /**
     * check the school has upper primary
     *
     * @return Boolean
     */


    function hasUpperPrimary()
    {
        return self::isUpperPrimary($this->LOWCLASS, $this->HIGHCLASS);
    }

    /**
     * check the school is government
     *
     * @return Boolean
     */
    function isGov()
    {
        return in_array($this->SCHMGT, Master::$GOV_MANAGEMENTS) ||
            in_array($this->SCHMGTS, Master::$GOV_MANAGEMENTS) ||
            in_array($this->SCHMGTHS, Master::$GOV_MANAGEMENTS);
    }

    /**
     * check the school is government-aided
     *
     * @return Boolean
     */
    function isGovAided()
    {
        return $this->SCHMGT == 4 || $this->SCHMGTS == 4 || $this->SCHMGTHS == 4;
    }
    function isPvtUnaided()
    {
        return $this->SCHMGT == 5 || $this->SCHMGTS == 5 || $this->SCHMGTHS == 5;
    }

    public static function getCategory($low, $high)
    {

        if(self::isPrimary($low) &&  self::isUpperPrimary($low, $high) && self::isSecondary($low, $high) && self::isHigherSecondary($high)) {
            return 3;
        } elseif(self::isPrimary($low) &&  self::isUpperPrimary($low, $high) && self::isSecondary($low, $high)) {
            return 6;
        } elseif(self::isUpperPrimary($low, $high) && self::isSecondary($low, $high) && self::isHigherSecondary($high)) {
            return 5;
        } elseif(self::isPrimary($low) && self::isUpperPrimary($low, $high)) {
            return 2;
        } elseif(self::isUpperPrimary($low, $high) && self::isSecondary($low, $high)) {
            return 7;
        } elseif(self::isSecondary($low, $high) && self::isHigherSecondary($high)) {
            return 10;
        } elseif(self::isPrimary($low)) {
            return 1;
        } elseif(self::isUpperPrimary($low, $high)) {
            return 4;
        } elseif(self::isSecondary($low, $high)) {
            return 8;
        } elseif(self::isHigherSecondary($high)) {
            return 11;
        }
        return null;
    }

    public static function isSecondary($lowClass, $highClass) {
        if($lowClass > 10 ||  $highClass < 9) {
            return false;
        }
        return true;
    }

    public static function isHigherSecondary($highClass) {
        if($highClass >= 11) {
            return true;
        }
        return false;
    }

    public static function isPrimary($lowClass) {
        if($lowClass >= 5) {
            return false;
        }
        return true;
    }

    public static function isUpperPrimary($lowClass, $highClass) {
        if($lowClass <= 6 && $highClass >= 6) {
            return true;
        }
        return false;
    }

    public static function isElementary($lowClass)
    {
        if($lowClass < 8) {
            return true;
        }
        return false;
    }

    public function school()
    {
        return $this->belongsTo('School', 'SCHCD');
    }

    /**
     * returns allowed routes for school
     * @return array
     */
    public function getUdiseRoutes()
    {
        $routes[] = 'particulars-one';
        $routes[] = 'particulars-two';
        $routes[] = 'facilities-one';
        $routes[] = 'enrolment-by-category';
        $routes[] = 'enrolment-minority-category';
        $routes[] = 'enrolment-by-medium';
        $routes[] = 'repeaters-by-category';
        $routes[] = 'repeaters-minority-category';
        $routes[] = 'facilities-cwsn';

        if ($this->LOWCLASS == 1) {
            $routes[] = 'first-grade-admission';
        }
        if ($this->hasElementary()) {
            $routes[] = 'particulars-three';
            $routes[] = 'exam-results';
        }
        if ($this->hasPrimary()) {
            $routes[] = 'enrolment-by-age';
            $routes[] = 'children-with-special-needs';
            $routes[] = 'facilities-to-children-primary';
        }
        if ($this->hasUpperPrimary()) {
            $routes[] = 'enrolment-by-age-up';
            $routes[] = 'children-with-special-needs-up';
            $routes[] = 'facilities-to-children-up';
        }
        if ($this->hasSecondary()) {
            $routes[] = 'enrolment-by-age-hs';
            $routes[] = 'children-with-special-needs-hs';
            $routes[] = 'x-result-by-category';
            $routes[] = 'result-by-score-x';
        }
        if ($this->hasHigherSecondary()) {
            $routes[] = 'enrolment-by-age-ss';
            $routes[] = 'children-with-special-needs-hss';
            $routes[] = 'streams-available';
            $routes[] = 'enrolment-by-stream';
            $routes[] = 'repeaters-by-stream';
            $routes[] = 'result-by-score-xii';
            $routes[] = 'result-by-stream';
        }
        if ($this->hasSecondary() || $this->hasHigherSecondary()) {
            $routes[] = 'particulars-four';
            $routes[] = 'facilities-two';
            $routes[] = 'staff';
        }
        if ( $this->isGov() || $this->isGovAided()) {

            $routes[] = 'mdm';

            if ($this->hasSecondary() || $this->hasHigherSecondary()) {
                $routes[] = 'rmsa-fund';
            }
        }
        return $routes;
    }

    /**
     * udise route information for generating menu
     *
     * return array of arrays
     */
    public function getUdiseRoutesForMenu()
    {
        $routes = $this->getUdiseRoutes();

        // we didn't made teachers route as mandatory because, there is no form associated with the route.
        // but handled inconsistencies using error, warning generation
        $routes[] = 'teachers';

        // entries in $routes will be returned
        $filteredRoutes = array_intersect_key(School::$UDISE_ROUTES, array_flip($routes));

        return $filteredRoutes;
    }

    /**
     * its updating tables to reflect the class change,
     * by deleting the additional class data lying there.
     *
     * @return void
     */
    public function removeNonExistingClasses()
    {
        $allClasses = range(1, 12);
        $classes = range($this->LOWCLASS, $this->HIGHCLASS);
        $data = array();
        $alternateData = array();
        $classesToDelete = array_diff_assoc($allClasses, $classes);

        // return if nothing has to be done
        if (empty($classesToDelete)) return;

        foreach ($classesToDelete as $class) {
            $data += array(
                "C{$class}_B" => null,
                "C{$class}_G" => null,
            );
            $alternateData += array(
                "C{$class}B" => null,
                "C{$class}G" => null
            );
        }
        // delete from enrolment
        Enrolment::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($data);


        // delete from ageEnrolment
        AgeEnrolment::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($alternateData);
        // delete from media enrolment
        MediumEnrolment::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($data);


        Cwsn::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($data);

        Repeater::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($data);

        // 5, 8, 10, 12 exam results STEPS_EXAMINATION09
        // here, redifining the classes for updating exam results
        $allClasses = array(5, 8, 10, 12);
        $classes = array();
        $data = array();

        if (($this->LOWCLASS <= 5 && $this->HIGHCLASS >= 5) || ($this->LOWCLASS <= 4 && $this->HIGHCLASS >= 4)){
            $classes[] = 5;
        }
        if (($this->LOWCLASS <= 7 && $this->HIGHCLASS >= 7) || ($this->LOWCLASS <= 8 && $this->HIGHCLASS >= 8)){
            $classes[] = 8;
        }
        if ($this->LOWCLASS <= 10 && $this->HIGHCLASS >= 10){
            $classes[] = 10;
        }
        if ($this->LOWCLASS <= 12 && $this->HIGHCLASS >= 12){
            $classes[] = 12;
        }

        $classesToDelete = array_diff_assoc($allClasses, $classes);

        foreach (Result::$CATEGORY_LIST as $category) {

            foreach($classesToDelete as $class) {
                    $data += array(
                        "C{$class}B_{$category['abbr']}" => null,
                        "C{$class}G_{$category['abbr']}" => null,
                    );
            }
        }

        Result::where('SCHCD', $this->SCHCD)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->update($data);
    }
}
