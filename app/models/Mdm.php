<?php

class Mdm extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_MDM';
    public $timestamps = false;

    public static $MDM_STATUS = [
        1 => '1 - Not provided',
        2 => '2 - provided & prepared in school premises',
        3 => '3 - provided but not prepared in school premises',
    ];

    public static $KITCHEN_STATUS = [
        1 => '1 - Available',
        3 => '3 - Under Construction',
        4 => '4 - Classroom used as kitchen',
    ];

    public static $MDM_SOURCE = [
        1 => '1 - From nearby school',
        2 => '2 - NGO',
        3 => '3 - Self Help Group',
        4 => '4 - PTA/MTA',
        5 => '5 - others ',
        6 => '6 - Gram panchayat',
        7 => '7 - central kitchen',
    ];

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previous = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', self::PREVIOUS_YEAR)->first();

        $current = new self();

        if(isset($previous->SCHCD)) {
            $current->MEALSINSCH = $previous->MEALSINSCH;
            $current->KITSHED = $previous->KITSHED;
            $current->MDM_MAINTAINER = $previous->MDM_MAINTAINER;
        }

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }

}
