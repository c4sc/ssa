<?php

class Village extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_VILLAGE';
    protected $primaray_key = 'VILCD';

    public static function getList($block)
    {
        $villages = self::where('block_villages.BLKCD', '=', $block)
            ->join('block_villages', 'STEPS_VILLAGE.VILCD', '=', 'block_villages.VILCD')
            ->select(DB::raw('CONCAT(STEPS_VILLAGE.VILNAME, " - ", STEPS_VILLAGE.VILCD) as name'),
            DB::raw('STEPS_VILLAGE.VILCD as id'))
            ->lists('name', 'id');

        if(count($villages)) {
            return $villages;
        }
        $district = substr($block, 0, 4);
        return Village::where('VILCD', 'LIKE', "{$district}%")
            ->orderBy('VILNAME')
            ->select(DB::raw('CONCAT(STEPS_VILLAGE.VILNAME, " - ", STEPS_VILLAGE.VILCD) as name'), 'VILCD')
            ->lists('name', 'VILCD');
    }
}
