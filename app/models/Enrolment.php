<?php

class Enrolment extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_ENROLMENT09';
    public $incrementing = false;
    public $timestamps = false;

    public static $CATEGORY = [
        1 => '1 - General',
        2 => '2 - SC',
        3 => '3 - ST',
        4 => '4 - OBC',
    ];

    public static $CATEGORYMINORITY = [
        5 => '5 - Muslim',
        6 => '6 - Christians',
        7 => '7 - Sikhs',
        8 => '8 - Buddhist',
        9 => '9 - Parsi',
        10 => '10 - Jain',
    ];

    public static function updateEnrolment($data, $categoryId, $groupId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('CASTEID', '=', $categoryId)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            0  => array(0),
            1  => range(1, 4),
            2  => range(5, 11),
        );

        // insert null/default values
        foreach($groups as $groupId => $groupItems) {

            foreach($groupItems as $item) {

                $newItem = new self();
                $newItem->CASTEID = $item;
                $newItem->ITEMIDGROUP = $groupId;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }
    }

    /**
     * sum it up
     * @param string schoolCode
     * @param int groupId
     * @param string year
     * @return array
     */
    public static function sum($schoolCode, $groupId, $year = AcademicYear::CURRENT_YEAR)
    {
        $sum = self::select(DB::raw('
            sum(IFNULL(CPP_B, 0)) as SPPB, sum(IFNULL(CPP_G, 0)) as SPPG,
            sum(IFNULL(C1_B, 0)) as S1B, sum(IFNULL(C1_G, 0)) as S1G,
            sum(IFNULL(C2_B, 0)) as S2B, sum(IFNULL(C2_G, 0)) as S2G,
            sum(IFNULL(C3_B, 0)) as S3B, sum(IFNULL(C3_G, 0)) as S3G,
            sum(IFNULL(C4_B, 0)) as S4B, sum(IFNULL(C4_G, 0)) as S4G,
            sum(IFNULL(C5_B, 0)) as S5B, sum(IFNULL(C5_G, 0)) as S5G,
            sum(IFNULL(C6_B, 0)) as S6B, sum(IFNULL(C6_G, 0)) as S6G,
            sum(IFNULL(C7_B, 0)) as S7B, sum(IFNULL(C7_G, 0)) as S7G,
            sum(IFNULL(C8_B, 0)) as S8B, sum(IFNULL(C8_G, 0)) as S8G,
            sum(IFNULL(C9_B, 0)) as S9B, sum(IFNULL(C9_G, 0)) as S9G,
            sum(IFNULL(C10_B, 0)) as S10B, sum(IFNULL(C10_G, 0)) as S10G,
            sum(IFNULL(C11_B, 0)) as S11B, sum(IFNULL(C11_G, 0)) as S11G,
            sum(IFNULL(C12_B, 0)) as S12B, sum(IFNULL(C12_G, 0)) as S12G
            '))->where('SCHCD', '=', $schoolCode)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->where('AC_YEAR', '=', $year)
            ->first();
        return $sum;

    }

    public static function sumUpDisabled($schoolCode, $groupId, $year = AcademicYear::CURRENT_YEAR)
    {
        return self::select(DB::raw('sum(IFNULL(CPP_B,0) + IFNULL(CPP_G, 0) + 
            IFNULL(C1_B, 0) + IFNULL(C1_G, 0)+ IFNULL(C2_B, 0)+   IFNULL(C2_G, 0) +  
            IFNULL(C3_B, 0)+ IFNULL(C3_G, 0) + IFNULL(C4_B, 0) + IFNULL(C4_G, 0) + 
            IFNULL(C5_B, 0)+ IFNULL(C5_G, 0)+ IFNULL(C6_B, 0) + IFNULL(C6_G, 0) + 
            IFNULL(C7_B, 0)+ IFNULL(C7_G, 0)+ IFNULL(C8_B, 0)+ IFNULL(C8_G, 0) + 
            IFNULL(C9_B, 0) + IFNULL(C9_G, 0)+ IFNULL(C10_B, 0)+ IFNULL(C10_G, 0) + 
            IFNULL(C11_B, 0) + IFNULL(C11_G, 0) + IFNULL(C12_B, 0) + IFNULL(C12_G, 0))
             as total'))
        ->where('SCHCD', '=', $schoolCode)
        ->where('ITEMIDGROUP', '=', $groupId)
        ->where('AC_YEAR', '=', $year)
        ->first();
    }
    public static function sumMinority($schoolCode, $casteId, $groupId = 2, $year = AcademicYear::CURRENT_YEAR)
    {
        return self::select(DB::raw('
            sum(IFNULL(CPP_B, 0)) as SPPB, sum(IFNULL(CPP_G, 0)) as SPPG,
            sum(IFNULL(C1_B, 0)) as S1B, sum(IFNULL(C1_G, 0)) as S1G,
            sum(IFNULL(C2_B, 0)) as S2B, sum(IFNULL(C2_G, 0)) as S2G,
            sum(IFNULL(C3_B, 0)) as S3B, sum(IFNULL(C3_G, 0)) as S3G,
            sum(IFNULL(C4_B, 0)) as S4B, sum(IFNULL(C4_G, 0)) as S4G,
            sum(IFNULL(C5_B, 0)) as S5B, sum(IFNULL(C5_G, 0)) as S5G,
            sum(IFNULL(C6_B, 0)) as S6B, sum(IFNULL(C6_G, 0)) as S6G,
            sum(IFNULL(C7_B, 0)) as S7B, sum(IFNULL(C7_G, 0)) as S7G,
            sum(IFNULL(C8_B, 0)) as S8B, sum(IFNULL(C8_G, 0)) as S8G,
            sum(IFNULL(C9_B, 0)) as S9B, sum(IFNULL(C9_G, 0)) as S9G,
            sum(IFNULL(C10_B, 0)) as S10B, sum(IFNULL(C10_G, 0)) as S10G,
            sum(IFNULL(C11_B, 0)) as S11B, sum(IFNULL(C11_G, 0)) as S11G,
            sum(IFNULL(C12_B, 0)) as S12B, sum(IFNULL(C12_G, 0)) as S12G
            '))
        ->where('SCHCD', '=', $schoolCode)
        ->where('ITEMIDGROUP', '=', $groupId)
        ->where('CASTEID', '=', $casteId)
        ->where('AC_YEAR', '=', $year)
        ->first();
    }

    /**
     * sum by caste
     * @param string schoolCode
     * @param string year
     * @return array
     */
    public static function sumByCaste($schoolCode, $year = AcademicYear::CURRENT_YEAR)
    {
        return Enrolment::sum($schoolCode, 1, $year);
    }

    public static function getDivisions($schoolCode, $year = AcademicYear::CURRENT_YEAR)
    {
        return Enrolment::findRecords($schoolCode, $year)
            ->where('CASTEID', 0)
            ->where('ITEMIDGROUP', '=', 0)
            ->first();
    }

    /**
     * check inconsistencies
     * @param $master   Master
     * @param $year     string
     */
    public static function isValid($master, $year = AcademicYear::CURRENT_YEAR)
    {
        // checking for zero in data
        $divisions = static::getDivisions($master->SCHCD);
        // pre primary
        if ($master->PPSEC_YN == 1 && empty($divisions->CPP_B)) {
            return array('type' => 'zero-filled-in-preprimary', 'messages' => ['error']);
        }

        // cross checking caste-enrlment with medium-enrolment
        $mediumEnrolmentSum = MediumEnrolment::getSum($master->SCHCD)->toArray();
        $casteEnrolmentSum = Enrolment::sumByCaste($master->SCHCD)->toArray();
        $ageEnrolmentSum = AgeEnrolment::sum($master->SCHCD)->toArray();

        // unsetting Preprimary data, as it is not present in all tables.
        unset($mediumEnrolmentSum['SPPB']);
        unset($mediumEnrolmentSum['SPPG']);

        unset($casteEnrolmentSum['SPPB']);
        unset($casteEnrolmentSum['SPPG']);

        unset($ageEnrolmentSum['SPPB']);
        unset($ageEnrolmentSum['SPPG']);

        if ( ! empty(array_diff_assoc($casteEnrolmentSum, $mediumEnrolmentSum))) {
            $notEqualList[] = 'social category wise enrolment is not equal with medium wise enrolment';
        }
        if ( ! empty(array_diff_assoc($casteEnrolmentSum, $ageEnrolmentSum))) {
            $notEqualList[] = 'social category wise enrolment is not equal with age wise enrolment';
        }
        if (isset($notEqualList)) {
            return array('type' => 'not-equal', 'messages'=> $notEqualList);
        }

        return false;
    }

}
