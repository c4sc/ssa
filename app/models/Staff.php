<?php

class Staff extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_NONTCH';
    public $incrementing = false;
    public $timestamps = false;

    public static $POSTS = array(
        1 => '1 - Accountant',
        2 => '2 - Library Assistant',
        3 => '3 - Laboratory Assistant',
        4 => '4 - UDC',
        5 => '5 - Head Clerk',
        6 => '6 - LDC',
        7 => '7 - Peon/MTS',
        8 => '8 - Night Watchman',
    );

    /**
     * update staff details
     *
     */
    public static function updateStaff($data, $postId,  $schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMIDGROUP', '=', 0)
            ->where('DESIG_ID', '=', $postId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previousData = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', self::PREVIOUS_YEAR)->get();

        if(count($previousData) != 6) {
            // insert null/default values
            foreach(range(1, 8) as $itemId) {

                $newItem = new self();
                $newItem->DESIG_ID = $itemId;
                $newItem->ITEMIDGROUP = 0;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
            return;
        }

        // insert values from previous year data
        foreach($previousData as $item) {

            $newItem = new self();
            $newItem->TOTSAN = $item->TOTSAN;
            $newItem->TOTPOS = $item->TOTPOS;
            $newItem->DESIG_ID = $item->DESIG_ID;
            $newItem->ITEMIDGROUP = $item->ITEMIDGROUP;
            $newItem->SCHCD = $schoolCode;
            $newItem->AC_YEAR = self::CURRENT_YEAR;
            $newItem->save();
        }
    }

}
