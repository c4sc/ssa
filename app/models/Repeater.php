<?php

class Repeater extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_REPEATERS09';
    public $incrementing = false;
    public $timestamps = false;


    public static function updateRepeater($data, $categoryId, $groupId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', $categoryId)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            1  => range(1, 4),
            2  => range(5, 11),
            3  => range(1, 3),
        );

        // insert null/default values
        foreach($groups as $groupId => $groupItems) {

            foreach($groupItems as $item) {

                $newItem = new self();
                $newItem->ITEMID = $item;
                $newItem->ITEMIDGROUP = $groupId;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }
    }

}
