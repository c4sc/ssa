<?php

class BlockCluster extends Eloquent {

	protected $fillable = ['BLKCD', 'CLUCD'];
    public $timestamps = false;

}
