<?php

class EducationBlock extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_EDUBLOCK';
    public $primaryKey = 'BLKCD';
    public $timestamps = false;
    public $incrementing = false;

    public static function getList($block)
    {
        $aeos = self::where('block_aeos.BLKCD', '=', $block)
            ->join('block_aeos', 'STEPS_EDUBLOCK.BLKCD', '=', 'block_aeos.aeo_id')
            ->select(DB::raw('CONCAT(STEPS_EDUBLOCK.BLKNAME, " - ", STEPS_EDUBLOCK.BLKCD) as name'),
            DB::raw('STEPS_EDUBLOCK.BLKCD as id'))
            ->lists('name', 'id');

        if(count($aeos)) {
            return $aeos;
        }
        $district = substr($block, 0, 4);
        return self::where('BLKCD', 'LIKE', "{$district}%")->orderBy('BLKNAME')
            ->select(DB::raw('CONCAT(STEPS_EDUBLOCK.BLKNAME, " - ", STEPS_EDUBLOCK.BLKCD) as name'), 'BLKCD')
            ->lists('name', 'BLKCD');
    }
}
