<?php

class Facility extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_FACILITIES';
    public $timestamps = false;

    public static $FACILITY_STATUS = [
        0 => '0 - Not Applicable',
        1 => '1 - Fully equipped ',
        2 => '2 - Partially equipped ',
        3 => '3 - Not equipped ',
        4 => '4 - Not Available',
    ];

    // groupId 4
    public static $STREAMS = [
        1 => 'Arts',
        2 => 'Science',
        3 => 'Commerce',
        4 => 'Vocational',
        5 => 'Other',
    ];

    public static function updateFacility($data, $itemId, $groupId, $schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', $itemId)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            1 => [1, 3, 5, 6, 8, 11, 12, 13, 14, 15, 16, 17],
            2 => range(1, 9),
            3 => [2, 5, 14, 16, 18, 19, 20, 21, 22],
            4 => range(1, 5)
        );
        // insert null/default values
        foreach($groups as $groupId => $groupItems) {
            foreach($groupItems as $item) {
                $data[] = array(
                    'ITEMID' => $item,
                    'ITEMIDGROUP' => $groupId,
                    'SCHCD' => $schoolCode,
                    'AC_YEAR' => self::CURRENT_YEAR
                );
            }
        }
		Eloquent::unguard();
        self::insert($data);
		Eloquent::reguard();

    }

    public static function getStreams($schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::findRecords($schoolCode, $year)
            ->where('ITEMIDGROUP', '=', 4)
            ->where('ITEMVALUE', '=', 1)
            ->orderBy('ITEMID')
            ->get();
    }

    /**
     * list of available streams
     * @param $schoolCode string
     * @return  array
     */
    public static function getStreamList($schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::findRecords($schoolCode, $year)
            ->where('ITEMIDGROUP', '=', 4)
            ->where('ITEMVALUE', '=', 1)
            ->orderBy('ITEMID')
            ->lists('ITEMID');
    }

}
