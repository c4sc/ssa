<?php

class Material extends Eloquent {

	protected $fillable = [];
    public $table = 'STEPS_TXTBKTLESPORTS';
    public $timestamps = false;

    public static $FREETXTBK = array(
        1 => '1 - Yes',
        2 => '2 - No',
        0 => '0 - Not applicable',
    );

    public static $ITEMS = array(
        'book' => 1,
        'tle' => 2,
        'sports' => 3
    );

	/**
	 * Find model by composite key
	 *
	 * @param  mixed $schoolCode
	 * @param  int $item
	 * @param  int $acYear
	 * @param  array  $columns
	 * @return \Illuminate\Database\Eloquent\Model|static|null
	 */
    public static function findRecord($schoolCode, $itemName, $year = AcademicYear::CURRENT_YEAR, $columns = array('*'))
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', self::$ITEMS[$itemName])
            ->first($columns);
    }

    /**
     * updating within scope
     *
     */
    public static function scopeItem($query, $itemName, $schoolCode, $year = AcademicYear::CURRENT_YEAR)
    {
        return $query->where('AC_YEAR', '=', $year)
            ->where('SCHCD', '=', $schoolCode)
            ->where('ITEMID', '=', self::$ITEMS[$itemName]);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previousData = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)->get();

        if(count($previousData) != 3) {
            // insert null/default values
            foreach(range(1, 3) as $id) {
                $newMaterial = new Material();
                $newMaterial->ITEMID = $id;
                $newMaterial->SCHCD = $schoolCode;
                $newMaterial->AC_YEAR = AcademicYear::CURRENT_YEAR;
                $newMaterial->save();
            }
            return;
        }

        // insert values from previous year data
        foreach($previousData as $material) {
            $newMaterial = new Material();
            foreach(range(1, 12) as $column) {
                $newMaterial->{"C$column"} =  $material->{"C$column"};
            }
            $newMaterial->ITEMID = $material->ITEMID;
            $newMaterial->SCHCD = $schoolCode;
            $newMaterial->AC_YEAR = AcademicYear::CURRENT_YEAR;
            $newMaterial->save();
        }
    }

}
