<?php

class MediumEnrolment extends AcademicYear
{

	protected $fillable = ['*'];
    public $table = 'STEPS_ENRMEDINSTR';
    public $incrementing = false;
    public $timestamps = false;

    public static function updateMediumEnrolment($data, $mediumId, $schoolCode, $year = self::CURRENT_YEAR)
    {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('MEDIUM', '=', $mediumId)
            ->update($data);
    }

    /**
     * get different mediums used in school
     *
     * @param int $schoolCode
     * @return array()
     */
    public static function getSchoolMediums($schoolCode)
    {
        $master = Master::findRecords($schoolCode, AcademicYear::CURRENT_YEAR,
            array('MEDINSTR1', 'MEDINSTR2', 'MEDINSTR3', 'MEDINSTR4'))
            ->first();

        $mediums = array();
        $mediumIdList = array();
        foreach(range(1, 4) as $sequence) {
            if(in_array($master->{"MEDINSTR$sequence"}, range(1, 29)) && ( ! in_array($master->{"MEDINSTR$sequence"}, $mediumIdList))) {

                // if the medium is valid and its not already there in our list
                $name = DB::table('STEPS_MEDINSTR')
                    ->where('MEDINSTR_ID', $master->{"MEDINSTR$sequence"})
                    ->pluck('MEDINSTR_DESC');
                $mediums[] = array('id' => $master->{"MEDINSTR$sequence"}, 'sequence' => $sequence, 'name' => $name);
                $mediumIdList[] = $master->{"MEDINSTR$sequence"};
            }
        }
        return $mediums;
    }

    public static function getSum($schoolCode, $year=AcademicYear::CURRENT_YEAR)
    {
        $mediums = static::getSchoolMediums($schoolCode);
        $mediums = array_column($mediums, 'id');

        return MediumEnrolment::select(DB::raw('
            sum(IFNULL(CPP_B, 0)) as SPPB, sum(IFNULL(CPP_G, 0)) as SPPG,
            sum(IFNULL(C1_B, 0)) as S1B, sum(IFNULL(C1_G, 0)) as S1G,
            sum(IFNULL(C2_B, 0)) as S2B, sum(IFNULL(C2_G, 0)) as S2G,
            sum(IFNULL(C3_B, 0)) as S3B, sum(IFNULL(C3_G, 0)) as S3G,
            sum(IFNULL(C4_B, 0)) as S4B, sum(IFNULL(C4_G, 0)) as S4G,
            sum(IFNULL(C5_B, 0)) as S5B, sum(IFNULL(C5_G, 0)) as S5G,
            sum(IFNULL(C6_B, 0)) as S6B, sum(IFNULL(C6_G, 0)) as S6G,
            sum(IFNULL(C7_B, 0)) as S7B, sum(IFNULL(C7_G, 0)) as S7G,
            sum(IFNULL(C8_B, 0)) as S8B, sum(IFNULL(C8_G, 0)) as S8G,
            sum(IFNULL(C9_B, 0)) as S9B, sum(IFNULL(C9_G, 0)) as S9G,
            sum(IFNULL(C10_B, 0)) as S10B, sum(IFNULL(C10_G, 0)) as S10G,
            sum(IFNULL(C11_B, 0)) as S11B, sum(IFNULL(C11_G, 0)) as S11G,
            sum(IFNULL(C12_B, 0)) as S12B, sum(IFNULL(C12_G, 0)) as S12G'))
            ->where('SCHCD', '=', $schoolCode)
            ->where('ac_year', $year)
            ->whereIn('medium', $mediums)
            ->first();

    }
}
