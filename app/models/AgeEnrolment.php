<?php

class AgeEnrolment extends AcademicYear
{
	protected $fillable = [];
    public $table = 'STEPS_ENRAGEBYCASTE';
    public $incrementing = false;
    public $timestamps = false;

    public static $agelimit = array(
        1 => array(4, 12),
        2 => array(5, 13),
        3 => array(7,23),
        4 => array(8,23),
        5 => array(9,23),
        6 => array(10,23),
        7 => array(11,23),
        8 => array(12,23),
        9 => array(12,23),
        10 => array(14,23),
        11 => array(14,23),
        12 => array(15,23),
    );

    public static function updateEnrolment($data, $categoryId, $ageId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('CASTEID', '=', $categoryId)
            ->where('AGEID', '=', $ageId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        // insert null/default values
        foreach (range(4, 23) as $ageId) {
            $newItem = new self();
            $newItem->CASTEID = 0;
            $newItem->AGEID = $ageId;
            $newItem->SCHCD = $schoolCode;
            $newItem->AC_YEAR = self::CURRENT_YEAR;
            $newItem->save();
        }
    }

    /**
     * sum it up
     * @param array class
     * @return array
     */
    public static function sum($schoolCode, $year = AcademicYear::CURRENT_YEAR)
    {
        $sum = self::select(DB::raw('
            sum(IFNULL(PPB, 0)) as SPPB, sum(IFNULL(PPG, 0)) as SPPG,
            sum(IFNULL(C1B, 0)) as S1B, sum(IFNULL(C1G, 0)) as S1G,
            sum(IFNULL(C2B, 0)) as S2B, sum(IFNULL(C2G, 0)) as S2G,
            sum(IFNULL(C3B, 0)) as S3B, sum(IFNULL(C3G, 0)) as S3G,
            sum(IFNULL(C4B, 0)) as S4B, sum(IFNULL(C4G, 0)) as S4G,
            sum(IFNULL(C5B, 0)) as S5B, sum(IFNULL(C5G, 0)) as S5G,
            sum(IFNULL(C6B, 0)) as S6B, sum(IFNULL(C6G, 0)) as S6G,
            sum(IFNULL(C7B, 0)) as S7B, sum(IFNULL(C7G, 0)) as S7G,
            sum(IFNULL(C8B, 0)) as S8B, sum(IFNULL(C8G, 0)) as S8G,
            sum(IFNULL(C9B, 0)) as S9B, sum(IFNULL(C9G, 0)) as S9G,
            sum(IFNULL(C10B, 0)) as S10B, sum(IFNULL(C10G, 0)) as S10G,
            sum(IFNULL(C11B, 0)) as S11B, sum(IFNULL(C11G, 0)) as S11G,
            sum(IFNULL(C12B, 0)) as S12B, sum(IFNULL(C12G, 0)) as S12G
            '))->where('SCHCD', '=', $schoolCode)
            ->where('CASTEID', '=', 0)
            ->where('AC_YEAR', '=', $year)
            ->first();
        return $sum;

    }

}
