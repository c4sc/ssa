<?php

class Building extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_BLDEQP';
    protected $primaryKey = 'SCHCD';
    public $timestamps = false;
    public $incrementing = false;

    public static $STATUS = array(
        10 => '10 - Dilapilated',
        11 => '11 - Under Construction',
        1 => '1 - Private',
        2 => '2 - Rented',
        3 => '3 - Government',
        4 => '4 - Govt. school in rent free building',
        6 => '6 - Dilapidated',
        7 => '7 - Under Construction',
    );

    public static $WATER_SOURCE = array(
        1 => '1 - Handpump',
        2 => '2 - Well',
        3 => '3 - Tap Water',
        4 => '4 - Others',
        5 => '5 - None',
    );

    public static $ELECTRICITY_STATUS = array(
        1 => '1 - Yes',
        2 => '2 - No',
        3 => '3 - Yes but not functional',
    );

    public static $FURNITURE_AVAILABILITY = array(
        1 => '1 - All',
        2 => '2 - Some',
        3 => '3 - None',
    );

    public static $BOUNDARY_WALL = array(
        1 => '1 - Pucca',
        2 => '2 - Pucca but Broken',
        3 => '3 - Barbed wire fencing',
        4 => '4 - Hedges',
        5 => '5 - No Boundary wall',
        6 => '6 - Others',
        7 => '7 - Partial',
        8 => '8 - Under Construction',
    );

    public static $BUILDING_TYPE = array(
        array(
            'pucca'=>'',
            'partially-pucca'=>'_PPU',
            'kucha'=>'_KUC',
            'tent'=>'_TNT'),
        array('secondary'=>'S'),
        array('higher-secondary'=>'HS')
    );

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previous = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', self::PREVIOUS_YEAR)->first();

        $current = new self();

        if(isset($previous->SCHCD)) {

            $current->BLDSTATUS = $previous->BLDSTATUS;
            $current->CLROOMS = $previous->CLROOMS;
            $current->CLSUNDERCONST = $previous->CLSUNDERCONST;
            $current->FURNSTU = $previous->FURNSTU;
            $current->OTHROOMS = $previous->OTHROOMS;
            $current->LAND4CLS_YN = $previous->LAND4CLS_YN;
            $current->HMROOM_YN = $previous->HMROOM_YN;
            $current->TOILETB = $previous->TOILETB;
            $current->TOILET_G = $previous->TOILET_G;
            $current->TOILETB_FUNC = $previous->TOILETB_FUNC;
            $current->TOILETG_FUNC = $previous->TOILETG_FUNC;
            $current->TOILETWATER_B = $previous->TOILETWATER_B;
            $current->TOILETWATER_G = $previous->TOILETWATER_G;
            $current->URINALS_B = $previous->URINALS_B;
            $current->URINALS_G = $previous->URINALS_G;
            $current->HANDWASH_YN = $previous->HANDWASH_YN;
            $current->TOILETD = $previous->TOILETD;
            $current->WATER = $previous->WATER;
            $current->WATER_FUNC_YN = $previous->WATER_FUNC_YN;
            $current->ELECTRIC_YN = $previous->ELECTRIC_YN;
            $current->BNDRYWALL = $previous->BNDRYWALL;
            $current->LIBRARY_YN = $previous->LIBRARY_YN;
            $current->BOOKINLIB = $previous->BOOKINLIB;
            $current->LIBRARIAN_YN = $previous->LIBRARIAN_YN;
            $current->NEWSPAPER_YN = $previous->NEWSPAPER_YN;
            $current->PGROUND_YN = $previous->PGROUND_YN;
            $current->LAND4PGROUND_YN = $previous->LAND4PGROUND_YN;
            $current->COMPUTER = $previous->COMPUTER;
            $current->CAL_YN = $previous->CAL_YN;
            $current->MEDCHK_YN = $previous->MEDCHK_YN;
            $current->RAMPSNEEDED_YN = $previous->RAMPSNEEDED_YN;
            $current->RAMPS_YN = $previous->RAMPS_YN;
            $current->HANDRAILS = $previous->HANDRAILS;
            $current->CAMPUSPLAN_YN = $previous->CAMPUSPLAN_YN;
            $current->HOSTELB_YN = $previous->HOSTELB_YN;
            $current->HOSTELBOYS = $previous->HOSTELBOYS;
            $current->HOSTELG_YN = $previous->HOSTELG_YN;
            $current->HOSTELGIRLS = $previous->HOSTELGIRLS;

            foreach(range(9, 12) as $class) {
                $current->{"TOTCLS$class"} = $previous->{"TOTCLS$class"};
                $current->{"CLSUCONST$class"} = $previous->{"CLSUCONST$class"};
                $current->{"FURN_YN$class"} = $previous->{"FURN_YN$class"};
            }

            foreach(Building::$BUILDING_TYPE as $sections) {
                foreach($sections as $type => $suffix) {
                    $current->{"CLGOOD$suffix"} = $previous->{"CLGOOD$suffix"};
                    $current->{"CLMINOR$suffix"} = $previous->{"CLMINOR$suffix"};
                    $current->{"CLMAJOR$suffix"} = $previous->{"CLMAJOR$suffix"};
                }
            }
        }

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }
}
