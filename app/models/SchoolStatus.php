<?php

class SchoolStatus extends Eloquent {

	protected $fillable = [];
    protected $table = 'school_status';
    const SAVED = 'S';
    const UDISE = 'U';

    public static function setStatus($schoolCode, $route, $status='S')
    {
        $link = Link::where('route', $route)->first();
        if( ! isset($link->id)) {
            return;
        }

        if (self::where('school_code', $schoolCode)->where('link_id', $link->id)
            ->where('academic_year', AcademicYear::CURRENT_YEAR)->exists()){
            // if the row exists, updating status

            self::where('school_code', $schoolCode)
                ->where('link_id', $link->id)
                ->where('academic_year', AcademicYear::CURRENT_YEAR)
                ->update(array('status'=>$status));

            return;
        }

        // creating a new row for storing status
        $schoolStatus = new self;
        $schoolStatus->link_id = $link->id;
        $schoolStatus->school_code = $schoolCode;
        $schoolStatus->status = $status;
        $schoolStatus->academic_year = AcademicYear::CURRENT_YEAR;
        $schoolStatus->save();
    }

    public function school()
    {
        return $this->belongsTo('School', 'school_code');
    }

    /**
     * This function is used to filter the whole statuses and returns
     * items with the specific @param $choolCode.
     * @param $collection is receiving whole collection.
     */
    public static function filterSchool($collection, $schoolCode)
    {
        return  $collection->filter(function($schoolStatus) use ($schoolCode){
            return $schoolStatus->school_code == $schoolCode;

        });
    }

    /**
     * This function is used to filter the whole statuses and returns
     * items with the specific @param $linkId.
     * @param $collection is receiving whole collection.
     * @filterSchool function is also doing the same
     * but this filters link and also it is saved or a specific status.  (S,C. G... etc)
     * Currently not using {18/12/2014}
     */
    public static function filterSchoolStatus($collection, $linkId, $status = 'S')
    {
        return  $collection->filter(function($schoolStatus) use ($linkId, $status){
            if ($schoolStatus->link_id == $linkId) {
                return $schoolStatus->status == $status;
            }
        });
    }

    /**
     * status of school
     *
     * @param $schoolCode string
     * @param $year      string
     * @return Collection
     */
    public static function getStatus($schoolCode, $status = false, $year = AcademicYear::CURRENT_YEAR)
    {
        $query = DB::table('school_status')
            ->select('links.id', 'route', 'title', 'status')
            ->rightJoin('links', function($join) use($schoolCode, $year) {
                $join->on('links.id', '=', 'school_status.link_id')
                    ->where('school_code', '=', $schoolCode)
                    ->where('academic_year', '=', $year);
            });

        if ($status) {
            $query = $query->where('status', '=', $status);
        }
        return $query;
    }

    /**
     * status of school by route-list
     *
     * @param $schoolCode string
     * @param $route     string/array
     * @param $year      string
     * @return Collection
     */
    public static function getStatusByRoutes($schoolCode, $routes, $status = false, $year = AcademicYear::CURRENT_YEAR)
    {
        if ( ! is_array($routes)) {
            $routes = array($routes);
        }
        $query = static::getStatus($schoolCode, $status, $year);
        $query = $query->whereIn('route', $routes);
        return $query->get();
    }

    /**
     * status of school by category
     *
     * @param $schoolCode string
     * @param $category string
     * @param $year      string
     * @return Collection
     */
    public static function getStatusByCategory($schoolCode, $category, $status = false, $year = AcademicYear::CURRENT_YEAR)
    {
        $query = static::getStatus($schoolCode, $status, $year);
        $query = $query->where('category', '=', $category);
        return $query->get();
    }

    /**
     * status of school by route-list
     *
     * @param $schoolCode string
     * @param $route     string/array
     * @param $year      string
     * @return Collection
     */
    public static function getStatusByRouteName($schoolCode, $routeName, $status = false, $year = AcademicYear::CURRENT_YEAR)
    {
        $query = static::getStatus($schoolCode, $status, $year);
        $query = $query->where('route', $routeName);
        return $query->first();
    }

}
