<?php

class Municipality extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_MUNICIPALITY';
    public $primaryKey = 'MUNCD';
    public $timestamps = false;
    public $incrementing = false;

    public static function getList($block)
    {
        $municipalities = self::where('block_municipalities.BLKCD', '=', $block)
            ->join('block_municipalities', 'STEPS_MUNICIPALITY.MUNCD', '=', 'block_municipalities.MUNCD')
            ->select(DB::raw('CONCAT(STEPS_MUNICIPALITY.MUNNAME, " - ", STEPS_MUNICIPALITY.MUNCD) as name'),
            DB::raw('STEPS_MUNICIPALITY.MUNCD as id'))
            ->lists('name', 'id');

        if(count($municipalities)) {
            return $municipalities;
        }
        $district = substr($block, 0, 4);
        return self::where('MUNCD', 'LIKE', "{$district}%")->orderBy('MUNNAME')
            ->select(DB::raw('CONCAT(STEPS_MUNICIPALITY.MUNNAME, " - ", STEPS_MUNICIPALITY.MUNCD) as name'), 'MUNCD')
            ->lists('name', 'MUNCD');
    }
}
