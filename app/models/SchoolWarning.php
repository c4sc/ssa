<?php

class SchoolWarning extends Eloquent {

    protected $fillable = ['warning_id', 'SCHCD'];

    public static function addWarning($schoolCode, $name, $message = null)
    {
        $warning = Warning::where('name', '=', $name)->first();
        $new = Self::firstOrNew( array( 'warning_id' => $warning->id, 'SCHCD' => $schoolCode));
        $new->message = $message;
        $new->touch();
        return $new->save();
    }

    public static function deleteWarning($schoolCode, $name)
    {
        $warning = Warning::where('name', '=', $name)->first();
        return Self::where('warning_id','=', $warning->id)
            ->where('SCHCD', '=', $schoolCode)->delete();
    }

    public function warning()
    {
        return $this->belongsTo('Warning');
    }

    public static function getWarningsByType($schoolCode, $type)
    {
        return DB::table('school_warnings')
            ->join('warnings', function($join) use($type, $schoolCode) {
                $join->on('warnings.id', '=',  'school_warnings.warning_id')
                    ->where('warnings.type', '=', $type);
            })
            ->select('warnings.id', 'warnings.name', 'warnings.route', 'warnings.type', 'message', 'warnings.description')
            ->where('school_warnings.schcd', '=', $schoolCode)
            ->get();
    }

    public static function getWarningByName($schoolCode, $name)
    {
        return DB::table('school_warnings')
            ->join('warnings', function($join) use($name) {
                $join->on('warnings.id', '=', 'school_warnings.warning_id')
                    ->where('warnings.name', '=', $name);
            })
            ->select('warnings.id', 'warnings.name', 'warnings.route', 'warnings.type', 'message', 'warnings.description')
            ->where('schcd', $schoolCode)
            ->first();
    }

    public static function getWarningsByRoute($schoolCode, $route)
    {
        return DB::table('school_warnings')
            ->join('warnings', function($join) use($route) {
                $join->on('warnings.id', '=', 'school_warnings.warning_id')
                    ->where('warnings.route', '=', $route);
            })
            ->select('warnings.id', 'warnings.name', 'warnings.route', 'warnings.type', 'message', 'warnings.description')
            ->where('schcd', $schoolCode)
            ->get();
    }
}
