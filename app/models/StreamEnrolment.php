<?php

class StreamEnrolment extends AcademicYear
{
	protected $fillable = [];
    public $table = 'STEPS_EREPBYSTREAM';
    public $incrementing = false;
    public $timestamps = false;

    public static function updateStreamData($data, $streamId, $categoryId, $groupId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('STREAMID', '=', $streamId)
            ->where('CASTEID', '=', $categoryId)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            0  => array(0),
            1  => range(1, 4),
            2  => range(5, 11),
        );

        // insert null/default values
        foreach(range(1, 5) as $streamId) {
            foreach(range(1, 4) as $casteId) {

                $newItem = new self();
                $newItem->CASTEID = $casteId;
                $newItem->STREAMID = $streamId;
                $newItem->ITEMIDGROUP = 1;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
            foreach(range(5, 11) as $casteId) {

                $newItem = new self();
                $newItem->CASTEID = $casteId;
                $newItem->STREAMID = $streamId;
                $newItem->ITEMIDGROUP = 2;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }
    }

}
