<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryModel;
class User extends SentryModel {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function fullName()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * checks permission to access school
     *
     * @param Model School
     * @param permission
     *
     * @return boolean
     */
    public function hasAccessToSchool($school, $permission = 'schoolReport')
    {
        switch($this->scope){
            case AuthorizedController::$SCOPE_SCHOOL:
                if($school->SCHCD != $this->scope_id) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_VILLAGE:
                if($school->VILCD != $this->scope_id) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_BLOCK:
                if($school->BLKCD != $this->scope_id) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_DISTRICT:
                if($school->DISTCD != $this->scope_id) {
                    return false;
                }
                break;
            case 'ST':
                break;
            default:
                return false;
        }

        return $this->hasAccess($permission);
    }

    /**
     * checks permission to access block
     *
     * @param block_id
     * @param permission
     *
     * @return boolean
     */
    public function hasAccessToBlock($block_id, $permission = 'blockReport')
    {
        switch($this->scope) {
            case AuthorizedController::$SCOPE_SCHOOL:
            case AuthorizedController::$SCOPE_VILLAGE:
                if(substr($this->scope_id, 0, 6) != $block_id ) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_BLOCK:
                if($this->scope_id != $block_id ) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_DISTRICT:
                if(substr($block_id, 0, 4) != $this->scope_id) {
                    return false;
                }
                break;
            case 'ST':
                break;
            default:
                return false;
        }

        return $this->hasAccess($permission);
    }

    /**
     * checks permission to access district
     *
     * @param district_id
     * @param permission
     *
     * @return boolean
     */
    public function hasAccessToDistrict($district_id, $permission = 'districtReport')
    {
        switch($this->scope){
            case AuthorizedController::$SCOPE_SCHOOL:
            case AuthorizedController::$SCOPE_VILLAGE:
            case AuthorizedController::$SCOPE_BLOCK:
                if(substr($this->scope_id, 0, 4) != $district_id ) {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_DISTRICT:
                if($district_id != $this->scope_id) {
                    return false;
                }
                break;
            case 'ST':
                break;
            default:
                return false;
        }

        return $this->hasAccess($permission);
    }

    /**
     * checks permission to access state
     *
     * @param state_id
     * @param permission
     *
     * @return boolean
     */
    public function hasAccessToState($state_id, $permission = 'stateReport')
    {
        switch($this->scope){
            case AuthorizedController::$SCOPE_SCHOOL:
            case AuthorizedController::$SCOPE_VILLAGE:
            case AuthorizedController::$SCOPE_BLOCK:
            case AuthorizedController::$SCOPE_DISTRICT:
                if(substr($this->scope_id,0, 2) != $state_id ) {
                    return false;
                }
                break;
            case 'ST':
                break;
            default:
                return false;
        }

        return $this->hasAccess($permission);
    }

    /**
     * checks permission to access user
     *
     * @param School
     * @param permission
     *
     * @return boolean
     */
    public function hasAccessToUser($user, $permission = 'user')
    {
        switch($this->scope) {
            case AuthorizedController::$SCOPE_SCHOOL:
                if ( $user->scope != AuthorizedController::$SCOPE_SCHOOL || $user->scope_id != $this->scope_id )
                {
                    return false;
                }
                break;
            case AuthorizedController::$SCOPE_VILLAGE:
                switch($user->scope) {
                    case AuthorizedController::$SCOPE_VILLAGE:
                        if ($user->scope_id != $this->scope_id) {
                            return false;
                        }
                        break;

                    case AuthorizedController::$SCOPE_SCHOOL:
                        if(School::find($user->scope_id)->VILCD == $this->scope_id) {
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
                break;
            case AuthorizedController::$SCOPE_BLOCK:
                switch($user->scope) {
                    case AuthorizedController::$SCOPE_BLOCK:
                        if ($user->scope_id != $this->scope_id) {
                            return false;
                        }
                        break;

                    case AuthorizedController::$SCOPE_SCHOOL:
                        if(School::find($user->scope_id)->BLKCD != $this->scope_id) {
                            return false;
                        }
                        break;
                    default:
                        return false;
                }
                break;
            case AuthorizedController::$SCOPE_DISTRICT:
                if (
                    (($user->scope != AuthorizedController::$SCOPE_BLOCK ||
                    $user->scope != AuthorizedController::$SCOPE_SCHOOL ) ||
                    ($user->scope != AuthorizedController::$SCOPE_VILLAGE||
                    $user->scope != AuthorizedController::$SCOPE_DISTRICT )) &&
                    (substr($user->scope_id, 0, 4) != $this->scope_id)
                )
                {
                    return false;
                }
                break;
            case 'ST':
                break;
            default:
                return false;
        }

        return $this->hasAccess($permission);
    }
}
