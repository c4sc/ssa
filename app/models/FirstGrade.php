<?php

class FirstGrade extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_NEWADMGRD1';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $current = new self();
        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }
}
