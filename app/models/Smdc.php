<?php

class Smdc extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_SMDC';
    public $timestamps = false;

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $previous = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', self::PREVIOUS_YEAR)->first();

        $current = new self();

        if(isset($previous->SCHCD)) {

            $current->WORKDAYS_SEC = $previous->WORKDAYS_SEC;
            $current->WORKDAYS_HSEC = $previous->WORKDAYS_HSEC;
            $current->SCHHRSCHILD_SEC = $previous->SCHHRSCHILD_SEC;
            $current->SCHHRSCHILD_HSEC = $previous->SCHHRSCHILD_HSEC;
            $current->SCHHRSTCH_SEC = $previous->SCHHRSTCH_SEC;
            $current->SCHHRSTCH_HSEC = $previous->SCHHRSTCH_HSEC;
            $current->CCESEC_YN = $previous->CCESEC_YN;
            $current->CCEHSEC_YN = $previous->CCEHSEC_YN;
            $current->SMDC_YN = $previous->SMDC_YN;
            $current->SMCSMDC1_YN = $previous->SMCSMDC1_YN;
            $current->TOT_M = $previous->TOT_M;
            $current->TOT_F = $previous->TOT_F;
            $current->PARENTS_M = $previous->PARENTS_M;
            $current->PARENTS_F = $previous->PARENTS_F;
            $current->LOCAL_M = $previous->LOCAL_M;
            $current->LOCAL_F = $previous->LOCAL_F;
            $current->EBMC_M = $previous->EBMC_M;
            $current->EBMC_F = $previous->EBMC_F;
            $current->WOMEN_M = $previous->WOMEN_M;
            $current->SCST_M = $previous->SCST_M;
            $current->SCST_F = $previous->SCST_F;
            $current->DEO_M = $previous->DEO_M;
            $current->DEO_F = $previous->DEO_F;
            $current->AAD_M = $previous->AAD_M;
            $current->AAD_F = $previous->AAD_F;
            $current->SUBEXP_M = $previous->SUBEXP_M;
            $current->SUBEXP_F = $previous->SUBEXP_F;
            $current->TCH_M = $previous->TCH_M;
            $current->TCH_F = $previous->TCH_F;
            $current->AHM_M = $previous->AHM_M;
            $current->AHM_F = $previous->AHM_F;
            $current->HM_M = $previous->HM_M;
            $current->HM_F = $previous->HM_F;
            $current->CP_M = $previous->CP_M;
            $current->CP_F = $previous->CP_F;
            $current->SMDCMEETING = $previous->SMDCMEETING;
            $current->SIP_YN = $previous->SIP_YN;
            $current->BANKAC_YN = $previous->BANKAC_YN;
            $current->BANKNAME = $previous->BANKNAME;
            $current->BANKBRANCH = $previous->BANKBRANCH;
            $current->BANKACNO = $previous->BANKACNO;
            $current->ACNAME = $previous->ACNAME;
            $current->IFSC = $previous->IFSC;
            $current->SBC_YN = $previous->SBC_YN;
            $current->AC_YN = $previous->AC_YN;
            $current->PTA_YN = $previous->PTA_YN;
            $current->PTAMEETING = $previous->PTAMEETING;
        }

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }
}
