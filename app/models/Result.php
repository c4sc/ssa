<?php

class Result extends AcademicYear {

	protected $fillable = [];
    public $table = 'STEPS_EXAMINATION09';
    public $incrementing = false;
    public $timestamps = false;

    public static $SCORE = array(
        1 => 'upto 40% ',
        2 => '41-60%',
        3 => '61-80%',
        4 => 'Above 80%',
    );

    public static $CATEGORY_LIST = array(
        1 => array('name' => 'General', 'abbr' => 'GEN'),
        2 => array('name' => 'SC', 'abbr' => 'SC'),
        3 => array('name' => 'ST', 'abbr' => 'ST'),
        4 => array('name' => 'OBC', 'abbr' => 'OBC'),
    );
    public static $RESULT_HEADS = array(
        1 => 'Enrolment at the end of previous academic session',
        2 => 'Number appeared',
        3 => 'Number passed',
        4 => 'Passed with more than 60%',
    );

    public static function updateResult($data, $categoryId, $groupId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', $categoryId)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->update($data);
    }

    /**
     * insert data based on previous year data
     *
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(
            1  => range(1, 4),
            3  => range(1, 4),
            4  => range(1, 5),
            5  => range(1, 4),
            6  => range(1, 4),
        );

        // insert null/default values
        foreach($groups as $groupId => $groupItems) {

            foreach($groupItems as $item) {

                $newItem = new self();
                $newItem->ITEMID = $item;
                $newItem->ITEMIDGROUP = $groupId;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }

    }

}
