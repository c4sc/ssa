<?php

class SchoolDetails extends AcademicYear
{
	protected $fillable = [];
    protected $table = 'STEPS_KEYVAR';
    public $timestamps = false;
    public $incrementing = false;

    /**
     * check the school is government
     *
     * @return Boolean
     */
    function isGov()
    {
        return in_array($this->SCHMGT, Master::$GOV_MANAGEMENTS) ||
            in_array($this->SCHMGTS, Master::$GOV_MANAGEMENTS) ||
            in_array($this->SCHMGTHS, Master::$GOV_MANAGEMENTS);
    }

    /**
     * check the school is government-aided
     *
     * @return Boolean
     */
    function isGovAided()
    {
        return $this->SCHMGT == 4 || $this->SCHMGTS == 4 || $this->SCHMGTHS == 4;
    }

    /**
     * insert data based on previous year data
     *
     * @todo filter the fields to be copied
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $exists = self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)->count() == 1;

        if ($exists) {
            return;
        }

        $previous = self::findRecord($schoolCode, AcademicYear::PREVIOUS_YEAR);

        if ( ! isset($previous->SCHCD)) {
            return;
        }

        $current = new self();
        // save
        $current->SCHNAME = $previous->SCHNAME;
        $current->SCHMGT = $previous->SCHMGT;
        $current->SCHCAT = $previous->SCHCAT;
        $current->SCHTYPE = $previous->SCHTYPE;
        $current->RURURB = $previous->RURURB;
        $current->CLUCD = $previous->CLUCD;
        $current->BLKCD = $previous->BLKCD;
        $current->VILCD = $previous->VILCD;
        $current->DISTCD = $previous->DISTCD;
        $current->DISTNAME = $previous->DISTNAME;
        $current->PANCD = $previous->PANCD;
        $current->SCHSTATUS = $previous->SCHSTATUS;
        $current->SCHTYPES = $previous->SCHTYPES;
        $current->SCHTYPEHS = $previous->SCHTYPEHS;
        $current->SCHMGTS = $previous->SCHMGTS;
        $current->SCHMGTHS = $previous->SCHMGTHS;

        $current->AC_YEAR = self::CURRENT_YEAR;
        $current->SCHCD = $schoolCode;
        $current->save();
    }

    public function school()
    {
        return $this->belongsTo('School', 'SCHCD');
    }
}
