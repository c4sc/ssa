<?php

class Constituency extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_CONSTITUENCY';
    public $primaryKey = 'CONSTCD';
    public $timestamps = false;
    public $incrementing = false;

    public static function getList($block)
    {
        $constituencies = self::where('block_constituencies.BLKCD', '=', $block)
            ->join('block_constituencies', 'STEPS_CONSTITUENCY.CONSTCD', '=', 'block_constituencies.CONSTCD')
            ->select(DB::raw('CONCAT(STEPS_CONSTITUENCY.CONSTNAME, " - ", STEPS_CONSTITUENCY.CONSTCD) as name'),
            DB::raw('STEPS_CONSTITUENCY.CONSTCD as id'))
            ->lists('name', 'id');

        if(count($constituencies)) {
            return $constituencies;
        }
        $district = substr($block, 0, 4);
        return self::where('CONSTCD', 'LIKE', "{$district}%")->orderBy('CONSTNAME')
            ->select(DB::raw('CONCAT(STEPS_CONSTITUENCY.CONSTNAME, " - ", STEPS_CONSTITUENCY.CONSTCD) as name'), 'CONSTCD')
            ->lists('name', 'CONSTCD');
    }
}
