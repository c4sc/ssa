<?php

class Block extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_BLOCK';
    public $primaryKey = 'BLKCD';

    public static function getList($district)
    {
        return self::where('BLKCD', 'LIKE', "{$district}%")
            ->orderBy('BLKNAME')
            ->select(DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name'), 'BLKCD')
            ->lists('name', 'BLKCD');
    }
}
