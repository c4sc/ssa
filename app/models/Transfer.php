<?php

class Transfer extends AcademicYear
{
    protected $fillable = [];
    protected $table = 'transfer_pool';
    public $timestamps = true;
    public $incrementing = true;

    const PENDING = 'P';
    const CANCELED = 'C';
    const APPROVED = 'A';

}
