<?php

class StudentDetails extends Elegant
{
    protected $fillable = [];
    protected $table = 'student_details';
    public $timestamps = true;
    public $incrementing = true;

    public static $CLASS = [
        0 => 'PrePrimary',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        ];
    public static $PREVIOUSCLASS = [
        0 => 'PrePrimary',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        99 => 'None',
        ];

    public static $PREVIOUSSTATUS = [
        1 => 'Same School',
        2 => 'Another School',
        3 => 'Anganwadi-ECCE',
        4 => 'None',
        ];
    public static $UNIFORMSETS = [
        0 => 'None',
        1 => 'One Set',
        2 => 'Two Set',
        99 => 'NA',
        ];

    public static function findRecord($studentId, $year = AcademicYear::CURRENT_YEAR, $columns = array('*'))
    {
        return self::where('student_id', '=', $studentId)
            ->where('academic_year', '=', $year)
            ->first($columns);
    }

    public static function updateRecord($datastudentdetails, $studentId, $year = AcademicYear::CURRENT_YEAR)
    {
        return self::where('student_id', '=', $studentId)
            ->where('academic_year', '=', $year)
            ->update($datastudentdetails);
    }


}
