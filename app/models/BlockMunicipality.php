<?php

class BlockMunicipality extends Eloquent {

	protected $fillable = ['BLKCD', 'MUNCD'];
    public $timestamps = false;

}
