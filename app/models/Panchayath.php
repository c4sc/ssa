<?php

class Panchayath extends Eloquent {

	protected $fillable = [];
    protected $table = 'STEPS_PANCHAYAT';
    protected $primaray_key = 'PANCD';

    public static function getList($block)
    {
        $panchayaths = self::where('block_panchayats.BLKCD', '=', $block)
            ->join('block_panchayats', 'STEPS_PANCHAYAT.PANCD', '=', 'block_panchayats.PANCD')
            ->select(DB::raw('CONCAT(STEPS_PANCHAYAT.PANNAME, " - ", STEPS_PANCHAYAT.PANCD) as name'),
            DB::raw('STEPS_PANCHAYAT.PANCD as id'))
            ->lists('name', 'id');

        if(count($panchayaths)) {
            return $panchayaths;
        }
        $district = substr($block, 0, 4);
        return self::where('PANCD', 'LIKE', "{$district}%")->orderBy('PANNAME')
            ->select(DB::raw('CONCAT(STEPS_PANCHAYAT.PANNAME, " - ", STEPS_PANCHAYAT.PANCD) as name'), 'PANCD')
            ->lists('name', 'PANCD');
    }

}
