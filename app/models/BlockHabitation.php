<?php

use Eloquent;

class BlockHabitation extends Eloquent {

	protected $fillable = ['HABCD', 'BLKCD'];
    public $timestamps = false;

}
