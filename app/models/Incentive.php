<?php

class Incentive extends AcademicYear
{

    protected $fillable = [];
    public $table = 'STEPS_INCENTIVES09';
    public $incrementing = false;
    public $timestamps = false;

    public static $TYPE = array(
        1 => '1 - Free text books',
        2 => '2 - Free stationary',
        3 => '3 - Free uniform',
        4 => '4 - Scholarship',
        5 => '5 - Free transport facility',
        6 => '6 - Free residential facility',
    );

    public static $CATEGORY = array(
        'GEN' => 'General Students',
        'SC' => 'SC Students',
        'ST' => 'ST Students',
        'OBC' => 'OBC Students',
        'MIN' => 'Muslim Minority',
    );

    public static function updateIncentive($data, $typeId, $gradeId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('INCENT_TYPE', '=', $typeId)
            ->where('GRADEPRUPR', '=', $gradeId)
            ->update($data);
    }
    /**
     * insert data based on previous year data
     * @param string $schoolCode
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        $groups = array(5, 8, 10, 12);

        // insert null/default values
        foreach($groups as $section) {
            foreach(range(1, 6) as $type) {

                $newItem = new self();
                $newItem->INCENT_TYPE = $type;
                $newItem->GRADEPRUPR = $section;
                $newItem->SCHCD = $schoolCode;
                $newItem->AC_YEAR = self::CURRENT_YEAR;
                $newItem->save();
            }
        }

    }
}
