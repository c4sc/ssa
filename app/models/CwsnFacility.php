<?php

class CwsnFacility extends AcademicYear
{

	protected $fillable = [];
    public $table = 'STEPS_CWSNFACILITY';
    public $incrementing = false;
    public $timestamps = false;

    public static $CWSNFACILITY = array(
        1 => '1 - Brail Books',
        2 => '2 - Brail Kit',
        3 => '3 - Low Vision Kit',
        4 => '4 - Hearing aid',
        5 => '5 - Braces',
        6 => '6 - Crutches',
        7 => '7 - Wheel chair',
        8 => '8 - Tri-cycle',
        9 => '9 - Caliper',
    );

    public static $SECTIONS = array(
        array('' , 'Elementary'),
        array('_SEC' , 'Secondary'),
        array('_HSEC' , 'Higher Secondary')
    );

    public static function updateCwsnFacility($data, $itemId, $schoolCode, $year = self::CURRENT_YEAR) {
        return self::where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', $year)
            ->where('ITEMID', '=', $itemId)
            ->update($data);
    }


    /**
     * insert data based on previous year data
     *
     * @return void
     */
    public static function insertDefault($schoolCode)
    {
        // insert null/default values
        foreach(range(1, 9) as $itemId) {

            $newItem = new self();
            $newItem->ITEMID = $itemId;
            $newItem->SCHCD = $schoolCode;
            $newItem->AC_YEAR = self::CURRENT_YEAR;
            $newItem->save();
        }
    }

}
