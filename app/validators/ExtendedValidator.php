<?php

class ExtendedValidator extends Illuminate\Validation\Validator
{
	/**
	 * Validating the dob provided. checking age is between provided values
     * expecting date format d-m-Y
	 *
	 * @param  string  $attribute
	 * @param  mixed   $value
	 * @param  mixed   $parameters
	 * @return bool
	 */
    public function validateAgeBetween($attribute, $value, $parameters)
    {
		$this->requireParameterCount(2, $parameters, 'age_between');
        $value = Carbon\Carbon::createFromFormat('d-m-Y', $value);
        return ($value->age >= $parameters[0] && $value->age <= $parameters[1]);
    }
    /**
     * [validateOlderThan description]
     * @param  [type] $attribute  [attributes]
     * @param  [type] $value      [Value of current field which the validation takes place]
     * @param  [type] $parameters [The parameters for this validation]
     * @param  [type] $messages   [custom validation failure messages.]
     * @return [Boolean]             [returns true if the difference between current year and dob is greater than 18 years. ]
     */
    public function validateOlderThan($attribute, $value, $parameters, $messages)
    {
        $minAge = ( ! empty($parameters[0])) ? (int) $parameters[0] : 18;
    	return (new DateTime)->diff(new DateTime($value))->y >= $minAge;

    }
    /**
     * [validateDobThan description]
     * @param  [type] $attribute  [attributes]
     * @param  [type] $value      [value of current field]
     * @param  [type] $parameters [parameters]
     * @return [Boolean]             [returnes true if the second parameter, years, satisfies/ $value and first param should gt. or eq. to the second parameter.]
     */
    public function validateDobThan($attribute, $value, $parameters )
    {
        $minAge = ( ! empty($parameters[0])) ? (int) $parameters[0] : date('Y');
        $maxAge = ( ! empty($value)) ? (int) $value : date('Y');
        $minTimePeriod =  ( ! empty($parameters[1])) ? (int) $parameters[1] : 18;
        $maxTimePeriod = 56;
    	return (( $maxAge - $minAge) >= $minTimePeriod && ( $maxAge - $minAge) <= $maxTimePeriod);

    }
	/**
	 * Replace all place-holders for the age_between rule.
	 *
	 * @param  string  $message
	 * @param  string  $attribute
	 * @param  string  $rule
	 * @param  array   $parameters
	 * @return string
	 */
	protected function replaceAgeBetween($message, $attribute, $rule, $parameters)
	{
		return str_replace(array(':min', ':max'), $parameters, $message);
	}
}

