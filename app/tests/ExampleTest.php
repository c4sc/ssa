<?php

class ExampleTest extends TestCase {

    /**
     * @dataProvider schoolCodeProvider
     * @return void
     */
	public function testEnrolmentCheck($schoolCode)
	{
        $master = Master::findRecord($schoolCode);
		$this->assertTrue(Enrolment::isValid($master));
	}

    public function schoolCodeProvider()
    {
        return array(
            array(32030100106),
            array(32120200221),
            array(32030301504),
            array(32111001007),
            array(32030100220),
            array(32030100134),
        );
    }

}
