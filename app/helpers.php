<?php
/**
 * check the variable is set, else print $count times div with specified/default class
 *
 * @param $value mixed
 * @param $count integer
 * @param $class string
 * @return string
 */
function shapeIt($value = null, $count = 0, $class='square')
{
    if( ! isset($value) || $value == null || $value == '') {
        $html = '';
        while($count) {
            $html .= "<div class='pull-left $class'></div>";
            $count--;
        }
        return $html;
    }
    return "<div class='pull-left'>$value</div>";
}

/**
 * securely print choice
 * @return string
 */
function choiceLabel($index, $array)
{
    if($index == '' || $index == 0) {
        return '';
    }
    if(array_key_exists($index, $array)) {
        return $array[$index];
    }
    return $index;
}

/**
 * insert item at array index
 * @param $input array
 * @param $insert mixed
 * @param $index integer
 * @return array
 */
function insertAt($input, $insert, $index)
{
    return array_merge(array_slice($input, 0, $index, true), array($insert), array_slice($input, $index, count($input)-$index, true));
}

/**
 * Check if there is a item in a collection by given key and value
 * @param Illuminate\Support\Collection $collection collection in which search is to be made
 * @param string $key name of key to be checked
 * @param string $value value of key to be checkied
 * @return boolean|object false if not found, object if it is found
 */
function findInCollection($collection, $key, $value) {
    foreach ($collection as $item) {
        if (isset($item->$key) && $item->$key == $value) {
            return $item;
        }
    }
    return false;
}

/**
 * returns neighboring elements of array
 * @see http://php.net/manual/en/function.next.php#42545
 *
 * @param array $arr
 * @param mixed $key
 * @return array
 */
function array_neighbor($arr, $key)
{
    $keys = array_keys($arr);
    $keyIndexes = array_flip($keys);

    $neighbors = array('previous' => null, 'next' => null);
    if ( ! isset($keyIndexes[$key])) {
        return $neighbors;
    }
    if (isset($keys[$keyIndexes[$key]-1])) {
        $neighbors['previous'] = $keys[$keyIndexes[$key]-1];
    }
    if (isset($keys[$keyIndexes[$key]+1])) {
        $neighbors['next'] = $keys[$keyIndexes[$key]+1];
    }
    return $neighbors;
}
