<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Controllers\Admin\SchoolController;

class InitializeSchools extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:initialize-school-data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'initialize school data for current academic-year';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $confirmation = $this->ask('Are you sure to continue with '.AcademicYear::CURRENT_YEAR.' initialization ?');
        if($confirmation != 'yes') {
            return;
        }

        $schools = SchoolDetails::where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)->chunk(100,
            function($schools) {
                foreach($schools as $school) {
                    SchoolController::initialize($school->SCHCD);
                }
            });

        $this->info('Initialization finnished');
	}

}
