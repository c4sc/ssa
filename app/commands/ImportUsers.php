<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportUsers extends Command
{
    /**
     * The console command name.
     *
     * @var	string
     */
    protected $name = 'app:create-users';

    /**
     * The console command description.
     *
     * @var	string
     */
    protected $description = 'create users based on the information from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->comment('-------------------------------------');
        $this->comment('');
        $this->createSchoolUsers();
        $this->createBlockUsers();
        $this->createDistrictUsers();
    }

    /**
     * creat user
     *
     * @return void
     */
    protected function createUser($group, $scope, $scope_id)
    {
        $data = array(
            'email' => $scope_id,
            'password' => $scope_id,
            'activated' => 1,
            'scope_id' => $scope_id,
            'scope'  => $scope,
        );

        try {

            if ($user = Sentry::createUser($data)) {
                $user->addGroup($group);
                $this->info($scope.'-'.$scope_id.' created successfully.');
            }
        } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
            $this->error($scope.'-'.$scope_id.' - user already exists.');
        }

    }

    protected function createSchoolUsers()
    {
        $group = Sentry::findGroupByName('SchoolAdmin');
        $schools = SchoolDetails::where('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)->chunk(100,
            function($schools) use (&$group) {
                foreach($schools as $school) {
                    $this->createUser($group, 'SC', $school->SCHCD);
                }
            });

        $this->info('SchoolUsers import finnished.');
        $this->comment('');
    }

    protected function createBlockUsers()
    {
        $blocks = Block::all();
        $group = Sentry::findGroupByName('BlockAdmin');
        foreach($blocks as $block) {
            $this->createUser($group, 'BL', $block->BLKCD);
        }

        $this->info('BlockUsers import finnished.');
        $this->comment('');
    }

    protected function createDistrictUsers()
    {
        $districts = District::all();
        $group = Sentry::findGroupByName('DistrictAdmin');
        foreach($districts as $district) {
            $this->createUser($group, 'DT', $district->DISTCD);
        }

        $this->info('DistrictUsers import finnished.');
        $this->comment('');
    }

}
