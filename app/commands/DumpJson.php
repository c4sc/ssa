<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DumpJson extends Command
{
    /**
     * The console command name.
     *
     * @var	string
     */
    protected $name = 'app:dump-json';

    /**
     * The console command description.
     *
     * @var	string
     */
    protected $description = 'generate json data files - villages, clusters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $blocks = Block::all();
        foreach($blocks as $block) {
            // dumping clusters to json files
            $clusters = Cluster::where('CLUCD', 'LIKE', "$block->BLKCD%")->get(array('CLUCD', 'CLUNAME'));
            $clusters_json = '{"":"Select Cluster",';
            foreach($clusters as $cluster){

                $clusters_json .= "\"$cluster->CLUCD\":\"$cluster->CLUNAME\",";
            }
            $clusters_json = rtrim($clusters_json, ",");
            $clusters_json .= '}';
            File::put(__DIR__."/../assets/clusters/$block->BLKCD.json", $clusters_json);

            // dumping villages to json files
            $villages = Village::where('VILCD', 'LIKE', "$block->BLKCD%")->get(array('VILCD', 'VILNAME'));
            $villages_json = '{"":"Select Village",';
            foreach($villages as $village){

                $villages_json .= "\"$village->VILCD\":\"$village->VILNAME\",";
            }
            $villages_json = rtrim($villages_json, ",");
            $villages_json .= '}';

            File::put(__DIR__."/../assets/villages/$block->BLKCD.json", $villages_json);

            // dumping Panchayaths to json files
            $panchayaths = Panchayath::where('PANCD', 'LIKE', "$block->BLKCD%")->get(array('PANCD', 'PANNAME'));
            $panchayaths_json = '{"":"Select Panchayath",';
            foreach($panchayaths as $panchayath){

                $panchayaths_json .= "\"$panchayath->PANCD\":\"$panchayath->PANNAME\",";
            }
            $panchayaths_json = rtrim($panchayaths_json, ",");
            $panchayaths_json .= '}';

            File::put(__DIR__."/../assets/panchayaths/$block->BLKCD.json", $panchayaths_json);
        }
        $this->info('json files generated');
    }

}
