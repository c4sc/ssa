<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RemoveSchoolData extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:remove-school-data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'delete school data related to academic-year';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $schoolCode = $this->argument('school');
        $year = $this->option('year');

        School::where('SCHCD', '=', $schoolCode)->update(array('SCHVAR1'=>''));
        Master::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        SchoolDetails::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Material::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Building::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Facility::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Rte::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Mdm::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Smdc::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Enrolment::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        FirstGrade::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        AgeEnrolment::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Repeater::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        StreamEnrolment::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        MediumEnrolment::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Incentive::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Result::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        RmsaFund::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Staff::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Teacher::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        Cwsn::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();
        CwsnFacility::where('SCHCD', '=', $schoolCode)->where('AC_YEAR', '=', $year)->delete();

        $this->info('data removed');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('school', InputArgument::REQUIRED, 'school-code'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('year', 'y', InputOption::VALUE_REQUIRED, 'academic-year', AcademicYear::CURRENT_YEAR),
		);
	}

}
