<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteDuplicatesOfMediumEnrolment extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:delete-medium-duplicates';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deletes the duplicate records of MediumEnrolment model';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        DB::table('STEPS_ENRMEDINSTR')
            ->select(DB::raw('SCHCD, SEQNO, AC_YEAR, MEDIUM, COUNT(*) as count'))
            ->groupBy(array('AC_YEAR', 'MEDIUM', 'SCHCD'))
            ->chunk(100, function($schools){
                foreach($schools as $school) {
                    // check if there is duplicate exists
                    $count = $school->count;
                    if( $count > 1) {
                        print "\n$count ---  ";
                        print "$school->AC_YEAR-$school->MEDIUM-$school->SCHCD-$school->SEQNO";
                        $limit = $count - 1;
                        DB::statement( "DELETE FROM `STEPS_ENRMEDINSTR` WHERE
                            `AC_YEAR` = '$school->AC_YEAR' AND `MEDIUM` ='$school->MEDIUM'
                            AND `SCHCD` ='$school->SCHCD'
                            ORDER BY `SEQNO` DESC LIMIT $limit" );
                    }
                }
            });
	}

}
