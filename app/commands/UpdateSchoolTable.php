<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateSchoolTable extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'app:update-school-table';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'update school table data with STEPS_KEYVAR table';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        $schoolDetails = SchoolDetails::where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->orWhere('AC_YEAR', '=', AcademicYear::PREVIOUS_YEAR)
            ->get();
        foreach ($schoolDetails as $details) {
            try {
                // updating data from SchoolDetails model
                $school = School::findOrFail($details->SCHCD);
                $school->SCHNAME = $details->SCHNAME;
                $school->VILCD = $details->VILCD;
                $school->BLKCD = $details->BLKCD;
                $school->DISTCD = $details->DISTCD;
                $school->save();
            } catch ( Illuminate\Database\Eloquent\ModelNotFoundException $e) {

                // insert new row
                $school = new School;
                $school->SCHCD = $details->SCHCD;
                $school->SCHNAME = $details->SCHNAME;
                $school->VILCD = $details->VILCD;
                $school->BLKCD = $details->BLKCD;
                $school->DISTCD = $details->DISTCD;
                $school->save();

                $this->info('adding school- ' . $details->SCHCD . ' year - ' . $details->AC_YEAR);
            }
        }

        $this->info('data updated');
	}
}

