<?php

return array(

    'does_not_exist' => 'User does not exist.',
    'exists' => 'User already exists!',
    'permission_error'   => 'Sorry, this is not your territory.',

    'create' => array(
        'error'   => 'User was not created, please try again.',
        'success' => 'User created successfully.'
    ),

    'update' => array(
        'error'   => 'User was not updated, please try again',
        'success' => 'User updated successfully.'
    ),

    'delete' => array(
        'confirm'   => 'Are you sure you wish to delete this asset user?',
        'error'   => 'There was an issue deleting the user. Please try again.',
        'success' => 'The user was deleted successfully.'
    )

);


