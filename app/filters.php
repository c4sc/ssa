<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/
Route::filter('school', function($route)
{
	if ( ! Sentry::check()) {
        Session::put('loginredirect', Request::url());
        return Redirect::route('login');
	}

    $user = Sentry::getUser();
    $schoolCode = $route->getParameter('schoolCode');
    try {
        $master = Master::findRecordOrFail($schoolCode, AcademicYear::CURRENT_YEAR,
            array('SCHCD', 'LOWCLASS', 'HIGHCLASS', 'SCHCAT', 'SCHTYPE', 'SCHTYPES', 'SCHTYPEHS',
            'SCHMGT', 'SCHMGTS', 'SCHMGTHS', 'PPSEC_YN'));
    } catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {

        try {
            Controllers\Admin\SchoolController::initialize($schoolCode);
            $master = Master::findRecordOrFail($schoolCode, AcademicYear::CURRENT_YEAR,
                array('SCHCD', 'LOWCLASS', 'HIGHCLASS', 'SCHCAT', 'SCHTYPE', 'SCHTYPES', 'SCHTYPEHS',
                'SCHMGT', 'SCHMGTS', 'SCHMGTHS', 'PPSEC_YN'));
        } catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Response::view('errors.404', array(), 404);
        }
    }

    // check user has right permissions
    if ( ! $user->hasAccessToSchool($master->school)) {
        return Response::view('errors.403', array(), 403);
    }

    App::singleton('master', function() use ($master)
    {
        return $master;
    });
    View::share('master', $master);
});

Route::filter('udise_menu', function($route)
{
    $master = App::make('master');
    $udiseMenu = $master->getUdiseRoutesForMenu();

    //getting next/previous route values
    $neighborRoutes = array_neighbor($udiseMenu, Route::currentRouteName());

    View::share('udiseMenu', $master->getUdiseRoutesForMenu());
    View::share('neighborRoutes', $neighborRoutes);
});


Route::filter('class_data', function($route)
{
    $master = App::make('master');
    if( ! isset($master->LOWCLASS) ||  ! isset($master->HIGHCLASS)) {
        return Redirect::route('particulars-one', $master->SCHCD)->with('error', 'Please update details');
    }
    if($master->LOWCLASS == 0 ||  $master->HIGHCLASS == 0) {
        return Redirect::route('particulars-one', $master->SCHCD)->with('error', 'Please update details');
    }
    if ( ! SchoolStatus::getStatusByRouteName($master->SCHCD, 'particulars-one', SchoolStatus::SAVED)) {
        return Redirect::route('particulars-one', $master->SCHCD)->with('error', 'Please update details');
    }
});

Route::filter('stream', function($route)
{
    $master = App::make('master');
    if($master->hasHigherSecondary() && count(Facility::getStreamList($master->SCHCD)) == 0) {
        return Redirect::route('streams-available', $master->SCHCD)->with('error', 'Please update stream details');
    }
});

Route::filter('school_secondary', function($route)
{
    $master = App::make('master');
    if ( ! $master->hasSecondary()) {
        return Redirect::route('udise-status', $master->SCHCD)
            ->with('error', 'not secondary school.');
    }
});

Route::filter('school_hss', function($route)
{
    $master = App::make('master');
    if ( ! $master->hasHigherSecondary()) {
        return Redirect::route('udise-status', $master->SCHCD)
            ->with('error', 'not higher-secondary school.');
    }
});

Route::filter('school_secondary_or_hss', function($route)
{
    $master = App::make('master');
    if ( ! ($master->hasSecondary() || $master->hasHigherSecondary())) {
        return Redirect::route('udise-status', $master->SCHCD)
            ->with('error', 'not secondary or higher-secondary school.');
    }
});

Route::filter('class_5_8', function($route)
{
    $master = App::make('master');
    if (($master->LOWCLASS <= 5 && $master->HIGHCLASS >= 5) || ($master->LOWCLASS <= 4 && $master->HIGHCLASS >= 4)){
        return;
    }
    if (($master->LOWCLASS <= 7 && $master->HIGHCLASS >= 7) || ($master->LOWCLASS <= 8 && $master->HIGHCLASS >= 8)){
        return;
    }
    return Redirect::route('particulars-one', $master->SCHCD)
        ->with('error', 'you may be attempting to access restricted page.');
});

Route::filter('auth', function()
{
	if ( ! Sentry::check())
	{
        Session::put('loginredirect', Request::url());
        return Redirect::route('login');
	}
});

Route::filter('auth.school', function()
{
	if ( ! Sentry::hasAccess('school'))
	{
        Session::put('loginredirect', Request::url());
        return Redirect::route('login');
	}
});

Route::filter('auth.user', function()
{
	if ( ! Sentry::hasAccess('user'))
	{
        Session::put('loginredirect', Request::url());
        return Redirect::route('login');
	}
});

Route::filter('auth.stateAdmin', function()
{
	if ( ! Sentry::hasAccess('stateAdmin')) {
        Session::put('loginredirect', Request::url());
        return Redirect::route('login');
	}
});

Route::filter('state', function()
{
    $user = Sentry::getUser();
	if ( ! $user->hasAccess('stateAdmin')) {
        return \App::abort(404);
	}
});
Route::filter('district', function($route)
{
    $user = Sentry::getUser();
    $distCode = $route->getParameter('districtCode');
    if ( ! $user->hasAccessToDistrict($distCode, 'districtAdmin')) {
        return \App::abort(404);
    }
});
Route::filter('block', function($route)
{
    $user = Sentry::getUser();
    if ( ! $user->hasAccessToBlock($route->getParameter('blockCode'), 'blockAdmin')) {
        return \App::abort(404);
    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
