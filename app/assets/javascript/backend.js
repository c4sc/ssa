function activate(group, item) {
  $(group).addClass('active')
  $(item).addClass('active')
  $(group+' > ul.treeview-menu').show()
  $(group+' > a').click()
  return 0
}

// checks the total is = to enrolment data
function sumClass(std, gender) {
  total = 0;
  classtotal = 0;
  $('.'+gender+std).each(function() {
    total += (parseInt($(this).val()) || 0);
  });
  e_total = $('.total-'+gender+std)
  e_total.html(total);
  classtotal=(parseInt($('.total-boys-'+std).html()) || 0) + (parseInt($('.total-girls-'+std).html()) || 0);
  $('.classtotal-'+std).html(classtotal);

  $('input[name="total-'+gender+std+'"]').val(total)
  if(e_total.data('sum') != total) {
    e_total.removeClass('success')
    e_total.addClass('danger')
  }
  else {
    e_total.removeClass('danger')
    e_total.addClass('success')
  }
}

// checks the total is <= to enrolment data
function sumClassMax(std, gender) {
  total = 0;
  classtotal = 0;
  $('.'+gender+std).each(function() {
    total += (parseInt($(this).val()) || 0);
  });
  e_total = $('.total-'+gender+std)
  e_total.html(total);

  classtotal=(parseInt($('.total-boys-'+std).html()) || 0) + (parseInt($('.total-girls-'+std).html()) || 0);
  $('.classtotal-'+std).html(classtotal);

  $('input[name="total-'+gender+std+'"]').val(total)
  if(e_total.data('sum') < total) {
    e_total.removeClass('success')
    e_total.addClass('danger')
  }
  else {
    e_total.removeClass('danger')
    e_total.addClass('success')
  }
}
$.listen('parsley:field:error', function(parsleyField) {
    // parsley field
    console.log(parsleyField);

    // which constraint has failed
    console.log(parsleyField.validationResult[0].assert.name);

    // the data-parsley-<constraint>-message
    console.log(parsleyField.options[parsleyField.validationResult[0].assert.name+'Message']);

    // the default constraint fail message
    console.log(window.ParsleyValidator.getErrorMessage(parsleyField.validationResult[0].assert));
});
