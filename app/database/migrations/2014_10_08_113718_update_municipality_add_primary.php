<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateMunicipalityAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_MUNICIPALITY', function(Blueprint $table)
		{
            $table->primary('MUNCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_MUNICIPALITY', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_MUNICIPALITY_MUNCD_primary');
		});
	}

}
