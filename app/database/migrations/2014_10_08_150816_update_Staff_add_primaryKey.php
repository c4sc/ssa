<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateStaffAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_NONTCH', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMIDGROUP', 'DESIG_ID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_NONTCH', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_NONTCH_SCHCD_AC_YEAR_ITEMIDGROUP_DESIG_ID_primary');
		});
	}

}
