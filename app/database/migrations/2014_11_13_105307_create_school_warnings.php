<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolWarnings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school_warnings', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('SCHCD', 11);
            $table->integer('warning_id')->unsigned();
            $table->text('message')->nullable();
			$table->timestamps();
            $table->foreign('warning_id')->references('id')->on('warnings');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_warnings');
	}

}
