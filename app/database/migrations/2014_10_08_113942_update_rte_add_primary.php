<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateRteAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_RTEINFO', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_RTEINFO', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_RTEINFO_SCHCD_AC_YEAR_primary');
		});
	}

}
