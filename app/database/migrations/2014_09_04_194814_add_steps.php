<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSteps extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

                /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
                /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
                /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
                /*!40101 SET NAMES utf8 */;

                --
                    -- Database: `ssa`
                    --

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ATTENDANCE09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ATTENDANCE09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `BLDSTATUS` int(11) DEFAULT NULL,
                        `CLROOMS` int(11) DEFAULT NULL,
                        `CLGOOD` int(11) DEFAULT NULL,
                        `CLMAJOR` int(11) DEFAULT NULL,
                        `CLMINOR` int(11) DEFAULT NULL,
                        `OTHGOOD` int(11) DEFAULT NULL,
                        `OTHMAJOR` int(11) DEFAULT NULL,
                        `OTHMINOR` int(11) DEFAULT NULL,
                        `TOILET_C` int(11) DEFAULT NULL,
                        `TOILET_G` int(11) DEFAULT NULL,
                        `ELECTRIC_YN` int(11) DEFAULT NULL,
                        `BNDRYWALL` int(11) DEFAULT NULL,
                        `BOOKBANK_YN` int(11) DEFAULT NULL,
                        `PGROUND_YN` int(11) DEFAULT NULL,
                        `BLACKBOARD` int(11) DEFAULT NULL,
                        `ALMIRAH` int(11) DEFAULT NULL,
                        `BOX` int(11) DEFAULT NULL,
                        `BOOKINLIB` int(11) DEFAULT NULL,
                        `WATER` int(11) DEFAULT NULL,
                        `MEDCHK_YN` int(11) DEFAULT NULL,
                        `RAMPS_YN` int(11) DEFAULT NULL,
                        `COMPUTER` int(11) DEFAULT NULL,
                        `BLDPUCCA` int(11) DEFAULT NULL,
                        `BLDPPUCCA` int(11) DEFAULT NULL,
                        `BLDKUCCHA` int(11) DEFAULT NULL,
                        `BLDTENT` int(11) DEFAULT NULL,
                        `CLSPUCCA` int(11) DEFAULT NULL,
                        `CLSPPUCCA` int(11) DEFAULT NULL,
                        `CLSKUCCHA` int(11) DEFAULT NULL,
                        `CLSTENT` int(11) DEFAULT NULL,
                        `ROOMSPUCCA` int(11) DEFAULT NULL,
                        `ROOMSPPUCCA` int(11) DEFAULT NULL,
                        `ROOMSKUCCHA` int(11) DEFAULT NULL,
                        `ROOMSTENT` int(11) DEFAULT NULL,
                        `ANANDCLASS` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `FURNTCH` int(11) DEFAULT NULL,
                        `FURNSTU` int(11) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL,
                        `BLDCOND` int(11) DEFAULT NULL,
                        `TOTCLS9` int(11) DEFAULT NULL,
                        `TOTCLS10` int(11) DEFAULT NULL,
                        `TOTCLS11` int(11) DEFAULT NULL,
                        `TOTCLS12` int(11) DEFAULT NULL,
                        `TOTOTH9` int(11) DEFAULT NULL,
                        `TOTOTH10` int(11) DEFAULT NULL,
                        `TOTOTH11` int(11) DEFAULT NULL,
                        `TOTOTH12` int(11) DEFAULT NULL,
                        `CLSUCONST9` int(11) DEFAULT NULL,
                        `CLSUCONST10` int(11) DEFAULT NULL,
                        `CLSUCONST11` int(11) DEFAULT NULL,
                        `CLSUCONST12` int(11) DEFAULT NULL,
                        `BBOARD9` int(11) DEFAULT NULL,
                        `BBOARD10` int(11) DEFAULT NULL,
                        `BBOARD11` int(11) DEFAULT NULL,
                        `BBOARD12` int(11) DEFAULT NULL,
                        `FURN_YN9` int(11) DEFAULT NULL,
                        `FURN_YN10` int(11) DEFAULT NULL,
                        `FURN_YN11` int(11) DEFAULT NULL,
                        `FURN_YN12` int(11) DEFAULT NULL,
                        `CLGOODHS` int(11) DEFAULT NULL,
                        `CLMINORHS` int(11) DEFAULT NULL,
                        `CLMAJORHS` int(11) DEFAULT NULL,
                        `OTHGOODHS` int(11) DEFAULT NULL,
                        `OTHMINORHS` int(11) DEFAULT NULL,
                        `OTHMAJORHS` int(11) DEFAULT NULL,
                        `TOILETWATER_B` int(11) DEFAULT NULL,
                        `TOILETWATER_G` int(11) DEFAULT NULL,
                        `URINALS_B` int(11) DEFAULT NULL,
                        `URINALS_G` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `LIBRARIAN_YN` int(11) DEFAULT NULL,
                        `COMPSEC` int(11) DEFAULT NULL,
                        `COMPSEC_FUNC` int(11) DEFAULT NULL,
                        `COMPHS` int(11) DEFAULT NULL,
                        `COMPHS_FUNC` int(11) DEFAULT NULL,
                        `RAMPSNEEDED_YN` int(11) DEFAULT NULL,
                        `HOSTELB_YN` int(11) DEFAULT NULL,
                        `HOSTELBOYS` int(11) DEFAULT NULL,
                        `HOSTELG_YN` int(11) DEFAULT NULL,
                        `HOSTELGIRLS` int(11) DEFAULT NULL,
                        `OTHROOMS` int(11) DEFAULT NULL,
                        `COMP_FUNC` int(11) DEFAULT NULL,
                        `CLGOODS` int(11) DEFAULT NULL,
                        `CLMINORS` int(11) DEFAULT NULL,
                        `CLMAJORS` int(11) DEFAULT NULL,
                        `OTHGOODS` int(11) DEFAULT NULL,
                        `OTHMINORS` int(11) DEFAULT NULL,
                        `OTHMAJORS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDSTATUS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDSTATUS` (
                        `BLDSTATUS_ID` int(11) DEFAULT NULL,
                        `BLDSTATUS_DESC` varchar(75) DEFAULT NULL,
                        `BLDSTATUS_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `TRIBAL_YN` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `BLKVAR1` varchar(35) DEFAULT NULL,
                        `BLKVAR2` varchar(35) DEFAULT NULL,
                        `BLKVAR3` varchar(35) DEFAULT NULL,
                        `BLKVAR4` varchar(35) DEFAULT NULL,
                        `BLKVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BNDRYWALL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BNDRYWALL` (
                        `BNDRYWALL_ID` int(11) DEFAULT NULL,
                        `BNDRYWALL_DESC` varchar(75) DEFAULT NULL,
                        `BNDRYWALL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CASTE` (
                        `CASTE_ID` int(11) DEFAULT NULL,
                        `CASTE_DESC` varchar(75) DEFAULT NULL,
                        `CASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CITY` (
                        `CITYCD` varchar(7) DEFAULT NULL,
                        `CITYNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CITYVAR1` varchar(35) DEFAULT NULL,
                        `CITYVAR2` varchar(35) DEFAULT NULL,
                        `CITYVAR3` varchar(35) DEFAULT NULL,
                        `CITYVAR4` varchar(35) DEFAULT NULL,
                        `CITYVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CLUSTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CLUSTER` (
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `CLUNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `CLUVAR1` varchar(35) DEFAULT NULL,
                        `CLUVAR2` varchar(35) DEFAULT NULL,
                        `CLUVAR3` varchar(35) DEFAULT NULL,
                        `CLUVAR4` varchar(35) DEFAULT NULL,
                        `CLUVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CONSTITUENCY` (
                        `CONSTCD` varchar(7) DEFAULT NULL,
                        `CONSTNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITYID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNFACILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNFACILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `TOT_B_SEC` int(11) DEFAULT NULL,
                        `TOT_G_SEC` int(11) DEFAULT NULL,
                        `TOT_B_HSEC` int(11) DEFAULT NULL,
                        `TOT_G_HSEC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CYCLE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CYCLE` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_BLINDB` int(11) DEFAULT NULL,
                        `C1_BLINDG` int(11) DEFAULT NULL,
                        `C1_DEAFB` int(11) DEFAULT NULL,
                        `C1_DEAFG` int(11) DEFAULT NULL,
                        `C1_ORTHOB` int(11) DEFAULT NULL,
                        `C1_ORTHOG` int(11) DEFAULT NULL,
                        `C1_MENTALB` int(11) DEFAULT NULL,
                        `C1_MENTALG` int(11) DEFAULT NULL,
                        `C1_OTHB` int(11) DEFAULT NULL,
                        `C1_OTHG` int(11) DEFAULT NULL,
                        `C2_BLINDB` int(11) DEFAULT NULL,
                        `C2_BLINDG` int(11) DEFAULT NULL,
                        `C2_DEAFB` int(11) DEFAULT NULL,
                        `C2_DEAFG` int(11) DEFAULT NULL,
                        `C2_ORTHOB` int(11) DEFAULT NULL,
                        `C2_ORTHOG` int(11) DEFAULT NULL,
                        `C2_MENTALB` int(11) DEFAULT NULL,
                        `C2_MENTALG` int(11) DEFAULT NULL,
                        `C2_OTHB` int(11) DEFAULT NULL,
                        `C2_OTHG` int(11) DEFAULT NULL,
                        `C3_BLINDB` int(11) DEFAULT NULL,
                        `C3_BLINDG` int(11) DEFAULT NULL,
                        `C3_DEAFB` int(11) DEFAULT NULL,
                        `C3_DEAFG` int(11) DEFAULT NULL,
                        `C3_ORTHOB` int(11) DEFAULT NULL,
                        `C3_ORTHOG` int(11) DEFAULT NULL,
                        `C3_MENTALB` int(11) DEFAULT NULL,
                        `C3_MENTALG` int(11) DEFAULT NULL,
                        `C3_OTHB` int(11) DEFAULT NULL,
                        `C3_OTHG` int(11) DEFAULT NULL,
                        `C4_BLINDB` int(11) DEFAULT NULL,
                        `C4_BLINDG` int(11) DEFAULT NULL,
                        `C4_DEAFB` int(11) DEFAULT NULL,
                        `C4_DEAFG` int(11) DEFAULT NULL,
                        `C4_ORTHOB` int(11) DEFAULT NULL,
                        `C4_ORTHOG` int(11) DEFAULT NULL,
                        `C4_MENTALB` int(11) DEFAULT NULL,
                        `C4_MENTALG` int(11) DEFAULT NULL,
                        `C4_OTHB` int(11) DEFAULT NULL,
                        `C4_OTHG` int(11) DEFAULT NULL,
                        `C5_BLINDB` int(11) DEFAULT NULL,
                        `C5_BLINDG` int(11) DEFAULT NULL,
                        `C5_DEAFB` int(11) DEFAULT NULL,
                        `C5_DEAFG` int(11) DEFAULT NULL,
                        `C5_ORTHOB` int(11) DEFAULT NULL,
                        `C5_ORTHOG` int(11) DEFAULT NULL,
                        `C5_MENTALB` int(11) DEFAULT NULL,
                        `C5_MENTALG` int(11) DEFAULT NULL,
                        `C5_OTHB` int(11) DEFAULT NULL,
                        `C5_OTHG` int(11) DEFAULT NULL,
                        `C6_BLINDB` int(11) DEFAULT NULL,
                        `C6_BLINDG` int(11) DEFAULT NULL,
                        `C6_DEAFB` int(11) DEFAULT NULL,
                        `C6_DEAFG` int(11) DEFAULT NULL,
                        `C6_ORTHOB` int(11) DEFAULT NULL,
                        `C6_ORTHOG` int(11) DEFAULT NULL,
                        `C6_MENTALB` int(11) DEFAULT NULL,
                        `C6_MENTALG` int(11) DEFAULT NULL,
                        `C6_OTHB` int(11) DEFAULT NULL,
                        `C6_OTHG` int(11) DEFAULT NULL,
                        `C7_BLINDB` int(11) DEFAULT NULL,
                        `C7_BLINDG` int(11) DEFAULT NULL,
                        `C7_DEAFB` int(11) DEFAULT NULL,
                        `C7_DEAFG` int(11) DEFAULT NULL,
                        `C7_ORTHOB` int(11) DEFAULT NULL,
                        `C7_ORTHOG` int(11) DEFAULT NULL,
                        `C7_MENTALB` int(11) DEFAULT NULL,
                        `C7_MENTALG` int(11) DEFAULT NULL,
                        `C7_OTHB` int(11) DEFAULT NULL,
                        `C7_OTHG` int(11) DEFAULT NULL,
                        `C8_BLINDB` int(11) DEFAULT NULL,
                        `C8_BLINDG` int(11) DEFAULT NULL,
                        `C8_DEAFB` int(11) DEFAULT NULL,
                        `C8_DEAFG` int(11) DEFAULT NULL,
                        `C8_ORTHOB` int(11) DEFAULT NULL,
                        `C8_ORTHOG` int(11) DEFAULT NULL,
                        `C8_MENTALB` int(11) DEFAULT NULL,
                        `C8_MENTALG` int(11) DEFAULT NULL,
                        `C8_OTHB` int(11) DEFAULT NULL,
                        `C8_OTHG` int(11) DEFAULT NULL,
                        `C1_DUMBB` int(11) DEFAULT NULL,
                        `C1_DUMBG` int(11) DEFAULT NULL,
                        `C2_DUMBB` int(11) DEFAULT NULL,
                        `C2_DUMBG` int(11) DEFAULT NULL,
                        `C3_DUMBB` int(11) DEFAULT NULL,
                        `C3_DUMBG` int(11) DEFAULT NULL,
                        `C4_DUMBB` int(11) DEFAULT NULL,
                        `C4_DUMBG` int(11) DEFAULT NULL,
                        `C5_DUMBB` int(11) DEFAULT NULL,
                        `C5_DUMBG` int(11) DEFAULT NULL,
                        `C6_DUMBB` int(11) DEFAULT NULL,
                        `C6_DUMBG` int(11) DEFAULT NULL,
                        `C7_DUMBB` int(11) DEFAULT NULL,
                        `C7_DUMBG` int(11) DEFAULT NULL,
                        `C8_DUMBB` int(11) DEFAULT NULL,
                        `C8_DUMBG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1MULTIB` int(11) DEFAULT NULL,
                        `C1MULTIG` int(11) DEFAULT NULL,
                        `C2MULTIB` int(11) DEFAULT NULL,
                        `C2MULTIG` int(11) DEFAULT NULL,
                        `C3MULTIB` int(11) DEFAULT NULL,
                        `C3MULTIG` int(11) DEFAULT NULL,
                        `C4MULTIB` int(11) DEFAULT NULL,
                        `C4MULTIG` int(11) DEFAULT NULL,
                        `C5MULTIB` int(11) DEFAULT NULL,
                        `C5MULTIG` int(11) DEFAULT NULL,
                        `C6MULTIB` int(11) DEFAULT NULL,
                        `C6MULTIG` int(11) DEFAULT NULL,
                        `C7MULTIB` int(11) DEFAULT NULL,
                        `C7MULTIG` int(11) DEFAULT NULL,
                        `C8MULTIB` int(11) DEFAULT NULL,
                        `C8MULTIG` int(11) DEFAULT NULL,
                        `C9MULTIB` int(11) DEFAULT NULL,
                        `C9MULTIG` int(11) DEFAULT NULL,
                        `C10MULTIB` int(11) DEFAULT NULL,
                        `C10MULTIG` int(11) DEFAULT NULL,
                        `C11MULTIB` int(11) DEFAULT NULL,
                        `C11MULTIG` int(11) DEFAULT NULL,
                        `C12MULTIB` int(11) DEFAULT NULL,
                        `C12MULTIG` int(11) DEFAULT NULL,
                        `C9BLINDB` int(11) DEFAULT NULL,
                        `C9BLINDG` int(11) DEFAULT NULL,
                        `C9DEAFB` int(11) DEFAULT NULL,
                        `C9DEAFG` int(11) DEFAULT NULL,
                        `C9DUMBB` int(11) DEFAULT NULL,
                        `C9DUMBG` int(11) DEFAULT NULL,
                        `C9ORTHOB` int(11) DEFAULT NULL,
                        `C9ORTHOG` int(11) DEFAULT NULL,
                        `C9MENTALB` int(11) DEFAULT NULL,
                        `C9MENTALG` int(11) DEFAULT NULL,
                        `C9OTHB` int(11) DEFAULT NULL,
                        `C9OTHG` int(11) DEFAULT NULL,
                        `C10BLINDB` int(11) DEFAULT NULL,
                        `C10BLINDG` int(11) DEFAULT NULL,
                        `C10DEAFB` int(11) DEFAULT NULL,
                        `C10DEAFG` int(11) DEFAULT NULL,
                        `C10DUMBB` int(11) DEFAULT NULL,
                        `C10DUMBG` int(11) DEFAULT NULL,
                        `C10ORTHOB` int(11) DEFAULT NULL,
                        `C10ORTHOG` int(11) DEFAULT NULL,
                        `C10MENTALB` int(11) DEFAULT NULL,
                        `C10MENTALG` int(11) DEFAULT NULL,
                        `C10OTHB` int(11) DEFAULT NULL,
                        `C10OTHG` int(11) DEFAULT NULL,
                        `C11BLINDB` int(11) DEFAULT NULL,
                        `C11BLINDG` int(11) DEFAULT NULL,
                        `C11DEAFB` int(11) DEFAULT NULL,
                        `C11DEAFG` int(11) DEFAULT NULL,
                        `C11DUMBB` int(11) DEFAULT NULL,
                        `C11DUMBG` int(11) DEFAULT NULL,
                        `C11ORTHOB` int(11) DEFAULT NULL,
                        `C11ORTHOG` int(11) DEFAULT NULL,
                        `C11MENTALB` int(11) DEFAULT NULL,
                        `C11MENTALG` int(11) DEFAULT NULL,
                        `C11OTHB` int(11) DEFAULT NULL,
                        `C11OTHG` int(11) DEFAULT NULL,
                        `C12BLINDB` int(11) DEFAULT NULL,
                        `C12BLINDG` int(11) DEFAULT NULL,
                        `C12DEAFB` int(11) DEFAULT NULL,
                        `C12DEAFG` int(11) DEFAULT NULL,
                        `C12DUMBB` int(11) DEFAULT NULL,
                        `C12DUMBG` int(11) DEFAULT NULL,
                        `C12ORTHOB` int(11) DEFAULT NULL,
                        `C12ORTHOG` int(11) DEFAULT NULL,
                        `C12MENTALB` int(11) DEFAULT NULL,
                        `C12MENTALG` int(11) DEFAULT NULL,
                        `C12OTHB` int(11) DEFAULT NULL,
                        `C12OTHG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTLIST` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTRICT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTRICT` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `DISTVAR1` varchar(35) DEFAULT NULL,
                        `DISTVAR2` varchar(35) DEFAULT NULL,
                        `DISTVAR3` varchar(35) DEFAULT NULL,
                        `DISTVAR4` varchar(35) DEFAULT NULL,
                        `DISTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EDUBLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EDUBLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `BLK_TRIB` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRAGEBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRAGEBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGEID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRBYCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRBYCASTEMEDIUM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUMID` int(11) DEFAULT NULL,
                        `CASTEIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `LKG_B` int(11) DEFAULT NULL,
                        `LKG_G` int(11) DEFAULT NULL,
                        `UKG_B` int(11) DEFAULT NULL,
                        `UKG_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(6) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C1GB` int(11) DEFAULT NULL,
                        `C1GG` int(11) DEFAULT NULL,
                        `C2GB` int(11) DEFAULT NULL,
                        `C2GG` int(11) DEFAULT NULL,
                        `C3GB` int(11) DEFAULT NULL,
                        `C3GG` int(11) DEFAULT NULL,
                        `C4GB` int(11) DEFAULT NULL,
                        `C4GG` int(11) DEFAULT NULL,
                        `C5GB` int(11) DEFAULT NULL,
                        `C5GG` int(11) DEFAULT NULL,
                        `C6GB` int(11) DEFAULT NULL,
                        `C6GG` int(11) DEFAULT NULL,
                        `C7GB` int(11) DEFAULT NULL,
                        `C7GG` int(11) DEFAULT NULL,
                        `C8GB` int(11) DEFAULT NULL,
                        `C8GG` int(11) DEFAULT NULL,
                        `C1CB` int(11) DEFAULT NULL,
                        `C1CG` int(11) DEFAULT NULL,
                        `C2CB` int(11) DEFAULT NULL,
                        `C2CG` int(11) DEFAULT NULL,
                        `C3CB` int(11) DEFAULT NULL,
                        `C3CG` int(11) DEFAULT NULL,
                        `C4CB` int(11) DEFAULT NULL,
                        `C4CG` int(11) DEFAULT NULL,
                        `C5CB` int(11) DEFAULT NULL,
                        `C5CG` int(11) DEFAULT NULL,
                        `C6CB` int(11) DEFAULT NULL,
                        `C6CG` int(11) DEFAULT NULL,
                        `C7CB` int(11) DEFAULT NULL,
                        `C7CG` int(11) DEFAULT NULL,
                        `C8CB` int(11) DEFAULT NULL,
                        `C8CG` int(11) DEFAULT NULL,
                        `C1TB` int(11) DEFAULT NULL,
                        `C1TG` int(11) DEFAULT NULL,
                        `C2TB` int(11) DEFAULT NULL,
                        `C2TG` int(11) DEFAULT NULL,
                        `C3TB` int(11) DEFAULT NULL,
                        `C3TG` int(11) DEFAULT NULL,
                        `C4TB` int(11) DEFAULT NULL,
                        `C4TG` int(11) DEFAULT NULL,
                        `C5TB` int(11) DEFAULT NULL,
                        `C5TG` int(11) DEFAULT NULL,
                        `C6TB` int(11) DEFAULT NULL,
                        `C6TG` int(11) DEFAULT NULL,
                        `C7TB` int(11) DEFAULT NULL,
                        `C7TG` int(11) DEFAULT NULL,
                        `C8TB` int(11) DEFAULT NULL,
                        `C8TG` int(11) DEFAULT NULL,
                        `C1OB` int(11) DEFAULT NULL,
                        `C1OG` int(11) DEFAULT NULL,
                        `C2OB` int(11) DEFAULT NULL,
                        `C2OG` int(11) DEFAULT NULL,
                        `C3OB` int(11) DEFAULT NULL,
                        `C3OG` int(11) DEFAULT NULL,
                        `C4OB` int(11) DEFAULT NULL,
                        `C4OG` int(11) DEFAULT NULL,
                        `C5OB` int(11) DEFAULT NULL,
                        `C5OG` int(11) DEFAULT NULL,
                        `C6OB` int(11) DEFAULT NULL,
                        `C6OG` int(11) DEFAULT NULL,
                        `C7OB` int(11) DEFAULT NULL,
                        `C7OG` int(11) DEFAULT NULL,
                        `C8OB` int(11) DEFAULT NULL,
                        `C8OG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `C1_TOTB` int(11) DEFAULT NULL,
                        `C1_TOTG` int(11) DEFAULT NULL,
                        `C2_TOTB` int(11) DEFAULT NULL,
                        `C2_TOTG` int(11) DEFAULT NULL,
                        `C3_TOTB` int(11) DEFAULT NULL,
                        `C3_TOTG` int(11) DEFAULT NULL,
                        `C4_TOTB` int(11) DEFAULT NULL,
                        `C4_TOTG` int(11) DEFAULT NULL,
                        `C5_TOTB` int(11) DEFAULT NULL,
                        `C5_TOTG` int(11) DEFAULT NULL,
                        `C6_TOTB` int(11) DEFAULT NULL,
                        `C6_TOTG` int(11) DEFAULT NULL,
                        `C7_TOTB` int(11) DEFAULT NULL,
                        `C7_TOTG` int(11) DEFAULT NULL,
                        `C8_TOTB` int(11) DEFAULT NULL,
                        `C8_TOTG` int(11) DEFAULT NULL,
                        `CPP_TOTB` int(11) DEFAULT NULL,
                        `CPP_TOTG` int(11) DEFAULT NULL,
                        `C9_TOTB` int(11) DEFAULT NULL,
                        `C9_TOTG` int(11) DEFAULT NULL,
                        `C10_TOTB` int(11) DEFAULT NULL,
                        `C10_TOTG` int(11) DEFAULT NULL,
                        `C11_TOTB` int(11) DEFAULT NULL,
                        `C11_TOTG` int(11) DEFAULT NULL,
                        `C12_TOTB` int(11) DEFAULT NULL,
                        `C12_TOTG` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_L5B` int(11) DEFAULT NULL,
                        `C1_5B` int(11) DEFAULT NULL,
                        `C1_6B` int(11) DEFAULT NULL,
                        `C1_7B` int(11) DEFAULT NULL,
                        `C1_8B` int(11) DEFAULT NULL,
                        `C1_9B` int(11) DEFAULT NULL,
                        `C1_10B` int(11) DEFAULT NULL,
                        `C1_11B` int(11) DEFAULT NULL,
                        `C1_12B` int(11) DEFAULT NULL,
                        `C1_L5G` int(11) DEFAULT NULL,
                        `C1_5G` int(11) DEFAULT NULL,
                        `C1_6G` int(11) DEFAULT NULL,
                        `C1_7G` int(11) DEFAULT NULL,
                        `C1_8G` int(11) DEFAULT NULL,
                        `C1_9G` int(11) DEFAULT NULL,
                        `C1_10G` int(11) DEFAULT NULL,
                        `C1_11G` int(11) DEFAULT NULL,
                        `C1_12G` int(11) DEFAULT NULL,
                        `C2_5B` int(11) DEFAULT NULL,
                        `C2_6B` int(11) DEFAULT NULL,
                        `C2_7B` int(11) DEFAULT NULL,
                        `C2_8B` int(11) DEFAULT NULL,
                        `C2_9B` int(11) DEFAULT NULL,
                        `C2_10B` int(11) DEFAULT NULL,
                        `C2_11B` int(11) DEFAULT NULL,
                        `C2_12B` int(11) DEFAULT NULL,
                        `C2_13B` int(11) DEFAULT NULL,
                        `C2_5G` int(11) DEFAULT NULL,
                        `C2_6G` int(11) DEFAULT NULL,
                        `C2_7G` int(11) DEFAULT NULL,
                        `C2_8G` int(11) DEFAULT NULL,
                        `C2_9G` int(11) DEFAULT NULL,
                        `C2_10G` int(11) DEFAULT NULL,
                        `C2_11G` int(11) DEFAULT NULL,
                        `C2_12G` int(11) DEFAULT NULL,
                        `C2_13G` int(11) DEFAULT NULL,
                        `C3_7B` int(11) DEFAULT NULL,
                        `C3_8B` int(11) DEFAULT NULL,
                        `C3_9B` int(11) DEFAULT NULL,
                        `C3_10B` int(11) DEFAULT NULL,
                        `C3_11B` int(11) DEFAULT NULL,
                        `C3_12B` int(11) DEFAULT NULL,
                        `C3_13B` int(11) DEFAULT NULL,
                        `C3_14B` int(11) DEFAULT NULL,
                        `C3_15B` int(11) DEFAULT NULL,
                        `C3_16B` int(11) DEFAULT NULL,
                        `C3_M16B` int(11) DEFAULT NULL,
                        `C3_7G` int(11) DEFAULT NULL,
                        `C3_8G` int(11) DEFAULT NULL,
                        `C3_9G` int(11) DEFAULT NULL,
                        `C3_10G` int(11) DEFAULT NULL,
                        `C3_11G` int(11) DEFAULT NULL,
                        `C3_12G` int(11) DEFAULT NULL,
                        `C3_13G` int(11) DEFAULT NULL,
                        `C3_14G` int(11) DEFAULT NULL,
                        `C3_15G` int(11) DEFAULT NULL,
                        `C3_16G` int(11) DEFAULT NULL,
                        `C3_M16G` int(11) DEFAULT NULL,
                        `C4_8B` int(11) DEFAULT NULL,
                        `C4_9B` int(11) DEFAULT NULL,
                        `C4_10B` int(11) DEFAULT NULL,
                        `C4_11B` int(11) DEFAULT NULL,
                        `C4_12B` int(11) DEFAULT NULL,
                        `C4_13B` int(11) DEFAULT NULL,
                        `C4_14B` int(11) DEFAULT NULL,
                        `C4_15B` int(11) DEFAULT NULL,
                        `C4_16B` int(11) DEFAULT NULL,
                        `C4_M16B` int(11) DEFAULT NULL,
                        `C4_8G` int(11) DEFAULT NULL,
                        `C4_9G` int(11) DEFAULT NULL,
                        `C4_10G` int(11) DEFAULT NULL,
                        `C4_11G` int(11) DEFAULT NULL,
                        `C4_12G` int(11) DEFAULT NULL,
                        `C4_13G` int(11) DEFAULT NULL,
                        `C4_14G` int(11) DEFAULT NULL,
                        `C4_15G` int(11) DEFAULT NULL,
                        `C4_16G` int(11) DEFAULT NULL,
                        `C4_M16G` int(11) DEFAULT NULL,
                        `C5_9B` int(11) DEFAULT NULL,
                        `C5_10B` int(11) DEFAULT NULL,
                        `C5_11B` int(11) DEFAULT NULL,
                        `C5_12B` int(11) DEFAULT NULL,
                        `C5_13B` int(11) DEFAULT NULL,
                        `C5_14B` int(11) DEFAULT NULL,
                        `C5_15B` int(11) DEFAULT NULL,
                        `C5_16B` int(11) DEFAULT NULL,
                        `C5_M16B` int(11) DEFAULT NULL,
                        `C5_9G` int(11) DEFAULT NULL,
                        `C5_10G` int(11) DEFAULT NULL,
                        `C5_11G` int(11) DEFAULT NULL,
                        `C5_12G` int(11) DEFAULT NULL,
                        `C5_13G` int(11) DEFAULT NULL,
                        `C5_14G` int(11) DEFAULT NULL,
                        `C5_15G` int(11) DEFAULT NULL,
                        `C5_16G` int(11) DEFAULT NULL,
                        `C5_M16G` int(11) DEFAULT NULL,
                        `C6_10B` int(11) DEFAULT NULL,
                        `C6_11B` int(11) DEFAULT NULL,
                        `C6_12B` int(11) DEFAULT NULL,
                        `C6_13B` int(11) DEFAULT NULL,
                        `C6_14B` int(11) DEFAULT NULL,
                        `C6_15B` int(11) DEFAULT NULL,
                        `C6_16B` int(11) DEFAULT NULL,
                        `C6_M16B` int(11) DEFAULT NULL,
                        `C6_10G` int(11) DEFAULT NULL,
                        `C6_11G` int(11) DEFAULT NULL,
                        `C6_12G` int(11) DEFAULT NULL,
                        `C6_13G` int(11) DEFAULT NULL,
                        `C6_14G` int(11) DEFAULT NULL,
                        `C6_15G` int(11) DEFAULT NULL,
                        `C6_16G` int(11) DEFAULT NULL,
                        `C6_M16G` int(11) DEFAULT NULL,
                        `C7_11B` int(11) DEFAULT NULL,
                        `C7_12B` int(11) DEFAULT NULL,
                        `C7_13B` int(11) DEFAULT NULL,
                        `C7_14B` int(11) DEFAULT NULL,
                        `C7_15B` int(11) DEFAULT NULL,
                        `C7_16B` int(11) DEFAULT NULL,
                        `C7_M16B` int(11) DEFAULT NULL,
                        `C7_11G` int(11) DEFAULT NULL,
                        `C7_12G` int(11) DEFAULT NULL,
                        `C7_13G` int(11) DEFAULT NULL,
                        `C7_14G` int(11) DEFAULT NULL,
                        `C7_15G` int(11) DEFAULT NULL,
                        `C7_16G` int(11) DEFAULT NULL,
                        `C7_M16G` int(11) DEFAULT NULL,
                        `C8_12B` int(11) DEFAULT NULL,
                        `C8_13B` int(11) DEFAULT NULL,
                        `C8_14B` int(11) DEFAULT NULL,
                        `C8_15B` int(11) DEFAULT NULL,
                        `C8_16B` int(11) DEFAULT NULL,
                        `C8_M16B` int(11) DEFAULT NULL,
                        `C8_12G` int(11) DEFAULT NULL,
                        `C8_13G` int(11) DEFAULT NULL,
                        `C8_14G` int(11) DEFAULT NULL,
                        `C8_15G` int(11) DEFAULT NULL,
                        `C8_16G` int(11) DEFAULT NULL,
                        `C8_M16G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PPB_L5` int(11) DEFAULT NULL,
                        `PPG_L5` int(11) DEFAULT NULL,
                        `PPB_5` int(11) DEFAULT NULL,
                        `PPG_5` int(11) DEFAULT NULL,
                        `PPB_6` int(11) DEFAULT NULL,
                        `PPG_6` int(11) DEFAULT NULL,
                        `PPB_7` int(11) DEFAULT NULL,
                        `PPG_7` int(11) DEFAULT NULL,
                        `PPB_8` int(11) DEFAULT NULL,
                        `PPG_8` int(11) DEFAULT NULL,
                        `PPB_9` int(11) DEFAULT NULL,
                        `PPG_9` int(11) DEFAULT NULL,
                        `PPB_10` int(11) DEFAULT NULL,
                        `PPG_10` int(11) DEFAULT NULL,
                        `PPB_11` int(11) DEFAULT NULL,
                        `PPG_11` int(11) DEFAULT NULL,
                        `PPB_12` int(11) DEFAULT NULL,
                        `PPG_12` int(11) DEFAULT NULL,
                        `C9B_13` int(11) DEFAULT NULL,
                        `C9G_13` int(11) DEFAULT NULL,
                        `C9B_14` int(11) DEFAULT NULL,
                        `C9G_14` int(11) DEFAULT NULL,
                        `C9B_M14` int(11) DEFAULT NULL,
                        `C9G_M14` int(11) DEFAULT NULL,
                        `C9B_16` int(11) DEFAULT NULL,
                        `C9G_16` int(11) DEFAULT NULL,
                        `C9B_M16` int(11) DEFAULT NULL,
                        `C9G_M16` int(11) DEFAULT NULL,
                        `C10B_14` int(11) DEFAULT NULL,
                        `C10G_14` int(11) DEFAULT NULL,
                        `C10B_M14` int(11) DEFAULT NULL,
                        `C10G_M14` int(11) DEFAULT NULL,
                        `C10B_16` int(11) DEFAULT NULL,
                        `C10G_16` int(11) DEFAULT NULL,
                        `C10B_M16` int(11) DEFAULT NULL,
                        `C10G_M16` int(11) DEFAULT NULL,
                        `C11B_M14` int(11) DEFAULT NULL,
                        `C11G_M14` int(11) DEFAULT NULL,
                        `C11B_16` int(11) DEFAULT NULL,
                        `C11G_16` int(11) DEFAULT NULL,
                        `C11B_M16` int(11) DEFAULT NULL,
                        `C11G_M16` int(11) DEFAULT NULL,
                        `C12B_16` int(11) DEFAULT NULL,
                        `C12G_16` int(11) DEFAULT NULL,
                        `C12B_M16` int(11) DEFAULT NULL,
                        `C12G_M16` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C1_GB` int(11) DEFAULT NULL,
                        `C1_GG` int(11) DEFAULT NULL,
                        `C2_GB` int(11) DEFAULT NULL,
                        `C2_GG` int(11) DEFAULT NULL,
                        `C3_GB` int(11) DEFAULT NULL,
                        `C3_GG` int(11) DEFAULT NULL,
                        `C4_GB` int(11) DEFAULT NULL,
                        `C4_GG` int(11) DEFAULT NULL,
                        `C5_GB` int(11) DEFAULT NULL,
                        `C5_GG` int(11) DEFAULT NULL,
                        `C6_GB` int(11) DEFAULT NULL,
                        `C6_GG` int(11) DEFAULT NULL,
                        `C7_GB` int(11) DEFAULT NULL,
                        `C7_GG` int(11) DEFAULT NULL,
                        `C8_GB` int(11) DEFAULT NULL,
                        `C8_GG` int(11) DEFAULT NULL,
                        `C1_CB` int(11) DEFAULT NULL,
                        `C1_CG` int(11) DEFAULT NULL,
                        `C2_CB` int(11) DEFAULT NULL,
                        `C2_CG` int(11) DEFAULT NULL,
                        `C3_CB` int(11) DEFAULT NULL,
                        `C3_CG` int(11) DEFAULT NULL,
                        `C4_CB` int(11) DEFAULT NULL,
                        `C4_CG` int(11) DEFAULT NULL,
                        `C5_CB` int(11) DEFAULT NULL,
                        `C5_CG` int(11) DEFAULT NULL,
                        `C6_CB` int(11) DEFAULT NULL,
                        `C6_CG` int(11) DEFAULT NULL,
                        `C7_CB` int(11) DEFAULT NULL,
                        `C7_CG` int(11) DEFAULT NULL,
                        `C8_CB` int(11) DEFAULT NULL,
                        `C8_CG` int(11) DEFAULT NULL,
                        `C1_TB` int(11) DEFAULT NULL,
                        `C1_TG` int(11) DEFAULT NULL,
                        `C2_TB` int(11) DEFAULT NULL,
                        `C2_TG` int(11) DEFAULT NULL,
                        `C3_TB` int(11) DEFAULT NULL,
                        `C3_TG` int(11) DEFAULT NULL,
                        `C4_TB` int(11) DEFAULT NULL,
                        `C4_TG` int(11) DEFAULT NULL,
                        `C5_TB` int(11) DEFAULT NULL,
                        `C5_TG` int(11) DEFAULT NULL,
                        `C6_TB` int(11) DEFAULT NULL,
                        `C6_TG` int(11) DEFAULT NULL,
                        `C7_TB` int(11) DEFAULT NULL,
                        `C7_TG` int(11) DEFAULT NULL,
                        `C8_TB` int(11) DEFAULT NULL,
                        `C8_TG` int(11) DEFAULT NULL,
                        `C1_OB` int(11) DEFAULT NULL,
                        `C1_OG` int(11) DEFAULT NULL,
                        `C2_OB` int(11) DEFAULT NULL,
                        `C2_OG` int(11) DEFAULT NULL,
                        `C3_OB` int(11) DEFAULT NULL,
                        `C3_OG` int(11) DEFAULT NULL,
                        `C4_OB` int(11) DEFAULT NULL,
                        `C4_OG` int(11) DEFAULT NULL,
                        `C5_OB` int(11) DEFAULT NULL,
                        `C5_OG` int(11) DEFAULT NULL,
                        `C6_OB` int(11) DEFAULT NULL,
                        `C6_OG` int(11) DEFAULT NULL,
                        `C7_OB` int(11) DEFAULT NULL,
                        `C7_OG` int(11) DEFAULT NULL,
                        `C8_OB` int(11) DEFAULT NULL,
                        `C8_OG` int(11) DEFAULT NULL,
                        `C1_OTH1B` int(11) DEFAULT NULL,
                        `C1_OTH1G` int(11) DEFAULT NULL,
                        `C2_OTH1B` int(11) DEFAULT NULL,
                        `C2_OTH1G` int(11) DEFAULT NULL,
                        `C3_OTH1B` int(11) DEFAULT NULL,
                        `C3_OTH1G` int(11) DEFAULT NULL,
                        `C4_OTH1B` int(11) DEFAULT NULL,
                        `C4_OTH1G` int(11) DEFAULT NULL,
                        `C5_OTH1B` int(11) DEFAULT NULL,
                        `C5_OTH1G` int(11) DEFAULT NULL,
                        `C6_OTH1B` int(11) DEFAULT NULL,
                        `C6_OTH1G` int(11) DEFAULT NULL,
                        `C7_OTH1B` int(11) DEFAULT NULL,
                        `C7_OTH1G` int(11) DEFAULT NULL,
                        `C8_OTH1B` int(11) DEFAULT NULL,
                        `C8_OTH1G` int(11) DEFAULT NULL,
                        `C1_OTH2B` int(11) DEFAULT NULL,
                        `C1_OTH2G` int(11) DEFAULT NULL,
                        `C2_OTH2B` int(11) DEFAULT NULL,
                        `C2_OTH2G` int(11) DEFAULT NULL,
                        `C3_OTH2B` int(11) DEFAULT NULL,
                        `C3_OTH2G` int(11) DEFAULT NULL,
                        `C4_OTH2B` int(11) DEFAULT NULL,
                        `C4_OTH2G` int(11) DEFAULT NULL,
                        `C5_OTH2B` int(11) DEFAULT NULL,
                        `C5_OTH2G` int(11) DEFAULT NULL,
                        `C6_OTH2B` int(11) DEFAULT NULL,
                        `C6_OTH2G` int(11) DEFAULT NULL,
                        `C7_OTH2B` int(11) DEFAULT NULL,
                        `C7_OTH2G` int(11) DEFAULT NULL,
                        `C8_OTH2B` int(11) DEFAULT NULL,
                        `C8_OTH2G` int(11) DEFAULT NULL,
                        `C1_OTH3B` int(11) DEFAULT NULL,
                        `C1_OTH3G` int(11) DEFAULT NULL,
                        `C2_OTH3B` int(11) DEFAULT NULL,
                        `C2_OTH3G` int(11) DEFAULT NULL,
                        `C3_OTH3B` int(11) DEFAULT NULL,
                        `C3_OTH3G` int(11) DEFAULT NULL,
                        `C4_OTH3B` int(11) DEFAULT NULL,
                        `C4_OTH3G` int(11) DEFAULT NULL,
                        `C5_OTH3B` int(11) DEFAULT NULL,
                        `C5_OTH3G` int(11) DEFAULT NULL,
                        `C6_OTH3B` int(11) DEFAULT NULL,
                        `C6_OTH3G` int(11) DEFAULT NULL,
                        `C7_OTH3B` int(11) DEFAULT NULL,
                        `C7_OTH3G` int(11) DEFAULT NULL,
                        `C8_OTH3B` int(11) DEFAULT NULL,
                        `C8_OTH3G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `V1CPPB` int(11) DEFAULT NULL,
                        `V1CPPG` int(11) DEFAULT NULL,
                        `V2CPPB` int(11) DEFAULT NULL,
                        `V2CPPG` int(11) DEFAULT NULL,
                        `V3CPPB` int(11) DEFAULT NULL,
                        `V3CPPG` int(11) DEFAULT NULL,
                        `V1C9B` int(11) DEFAULT NULL,
                        `V1C9G` int(11) DEFAULT NULL,
                        `V2C9B` int(11) DEFAULT NULL,
                        `V2C9G` int(11) DEFAULT NULL,
                        `V3C9B` int(11) DEFAULT NULL,
                        `V3C9G` int(11) DEFAULT NULL,
                        `V1C10B` int(11) DEFAULT NULL,
                        `V1C10G` int(11) DEFAULT NULL,
                        `V2C10B` int(11) DEFAULT NULL,
                        `V2C10G` int(11) DEFAULT NULL,
                        `V3C10B` int(11) DEFAULT NULL,
                        `V3C10G` int(11) DEFAULT NULL,
                        `V1C11B` int(11) DEFAULT NULL,
                        `V1C11G` int(11) DEFAULT NULL,
                        `V2C11B` int(11) DEFAULT NULL,
                        `V2C11G` int(11) DEFAULT NULL,
                        `V3C11B` int(11) DEFAULT NULL,
                        `V3C11G` int(11) DEFAULT NULL,
                        `V1C12B` int(11) DEFAULT NULL,
                        `V1C12G` int(11) DEFAULT NULL,
                        `V2C12B` int(11) DEFAULT NULL,
                        `V2C12G` int(11) DEFAULT NULL,
                        `V3C12B` int(11) DEFAULT NULL,
                        `V3C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `ENTITY_ID` int(11) DEFAULT NULL,
                        `ENTITY_DESC` varchar(75) DEFAULT NULL,
                        `ENTITY_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYTABLEMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYTABLEMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `TABLE_DESC` varchar(75) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EREPBYSTREAM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EREPBYSTREAM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STREAMID` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `EC11_B` int(11) DEFAULT NULL,
                        `EC11_G` int(11) DEFAULT NULL,
                        `EC12_B` int(11) DEFAULT NULL,
                        `EC12_G` int(11) DEFAULT NULL,
                        `RC11_B` int(11) DEFAULT NULL,
                        `RC11_G` int(11) DEFAULT NULL,
                        `RC12_B` int(11) DEFAULT NULL,
                        `RC12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMINATION09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMINATION09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C5B_GEN` int(11) DEFAULT NULL,
                        `C5G_GEN` int(11) DEFAULT NULL,
                        `C8B_GEN` int(11) DEFAULT NULL,
                        `C8G_GEN` int(11) DEFAULT NULL,
                        `C5B_SC` int(11) DEFAULT NULL,
                        `C5G_SC` int(11) DEFAULT NULL,
                        `C8B_SC` int(11) DEFAULT NULL,
                        `C8G_SC` int(11) DEFAULT NULL,
                        `C5B_ST` int(11) DEFAULT NULL,
                        `C5G_ST` int(11) DEFAULT NULL,
                        `C8B_ST` int(11) DEFAULT NULL,
                        `C8G_ST` int(11) DEFAULT NULL,
                        `C5B_OBC` int(11) DEFAULT NULL,
                        `C5G_OBC` int(11) DEFAULT NULL,
                        `C8B_OBC` int(11) DEFAULT NULL,
                        `C8G_OBC` int(11) DEFAULT NULL,
                        `C5B_MIN` int(11) DEFAULT NULL,
                        `C5G_MIN` int(11) DEFAULT NULL,
                        `C8B_MIN` int(11) DEFAULT NULL,
                        `C8G_MIN` int(11) DEFAULT NULL,
                        `C10B_GEN` int(11) DEFAULT NULL,
                        `C10G_GEN` int(11) DEFAULT NULL,
                        `C12B_GEN` int(11) DEFAULT NULL,
                        `C12G_GEN` int(11) DEFAULT NULL,
                        `C10B_SC` int(11) DEFAULT NULL,
                        `C10G_SC` int(11) DEFAULT NULL,
                        `C12B_SC` int(11) DEFAULT NULL,
                        `C12G_SC` int(11) DEFAULT NULL,
                        `C10B_ST` int(11) DEFAULT NULL,
                        `C10G_ST` int(11) DEFAULT NULL,
                        `C12B_ST` int(11) DEFAULT NULL,
                        `C12G_ST` int(11) DEFAULT NULL,
                        `C10B_OBC` int(11) DEFAULT NULL,
                        `C10G_OBC` int(11) DEFAULT NULL,
                        `C12B_OBC` int(11) DEFAULT NULL,
                        `C12G_OBC` int(11) DEFAULT NULL,
                        `C10B_MIN` int(11) DEFAULT NULL,
                        `C10G_MIN` int(11) DEFAULT NULL,
                        `C12B_MIN` int(11) DEFAULT NULL,
                        `C12G_MIN` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C5_ENROLB` int(11) DEFAULT NULL,
                        `C5_ENROLG` int(11) DEFAULT NULL,
                        `C7_ENROLB` int(11) DEFAULT NULL,
                        `C7_ENROLG` int(11) DEFAULT NULL,
                        `C5_APPEARB` int(11) DEFAULT NULL,
                        `C5_APPEARG` int(11) DEFAULT NULL,
                        `C7_APPEARB` int(11) DEFAULT NULL,
                        `C7_APPEARG` int(11) DEFAULT NULL,
                        `C5_PASSB` int(11) DEFAULT NULL,
                        `C5_PASSG` int(11) DEFAULT NULL,
                        `C7_PASSB` int(11) DEFAULT NULL,
                        `C7_PASSG` int(11) DEFAULT NULL,
                        `C5_M60B` int(11) DEFAULT NULL,
                        `C5_M60G` int(11) DEFAULT NULL,
                        `C7_M60B` int(11) DEFAULT NULL,
                        `C7_M60G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ENROLSC5B` int(11) DEFAULT NULL,
                        `ENROLSC5G` int(11) DEFAULT NULL,
                        `ENROLSC7B` int(11) DEFAULT NULL,
                        `ENROLSC7G` int(11) DEFAULT NULL,
                        `APPEARSC5B` int(11) DEFAULT NULL,
                        `APPEARSC5G` int(11) DEFAULT NULL,
                        `APPEARSC7B` int(11) DEFAULT NULL,
                        `APPEARSC7G` int(11) DEFAULT NULL,
                        `TPASSEDSC5B` int(11) DEFAULT NULL,
                        `TPASSEDSC5G` int(11) DEFAULT NULL,
                        `TPASSEDSC7B` int(11) DEFAULT NULL,
                        `TPASSEDSC7G` int(11) DEFAULT NULL,
                        `PASSM60SC5B` int(11) DEFAULT NULL,
                        `PASSM60SC5G` int(11) DEFAULT NULL,
                        `PASSM60SC7B` int(11) DEFAULT NULL,
                        `PASSM60SC7G` int(11) DEFAULT NULL,
                        `ENROLST5B` int(11) DEFAULT NULL,
                        `ENROLST5G` int(11) DEFAULT NULL,
                        `ENROLST7B` int(11) DEFAULT NULL,
                        `ENROLST7G` int(11) DEFAULT NULL,
                        `APPEARST5B` int(11) DEFAULT NULL,
                        `APPEARST5G` int(11) DEFAULT NULL,
                        `APPEARST7B` int(11) DEFAULT NULL,
                        `APPEARST7G` int(11) DEFAULT NULL,
                        `TPASSEDST5B` int(11) DEFAULT NULL,
                        `TPASSEDST5G` int(11) DEFAULT NULL,
                        `TPASSEDST7B` int(11) DEFAULT NULL,
                        `TPASSEDST7G` int(11) DEFAULT NULL,
                        `PASSM60ST5B` int(11) DEFAULT NULL,
                        `PASSM60ST5G` int(11) DEFAULT NULL,
                        `PASSM60ST7B` int(11) DEFAULT NULL,
                        `PASSM60ST7G` int(11) DEFAULT NULL,
                        `ENROL10B` int(11) DEFAULT NULL,
                        `ENROL10G` int(11) DEFAULT NULL,
                        `APPEAR10B` int(11) DEFAULT NULL,
                        `APPEAR10G` int(11) DEFAULT NULL,
                        `TPASSED10B` int(11) DEFAULT NULL,
                        `TPASSED10G` int(11) DEFAULT NULL,
                        `PASSM6010B` int(11) DEFAULT NULL,
                        `PASSM6010G` int(11) DEFAULT NULL,
                        `ENROL12B` int(11) DEFAULT NULL,
                        `ENROL12G` int(11) DEFAULT NULL,
                        `APPEAR12B` int(11) DEFAULT NULL,
                        `APPEAR12G` int(11) DEFAULT NULL,
                        `TPASSED12B` int(11) DEFAULT NULL,
                        `TPASSED12G` int(11) DEFAULT NULL,
                        `PASSM6012B` int(11) DEFAULT NULL,
                        `PASSM6012G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FACILITIES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FACILITIES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `ITEMVALUE` int(11) DEFAULT NULL,
                        `ITEMVALUE1` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FUNDS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FUNDS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `AMT_R` double DEFAULT NULL,
                        `AMT_U` double DEFAULT NULL,
                        `AMT_S` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_HABITATION`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_HABITATION` (
                        `HABCD` varchar(11) DEFAULT NULL,
                        `HABNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `HABVAR1` varchar(35) DEFAULT NULL,
                        `HABVAR2` varchar(35) DEFAULT NULL,
                        `HABVAR3` varchar(35) DEFAULT NULL,
                        `HABVAR4` varchar(35) DEFAULT NULL,
                        `HABVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_INCENTIVES09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_INCENTIVES09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `GRADEPRUPR` int(11) DEFAULT NULL,
                        `INCENT_TYPE` int(11) DEFAULT NULL,
                        `GEN_B` int(11) DEFAULT NULL,
                        `GEN_G` int(11) DEFAULT NULL,
                        `SC_B` int(11) DEFAULT NULL,
                        `SC_G` int(11) DEFAULT NULL,
                        `ST_B` int(11) DEFAULT NULL,
                        `ST_G` int(11) DEFAULT NULL,
                        `OBC_B` int(11) DEFAULT NULL,
                        `OBC_G` int(11) DEFAULT NULL,
                        `MIN_B` int(11) DEFAULT NULL,
                        `MIN_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_KEYVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_KEYVAR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MAPPING`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MAPPING` (
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHCD_DISE` varchar(11) DEFAULT NULL,
                        `SCHCD_SEMIS` varchar(11) DEFAULT NULL,
                        `MAPREMARKS` varchar(100) DEFAULT NULL,
                        `METHOD` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `DISTHQ` int(11) DEFAULT NULL,
                        `DISTCRC` int(11) DEFAULT NULL,
                        `ESTDYEAR` int(11) DEFAULT NULL,
                        `LOWCLASS` int(11) DEFAULT NULL,
                        `HIGHCLASS` int(11) DEFAULT NULL,
                        `PPSEC_YN` int(11) DEFAULT NULL,
                        `PPSTUDENT` int(11) DEFAULT NULL,
                        `PPTEACHER` int(11) DEFAULT NULL,
                        `WORKDAYS` int(11) DEFAULT NULL,
                        `SCHRES_YN` int(11) DEFAULT NULL,
                        `RESITYPE` int(11) DEFAULT NULL,
                        `SCHSHI_YN` int(11) DEFAULT NULL,
                        `NOINSPECT` int(11) DEFAULT NULL,
                        `VISITSCRC` int(11) DEFAULT NULL,
                        `VISITSBRC` int(11) DEFAULT NULL,
                        `CONTI_R` int(11) DEFAULT NULL,
                        `CONTI_E` int(11) DEFAULT NULL,
                        `TLM_R` int(11) DEFAULT NULL,
                        `TLM_E` int(11) DEFAULT NULL,
                        `FUNDS_E` int(11) DEFAULT NULL,
                        `FUNDS_R` int(11) DEFAULT NULL,
                        `OSRC_E` int(11) DEFAULT NULL,
                        `OSRC_R` int(11) DEFAULT NULL,
                        `TCHSAN` int(11) DEFAULT NULL,
                        `TCHPOS` int(11) DEFAULT NULL,
                        `PARAPOS` int(11) DEFAULT NULL,
                        `NTPOS` int(11) DEFAULT NULL,
                        `MEDINSTR1` int(11) DEFAULT NULL,
                        `MEDINSTR2` int(11) DEFAULT NULL,
                        `MEDINSTR3` int(11) DEFAULT NULL,
                        `MEDINSTR4` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `EDUBLKCD` varchar(6) DEFAULT NULL,
                        `HABITCD` varchar(11) DEFAULT NULL,
                        `ACONSTCD` varchar(7) DEFAULT NULL,
                        `MUNICIPALCD` varchar(7) DEFAULT NULL,
                        `CITY` varchar(7) DEFAULT NULL,
                        `SCHMNTCGRANT_R` int(11) DEFAULT NULL,
                        `SCHMNTCGRANT_E` int(11) DEFAULT NULL,
                        `TLE_R` int(11) DEFAULT NULL,
                        `TLE_E` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL,
                        `DISTP` int(11) DEFAULT NULL,
                        `DISTU` int(11) DEFAULT NULL,
                        `DISTS` int(11) DEFAULT NULL,
                        `DISTH` int(11) DEFAULT NULL,
                        `MEDS1` int(11) DEFAULT NULL,
                        `MEDS2` int(11) DEFAULT NULL,
                        `MEDS3` int(11) DEFAULT NULL,
                        `MEDS4` int(11) DEFAULT NULL,
                        `MEDH1` int(11) DEFAULT NULL,
                        `MEDH2` int(11) DEFAULT NULL,
                        `MEDH3` int(11) DEFAULT NULL,
                        `MEDH4` int(11) DEFAULT NULL,
                        `BOARDSEC` int(11) DEFAULT NULL,
                        `BOARDHSEC` int(11) DEFAULT NULL,
                        `YEARRECOGS` int(11) DEFAULT NULL,
                        `YEARRECOGH` int(11) DEFAULT NULL,
                        `YEARUPGRDS` int(11) DEFAULT NULL,
                        `YEARUPGRDH` int(11) DEFAULT NULL,
                        `SCHRESUPR_YN` int(11) DEFAULT NULL,
                        `SCHRESSEC_YN` int(11) DEFAULT NULL,
                        `SCHRESHSEC_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MDM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MDM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEALS_SUPPLIER` int(11) DEFAULT NULL,
                        `SUPPLIER_NAME` varchar(100) DEFAULT NULL,
                        `MDM_MAINTAINER` int(11) DEFAULT NULL,
                        `TAGGED_YN` int(11) DEFAULT NULL,
                        `TAGGED_TOSCH` varchar(7) DEFAULT NULL,
                        `TAGGED_TOCRC` varchar(6) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `FUEL_USED` int(11) DEFAULT NULL,
                        `WATER_YN` int(11) DEFAULT NULL,
                        `STOREROOM_YN` int(11) DEFAULT NULL,
                        `KITCHEN_TYPE` int(11) DEFAULT NULL,
                        `DAYS_WITHFOOD` int(11) DEFAULT NULL,
                        `DAYS_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `REASON_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `MDM_TOPROGRAM` int(11) DEFAULT NULL,
                        `BENEFITTED_BOYS` int(11) DEFAULT NULL,
                        `BENEFITTED_GIRLS` int(11) DEFAULT NULL,
                        `KITSCHEME` int(11) DEFAULT NULL,
                        `HEALTHPROG` int(11) DEFAULT NULL,
                        `REFBOYS` int(11) DEFAULT NULL,
                        `REFGIRLS` int(11) DEFAULT NULL,
                        `REFBOYSSTATE` int(11) DEFAULT NULL,
                        `REFGIRLSSTATE` int(11) DEFAULT NULL,
                        `TOTLPGCLNDR` int(11) DEFAULT NULL,
                        `MEALSINSCH` int(11) DEFAULT NULL,
                        `NPEGEL_YN` int(11) DEFAULT NULL,
                        `KGBV_MODEL` int(11) DEFAULT NULL,
                        `UTENSILS_YN` int(11) DEFAULT NULL,
                        `MDMVAR1` int(11) DEFAULT NULL,
                        `MDMVAR2` int(11) DEFAULT NULL,
                        `MDMVAR3` int(11) DEFAULT NULL,
                        `MDMVAR4` varchar(50) DEFAULT NULL,
                        `MDMVAR5` varchar(50) DEFAULT NULL,
                        `KITDEVGRANT_YN` int(11) DEFAULT NULL,
                        `COOK_M` int(11) DEFAULT NULL,
                        `COOK_F` int(11) DEFAULT NULL,
                        `MEALSERVED` int(11) DEFAULT NULL,
                        `INSPECT_SO` int(11) DEFAULT NULL,
                        `INSPECT_CM` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `HWFUN_YN` int(11) DEFAULT NULL,
                        `HWSUF_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MEDINSTR` (
                        `MEDINSTR_ID` int(11) DEFAULT NULL,
                        `MEDINSTR_DESC` varchar(75) DEFAULT NULL,
                        `MEDINSTR_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MUNICIPALITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MUNICIPALITY` (
                        `MUNCD` varchar(7) DEFAULT NULL,
                        `MUNNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `MUNVAR1` varchar(35) DEFAULT NULL,
                        `MUNVAR2` varchar(35) DEFAULT NULL,
                        `MUNVAR3` varchar(35) DEFAULT NULL,
                        `MUNVAR4` varchar(35) DEFAULT NULL,
                        `MUNVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_CURNEWB` int(11) DEFAULT NULL,
                        `C1_CURNEWG` int(11) DEFAULT NULL,
                        `C2_CURNEWB` int(11) DEFAULT NULL,
                        `C2_CURNEWG` int(11) DEFAULT NULL,
                        `C3_CURNEWB` int(11) DEFAULT NULL,
                        `C3_CURNEWG` int(11) DEFAULT NULL,
                        `C4_CURNEWB` int(11) DEFAULT NULL,
                        `C4_CURNEWG` int(11) DEFAULT NULL,
                        `C5_CURNEWB` int(11) DEFAULT NULL,
                        `C5_CURNEWG` int(11) DEFAULT NULL,
                        `C6_CURNEWB` int(11) DEFAULT NULL,
                        `C6_CURNEWG` int(11) DEFAULT NULL,
                        `C7_CURNEWB` int(11) DEFAULT NULL,
                        `C7_CURNEWG` int(11) DEFAULT NULL,
                        `C8_CURNEWB` int(11) DEFAULT NULL,
                        `C8_CURNEWG` int(11) DEFAULT NULL,
                        `C1_CURTCB` int(11) DEFAULT NULL,
                        `C1_CURTCG` int(11) DEFAULT NULL,
                        `C2_CURTCB` int(11) DEFAULT NULL,
                        `C2_CURTCG` int(11) DEFAULT NULL,
                        `C3_CURTCB` int(11) DEFAULT NULL,
                        `C3_CURTCG` int(11) DEFAULT NULL,
                        `C4_CURTCB` int(11) DEFAULT NULL,
                        `C4_CURTCG` int(11) DEFAULT NULL,
                        `C5_CURTCB` int(11) DEFAULT NULL,
                        `C5_CURTCG` int(11) DEFAULT NULL,
                        `C6_CURTCB` int(11) DEFAULT NULL,
                        `C6_CURTCG` int(11) DEFAULT NULL,
                        `C7_CURTCB` int(11) DEFAULT NULL,
                        `C7_CURTCG` int(11) DEFAULT NULL,
                        `C8_CURTCB` int(11) DEFAULT NULL,
                        `C8_CURTCG` int(11) DEFAULT NULL,
                        `C1_PRVNEWB` int(11) DEFAULT NULL,
                        `C1_PRVNEWG` int(11) DEFAULT NULL,
                        `C2_PRVNEWB` int(11) DEFAULT NULL,
                        `C2_PRVNEWG` int(11) DEFAULT NULL,
                        `C3_PRVNEWB` int(11) DEFAULT NULL,
                        `C3_PRVNEWG` int(11) DEFAULT NULL,
                        `C4_PRVNEWB` int(11) DEFAULT NULL,
                        `C4_PRVNEWG` int(11) DEFAULT NULL,
                        `C5_PRVNEWB` int(11) DEFAULT NULL,
                        `C5_PRVNEWG` int(11) DEFAULT NULL,
                        `C6_PRVNEWB` int(11) DEFAULT NULL,
                        `C6_PRVNEWG` int(11) DEFAULT NULL,
                        `C7_PRVNEWB` int(11) DEFAULT NULL,
                        `C7_PRVNEWG` int(11) DEFAULT NULL,
                        `C8_PRVNEWB` int(11) DEFAULT NULL,
                        `C8_PRVNEWG` int(11) DEFAULT NULL,
                        `C1_PRVTCB` int(11) DEFAULT NULL,
                        `C1_PRVTCG` int(11) DEFAULT NULL,
                        `C2_PRVTCB` int(11) DEFAULT NULL,
                        `C2_PRVTCG` int(11) DEFAULT NULL,
                        `C3_PRVTCB` int(11) DEFAULT NULL,
                        `C3_PRVTCG` int(11) DEFAULT NULL,
                        `C4_PRVTCB` int(11) DEFAULT NULL,
                        `C4_PRVTCG` int(11) DEFAULT NULL,
                        `C5_PRVTCB` int(11) DEFAULT NULL,
                        `C5_PRVTCG` int(11) DEFAULT NULL,
                        `C6_PRVTCB` int(11) DEFAULT NULL,
                        `C6_PRVTCG` int(11) DEFAULT NULL,
                        `C7_PRVTCB` int(11) DEFAULT NULL,
                        `C7_PRVTCG` int(11) DEFAULT NULL,
                        `C8_PRVTCB` int(11) DEFAULT NULL,
                        `C8_PRVTCG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CURNEW_9B` int(11) DEFAULT NULL,
                        `CURNEW_9G` int(11) DEFAULT NULL,
                        `CURTC_9B` int(11) DEFAULT NULL,
                        `CURTC_9G` int(11) DEFAULT NULL,
                        `PRVNEW_9B` int(11) DEFAULT NULL,
                        `PRVNEW_9G` int(11) DEFAULT NULL,
                        `PRVTC_9B` int(11) DEFAULT NULL,
                        `PRVTC_9G` int(11) DEFAULT NULL,
                        `CURNEW_10B` int(11) DEFAULT NULL,
                        `CURNEW_10G` int(11) DEFAULT NULL,
                        `CURTC_10B` int(11) DEFAULT NULL,
                        `CURTC_10G` int(11) DEFAULT NULL,
                        `PRVNEW_10B` int(11) DEFAULT NULL,
                        `PRVNEW_10G` int(11) DEFAULT NULL,
                        `PRVTC_10B` int(11) DEFAULT NULL,
                        `PRVTC_10G` int(11) DEFAULT NULL,
                        `CURNEW_11B` int(11) DEFAULT NULL,
                        `CURNEW_11G` int(11) DEFAULT NULL,
                        `CURTC_11B` int(11) DEFAULT NULL,
                        `CURTC_11G` int(11) DEFAULT NULL,
                        `PRVNEW_11B` int(11) DEFAULT NULL,
                        `PRVNEW_11G` int(11) DEFAULT NULL,
                        `PRVTC_11B` int(11) DEFAULT NULL,
                        `PRVTC_11G` int(11) DEFAULT NULL,
                        `CURNEW_12B` int(11) DEFAULT NULL,
                        `CURNEW_12G` int(11) DEFAULT NULL,
                        `CURTC_12B` int(11) DEFAULT NULL,
                        `CURTC_12G` int(11) DEFAULT NULL,
                        `PRVNEW_12B` int(11) DEFAULT NULL,
                        `PRVNEW_12G` int(11) DEFAULT NULL,
                        `PRVTC_12B` int(11) DEFAULT NULL,
                        `PRVTC_12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADMGRD1`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADMGRD1` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGE4_B` int(11) DEFAULT NULL,
                        `AGE4_G` int(11) DEFAULT NULL,
                        `AGE5_B` int(11) DEFAULT NULL,
                        `AGE5_G` int(11) DEFAULT NULL,
                        `AGE6_B` int(11) DEFAULT NULL,
                        `AGE6_G` int(11) DEFAULT NULL,
                        `AGE7_B` int(11) DEFAULT NULL,
                        `AGE7_G` int(11) DEFAULT NULL,
                        `AGE8_B` int(11) DEFAULT NULL,
                        `AGE8_G` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `SAMESCH_B` int(11) DEFAULT NULL,
                        `SAMESCH_G` int(11) DEFAULT NULL,
                        `OTHERSCH_B` int(11) DEFAULT NULL,
                        `OTHERSCH_G` int(11) DEFAULT NULL,
                        `ECCE_B` int(11) DEFAULT NULL,
                        `ECCE_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NONTCH`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NONTCH` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `DESIG_ID` int(11) DEFAULT NULL,
                        `TOTSAN` int(11) DEFAULT NULL,
                        `TOTPOS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PANCHAYAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PANCHAYAT` (
                        `PANCD` varchar(9) DEFAULT NULL,
                        `PANNAME` varchar(30) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PANVAR1` varchar(35) DEFAULT NULL,
                        `PANVAR2` varchar(35) DEFAULT NULL,
                        `PANVAR3` varchar(35) DEFAULT NULL,
                        `PANVAR4` varchar(35) DEFAULT NULL,
                        `PANVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PCONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PCONSTITUENCY` (
                        `CONSTCD` varchar(8) DEFAULT NULL,
                        `CONSTNAME` varchar(75) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_POPDROPNENR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_POPDROPNENR` (
                        `TABLEID` int(11) DEFAULT NULL,
                        `DBVHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `LEVELID` int(11) DEFAULT NULL,
                        `GEN610B` int(11) DEFAULT NULL,
                        `GEN610G` int(11) DEFAULT NULL,
                        `SC610B` int(11) DEFAULT NULL,
                        `SC610G` int(11) DEFAULT NULL,
                        `ST610B` int(11) DEFAULT NULL,
                        `ST610G` int(11) DEFAULT NULL,
                        `OBC610B` int(11) DEFAULT NULL,
                        `OBC610G` int(11) DEFAULT NULL,
                        `TOT610B` int(11) DEFAULT NULL,
                        `TOT610G` int(11) DEFAULT NULL,
                        `MIN_610B` int(11) DEFAULT NULL,
                        `MIN_610G` int(11) DEFAULT NULL,
                        `GEN1113B` int(11) DEFAULT NULL,
                        `GEN1113G` int(11) DEFAULT NULL,
                        `SC1113B` int(11) DEFAULT NULL,
                        `SC1113G` int(11) DEFAULT NULL,
                        `ST1113B` int(11) DEFAULT NULL,
                        `ST1113G` int(11) DEFAULT NULL,
                        `OBC1113B` int(11) DEFAULT NULL,
                        `OBC1113G` int(11) DEFAULT NULL,
                        `TOT1113B` int(11) DEFAULT NULL,
                        `TOT1113G` int(11) DEFAULT NULL,
                        `MIN_1113B` int(11) DEFAULT NULL,
                        `MIN_1113G` int(11) DEFAULT NULL,
                        `GEN1415B` int(11) DEFAULT NULL,
                        `GEN1415G` int(11) DEFAULT NULL,
                        `SC1415B` int(11) DEFAULT NULL,
                        `SC1415G` int(11) DEFAULT NULL,
                        `ST1415B` int(11) DEFAULT NULL,
                        `ST1415G` int(11) DEFAULT NULL,
                        `OBC1415B` int(11) DEFAULT NULL,
                        `OBC1415G` int(11) DEFAULT NULL,
                        `TOT1415B` int(11) DEFAULT NULL,
                        `TOT1415G` int(11) DEFAULT NULL,
                        `MIN_1415B` int(11) DEFAULT NULL,
                        `MIN_1415G` int(11) DEFAULT NULL,
                        `DIST1` double DEFAULT NULL,
                        `DIST2` double DEFAULT NULL,
                        `DIST3` double DEFAULT NULL,
                        `DIST4` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_FAILB` int(11) DEFAULT NULL,
                        `C1_FAILG` int(11) DEFAULT NULL,
                        `C2_FAILB` int(11) DEFAULT NULL,
                        `C2_FAILG` int(11) DEFAULT NULL,
                        `C3_FAILB` int(11) DEFAULT NULL,
                        `C3_FAILG` int(11) DEFAULT NULL,
                        `C4_FAILB` int(11) DEFAULT NULL,
                        `C4_FAILG` int(11) DEFAULT NULL,
                        `C5_FAILB` int(11) DEFAULT NULL,
                        `C5_FAILG` int(11) DEFAULT NULL,
                        `C6_FAILB` int(11) DEFAULT NULL,
                        `C6_FAILG` int(11) DEFAULT NULL,
                        `C7_FAILB` int(11) DEFAULT NULL,
                        `C7_FAILG` int(11) DEFAULT NULL,
                        `C8_FAILB` int(11) DEFAULT NULL,
                        `C8_FAILG` int(11) DEFAULT NULL,
                        `C1_ABSB` int(11) DEFAULT NULL,
                        `C1_ABSG` int(11) DEFAULT NULL,
                        `C2_ABSB` int(11) DEFAULT NULL,
                        `C2_ABSG` int(11) DEFAULT NULL,
                        `C3_ABSB` int(11) DEFAULT NULL,
                        `C3_ABSG` int(11) DEFAULT NULL,
                        `C4_ABSB` int(11) DEFAULT NULL,
                        `C4_ABSG` int(11) DEFAULT NULL,
                        `C5_ABSB` int(11) DEFAULT NULL,
                        `C5_ABSG` int(11) DEFAULT NULL,
                        `C6_ABSB` int(11) DEFAULT NULL,
                        `C6_ABSG` int(11) DEFAULT NULL,
                        `C7_ABSB` int(11) DEFAULT NULL,
                        `C7_ABSG` int(11) DEFAULT NULL,
                        `C8_ABSB` int(11) DEFAULT NULL,
                        `C8_ABSG` int(11) DEFAULT NULL,
                        `C1_READB` int(11) DEFAULT NULL,
                        `C1_READG` int(11) DEFAULT NULL,
                        `C2_READB` int(11) DEFAULT NULL,
                        `C2_READG` int(11) DEFAULT NULL,
                        `C3_READB` int(11) DEFAULT NULL,
                        `C3_READG` int(11) DEFAULT NULL,
                        `C4_READB` int(11) DEFAULT NULL,
                        `C4_READG` int(11) DEFAULT NULL,
                        `C5_READB` int(11) DEFAULT NULL,
                        `C5_READG` int(11) DEFAULT NULL,
                        `C6_READB` int(11) DEFAULT NULL,
                        `C6_READG` int(11) DEFAULT NULL,
                        `C7_READB` int(11) DEFAULT NULL,
                        `C7_READG` int(11) DEFAULT NULL,
                        `C8_READB` int(11) DEFAULT NULL,
                        `C8_READG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9BFAIL` int(11) DEFAULT NULL,
                        `C9GFAIL` int(11) DEFAULT NULL,
                        `C9BABS` int(11) DEFAULT NULL,
                        `C9GABS` int(11) DEFAULT NULL,
                        `C9BREAD` int(11) DEFAULT NULL,
                        `C9GREAD` int(11) DEFAULT NULL,
                        `C10BFAIL` int(11) DEFAULT NULL,
                        `C10GFAIL` int(11) DEFAULT NULL,
                        `C10BABS` int(11) DEFAULT NULL,
                        `C10GABS` int(11) DEFAULT NULL,
                        `C10BREAD` int(11) DEFAULT NULL,
                        `C10GREAD` int(11) DEFAULT NULL,
                        `C11BFAIL` int(11) DEFAULT NULL,
                        `C11GFAIL` int(11) DEFAULT NULL,
                        `C11BABS` int(11) DEFAULT NULL,
                        `C11GABS` int(11) DEFAULT NULL,
                        `C11BREAD` int(11) DEFAULT NULL,
                        `C11GREAD` int(11) DEFAULT NULL,
                        `C12BFAIL` int(11) DEFAULT NULL,
                        `C12GFAIL` int(11) DEFAULT NULL,
                        `C12BABS` int(11) DEFAULT NULL,
                        `C12GABS` int(11) DEFAULT NULL,
                        `C12BREAD` int(11) DEFAULT NULL,
                        `C12GREAD` int(11) DEFAULT NULL,
                        `C1BSC` int(11) DEFAULT NULL,
                        `C1GSC` int(11) DEFAULT NULL,
                        `C2BSC` int(11) DEFAULT NULL,
                        `C2GSC` int(11) DEFAULT NULL,
                        `C3BSC` int(11) DEFAULT NULL,
                        `C3GSC` int(11) DEFAULT NULL,
                        `C4BSC` int(11) DEFAULT NULL,
                        `C4GSC` int(11) DEFAULT NULL,
                        `C5BSC` int(11) DEFAULT NULL,
                        `C5GSC` int(11) DEFAULT NULL,
                        `C6BSC` int(11) DEFAULT NULL,
                        `C6GSC` int(11) DEFAULT NULL,
                        `C7BSC` int(11) DEFAULT NULL,
                        `C7GSC` int(11) DEFAULT NULL,
                        `C8BSC` int(11) DEFAULT NULL,
                        `C8GSC` int(11) DEFAULT NULL,
                        `C9BSC` int(11) DEFAULT NULL,
                        `C9GSC` int(11) DEFAULT NULL,
                        `C10BSC` int(11) DEFAULT NULL,
                        `C10GSC` int(11) DEFAULT NULL,
                        `C11BSC` int(11) DEFAULT NULL,
                        `C11GSC` int(11) DEFAULT NULL,
                        `C12BSC` int(11) DEFAULT NULL,
                        `C12GSC` int(11) DEFAULT NULL,
                        `C1BST` int(11) DEFAULT NULL,
                        `C1GST` int(11) DEFAULT NULL,
                        `C2BST` int(11) DEFAULT NULL,
                        `C2GST` int(11) DEFAULT NULL,
                        `C3BST` int(11) DEFAULT NULL,
                        `C3GST` int(11) DEFAULT NULL,
                        `C4BST` int(11) DEFAULT NULL,
                        `C4GST` int(11) DEFAULT NULL,
                        `C5BST` int(11) DEFAULT NULL,
                        `C5GST` int(11) DEFAULT NULL,
                        `C6BST` int(11) DEFAULT NULL,
                        `C6GST` int(11) DEFAULT NULL,
                        `C7BST` int(11) DEFAULT NULL,
                        `C7GST` int(11) DEFAULT NULL,
                        `C8BST` int(11) DEFAULT NULL,
                        `C8GST` int(11) DEFAULT NULL,
                        `C9BST` int(11) DEFAULT NULL,
                        `C9GST` int(11) DEFAULT NULL,
                        `C10BST` int(11) DEFAULT NULL,
                        `C10GST` int(11) DEFAULT NULL,
                        `C11BST` int(11) DEFAULT NULL,
                        `C11GST` int(11) DEFAULT NULL,
                        `C12BST` int(11) DEFAULT NULL,
                        `C12GST` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RESITYPE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RESITYPE` (
                        `RESITYPE_ID` int(11) DEFAULT NULL,
                        `RESITYPE_DESC` varchar(75) DEFAULT NULL,
                        `RESITYPE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RTEINFO`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RTEINFO` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `WORKDAYS_PR` int(11) DEFAULT NULL,
                        `WORKDAYS_UPR` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_PR` double DEFAULT NULL,
                        `SCHHRSCHILD_UPR` double DEFAULT NULL,
                        `SCHHRSTCH_PR` double DEFAULT NULL,
                        `SCHHRSTCH_UPR` double DEFAULT NULL,
                        `CCE_YN` int(11) DEFAULT NULL,
                        `PCR_MAINTAINED` int(11) DEFAULT NULL,
                        `PCR_SHARED` int(11) DEFAULT NULL,
                        `WSEC25P_APPLIED` int(11) DEFAULT NULL,
                        `WSEC25P_ENROLLED` int(11) DEFAULT NULL,
                        `AIDRECD` int(11) DEFAULT NULL,
                        `STUADMITTED` int(11) DEFAULT NULL,
                        `SMC_YN` int(11) DEFAULT NULL,
                        `SMCMEM_M` int(11) DEFAULT NULL,
                        `SMCMEM_F` int(11) DEFAULT NULL,
                        `SMSPARENTS_M` int(11) DEFAULT NULL,
                        `SMSPARENTS_F` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_M` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_F` int(11) DEFAULT NULL,
                        `SMCMEETINGS` int(11) DEFAULT NULL,
                        `SMCSDP_YN` int(11) DEFAULT NULL,
                        `SMSCHILDREC_YN` int(11) DEFAULT NULL,
                        `SMCBANKAC_YN` int(11) DEFAULT NULL,
                        `SMCBANK` varchar(75) DEFAULT NULL,
                        `SMCBANKBRANCH` varchar(75) DEFAULT NULL,
                        `SMCACNO` varchar(20) DEFAULT NULL,
                        `IFSCCODE` varchar(20) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_BY` int(11) DEFAULT NULL,
                        `SPLTRG_PLACE` int(11) DEFAULT NULL,
                        `SPLTRG_TYPE` int(11) DEFAULT NULL,
                        `SPLTRG_TOTEV` int(11) DEFAULT NULL,
                        `SPLTRG_EVTRND` int(11) DEFAULT NULL,
                        `SPLTRG_MATERIAL_YN` int(11) DEFAULT NULL,
                        `TXTBKRECD_YN` int(11) DEFAULT NULL,
                        `TXTBKMNTH` int(11) DEFAULT NULL,
                        `TXTBKYEAR` int(11) DEFAULT NULL,
                        `ACSTARTMNTH` int(11) DEFAULT NULL,
                        `SMCACNAME` varchar(100) DEFAULT NULL,
                        `SPLTRNG_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHCAT` (
                        `SCHCAT_ID` int(11) DEFAULT NULL,
                        `SCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `SCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHMGT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHMGT` (
                        `SCHMGT_ID` int(11) DEFAULT NULL,
                        `SCHMGT_DESC` varchar(75) DEFAULT NULL,
                        `SCHMGT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHOOL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHOOL` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `SCHVAR1` varchar(35) DEFAULT NULL,
                        `SCHVAR2` varchar(35) DEFAULT NULL,
                        `SCHVAR3` varchar(35) DEFAULT NULL,
                        `SCHVAR4` varchar(35) DEFAULT NULL,
                        `SCHVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SMDC`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SMDC` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SMDC_YN` int(11) DEFAULT NULL,
                        `PARENTS_M` int(11) DEFAULT NULL,
                        `PARENTS_F` int(11) DEFAULT NULL,
                        `LOCAL_M` int(11) DEFAULT NULL,
                        `LOCAL_F` int(11) DEFAULT NULL,
                        `EBMC_M` int(11) DEFAULT NULL,
                        `EBMC_F` int(11) DEFAULT NULL,
                        `WOMEN_M` int(11) DEFAULT NULL,
                        `SCST_M` int(11) DEFAULT NULL,
                        `SCST_F` int(11) DEFAULT NULL,
                        `DEO_M` int(11) DEFAULT NULL,
                        `DEO_F` int(11) DEFAULT NULL,
                        `AAD_M` int(11) DEFAULT NULL,
                        `AAD_F` int(11) DEFAULT NULL,
                        `SUBEXP_M` int(11) DEFAULT NULL,
                        `SUBEXP_F` int(11) DEFAULT NULL,
                        `TCH_M` int(11) DEFAULT NULL,
                        `TCH_F` int(11) DEFAULT NULL,
                        `AHM_M` int(11) DEFAULT NULL,
                        `AHM_F` int(11) DEFAULT NULL,
                        `HM_M` int(11) DEFAULT NULL,
                        `HM_F` int(11) DEFAULT NULL,
                        `CP_M` int(11) DEFAULT NULL,
                        `CP_F` int(11) DEFAULT NULL,
                        `SMDCMEETING` int(11) DEFAULT NULL,
                        `SIP_YN` int(11) DEFAULT NULL,
                        `BANKAC_YN` int(11) DEFAULT NULL,
                        `BANKNAME` varchar(75) DEFAULT NULL,
                        `BANKBRANCH` varchar(75) DEFAULT NULL,
                        `BANKACNO` varchar(50) DEFAULT NULL,
                        `ACNAME` varchar(100) DEFAULT NULL,
                        `IFSC` varchar(50) DEFAULT NULL,
                        `SBC_YN` int(11) DEFAULT NULL,
                        `AC_YN` int(11) DEFAULT NULL,
                        `PTA_YN` int(11) DEFAULT NULL,
                        `PTAMEETING` int(11) DEFAULT NULL,
                        `WORKDAYS_SEC` int(11) DEFAULT NULL,
                        `WORKDAYS_HSEC` int(11) DEFAULT NULL,
                        `CCESEC_YN` int(11) DEFAULT NULL,
                        `CCEHSEC_YN` int(11) DEFAULT NULL,
                        `SMCSMDC1_YN` int(11) DEFAULT NULL,
                        `TOT_M` int(11) DEFAULT NULL,
                        `TOT_F` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_SEC` double DEFAULT NULL,
                        `SCHHRSCHILD_HSEC` double DEFAULT NULL,
                        `SCHHRSTCH_SEC` double DEFAULT NULL,
                        `SCHHRSTCH_HSEC` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SPLTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SPLTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_STATLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_STATLIST` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVAR` (
                        `VARID` int(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARGROUP` int(11) DEFAULT NULL,
                        `VARSEQ` int(11) DEFAULT NULL,
                        `VARDESC` varchar(100) DEFAULT NULL,
                        `VARTYPE` int(11) DEFAULT NULL,
                        `VARLEN` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(100) DEFAULT NULL,
                        `VARVALIDATION` varchar(50) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVARENTRY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVARENTRY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARID` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(50) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHAQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHAQUAL` (
                        `TCHAQUAL_ID` int(11) DEFAULT NULL,
                        `TCHAQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHAQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHBYSUB`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHBYSUB` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLASSID` int(11) DEFAULT NULL,
                        `SUBJECTID` int(11) DEFAULT NULL,
                        `SANCTIONED` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTE` (
                        `TCHCASTE_ID` int(11) DEFAULT NULL,
                        `TCHCASTE_DESC` varchar(75) DEFAULT NULL,
                        `TCHCASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHTYPE` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(10) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `TRNGM` int(11) DEFAULT NULL,
                        `TRNGF` int(11) DEFAULT NULL,
                        `TRNCM` int(11) DEFAULT NULL,
                        `TRNCF` int(11) DEFAULT NULL,
                        `TRNTM` int(11) DEFAULT NULL,
                        `TRNTF` int(11) DEFAULT NULL,
                        `TRNOM` int(11) DEFAULT NULL,
                        `TRNOF` int(11) DEFAULT NULL,
                        `UTRGM` int(11) DEFAULT NULL,
                        `UTRGF` int(11) DEFAULT NULL,
                        `UTRCM` int(11) DEFAULT NULL,
                        `UTRCF` int(11) DEFAULT NULL,
                        `UTRTM` int(11) DEFAULT NULL,
                        `UTRTF` int(11) DEFAULT NULL,
                        `UTROM` int(11) DEFAULT NULL,
                        `UTROF` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCAT` (
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `TCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCATMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCATMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `NUM_SANCTIONED` int(11) DEFAULT NULL,
                        `NUM_POSITION` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCLSTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCLSTAUGHT` (
                        `TCHCLSTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHCLSTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCLSTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHPQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHPQUAL` (
                        `TCHPQUAL_ID` int(11) DEFAULT NULL,
                        `TCHPQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHPQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHSUBTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHSUBTAUGHT` (
                        `TCHSUBTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHSUBTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHSUBTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TEACHER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TEACHER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TCHCD` varchar(12) DEFAULT NULL,
                        `SLNO` int(11) DEFAULT NULL,
                        `TCHNAME` varchar(50) DEFAULT NULL,
                        `SEX` int(11) DEFAULT NULL,
                        `DOB` datetime DEFAULT NULL,
                        `CASTE` int(11) DEFAULT NULL,
                        `CATEGORY` int(11) DEFAULT NULL,
                        `YOJ` int(11) DEFAULT NULL,
                        `QUAL_ACAD` int(11) DEFAULT NULL,
                        `QUAL_PROF` int(11) DEFAULT NULL,
                        `CLS_TAUGHT` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT1` int(11) DEFAULT NULL,
                        `MATH_UPTO` int(11) DEFAULT NULL,
                        `ENG_UPTO` int(11) DEFAULT NULL,
                        `TRN_BRC` int(11) DEFAULT NULL,
                        `TRN_CRC` int(11) DEFAULT NULL,
                        `TRN_DIET` int(11) DEFAULT NULL,
                        `TRN_OTHER` int(11) DEFAULT NULL,
                        `NONTCH_ASS` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT2` int(11) DEFAULT NULL,
                        `SUPVAR1` int(11) DEFAULT NULL,
                        `SUPVAR2` int(11) DEFAULT NULL,
                        `SUPVAR3` int(11) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` datetime DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `WORKINGSINCE` int(11) DEFAULT NULL,
                        `POST_STATUS` int(11) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `DEPUTATION_YN` int(11) DEFAULT NULL,
                        `SCIENCEUPTO` int(11) DEFAULT NULL,
                        `CWSNTRAINED_YN` int(11) DEFAULT NULL,
                        `APPOINTSUB` int(11) DEFAULT NULL,
                        `STREAM` int(11) DEFAULT NULL,
                        `AADHAAR` bigint(12) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TXTBKTLESPORTS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TXTBKTLESPORTS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1` int(11) DEFAULT NULL,
                        `C2` int(11) DEFAULT NULL,
                        `C3` int(11) DEFAULT NULL,
                        `C4` int(11) DEFAULT NULL,
                        `C5` int(11) DEFAULT NULL,
                        `C6` int(11) DEFAULT NULL,
                        `C7` int(11) DEFAULT NULL,
                        `C8` int(11) DEFAULT NULL,
                        `C9` int(11) DEFAULT NULL,
                        `C10` int(11) DEFAULT NULL,
                        `C11` int(11) DEFAULT NULL,
                        `C12` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UCDETAILS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UCDETAILS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `OPENINGBAL` double DEFAULT NULL,
                        `AMTRECD` double DEFAULT NULL,
                        `AMTSPENT` double DEFAULT NULL,
                        `CLOSINGBAL` double DEFAULT NULL,
                        `BANKINT` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VARSETTINGS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VARSETTINGS` (
                        `VAR_ID` varchar(10) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VAR_TYPE` int(11) DEFAULT NULL,
                        `VAR_LEN` int(11) DEFAULT NULL,
                        `VAR_LBL` varchar(30) DEFAULT NULL,
                        `VAR_TBL` varchar(30) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VILLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VILLAGE` (
                        `VILCD` varchar(9) DEFAULT NULL,
                        `VILNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `TRIBAL_YN` int(11) DEFAULT NULL,
                        `CENCD` varchar(21) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `VILVAR1` varchar(35) DEFAULT NULL,
                        `VILVAR2` varchar(35) DEFAULT NULL,
                        `VILVAR3` varchar(35) DEFAULT NULL,
                        `VILVAR4` varchar(35) DEFAULT NULL,
                        `VILVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_WATER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_WATER` (
                        `WATER_ID` int(11) DEFAULT NULL,
                        `WATER_DESC` varchar(75) DEFAULT NULL,
                        `WATER_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
                /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
                /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
                iiSET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


                /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
                /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
                /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
                /*!40101 SET NAMES utf8 */;

                --
                    -- Database: `ssa`
                    --

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ATTENDANCE09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ATTENDANCE09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `BLDSTATUS` int(11) DEFAULT NULL,
                        `CLROOMS` int(11) DEFAULT NULL,
                        `CLGOOD` int(11) DEFAULT NULL,
                        `CLMAJOR` int(11) DEFAULT NULL,
                        `CLMINOR` int(11) DEFAULT NULL,
                        `OTHGOOD` int(11) DEFAULT NULL,
                        `OTHMAJOR` int(11) DEFAULT NULL,
                        `OTHMINOR` int(11) DEFAULT NULL,
                        `TOILET_C` int(11) DEFAULT NULL,
                        `TOILET_G` int(11) DEFAULT NULL,
                        `ELECTRIC_YN` int(11) DEFAULT NULL,
                        `BNDRYWALL` int(11) DEFAULT NULL,
                        `BOOKBANK_YN` int(11) DEFAULT NULL,
                        `PGROUND_YN` int(11) DEFAULT NULL,
                        `BLACKBOARD` int(11) DEFAULT NULL,
                        `ALMIRAH` int(11) DEFAULT NULL,
                        `BOX` int(11) DEFAULT NULL,
                        `BOOKINLIB` int(11) DEFAULT NULL,
                        `WATER` int(11) DEFAULT NULL,
                        `MEDCHK_YN` int(11) DEFAULT NULL,
                        `RAMPS_YN` int(11) DEFAULT NULL,
                        `COMPUTER` int(11) DEFAULT NULL,
                        `BLDPUCCA` int(11) DEFAULT NULL,
                        `BLDPPUCCA` int(11) DEFAULT NULL,
                        `BLDKUCCHA` int(11) DEFAULT NULL,
                        `BLDTENT` int(11) DEFAULT NULL,
                        `CLSPUCCA` int(11) DEFAULT NULL,
                        `CLSPPUCCA` int(11) DEFAULT NULL,
                        `CLSKUCCHA` int(11) DEFAULT NULL,
                        `CLSTENT` int(11) DEFAULT NULL,
                        `ROOMSPUCCA` int(11) DEFAULT NULL,
                        `ROOMSPPUCCA` int(11) DEFAULT NULL,
                        `ROOMSKUCCHA` int(11) DEFAULT NULL,
                        `ROOMSTENT` int(11) DEFAULT NULL,
                        `ANANDCLASS` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `FURNTCH` int(11) DEFAULT NULL,
                        `FURNSTU` int(11) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL,
                        `BLDCOND` int(11) DEFAULT NULL,
                        `TOTCLS9` int(11) DEFAULT NULL,
                        `TOTCLS10` int(11) DEFAULT NULL,
                        `TOTCLS11` int(11) DEFAULT NULL,
                        `TOTCLS12` int(11) DEFAULT NULL,
                        `TOTOTH9` int(11) DEFAULT NULL,
                        `TOTOTH10` int(11) DEFAULT NULL,
                        `TOTOTH11` int(11) DEFAULT NULL,
                        `TOTOTH12` int(11) DEFAULT NULL,
                        `CLSUCONST9` int(11) DEFAULT NULL,
                        `CLSUCONST10` int(11) DEFAULT NULL,
                        `CLSUCONST11` int(11) DEFAULT NULL,
                        `CLSUCONST12` int(11) DEFAULT NULL,
                        `BBOARD9` int(11) DEFAULT NULL,
                        `BBOARD10` int(11) DEFAULT NULL,
                        `BBOARD11` int(11) DEFAULT NULL,
                        `BBOARD12` int(11) DEFAULT NULL,
                        `FURN_YN9` int(11) DEFAULT NULL,
                        `FURN_YN10` int(11) DEFAULT NULL,
                        `FURN_YN11` int(11) DEFAULT NULL,
                        `FURN_YN12` int(11) DEFAULT NULL,
                        `CLGOODHS` int(11) DEFAULT NULL,
                        `CLMINORHS` int(11) DEFAULT NULL,
                        `CLMAJORHS` int(11) DEFAULT NULL,
                        `OTHGOODHS` int(11) DEFAULT NULL,
                        `OTHMINORHS` int(11) DEFAULT NULL,
                        `OTHMAJORHS` int(11) DEFAULT NULL,
                        `TOILETWATER_B` int(11) DEFAULT NULL,
                        `TOILETWATER_G` int(11) DEFAULT NULL,
                        `URINALS_B` int(11) DEFAULT NULL,
                        `URINALS_G` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `LIBRARIAN_YN` int(11) DEFAULT NULL,
                        `COMPSEC` int(11) DEFAULT NULL,
                        `COMPSEC_FUNC` int(11) DEFAULT NULL,
                        `COMPHS` int(11) DEFAULT NULL,
                        `COMPHS_FUNC` int(11) DEFAULT NULL,
                        `RAMPSNEEDED_YN` int(11) DEFAULT NULL,
                        `HOSTELB_YN` int(11) DEFAULT NULL,
                        `HOSTELBOYS` int(11) DEFAULT NULL,
                        `HOSTELG_YN` int(11) DEFAULT NULL,
                        `HOSTELGIRLS` int(11) DEFAULT NULL,
                        `OTHROOMS` int(11) DEFAULT NULL,
                        `COMP_FUNC` int(11) DEFAULT NULL,
                        `CLGOODS` int(11) DEFAULT NULL,
                        `CLMINORS` int(11) DEFAULT NULL,
                        `CLMAJORS` int(11) DEFAULT NULL,
                        `OTHGOODS` int(11) DEFAULT NULL,
                        `OTHMINORS` int(11) DEFAULT NULL,
                        `OTHMAJORS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDSTATUS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDSTATUS` (
                        `BLDSTATUS_ID` int(11) DEFAULT NULL,
                        `BLDSTATUS_DESC` varchar(75) DEFAULT NULL,
                        `BLDSTATUS_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `TRIBAL_YN` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `BLKVAR1` varchar(35) DEFAULT NULL,
                        `BLKVAR2` varchar(35) DEFAULT NULL,
                        `BLKVAR3` varchar(35) DEFAULT NULL,
                        `BLKVAR4` varchar(35) DEFAULT NULL,
                        `BLKVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BNDRYWALL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BNDRYWALL` (
                        `BNDRYWALL_ID` int(11) DEFAULT NULL,
                        `BNDRYWALL_DESC` varchar(75) DEFAULT NULL,
                        `BNDRYWALL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CASTE` (
                        `CASTE_ID` int(11) DEFAULT NULL,
                        `CASTE_DESC` varchar(75) DEFAULT NULL,
                        `CASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CITY` (
                        `CITYCD` varchar(7) DEFAULT NULL,
                        `CITYNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CITYVAR1` varchar(35) DEFAULT NULL,
                        `CITYVAR2` varchar(35) DEFAULT NULL,
                        `CITYVAR3` varchar(35) DEFAULT NULL,
                        `CITYVAR4` varchar(35) DEFAULT NULL,
                        `CITYVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CLUSTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CLUSTER` (
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `CLUNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `CLUVAR1` varchar(35) DEFAULT NULL,
                        `CLUVAR2` varchar(35) DEFAULT NULL,
                        `CLUVAR3` varchar(35) DEFAULT NULL,
                        `CLUVAR4` varchar(35) DEFAULT NULL,
                        `CLUVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CONSTITUENCY` (
                        `CONSTCD` varchar(7) DEFAULT NULL,
                        `CONSTNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITYID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNFACILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNFACILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `TOT_B_SEC` int(11) DEFAULT NULL,
                        `TOT_G_SEC` int(11) DEFAULT NULL,
                        `TOT_B_HSEC` int(11) DEFAULT NULL,
                        `TOT_G_HSEC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CYCLE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CYCLE` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_BLINDB` int(11) DEFAULT NULL,
                        `C1_BLINDG` int(11) DEFAULT NULL,
                        `C1_DEAFB` int(11) DEFAULT NULL,
                        `C1_DEAFG` int(11) DEFAULT NULL,
                        `C1_ORTHOB` int(11) DEFAULT NULL,
                        `C1_ORTHOG` int(11) DEFAULT NULL,
                        `C1_MENTALB` int(11) DEFAULT NULL,
                        `C1_MENTALG` int(11) DEFAULT NULL,
                        `C1_OTHB` int(11) DEFAULT NULL,
                        `C1_OTHG` int(11) DEFAULT NULL,
                        `C2_BLINDB` int(11) DEFAULT NULL,
                        `C2_BLINDG` int(11) DEFAULT NULL,
                        `C2_DEAFB` int(11) DEFAULT NULL,
                        `C2_DEAFG` int(11) DEFAULT NULL,
                        `C2_ORTHOB` int(11) DEFAULT NULL,
                        `C2_ORTHOG` int(11) DEFAULT NULL,
                        `C2_MENTALB` int(11) DEFAULT NULL,
                        `C2_MENTALG` int(11) DEFAULT NULL,
                        `C2_OTHB` int(11) DEFAULT NULL,
                        `C2_OTHG` int(11) DEFAULT NULL,
                        `C3_BLINDB` int(11) DEFAULT NULL,
                        `C3_BLINDG` int(11) DEFAULT NULL,
                        `C3_DEAFB` int(11) DEFAULT NULL,
                        `C3_DEAFG` int(11) DEFAULT NULL,
                        `C3_ORTHOB` int(11) DEFAULT NULL,
                        `C3_ORTHOG` int(11) DEFAULT NULL,
                        `C3_MENTALB` int(11) DEFAULT NULL,
                        `C3_MENTALG` int(11) DEFAULT NULL,
                        `C3_OTHB` int(11) DEFAULT NULL,
                        `C3_OTHG` int(11) DEFAULT NULL,
                        `C4_BLINDB` int(11) DEFAULT NULL,
                        `C4_BLINDG` int(11) DEFAULT NULL,
                        `C4_DEAFB` int(11) DEFAULT NULL,
                        `C4_DEAFG` int(11) DEFAULT NULL,
                        `C4_ORTHOB` int(11) DEFAULT NULL,
                        `C4_ORTHOG` int(11) DEFAULT NULL,
                        `C4_MENTALB` int(11) DEFAULT NULL,
                        `C4_MENTALG` int(11) DEFAULT NULL,
                        `C4_OTHB` int(11) DEFAULT NULL,
                        `C4_OTHG` int(11) DEFAULT NULL,
                        `C5_BLINDB` int(11) DEFAULT NULL,
                        `C5_BLINDG` int(11) DEFAULT NULL,
                        `C5_DEAFB` int(11) DEFAULT NULL,
                        `C5_DEAFG` int(11) DEFAULT NULL,
                        `C5_ORTHOB` int(11) DEFAULT NULL,
                        `C5_ORTHOG` int(11) DEFAULT NULL,
                        `C5_MENTALB` int(11) DEFAULT NULL,
                        `C5_MENTALG` int(11) DEFAULT NULL,
                        `C5_OTHB` int(11) DEFAULT NULL,
                        `C5_OTHG` int(11) DEFAULT NULL,
                        `C6_BLINDB` int(11) DEFAULT NULL,
                        `C6_BLINDG` int(11) DEFAULT NULL,
                        `C6_DEAFB` int(11) DEFAULT NULL,
                        `C6_DEAFG` int(11) DEFAULT NULL,
                        `C6_ORTHOB` int(11) DEFAULT NULL,
                        `C6_ORTHOG` int(11) DEFAULT NULL,
                        `C6_MENTALB` int(11) DEFAULT NULL,
                        `C6_MENTALG` int(11) DEFAULT NULL,
                        `C6_OTHB` int(11) DEFAULT NULL,
                        `C6_OTHG` int(11) DEFAULT NULL,
                        `C7_BLINDB` int(11) DEFAULT NULL,
                        `C7_BLINDG` int(11) DEFAULT NULL,
                        `C7_DEAFB` int(11) DEFAULT NULL,
                        `C7_DEAFG` int(11) DEFAULT NULL,
                        `C7_ORTHOB` int(11) DEFAULT NULL,
                        `C7_ORTHOG` int(11) DEFAULT NULL,
                        `C7_MENTALB` int(11) DEFAULT NULL,
                        `C7_MENTALG` int(11) DEFAULT NULL,
                        `C7_OTHB` int(11) DEFAULT NULL,
                        `C7_OTHG` int(11) DEFAULT NULL,
                        `C8_BLINDB` int(11) DEFAULT NULL,
                        `C8_BLINDG` int(11) DEFAULT NULL,
                        `C8_DEAFB` int(11) DEFAULT NULL,
                        `C8_DEAFG` int(11) DEFAULT NULL,
                        `C8_ORTHOB` int(11) DEFAULT NULL,
                        `C8_ORTHOG` int(11) DEFAULT NULL,
                        `C8_MENTALB` int(11) DEFAULT NULL,
                        `C8_MENTALG` int(11) DEFAULT NULL,
                        `C8_OTHB` int(11) DEFAULT NULL,
                        `C8_OTHG` int(11) DEFAULT NULL,
                        `C1_DUMBB` int(11) DEFAULT NULL,
                        `C1_DUMBG` int(11) DEFAULT NULL,
                        `C2_DUMBB` int(11) DEFAULT NULL,
                        `C2_DUMBG` int(11) DEFAULT NULL,
                        `C3_DUMBB` int(11) DEFAULT NULL,
                        `C3_DUMBG` int(11) DEFAULT NULL,
                        `C4_DUMBB` int(11) DEFAULT NULL,
                        `C4_DUMBG` int(11) DEFAULT NULL,
                        `C5_DUMBB` int(11) DEFAULT NULL,
                        `C5_DUMBG` int(11) DEFAULT NULL,
                        `C6_DUMBB` int(11) DEFAULT NULL,
                        `C6_DUMBG` int(11) DEFAULT NULL,
                        `C7_DUMBB` int(11) DEFAULT NULL,
                        `C7_DUMBG` int(11) DEFAULT NULL,
                        `C8_DUMBB` int(11) DEFAULT NULL,
                        `C8_DUMBG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1MULTIB` int(11) DEFAULT NULL,
                        `C1MULTIG` int(11) DEFAULT NULL,
                        `C2MULTIB` int(11) DEFAULT NULL,
                        `C2MULTIG` int(11) DEFAULT NULL,
                        `C3MULTIB` int(11) DEFAULT NULL,
                        `C3MULTIG` int(11) DEFAULT NULL,
                        `C4MULTIB` int(11) DEFAULT NULL,
                        `C4MULTIG` int(11) DEFAULT NULL,
                        `C5MULTIB` int(11) DEFAULT NULL,
                        `C5MULTIG` int(11) DEFAULT NULL,
                        `C6MULTIB` int(11) DEFAULT NULL,
                        `C6MULTIG` int(11) DEFAULT NULL,
                        `C7MULTIB` int(11) DEFAULT NULL,
                        `C7MULTIG` int(11) DEFAULT NULL,
                        `C8MULTIB` int(11) DEFAULT NULL,
                        `C8MULTIG` int(11) DEFAULT NULL,
                        `C9MULTIB` int(11) DEFAULT NULL,
                        `C9MULTIG` int(11) DEFAULT NULL,
                        `C10MULTIB` int(11) DEFAULT NULL,
                        `C10MULTIG` int(11) DEFAULT NULL,
                        `C11MULTIB` int(11) DEFAULT NULL,
                        `C11MULTIG` int(11) DEFAULT NULL,
                        `C12MULTIB` int(11) DEFAULT NULL,
                        `C12MULTIG` int(11) DEFAULT NULL,
                        `C9BLINDB` int(11) DEFAULT NULL,
                        `C9BLINDG` int(11) DEFAULT NULL,
                        `C9DEAFB` int(11) DEFAULT NULL,
                        `C9DEAFG` int(11) DEFAULT NULL,
                        `C9DUMBB` int(11) DEFAULT NULL,
                        `C9DUMBG` int(11) DEFAULT NULL,
                        `C9ORTHOB` int(11) DEFAULT NULL,
                        `C9ORTHOG` int(11) DEFAULT NULL,
                        `C9MENTALB` int(11) DEFAULT NULL,
                        `C9MENTALG` int(11) DEFAULT NULL,
                        `C9OTHB` int(11) DEFAULT NULL,
                        `C9OTHG` int(11) DEFAULT NULL,
                        `C10BLINDB` int(11) DEFAULT NULL,
                        `C10BLINDG` int(11) DEFAULT NULL,
                        `C10DEAFB` int(11) DEFAULT NULL,
                        `C10DEAFG` int(11) DEFAULT NULL,
                        `C10DUMBB` int(11) DEFAULT NULL,
                        `C10DUMBG` int(11) DEFAULT NULL,
                        `C10ORTHOB` int(11) DEFAULT NULL,
                        `C10ORTHOG` int(11) DEFAULT NULL,
                        `C10MENTALB` int(11) DEFAULT NULL,
                        `C10MENTALG` int(11) DEFAULT NULL,
                        `C10OTHB` int(11) DEFAULT NULL,
                        `C10OTHG` int(11) DEFAULT NULL,
                        `C11BLINDB` int(11) DEFAULT NULL,
                        `C11BLINDG` int(11) DEFAULT NULL,
                        `C11DEAFB` int(11) DEFAULT NULL,
                        `C11DEAFG` int(11) DEFAULT NULL,
                        `C11DUMBB` int(11) DEFAULT NULL,
                        `C11DUMBG` int(11) DEFAULT NULL,
                        `C11ORTHOB` int(11) DEFAULT NULL,
                        `C11ORTHOG` int(11) DEFAULT NULL,
                        `C11MENTALB` int(11) DEFAULT NULL,
                        `C11MENTALG` int(11) DEFAULT NULL,
                        `C11OTHB` int(11) DEFAULT NULL,
                        `C11OTHG` int(11) DEFAULT NULL,
                        `C12BLINDB` int(11) DEFAULT NULL,
                        `C12BLINDG` int(11) DEFAULT NULL,
                        `C12DEAFB` int(11) DEFAULT NULL,
                        `C12DEAFG` int(11) DEFAULT NULL,
                        `C12DUMBB` int(11) DEFAULT NULL,
                        `C12DUMBG` int(11) DEFAULT NULL,
                        `C12ORTHOB` int(11) DEFAULT NULL,
                        `C12ORTHOG` int(11) DEFAULT NULL,
                        `C12MENTALB` int(11) DEFAULT NULL,
                        `C12MENTALG` int(11) DEFAULT NULL,
                        `C12OTHB` int(11) DEFAULT NULL,
                        `C12OTHG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTLIST` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTRICT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTRICT` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `DISTVAR1` varchar(35) DEFAULT NULL,
                        `DISTVAR2` varchar(35) DEFAULT NULL,
                        `DISTVAR3` varchar(35) DEFAULT NULL,
                        `DISTVAR4` varchar(35) DEFAULT NULL,
                        `DISTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EDUBLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EDUBLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `BLK_TRIB` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRAGEBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRAGEBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGEID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRBYCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRBYCASTEMEDIUM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUMID` int(11) DEFAULT NULL,
                        `CASTEIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `LKG_B` int(11) DEFAULT NULL,
                        `LKG_G` int(11) DEFAULT NULL,
                        `UKG_B` int(11) DEFAULT NULL,
                        `UKG_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(6) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C1GB` int(11) DEFAULT NULL,
                        `C1GG` int(11) DEFAULT NULL,
                        `C2GB` int(11) DEFAULT NULL,
                        `C2GG` int(11) DEFAULT NULL,
                        `C3GB` int(11) DEFAULT NULL,
                        `C3GG` int(11) DEFAULT NULL,
                        `C4GB` int(11) DEFAULT NULL,
                        `C4GG` int(11) DEFAULT NULL,
                        `C5GB` int(11) DEFAULT NULL,
                        `C5GG` int(11) DEFAULT NULL,
                        `C6GB` int(11) DEFAULT NULL,
                        `C6GG` int(11) DEFAULT NULL,
                        `C7GB` int(11) DEFAULT NULL,
                        `C7GG` int(11) DEFAULT NULL,
                        `C8GB` int(11) DEFAULT NULL,
                        `C8GG` int(11) DEFAULT NULL,
                        `C1CB` int(11) DEFAULT NULL,
                        `C1CG` int(11) DEFAULT NULL,
                        `C2CB` int(11) DEFAULT NULL,
                        `C2CG` int(11) DEFAULT NULL,
                        `C3CB` int(11) DEFAULT NULL,
                        `C3CG` int(11) DEFAULT NULL,
                        `C4CB` int(11) DEFAULT NULL,
                        `C4CG` int(11) DEFAULT NULL,
                        `C5CB` int(11) DEFAULT NULL,
                        `C5CG` int(11) DEFAULT NULL,
                        `C6CB` int(11) DEFAULT NULL,
                        `C6CG` int(11) DEFAULT NULL,
                        `C7CB` int(11) DEFAULT NULL,
                        `C7CG` int(11) DEFAULT NULL,
                        `C8CB` int(11) DEFAULT NULL,
                        `C8CG` int(11) DEFAULT NULL,
                        `C1TB` int(11) DEFAULT NULL,
                        `C1TG` int(11) DEFAULT NULL,
                        `C2TB` int(11) DEFAULT NULL,
                        `C2TG` int(11) DEFAULT NULL,
                        `C3TB` int(11) DEFAULT NULL,
                        `C3TG` int(11) DEFAULT NULL,
                        `C4TB` int(11) DEFAULT NULL,
                        `C4TG` int(11) DEFAULT NULL,
                        `C5TB` int(11) DEFAULT NULL,
                        `C5TG` int(11) DEFAULT NULL,
                        `C6TB` int(11) DEFAULT NULL,
                        `C6TG` int(11) DEFAULT NULL,
                        `C7TB` int(11) DEFAULT NULL,
                        `C7TG` int(11) DEFAULT NULL,
                        `C8TB` int(11) DEFAULT NULL,
                        `C8TG` int(11) DEFAULT NULL,
                        `C1OB` int(11) DEFAULT NULL,
                        `C1OG` int(11) DEFAULT NULL,
                        `C2OB` int(11) DEFAULT NULL,
                        `C2OG` int(11) DEFAULT NULL,
                        `C3OB` int(11) DEFAULT NULL,
                        `C3OG` int(11) DEFAULT NULL,
                        `C4OB` int(11) DEFAULT NULL,
                        `C4OG` int(11) DEFAULT NULL,
                        `C5OB` int(11) DEFAULT NULL,
                        `C5OG` int(11) DEFAULT NULL,
                        `C6OB` int(11) DEFAULT NULL,
                        `C6OG` int(11) DEFAULT NULL,
                        `C7OB` int(11) DEFAULT NULL,
                        `C7OG` int(11) DEFAULT NULL,
                        `C8OB` int(11) DEFAULT NULL,
                        `C8OG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `C1_TOTB` int(11) DEFAULT NULL,
                        `C1_TOTG` int(11) DEFAULT NULL,
                        `C2_TOTB` int(11) DEFAULT NULL,
                        `C2_TOTG` int(11) DEFAULT NULL,
                        `C3_TOTB` int(11) DEFAULT NULL,
                        `C3_TOTG` int(11) DEFAULT NULL,
                        `C4_TOTB` int(11) DEFAULT NULL,
                        `C4_TOTG` int(11) DEFAULT NULL,
                        `C5_TOTB` int(11) DEFAULT NULL,
                        `C5_TOTG` int(11) DEFAULT NULL,
                        `C6_TOTB` int(11) DEFAULT NULL,
                        `C6_TOTG` int(11) DEFAULT NULL,
                        `C7_TOTB` int(11) DEFAULT NULL,
                        `C7_TOTG` int(11) DEFAULT NULL,
                        `C8_TOTB` int(11) DEFAULT NULL,
                        `C8_TOTG` int(11) DEFAULT NULL,
                        `CPP_TOTB` int(11) DEFAULT NULL,
                        `CPP_TOTG` int(11) DEFAULT NULL,
                        `C9_TOTB` int(11) DEFAULT NULL,
                        `C9_TOTG` int(11) DEFAULT NULL,
                        `C10_TOTB` int(11) DEFAULT NULL,
                        `C10_TOTG` int(11) DEFAULT NULL,
                        `C11_TOTB` int(11) DEFAULT NULL,
                        `C11_TOTG` int(11) DEFAULT NULL,
                        `C12_TOTB` int(11) DEFAULT NULL,
                        `C12_TOTG` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_L5B` int(11) DEFAULT NULL,
                        `C1_5B` int(11) DEFAULT NULL,
                        `C1_6B` int(11) DEFAULT NULL,
                        `C1_7B` int(11) DEFAULT NULL,
                        `C1_8B` int(11) DEFAULT NULL,
                        `C1_9B` int(11) DEFAULT NULL,
                        `C1_10B` int(11) DEFAULT NULL,
                        `C1_11B` int(11) DEFAULT NULL,
                        `C1_12B` int(11) DEFAULT NULL,
                        `C1_L5G` int(11) DEFAULT NULL,
                        `C1_5G` int(11) DEFAULT NULL,
                        `C1_6G` int(11) DEFAULT NULL,
                        `C1_7G` int(11) DEFAULT NULL,
                        `C1_8G` int(11) DEFAULT NULL,
                        `C1_9G` int(11) DEFAULT NULL,
                        `C1_10G` int(11) DEFAULT NULL,
                        `C1_11G` int(11) DEFAULT NULL,
                        `C1_12G` int(11) DEFAULT NULL,
                        `C2_5B` int(11) DEFAULT NULL,
                        `C2_6B` int(11) DEFAULT NULL,
                        `C2_7B` int(11) DEFAULT NULL,
                        `C2_8B` int(11) DEFAULT NULL,
                        `C2_9B` int(11) DEFAULT NULL,
                        `C2_10B` int(11) DEFAULT NULL,
                        `C2_11B` int(11) DEFAULT NULL,
                        `C2_12B` int(11) DEFAULT NULL,
                        `C2_13B` int(11) DEFAULT NULL,
                        `C2_5G` int(11) DEFAULT NULL,
                        `C2_6G` int(11) DEFAULT NULL,
                        `C2_7G` int(11) DEFAULT NULL,
                        `C2_8G` int(11) DEFAULT NULL,
                        `C2_9G` int(11) DEFAULT NULL,
                        `C2_10G` int(11) DEFAULT NULL,
                        `C2_11G` int(11) DEFAULT NULL,
                        `C2_12G` int(11) DEFAULT NULL,
                        `C2_13G` int(11) DEFAULT NULL,
                        `C3_7B` int(11) DEFAULT NULL,
                        `C3_8B` int(11) DEFAULT NULL,
                        `C3_9B` int(11) DEFAULT NULL,
                        `C3_10B` int(11) DEFAULT NULL,
                        `C3_11B` int(11) DEFAULT NULL,
                        `C3_12B` int(11) DEFAULT NULL,
                        `C3_13B` int(11) DEFAULT NULL,
                        `C3_14B` int(11) DEFAULT NULL,
                        `C3_15B` int(11) DEFAULT NULL,
                        `C3_16B` int(11) DEFAULT NULL,
                        `C3_M16B` int(11) DEFAULT NULL,
                        `C3_7G` int(11) DEFAULT NULL,
                        `C3_8G` int(11) DEFAULT NULL,
                        `C3_9G` int(11) DEFAULT NULL,
                        `C3_10G` int(11) DEFAULT NULL,
                        `C3_11G` int(11) DEFAULT NULL,
                        `C3_12G` int(11) DEFAULT NULL,
                        `C3_13G` int(11) DEFAULT NULL,
                        `C3_14G` int(11) DEFAULT NULL,
                        `C3_15G` int(11) DEFAULT NULL,
                        `C3_16G` int(11) DEFAULT NULL,
                        `C3_M16G` int(11) DEFAULT NULL,
                        `C4_8B` int(11) DEFAULT NULL,
                        `C4_9B` int(11) DEFAULT NULL,
                        `C4_10B` int(11) DEFAULT NULL,
                        `C4_11B` int(11) DEFAULT NULL,
                        `C4_12B` int(11) DEFAULT NULL,
                        `C4_13B` int(11) DEFAULT NULL,
                        `C4_14B` int(11) DEFAULT NULL,
                        `C4_15B` int(11) DEFAULT NULL,
                        `C4_16B` int(11) DEFAULT NULL,
                        `C4_M16B` int(11) DEFAULT NULL,
                        `C4_8G` int(11) DEFAULT NULL,
                        `C4_9G` int(11) DEFAULT NULL,
                        `C4_10G` int(11) DEFAULT NULL,
                        `C4_11G` int(11) DEFAULT NULL,
                        `C4_12G` int(11) DEFAULT NULL,
                        `C4_13G` int(11) DEFAULT NULL,
                        `C4_14G` int(11) DEFAULT NULL,
                        `C4_15G` int(11) DEFAULT NULL,
                        `C4_16G` int(11) DEFAULT NULL,
                        `C4_M16G` int(11) DEFAULT NULL,
                        `C5_9B` int(11) DEFAULT NULL,
                        `C5_10B` int(11) DEFAULT NULL,
                        `C5_11B` int(11) DEFAULT NULL,
                        `C5_12B` int(11) DEFAULT NULL,
                        `C5_13B` int(11) DEFAULT NULL,
                        `C5_14B` int(11) DEFAULT NULL,
                        `C5_15B` int(11) DEFAULT NULL,
                        `C5_16B` int(11) DEFAULT NULL,
                        `C5_M16B` int(11) DEFAULT NULL,
                        `C5_9G` int(11) DEFAULT NULL,
                        `C5_10G` int(11) DEFAULT NULL,
                        `C5_11G` int(11) DEFAULT NULL,
                        `C5_12G` int(11) DEFAULT NULL,
                        `C5_13G` int(11) DEFAULT NULL,
                        `C5_14G` int(11) DEFAULT NULL,
                        `C5_15G` int(11) DEFAULT NULL,
                        `C5_16G` int(11) DEFAULT NULL,
                        `C5_M16G` int(11) DEFAULT NULL,
                        `C6_10B` int(11) DEFAULT NULL,
                        `C6_11B` int(11) DEFAULT NULL,
                        `C6_12B` int(11) DEFAULT NULL,
                        `C6_13B` int(11) DEFAULT NULL,
                        `C6_14B` int(11) DEFAULT NULL,
                        `C6_15B` int(11) DEFAULT NULL,
                        `C6_16B` int(11) DEFAULT NULL,
                        `C6_M16B` int(11) DEFAULT NULL,
                        `C6_10G` int(11) DEFAULT NULL,
                        `C6_11G` int(11) DEFAULT NULL,
                        `C6_12G` int(11) DEFAULT NULL,
                        `C6_13G` int(11) DEFAULT NULL,
                        `C6_14G` int(11) DEFAULT NULL,
                        `C6_15G` int(11) DEFAULT NULL,
                        `C6_16G` int(11) DEFAULT NULL,
                        `C6_M16G` int(11) DEFAULT NULL,
                        `C7_11B` int(11) DEFAULT NULL,
                        `C7_12B` int(11) DEFAULT NULL,
                        `C7_13B` int(11) DEFAULT NULL,
                        `C7_14B` int(11) DEFAULT NULL,
                        `C7_15B` int(11) DEFAULT NULL,
                        `C7_16B` int(11) DEFAULT NULL,
                        `C7_M16B` int(11) DEFAULT NULL,
                        `C7_11G` int(11) DEFAULT NULL,
                        `C7_12G` int(11) DEFAULT NULL,
                        `C7_13G` int(11) DEFAULT NULL,
                        `C7_14G` int(11) DEFAULT NULL,
                        `C7_15G` int(11) DEFAULT NULL,
                        `C7_16G` int(11) DEFAULT NULL,
                        `C7_M16G` int(11) DEFAULT NULL,
                        `C8_12B` int(11) DEFAULT NULL,
                        `C8_13B` int(11) DEFAULT NULL,
                        `C8_14B` int(11) DEFAULT NULL,
                        `C8_15B` int(11) DEFAULT NULL,
                        `C8_16B` int(11) DEFAULT NULL,
                        `C8_M16B` int(11) DEFAULT NULL,
                        `C8_12G` int(11) DEFAULT NULL,
                        `C8_13G` int(11) DEFAULT NULL,
                        `C8_14G` int(11) DEFAULT NULL,
                        `C8_15G` int(11) DEFAULT NULL,
                        `C8_16G` int(11) DEFAULT NULL,
                        `C8_M16G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PPB_L5` int(11) DEFAULT NULL,
                        `PPG_L5` int(11) DEFAULT NULL,
                        `PPB_5` int(11) DEFAULT NULL,
                        `PPG_5` int(11) DEFAULT NULL,
                        `PPB_6` int(11) DEFAULT NULL,
                        `PPG_6` int(11) DEFAULT NULL,
                        `PPB_7` int(11) DEFAULT NULL,
                        `PPG_7` int(11) DEFAULT NULL,
                        `PPB_8` int(11) DEFAULT NULL,
                        `PPG_8` int(11) DEFAULT NULL,
                        `PPB_9` int(11) DEFAULT NULL,
                        `PPG_9` int(11) DEFAULT NULL,
                        `PPB_10` int(11) DEFAULT NULL,
                        `PPG_10` int(11) DEFAULT NULL,
                        `PPB_11` int(11) DEFAULT NULL,
                        `PPG_11` int(11) DEFAULT NULL,
                        `PPB_12` int(11) DEFAULT NULL,
                        `PPG_12` int(11) DEFAULT NULL,
                        `C9B_13` int(11) DEFAULT NULL,
                        `C9G_13` int(11) DEFAULT NULL,
                        `C9B_14` int(11) DEFAULT NULL,
                        `C9G_14` int(11) DEFAULT NULL,
                        `C9B_M14` int(11) DEFAULT NULL,
                        `C9G_M14` int(11) DEFAULT NULL,
                        `C9B_16` int(11) DEFAULT NULL,
                        `C9G_16` int(11) DEFAULT NULL,
                        `C9B_M16` int(11) DEFAULT NULL,
                        `C9G_M16` int(11) DEFAULT NULL,
                        `C10B_14` int(11) DEFAULT NULL,
                        `C10G_14` int(11) DEFAULT NULL,
                        `C10B_M14` int(11) DEFAULT NULL,
                        `C10G_M14` int(11) DEFAULT NULL,
                        `C10B_16` int(11) DEFAULT NULL,
                        `C10G_16` int(11) DEFAULT NULL,
                        `C10B_M16` int(11) DEFAULT NULL,
                        `C10G_M16` int(11) DEFAULT NULL,
                        `C11B_M14` int(11) DEFAULT NULL,
                        `C11G_M14` int(11) DEFAULT NULL,
                        `C11B_16` int(11) DEFAULT NULL,
                        `C11G_16` int(11) DEFAULT NULL,
                        `C11B_M16` int(11) DEFAULT NULL,
                        `C11G_M16` int(11) DEFAULT NULL,
                        `C12B_16` int(11) DEFAULT NULL,
                        `C12G_16` int(11) DEFAULT NULL,
                        `C12B_M16` int(11) DEFAULT NULL,
                        `C12G_M16` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C1_GB` int(11) DEFAULT NULL,
                        `C1_GG` int(11) DEFAULT NULL,
                        `C2_GB` int(11) DEFAULT NULL,
                        `C2_GG` int(11) DEFAULT NULL,
                        `C3_GB` int(11) DEFAULT NULL,
                        `C3_GG` int(11) DEFAULT NULL,
                        `C4_GB` int(11) DEFAULT NULL,
                        `C4_GG` int(11) DEFAULT NULL,
                        `C5_GB` int(11) DEFAULT NULL,
                        `C5_GG` int(11) DEFAULT NULL,
                        `C6_GB` int(11) DEFAULT NULL,
                        `C6_GG` int(11) DEFAULT NULL,
                        `C7_GB` int(11) DEFAULT NULL,
                        `C7_GG` int(11) DEFAULT NULL,
                        `C8_GB` int(11) DEFAULT NULL,
                        `C8_GG` int(11) DEFAULT NULL,
                        `C1_CB` int(11) DEFAULT NULL,
                        `C1_CG` int(11) DEFAULT NULL,
                        `C2_CB` int(11) DEFAULT NULL,
                        `C2_CG` int(11) DEFAULT NULL,
                        `C3_CB` int(11) DEFAULT NULL,
                        `C3_CG` int(11) DEFAULT NULL,
                        `C4_CB` int(11) DEFAULT NULL,
                        `C4_CG` int(11) DEFAULT NULL,
                        `C5_CB` int(11) DEFAULT NULL,
                        `C5_CG` int(11) DEFAULT NULL,
                        `C6_CB` int(11) DEFAULT NULL,
                        `C6_CG` int(11) DEFAULT NULL,
                        `C7_CB` int(11) DEFAULT NULL,
                        `C7_CG` int(11) DEFAULT NULL,
                        `C8_CB` int(11) DEFAULT NULL,
                        `C8_CG` int(11) DEFAULT NULL,
                        `C1_TB` int(11) DEFAULT NULL,
                        `C1_TG` int(11) DEFAULT NULL,
                        `C2_TB` int(11) DEFAULT NULL,
                        `C2_TG` int(11) DEFAULT NULL,
                        `C3_TB` int(11) DEFAULT NULL,
                        `C3_TG` int(11) DEFAULT NULL,
                        `C4_TB` int(11) DEFAULT NULL,
                        `C4_TG` int(11) DEFAULT NULL,
                        `C5_TB` int(11) DEFAULT NULL,
                        `C5_TG` int(11) DEFAULT NULL,
                        `C6_TB` int(11) DEFAULT NULL,
                        `C6_TG` int(11) DEFAULT NULL,
                        `C7_TB` int(11) DEFAULT NULL,
                        `C7_TG` int(11) DEFAULT NULL,
                        `C8_TB` int(11) DEFAULT NULL,
                        `C8_TG` int(11) DEFAULT NULL,
                        `C1_OB` int(11) DEFAULT NULL,
                        `C1_OG` int(11) DEFAULT NULL,
                        `C2_OB` int(11) DEFAULT NULL,
                        `C2_OG` int(11) DEFAULT NULL,
                        `C3_OB` int(11) DEFAULT NULL,
                        `C3_OG` int(11) DEFAULT NULL,
                        `C4_OB` int(11) DEFAULT NULL,
                        `C4_OG` int(11) DEFAULT NULL,
                        `C5_OB` int(11) DEFAULT NULL,
                        `C5_OG` int(11) DEFAULT NULL,
                        `C6_OB` int(11) DEFAULT NULL,
                        `C6_OG` int(11) DEFAULT NULL,
                        `C7_OB` int(11) DEFAULT NULL,
                        `C7_OG` int(11) DEFAULT NULL,
                        `C8_OB` int(11) DEFAULT NULL,
                        `C8_OG` int(11) DEFAULT NULL,
                        `C1_OTH1B` int(11) DEFAULT NULL,
                        `C1_OTH1G` int(11) DEFAULT NULL,
                        `C2_OTH1B` int(11) DEFAULT NULL,
                        `C2_OTH1G` int(11) DEFAULT NULL,
                        `C3_OTH1B` int(11) DEFAULT NULL,
                        `C3_OTH1G` int(11) DEFAULT NULL,
                        `C4_OTH1B` int(11) DEFAULT NULL,
                        `C4_OTH1G` int(11) DEFAULT NULL,
                        `C5_OTH1B` int(11) DEFAULT NULL,
                        `C5_OTH1G` int(11) DEFAULT NULL,
                        `C6_OTH1B` int(11) DEFAULT NULL,
                        `C6_OTH1G` int(11) DEFAULT NULL,
                        `C7_OTH1B` int(11) DEFAULT NULL,
                        `C7_OTH1G` int(11) DEFAULT NULL,
                        `C8_OTH1B` int(11) DEFAULT NULL,
                        `C8_OTH1G` int(11) DEFAULT NULL,
                        `C1_OTH2B` int(11) DEFAULT NULL,
                        `C1_OTH2G` int(11) DEFAULT NULL,
                        `C2_OTH2B` int(11) DEFAULT NULL,
                        `C2_OTH2G` int(11) DEFAULT NULL,
                        `C3_OTH2B` int(11) DEFAULT NULL,
                        `C3_OTH2G` int(11) DEFAULT NULL,
                        `C4_OTH2B` int(11) DEFAULT NULL,
                        `C4_OTH2G` int(11) DEFAULT NULL,
                        `C5_OTH2B` int(11) DEFAULT NULL,
                        `C5_OTH2G` int(11) DEFAULT NULL,
                        `C6_OTH2B` int(11) DEFAULT NULL,
                        `C6_OTH2G` int(11) DEFAULT NULL,
                        `C7_OTH2B` int(11) DEFAULT NULL,
                        `C7_OTH2G` int(11) DEFAULT NULL,
                        `C8_OTH2B` int(11) DEFAULT NULL,
                        `C8_OTH2G` int(11) DEFAULT NULL,
                        `C1_OTH3B` int(11) DEFAULT NULL,
                        `C1_OTH3G` int(11) DEFAULT NULL,
                        `C2_OTH3B` int(11) DEFAULT NULL,
                        `C2_OTH3G` int(11) DEFAULT NULL,
                        `C3_OTH3B` int(11) DEFAULT NULL,
                        `C3_OTH3G` int(11) DEFAULT NULL,
                        `C4_OTH3B` int(11) DEFAULT NULL,
                        `C4_OTH3G` int(11) DEFAULT NULL,
                        `C5_OTH3B` int(11) DEFAULT NULL,
                        `C5_OTH3G` int(11) DEFAULT NULL,
                        `C6_OTH3B` int(11) DEFAULT NULL,
                        `C6_OTH3G` int(11) DEFAULT NULL,
                        `C7_OTH3B` int(11) DEFAULT NULL,
                        `C7_OTH3G` int(11) DEFAULT NULL,
                        `C8_OTH3B` int(11) DEFAULT NULL,
                        `C8_OTH3G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `V1CPPB` int(11) DEFAULT NULL,
                        `V1CPPG` int(11) DEFAULT NULL,
                        `V2CPPB` int(11) DEFAULT NULL,
                        `V2CPPG` int(11) DEFAULT NULL,
                        `V3CPPB` int(11) DEFAULT NULL,
                        `V3CPPG` int(11) DEFAULT NULL,
                        `V1C9B` int(11) DEFAULT NULL,
                        `V1C9G` int(11) DEFAULT NULL,
                        `V2C9B` int(11) DEFAULT NULL,
                        `V2C9G` int(11) DEFAULT NULL,
                        `V3C9B` int(11) DEFAULT NULL,
                        `V3C9G` int(11) DEFAULT NULL,
                        `V1C10B` int(11) DEFAULT NULL,
                        `V1C10G` int(11) DEFAULT NULL,
                        `V2C10B` int(11) DEFAULT NULL,
                        `V2C10G` int(11) DEFAULT NULL,
                        `V3C10B` int(11) DEFAULT NULL,
                        `V3C10G` int(11) DEFAULT NULL,
                        `V1C11B` int(11) DEFAULT NULL,
                        `V1C11G` int(11) DEFAULT NULL,
                        `V2C11B` int(11) DEFAULT NULL,
                        `V2C11G` int(11) DEFAULT NULL,
                        `V3C11B` int(11) DEFAULT NULL,
                        `V3C11G` int(11) DEFAULT NULL,
                        `V1C12B` int(11) DEFAULT NULL,
                        `V1C12G` int(11) DEFAULT NULL,
                        `V2C12B` int(11) DEFAULT NULL,
                        `V2C12G` int(11) DEFAULT NULL,
                        `V3C12B` int(11) DEFAULT NULL,
                        `V3C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `ENTITY_ID` int(11) DEFAULT NULL,
                        `ENTITY_DESC` varchar(75) DEFAULT NULL,
                        `ENTITY_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYTABLEMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYTABLEMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `TABLE_DESC` varchar(75) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EREPBYSTREAM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EREPBYSTREAM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STREAMID` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `EC11_B` int(11) DEFAULT NULL,
                        `EC11_G` int(11) DEFAULT NULL,
                        `EC12_B` int(11) DEFAULT NULL,
                        `EC12_G` int(11) DEFAULT NULL,
                        `RC11_B` int(11) DEFAULT NULL,
                        `RC11_G` int(11) DEFAULT NULL,
                        `RC12_B` int(11) DEFAULT NULL,
                        `RC12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMINATION09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMINATION09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C5B_GEN` int(11) DEFAULT NULL,
                        `C5G_GEN` int(11) DEFAULT NULL,
                        `C8B_GEN` int(11) DEFAULT NULL,
                        `C8G_GEN` int(11) DEFAULT NULL,
                        `C5B_SC` int(11) DEFAULT NULL,
                        `C5G_SC` int(11) DEFAULT NULL,
                        `C8B_SC` int(11) DEFAULT NULL,
                        `C8G_SC` int(11) DEFAULT NULL,
                        `C5B_ST` int(11) DEFAULT NULL,
                        `C5G_ST` int(11) DEFAULT NULL,
                        `C8B_ST` int(11) DEFAULT NULL,
                        `C8G_ST` int(11) DEFAULT NULL,
                        `C5B_OBC` int(11) DEFAULT NULL,
                        `C5G_OBC` int(11) DEFAULT NULL,
                        `C8B_OBC` int(11) DEFAULT NULL,
                        `C8G_OBC` int(11) DEFAULT NULL,
                        `C5B_MIN` int(11) DEFAULT NULL,
                        `C5G_MIN` int(11) DEFAULT NULL,
                        `C8B_MIN` int(11) DEFAULT NULL,
                        `C8G_MIN` int(11) DEFAULT NULL,
                        `C10B_GEN` int(11) DEFAULT NULL,
                        `C10G_GEN` int(11) DEFAULT NULL,
                        `C12B_GEN` int(11) DEFAULT NULL,
                        `C12G_GEN` int(11) DEFAULT NULL,
                        `C10B_SC` int(11) DEFAULT NULL,
                        `C10G_SC` int(11) DEFAULT NULL,
                        `C12B_SC` int(11) DEFAULT NULL,
                        `C12G_SC` int(11) DEFAULT NULL,
                        `C10B_ST` int(11) DEFAULT NULL,
                        `C10G_ST` int(11) DEFAULT NULL,
                        `C12B_ST` int(11) DEFAULT NULL,
                        `C12G_ST` int(11) DEFAULT NULL,
                        `C10B_OBC` int(11) DEFAULT NULL,
                        `C10G_OBC` int(11) DEFAULT NULL,
                        `C12B_OBC` int(11) DEFAULT NULL,
                        `C12G_OBC` int(11) DEFAULT NULL,
                        `C10B_MIN` int(11) DEFAULT NULL,
                        `C10G_MIN` int(11) DEFAULT NULL,
                        `C12B_MIN` int(11) DEFAULT NULL,
                        `C12G_MIN` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C5_ENROLB` int(11) DEFAULT NULL,
                        `C5_ENROLG` int(11) DEFAULT NULL,
                        `C7_ENROLB` int(11) DEFAULT NULL,
                        `C7_ENROLG` int(11) DEFAULT NULL,
                        `C5_APPEARB` int(11) DEFAULT NULL,
                        `C5_APPEARG` int(11) DEFAULT NULL,
                        `C7_APPEARB` int(11) DEFAULT NULL,
                        `C7_APPEARG` int(11) DEFAULT NULL,
                        `C5_PASSB` int(11) DEFAULT NULL,
                        `C5_PASSG` int(11) DEFAULT NULL,
                        `C7_PASSB` int(11) DEFAULT NULL,
                        `C7_PASSG` int(11) DEFAULT NULL,
                        `C5_M60B` int(11) DEFAULT NULL,
                        `C5_M60G` int(11) DEFAULT NULL,
                        `C7_M60B` int(11) DEFAULT NULL,
                        `C7_M60G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ENROLSC5B` int(11) DEFAULT NULL,
                        `ENROLSC5G` int(11) DEFAULT NULL,
                        `ENROLSC7B` int(11) DEFAULT NULL,
                        `ENROLSC7G` int(11) DEFAULT NULL,
                        `APPEARSC5B` int(11) DEFAULT NULL,
                        `APPEARSC5G` int(11) DEFAULT NULL,
                        `APPEARSC7B` int(11) DEFAULT NULL,
                        `APPEARSC7G` int(11) DEFAULT NULL,
                        `TPASSEDSC5B` int(11) DEFAULT NULL,
                        `TPASSEDSC5G` int(11) DEFAULT NULL,
                        `TPASSEDSC7B` int(11) DEFAULT NULL,
                        `TPASSEDSC7G` int(11) DEFAULT NULL,
                        `PASSM60SC5B` int(11) DEFAULT NULL,
                        `PASSM60SC5G` int(11) DEFAULT NULL,
                        `PASSM60SC7B` int(11) DEFAULT NULL,
                        `PASSM60SC7G` int(11) DEFAULT NULL,
                        `ENROLST5B` int(11) DEFAULT NULL,
                        `ENROLST5G` int(11) DEFAULT NULL,
                        `ENROLST7B` int(11) DEFAULT NULL,
                        `ENROLST7G` int(11) DEFAULT NULL,
                        `APPEARST5B` int(11) DEFAULT NULL,
                        `APPEARST5G` int(11) DEFAULT NULL,
                        `APPEARST7B` int(11) DEFAULT NULL,
                        `APPEARST7G` int(11) DEFAULT NULL,
                        `TPASSEDST5B` int(11) DEFAULT NULL,
                        `TPASSEDST5G` int(11) DEFAULT NULL,
                        `TPASSEDST7B` int(11) DEFAULT NULL,
                        `TPASSEDST7G` int(11) DEFAULT NULL,
                        `PASSM60ST5B` int(11) DEFAULT NULL,
                        `PASSM60ST5G` int(11) DEFAULT NULL,
                        `PASSM60ST7B` int(11) DEFAULT NULL,
                        `PASSM60ST7G` int(11) DEFAULT NULL,
                        `ENROL10B` int(11) DEFAULT NULL,
                        `ENROL10G` int(11) DEFAULT NULL,
                        `APPEAR10B` int(11) DEFAULT NULL,
                        `APPEAR10G` int(11) DEFAULT NULL,
                        `TPASSED10B` int(11) DEFAULT NULL,
                        `TPASSED10G` int(11) DEFAULT NULL,
                        `PASSM6010B` int(11) DEFAULT NULL,
                        `PASSM6010G` int(11) DEFAULT NULL,
                        `ENROL12B` int(11) DEFAULT NULL,
                        `ENROL12G` int(11) DEFAULT NULL,
                        `APPEAR12B` int(11) DEFAULT NULL,
                        `APPEAR12G` int(11) DEFAULT NULL,
                        `TPASSED12B` int(11) DEFAULT NULL,
                        `TPASSED12G` int(11) DEFAULT NULL,
                        `PASSM6012B` int(11) DEFAULT NULL,
                        `PASSM6012G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FACILITIES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FACILITIES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `ITEMVALUE` int(11) DEFAULT NULL,
                        `ITEMVALUE1` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FUNDS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FUNDS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `AMT_R` double DEFAULT NULL,
                        `AMT_U` double DEFAULT NULL,
                        `AMT_S` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_HABITATION`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_HABITATION` (
                        `HABCD` varchar(11) DEFAULT NULL,
                        `HABNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `HABVAR1` varchar(35) DEFAULT NULL,
                        `HABVAR2` varchar(35) DEFAULT NULL,
                        `HABVAR3` varchar(35) DEFAULT NULL,
                        `HABVAR4` varchar(35) DEFAULT NULL,
                        `HABVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_INCENTIVES09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_INCENTIVES09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `GRADEPRUPR` int(11) DEFAULT NULL,
                        `INCENT_TYPE` int(11) DEFAULT NULL,
                        `GEN_B` int(11) DEFAULT NULL,
                        `GEN_G` int(11) DEFAULT NULL,
                        `SC_B` int(11) DEFAULT NULL,
                        `SC_G` int(11) DEFAULT NULL,
                        `ST_B` int(11) DEFAULT NULL,
                        `ST_G` int(11) DEFAULT NULL,
                        `OBC_B` int(11) DEFAULT NULL,
                        `OBC_G` int(11) DEFAULT NULL,
                        `MIN_B` int(11) DEFAULT NULL,
                        `MIN_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_KEYVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_KEYVAR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MAPPING`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MAPPING` (
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHCD_DISE` varchar(11) DEFAULT NULL,
                        `SCHCD_SEMIS` varchar(11) DEFAULT NULL,
                        `MAPREMARKS` varchar(100) DEFAULT NULL,
                        `METHOD` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `DISTHQ` int(11) DEFAULT NULL,
                        `DISTCRC` int(11) DEFAULT NULL,
                        `ESTDYEAR` int(11) DEFAULT NULL,
                        `LOWCLASS` int(11) DEFAULT NULL,
                        `HIGHCLASS` int(11) DEFAULT NULL,
                        `PPSEC_YN` int(11) DEFAULT NULL,
                        `PPSTUDENT` int(11) DEFAULT NULL,
                        `PPTEACHER` int(11) DEFAULT NULL,
                        `WORKDAYS` int(11) DEFAULT NULL,
                        `SCHRES_YN` int(11) DEFAULT NULL,
                        `RESITYPE` int(11) DEFAULT NULL,
                        `SCHSHI_YN` int(11) DEFAULT NULL,
                        `NOINSPECT` int(11) DEFAULT NULL,
                        `VISITSCRC` int(11) DEFAULT NULL,
                        `VISITSBRC` int(11) DEFAULT NULL,
                        `CONTI_R` int(11) DEFAULT NULL,
                        `CONTI_E` int(11) DEFAULT NULL,
                        `TLM_R` int(11) DEFAULT NULL,
                        `TLM_E` int(11) DEFAULT NULL,
                        `FUNDS_E` int(11) DEFAULT NULL,
                        `FUNDS_R` int(11) DEFAULT NULL,
                        `OSRC_E` int(11) DEFAULT NULL,
                        `OSRC_R` int(11) DEFAULT NULL,
                        `TCHSAN` int(11) DEFAULT NULL,
                        `TCHPOS` int(11) DEFAULT NULL,
                        `PARAPOS` int(11) DEFAULT NULL,
                        `NTPOS` int(11) DEFAULT NULL,
                        `MEDINSTR1` int(11) DEFAULT NULL,
                        `MEDINSTR2` int(11) DEFAULT NULL,
                        `MEDINSTR3` int(11) DEFAULT NULL,
                        `MEDINSTR4` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `EDUBLKCD` varchar(6) DEFAULT NULL,
                        `HABITCD` varchar(11) DEFAULT NULL,
                        `ACONSTCD` varchar(7) DEFAULT NULL,
                        `MUNICIPALCD` varchar(7) DEFAULT NULL,
                        `CITY` varchar(7) DEFAULT NULL,
                        `SCHMNTCGRANT_R` int(11) DEFAULT NULL,
                        `SCHMNTCGRANT_E` int(11) DEFAULT NULL,
                        `TLE_R` int(11) DEFAULT NULL,
                        `TLE_E` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL,
                        `DISTP` int(11) DEFAULT NULL,
                        `DISTU` int(11) DEFAULT NULL,
                        `DISTS` int(11) DEFAULT NULL,
                        `DISTH` int(11) DEFAULT NULL,
                        `MEDS1` int(11) DEFAULT NULL,
                        `MEDS2` int(11) DEFAULT NULL,
                        `MEDS3` int(11) DEFAULT NULL,
                        `MEDS4` int(11) DEFAULT NULL,
                        `MEDH1` int(11) DEFAULT NULL,
                        `MEDH2` int(11) DEFAULT NULL,
                        `MEDH3` int(11) DEFAULT NULL,
                        `MEDH4` int(11) DEFAULT NULL,
                        `BOARDSEC` int(11) DEFAULT NULL,
                        `BOARDHSEC` int(11) DEFAULT NULL,
                        `YEARRECOGS` int(11) DEFAULT NULL,
                        `YEARRECOGH` int(11) DEFAULT NULL,
                        `YEARUPGRDS` int(11) DEFAULT NULL,
                        `YEARUPGRDH` int(11) DEFAULT NULL,
                        `SCHRESUPR_YN` int(11) DEFAULT NULL,
                        `SCHRESSEC_YN` int(11) DEFAULT NULL,
                        `SCHRESHSEC_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MDM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MDM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEALS_SUPPLIER` int(11) DEFAULT NULL,
                        `SUPPLIER_NAME` varchar(100) DEFAULT NULL,
                        `MDM_MAINTAINER` int(11) DEFAULT NULL,
                        `TAGGED_YN` int(11) DEFAULT NULL,
                        `TAGGED_TOSCH` varchar(7) DEFAULT NULL,
                        `TAGGED_TOCRC` varchar(6) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `FUEL_USED` int(11) DEFAULT NULL,
                        `WATER_YN` int(11) DEFAULT NULL,
                        `STOREROOM_YN` int(11) DEFAULT NULL,
                        `KITCHEN_TYPE` int(11) DEFAULT NULL,
                        `DAYS_WITHFOOD` int(11) DEFAULT NULL,
                        `DAYS_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `REASON_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `MDM_TOPROGRAM` int(11) DEFAULT NULL,
                        `BENEFITTED_BOYS` int(11) DEFAULT NULL,
                        `BENEFITTED_GIRLS` int(11) DEFAULT NULL,
                        `KITSCHEME` int(11) DEFAULT NULL,
                        `HEALTHPROG` int(11) DEFAULT NULL,
                        `REFBOYS` int(11) DEFAULT NULL,
                        `REFGIRLS` int(11) DEFAULT NULL,
                        `REFBOYSSTATE` int(11) DEFAULT NULL,
                        `REFGIRLSSTATE` int(11) DEFAULT NULL,
                        `TOTLPGCLNDR` int(11) DEFAULT NULL,
                        `MEALSINSCH` int(11) DEFAULT NULL,
                        `NPEGEL_YN` int(11) DEFAULT NULL,
                        `KGBV_MODEL` int(11) DEFAULT NULL,
                        `UTENSILS_YN` int(11) DEFAULT NULL,
                        `MDMVAR1` int(11) DEFAULT NULL,
                        `MDMVAR2` int(11) DEFAULT NULL,
                        `MDMVAR3` int(11) DEFAULT NULL,
                        `MDMVAR4` varchar(50) DEFAULT NULL,
                        `MDMVAR5` varchar(50) DEFAULT NULL,
                        `KITDEVGRANT_YN` int(11) DEFAULT NULL,
                        `COOK_M` int(11) DEFAULT NULL,
                        `COOK_F` int(11) DEFAULT NULL,
                        `MEALSERVED` int(11) DEFAULT NULL,
                        `INSPECT_SO` int(11) DEFAULT NULL,
                        `INSPECT_CM` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `HWFUN_YN` int(11) DEFAULT NULL,
                        `HWSUF_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MEDINSTR` (
                        `MEDINSTR_ID` int(11) DEFAULT NULL,
                        `MEDINSTR_DESC` varchar(75) DEFAULT NULL,
                        `MEDINSTR_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MUNICIPALITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MUNICIPALITY` (
                        `MUNCD` varchar(7) DEFAULT NULL,
                        `MUNNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `MUNVAR1` varchar(35) DEFAULT NULL,
                        `MUNVAR2` varchar(35) DEFAULT NULL,
                        `MUNVAR3` varchar(35) DEFAULT NULL,
                        `MUNVAR4` varchar(35) DEFAULT NULL,
                        `MUNVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_CURNEWB` int(11) DEFAULT NULL,
                        `C1_CURNEWG` int(11) DEFAULT NULL,
                        `C2_CURNEWB` int(11) DEFAULT NULL,
                        `C2_CURNEWG` int(11) DEFAULT NULL,
                        `C3_CURNEWB` int(11) DEFAULT NULL,
                        `C3_CURNEWG` int(11) DEFAULT NULL,
                        `C4_CURNEWB` int(11) DEFAULT NULL,
                        `C4_CURNEWG` int(11) DEFAULT NULL,
                        `C5_CURNEWB` int(11) DEFAULT NULL,
                        `C5_CURNEWG` int(11) DEFAULT NULL,
                        `C6_CURNEWB` int(11) DEFAULT NULL,
                        `C6_CURNEWG` int(11) DEFAULT NULL,
                        `C7_CURNEWB` int(11) DEFAULT NULL,
                        `C7_CURNEWG` int(11) DEFAULT NULL,
                        `C8_CURNEWB` int(11) DEFAULT NULL,
                        `C8_CURNEWG` int(11) DEFAULT NULL,
                        `C1_CURTCB` int(11) DEFAULT NULL,
                        `C1_CURTCG` int(11) DEFAULT NULL,
                        `C2_CURTCB` int(11) DEFAULT NULL,
                        `C2_CURTCG` int(11) DEFAULT NULL,
                        `C3_CURTCB` int(11) DEFAULT NULL,
                        `C3_CURTCG` int(11) DEFAULT NULL,
                        `C4_CURTCB` int(11) DEFAULT NULL,
                        `C4_CURTCG` int(11) DEFAULT NULL,
                        `C5_CURTCB` int(11) DEFAULT NULL,
                        `C5_CURTCG` int(11) DEFAULT NULL,
                        `C6_CURTCB` int(11) DEFAULT NULL,
                        `C6_CURTCG` int(11) DEFAULT NULL,
                        `C7_CURTCB` int(11) DEFAULT NULL,
                        `C7_CURTCG` int(11) DEFAULT NULL,
                        `C8_CURTCB` int(11) DEFAULT NULL,
                        `C8_CURTCG` int(11) DEFAULT NULL,
                        `C1_PRVNEWB` int(11) DEFAULT NULL,
                        `C1_PRVNEWG` int(11) DEFAULT NULL,
                        `C2_PRVNEWB` int(11) DEFAULT NULL,
                        `C2_PRVNEWG` int(11) DEFAULT NULL,
                        `C3_PRVNEWB` int(11) DEFAULT NULL,
                        `C3_PRVNEWG` int(11) DEFAULT NULL,
                        `C4_PRVNEWB` int(11) DEFAULT NULL,
                        `C4_PRVNEWG` int(11) DEFAULT NULL,
                        `C5_PRVNEWB` int(11) DEFAULT NULL,
                        `C5_PRVNEWG` int(11) DEFAULT NULL,
                        `C6_PRVNEWB` int(11) DEFAULT NULL,
                        `C6_PRVNEWG` int(11) DEFAULT NULL,
                        `C7_PRVNEWB` int(11) DEFAULT NULL,
                        `C7_PRVNEWG` int(11) DEFAULT NULL,
                        `C8_PRVNEWB` int(11) DEFAULT NULL,
                        `C8_PRVNEWG` int(11) DEFAULT NULL,
                        `C1_PRVTCB` int(11) DEFAULT NULL,
                        `C1_PRVTCG` int(11) DEFAULT NULL,
                        `C2_PRVTCB` int(11) DEFAULT NULL,
                        `C2_PRVTCG` int(11) DEFAULT NULL,
                        `C3_PRVTCB` int(11) DEFAULT NULL,
                        `C3_PRVTCG` int(11) DEFAULT NULL,
                        `C4_PRVTCB` int(11) DEFAULT NULL,
                        `C4_PRVTCG` int(11) DEFAULT NULL,
                        `C5_PRVTCB` int(11) DEFAULT NULL,
                        `C5_PRVTCG` int(11) DEFAULT NULL,
                        `C6_PRVTCB` int(11) DEFAULT NULL,
                        `C6_PRVTCG` int(11) DEFAULT NULL,
                        `C7_PRVTCB` int(11) DEFAULT NULL,
                        `C7_PRVTCG` int(11) DEFAULT NULL,
                        `C8_PRVTCB` int(11) DEFAULT NULL,
                        `C8_PRVTCG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CURNEW_9B` int(11) DEFAULT NULL,
                        `CURNEW_9G` int(11) DEFAULT NULL,
                        `CURTC_9B` int(11) DEFAULT NULL,
                        `CURTC_9G` int(11) DEFAULT NULL,
                        `PRVNEW_9B` int(11) DEFAULT NULL,
                        `PRVNEW_9G` int(11) DEFAULT NULL,
                        `PRVTC_9B` int(11) DEFAULT NULL,
                        `PRVTC_9G` int(11) DEFAULT NULL,
                        `CURNEW_10B` int(11) DEFAULT NULL,
                        `CURNEW_10G` int(11) DEFAULT NULL,
                        `CURTC_10B` int(11) DEFAULT NULL,
                        `CURTC_10G` int(11) DEFAULT NULL,
                        `PRVNEW_10B` int(11) DEFAULT NULL,
                        `PRVNEW_10G` int(11) DEFAULT NULL,
                        `PRVTC_10B` int(11) DEFAULT NULL,
                        `PRVTC_10G` int(11) DEFAULT NULL,
                        `CURNEW_11B` int(11) DEFAULT NULL,
                        `CURNEW_11G` int(11) DEFAULT NULL,
                        `CURTC_11B` int(11) DEFAULT NULL,
                        `CURTC_11G` int(11) DEFAULT NULL,
                        `PRVNEW_11B` int(11) DEFAULT NULL,
                        `PRVNEW_11G` int(11) DEFAULT NULL,
                        `PRVTC_11B` int(11) DEFAULT NULL,
                        `PRVTC_11G` int(11) DEFAULT NULL,
                        `CURNEW_12B` int(11) DEFAULT NULL,
                        `CURNEW_12G` int(11) DEFAULT NULL,
                        `CURTC_12B` int(11) DEFAULT NULL,
                        `CURTC_12G` int(11) DEFAULT NULL,
                        `PRVNEW_12B` int(11) DEFAULT NULL,
                        `PRVNEW_12G` int(11) DEFAULT NULL,
                        `PRVTC_12B` int(11) DEFAULT NULL,
                        `PRVTC_12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADMGRD1`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADMGRD1` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGE4_B` int(11) DEFAULT NULL,
                        `AGE4_G` int(11) DEFAULT NULL,
                        `AGE5_B` int(11) DEFAULT NULL,
                        `AGE5_G` int(11) DEFAULT NULL,
                        `AGE6_B` int(11) DEFAULT NULL,
                        `AGE6_G` int(11) DEFAULT NULL,
                        `AGE7_B` int(11) DEFAULT NULL,
                        `AGE7_G` int(11) DEFAULT NULL,
                        `AGE8_B` int(11) DEFAULT NULL,
                        `AGE8_G` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `SAMESCH_B` int(11) DEFAULT NULL,
                        `SAMESCH_G` int(11) DEFAULT NULL,
                        `OTHERSCH_B` int(11) DEFAULT NULL,
                        `OTHERSCH_G` int(11) DEFAULT NULL,
                        `ECCE_B` int(11) DEFAULT NULL,
                        `ECCE_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NONTCH`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NONTCH` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `DESIG_ID` int(11) DEFAULT NULL,
                        `TOTSAN` int(11) DEFAULT NULL,
                        `TOTPOS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PANCHAYAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PANCHAYAT` (
                        `PANCD` varchar(9) DEFAULT NULL,
                        `PANNAME` varchar(30) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PANVAR1` varchar(35) DEFAULT NULL,
                        `PANVAR2` varchar(35) DEFAULT NULL,
                        `PANVAR3` varchar(35) DEFAULT NULL,
                        `PANVAR4` varchar(35) DEFAULT NULL,
                        `PANVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PCONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PCONSTITUENCY` (
                        `CONSTCD` varchar(8) DEFAULT NULL,
                        `CONSTNAME` varchar(75) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_POPDROPNENR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_POPDROPNENR` (
                        `TABLEID` int(11) DEFAULT NULL,
                        `DBVHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `LEVELID` int(11) DEFAULT NULL,
                        `GEN610B` int(11) DEFAULT NULL,
                        `GEN610G` int(11) DEFAULT NULL,
                        `SC610B` int(11) DEFAULT NULL,
                        `SC610G` int(11) DEFAULT NULL,
                        `ST610B` int(11) DEFAULT NULL,
                        `ST610G` int(11) DEFAULT NULL,
                        `OBC610B` int(11) DEFAULT NULL,
                        `OBC610G` int(11) DEFAULT NULL,
                        `TOT610B` int(11) DEFAULT NULL,
                        `TOT610G` int(11) DEFAULT NULL,
                        `MIN_610B` int(11) DEFAULT NULL,
                        `MIN_610G` int(11) DEFAULT NULL,
                        `GEN1113B` int(11) DEFAULT NULL,
                        `GEN1113G` int(11) DEFAULT NULL,
                        `SC1113B` int(11) DEFAULT NULL,
                        `SC1113G` int(11) DEFAULT NULL,
                        `ST1113B` int(11) DEFAULT NULL,
                        `ST1113G` int(11) DEFAULT NULL,
                        `OBC1113B` int(11) DEFAULT NULL,
                        `OBC1113G` int(11) DEFAULT NULL,
                        `TOT1113B` int(11) DEFAULT NULL,
                        `TOT1113G` int(11) DEFAULT NULL,
                        `MIN_1113B` int(11) DEFAULT NULL,
                        `MIN_1113G` int(11) DEFAULT NULL,
                        `GEN1415B` int(11) DEFAULT NULL,
                        `GEN1415G` int(11) DEFAULT NULL,
                        `SC1415B` int(11) DEFAULT NULL,
                        `SC1415G` int(11) DEFAULT NULL,
                        `ST1415B` int(11) DEFAULT NULL,
                        `ST1415G` int(11) DEFAULT NULL,
                        `OBC1415B` int(11) DEFAULT NULL,
                        `OBC1415G` int(11) DEFAULT NULL,
                        `TOT1415B` int(11) DEFAULT NULL,
                        `TOT1415G` int(11) DEFAULT NULL,
                        `MIN_1415B` int(11) DEFAULT NULL,
                        `MIN_1415G` int(11) DEFAULT NULL,
                        `DIST1` double DEFAULT NULL,
                        `DIST2` double DEFAULT NULL,
                        `DIST3` double DEFAULT NULL,
                        `DIST4` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_FAILB` int(11) DEFAULT NULL,
                        `C1_FAILG` int(11) DEFAULT NULL,
                        `C2_FAILB` int(11) DEFAULT NULL,
                        `C2_FAILG` int(11) DEFAULT NULL,
                        `C3_FAILB` int(11) DEFAULT NULL,
                        `C3_FAILG` int(11) DEFAULT NULL,
                        `C4_FAILB` int(11) DEFAULT NULL,
                        `C4_FAILG` int(11) DEFAULT NULL,
                        `C5_FAILB` int(11) DEFAULT NULL,
                        `C5_FAILG` int(11) DEFAULT NULL,
                        `C6_FAILB` int(11) DEFAULT NULL,
                        `C6_FAILG` int(11) DEFAULT NULL,
                        `C7_FAILB` int(11) DEFAULT NULL,
                        `C7_FAILG` int(11) DEFAULT NULL,
                        `C8_FAILB` int(11) DEFAULT NULL,
                        `C8_FAILG` int(11) DEFAULT NULL,
                        `C1_ABSB` int(11) DEFAULT NULL,
                        `C1_ABSG` int(11) DEFAULT NULL,
                        `C2_ABSB` int(11) DEFAULT NULL,
                        `C2_ABSG` int(11) DEFAULT NULL,
                        `C3_ABSB` int(11) DEFAULT NULL,
                        `C3_ABSG` int(11) DEFAULT NULL,
                        `C4_ABSB` int(11) DEFAULT NULL,
                        `C4_ABSG` int(11) DEFAULT NULL,
                        `C5_ABSB` int(11) DEFAULT NULL,
                        `C5_ABSG` int(11) DEFAULT NULL,
                        `C6_ABSB` int(11) DEFAULT NULL,
                        `C6_ABSG` int(11) DEFAULT NULL,
                        `C7_ABSB` int(11) DEFAULT NULL,
                        `C7_ABSG` int(11) DEFAULT NULL,
                        `C8_ABSB` int(11) DEFAULT NULL,
                        `C8_ABSG` int(11) DEFAULT NULL,
                        `C1_READB` int(11) DEFAULT NULL,
                        `C1_READG` int(11) DEFAULT NULL,
                        `C2_READB` int(11) DEFAULT NULL,
                        `C2_READG` int(11) DEFAULT NULL,
                        `C3_READB` int(11) DEFAULT NULL,
                        `C3_READG` int(11) DEFAULT NULL,
                        `C4_READB` int(11) DEFAULT NULL,
                        `C4_READG` int(11) DEFAULT NULL,
                        `C5_READB` int(11) DEFAULT NULL,
                        `C5_READG` int(11) DEFAULT NULL,
                        `C6_READB` int(11) DEFAULT NULL,
                        `C6_READG` int(11) DEFAULT NULL,
                        `C7_READB` int(11) DEFAULT NULL,
                        `C7_READG` int(11) DEFAULT NULL,
                        `C8_READB` int(11) DEFAULT NULL,
                        `C8_READG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9BFAIL` int(11) DEFAULT NULL,
                        `C9GFAIL` int(11) DEFAULT NULL,
                        `C9BABS` int(11) DEFAULT NULL,
                        `C9GABS` int(11) DEFAULT NULL,
                        `C9BREAD` int(11) DEFAULT NULL,
                        `C9GREAD` int(11) DEFAULT NULL,
                        `C10BFAIL` int(11) DEFAULT NULL,
                        `C10GFAIL` int(11) DEFAULT NULL,
                        `C10BABS` int(11) DEFAULT NULL,
                        `C10GABS` int(11) DEFAULT NULL,
                        `C10BREAD` int(11) DEFAULT NULL,
                        `C10GREAD` int(11) DEFAULT NULL,
                        `C11BFAIL` int(11) DEFAULT NULL,
                        `C11GFAIL` int(11) DEFAULT NULL,
                        `C11BABS` int(11) DEFAULT NULL,
                        `C11GABS` int(11) DEFAULT NULL,
                        `C11BREAD` int(11) DEFAULT NULL,
                        `C11GREAD` int(11) DEFAULT NULL,
                        `C12BFAIL` int(11) DEFAULT NULL,
                        `C12GFAIL` int(11) DEFAULT NULL,
                        `C12BABS` int(11) DEFAULT NULL,
                        `C12GABS` int(11) DEFAULT NULL,
                        `C12BREAD` int(11) DEFAULT NULL,
                        `C12GREAD` int(11) DEFAULT NULL,
                        `C1BSC` int(11) DEFAULT NULL,
                        `C1GSC` int(11) DEFAULT NULL,
                        `C2BSC` int(11) DEFAULT NULL,
                        `C2GSC` int(11) DEFAULT NULL,
                        `C3BSC` int(11) DEFAULT NULL,
                        `C3GSC` int(11) DEFAULT NULL,
                        `C4BSC` int(11) DEFAULT NULL,
                        `C4GSC` int(11) DEFAULT NULL,
                        `C5BSC` int(11) DEFAULT NULL,
                        `C5GSC` int(11) DEFAULT NULL,
                        `C6BSC` int(11) DEFAULT NULL,
                        `C6GSC` int(11) DEFAULT NULL,
                        `C7BSC` int(11) DEFAULT NULL,
                        `C7GSC` int(11) DEFAULT NULL,
                        `C8BSC` int(11) DEFAULT NULL,
                        `C8GSC` int(11) DEFAULT NULL,
                        `C9BSC` int(11) DEFAULT NULL,
                        `C9GSC` int(11) DEFAULT NULL,
                        `C10BSC` int(11) DEFAULT NULL,
                        `C10GSC` int(11) DEFAULT NULL,
                        `C11BSC` int(11) DEFAULT NULL,
                        `C11GSC` int(11) DEFAULT NULL,
                        `C12BSC` int(11) DEFAULT NULL,
                        `C12GSC` int(11) DEFAULT NULL,
                        `C1BST` int(11) DEFAULT NULL,
                        `C1GST` int(11) DEFAULT NULL,
                        `C2BST` int(11) DEFAULT NULL,
                        `C2GST` int(11) DEFAULT NULL,
                        `C3BST` int(11) DEFAULT NULL,
                        `C3GST` int(11) DEFAULT NULL,
                        `C4BST` int(11) DEFAULT NULL,
                        `C4GST` int(11) DEFAULT NULL,
                        `C5BST` int(11) DEFAULT NULL,
                        `C5GST` int(11) DEFAULT NULL,
                        `C6BST` int(11) DEFAULT NULL,
                        `C6GST` int(11) DEFAULT NULL,
                        `C7BST` int(11) DEFAULT NULL,
                        `C7GST` int(11) DEFAULT NULL,
                        `C8BST` int(11) DEFAULT NULL,
                        `C8GST` int(11) DEFAULT NULL,
                        `C9BST` int(11) DEFAULT NULL,
                        `C9GST` int(11) DEFAULT NULL,
                        `C10BST` int(11) DEFAULT NULL,
                        `C10GST` int(11) DEFAULT NULL,
                        `C11BST` int(11) DEFAULT NULL,
                        `C11GST` int(11) DEFAULT NULL,
                        `C12BST` int(11) DEFAULT NULL,
                        `C12GST` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RESITYPE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RESITYPE` (
                        `RESITYPE_ID` int(11) DEFAULT NULL,
                        `RESITYPE_DESC` varchar(75) DEFAULT NULL,
                        `RESITYPE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RTEINFO`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RTEINFO` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `WORKDAYS_PR` int(11) DEFAULT NULL,
                        `WORKDAYS_UPR` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_PR` double DEFAULT NULL,
                        `SCHHRSCHILD_UPR` double DEFAULT NULL,
                        `SCHHRSTCH_PR` double DEFAULT NULL,
                        `SCHHRSTCH_UPR` double DEFAULT NULL,
                        `CCE_YN` int(11) DEFAULT NULL,
                        `PCR_MAINTAINED` int(11) DEFAULT NULL,
                        `PCR_SHARED` int(11) DEFAULT NULL,
                        `WSEC25P_APPLIED` int(11) DEFAULT NULL,
                        `WSEC25P_ENROLLED` int(11) DEFAULT NULL,
                        `AIDRECD` int(11) DEFAULT NULL,
                        `STUADMITTED` int(11) DEFAULT NULL,
                        `SMC_YN` int(11) DEFAULT NULL,
                        `SMCMEM_M` int(11) DEFAULT NULL,
                        `SMCMEM_F` int(11) DEFAULT NULL,
                        `SMSPARENTS_M` int(11) DEFAULT NULL,
                        `SMSPARENTS_F` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_M` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_F` int(11) DEFAULT NULL,
                        `SMCMEETINGS` int(11) DEFAULT NULL,
                        `SMCSDP_YN` int(11) DEFAULT NULL,
                        `SMSCHILDREC_YN` int(11) DEFAULT NULL,
                        `SMCBANKAC_YN` int(11) DEFAULT NULL,
                        `SMCBANK` varchar(75) DEFAULT NULL,
                        `SMCBANKBRANCH` varchar(75) DEFAULT NULL,
                        `SMCACNO` varchar(20) DEFAULT NULL,
                        `IFSCCODE` varchar(20) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_BY` int(11) DEFAULT NULL,
                        `SPLTRG_PLACE` int(11) DEFAULT NULL,
                        `SPLTRG_TYPE` int(11) DEFAULT NULL,
                        `SPLTRG_TOTEV` int(11) DEFAULT NULL,
                        `SPLTRG_EVTRND` int(11) DEFAULT NULL,
                        `SPLTRG_MATERIAL_YN` int(11) DEFAULT NULL,
                        `TXTBKRECD_YN` int(11) DEFAULT NULL,
                        `TXTBKMNTH` int(11) DEFAULT NULL,
                        `TXTBKYEAR` int(11) DEFAULT NULL,
                        `ACSTARTMNTH` int(11) DEFAULT NULL,
                        `SMCACNAME` varchar(100) DEFAULT NULL,
                        `SPLTRNG_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHCAT` (
                        `SCHCAT_ID` int(11) DEFAULT NULL,
                        `SCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `SCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHMGT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHMGT` (
                        `SCHMGT_ID` int(11) DEFAULT NULL,
                        `SCHMGT_DESC` varchar(75) DEFAULT NULL,
                        `SCHMGT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHOOL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHOOL` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `SCHVAR1` varchar(35) DEFAULT NULL,
                        `SCHVAR2` varchar(35) DEFAULT NULL,
                        `SCHVAR3` varchar(35) DEFAULT NULL,
                        `SCHVAR4` varchar(35) DEFAULT NULL,
                        `SCHVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SMDC`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SMDC` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SMDC_YN` int(11) DEFAULT NULL,
                        `PARENTS_M` int(11) DEFAULT NULL,
                        `PARENTS_F` int(11) DEFAULT NULL,
                        `LOCAL_M` int(11) DEFAULT NULL,
                        `LOCAL_F` int(11) DEFAULT NULL,
                        `EBMC_M` int(11) DEFAULT NULL,
                        `EBMC_F` int(11) DEFAULT NULL,
                        `WOMEN_M` int(11) DEFAULT NULL,
                        `SCST_M` int(11) DEFAULT NULL,
                        `SCST_F` int(11) DEFAULT NULL,
                        `DEO_M` int(11) DEFAULT NULL,
                        `DEO_F` int(11) DEFAULT NULL,
                        `AAD_M` int(11) DEFAULT NULL,
                        `AAD_F` int(11) DEFAULT NULL,
                        `SUBEXP_M` int(11) DEFAULT NULL,
                        `SUBEXP_F` int(11) DEFAULT NULL,
                        `TCH_M` int(11) DEFAULT NULL,
                        `TCH_F` int(11) DEFAULT NULL,
                        `AHM_M` int(11) DEFAULT NULL,
                        `AHM_F` int(11) DEFAULT NULL,
                        `HM_M` int(11) DEFAULT NULL,
                        `HM_F` int(11) DEFAULT NULL,
                        `CP_M` int(11) DEFAULT NULL,
                        `CP_F` int(11) DEFAULT NULL,
                        `SMDCMEETING` int(11) DEFAULT NULL,
                        `SIP_YN` int(11) DEFAULT NULL,
                        `BANKAC_YN` int(11) DEFAULT NULL,
                        `BANKNAME` varchar(75) DEFAULT NULL,
                        `BANKBRANCH` varchar(75) DEFAULT NULL,
                        `BANKACNO` varchar(50) DEFAULT NULL,
                        `ACNAME` varchar(100) DEFAULT NULL,
                        `IFSC` varchar(50) DEFAULT NULL,
                        `SBC_YN` int(11) DEFAULT NULL,
                        `AC_YN` int(11) DEFAULT NULL,
                        `PTA_YN` int(11) DEFAULT NULL,
                        `PTAMEETING` int(11) DEFAULT NULL,
                        `WORKDAYS_SEC` int(11) DEFAULT NULL,
                        `WORKDAYS_HSEC` int(11) DEFAULT NULL,
                        `CCESEC_YN` int(11) DEFAULT NULL,
                        `CCEHSEC_YN` int(11) DEFAULT NULL,
                        `SMCSMDC1_YN` int(11) DEFAULT NULL,
                        `TOT_M` int(11) DEFAULT NULL,
                        `TOT_F` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_SEC` double DEFAULT NULL,
                        `SCHHRSCHILD_HSEC` double DEFAULT NULL,
                        `SCHHRSTCH_SEC` double DEFAULT NULL,
                        `SCHHRSTCH_HSEC` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SPLTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SPLTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_STATLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_STATLIST` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVAR` (
                        `VARID` int(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARGROUP` int(11) DEFAULT NULL,
                        `VARSEQ` int(11) DEFAULT NULL,
                        `VARDESC` varchar(100) DEFAULT NULL,
                        `VARTYPE` int(11) DEFAULT NULL,
                        `VARLEN` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(100) DEFAULT NULL,
                        `VARVALIDATION` varchar(50) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVARENTRY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVARENTRY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARID` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(50) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHAQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHAQUAL` (
                        `TCHAQUAL_ID` int(11) DEFAULT NULL,
                        `TCHAQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHAQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHBYSUB`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHBYSUB` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLASSID` int(11) DEFAULT NULL,
                        `SUBJECTID` int(11) DEFAULT NULL,
                        `SANCTIONED` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTE` (
                        `TCHCASTE_ID` int(11) DEFAULT NULL,
                        `TCHCASTE_DESC` varchar(75) DEFAULT NULL,
                        `TCHCASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHTYPE` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(10) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `TRNGM` int(11) DEFAULT NULL,
                        `TRNGF` int(11) DEFAULT NULL,
                        `TRNCM` int(11) DEFAULT NULL,
                        `TRNCF` int(11) DEFAULT NULL,
                        `TRNTM` int(11) DEFAULT NULL,
                        `TRNTF` int(11) DEFAULT NULL,
                        `TRNOM` int(11) DEFAULT NULL,
                        `TRNOF` int(11) DEFAULT NULL,
                        `UTRGM` int(11) DEFAULT NULL,
                        `UTRGF` int(11) DEFAULT NULL,
                        `UTRCM` int(11) DEFAULT NULL,
                        `UTRCF` int(11) DEFAULT NULL,
                        `UTRTM` int(11) DEFAULT NULL,
                        `UTRTF` int(11) DEFAULT NULL,
                        `UTROM` int(11) DEFAULT NULL,
                        `UTROF` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCAT` (
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `TCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCATMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCATMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `NUM_SANCTIONED` int(11) DEFAULT NULL,
                        `NUM_POSITION` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCLSTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCLSTAUGHT` (
                        `TCHCLSTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHCLSTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCLSTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHPQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHPQUAL` (
                        `TCHPQUAL_ID` int(11) DEFAULT NULL,
                        `TCHPQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHPQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHSUBTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHSUBTAUGHT` (
                        `TCHSUBTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHSUBTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHSUBTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TEACHER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TEACHER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TCHCD` varchar(12) DEFAULT NULL,
                        `SLNO` int(11) DEFAULT NULL,
                        `TCHNAME` varchar(50) DEFAULT NULL,
                        `SEX` int(11) DEFAULT NULL,
                        `DOB` datetime DEFAULT NULL,
                        `CASTE` int(11) DEFAULT NULL,
                        `CATEGORY` int(11) DEFAULT NULL,
                        `YOJ` int(11) DEFAULT NULL,
                        `QUAL_ACAD` int(11) DEFAULT NULL,
                        `QUAL_PROF` int(11) DEFAULT NULL,
                        `CLS_TAUGHT` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT1` int(11) DEFAULT NULL,
                        `MATH_UPTO` int(11) DEFAULT NULL,
                        `ENG_UPTO` int(11) DEFAULT NULL,
                        `TRN_BRC` int(11) DEFAULT NULL,
                        `TRN_CRC` int(11) DEFAULT NULL,
                        `TRN_DIET` int(11) DEFAULT NULL,
                        `TRN_OTHER` int(11) DEFAULT NULL,
                        `NONTCH_ASS` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT2` int(11) DEFAULT NULL,
                        `SUPVAR1` int(11) DEFAULT NULL,
                        `SUPVAR2` int(11) DEFAULT NULL,
                        `SUPVAR3` int(11) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` datetime DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `WORKINGSINCE` int(11) DEFAULT NULL,
                        `POST_STATUS` int(11) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `DEPUTATION_YN` int(11) DEFAULT NULL,
                        `SCIENCEUPTO` int(11) DEFAULT NULL,
                        `CWSNTRAINED_YN` int(11) DEFAULT NULL,
                        `APPOINTSUB` int(11) DEFAULT NULL,
                        `STREAM` int(11) DEFAULT NULL,
                        `AADHAAR` bigint(12) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TXTBKTLESPORTS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TXTBKTLESPORTS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1` int(11) DEFAULT NULL,
                        `C2` int(11) DEFAULT NULL,
                        `C3` int(11) DEFAULT NULL,
                        `C4` int(11) DEFAULT NULL,
                        `C5` int(11) DEFAULT NULL,
                        `C6` int(11) DEFAULT NULL,
                        `C7` int(11) DEFAULT NULL,
                        `C8` int(11) DEFAULT NULL,
                        `C9` int(11) DEFAULT NULL,
                        `C10` int(11) DEFAULT NULL,
                        `C11` int(11) DEFAULT NULL,
                        `C12` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UCDETAILS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UCDETAILS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `OPENINGBAL` double DEFAULT NULL,
                        `AMTRECD` double DEFAULT NULL,
                        `AMTSPENT` double DEFAULT NULL,
                        `CLOSINGBAL` double DEFAULT NULL,
                        `BANKINT` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VARSETTINGS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VARSETTINGS` (
                        `VAR_ID` varchar(10) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VAR_TYPE` int(11) DEFAULT NULL,
                        `VAR_LEN` int(11) DEFAULT NULL,
                        `VAR_LBL` varchar(30) DEFAULT NULL,
                        `VAR_TBL` varchar(30) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VILLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VILLAGE` (
                        `VILCD` varchar(9) DEFAULT NULL,
                        `VILNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `TRIBAL_YN` int(11) DEFAULT NULL,
                        `CENCD` varchar(21) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `VILVAR1` varchar(35) DEFAULT NULL,
                        `VILVAR2` varchar(35) DEFAULT NULL,
                        `VILVAR3` varchar(35) DEFAULT NULL,
                        `VILVAR4` varchar(35) DEFAULT NULL,
                        `VILVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_WATER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_WATER` (
                        `WATER_ID` int(11) DEFAULT NULL,
                        `WATER_DESC` varchar(75) DEFAULT NULL,
                        `WATER_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
                /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
                /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
                SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


                /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
                /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
                /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
                /*!40101 SET NAMES utf8 */;

                --
                    -- Database: `ssa`
                    --

                    -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ATTENDANCE09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ATTENDANCE09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `BLDSTATUS` int(11) DEFAULT NULL,
                        `CLROOMS` int(11) DEFAULT NULL,
                        `CLGOOD` int(11) DEFAULT NULL,
                        `CLMAJOR` int(11) DEFAULT NULL,
                        `CLMINOR` int(11) DEFAULT NULL,
                        `OTHGOOD` int(11) DEFAULT NULL,
                        `OTHMAJOR` int(11) DEFAULT NULL,
                        `OTHMINOR` int(11) DEFAULT NULL,
                        `TOILET_C` int(11) DEFAULT NULL,
                        `TOILET_G` int(11) DEFAULT NULL,
                        `ELECTRIC_YN` int(11) DEFAULT NULL,
                        `BNDRYWALL` int(11) DEFAULT NULL,
                        `BOOKBANK_YN` int(11) DEFAULT NULL,
                        `PGROUND_YN` int(11) DEFAULT NULL,
                        `BLACKBOARD` int(11) DEFAULT NULL,
                        `ALMIRAH` int(11) DEFAULT NULL,
                        `BOX` int(11) DEFAULT NULL,
                        `BOOKINLIB` int(11) DEFAULT NULL,
                        `WATER` int(11) DEFAULT NULL,
                        `MEDCHK_YN` int(11) DEFAULT NULL,
                        `RAMPS_YN` int(11) DEFAULT NULL,
                        `COMPUTER` int(11) DEFAULT NULL,
                        `BLDPUCCA` int(11) DEFAULT NULL,
                        `BLDPPUCCA` int(11) DEFAULT NULL,
                        `BLDKUCCHA` int(11) DEFAULT NULL,
                        `BLDTENT` int(11) DEFAULT NULL,
                        `CLSPUCCA` int(11) DEFAULT NULL,
                        `CLSPPUCCA` int(11) DEFAULT NULL,
                        `CLSKUCCHA` int(11) DEFAULT NULL,
                        `CLSTENT` int(11) DEFAULT NULL,
                        `ROOMSPUCCA` int(11) DEFAULT NULL,
                        `ROOMSPPUCCA` int(11) DEFAULT NULL,
                        `ROOMSKUCCHA` int(11) DEFAULT NULL,
                        `ROOMSTENT` int(11) DEFAULT NULL,
                        `ANANDCLASS` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `FURNTCH` int(11) DEFAULT NULL,
                        `FURNSTU` int(11) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL,
                        `BLDCOND` int(11) DEFAULT NULL,
                        `TOTCLS9` int(11) DEFAULT NULL,
                        `TOTCLS10` int(11) DEFAULT NULL,
                        `TOTCLS11` int(11) DEFAULT NULL,
                        `TOTCLS12` int(11) DEFAULT NULL,
                        `TOTOTH9` int(11) DEFAULT NULL,
                        `TOTOTH10` int(11) DEFAULT NULL,
                        `TOTOTH11` int(11) DEFAULT NULL,
                        `TOTOTH12` int(11) DEFAULT NULL,
                        `CLSUCONST9` int(11) DEFAULT NULL,
                        `CLSUCONST10` int(11) DEFAULT NULL,
                        `CLSUCONST11` int(11) DEFAULT NULL,
                        `CLSUCONST12` int(11) DEFAULT NULL,
                        `BBOARD9` int(11) DEFAULT NULL,
                        `BBOARD10` int(11) DEFAULT NULL,
                        `BBOARD11` int(11) DEFAULT NULL,
                        `BBOARD12` int(11) DEFAULT NULL,
                        `FURN_YN9` int(11) DEFAULT NULL,
                        `FURN_YN10` int(11) DEFAULT NULL,
                        `FURN_YN11` int(11) DEFAULT NULL,
                        `FURN_YN12` int(11) DEFAULT NULL,
                        `CLGOODHS` int(11) DEFAULT NULL,
                        `CLMINORHS` int(11) DEFAULT NULL,
                        `CLMAJORHS` int(11) DEFAULT NULL,
                        `OTHGOODHS` int(11) DEFAULT NULL,
                        `OTHMINORHS` int(11) DEFAULT NULL,
                        `OTHMAJORHS` int(11) DEFAULT NULL,
                        `TOILETWATER_B` int(11) DEFAULT NULL,
                        `TOILETWATER_G` int(11) DEFAULT NULL,
                        `URINALS_B` int(11) DEFAULT NULL,
                        `URINALS_G` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `LIBRARIAN_YN` int(11) DEFAULT NULL,
                        `COMPSEC` int(11) DEFAULT NULL,
                        `COMPSEC_FUNC` int(11) DEFAULT NULL,
                        `COMPHS` int(11) DEFAULT NULL,
                        `COMPHS_FUNC` int(11) DEFAULT NULL,
                        `RAMPSNEEDED_YN` int(11) DEFAULT NULL,
                        `HOSTELB_YN` int(11) DEFAULT NULL,
                        `HOSTELBOYS` int(11) DEFAULT NULL,
                        `HOSTELG_YN` int(11) DEFAULT NULL,
                        `HOSTELGIRLS` int(11) DEFAULT NULL,
                        `OTHROOMS` int(11) DEFAULT NULL,
                        `COMP_FUNC` int(11) DEFAULT NULL,
                        `CLGOODS` int(11) DEFAULT NULL,
                        `CLMINORS` int(11) DEFAULT NULL,
                        `CLMAJORS` int(11) DEFAULT NULL,
                        `OTHGOODS` int(11) DEFAULT NULL,
                        `OTHMINORS` int(11) DEFAULT NULL,
                        `OTHMAJORS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDEQP09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDEQP09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLSUNDERCONST` int(11) DEFAULT NULL,
                        `LAND4CLS_YN` int(11) DEFAULT NULL,
                        `TOILETT` int(11) DEFAULT NULL,
                        `TOILETT_FUNC` int(11) DEFAULT NULL,
                        `TOILETB` int(11) DEFAULT NULL,
                        `TOILETB_FUNC` int(11) DEFAULT NULL,
                        `TOILETC_FUNC` int(11) DEFAULT NULL,
                        `TOILETG_FUNC` int(11) DEFAULT NULL,
                        `LIBRARY_YN` int(11) DEFAULT NULL,
                        `WATER_FUNC_YN` int(11) DEFAULT NULL,
                        `TOTCOMP_FUNC` int(11) DEFAULT NULL,
                        `HMROOM_YN` int(11) DEFAULT NULL,
                        `CLGOOD_PPU` int(11) DEFAULT NULL,
                        `CLGOOD_KUC` int(11) DEFAULT NULL,
                        `CLGOOD_TNT` int(11) DEFAULT NULL,
                        `CLMINOR_PPU` int(11) DEFAULT NULL,
                        `CLMINOR_KUC` int(11) DEFAULT NULL,
                        `CLMINOR_TNT` int(11) DEFAULT NULL,
                        `CLMAJOR_PPU` int(11) DEFAULT NULL,
                        `CLMAJOR_KUC` int(11) DEFAULT NULL,
                        `CLMAJOR_TNT` int(11) DEFAULT NULL,
                        `OTGOOD_PPU` int(11) DEFAULT NULL,
                        `OTGOOD_KUC` int(11) DEFAULT NULL,
                        `OTGOOD_TNT` int(11) DEFAULT NULL,
                        `OTMINOR_PPU` int(11) DEFAULT NULL,
                        `OTMINOR_KUC` int(11) DEFAULT NULL,
                        `OTMINOR_TNT` int(11) DEFAULT NULL,
                        `OTMAJOR_PPU` int(11) DEFAULT NULL,
                        `OTMAJOR_KUC` int(11) DEFAULT NULL,
                        `OTMAJOR_TNT` int(11) DEFAULT NULL,
                        `CAL_YN` int(11) DEFAULT NULL,
                        `NEWSPAPER_YN` int(11) DEFAULT NULL,
                        `LAND4PGROUND_YN` int(11) DEFAULT NULL,
                        `HANDRAILS` int(11) DEFAULT NULL,
                        `CAMPUSPLAN_YN` int(11) DEFAULT NULL,
                        `TOILETD` int(11) DEFAULT NULL,
                        `TOILETD_FUNC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLDSTATUS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLDSTATUS` (
                        `BLDSTATUS_ID` int(11) DEFAULT NULL,
                        `BLDSTATUS_DESC` varchar(75) DEFAULT NULL,
                        `BLDSTATUS_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `TRIBAL_YN` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `BLKVAR1` varchar(35) DEFAULT NULL,
                        `BLKVAR2` varchar(35) DEFAULT NULL,
                        `BLKVAR3` varchar(35) DEFAULT NULL,
                        `BLKVAR4` varchar(35) DEFAULT NULL,
                        `BLKVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_BNDRYWALL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_BNDRYWALL` (
                        `BNDRYWALL_ID` int(11) DEFAULT NULL,
                        `BNDRYWALL_DESC` varchar(75) DEFAULT NULL,
                        `BNDRYWALL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CASTE` (
                        `CASTE_ID` int(11) DEFAULT NULL,
                        `CASTE_DESC` varchar(75) DEFAULT NULL,
                        `CASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CITY` (
                        `CITYCD` varchar(7) DEFAULT NULL,
                        `CITYNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CITYVAR1` varchar(35) DEFAULT NULL,
                        `CITYVAR2` varchar(35) DEFAULT NULL,
                        `CITYVAR3` varchar(35) DEFAULT NULL,
                        `CITYVAR4` varchar(35) DEFAULT NULL,
                        `CITYVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CLUSTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CLUSTER` (
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `CLUNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `CLUVAR1` varchar(35) DEFAULT NULL,
                        `CLUVAR2` varchar(35) DEFAULT NULL,
                        `CLUVAR3` varchar(35) DEFAULT NULL,
                        `CLUVAR4` varchar(35) DEFAULT NULL,
                        `CLUVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CONSTITUENCY` (
                        `CONSTCD` varchar(7) DEFAULT NULL,
                        `CONSTNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITYID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CWSNFACILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CWSNFACILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `TOT_B_SEC` int(11) DEFAULT NULL,
                        `TOT_G_SEC` int(11) DEFAULT NULL,
                        `TOT_B_HSEC` int(11) DEFAULT NULL,
                        `TOT_G_HSEC` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_CYCLE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_CYCLE` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_BLINDB` int(11) DEFAULT NULL,
                        `C1_BLINDG` int(11) DEFAULT NULL,
                        `C1_DEAFB` int(11) DEFAULT NULL,
                        `C1_DEAFG` int(11) DEFAULT NULL,
                        `C1_ORTHOB` int(11) DEFAULT NULL,
                        `C1_ORTHOG` int(11) DEFAULT NULL,
                        `C1_MENTALB` int(11) DEFAULT NULL,
                        `C1_MENTALG` int(11) DEFAULT NULL,
                        `C1_OTHB` int(11) DEFAULT NULL,
                        `C1_OTHG` int(11) DEFAULT NULL,
                        `C2_BLINDB` int(11) DEFAULT NULL,
                        `C2_BLINDG` int(11) DEFAULT NULL,
                        `C2_DEAFB` int(11) DEFAULT NULL,
                        `C2_DEAFG` int(11) DEFAULT NULL,
                        `C2_ORTHOB` int(11) DEFAULT NULL,
                        `C2_ORTHOG` int(11) DEFAULT NULL,
                        `C2_MENTALB` int(11) DEFAULT NULL,
                        `C2_MENTALG` int(11) DEFAULT NULL,
                        `C2_OTHB` int(11) DEFAULT NULL,
                        `C2_OTHG` int(11) DEFAULT NULL,
                        `C3_BLINDB` int(11) DEFAULT NULL,
                        `C3_BLINDG` int(11) DEFAULT NULL,
                        `C3_DEAFB` int(11) DEFAULT NULL,
                        `C3_DEAFG` int(11) DEFAULT NULL,
                        `C3_ORTHOB` int(11) DEFAULT NULL,
                        `C3_ORTHOG` int(11) DEFAULT NULL,
                        `C3_MENTALB` int(11) DEFAULT NULL,
                        `C3_MENTALG` int(11) DEFAULT NULL,
                        `C3_OTHB` int(11) DEFAULT NULL,
                        `C3_OTHG` int(11) DEFAULT NULL,
                        `C4_BLINDB` int(11) DEFAULT NULL,
                        `C4_BLINDG` int(11) DEFAULT NULL,
                        `C4_DEAFB` int(11) DEFAULT NULL,
                        `C4_DEAFG` int(11) DEFAULT NULL,
                        `C4_ORTHOB` int(11) DEFAULT NULL,
                        `C4_ORTHOG` int(11) DEFAULT NULL,
                        `C4_MENTALB` int(11) DEFAULT NULL,
                        `C4_MENTALG` int(11) DEFAULT NULL,
                        `C4_OTHB` int(11) DEFAULT NULL,
                        `C4_OTHG` int(11) DEFAULT NULL,
                        `C5_BLINDB` int(11) DEFAULT NULL,
                        `C5_BLINDG` int(11) DEFAULT NULL,
                        `C5_DEAFB` int(11) DEFAULT NULL,
                        `C5_DEAFG` int(11) DEFAULT NULL,
                        `C5_ORTHOB` int(11) DEFAULT NULL,
                        `C5_ORTHOG` int(11) DEFAULT NULL,
                        `C5_MENTALB` int(11) DEFAULT NULL,
                        `C5_MENTALG` int(11) DEFAULT NULL,
                        `C5_OTHB` int(11) DEFAULT NULL,
                        `C5_OTHG` int(11) DEFAULT NULL,
                        `C6_BLINDB` int(11) DEFAULT NULL,
                        `C6_BLINDG` int(11) DEFAULT NULL,
                        `C6_DEAFB` int(11) DEFAULT NULL,
                        `C6_DEAFG` int(11) DEFAULT NULL,
                        `C6_ORTHOB` int(11) DEFAULT NULL,
                        `C6_ORTHOG` int(11) DEFAULT NULL,
                        `C6_MENTALB` int(11) DEFAULT NULL,
                        `C6_MENTALG` int(11) DEFAULT NULL,
                        `C6_OTHB` int(11) DEFAULT NULL,
                        `C6_OTHG` int(11) DEFAULT NULL,
                        `C7_BLINDB` int(11) DEFAULT NULL,
                        `C7_BLINDG` int(11) DEFAULT NULL,
                        `C7_DEAFB` int(11) DEFAULT NULL,
                        `C7_DEAFG` int(11) DEFAULT NULL,
                        `C7_ORTHOB` int(11) DEFAULT NULL,
                        `C7_ORTHOG` int(11) DEFAULT NULL,
                        `C7_MENTALB` int(11) DEFAULT NULL,
                        `C7_MENTALG` int(11) DEFAULT NULL,
                        `C7_OTHB` int(11) DEFAULT NULL,
                        `C7_OTHG` int(11) DEFAULT NULL,
                        `C8_BLINDB` int(11) DEFAULT NULL,
                        `C8_BLINDG` int(11) DEFAULT NULL,
                        `C8_DEAFB` int(11) DEFAULT NULL,
                        `C8_DEAFG` int(11) DEFAULT NULL,
                        `C8_ORTHOB` int(11) DEFAULT NULL,
                        `C8_ORTHOG` int(11) DEFAULT NULL,
                        `C8_MENTALB` int(11) DEFAULT NULL,
                        `C8_MENTALG` int(11) DEFAULT NULL,
                        `C8_OTHB` int(11) DEFAULT NULL,
                        `C8_OTHG` int(11) DEFAULT NULL,
                        `C1_DUMBB` int(11) DEFAULT NULL,
                        `C1_DUMBG` int(11) DEFAULT NULL,
                        `C2_DUMBB` int(11) DEFAULT NULL,
                        `C2_DUMBG` int(11) DEFAULT NULL,
                        `C3_DUMBB` int(11) DEFAULT NULL,
                        `C3_DUMBG` int(11) DEFAULT NULL,
                        `C4_DUMBB` int(11) DEFAULT NULL,
                        `C4_DUMBG` int(11) DEFAULT NULL,
                        `C5_DUMBB` int(11) DEFAULT NULL,
                        `C5_DUMBG` int(11) DEFAULT NULL,
                        `C6_DUMBB` int(11) DEFAULT NULL,
                        `C6_DUMBG` int(11) DEFAULT NULL,
                        `C7_DUMBB` int(11) DEFAULT NULL,
                        `C7_DUMBG` int(11) DEFAULT NULL,
                        `C8_DUMBB` int(11) DEFAULT NULL,
                        `C8_DUMBG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISABILITY912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISABILITY912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1MULTIB` int(11) DEFAULT NULL,
                        `C1MULTIG` int(11) DEFAULT NULL,
                        `C2MULTIB` int(11) DEFAULT NULL,
                        `C2MULTIG` int(11) DEFAULT NULL,
                        `C3MULTIB` int(11) DEFAULT NULL,
                        `C3MULTIG` int(11) DEFAULT NULL,
                        `C4MULTIB` int(11) DEFAULT NULL,
                        `C4MULTIG` int(11) DEFAULT NULL,
                        `C5MULTIB` int(11) DEFAULT NULL,
                        `C5MULTIG` int(11) DEFAULT NULL,
                        `C6MULTIB` int(11) DEFAULT NULL,
                        `C6MULTIG` int(11) DEFAULT NULL,
                        `C7MULTIB` int(11) DEFAULT NULL,
                        `C7MULTIG` int(11) DEFAULT NULL,
                        `C8MULTIB` int(11) DEFAULT NULL,
                        `C8MULTIG` int(11) DEFAULT NULL,
                        `C9MULTIB` int(11) DEFAULT NULL,
                        `C9MULTIG` int(11) DEFAULT NULL,
                        `C10MULTIB` int(11) DEFAULT NULL,
                        `C10MULTIG` int(11) DEFAULT NULL,
                        `C11MULTIB` int(11) DEFAULT NULL,
                        `C11MULTIG` int(11) DEFAULT NULL,
                        `C12MULTIB` int(11) DEFAULT NULL,
                        `C12MULTIG` int(11) DEFAULT NULL,
                        `C9BLINDB` int(11) DEFAULT NULL,
                        `C9BLINDG` int(11) DEFAULT NULL,
                        `C9DEAFB` int(11) DEFAULT NULL,
                        `C9DEAFG` int(11) DEFAULT NULL,
                        `C9DUMBB` int(11) DEFAULT NULL,
                        `C9DUMBG` int(11) DEFAULT NULL,
                        `C9ORTHOB` int(11) DEFAULT NULL,
                        `C9ORTHOG` int(11) DEFAULT NULL,
                        `C9MENTALB` int(11) DEFAULT NULL,
                        `C9MENTALG` int(11) DEFAULT NULL,
                        `C9OTHB` int(11) DEFAULT NULL,
                        `C9OTHG` int(11) DEFAULT NULL,
                        `C10BLINDB` int(11) DEFAULT NULL,
                        `C10BLINDG` int(11) DEFAULT NULL,
                        `C10DEAFB` int(11) DEFAULT NULL,
                        `C10DEAFG` int(11) DEFAULT NULL,
                        `C10DUMBB` int(11) DEFAULT NULL,
                        `C10DUMBG` int(11) DEFAULT NULL,
                        `C10ORTHOB` int(11) DEFAULT NULL,
                        `C10ORTHOG` int(11) DEFAULT NULL,
                        `C10MENTALB` int(11) DEFAULT NULL,
                        `C10MENTALG` int(11) DEFAULT NULL,
                        `C10OTHB` int(11) DEFAULT NULL,
                        `C10OTHG` int(11) DEFAULT NULL,
                        `C11BLINDB` int(11) DEFAULT NULL,
                        `C11BLINDG` int(11) DEFAULT NULL,
                        `C11DEAFB` int(11) DEFAULT NULL,
                        `C11DEAFG` int(11) DEFAULT NULL,
                        `C11DUMBB` int(11) DEFAULT NULL,
                        `C11DUMBG` int(11) DEFAULT NULL,
                        `C11ORTHOB` int(11) DEFAULT NULL,
                        `C11ORTHOG` int(11) DEFAULT NULL,
                        `C11MENTALB` int(11) DEFAULT NULL,
                        `C11MENTALG` int(11) DEFAULT NULL,
                        `C11OTHB` int(11) DEFAULT NULL,
                        `C11OTHG` int(11) DEFAULT NULL,
                        `C12BLINDB` int(11) DEFAULT NULL,
                        `C12BLINDG` int(11) DEFAULT NULL,
                        `C12DEAFB` int(11) DEFAULT NULL,
                        `C12DEAFG` int(11) DEFAULT NULL,
                        `C12DUMBB` int(11) DEFAULT NULL,
                        `C12DUMBG` int(11) DEFAULT NULL,
                        `C12ORTHOB` int(11) DEFAULT NULL,
                        `C12ORTHOG` int(11) DEFAULT NULL,
                        `C12MENTALB` int(11) DEFAULT NULL,
                        `C12MENTALG` int(11) DEFAULT NULL,
                        `C12OTHB` int(11) DEFAULT NULL,
                        `C12OTHG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTLIST` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_DISTRICT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_DISTRICT` (
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `PRLIMIT` int(11) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `UPRLIMIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `DISTVAR1` varchar(35) DEFAULT NULL,
                        `DISTVAR2` varchar(35) DEFAULT NULL,
                        `DISTVAR3` varchar(35) DEFAULT NULL,
                        `DISTVAR4` varchar(35) DEFAULT NULL,
                        `DISTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EDUBLOCK`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EDUBLOCK` (
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `BLKNAME` varchar(35) DEFAULT NULL,
                        `BLK_TRIB` varchar(1) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRAGEBYCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRAGEBYCASTE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGEID` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `PPB` int(11) DEFAULT NULL,
                        `PPG` int(11) DEFAULT NULL,
                        `C1B` int(11) DEFAULT NULL,
                        `C1G` int(11) DEFAULT NULL,
                        `C2B` int(11) DEFAULT NULL,
                        `C2G` int(11) DEFAULT NULL,
                        `C3B` int(11) DEFAULT NULL,
                        `C3G` int(11) DEFAULT NULL,
                        `C4B` int(11) DEFAULT NULL,
                        `C4G` int(11) DEFAULT NULL,
                        `C5B` int(11) DEFAULT NULL,
                        `C5G` int(11) DEFAULT NULL,
                        `C6B` int(11) DEFAULT NULL,
                        `C6G` int(11) DEFAULT NULL,
                        `C7B` int(11) DEFAULT NULL,
                        `C7G` int(11) DEFAULT NULL,
                        `C8B` int(11) DEFAULT NULL,
                        `C8G` int(11) DEFAULT NULL,
                        `C9B` int(11) DEFAULT NULL,
                        `C9G` int(11) DEFAULT NULL,
                        `C10B` int(11) DEFAULT NULL,
                        `C10G` int(11) DEFAULT NULL,
                        `C11B` int(11) DEFAULT NULL,
                        `C11G` int(11) DEFAULT NULL,
                        `C12B` int(11) DEFAULT NULL,
                        `C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRBYCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRBYCASTEMEDIUM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUMID` int(11) DEFAULT NULL,
                        `CASTEIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `LKG_B` int(11) DEFAULT NULL,
                        `LKG_G` int(11) DEFAULT NULL,
                        `UKG_B` int(11) DEFAULT NULL,
                        `UKG_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(6) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C1GB` int(11) DEFAULT NULL,
                        `C1GG` int(11) DEFAULT NULL,
                        `C2GB` int(11) DEFAULT NULL,
                        `C2GG` int(11) DEFAULT NULL,
                        `C3GB` int(11) DEFAULT NULL,
                        `C3GG` int(11) DEFAULT NULL,
                        `C4GB` int(11) DEFAULT NULL,
                        `C4GG` int(11) DEFAULT NULL,
                        `C5GB` int(11) DEFAULT NULL,
                        `C5GG` int(11) DEFAULT NULL,
                        `C6GB` int(11) DEFAULT NULL,
                        `C6GG` int(11) DEFAULT NULL,
                        `C7GB` int(11) DEFAULT NULL,
                        `C7GG` int(11) DEFAULT NULL,
                        `C8GB` int(11) DEFAULT NULL,
                        `C8GG` int(11) DEFAULT NULL,
                        `C1CB` int(11) DEFAULT NULL,
                        `C1CG` int(11) DEFAULT NULL,
                        `C2CB` int(11) DEFAULT NULL,
                        `C2CG` int(11) DEFAULT NULL,
                        `C3CB` int(11) DEFAULT NULL,
                        `C3CG` int(11) DEFAULT NULL,
                        `C4CB` int(11) DEFAULT NULL,
                        `C4CG` int(11) DEFAULT NULL,
                        `C5CB` int(11) DEFAULT NULL,
                        `C5CG` int(11) DEFAULT NULL,
                        `C6CB` int(11) DEFAULT NULL,
                        `C6CG` int(11) DEFAULT NULL,
                        `C7CB` int(11) DEFAULT NULL,
                        `C7CG` int(11) DEFAULT NULL,
                        `C8CB` int(11) DEFAULT NULL,
                        `C8CG` int(11) DEFAULT NULL,
                        `C1TB` int(11) DEFAULT NULL,
                        `C1TG` int(11) DEFAULT NULL,
                        `C2TB` int(11) DEFAULT NULL,
                        `C2TG` int(11) DEFAULT NULL,
                        `C3TB` int(11) DEFAULT NULL,
                        `C3TG` int(11) DEFAULT NULL,
                        `C4TB` int(11) DEFAULT NULL,
                        `C4TG` int(11) DEFAULT NULL,
                        `C5TB` int(11) DEFAULT NULL,
                        `C5TG` int(11) DEFAULT NULL,
                        `C6TB` int(11) DEFAULT NULL,
                        `C6TG` int(11) DEFAULT NULL,
                        `C7TB` int(11) DEFAULT NULL,
                        `C7TG` int(11) DEFAULT NULL,
                        `C8TB` int(11) DEFAULT NULL,
                        `C8TG` int(11) DEFAULT NULL,
                        `C1OB` int(11) DEFAULT NULL,
                        `C1OG` int(11) DEFAULT NULL,
                        `C2OB` int(11) DEFAULT NULL,
                        `C2OG` int(11) DEFAULT NULL,
                        `C3OB` int(11) DEFAULT NULL,
                        `C3OG` int(11) DEFAULT NULL,
                        `C4OB` int(11) DEFAULT NULL,
                        `C4OG` int(11) DEFAULT NULL,
                        `C5OB` int(11) DEFAULT NULL,
                        `C5OG` int(11) DEFAULT NULL,
                        `C6OB` int(11) DEFAULT NULL,
                        `C6OG` int(11) DEFAULT NULL,
                        `C7OB` int(11) DEFAULT NULL,
                        `C7OG` int(11) DEFAULT NULL,
                        `C8OB` int(11) DEFAULT NULL,
                        `C8OG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `C1_TOTB` int(11) DEFAULT NULL,
                        `C1_TOTG` int(11) DEFAULT NULL,
                        `C2_TOTB` int(11) DEFAULT NULL,
                        `C2_TOTG` int(11) DEFAULT NULL,
                        `C3_TOTB` int(11) DEFAULT NULL,
                        `C3_TOTG` int(11) DEFAULT NULL,
                        `C4_TOTB` int(11) DEFAULT NULL,
                        `C4_TOTG` int(11) DEFAULT NULL,
                        `C5_TOTB` int(11) DEFAULT NULL,
                        `C5_TOTG` int(11) DEFAULT NULL,
                        `C6_TOTB` int(11) DEFAULT NULL,
                        `C6_TOTG` int(11) DEFAULT NULL,
                        `C7_TOTB` int(11) DEFAULT NULL,
                        `C7_TOTG` int(11) DEFAULT NULL,
                        `C8_TOTB` int(11) DEFAULT NULL,
                        `C8_TOTG` int(11) DEFAULT NULL,
                        `CPP_TOTB` int(11) DEFAULT NULL,
                        `CPP_TOTG` int(11) DEFAULT NULL,
                        `C9_TOTB` int(11) DEFAULT NULL,
                        `C9_TOTG` int(11) DEFAULT NULL,
                        `C10_TOTB` int(11) DEFAULT NULL,
                        `C10_TOTG` int(11) DEFAULT NULL,
                        `C11_TOTB` int(11) DEFAULT NULL,
                        `C11_TOTG` int(11) DEFAULT NULL,
                        `C12_TOTB` int(11) DEFAULT NULL,
                        `C12_TOTG` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENRMEDINSTR912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENRMEDINSTR912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `SEQNO` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_L5B` int(11) DEFAULT NULL,
                        `C1_5B` int(11) DEFAULT NULL,
                        `C1_6B` int(11) DEFAULT NULL,
                        `C1_7B` int(11) DEFAULT NULL,
                        `C1_8B` int(11) DEFAULT NULL,
                        `C1_9B` int(11) DEFAULT NULL,
                        `C1_10B` int(11) DEFAULT NULL,
                        `C1_11B` int(11) DEFAULT NULL,
                        `C1_12B` int(11) DEFAULT NULL,
                        `C1_L5G` int(11) DEFAULT NULL,
                        `C1_5G` int(11) DEFAULT NULL,
                        `C1_6G` int(11) DEFAULT NULL,
                        `C1_7G` int(11) DEFAULT NULL,
                        `C1_8G` int(11) DEFAULT NULL,
                        `C1_9G` int(11) DEFAULT NULL,
                        `C1_10G` int(11) DEFAULT NULL,
                        `C1_11G` int(11) DEFAULT NULL,
                        `C1_12G` int(11) DEFAULT NULL,
                        `C2_5B` int(11) DEFAULT NULL,
                        `C2_6B` int(11) DEFAULT NULL,
                        `C2_7B` int(11) DEFAULT NULL,
                        `C2_8B` int(11) DEFAULT NULL,
                        `C2_9B` int(11) DEFAULT NULL,
                        `C2_10B` int(11) DEFAULT NULL,
                        `C2_11B` int(11) DEFAULT NULL,
                        `C2_12B` int(11) DEFAULT NULL,
                        `C2_13B` int(11) DEFAULT NULL,
                        `C2_5G` int(11) DEFAULT NULL,
                        `C2_6G` int(11) DEFAULT NULL,
                        `C2_7G` int(11) DEFAULT NULL,
                        `C2_8G` int(11) DEFAULT NULL,
                        `C2_9G` int(11) DEFAULT NULL,
                        `C2_10G` int(11) DEFAULT NULL,
                        `C2_11G` int(11) DEFAULT NULL,
                        `C2_12G` int(11) DEFAULT NULL,
                        `C2_13G` int(11) DEFAULT NULL,
                        `C3_7B` int(11) DEFAULT NULL,
                        `C3_8B` int(11) DEFAULT NULL,
                        `C3_9B` int(11) DEFAULT NULL,
                        `C3_10B` int(11) DEFAULT NULL,
                        `C3_11B` int(11) DEFAULT NULL,
                        `C3_12B` int(11) DEFAULT NULL,
                        `C3_13B` int(11) DEFAULT NULL,
                        `C3_14B` int(11) DEFAULT NULL,
                        `C3_15B` int(11) DEFAULT NULL,
                        `C3_16B` int(11) DEFAULT NULL,
                        `C3_M16B` int(11) DEFAULT NULL,
                        `C3_7G` int(11) DEFAULT NULL,
                        `C3_8G` int(11) DEFAULT NULL,
                        `C3_9G` int(11) DEFAULT NULL,
                        `C3_10G` int(11) DEFAULT NULL,
                        `C3_11G` int(11) DEFAULT NULL,
                        `C3_12G` int(11) DEFAULT NULL,
                        `C3_13G` int(11) DEFAULT NULL,
                        `C3_14G` int(11) DEFAULT NULL,
                        `C3_15G` int(11) DEFAULT NULL,
                        `C3_16G` int(11) DEFAULT NULL,
                        `C3_M16G` int(11) DEFAULT NULL,
                        `C4_8B` int(11) DEFAULT NULL,
                        `C4_9B` int(11) DEFAULT NULL,
                        `C4_10B` int(11) DEFAULT NULL,
                        `C4_11B` int(11) DEFAULT NULL,
                        `C4_12B` int(11) DEFAULT NULL,
                        `C4_13B` int(11) DEFAULT NULL,
                        `C4_14B` int(11) DEFAULT NULL,
                        `C4_15B` int(11) DEFAULT NULL,
                        `C4_16B` int(11) DEFAULT NULL,
                        `C4_M16B` int(11) DEFAULT NULL,
                        `C4_8G` int(11) DEFAULT NULL,
                        `C4_9G` int(11) DEFAULT NULL,
                        `C4_10G` int(11) DEFAULT NULL,
                        `C4_11G` int(11) DEFAULT NULL,
                        `C4_12G` int(11) DEFAULT NULL,
                        `C4_13G` int(11) DEFAULT NULL,
                        `C4_14G` int(11) DEFAULT NULL,
                        `C4_15G` int(11) DEFAULT NULL,
                        `C4_16G` int(11) DEFAULT NULL,
                        `C4_M16G` int(11) DEFAULT NULL,
                        `C5_9B` int(11) DEFAULT NULL,
                        `C5_10B` int(11) DEFAULT NULL,
                        `C5_11B` int(11) DEFAULT NULL,
                        `C5_12B` int(11) DEFAULT NULL,
                        `C5_13B` int(11) DEFAULT NULL,
                        `C5_14B` int(11) DEFAULT NULL,
                        `C5_15B` int(11) DEFAULT NULL,
                        `C5_16B` int(11) DEFAULT NULL,
                        `C5_M16B` int(11) DEFAULT NULL,
                        `C5_9G` int(11) DEFAULT NULL,
                        `C5_10G` int(11) DEFAULT NULL,
                        `C5_11G` int(11) DEFAULT NULL,
                        `C5_12G` int(11) DEFAULT NULL,
                        `C5_13G` int(11) DEFAULT NULL,
                        `C5_14G` int(11) DEFAULT NULL,
                        `C5_15G` int(11) DEFAULT NULL,
                        `C5_16G` int(11) DEFAULT NULL,
                        `C5_M16G` int(11) DEFAULT NULL,
                        `C6_10B` int(11) DEFAULT NULL,
                        `C6_11B` int(11) DEFAULT NULL,
                        `C6_12B` int(11) DEFAULT NULL,
                        `C6_13B` int(11) DEFAULT NULL,
                        `C6_14B` int(11) DEFAULT NULL,
                        `C6_15B` int(11) DEFAULT NULL,
                        `C6_16B` int(11) DEFAULT NULL,
                        `C6_M16B` int(11) DEFAULT NULL,
                        `C6_10G` int(11) DEFAULT NULL,
                        `C6_11G` int(11) DEFAULT NULL,
                        `C6_12G` int(11) DEFAULT NULL,
                        `C6_13G` int(11) DEFAULT NULL,
                        `C6_14G` int(11) DEFAULT NULL,
                        `C6_15G` int(11) DEFAULT NULL,
                        `C6_16G` int(11) DEFAULT NULL,
                        `C6_M16G` int(11) DEFAULT NULL,
                        `C7_11B` int(11) DEFAULT NULL,
                        `C7_12B` int(11) DEFAULT NULL,
                        `C7_13B` int(11) DEFAULT NULL,
                        `C7_14B` int(11) DEFAULT NULL,
                        `C7_15B` int(11) DEFAULT NULL,
                        `C7_16B` int(11) DEFAULT NULL,
                        `C7_M16B` int(11) DEFAULT NULL,
                        `C7_11G` int(11) DEFAULT NULL,
                        `C7_12G` int(11) DEFAULT NULL,
                        `C7_13G` int(11) DEFAULT NULL,
                        `C7_14G` int(11) DEFAULT NULL,
                        `C7_15G` int(11) DEFAULT NULL,
                        `C7_16G` int(11) DEFAULT NULL,
                        `C7_M16G` int(11) DEFAULT NULL,
                        `C8_12B` int(11) DEFAULT NULL,
                        `C8_13B` int(11) DEFAULT NULL,
                        `C8_14B` int(11) DEFAULT NULL,
                        `C8_15B` int(11) DEFAULT NULL,
                        `C8_16B` int(11) DEFAULT NULL,
                        `C8_M16B` int(11) DEFAULT NULL,
                        `C8_12G` int(11) DEFAULT NULL,
                        `C8_13G` int(11) DEFAULT NULL,
                        `C8_14G` int(11) DEFAULT NULL,
                        `C8_15G` int(11) DEFAULT NULL,
                        `C8_16G` int(11) DEFAULT NULL,
                        `C8_M16G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLAGE912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLAGE912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PPB_L5` int(11) DEFAULT NULL,
                        `PPG_L5` int(11) DEFAULT NULL,
                        `PPB_5` int(11) DEFAULT NULL,
                        `PPG_5` int(11) DEFAULT NULL,
                        `PPB_6` int(11) DEFAULT NULL,
                        `PPG_6` int(11) DEFAULT NULL,
                        `PPB_7` int(11) DEFAULT NULL,
                        `PPG_7` int(11) DEFAULT NULL,
                        `PPB_8` int(11) DEFAULT NULL,
                        `PPG_8` int(11) DEFAULT NULL,
                        `PPB_9` int(11) DEFAULT NULL,
                        `PPG_9` int(11) DEFAULT NULL,
                        `PPB_10` int(11) DEFAULT NULL,
                        `PPG_10` int(11) DEFAULT NULL,
                        `PPB_11` int(11) DEFAULT NULL,
                        `PPG_11` int(11) DEFAULT NULL,
                        `PPB_12` int(11) DEFAULT NULL,
                        `PPG_12` int(11) DEFAULT NULL,
                        `C9B_13` int(11) DEFAULT NULL,
                        `C9G_13` int(11) DEFAULT NULL,
                        `C9B_14` int(11) DEFAULT NULL,
                        `C9G_14` int(11) DEFAULT NULL,
                        `C9B_M14` int(11) DEFAULT NULL,
                        `C9G_M14` int(11) DEFAULT NULL,
                        `C9B_16` int(11) DEFAULT NULL,
                        `C9G_16` int(11) DEFAULT NULL,
                        `C9B_M16` int(11) DEFAULT NULL,
                        `C9G_M16` int(11) DEFAULT NULL,
                        `C10B_14` int(11) DEFAULT NULL,
                        `C10G_14` int(11) DEFAULT NULL,
                        `C10B_M14` int(11) DEFAULT NULL,
                        `C10G_M14` int(11) DEFAULT NULL,
                        `C10B_16` int(11) DEFAULT NULL,
                        `C10G_16` int(11) DEFAULT NULL,
                        `C10B_M16` int(11) DEFAULT NULL,
                        `C10G_M16` int(11) DEFAULT NULL,
                        `C11B_M14` int(11) DEFAULT NULL,
                        `C11G_M14` int(11) DEFAULT NULL,
                        `C11B_16` int(11) DEFAULT NULL,
                        `C11G_16` int(11) DEFAULT NULL,
                        `C11B_M16` int(11) DEFAULT NULL,
                        `C11G_M16` int(11) DEFAULT NULL,
                        `C12B_16` int(11) DEFAULT NULL,
                        `C12G_16` int(11) DEFAULT NULL,
                        `C12B_M16` int(11) DEFAULT NULL,
                        `C12G_M16` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_SEC` int(11) DEFAULT NULL,
                        `C2_SEC` int(11) DEFAULT NULL,
                        `C3_SEC` int(11) DEFAULT NULL,
                        `C4_SEC` int(11) DEFAULT NULL,
                        `C5_SEC` int(11) DEFAULT NULL,
                        `C6_SEC` int(11) DEFAULT NULL,
                        `C7_SEC` int(11) DEFAULT NULL,
                        `C8_SEC` int(11) DEFAULT NULL,
                        `C1_GB` int(11) DEFAULT NULL,
                        `C1_GG` int(11) DEFAULT NULL,
                        `C2_GB` int(11) DEFAULT NULL,
                        `C2_GG` int(11) DEFAULT NULL,
                        `C3_GB` int(11) DEFAULT NULL,
                        `C3_GG` int(11) DEFAULT NULL,
                        `C4_GB` int(11) DEFAULT NULL,
                        `C4_GG` int(11) DEFAULT NULL,
                        `C5_GB` int(11) DEFAULT NULL,
                        `C5_GG` int(11) DEFAULT NULL,
                        `C6_GB` int(11) DEFAULT NULL,
                        `C6_GG` int(11) DEFAULT NULL,
                        `C7_GB` int(11) DEFAULT NULL,
                        `C7_GG` int(11) DEFAULT NULL,
                        `C8_GB` int(11) DEFAULT NULL,
                        `C8_GG` int(11) DEFAULT NULL,
                        `C1_CB` int(11) DEFAULT NULL,
                        `C1_CG` int(11) DEFAULT NULL,
                        `C2_CB` int(11) DEFAULT NULL,
                        `C2_CG` int(11) DEFAULT NULL,
                        `C3_CB` int(11) DEFAULT NULL,
                        `C3_CG` int(11) DEFAULT NULL,
                        `C4_CB` int(11) DEFAULT NULL,
                        `C4_CG` int(11) DEFAULT NULL,
                        `C5_CB` int(11) DEFAULT NULL,
                        `C5_CG` int(11) DEFAULT NULL,
                        `C6_CB` int(11) DEFAULT NULL,
                        `C6_CG` int(11) DEFAULT NULL,
                        `C7_CB` int(11) DEFAULT NULL,
                        `C7_CG` int(11) DEFAULT NULL,
                        `C8_CB` int(11) DEFAULT NULL,
                        `C8_CG` int(11) DEFAULT NULL,
                        `C1_TB` int(11) DEFAULT NULL,
                        `C1_TG` int(11) DEFAULT NULL,
                        `C2_TB` int(11) DEFAULT NULL,
                        `C2_TG` int(11) DEFAULT NULL,
                        `C3_TB` int(11) DEFAULT NULL,
                        `C3_TG` int(11) DEFAULT NULL,
                        `C4_TB` int(11) DEFAULT NULL,
                        `C4_TG` int(11) DEFAULT NULL,
                        `C5_TB` int(11) DEFAULT NULL,
                        `C5_TG` int(11) DEFAULT NULL,
                        `C6_TB` int(11) DEFAULT NULL,
                        `C6_TG` int(11) DEFAULT NULL,
                        `C7_TB` int(11) DEFAULT NULL,
                        `C7_TG` int(11) DEFAULT NULL,
                        `C8_TB` int(11) DEFAULT NULL,
                        `C8_TG` int(11) DEFAULT NULL,
                        `C1_OB` int(11) DEFAULT NULL,
                        `C1_OG` int(11) DEFAULT NULL,
                        `C2_OB` int(11) DEFAULT NULL,
                        `C2_OG` int(11) DEFAULT NULL,
                        `C3_OB` int(11) DEFAULT NULL,
                        `C3_OG` int(11) DEFAULT NULL,
                        `C4_OB` int(11) DEFAULT NULL,
                        `C4_OG` int(11) DEFAULT NULL,
                        `C5_OB` int(11) DEFAULT NULL,
                        `C5_OG` int(11) DEFAULT NULL,
                        `C6_OB` int(11) DEFAULT NULL,
                        `C6_OG` int(11) DEFAULT NULL,
                        `C7_OB` int(11) DEFAULT NULL,
                        `C7_OG` int(11) DEFAULT NULL,
                        `C8_OB` int(11) DEFAULT NULL,
                        `C8_OG` int(11) DEFAULT NULL,
                        `C1_OTH1B` int(11) DEFAULT NULL,
                        `C1_OTH1G` int(11) DEFAULT NULL,
                        `C2_OTH1B` int(11) DEFAULT NULL,
                        `C2_OTH1G` int(11) DEFAULT NULL,
                        `C3_OTH1B` int(11) DEFAULT NULL,
                        `C3_OTH1G` int(11) DEFAULT NULL,
                        `C4_OTH1B` int(11) DEFAULT NULL,
                        `C4_OTH1G` int(11) DEFAULT NULL,
                        `C5_OTH1B` int(11) DEFAULT NULL,
                        `C5_OTH1G` int(11) DEFAULT NULL,
                        `C6_OTH1B` int(11) DEFAULT NULL,
                        `C6_OTH1G` int(11) DEFAULT NULL,
                        `C7_OTH1B` int(11) DEFAULT NULL,
                        `C7_OTH1G` int(11) DEFAULT NULL,
                        `C8_OTH1B` int(11) DEFAULT NULL,
                        `C8_OTH1G` int(11) DEFAULT NULL,
                        `C1_OTH2B` int(11) DEFAULT NULL,
                        `C1_OTH2G` int(11) DEFAULT NULL,
                        `C2_OTH2B` int(11) DEFAULT NULL,
                        `C2_OTH2G` int(11) DEFAULT NULL,
                        `C3_OTH2B` int(11) DEFAULT NULL,
                        `C3_OTH2G` int(11) DEFAULT NULL,
                        `C4_OTH2B` int(11) DEFAULT NULL,
                        `C4_OTH2G` int(11) DEFAULT NULL,
                        `C5_OTH2B` int(11) DEFAULT NULL,
                        `C5_OTH2G` int(11) DEFAULT NULL,
                        `C6_OTH2B` int(11) DEFAULT NULL,
                        `C6_OTH2G` int(11) DEFAULT NULL,
                        `C7_OTH2B` int(11) DEFAULT NULL,
                        `C7_OTH2G` int(11) DEFAULT NULL,
                        `C8_OTH2B` int(11) DEFAULT NULL,
                        `C8_OTH2G` int(11) DEFAULT NULL,
                        `C1_OTH3B` int(11) DEFAULT NULL,
                        `C1_OTH3G` int(11) DEFAULT NULL,
                        `C2_OTH3B` int(11) DEFAULT NULL,
                        `C2_OTH3G` int(11) DEFAULT NULL,
                        `C3_OTH3B` int(11) DEFAULT NULL,
                        `C3_OTH3G` int(11) DEFAULT NULL,
                        `C4_OTH3B` int(11) DEFAULT NULL,
                        `C4_OTH3G` int(11) DEFAULT NULL,
                        `C5_OTH3B` int(11) DEFAULT NULL,
                        `C5_OTH3G` int(11) DEFAULT NULL,
                        `C6_OTH3B` int(11) DEFAULT NULL,
                        `C6_OTH3G` int(11) DEFAULT NULL,
                        `C7_OTH3B` int(11) DEFAULT NULL,
                        `C7_OTH3G` int(11) DEFAULT NULL,
                        `C8_OTH3B` int(11) DEFAULT NULL,
                        `C8_OTH3G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `CPP_B` int(11) DEFAULT NULL,
                        `CPP_G` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENROLMENT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENROLMENT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9_SEC` int(11) DEFAULT NULL,
                        `C10_SEC` int(11) DEFAULT NULL,
                        `C11_SEC` int(11) DEFAULT NULL,
                        `C12_SEC` int(11) DEFAULT NULL,
                        `C1MB` int(11) DEFAULT NULL,
                        `C1MG` int(11) DEFAULT NULL,
                        `C2MB` int(11) DEFAULT NULL,
                        `C2MG` int(11) DEFAULT NULL,
                        `C3MB` int(11) DEFAULT NULL,
                        `C3MG` int(11) DEFAULT NULL,
                        `C4MB` int(11) DEFAULT NULL,
                        `C4MG` int(11) DEFAULT NULL,
                        `C5MB` int(11) DEFAULT NULL,
                        `C5MG` int(11) DEFAULT NULL,
                        `C6MB` int(11) DEFAULT NULL,
                        `C6MG` int(11) DEFAULT NULL,
                        `C7MB` int(11) DEFAULT NULL,
                        `C7MG` int(11) DEFAULT NULL,
                        `C8MB` int(11) DEFAULT NULL,
                        `C8MG` int(11) DEFAULT NULL,
                        `C9MB` int(11) DEFAULT NULL,
                        `C9MG` int(11) DEFAULT NULL,
                        `C10MB` int(11) DEFAULT NULL,
                        `C10MG` int(11) DEFAULT NULL,
                        `C11MB` int(11) DEFAULT NULL,
                        `C11MG` int(11) DEFAULT NULL,
                        `C12MB` int(11) DEFAULT NULL,
                        `C12MG` int(11) DEFAULT NULL,
                        `CPPGB` int(11) DEFAULT NULL,
                        `CPPGG` int(11) DEFAULT NULL,
                        `CPPCB` int(11) DEFAULT NULL,
                        `CPPCG` int(11) DEFAULT NULL,
                        `CPPTB` int(11) DEFAULT NULL,
                        `CPPTG` int(11) DEFAULT NULL,
                        `CPPOB` int(11) DEFAULT NULL,
                        `CPPOG` int(11) DEFAULT NULL,
                        `CPPMB` int(11) DEFAULT NULL,
                        `CPPMG` int(11) DEFAULT NULL,
                        `C9GB` int(11) DEFAULT NULL,
                        `C9GG` int(11) DEFAULT NULL,
                        `C9CB` int(11) DEFAULT NULL,
                        `C9CG` int(11) DEFAULT NULL,
                        `C9TB` int(11) DEFAULT NULL,
                        `C9TG` int(11) DEFAULT NULL,
                        `C9OB` int(11) DEFAULT NULL,
                        `C9OG` int(11) DEFAULT NULL,
                        `C10GB` int(11) DEFAULT NULL,
                        `C10GG` int(11) DEFAULT NULL,
                        `C10CB` int(11) DEFAULT NULL,
                        `C10CG` int(11) DEFAULT NULL,
                        `C10TB` int(11) DEFAULT NULL,
                        `C10TG` int(11) DEFAULT NULL,
                        `C10OB` int(11) DEFAULT NULL,
                        `C10OG` int(11) DEFAULT NULL,
                        `C11GB` int(11) DEFAULT NULL,
                        `C11GG` int(11) DEFAULT NULL,
                        `C11CB` int(11) DEFAULT NULL,
                        `C11CG` int(11) DEFAULT NULL,
                        `C11TB` int(11) DEFAULT NULL,
                        `C11TG` int(11) DEFAULT NULL,
                        `C11OB` int(11) DEFAULT NULL,
                        `C11OG` int(11) DEFAULT NULL,
                        `C12GB` int(11) DEFAULT NULL,
                        `C12GG` int(11) DEFAULT NULL,
                        `C12CB` int(11) DEFAULT NULL,
                        `C12CG` int(11) DEFAULT NULL,
                        `C12TB` int(11) DEFAULT NULL,
                        `C12TG` int(11) DEFAULT NULL,
                        `C12OB` int(11) DEFAULT NULL,
                        `C12OG` int(11) DEFAULT NULL,
                        `V1CPPB` int(11) DEFAULT NULL,
                        `V1CPPG` int(11) DEFAULT NULL,
                        `V2CPPB` int(11) DEFAULT NULL,
                        `V2CPPG` int(11) DEFAULT NULL,
                        `V3CPPB` int(11) DEFAULT NULL,
                        `V3CPPG` int(11) DEFAULT NULL,
                        `V1C9B` int(11) DEFAULT NULL,
                        `V1C9G` int(11) DEFAULT NULL,
                        `V2C9B` int(11) DEFAULT NULL,
                        `V2C9G` int(11) DEFAULT NULL,
                        `V3C9B` int(11) DEFAULT NULL,
                        `V3C9G` int(11) DEFAULT NULL,
                        `V1C10B` int(11) DEFAULT NULL,
                        `V1C10G` int(11) DEFAULT NULL,
                        `V2C10B` int(11) DEFAULT NULL,
                        `V2C10G` int(11) DEFAULT NULL,
                        `V3C10B` int(11) DEFAULT NULL,
                        `V3C10G` int(11) DEFAULT NULL,
                        `V1C11B` int(11) DEFAULT NULL,
                        `V1C11G` int(11) DEFAULT NULL,
                        `V2C11B` int(11) DEFAULT NULL,
                        `V2C11G` int(11) DEFAULT NULL,
                        `V3C11B` int(11) DEFAULT NULL,
                        `V3C11G` int(11) DEFAULT NULL,
                        `V1C12B` int(11) DEFAULT NULL,
                        `V1C12G` int(11) DEFAULT NULL,
                        `V2C12B` int(11) DEFAULT NULL,
                        `V2C12G` int(11) DEFAULT NULL,
                        `V3C12B` int(11) DEFAULT NULL,
                        `V3C12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `ENTITY_ID` int(11) DEFAULT NULL,
                        `ENTITY_DESC` varchar(75) DEFAULT NULL,
                        `ENTITY_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `LANGUAGE_DESC` varchar(150) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_ENTITYTABLEMASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_ENTITYTABLEMASTER` (
                        `TABLE_ID` int(11) DEFAULT NULL,
                        `TABLE_DESC` varchar(75) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EREPBYSTREAM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EREPBYSTREAM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `STREAMID` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `CASTEID` int(11) DEFAULT NULL,
                        `EC11_B` int(11) DEFAULT NULL,
                        `EC11_G` int(11) DEFAULT NULL,
                        `EC12_B` int(11) DEFAULT NULL,
                        `EC12_G` int(11) DEFAULT NULL,
                        `RC11_B` int(11) DEFAULT NULL,
                        `RC11_G` int(11) DEFAULT NULL,
                        `RC12_B` int(11) DEFAULT NULL,
                        `RC12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMINATION09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMINATION09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C5B_GEN` int(11) DEFAULT NULL,
                        `C5G_GEN` int(11) DEFAULT NULL,
                        `C8B_GEN` int(11) DEFAULT NULL,
                        `C8G_GEN` int(11) DEFAULT NULL,
                        `C5B_SC` int(11) DEFAULT NULL,
                        `C5G_SC` int(11) DEFAULT NULL,
                        `C8B_SC` int(11) DEFAULT NULL,
                        `C8G_SC` int(11) DEFAULT NULL,
                        `C5B_ST` int(11) DEFAULT NULL,
                        `C5G_ST` int(11) DEFAULT NULL,
                        `C8B_ST` int(11) DEFAULT NULL,
                        `C8G_ST` int(11) DEFAULT NULL,
                        `C5B_OBC` int(11) DEFAULT NULL,
                        `C5G_OBC` int(11) DEFAULT NULL,
                        `C8B_OBC` int(11) DEFAULT NULL,
                        `C8G_OBC` int(11) DEFAULT NULL,
                        `C5B_MIN` int(11) DEFAULT NULL,
                        `C5G_MIN` int(11) DEFAULT NULL,
                        `C8B_MIN` int(11) DEFAULT NULL,
                        `C8G_MIN` int(11) DEFAULT NULL,
                        `C10B_GEN` int(11) DEFAULT NULL,
                        `C10G_GEN` int(11) DEFAULT NULL,
                        `C12B_GEN` int(11) DEFAULT NULL,
                        `C12G_GEN` int(11) DEFAULT NULL,
                        `C10B_SC` int(11) DEFAULT NULL,
                        `C10G_SC` int(11) DEFAULT NULL,
                        `C12B_SC` int(11) DEFAULT NULL,
                        `C12G_SC` int(11) DEFAULT NULL,
                        `C10B_ST` int(11) DEFAULT NULL,
                        `C10G_ST` int(11) DEFAULT NULL,
                        `C12B_ST` int(11) DEFAULT NULL,
                        `C12G_ST` int(11) DEFAULT NULL,
                        `C10B_OBC` int(11) DEFAULT NULL,
                        `C10G_OBC` int(11) DEFAULT NULL,
                        `C12B_OBC` int(11) DEFAULT NULL,
                        `C12G_OBC` int(11) DEFAULT NULL,
                        `C10B_MIN` int(11) DEFAULT NULL,
                        `C10G_MIN` int(11) DEFAULT NULL,
                        `C12B_MIN` int(11) DEFAULT NULL,
                        `C12G_MIN` int(11) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C5_ENROLB` int(11) DEFAULT NULL,
                        `C5_ENROLG` int(11) DEFAULT NULL,
                        `C7_ENROLB` int(11) DEFAULT NULL,
                        `C7_ENROLG` int(11) DEFAULT NULL,
                        `C5_APPEARB` int(11) DEFAULT NULL,
                        `C5_APPEARG` int(11) DEFAULT NULL,
                        `C7_APPEARB` int(11) DEFAULT NULL,
                        `C7_APPEARG` int(11) DEFAULT NULL,
                        `C5_PASSB` int(11) DEFAULT NULL,
                        `C5_PASSG` int(11) DEFAULT NULL,
                        `C7_PASSB` int(11) DEFAULT NULL,
                        `C7_PASSG` int(11) DEFAULT NULL,
                        `C5_M60B` int(11) DEFAULT NULL,
                        `C5_M60G` int(11) DEFAULT NULL,
                        `C7_M60B` int(11) DEFAULT NULL,
                        `C7_M60G` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_EXAMRESULT912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_EXAMRESULT912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ENROLSC5B` int(11) DEFAULT NULL,
                        `ENROLSC5G` int(11) DEFAULT NULL,
                        `ENROLSC7B` int(11) DEFAULT NULL,
                        `ENROLSC7G` int(11) DEFAULT NULL,
                        `APPEARSC5B` int(11) DEFAULT NULL,
                        `APPEARSC5G` int(11) DEFAULT NULL,
                        `APPEARSC7B` int(11) DEFAULT NULL,
                        `APPEARSC7G` int(11) DEFAULT NULL,
                        `TPASSEDSC5B` int(11) DEFAULT NULL,
                        `TPASSEDSC5G` int(11) DEFAULT NULL,
                        `TPASSEDSC7B` int(11) DEFAULT NULL,
                        `TPASSEDSC7G` int(11) DEFAULT NULL,
                        `PASSM60SC5B` int(11) DEFAULT NULL,
                        `PASSM60SC5G` int(11) DEFAULT NULL,
                        `PASSM60SC7B` int(11) DEFAULT NULL,
                        `PASSM60SC7G` int(11) DEFAULT NULL,
                        `ENROLST5B` int(11) DEFAULT NULL,
                        `ENROLST5G` int(11) DEFAULT NULL,
                        `ENROLST7B` int(11) DEFAULT NULL,
                        `ENROLST7G` int(11) DEFAULT NULL,
                        `APPEARST5B` int(11) DEFAULT NULL,
                        `APPEARST5G` int(11) DEFAULT NULL,
                        `APPEARST7B` int(11) DEFAULT NULL,
                        `APPEARST7G` int(11) DEFAULT NULL,
                        `TPASSEDST5B` int(11) DEFAULT NULL,
                        `TPASSEDST5G` int(11) DEFAULT NULL,
                        `TPASSEDST7B` int(11) DEFAULT NULL,
                        `TPASSEDST7G` int(11) DEFAULT NULL,
                        `PASSM60ST5B` int(11) DEFAULT NULL,
                        `PASSM60ST5G` int(11) DEFAULT NULL,
                        `PASSM60ST7B` int(11) DEFAULT NULL,
                        `PASSM60ST7G` int(11) DEFAULT NULL,
                        `ENROL10B` int(11) DEFAULT NULL,
                        `ENROL10G` int(11) DEFAULT NULL,
                        `APPEAR10B` int(11) DEFAULT NULL,
                        `APPEAR10G` int(11) DEFAULT NULL,
                        `TPASSED10B` int(11) DEFAULT NULL,
                        `TPASSED10G` int(11) DEFAULT NULL,
                        `PASSM6010B` int(11) DEFAULT NULL,
                        `PASSM6010G` int(11) DEFAULT NULL,
                        `ENROL12B` int(11) DEFAULT NULL,
                        `ENROL12G` int(11) DEFAULT NULL,
                        `APPEAR12B` int(11) DEFAULT NULL,
                        `APPEAR12G` int(11) DEFAULT NULL,
                        `TPASSED12B` int(11) DEFAULT NULL,
                        `TPASSED12G` int(11) DEFAULT NULL,
                        `PASSM6012B` int(11) DEFAULT NULL,
                        `PASSM6012G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FACILITIES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FACILITIES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `ITEMVALUE` int(11) DEFAULT NULL,
                        `ITEMVALUE1` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_FUNDS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_FUNDS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `AMT_R` double DEFAULT NULL,
                        `AMT_U` double DEFAULT NULL,
                        `AMT_S` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_HABITATION`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_HABITATION` (
                        `HABCD` varchar(11) DEFAULT NULL,
                        `HABNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `HABVAR1` varchar(35) DEFAULT NULL,
                        `HABVAR2` varchar(35) DEFAULT NULL,
                        `HABVAR3` varchar(35) DEFAULT NULL,
                        `HABVAR4` varchar(35) DEFAULT NULL,
                        `HABVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_INCENTIVES09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_INCENTIVES09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `GRADEPRUPR` int(11) DEFAULT NULL,
                        `INCENT_TYPE` int(11) DEFAULT NULL,
                        `GEN_B` int(11) DEFAULT NULL,
                        `GEN_G` int(11) DEFAULT NULL,
                        `SC_B` int(11) DEFAULT NULL,
                        `SC_G` int(11) DEFAULT NULL,
                        `ST_B` int(11) DEFAULT NULL,
                        `ST_G` int(11) DEFAULT NULL,
                        `OBC_B` int(11) DEFAULT NULL,
                        `OBC_G` int(11) DEFAULT NULL,
                        `MIN_B` int(11) DEFAULT NULL,
                        `MIN_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_KEYVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_KEYVAR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `DISTNAME` varchar(35) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MAPPING`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MAPPING` (
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SCHCD_DISE` varchar(11) DEFAULT NULL,
                        `SCHCD_SEMIS` varchar(11) DEFAULT NULL,
                        `MAPREMARKS` varchar(100) DEFAULT NULL,
                        `METHOD` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `DISTHQ` int(11) DEFAULT NULL,
                        `DISTCRC` int(11) DEFAULT NULL,
                        `ESTDYEAR` int(11) DEFAULT NULL,
                        `LOWCLASS` int(11) DEFAULT NULL,
                        `HIGHCLASS` int(11) DEFAULT NULL,
                        `PPSEC_YN` int(11) DEFAULT NULL,
                        `PPSTUDENT` int(11) DEFAULT NULL,
                        `PPTEACHER` int(11) DEFAULT NULL,
                        `WORKDAYS` int(11) DEFAULT NULL,
                        `SCHRES_YN` int(11) DEFAULT NULL,
                        `RESITYPE` int(11) DEFAULT NULL,
                        `SCHSHI_YN` int(11) DEFAULT NULL,
                        `NOINSPECT` int(11) DEFAULT NULL,
                        `VISITSCRC` int(11) DEFAULT NULL,
                        `VISITSBRC` int(11) DEFAULT NULL,
                        `CONTI_R` int(11) DEFAULT NULL,
                        `CONTI_E` int(11) DEFAULT NULL,
                        `TLM_R` int(11) DEFAULT NULL,
                        `TLM_E` int(11) DEFAULT NULL,
                        `FUNDS_E` int(11) DEFAULT NULL,
                        `FUNDS_R` int(11) DEFAULT NULL,
                        `OSRC_E` int(11) DEFAULT NULL,
                        `OSRC_R` int(11) DEFAULT NULL,
                        `TCHSAN` int(11) DEFAULT NULL,
                        `TCHPOS` int(11) DEFAULT NULL,
                        `PARAPOS` int(11) DEFAULT NULL,
                        `NTPOS` int(11) DEFAULT NULL,
                        `MEDINSTR1` int(11) DEFAULT NULL,
                        `MEDINSTR2` int(11) DEFAULT NULL,
                        `MEDINSTR3` int(11) DEFAULT NULL,
                        `MEDINSTR4` int(11) DEFAULT NULL,
                        `SUPVAR1` varchar(30) DEFAULT NULL,
                        `SUPVAR2` varchar(30) DEFAULT NULL,
                        `SUPVAR3` varchar(30) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `EDUBLKCD` varchar(6) DEFAULT NULL,
                        `HABITCD` varchar(11) DEFAULT NULL,
                        `ACONSTCD` varchar(7) DEFAULT NULL,
                        `MUNICIPALCD` varchar(7) DEFAULT NULL,
                        `CITY` varchar(7) DEFAULT NULL,
                        `SCHMNTCGRANT_R` int(11) DEFAULT NULL,
                        `SCHMNTCGRANT_E` int(11) DEFAULT NULL,
                        `TLE_R` int(11) DEFAULT NULL,
                        `TLE_E` int(11) DEFAULT NULL,
                        `SCHSTATUS` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLUCD` varchar(10) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `PANCD` varchar(9) DEFAULT NULL,
                        `SCHTYPES` int(11) DEFAULT NULL,
                        `SCHTYPEHS` int(11) DEFAULT NULL,
                        `SCHMGTS` int(11) DEFAULT NULL,
                        `SCHMGTHS` int(11) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL,
                        `DISTP` int(11) DEFAULT NULL,
                        `DISTU` int(11) DEFAULT NULL,
                        `DISTS` int(11) DEFAULT NULL,
                        `DISTH` int(11) DEFAULT NULL,
                        `MEDS1` int(11) DEFAULT NULL,
                        `MEDS2` int(11) DEFAULT NULL,
                        `MEDS3` int(11) DEFAULT NULL,
                        `MEDS4` int(11) DEFAULT NULL,
                        `MEDH1` int(11) DEFAULT NULL,
                        `MEDH2` int(11) DEFAULT NULL,
                        `MEDH3` int(11) DEFAULT NULL,
                        `MEDH4` int(11) DEFAULT NULL,
                        `BOARDSEC` int(11) DEFAULT NULL,
                        `BOARDHSEC` int(11) DEFAULT NULL,
                        `YEARRECOGS` int(11) DEFAULT NULL,
                        `YEARRECOGH` int(11) DEFAULT NULL,
                        `YEARUPGRDS` int(11) DEFAULT NULL,
                        `YEARUPGRDH` int(11) DEFAULT NULL,
                        `SCHRESUPR_YN` int(11) DEFAULT NULL,
                        `SCHRESSEC_YN` int(11) DEFAULT NULL,
                        `SCHRESHSEC_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MASTER09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MASTER09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `POSTALADDR` varchar(100) DEFAULT NULL,
                        `STDCODE1` varchar(6) DEFAULT NULL,
                        `PHONE1` varchar(8) DEFAULT NULL,
                        `MOBILE1` varchar(11) DEFAULT NULL,
                        `STDCODE2` varchar(6) DEFAULT NULL,
                        `PHONE2` varchar(8) DEFAULT NULL,
                        `MOBILE2` varchar(11) DEFAULT NULL,
                        `EMAIL` varchar(50) DEFAULT NULL,
                        `WEBSITE` varchar(50) DEFAULT NULL,
                        `PARASANCT_ELE` int(11) DEFAULT NULL,
                        `TCHSANCT_SEC` int(11) DEFAULT NULL,
                        `TCHPOSN_SEC` int(11) DEFAULT NULL,
                        `PARASANCT_SEC` int(11) DEFAULT NULL,
                        `PARAPOSN_SEC` int(11) DEFAULT NULL,
                        `TCHSANCT_HSEC` int(11) DEFAULT NULL,
                        `TCHPOSN_HSEC` int(11) DEFAULT NULL,
                        `PARASANCT_HSEC` int(11) DEFAULT NULL,
                        `PARAPOSN_HSEC` int(11) DEFAULT NULL,
                        `NTSANCT_SEC` int(11) DEFAULT NULL,
                        `NTPOSN_SEC` int(11) DEFAULT NULL,
                        `NTSANCT_HSEC` int(11) DEFAULT NULL,
                        `NTPOSN_HSEC` int(11) DEFAULT NULL,
                        `ANGANWADI_YN` int(11) DEFAULT NULL,
                        `ANGANWADI_STU` int(11) DEFAULT NULL,
                        `ANGANWADI_TCH` int(11) DEFAULT NULL,
                        `LATDEG` int(11) DEFAULT NULL,
                        `LATMIN` int(11) DEFAULT NULL,
                        `LATSEC` int(11) DEFAULT NULL,
                        `LONDEG` int(11) DEFAULT NULL,
                        `LONMIN` int(11) DEFAULT NULL,
                        `LONSEC` int(11) DEFAULT NULL,
                        `APPROACHBYROAD` int(11) DEFAULT NULL,
                        `YEARRECOG` int(11) DEFAULT NULL,
                        `YEARUPGRD` int(11) DEFAULT NULL,
                        `CWSNSCH_YN` int(11) DEFAULT NULL,
                        `VISITSRTCWSN` int(11) DEFAULT NULL,
                        `TCHREGU_UPR` int(11) DEFAULT NULL,
                        `TCHPARA_UPR` int(11) DEFAULT NULL,
                        `TCHPART_UPR` int(11) DEFAULT NULL,
                        `TCHSANCT_PR` int(11) DEFAULT NULL,
                        `PARASANCT_PR` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MDM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MDM` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEALS_SUPPLIER` int(11) DEFAULT NULL,
                        `SUPPLIER_NAME` varchar(100) DEFAULT NULL,
                        `MDM_MAINTAINER` int(11) DEFAULT NULL,
                        `TAGGED_YN` int(11) DEFAULT NULL,
                        `TAGGED_TOSCH` varchar(7) DEFAULT NULL,
                        `TAGGED_TOCRC` varchar(6) DEFAULT NULL,
                        `KITSHED` int(11) DEFAULT NULL,
                        `FUEL_USED` int(11) DEFAULT NULL,
                        `WATER_YN` int(11) DEFAULT NULL,
                        `STOREROOM_YN` int(11) DEFAULT NULL,
                        `KITCHEN_TYPE` int(11) DEFAULT NULL,
                        `DAYS_WITHFOOD` int(11) DEFAULT NULL,
                        `DAYS_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `REASON_WITHOUTFOOD` int(11) DEFAULT NULL,
                        `MDM_TOPROGRAM` int(11) DEFAULT NULL,
                        `BENEFITTED_BOYS` int(11) DEFAULT NULL,
                        `BENEFITTED_GIRLS` int(11) DEFAULT NULL,
                        `KITSCHEME` int(11) DEFAULT NULL,
                        `HEALTHPROG` int(11) DEFAULT NULL,
                        `REFBOYS` int(11) DEFAULT NULL,
                        `REFGIRLS` int(11) DEFAULT NULL,
                        `REFBOYSSTATE` int(11) DEFAULT NULL,
                        `REFGIRLSSTATE` int(11) DEFAULT NULL,
                        `TOTLPGCLNDR` int(11) DEFAULT NULL,
                        `MEALSINSCH` int(11) DEFAULT NULL,
                        `NPEGEL_YN` int(11) DEFAULT NULL,
                        `KGBV_MODEL` int(11) DEFAULT NULL,
                        `UTENSILS_YN` int(11) DEFAULT NULL,
                        `MDMVAR1` int(11) DEFAULT NULL,
                        `MDMVAR2` int(11) DEFAULT NULL,
                        `MDMVAR3` int(11) DEFAULT NULL,
                        `MDMVAR4` varchar(50) DEFAULT NULL,
                        `MDMVAR5` varchar(50) DEFAULT NULL,
                        `KITDEVGRANT_YN` int(11) DEFAULT NULL,
                        `COOK_M` int(11) DEFAULT NULL,
                        `COOK_F` int(11) DEFAULT NULL,
                        `MEALSERVED` int(11) DEFAULT NULL,
                        `INSPECT_SO` int(11) DEFAULT NULL,
                        `INSPECT_CM` int(11) DEFAULT NULL,
                        `HANDWASH_YN` int(11) DEFAULT NULL,
                        `HWFUN_YN` int(11) DEFAULT NULL,
                        `HWSUF_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MEDINSTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MEDINSTR` (
                        `MEDINSTR_ID` int(11) DEFAULT NULL,
                        `MEDINSTR_DESC` varchar(75) DEFAULT NULL,
                        `MEDINSTR_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_MUNICIPALITY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_MUNICIPALITY` (
                        `MUNCD` varchar(7) DEFAULT NULL,
                        `MUNNAME` varchar(35) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `MUNVAR1` varchar(35) DEFAULT NULL,
                        `MUNVAR2` varchar(35) DEFAULT NULL,
                        `MUNVAR3` varchar(35) DEFAULT NULL,
                        `MUNVAR4` varchar(35) DEFAULT NULL,
                        `MUNVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_CURNEWB` int(11) DEFAULT NULL,
                        `C1_CURNEWG` int(11) DEFAULT NULL,
                        `C2_CURNEWB` int(11) DEFAULT NULL,
                        `C2_CURNEWG` int(11) DEFAULT NULL,
                        `C3_CURNEWB` int(11) DEFAULT NULL,
                        `C3_CURNEWG` int(11) DEFAULT NULL,
                        `C4_CURNEWB` int(11) DEFAULT NULL,
                        `C4_CURNEWG` int(11) DEFAULT NULL,
                        `C5_CURNEWB` int(11) DEFAULT NULL,
                        `C5_CURNEWG` int(11) DEFAULT NULL,
                        `C6_CURNEWB` int(11) DEFAULT NULL,
                        `C6_CURNEWG` int(11) DEFAULT NULL,
                        `C7_CURNEWB` int(11) DEFAULT NULL,
                        `C7_CURNEWG` int(11) DEFAULT NULL,
                        `C8_CURNEWB` int(11) DEFAULT NULL,
                        `C8_CURNEWG` int(11) DEFAULT NULL,
                        `C1_CURTCB` int(11) DEFAULT NULL,
                        `C1_CURTCG` int(11) DEFAULT NULL,
                        `C2_CURTCB` int(11) DEFAULT NULL,
                        `C2_CURTCG` int(11) DEFAULT NULL,
                        `C3_CURTCB` int(11) DEFAULT NULL,
                        `C3_CURTCG` int(11) DEFAULT NULL,
                        `C4_CURTCB` int(11) DEFAULT NULL,
                        `C4_CURTCG` int(11) DEFAULT NULL,
                        `C5_CURTCB` int(11) DEFAULT NULL,
                        `C5_CURTCG` int(11) DEFAULT NULL,
                        `C6_CURTCB` int(11) DEFAULT NULL,
                        `C6_CURTCG` int(11) DEFAULT NULL,
                        `C7_CURTCB` int(11) DEFAULT NULL,
                        `C7_CURTCG` int(11) DEFAULT NULL,
                        `C8_CURTCB` int(11) DEFAULT NULL,
                        `C8_CURTCG` int(11) DEFAULT NULL,
                        `C1_PRVNEWB` int(11) DEFAULT NULL,
                        `C1_PRVNEWG` int(11) DEFAULT NULL,
                        `C2_PRVNEWB` int(11) DEFAULT NULL,
                        `C2_PRVNEWG` int(11) DEFAULT NULL,
                        `C3_PRVNEWB` int(11) DEFAULT NULL,
                        `C3_PRVNEWG` int(11) DEFAULT NULL,
                        `C4_PRVNEWB` int(11) DEFAULT NULL,
                        `C4_PRVNEWG` int(11) DEFAULT NULL,
                        `C5_PRVNEWB` int(11) DEFAULT NULL,
                        `C5_PRVNEWG` int(11) DEFAULT NULL,
                        `C6_PRVNEWB` int(11) DEFAULT NULL,
                        `C6_PRVNEWG` int(11) DEFAULT NULL,
                        `C7_PRVNEWB` int(11) DEFAULT NULL,
                        `C7_PRVNEWG` int(11) DEFAULT NULL,
                        `C8_PRVNEWB` int(11) DEFAULT NULL,
                        `C8_PRVNEWG` int(11) DEFAULT NULL,
                        `C1_PRVTCB` int(11) DEFAULT NULL,
                        `C1_PRVTCG` int(11) DEFAULT NULL,
                        `C2_PRVTCB` int(11) DEFAULT NULL,
                        `C2_PRVTCG` int(11) DEFAULT NULL,
                        `C3_PRVTCB` int(11) DEFAULT NULL,
                        `C3_PRVTCG` int(11) DEFAULT NULL,
                        `C4_PRVTCB` int(11) DEFAULT NULL,
                        `C4_PRVTCG` int(11) DEFAULT NULL,
                        `C5_PRVTCB` int(11) DEFAULT NULL,
                        `C5_PRVTCG` int(11) DEFAULT NULL,
                        `C6_PRVTCB` int(11) DEFAULT NULL,
                        `C6_PRVTCG` int(11) DEFAULT NULL,
                        `C7_PRVTCB` int(11) DEFAULT NULL,
                        `C7_PRVTCG` int(11) DEFAULT NULL,
                        `C8_PRVTCB` int(11) DEFAULT NULL,
                        `C8_PRVTCG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADD912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADD912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CURNEW_9B` int(11) DEFAULT NULL,
                        `CURNEW_9G` int(11) DEFAULT NULL,
                        `CURTC_9B` int(11) DEFAULT NULL,
                        `CURTC_9G` int(11) DEFAULT NULL,
                        `PRVNEW_9B` int(11) DEFAULT NULL,
                        `PRVNEW_9G` int(11) DEFAULT NULL,
                        `PRVTC_9B` int(11) DEFAULT NULL,
                        `PRVTC_9G` int(11) DEFAULT NULL,
                        `CURNEW_10B` int(11) DEFAULT NULL,
                        `CURNEW_10G` int(11) DEFAULT NULL,
                        `CURTC_10B` int(11) DEFAULT NULL,
                        `CURTC_10G` int(11) DEFAULT NULL,
                        `PRVNEW_10B` int(11) DEFAULT NULL,
                        `PRVNEW_10G` int(11) DEFAULT NULL,
                        `PRVTC_10B` int(11) DEFAULT NULL,
                        `PRVTC_10G` int(11) DEFAULT NULL,
                        `CURNEW_11B` int(11) DEFAULT NULL,
                        `CURNEW_11G` int(11) DEFAULT NULL,
                        `CURTC_11B` int(11) DEFAULT NULL,
                        `CURTC_11G` int(11) DEFAULT NULL,
                        `PRVNEW_11B` int(11) DEFAULT NULL,
                        `PRVNEW_11G` int(11) DEFAULT NULL,
                        `PRVTC_11B` int(11) DEFAULT NULL,
                        `PRVTC_11G` int(11) DEFAULT NULL,
                        `CURNEW_12B` int(11) DEFAULT NULL,
                        `CURNEW_12G` int(11) DEFAULT NULL,
                        `CURTC_12B` int(11) DEFAULT NULL,
                        `CURTC_12G` int(11) DEFAULT NULL,
                        `PRVNEW_12B` int(11) DEFAULT NULL,
                        `PRVNEW_12G` int(11) DEFAULT NULL,
                        `PRVTC_12B` int(11) DEFAULT NULL,
                        `PRVTC_12G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NEWADMGRD1`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NEWADMGRD1` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `AGE4_B` int(11) DEFAULT NULL,
                        `AGE4_G` int(11) DEFAULT NULL,
                        `AGE5_B` int(11) DEFAULT NULL,
                        `AGE5_G` int(11) DEFAULT NULL,
                        `AGE6_B` int(11) DEFAULT NULL,
                        `AGE6_G` int(11) DEFAULT NULL,
                        `AGE7_B` int(11) DEFAULT NULL,
                        `AGE7_G` int(11) DEFAULT NULL,
                        `AGE8_B` int(11) DEFAULT NULL,
                        `AGE8_G` int(11) DEFAULT NULL,
                        `TOT_B` int(11) DEFAULT NULL,
                        `TOT_G` int(11) DEFAULT NULL,
                        `SAMESCH_B` int(11) DEFAULT NULL,
                        `SAMESCH_G` int(11) DEFAULT NULL,
                        `OTHERSCH_B` int(11) DEFAULT NULL,
                        `OTHERSCH_G` int(11) DEFAULT NULL,
                        `ECCE_B` int(11) DEFAULT NULL,
                        `ECCE_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_NONTCH`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_NONTCH` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `DESIG_ID` int(11) DEFAULT NULL,
                        `TOTSAN` int(11) DEFAULT NULL,
                        `TOTPOS` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PANCHAYAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PANCHAYAT` (
                        `PANCD` varchar(9) DEFAULT NULL,
                        `PANNAME` varchar(30) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `PANVAR1` varchar(35) DEFAULT NULL,
                        `PANVAR2` varchar(35) DEFAULT NULL,
                        `PANVAR3` varchar(35) DEFAULT NULL,
                        `PANVAR4` varchar(35) DEFAULT NULL,
                        `PANVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PCONSTITUENCY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PCONSTITUENCY` (
                        `CONSTCD` varchar(8) DEFAULT NULL,
                        `CONSTNAME` varchar(75) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHK_BIT` int(11) DEFAULT NULL,
                        `CONSTVAR1` varchar(35) DEFAULT NULL,
                        `CONSTVAR2` varchar(35) DEFAULT NULL,
                        `CONSTVAR3` varchar(35) DEFAULT NULL,
                        `CONSTVAR4` varchar(35) DEFAULT NULL,
                        `CONSTVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_PINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_PINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_POPDROPNENR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_POPDROPNENR` (
                        `TABLEID` int(11) DEFAULT NULL,
                        `DBVHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `LEVELID` int(11) DEFAULT NULL,
                        `GEN610B` int(11) DEFAULT NULL,
                        `GEN610G` int(11) DEFAULT NULL,
                        `SC610B` int(11) DEFAULT NULL,
                        `SC610G` int(11) DEFAULT NULL,
                        `ST610B` int(11) DEFAULT NULL,
                        `ST610G` int(11) DEFAULT NULL,
                        `OBC610B` int(11) DEFAULT NULL,
                        `OBC610G` int(11) DEFAULT NULL,
                        `TOT610B` int(11) DEFAULT NULL,
                        `TOT610G` int(11) DEFAULT NULL,
                        `MIN_610B` int(11) DEFAULT NULL,
                        `MIN_610G` int(11) DEFAULT NULL,
                        `GEN1113B` int(11) DEFAULT NULL,
                        `GEN1113G` int(11) DEFAULT NULL,
                        `SC1113B` int(11) DEFAULT NULL,
                        `SC1113G` int(11) DEFAULT NULL,
                        `ST1113B` int(11) DEFAULT NULL,
                        `ST1113G` int(11) DEFAULT NULL,
                        `OBC1113B` int(11) DEFAULT NULL,
                        `OBC1113G` int(11) DEFAULT NULL,
                        `TOT1113B` int(11) DEFAULT NULL,
                        `TOT1113G` int(11) DEFAULT NULL,
                        `MIN_1113B` int(11) DEFAULT NULL,
                        `MIN_1113G` int(11) DEFAULT NULL,
                        `GEN1415B` int(11) DEFAULT NULL,
                        `GEN1415G` int(11) DEFAULT NULL,
                        `SC1415B` int(11) DEFAULT NULL,
                        `SC1415G` int(11) DEFAULT NULL,
                        `ST1415B` int(11) DEFAULT NULL,
                        `ST1415G` int(11) DEFAULT NULL,
                        `OBC1415B` int(11) DEFAULT NULL,
                        `OBC1415G` int(11) DEFAULT NULL,
                        `TOT1415B` int(11) DEFAULT NULL,
                        `TOT1415G` int(11) DEFAULT NULL,
                        `MIN_1415B` int(11) DEFAULT NULL,
                        `MIN_1415G` int(11) DEFAULT NULL,
                        `DIST1` double DEFAULT NULL,
                        `DIST2` double DEFAULT NULL,
                        `DIST3` double DEFAULT NULL,
                        `DIST4` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C1_FAILB` int(11) DEFAULT NULL,
                        `C1_FAILG` int(11) DEFAULT NULL,
                        `C2_FAILB` int(11) DEFAULT NULL,
                        `C2_FAILG` int(11) DEFAULT NULL,
                        `C3_FAILB` int(11) DEFAULT NULL,
                        `C3_FAILG` int(11) DEFAULT NULL,
                        `C4_FAILB` int(11) DEFAULT NULL,
                        `C4_FAILG` int(11) DEFAULT NULL,
                        `C5_FAILB` int(11) DEFAULT NULL,
                        `C5_FAILG` int(11) DEFAULT NULL,
                        `C6_FAILB` int(11) DEFAULT NULL,
                        `C6_FAILG` int(11) DEFAULT NULL,
                        `C7_FAILB` int(11) DEFAULT NULL,
                        `C7_FAILG` int(11) DEFAULT NULL,
                        `C8_FAILB` int(11) DEFAULT NULL,
                        `C8_FAILG` int(11) DEFAULT NULL,
                        `C1_ABSB` int(11) DEFAULT NULL,
                        `C1_ABSG` int(11) DEFAULT NULL,
                        `C2_ABSB` int(11) DEFAULT NULL,
                        `C2_ABSG` int(11) DEFAULT NULL,
                        `C3_ABSB` int(11) DEFAULT NULL,
                        `C3_ABSG` int(11) DEFAULT NULL,
                        `C4_ABSB` int(11) DEFAULT NULL,
                        `C4_ABSG` int(11) DEFAULT NULL,
                        `C5_ABSB` int(11) DEFAULT NULL,
                        `C5_ABSG` int(11) DEFAULT NULL,
                        `C6_ABSB` int(11) DEFAULT NULL,
                        `C6_ABSG` int(11) DEFAULT NULL,
                        `C7_ABSB` int(11) DEFAULT NULL,
                        `C7_ABSG` int(11) DEFAULT NULL,
                        `C8_ABSB` int(11) DEFAULT NULL,
                        `C8_ABSG` int(11) DEFAULT NULL,
                        `C1_READB` int(11) DEFAULT NULL,
                        `C1_READG` int(11) DEFAULT NULL,
                        `C2_READB` int(11) DEFAULT NULL,
                        `C2_READG` int(11) DEFAULT NULL,
                        `C3_READB` int(11) DEFAULT NULL,
                        `C3_READG` int(11) DEFAULT NULL,
                        `C4_READB` int(11) DEFAULT NULL,
                        `C4_READG` int(11) DEFAULT NULL,
                        `C5_READB` int(11) DEFAULT NULL,
                        `C5_READG` int(11) DEFAULT NULL,
                        `C6_READB` int(11) DEFAULT NULL,
                        `C6_READG` int(11) DEFAULT NULL,
                        `C7_READB` int(11) DEFAULT NULL,
                        `C7_READG` int(11) DEFAULT NULL,
                        `C8_READB` int(11) DEFAULT NULL,
                        `C8_READG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS09`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS09` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1_B` int(11) DEFAULT NULL,
                        `C1_G` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL,
                        `C9_B` int(11) DEFAULT NULL,
                        `C9_G` int(11) DEFAULT NULL,
                        `C10_B` int(11) DEFAULT NULL,
                        `C10_G` int(11) DEFAULT NULL,
                        `C11_B` int(11) DEFAULT NULL,
                        `C11_G` int(11) DEFAULT NULL,
                        `C12_B` int(11) DEFAULT NULL,
                        `C12_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_REPEATERS912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_REPEATERS912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `C9BFAIL` int(11) DEFAULT NULL,
                        `C9GFAIL` int(11) DEFAULT NULL,
                        `C9BABS` int(11) DEFAULT NULL,
                        `C9GABS` int(11) DEFAULT NULL,
                        `C9BREAD` int(11) DEFAULT NULL,
                        `C9GREAD` int(11) DEFAULT NULL,
                        `C10BFAIL` int(11) DEFAULT NULL,
                        `C10GFAIL` int(11) DEFAULT NULL,
                        `C10BABS` int(11) DEFAULT NULL,
                        `C10GABS` int(11) DEFAULT NULL,
                        `C10BREAD` int(11) DEFAULT NULL,
                        `C10GREAD` int(11) DEFAULT NULL,
                        `C11BFAIL` int(11) DEFAULT NULL,
                        `C11GFAIL` int(11) DEFAULT NULL,
                        `C11BABS` int(11) DEFAULT NULL,
                        `C11GABS` int(11) DEFAULT NULL,
                        `C11BREAD` int(11) DEFAULT NULL,
                        `C11GREAD` int(11) DEFAULT NULL,
                        `C12BFAIL` int(11) DEFAULT NULL,
                        `C12GFAIL` int(11) DEFAULT NULL,
                        `C12BABS` int(11) DEFAULT NULL,
                        `C12GABS` int(11) DEFAULT NULL,
                        `C12BREAD` int(11) DEFAULT NULL,
                        `C12GREAD` int(11) DEFAULT NULL,
                        `C1BSC` int(11) DEFAULT NULL,
                        `C1GSC` int(11) DEFAULT NULL,
                        `C2BSC` int(11) DEFAULT NULL,
                        `C2GSC` int(11) DEFAULT NULL,
                        `C3BSC` int(11) DEFAULT NULL,
                        `C3GSC` int(11) DEFAULT NULL,
                        `C4BSC` int(11) DEFAULT NULL,
                        `C4GSC` int(11) DEFAULT NULL,
                        `C5BSC` int(11) DEFAULT NULL,
                        `C5GSC` int(11) DEFAULT NULL,
                        `C6BSC` int(11) DEFAULT NULL,
                        `C6GSC` int(11) DEFAULT NULL,
                        `C7BSC` int(11) DEFAULT NULL,
                        `C7GSC` int(11) DEFAULT NULL,
                        `C8BSC` int(11) DEFAULT NULL,
                        `C8GSC` int(11) DEFAULT NULL,
                        `C9BSC` int(11) DEFAULT NULL,
                        `C9GSC` int(11) DEFAULT NULL,
                        `C10BSC` int(11) DEFAULT NULL,
                        `C10GSC` int(11) DEFAULT NULL,
                        `C11BSC` int(11) DEFAULT NULL,
                        `C11GSC` int(11) DEFAULT NULL,
                        `C12BSC` int(11) DEFAULT NULL,
                        `C12GSC` int(11) DEFAULT NULL,
                        `C1BST` int(11) DEFAULT NULL,
                        `C1GST` int(11) DEFAULT NULL,
                        `C2BST` int(11) DEFAULT NULL,
                        `C2GST` int(11) DEFAULT NULL,
                        `C3BST` int(11) DEFAULT NULL,
                        `C3GST` int(11) DEFAULT NULL,
                        `C4BST` int(11) DEFAULT NULL,
                        `C4GST` int(11) DEFAULT NULL,
                        `C5BST` int(11) DEFAULT NULL,
                        `C5GST` int(11) DEFAULT NULL,
                        `C6BST` int(11) DEFAULT NULL,
                        `C6GST` int(11) DEFAULT NULL,
                        `C7BST` int(11) DEFAULT NULL,
                        `C7GST` int(11) DEFAULT NULL,
                        `C8BST` int(11) DEFAULT NULL,
                        `C8GST` int(11) DEFAULT NULL,
                        `C9BST` int(11) DEFAULT NULL,
                        `C9GST` int(11) DEFAULT NULL,
                        `C10BST` int(11) DEFAULT NULL,
                        `C10GST` int(11) DEFAULT NULL,
                        `C11BST` int(11) DEFAULT NULL,
                        `C11GST` int(11) DEFAULT NULL,
                        `C12BST` int(11) DEFAULT NULL,
                        `C12GST` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RESITYPE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RESITYPE` (
                        `RESITYPE_ID` int(11) DEFAULT NULL,
                        `RESITYPE_DESC` varchar(75) DEFAULT NULL,
                        `RESITYPE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_RTEINFO`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_RTEINFO` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `WORKDAYS_PR` int(11) DEFAULT NULL,
                        `WORKDAYS_UPR` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_PR` double DEFAULT NULL,
                        `SCHHRSCHILD_UPR` double DEFAULT NULL,
                        `SCHHRSTCH_PR` double DEFAULT NULL,
                        `SCHHRSTCH_UPR` double DEFAULT NULL,
                        `CCE_YN` int(11) DEFAULT NULL,
                        `PCR_MAINTAINED` int(11) DEFAULT NULL,
                        `PCR_SHARED` int(11) DEFAULT NULL,
                        `WSEC25P_APPLIED` int(11) DEFAULT NULL,
                        `WSEC25P_ENROLLED` int(11) DEFAULT NULL,
                        `AIDRECD` int(11) DEFAULT NULL,
                        `STUADMITTED` int(11) DEFAULT NULL,
                        `SMC_YN` int(11) DEFAULT NULL,
                        `SMCMEM_M` int(11) DEFAULT NULL,
                        `SMCMEM_F` int(11) DEFAULT NULL,
                        `SMSPARENTS_M` int(11) DEFAULT NULL,
                        `SMSPARENTS_F` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_M` int(11) DEFAULT NULL,
                        `SMCNOMLOCAL_F` int(11) DEFAULT NULL,
                        `SMCMEETINGS` int(11) DEFAULT NULL,
                        `SMCSDP_YN` int(11) DEFAULT NULL,
                        `SMSCHILDREC_YN` int(11) DEFAULT NULL,
                        `SMCBANKAC_YN` int(11) DEFAULT NULL,
                        `SMCBANK` varchar(75) DEFAULT NULL,
                        `SMCBANKBRANCH` varchar(75) DEFAULT NULL,
                        `SMCACNO` varchar(20) DEFAULT NULL,
                        `IFSCCODE` varchar(20) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_CY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_ENROLLED_G` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_B` int(11) DEFAULT NULL,
                        `SPLTRG_PY_PROVIDED_G` int(11) DEFAULT NULL,
                        `SPLTRG_BY` int(11) DEFAULT NULL,
                        `SPLTRG_PLACE` int(11) DEFAULT NULL,
                        `SPLTRG_TYPE` int(11) DEFAULT NULL,
                        `SPLTRG_TOTEV` int(11) DEFAULT NULL,
                        `SPLTRG_EVTRND` int(11) DEFAULT NULL,
                        `SPLTRG_MATERIAL_YN` int(11) DEFAULT NULL,
                        `TXTBKRECD_YN` int(11) DEFAULT NULL,
                        `TXTBKMNTH` int(11) DEFAULT NULL,
                        `TXTBKYEAR` int(11) DEFAULT NULL,
                        `ACSTARTMNTH` int(11) DEFAULT NULL,
                        `SMCACNAME` varchar(100) DEFAULT NULL,
                        `SPLTRNG_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHCAT` (
                        `SCHCAT_ID` int(11) DEFAULT NULL,
                        `SCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `SCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHMGT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHMGT` (
                        `SCHMGT_ID` int(11) DEFAULT NULL,
                        `SCHMGT_DESC` varchar(75) DEFAULT NULL,
                        `SCHMGT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SCHOOL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SCHOOL` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `SCHNAME` varchar(100) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `VILCD` varchar(9) DEFAULT NULL,
                        `BLKCD` varchar(6) DEFAULT NULL,
                        `DISTCD` varchar(4) DEFAULT NULL,
                        `SCHVAR1` varchar(35) DEFAULT NULL,
                        `SCHVAR2` varchar(35) DEFAULT NULL,
                        `SCHVAR3` varchar(35) DEFAULT NULL,
                        `SCHVAR4` varchar(35) DEFAULT NULL,
                        `SCHVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SMDC`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SMDC` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `SMDC_YN` int(11) DEFAULT NULL,
                        `PARENTS_M` int(11) DEFAULT NULL,
                        `PARENTS_F` int(11) DEFAULT NULL,
                        `LOCAL_M` int(11) DEFAULT NULL,
                        `LOCAL_F` int(11) DEFAULT NULL,
                        `EBMC_M` int(11) DEFAULT NULL,
                        `EBMC_F` int(11) DEFAULT NULL,
                        `WOMEN_M` int(11) DEFAULT NULL,
                        `SCST_M` int(11) DEFAULT NULL,
                        `SCST_F` int(11) DEFAULT NULL,
                        `DEO_M` int(11) DEFAULT NULL,
                        `DEO_F` int(11) DEFAULT NULL,
                        `AAD_M` int(11) DEFAULT NULL,
                        `AAD_F` int(11) DEFAULT NULL,
                        `SUBEXP_M` int(11) DEFAULT NULL,
                        `SUBEXP_F` int(11) DEFAULT NULL,
                        `TCH_M` int(11) DEFAULT NULL,
                        `TCH_F` int(11) DEFAULT NULL,
                        `AHM_M` int(11) DEFAULT NULL,
                        `AHM_F` int(11) DEFAULT NULL,
                        `HM_M` int(11) DEFAULT NULL,
                        `HM_F` int(11) DEFAULT NULL,
                        `CP_M` int(11) DEFAULT NULL,
                        `CP_F` int(11) DEFAULT NULL,
                        `SMDCMEETING` int(11) DEFAULT NULL,
                        `SIP_YN` int(11) DEFAULT NULL,
                        `BANKAC_YN` int(11) DEFAULT NULL,
                        `BANKNAME` varchar(75) DEFAULT NULL,
                        `BANKBRANCH` varchar(75) DEFAULT NULL,
                        `BANKACNO` varchar(50) DEFAULT NULL,
                        `ACNAME` varchar(100) DEFAULT NULL,
                        `IFSC` varchar(50) DEFAULT NULL,
                        `SBC_YN` int(11) DEFAULT NULL,
                        `AC_YN` int(11) DEFAULT NULL,
                        `PTA_YN` int(11) DEFAULT NULL,
                        `PTAMEETING` int(11) DEFAULT NULL,
                        `WORKDAYS_SEC` int(11) DEFAULT NULL,
                        `WORKDAYS_HSEC` int(11) DEFAULT NULL,
                        `CCESEC_YN` int(11) DEFAULT NULL,
                        `CCEHSEC_YN` int(11) DEFAULT NULL,
                        `SMCSMDC1_YN` int(11) DEFAULT NULL,
                        `TOT_M` int(11) DEFAULT NULL,
                        `TOT_F` int(11) DEFAULT NULL,
                        `SCHHRSCHILD_SEC` double DEFAULT NULL,
                        `SCHHRSCHILD_HSEC` double DEFAULT NULL,
                        `SCHHRSTCH_SEC` double DEFAULT NULL,
                        `SCHHRSTCH_HSEC` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SPLTR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SPLTR` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C2_B` int(11) DEFAULT NULL,
                        `C2_G` int(11) DEFAULT NULL,
                        `C3_B` int(11) DEFAULT NULL,
                        `C3_G` int(11) DEFAULT NULL,
                        `C4_B` int(11) DEFAULT NULL,
                        `C4_G` int(11) DEFAULT NULL,
                        `C5_B` int(11) DEFAULT NULL,
                        `C5_G` int(11) DEFAULT NULL,
                        `C6_B` int(11) DEFAULT NULL,
                        `C6_G` int(11) DEFAULT NULL,
                        `C7_B` int(11) DEFAULT NULL,
                        `C7_G` int(11) DEFAULT NULL,
                        `C8_B` int(11) DEFAULT NULL,
                        `C8_G` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_STATLIST`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_STATLIST` (
                        `STATCD` varchar(2) DEFAULT NULL,
                        `STATNAME` varchar(35) DEFAULT NULL,
                        `PHASE` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVAR`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVAR` (
                        `VARID` int(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARGROUP` int(11) DEFAULT NULL,
                        `VARSEQ` int(11) DEFAULT NULL,
                        `VARDESC` varchar(100) DEFAULT NULL,
                        `VARTYPE` int(11) DEFAULT NULL,
                        `VARLEN` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(100) DEFAULT NULL,
                        `VARVALIDATION` varchar(50) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_SUPPVARENTRY`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_SUPPVARENTRY` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VARID` int(11) DEFAULT NULL,
                        `VARVALUE` varchar(50) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHAQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHAQUAL` (
                        `TCHAQUAL_ID` int(11) DEFAULT NULL,
                        `TCHAQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHAQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHBYSUB`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHBYSUB` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `CLASSID` int(11) DEFAULT NULL,
                        `SUBJECTID` int(11) DEFAULT NULL,
                        `SANCTIONED` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTE` (
                        `TCHCASTE_ID` int(11) DEFAULT NULL,
                        `TCHCASTE_DESC` varchar(75) DEFAULT NULL,
                        `TCHCASTE_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCASTEMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCASTEMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHTYPE` int(11) DEFAULT NULL,
                        `SCHMGT` int(11) DEFAULT NULL,
                        `SCHCAT` int(11) DEFAULT NULL,
                        `RURURB` int(11) DEFAULT NULL,
                        `CLCODE` varchar(10) DEFAULT NULL,
                        `SCHTYPE` int(11) DEFAULT NULL,
                        `TRNGM` int(11) DEFAULT NULL,
                        `TRNGF` int(11) DEFAULT NULL,
                        `TRNCM` int(11) DEFAULT NULL,
                        `TRNCF` int(11) DEFAULT NULL,
                        `TRNTM` int(11) DEFAULT NULL,
                        `TRNTF` int(11) DEFAULT NULL,
                        `TRNOM` int(11) DEFAULT NULL,
                        `TRNOF` int(11) DEFAULT NULL,
                        `UTRGM` int(11) DEFAULT NULL,
                        `UTRGF` int(11) DEFAULT NULL,
                        `UTRCM` int(11) DEFAULT NULL,
                        `UTRCF` int(11) DEFAULT NULL,
                        `UTRTM` int(11) DEFAULT NULL,
                        `UTRTF` int(11) DEFAULT NULL,
                        `UTROM` int(11) DEFAULT NULL,
                        `UTROF` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCAT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCAT` (
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `TCHCAT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCAT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCATMEDIUM`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCATMEDIUM` (
                        `SCHCODE` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MEDIUM` int(11) DEFAULT NULL,
                        `TCHCAT_ID` int(11) DEFAULT NULL,
                        `NUM_SANCTIONED` int(11) DEFAULT NULL,
                        `NUM_POSITION` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHCLSTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHCLSTAUGHT` (
                        `TCHCLSTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHCLSTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHCLSTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHPQUAL`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHPQUAL` (
                        `TCHPQUAL_ID` int(11) DEFAULT NULL,
                        `TCHPQUAL_DESC` varchar(75) DEFAULT NULL,
                        `TCHPQUAL_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TCHSUBTAUGHT`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TCHSUBTAUGHT` (
                        `TCHSUBTAUGHT_ID` int(11) DEFAULT NULL,
                        `TCHSUBTAUGHT_DESC` varchar(75) DEFAULT NULL,
                        `TCHSUBTAUGHT_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TEACHER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TEACHER` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TCHCD` varchar(12) DEFAULT NULL,
                        `SLNO` int(11) DEFAULT NULL,
                        `TCHNAME` varchar(50) DEFAULT NULL,
                        `SEX` int(11) DEFAULT NULL,
                        `DOB` datetime DEFAULT NULL,
                        `CASTE` int(11) DEFAULT NULL,
                        `CATEGORY` int(11) DEFAULT NULL,
                        `YOJ` int(11) DEFAULT NULL,
                        `QUAL_ACAD` int(11) DEFAULT NULL,
                        `QUAL_PROF` int(11) DEFAULT NULL,
                        `CLS_TAUGHT` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT1` int(11) DEFAULT NULL,
                        `MATH_UPTO` int(11) DEFAULT NULL,
                        `ENG_UPTO` int(11) DEFAULT NULL,
                        `TRN_BRC` int(11) DEFAULT NULL,
                        `TRN_CRC` int(11) DEFAULT NULL,
                        `TRN_DIET` int(11) DEFAULT NULL,
                        `TRN_OTHER` int(11) DEFAULT NULL,
                        `NONTCH_ASS` int(11) DEFAULT NULL,
                        `MAIN_TAUGHT2` int(11) DEFAULT NULL,
                        `SUPVAR1` int(11) DEFAULT NULL,
                        `SUPVAR2` int(11) DEFAULT NULL,
                        `SUPVAR3` int(11) DEFAULT NULL,
                        `SUPVAR4` varchar(30) DEFAULT NULL,
                        `SUPVAR5` varchar(30) DEFAULT NULL,
                        `SUPVAR6` varchar(30) DEFAULT NULL,
                        `SUPVAR7` varchar(30) DEFAULT NULL,
                        `SUPVAR8` varchar(30) DEFAULT NULL,
                        `SUPVAR9` varchar(30) DEFAULT NULL,
                        `SUPVAR10` datetime DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `WORKINGSINCE` int(11) DEFAULT NULL,
                        `POST_STATUS` int(11) DEFAULT NULL,
                        `DISABILITY_TYPE` int(11) DEFAULT NULL,
                        `DEPUTATION_YN` int(11) DEFAULT NULL,
                        `SCIENCEUPTO` int(11) DEFAULT NULL,
                        `CWSNTRAINED_YN` int(11) DEFAULT NULL,
                        `APPOINTSUB` int(11) DEFAULT NULL,
                        `STREAM` int(11) DEFAULT NULL,
                        `AADHAAR` bigint(12) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_TXTBKTLESPORTS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_TXTBKTLESPORTS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `C1` int(11) DEFAULT NULL,
                        `C2` int(11) DEFAULT NULL,
                        `C3` int(11) DEFAULT NULL,
                        `C4` int(11) DEFAULT NULL,
                        `C5` int(11) DEFAULT NULL,
                        `C6` int(11) DEFAULT NULL,
                        `C7` int(11) DEFAULT NULL,
                        `C8` int(11) DEFAULT NULL,
                        `C9` int(11) DEFAULT NULL,
                        `C10` int(11) DEFAULT NULL,
                        `C11` int(11) DEFAULT NULL,
                        `C12` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UCDETAILS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UCDETAILS` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `ITEMIDGROUP` int(11) DEFAULT NULL,
                        `ITEMID` int(11) DEFAULT NULL,
                        `OPENINGBAL` double DEFAULT NULL,
                        `AMTRECD` double DEFAULT NULL,
                        `AMTSPENT` double DEFAULT NULL,
                        `CLOSINGBAL` double DEFAULT NULL,
                        `BANKINT` double DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `TEXT_AB` int(11) DEFAULT NULL,
                        `TEXT_AG` int(11) DEFAULT NULL,
                        `TEXT_CB` int(11) DEFAULT NULL,
                        `TEXT_CG` int(11) DEFAULT NULL,
                        `TEXT_TB` int(11) DEFAULT NULL,
                        `TEXT_TG` int(11) DEFAULT NULL,
                        `TEXT_OB` int(11) DEFAULT NULL,
                        `TEXT_OG` int(11) DEFAULT NULL,
                        `STATION_AB` int(11) DEFAULT NULL,
                        `STATION_AG` int(11) DEFAULT NULL,
                        `STATION_CB` int(11) DEFAULT NULL,
                        `STATION_CG` int(11) DEFAULT NULL,
                        `STATION_TB` int(11) DEFAULT NULL,
                        `STATION_TG` int(11) DEFAULT NULL,
                        `STATION_OB` int(11) DEFAULT NULL,
                        `STATION_OG` int(11) DEFAULT NULL,
                        `UNIFORM_AB` int(11) DEFAULT NULL,
                        `UNIFORM_AG` int(11) DEFAULT NULL,
                        `UNIFORM_CB` int(11) DEFAULT NULL,
                        `UNIFORM_CG` int(11) DEFAULT NULL,
                        `UNIFORM_TB` int(11) DEFAULT NULL,
                        `UNIFORM_TG` int(11) DEFAULT NULL,
                        `UNIFORM_OB` int(11) DEFAULT NULL,
                        `UNIFORM_OG` int(11) DEFAULT NULL,
                        `ATTEND_AB` int(11) DEFAULT NULL,
                        `ATTEND_AG` int(11) DEFAULT NULL,
                        `ATTEND_CB` int(11) DEFAULT NULL,
                        `ATTEND_CG` int(11) DEFAULT NULL,
                        `ATTEND_TB` int(11) DEFAULT NULL,
                        `ATTEND_TG` int(11) DEFAULT NULL,
                        `ATTEND_OB` int(11) DEFAULT NULL,
                        `ATTEND_OG` int(11) DEFAULT NULL,
                        `OTH1_AB` int(11) DEFAULT NULL,
                        `OTH1_AG` int(11) DEFAULT NULL,
                        `OTH1_CB` int(11) DEFAULT NULL,
                        `OTH1_CG` int(11) DEFAULT NULL,
                        `OTH1_TB` int(11) DEFAULT NULL,
                        `OTH1_TG` int(11) DEFAULT NULL,
                        `OTH1_OB` int(11) DEFAULT NULL,
                        `OTH1_OG` int(11) DEFAULT NULL,
                        `OTH2_AB` int(11) DEFAULT NULL,
                        `OTH2_AG` int(11) DEFAULT NULL,
                        `OTH2_CB` int(11) DEFAULT NULL,
                        `OTH2_CG` int(11) DEFAULT NULL,
                        `OTH2_TB` int(11) DEFAULT NULL,
                        `OTH2_TG` int(11) DEFAULT NULL,
                        `OTH2_OB` int(11) DEFAULT NULL,
                        `OTH2_OG` int(11) DEFAULT NULL,
                        `OTH3_AB` int(11) DEFAULT NULL,
                        `OTH3_AG` int(11) DEFAULT NULL,
                        `OTH3_CB` int(11) DEFAULT NULL,
                        `OTH3_CG` int(11) DEFAULT NULL,
                        `OTH3_TB` int(11) DEFAULT NULL,
                        `OTH3_TG` int(11) DEFAULT NULL,
                        `OTH3_OB` int(11) DEFAULT NULL,
                        `OTH3_OG` int(11) DEFAULT NULL,
                        `OTH4_AB` int(11) DEFAULT NULL,
                        `OTH4_AG` int(11) DEFAULT NULL,
                        `OTH4_CB` int(11) DEFAULT NULL,
                        `OTH4_CG` int(11) DEFAULT NULL,
                        `OTH4_TB` int(11) DEFAULT NULL,
                        `OTH4_TG` int(11) DEFAULT NULL,
                        `OTH4_OB` int(11) DEFAULT NULL,
                        `OTH4_OG` int(11) DEFAULT NULL,
                        `OTH5_AB` int(11) DEFAULT NULL,
                        `OTH5_AG` int(11) DEFAULT NULL,
                        `OTH5_CB` int(11) DEFAULT NULL,
                        `OTH5_CG` int(11) DEFAULT NULL,
                        `OTH5_TB` int(11) DEFAULT NULL,
                        `OTH5_TG` int(11) DEFAULT NULL,
                        `OTH5_OB` int(11) DEFAULT NULL,
                        `OTH5_OG` int(11) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_UINCENTIVES912`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_UINCENTIVES912` (
                        `SCHCD` varchar(11) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `MDM_AB` int(11) DEFAULT NULL,
                        `MDM_AG` int(11) DEFAULT NULL,
                        `MDM_CB` int(11) DEFAULT NULL,
                        `MDM_CG` int(11) DEFAULT NULL,
                        `MDM_TB` int(11) DEFAULT NULL,
                        `MDM_TG` int(11) DEFAULT NULL,
                        `MDM_OB` int(11) DEFAULT NULL,
                        `MDM_OG` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VARSETTINGS`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VARSETTINGS` (
                        `VAR_ID` varchar(10) DEFAULT NULL,
                        `AC_YEAR` varchar(7) DEFAULT NULL,
                        `VAR_TYPE` int(11) DEFAULT NULL,
                        `VAR_LEN` int(11) DEFAULT NULL,
                        `VAR_LBL` varchar(30) DEFAULT NULL,
                        `VAR_TBL` varchar(30) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_VILLAGE`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_VILLAGE` (
                        `VILCD` varchar(9) DEFAULT NULL,
                        `VILNAME` varchar(35) DEFAULT NULL,
                        `INITYEAR` varchar(7) DEFAULT NULL,
                        `CHECKBIT` int(11) DEFAULT NULL,
                        `TRIBAL_YN` int(11) DEFAULT NULL,
                        `CENCD` varchar(21) DEFAULT NULL,
                        `PINCD` int(11) DEFAULT NULL,
                        `VILVAR1` varchar(35) DEFAULT NULL,
                        `VILVAR2` varchar(35) DEFAULT NULL,
                        `VILVAR3` varchar(35) DEFAULT NULL,
                        `VILVAR4` varchar(35) DEFAULT NULL,
                        `VILVAR5` varchar(35) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                -- --------------------------------------------------------

                    --
                    -- Table structure for table `STEPS_WATER`
                    --

                    CREATE TABLE IF NOT EXISTS `STEPS_WATER` (
                        `WATER_ID` int(11) DEFAULT NULL,
                        `WATER_DESC` varchar(75) DEFAULT NULL,
                        `WATER_PARENT` int(11) DEFAULT NULL,
                        `PREDEFINED` int(11) DEFAULT NULL,
                        `VISIBLE_YN` int(11) DEFAULT NULL
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

                /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
                /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
                /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
                ';
        DB::unprepared($sql);


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('
            DROP TABLE `STEPS_ATTENDANCE09`, `STEPS_BLDEQP`, `STEPS_BLDEQP09`, `STEPS_BLDSTATUS`, `STEPS_BLOCK`,
            `STEPS_BNDRYWALL`, `STEPS_CASTE`, `STEPS_CITY`, `STEPS_CLUSTER`, `STEPS_CONSTITUENCY`, `STEPS_CWSNBYCASTE`,
            `STEPS_CWSNFACILITY`, `STEPS_CYCLE`, `STEPS_DISABILITY`, `STEPS_DISABILITY09`, `STEPS_DISABILITY912`,
            `STEPS_DISTLIST`, `STEPS_DISTRICT`, `STEPS_EDUBLOCK`, `STEPS_ENRAGEBYCASTE`, `STEPS_ENRBYCASTEMEDIUM`,
            `STEPS_ENRCASTEMEDIUM`, `STEPS_ENRMEDINSTR`, `STEPS_ENRMEDINSTR912`, `STEPS_ENROLAGE`, `STEPS_ENROLAGE912`,
            `STEPS_ENROLMENT`, `STEPS_ENROLMENT09`, `STEPS_ENROLMENT912`, `STEPS_ENTITYMASTER`,
            `STEPS_ENTITYTABLEMASTER`, `STEPS_EREPBYSTREAM`, `STEPS_EXAMINATION09`, `STEPS_EXAMRESULT`,
            `STEPS_EXAMRESULT912`, `STEPS_FACILITIES`, `STEPS_FUNDS`, `STEPS_HABITATION`, `STEPS_INCENTIVES09`,
            `STEPS_KEYVAR`, `STEPS_MAPPING`, `STEPS_MASTER`, `STEPS_MASTER09`, `STEPS_MDM`, `STEPS_MEDINSTR`,
            `STEPS_MUNICIPALITY`, `STEPS_NEWADD`, `STEPS_NEWADD912`, `STEPS_NEWADMGRD1`, `STEPS_NONTCH`,
            `STEPS_PANCHAYAT`, `STEPS_PCONSTITUENCY`, `STEPS_PINCENTIVES`, `STEPS_PINCENTIVES912`, `STEPS_POPDROPNENR`,
            `STEPS_REPEATERS`, `STEPS_REPEATERS09`, `STEPS_REPEATERS912`, `STEPS_RESITYPE`, `STEPS_RTEINFO`,
            `STEPS_SCHCAT`, `STEPS_SCHMGT`, `STEPS_SCHOOL`, `STEPS_SMDC`, `STEPS_SPLTR`, `STEPS_STATLIST`,
            `STEPS_SUPPVAR`, `STEPS_SUPPVARENTRY`, `STEPS_TCHAQUAL`, `STEPS_TCHBYSUB`, `STEPS_TCHCASTE`,
            `STEPS_TCHCASTEMEDIUM`, `STEPS_TCHCAT`, `STEPS_TCHCATMEDIUM`, `STEPS_TCHCLSTAUGHT`, `STEPS_TCHPQUAL`,
            `STEPS_TCHSUBTAUGHT`, `STEPS_TEACHER`, `STEPS_TXTBKTLESPORTS`, `STEPS_UCDETAILS`, `STEPS_UINCENTIVES`,
            `STEPS_UINCENTIVES912`, `STEPS_VARSETTINGS`, `STEPS_VILLAGE`, `STEPS_WATER`;');
    }

}
