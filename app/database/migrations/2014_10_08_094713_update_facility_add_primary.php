<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateFacilityAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_FACILITIES', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMIDGROUP', 'ITEMID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_FACILITIES', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_FACILITIES_SCHCD_AC_YEAR_ITEMIDGROUP_ITEMID_primary');
		});
	}

}
