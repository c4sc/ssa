<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateResultAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_EXAMINATION09', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMIDGROUP', 'ITEMID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_EXAMINATION09', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_EXAMINATION09_SCHCD_AC_YEAR_ITEMIDGROUP_ITEMID_primary');
		});
	}

}
