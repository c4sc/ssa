<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('schoolcode', 12);
            $table->string('name');
            $table->string('father');
            $table->string('mother');
            $table->dateTime('dob');
            $table->tinyInteger('gender');
            $table->tinyInteger('social_category');
            $table->tinyInteger('religion');
            $table->tinyInteger('bpl_yn');
            $table->tinyInteger('disadvantagegroup_yn');
            $table->tinyInteger('disability_yn');
            $table->string('aadhaar', 12);
            $table->tinyInteger('rural_urban');
            $table->tinyInteger('habitation_code');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
