<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateRepeaterAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_REPEATERS09', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMIDGROUP', 'ITEMID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_REPEATERS09', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_REPEATERS09_SCHCD_AC_YEAR_ITEMIDGROUP_ITEMID_primary');
		});
	}

}
