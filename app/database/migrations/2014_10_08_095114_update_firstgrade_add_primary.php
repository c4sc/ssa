<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateFirstgradeAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_NEWADMGRD1', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_NEWADMGRD1', function(Blueprint $table)
		{
            $table->dropPrimary(array('SCHCD', 'AC_YEAR'));
		});
	}

}
