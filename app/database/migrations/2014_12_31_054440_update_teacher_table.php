<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTeacherTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            DB::statement('ALTER TABLE STEPS_TEACHER MODIFY TCHCD1 VARCHAR(16);');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            DB::statement('ALTER TABLE STEPS_TEACHER MODIFY TCHCD1 VARCHAR(14);');
		});
	}

}
