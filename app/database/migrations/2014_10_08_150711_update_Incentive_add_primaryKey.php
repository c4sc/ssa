<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateIncentiveAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_INCENTIVES09', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'GRADEPRUPR', 'INCENT_TYPE'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_INCENTIVES09', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_INCENTIVES09_SCHCD_AC_YEAR_GRADEPRUPR_INCENT_TYPE_primary');
		});
	}

}
