<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\QueryException;

class UpdateMediumEnrolmentChangePrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Artisan::call('app:delete-medium-duplicates');
        Schema::table('STEPS_ENRMEDINSTR', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_ENRMEDINSTR_SCHCD_AC_YEAR_MEDIUM_SEQNO_primary');
            $table->primary(array('SCHCD', 'AC_YEAR', 'MEDIUM'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_ENRMEDINSTR', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_ENRMEDINSTR_SCHCD_AC_YEAR_MEDIUM_primary');
            $table->primary(array('SCHCD', 'AC_YEAR', 'MEDIUM', 'SEQNO'));
		});
	}

}
