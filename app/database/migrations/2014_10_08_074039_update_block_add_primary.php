<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateBlockAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_BLOCK', function(Blueprint $table)
		{
            $table->primary('BLKCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_BLOCK', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_BLOCK_BLKCD_primary');
		});
	}

}
