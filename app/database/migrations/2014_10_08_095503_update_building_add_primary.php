<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateBuildingAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_BLDEQP', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_BLDEQP', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_BLDEQP_SCHCD_AC_YEAR_primary');
		});
	}

}
