<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTeacherAddMoreFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('DROP table STEPS_TEACHER');
        DB::statement("
            CREATE TABLE IF NOT EXISTS `STEPS_TEACHER` (
                `SCHCD` varchar(11) DEFAULT NULL,
                `TCHCD1` char(14) NOT NULL,
                `AC_YEAR` varchar(7) NOT NULL DEFAULT '',
                `TCHCD` varchar(12) DEFAULT NULL,
                `SLNO` int(11) DEFAULT NULL,
                `TCHNAME` varchar(50) DEFAULT NULL,
                `SEX` int(11) DEFAULT NULL,
                `DOB` datetime DEFAULT NULL,
                `CASTE` int(11) DEFAULT NULL,
                `CATEGORY` int(11) DEFAULT NULL,
                `YOJ` int(11) DEFAULT NULL,
                `QUAL_ACAD` int(11) DEFAULT NULL,
                `QUAL_PROF` int(11) DEFAULT NULL,
                `CLS_TAUGHT` int(11) DEFAULT NULL,
                `MAIN_TAUGHT1` int(11) DEFAULT NULL,
                `MATH_UPTO` int(11) DEFAULT NULL,
                `ENG_UPTO` int(11) DEFAULT NULL,
                `TRN_BRC` int(11) DEFAULT NULL,
                `TRN_CRC` int(11) DEFAULT NULL,
                `TRN_DIET` int(11) DEFAULT NULL,
                `TRN_OTHER` int(11) DEFAULT NULL,
                `NONTCH_ASS` int(11) DEFAULT NULL,
                `MAIN_TAUGHT2` int(11) DEFAULT NULL,
                `SUPVAR1` int(11) DEFAULT NULL,
                `SUPVAR2` int(11) DEFAULT NULL,
                `SUPVAR3` int(11) DEFAULT NULL,
                `SUPVAR4` varchar(30) DEFAULT NULL,
                `SUPVAR5` varchar(30) DEFAULT NULL,
                `SUPVAR6` varchar(30) DEFAULT NULL,
                `SUPVAR7` varchar(30) DEFAULT NULL,
                `SUPVAR8` varchar(30) DEFAULT NULL,
                `SUPVAR9` varchar(30) DEFAULT NULL,
                `SUPVAR10` datetime DEFAULT NULL,
                `CHECKBIT` int(11) DEFAULT NULL,
                `WORKINGSINCE` int(11) DEFAULT NULL,
                `POST_STATUS` int(11) DEFAULT NULL,
                `DISABILITY_TYPE` int(11) DEFAULT NULL,
                `DEPUTATION_YN` int(11) DEFAULT NULL,
                `SCIENCEUPTO` int(11) DEFAULT NULL,
                `CWSNTRAINED_YN` int(11) DEFAULT NULL,
                `APPOINTSUB` int(11) DEFAULT NULL,
                `STREAM` int(11) DEFAULT NULL,
                `PENNO` int(6) NOT NULL,
                `DESIGNATION` int(2) NOT NULL,
                `ROLE` int(2) NOT NULL,
                `MOB` bigint(13) NOT NULL,
                `EMAIL` varchar(75) NOT NULL,
                `STATUS` int(1) NOT NULL DEFAULT '1',
                PRIMARY KEY (`TCHCD1`,`AC_YEAR`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
    }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('STEPS_TEACHER');
	}

}
