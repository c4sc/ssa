<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateStreamEnrolmentAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('ALTER TABLE `STEPS_EREPBYSTREAM` ADD PRIMARY KEY ( `SCHCD` , `AC_YEAR` , `STREAMID` ,
            `ITEMIDGROUP` , `CASTEID` ) ;');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('ALTER TABLE `STEPS_EREPBYSTREAM` DROP PRIMARY KEY ;');
	}

}
