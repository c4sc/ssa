<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSchoolAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_SCHOOL', function(Blueprint $table)
		{
            $table->primary('SCHCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_SCHOOL', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_SCHOOL_SCHCD_primary');
		});
	}

}
