<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateEnrolmentAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_ENROLMENT09', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMIDGROUP', 'CASTEID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_ENROLMENT09', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_ENROLMENT09_SCHCD_AC_YEAR_ITEMIDGROUP_CASTEID_primary');
		});
	}

}
