<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateVillagesChangePrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_VILLAGE', function(Blueprint $table)
		{
            $table->primary('VILCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_VILLAGE', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_VILCD_primary');
		});
	}

}
