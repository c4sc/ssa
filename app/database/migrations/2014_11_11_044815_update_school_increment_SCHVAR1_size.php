<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateSchoolIncrementSCHVAR1Size extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('ALTER TABLE `STEPS_SCHOOL` MODIFY `SCHVAR1` VARCHAR(128)');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('ALTER TABLE `STEPS_SCHOOL` MODIFY `SCHVAR1` VARCHAR(35)');
	}

}
