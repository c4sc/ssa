<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateAgeEnrolmentAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_ENRAGEBYCASTE', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'AGEID', 'CASTEID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_ENRAGEBYCASTE', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_ENRAGEBYCASTE_SCHCD_AC_YEAR_AGEID_CASTEID_primary');
		});
	}

}
