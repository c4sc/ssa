<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateCwsnAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_DISABILITY09', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'DISABILITY_TYPE'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_DISABILITY09', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_DISABILITY09_SCHCD_AC_YEAR_DISABILITY_TYPE_primary');
		});
	}

}
