<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateFacilityDetailsAddFieldAvailableFor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('facility_details', function(Blueprint $table)
		{
            $table->string('available_for')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facility_details', function(Blueprint $table)
		{
            $table->dropColumn('available_for');
		});
	}

}
