<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransferPoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfer_pool', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->integer('transfered_by')->unsigned();
            $table->string('from_school', 11);
            $table->string('to_school', 11);
            $table->string('academic_year', 7);
            $table->string('status', 1);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transfer_pool');
	}

}
