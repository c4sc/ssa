<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_details', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('schoolcode', 12);
            $table->string('academic_year',7);
            $table->Integer('student_id')->unsigned();
            $table->dateTime('admission_date');
            $table->string('admission_no');
            $table->tinyInteger('class');
            $table->tinyInteger('previous_class');
            $table->tinyInteger('previous_status');
            $table->Integer('previous_days')->unsigned();
            $table->tinyInteger('medium');
            $table->tinyInteger('free_education_yn');
            $table->tinyInteger('facilitiescwsn');
            $table->tinyInteger('books_yn');
            $table->tinyInteger('uniforms');
            $table->tinyInteger('transport_yn');
            $table->tinyInteger('escort_yn');
            $table->tinyInteger('hostel_yn');
            $table->tinyInteger('special_training_yn');
            $table->tinyInteger('homeless_yn');
            $table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_details');
	}

}
