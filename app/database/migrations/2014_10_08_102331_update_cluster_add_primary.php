<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateClusterAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_CLUSTER', function(Blueprint $table)
		{
            $table->primary('CLUCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_CLUSTER', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_CLUSTER_CLUCD_primary');
		});
	}

}
