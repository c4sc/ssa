<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateMediumEnrolmentAddPrimaryKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_ENRMEDINSTR', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'MEDIUM', 'SEQNO'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_ENRMEDINSTR', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_ENRMEDINSTR_SCHCD_AC_YEAR_MEDIUM_SEQNO_primary');
		});
	}

}
