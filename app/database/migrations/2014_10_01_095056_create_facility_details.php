<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacilityDetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('facility_details', function(Blueprint $table)
		{
			$table->increments('id');
            $table->smallInteger('facility_id')->unsigned();
            $table->smallInteger('group_id')->unsigned();
            $table->string('description');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facility_details');
	}

}
