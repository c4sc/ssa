<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTeacherAddAadhaar extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            $table->string('AADHAAR', 12)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            $table->dropColumn('AADHAAR');
		});
	}

}
