<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateMaterialAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_TXTBKTLESPORTS', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'ITEMID'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_TXTBKTLESPORTS', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_TXTBKTLESPORTS_SCHCD_AC_YEAR_ITEMID_primary');
		});
	}

}
