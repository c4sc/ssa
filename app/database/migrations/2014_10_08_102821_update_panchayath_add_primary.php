<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdatePanchayathAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_PANCHAYAT', function(Blueprint $table)
		{
            $table->primary('PANCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_PANCHAYAT', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_PANCHAYAT_PANCD_primary');
		});
	}

}
