<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateConstituencyAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_CONSTITUENCY', function(Blueprint $table)
		{
            $table->primary('CONSTCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_CONSTITUENCY', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_CONSTITUENCY_CONSTCD_primary');
		});
	}

}
