<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTeacherAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            $table->primary(array('SCHCD', 'AC_YEAR', 'SLNO'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_TEACHER', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_TEACHER_SCHCD_AC_YEAR_SLNO_primary');
		});
	}

}
