<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateCityAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_CITY', function(Blueprint $table)
		{
            $table->primary('CITYCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_CITY', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_CITY_CITYCD_primary');
		});
	}

}
