<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateDistrictAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_DISTRICT', function(Blueprint $table)
		{
            $table->primary('DISTCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_DISTRICT', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_DISTRICT_DISTCD_primary');
		});
	}

}
