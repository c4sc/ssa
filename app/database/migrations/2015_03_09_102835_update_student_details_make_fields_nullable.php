<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateStudentDetailsMakeFieldsNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $sql = '
            ALTER TABLE `student_details`
			MODIFY COLUMN `previous_class` TINYINT(4) NULL,
			MODIFY COLUMN `previous_status` TINYINT(4) NULL,
			MODIFY COLUMN `previous_days` INT(11) NULL,
			MODIFY COLUMN `medium` TINYINT(4) NULL,
			MODIFY COLUMN `free_education_yn` TINYINT(4) NULL,
			MODIFY COLUMN `facilitiescwsn` TINYINT(4) NULL,
			MODIFY COLUMN `books_yn` TINYINT(4) NULL,
			MODIFY COLUMN `uniforms` TINYINT(4) NULL,
			MODIFY COLUMN `transport_yn` TINYINT(4) NULL,
			MODIFY COLUMN `escort_yn` TINYINT(4) NULL,
			MODIFY COLUMN `hostel_yn` TINYINT(4) NULL,
			MODIFY COLUMN `special_training_yn` TINYINT(4) NULL,
            MODIFY COLUMN `homeless_yn` TINYINT(4) NULL,
			MODIFY COLUMN `division` VARCHAR(4) NULL;';
        DB::statement($sql);
    }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $sql = '
            ALTER TABLE `student_details`
            MODIFY COLUMN `previous_class` TINYINT(4) NOT NULL,
            MODIFY COLUMN `previous_status` TINYINT(4) NOT NULL,
            MODIFY COLUMN `previous_days` INT(11) NOT NULL,
            MODIFY COLUMN `medium` TINYINT(4) NOT NULL,
            MODIFY COLUMN `free_education_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `facilitiescwsn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `books_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `uniforms` TINYINT(4) NOT NULL,
            MODIFY COLUMN `transport_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `escort_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `hostel_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `special_training_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `homeless_yn` TINYINT(4) NOT NULL,
            MODIFY COLUMN `division` VARCHAR(4) NOT NULL;';
        DB::statement($sql);
	}

}
