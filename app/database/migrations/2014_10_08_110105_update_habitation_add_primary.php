<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateHabitationAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_HABITATION', function(Blueprint $table)
		{
            $table->primary('HABCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_HABITATION', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_HABITATION_HABCD_primary');
		});
	}

}
