<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateWarningsAddName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Warning::truncate();

		Schema::table('warnings', function(Blueprint $table)
		{
            $table->string('name')->unique();
		});
        Artisan::call('db:seed', array('--class'=>'WarningTableSeeder'));
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('warnings', function(Blueprint $table)
		{
            $table->dropColumn('name');
		});
	}

}
