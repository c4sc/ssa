<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateEducationBlockAddPrimary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('STEPS_EDUBLOCK', function(Blueprint $table)
		{
            $table->primary('BLKCD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('STEPS_EDUBLOCK', function(Blueprint $table)
		{
            $table->dropPrimary('STEPS_EDUBLOCK_BLKCD_primary');
		});
	}

}
