<?php

class InstructionMediumsTableSeeder extends Seeder {

	public function run()
	{
        Eloquent::unguard();
        DB::table('STEPS_MEDINSTR')->truncate();
        $data =  array(
            array('MEDINSTR_ID' => 1, 'MEDINSTR_DESC' => '01 - Assamese'),
            array('MEDINSTR_ID' => 2, 'MEDINSTR_DESC' => '02 - Bengali'),
            array('MEDINSTR_ID' => 3, 'MEDINSTR_DESC' => '03 - Gujarati'),
            array('MEDINSTR_ID' => 4, 'MEDINSTR_DESC' => '04 - Hindi'),
            array('MEDINSTR_ID' => 5, 'MEDINSTR_DESC' => '05 - Kannada'),
            array('MEDINSTR_ID' => 6, 'MEDINSTR_DESC' => '06 - Kashmiri'),
            array('MEDINSTR_ID' => 7, 'MEDINSTR_DESC' => '07 - Konkani'),
            array('MEDINSTR_ID' => 8, 'MEDINSTR_DESC' => '08 - Malayalam'),
            array('MEDINSTR_ID' => 9, 'MEDINSTR_DESC' => '09 - Manipuri'),
            array('MEDINSTR_ID' => 10, 'MEDINSTR_DESC' => '10 - Marathi'),
            array('MEDINSTR_ID' => 11, 'MEDINSTR_DESC' => '11 - Nepali'),
            array('MEDINSTR_ID' => 12, 'MEDINSTR_DESC' => '12 - Oriya'),
            array('MEDINSTR_ID' => 13, 'MEDINSTR_DESC' => '13 - Punjabi'),
            array('MEDINSTR_ID' => 14, 'MEDINSTR_DESC' => '14 - Sanskrit'),
            array('MEDINSTR_ID' => 15, 'MEDINSTR_DESC' => '15 - Sindhi'),
            array('MEDINSTR_ID' => 16, 'MEDINSTR_DESC' => '16 - Tamil'),
            array('MEDINSTR_ID' => 17, 'MEDINSTR_DESC' => '17 - Telugu'),
            array('MEDINSTR_ID' => 18, 'MEDINSTR_DESC' => '18 - Urdu'),
            array('MEDINSTR_ID' => 19, 'MEDINSTR_DESC' => '19 - English'),
            array('MEDINSTR_ID' => 20, 'MEDINSTR_DESC' => '20 - Bodo'),
            array('MEDINSTR_ID' => 22, 'MEDINSTR_DESC' => '22 - Dogri'),
            array('MEDINSTR_ID' => 23, 'MEDINSTR_DESC' => '23 - Khasi'),
            array('MEDINSTR_ID' => 24, 'MEDINSTR_DESC' => '24 - Garo'),
            array('MEDINSTR_ID' => 25, 'MEDINSTR_DESC' => '25 - Mizo'),
            array('MEDINSTR_ID' => 26, 'MEDINSTR_DESC' => '26 - Bhutia'),
            array('MEDINSTR_ID' => 27, 'MEDINSTR_DESC' => '27 - Lepcha'),
            array('MEDINSTR_ID' => 28, 'MEDINSTR_DESC' => '28 - Limboo'),
            array('MEDINSTR_ID' => 29, 'MEDINSTR_DESC' => '29 - French'),
            array('MEDINSTR_ID' => 41, 'MEDINSTR_DESC' => '41 - Angami'),
            array('MEDINSTR_ID' => 42, 'MEDINSTR_DESC' => '42 - Ao'),
            array('MEDINSTR_ID' => 43, 'MEDINSTR_DESC' => '43 - Arabic'),
            array('MEDINSTR_ID' => 44, 'MEDINSTR_DESC' => '44 - Bhoti'),
            array('MEDINSTR_ID' => 45, 'MEDINSTR_DESC' => '45 - Bodhi'),
            array('MEDINSTR_ID' => 46, 'MEDINSTR_DESC' => '46 - German'),
            array('MEDINSTR_ID' => 47, 'MEDINSTR_DESC' => '47 - Kakbarak'),
            array('MEDINSTR_ID' => 48, 'MEDINSTR_DESC' => '48 - Konyak'),
            array('MEDINSTR_ID' => 49, 'MEDINSTR_DESC' => '49 - Laddakhi'),
            array('MEDINSTR_ID' => 50, 'MEDINSTR_DESC' => '50 - Lotha'),
            array('MEDINSTR_ID' => 51, 'MEDINSTR_DESC' => '51 - Maithili'),
            array('MEDINSTR_ID' => 52, 'MEDINSTR_DESC' => '52 - Nicobaree'),
            array('MEDINSTR_ID' => 53, 'MEDINSTR_DESC' => '53 - Oriya(lower)'),
            array('MEDINSTR_ID' => 54, 'MEDINSTR_DESC' => '54 - Persian'),
            array('MEDINSTR_ID' => 55, 'MEDINSTR_DESC' => '55 - Portuguese'),
            array('MEDINSTR_ID' => 56, 'MEDINSTR_DESC' => '56 - Rajasthani'),
            array('MEDINSTR_ID' => 57, 'MEDINSTR_DESC' => '57 - Russian'),
            array('MEDINSTR_ID' => 58, 'MEDINSTR_DESC' => '58 - Sema'),
            array('MEDINSTR_ID' => 59, 'MEDINSTR_DESC' => '59 - Spanish'),
            array('MEDINSTR_ID' => 60, 'MEDINSTR_DESC' => '60 - Tibetan'),
            array('MEDINSTR_ID' => 61, 'MEDINSTR_DESC' => '61 - Zeliang'),
            array('MEDINSTR_ID' => 99, 'MEDINSTR_DESC' => '61 - Other languages')
        );

        DB::table('STEPS_MEDINSTR')->insert($data);
	}

}
