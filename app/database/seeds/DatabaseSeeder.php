<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        // @see https://gist.github.com/isimmons/8202227
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('FacilityDetailsTableSeeder');
		$this->call('WarningTableSeeder');
		$this->call('LinksTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
