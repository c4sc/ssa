<?php

class FacilityDetailsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('facility_details')->truncate();

        $facility_details = array(
            array('facility_id'=>'1', 'group_id'=>'1', 'description'=>'Separate room for Asst Head Master/ Vice Principal', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'3', 'group_id'=>'1', 'description'=>'Separate common room for girls', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'4', 'group_id'=>'1', 'description'=>'Staffroom for teachers', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'5', 'group_id'=>'1', 'description'=>'ICT Lab', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'6', 'group_id'=>'1', 'description'=>'Computer room', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'8', 'group_id'=>'1', 'description'=>'Co-curricular/activity room', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'11', 'group_id'=>'1', 'description'=>'Staff quarters (including residential quarters for HM/Principal and Asst. HM/Vice Principal)', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'12', 'group_id'=>'1', 'description'=>'Integrated science laboratory? (Integrated laboratory is the one in which Physics, Chemistry and Biology practical are held) for Secondary sections only', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'13', 'group_id'=>'1', 'description'=>'Library room', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'14', 'group_id'=>'1', 'description'=>'Science Lab for Secondary sections (classes IX and X) only', 'available_for'=>'secondary'),
            array('facility_id'=>'15', 'group_id'=>'1', 'description'=>'Mathematics Lab for Secondary sections (classes IX and X) only', 'available_for'=>'secondary'),
            array('facility_id'=>'16', 'group_id'=>'1', 'description'=>'Social Science Lab for Secondary sections (classes IX and X) only', 'available_for'=>'secondary'),
            array('facility_id'=>'17', 'group_id'=>'1', 'description'=>'Pre-vocation Lab for Secondary sections (classes IX and X) only', 'available_for'=>'secondary'),

            array('facility_id'=>'1', 'group_id'=>'2', 'description'=>'Physics', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'2', 'group_id'=>'2', 'description'=>'Chemistry', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'3', 'group_id'=>'2', 'description'=>'Biology', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'4', 'group_id'=>'2', 'description'=>'Computer', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'5', 'group_id'=>'2', 'description'=>'Mathematics', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'6', 'group_id'=>'2', 'description'=>'Language', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'7', 'group_id'=>'2', 'description'=>'Geography', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'8', 'group_id'=>'2', 'description'=>'Home Science', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'9', 'group_id'=>'2', 'description'=>'Psychology', 'available_for'=>'secondary,hs'),

            array('facility_id'=>'2', 'group_id'=>'3', 'description'=>'Audio/Visual/Public Address System', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'5', 'group_id'=>'3', 'description'=>'LCD Projector', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'14', 'group_id'=>'3', 'description'=>'Fire Extinguisher', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'16', 'group_id'=>'3', 'description'=>'Internet connection', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'18', 'group_id'=>'3', 'description'=>'Is Rain Water Harvesting Implemented', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'19', 'group_id'=>'3', 'description'=>'Science Kit', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'20', 'group_id'=>'3', 'description'=>'Math Kit', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'21', 'group_id'=>'3', 'description'=>'Local Area Network', 'available_for'=>'secondary,hs'),
            array('facility_id'=>'22', 'group_id'=>'3', 'description'=>'Printer', 'available_for'=>'secondary,hs'),
        );

        FacilityDetail::insert($facility_details);

    }

}
