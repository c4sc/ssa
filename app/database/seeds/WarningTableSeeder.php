<?php

class WarningTableSeeder extends Seeder {

	public function run()
	{
        Warning::truncate();
        $warnings = array(
            array('id'=>1, 'name'=>'teachers', 'route'=>'teachers', 'type'=>'ER', 'description'=>'teachers to be added/deleted'),
            array('id'=>2, 'name'=>'class-with-no-students', 'route'=>'enrolment-by-social-category', 'type'=>'WN', 'description'=>'Student count is zero for some classes'),
            array('id'=>3, 'name'=>'teachers-with-invalid-class-taught', 'route'=>'teachers', 'type'=>'ER', 'description'=>'teachers has invalid class-taught value'),
            array('id'=>4, 'name'=>'textbook-recieved-no', 'route'=>'particulars-three', 'type'=>'WN', 'description'=>'Textbook recieved is no')
        );
        Warning::insert($warnings);
	}

}
