<?php

class LinksTableSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();
        Link::truncate();
        /**
         * !! do not add links in between existing links.
         * order matters
         * as we are not using `id` as attribute, id will be automatically generated based on order of insertion
         */
        $data =  array(
            array('category'=>'U', 'title'=>'School Particulars', 'route'=>'particulars-one'),
            array('category'=>'U', 'title'=>'Particulars - Schools with Pre Primary', 'route'=>'particulars-two'),
            array('category'=>'U', 'title'=>'Particulars - Elementary', 'route'=>'particulars-three'),
            array('category'=>'U', 'title'=>'SMDC Details', 'route'=>'particulars-four'),
            array('category'=>'U', 'title'=>'Facilities - Secondary & Higher Secondary', 'route'=>'facilities-two'),
            array('category'=>'U', 'title'=>'Physical facilities and Equipment', 'route'=>'facilities-one'),
            array('category'=>'U', 'title'=>'Mid day meal', 'route'=>'mdm'),
            array('category'=>'U', 'title'=>'Teachers', 'route'=>'teachers'),

            array('category'=>'U', 'title'=>'First Grade Admission', 'route'=>'first-grade-admission'),
            array('category'=>'U', 'title'=>'Enrolment by Social Category', 'route'=>'enrolment-by-category'),
            array('category'=>'U', 'title'=>'Enrolment by Social Category - Minority', 'route'=>'enrolment-minority-category'),
            array('category'=>'U', 'title'=>'Enrolment by Medium of Instruction', 'route'=>'enrolment-by-medium'),
            array('category'=>'U', 'title'=>'LP Section', 'route'=>'enrolment-by-age'),
            array('category'=>'U', 'title'=>'UP Section', 'route'=>'enrolment-by-age-up'),
            array('category'=>'U', 'title'=>'Secondary', 'route'=>'enrolment-by-age-hs'),
            array('category'=>'U', 'title'=>'Higher Secondary', 'route'=>'enrolment-by-age-ss'),

            array('category'=>'U', 'title'=>'By Social Category', 'route'=>'repeaters-by-category'),
            array('category'=>'U', 'title'=>'Minority Category', 'route'=>'repeaters-minority-category'),

            array('category'=>'U', 'title'=>'LP Section', 'route'=>'children-with-special-needs'),
            array('category'=>'U', 'title'=>'UP Section', 'route'=>'children-with-special-needs-up'),
            array('category'=>'U', 'title'=>'Secondary', 'route'=>'children-with-special-needs-hs'),
            array('category'=>'U', 'title'=>'Higher Secondary', 'route'=>'children-with-special-needs-hss'),

            array('category'=>'U', 'title'=>'To (CWSN)-Children with Special Needs', 'route'=>'facilities-cwsn'),
            array('category'=>'U', 'title'=>'To Primary Classes', 'route'=>'facilities-to-children-primary'),
            array('category'=>'U', 'title'=>'To Upper Primary Classes', 'route'=>'facilities-to-children-up'),

            array('category'=>'U', 'title'=>'X Results by category', 'route'=>'x-result-by-category'),
            array('category'=>'U', 'title'=>'X Results by score', 'route'=>'result-by-score-x'),
            array('category'=>'U', 'title'=>'Streams available', 'route'=>'streams-available'),
            array('category'=>'U', 'title'=>'Enrolment by Stream', 'route'=>'enrolment-by-stream'),
            array('category'=>'U', 'title'=>'Repeaters by Stream', 'route'=>'repeaters-by-stream'),
            array('category'=>'U', 'title'=>'XII Results by score', 'route'=>'result-by-score-xii'),
            array('category'=>'U', 'title'=>'XII Results by stream', 'route'=>'result-by-stream'),

            array('category'=>'U', 'title'=>'RMSA Fund', 'route'=>'rmsa-fund'),
            array('category'=>'U', 'title'=>'Staff Details', 'route'=>'staff'),
            array('category'=>'G', 'title'=>'login', 'route'=>'login'),
            array('category'=>'U', 'title'=>'Examination Results', 'route'=>'exam-results'),
            // insert new links above this comment
        );

        Link::insert($data);
    }

}
