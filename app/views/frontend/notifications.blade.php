@if ($errors->any())
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Error!</b> Please check the form below for errors
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissable">
  <i class="fa fa-check"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  {{ $message }}
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Error!</b>
  {{ $message }}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissable">
  <i class="fa fa-warning"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Warning!</b>
  {{ $message }}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissable">
  <i class="fa fa-info"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Info!</b>
  {{ $message }}
</div>
@endif
