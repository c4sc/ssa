@extends('frontend.layouts.site')
@section('content')
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog login">
    <div class="modal-content login-box-green">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">

        <form class="form-horizontal" role="form" method="post" action="">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="email" id="email" placeholder="Email" autofocus>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-10">
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Remember me
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-6">
              <a href="{{ route('forgot-password') }}">Forgot Password</a>
            </div>
          </div>
        </div>

        <footer class="row text-center">
          <button type="submit" class="btn btn-default">Sign in</button>
        </footer>
      </form>
    </div>
  </div>
</div>

<div class="text-center banner-center">
  <a href="#" class="login"  data-toggle="modal" data-target="#myModal">Login</a>
  <h2>SARVA SHIKSHA ABHIYAN</h2>
  <div class="row stars">
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
    <span class="glyphicon glyphicon-star"></span>
  </div>
</div>
@stop
