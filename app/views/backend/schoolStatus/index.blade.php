@extends('backend.layouts.udise')

@section('title')
  Schools Login Status
@stop

@section('path')
@stop

@section('main-page')
  Schools
@stop
@section('script')
$(function() {
  activate('.statusGroup', '.status')
});
@stop

@section('content')
<div class="box box-primary">
  <div class="box-body">
    <form action="" method="get" class="form-horizontal form">
      <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <input type="text" name="q" id="q" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default"
                type="submit">Search!</button>
            </span>
          </div>
        </div>
      </div>
    </form>

    @if(count(@$schools))
    <table class="table">
      <thead>
        <th>School Code</th>
        <th>Name</th>
      </thead>
      <tbody>
      @foreach ($schools as $school)
      <tr>
        <td>
          {{{ $school->school_code }}}</a>
        </td>
        <a href="{{ URL::Route('show-school', $school->school_code ) }}" class="name">
        <td>
          <a href="{{ URL::Route('show-school', $school->school_code ) }}" class="name">
            {{ $school->school->SCHNAME }}</a>
        </td>
      @endforeach
      </tbody>
    </table>
    <div>
      {{ $schools->appends(array('q' => $query))->links() }}
    </div>
    @elseif(@$query)
    <br>
    <div class='alert alert-danger'>
      No results found, please try again.
    </div>
    @endif
  </div>
</div>
@stop