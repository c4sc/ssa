@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.fundGroup', '.rmsa-fund')
  function sumFund(head) {
    var total = 0;
    $('.'+head).each(function() {
      total += (parseInt($(this).val()) || 0);
    });
    $('.total-'+head).html(total);
  }
  $('.received').keyup(function() {
    sumFund('received');
  });
  $('.utilized').keyup(function() {
    sumFund('utilized');
  });
  $('.spillovers').keyup(function() {
    sumFund('spillovers');
  });
  $('.received:first').keyup()
  $('.utilized:first').keyup()
  $('.spillovers:first').keyup()
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Receipts and Expenditure - Under RMSA</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Details of school level grants</th>
                  <th>Grants Recieved</th>
                  <th>Grants Utilized</th>
                  <th>Spillovers as on 1st April, {{ date('Y') }}</th>
                </tr>
              </thead>
              <tbody>
              <tr>
                <th>Civil Works</th>
                <td>{{ Form::text("civil-received", Input::old("civil-received", $fundData[0]->AMT_R),
                  array('data-parsley-type'=>"number",
                  'data-parsley-trigger'=>'keyup', 'class'=>'form-control received', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("civil-utilized", Input::old("civil-utilized", $fundData[0]->AMT_U),
                  array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control utilized', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("civil-spillovers", Input::old("civil-spillovers", $fundData[0]->AMT_S),
                  array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control spillovers', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
              </tr>
              <tr>
                <th colspan="4">Annual School Grants (recurring)</th>
              </tr>
              @foreach(RmsaFund::$SCHOOL_GRANT_HEADS as $itemId => $item)
              <tr>
                <td>{{ $item }}</td>
                <td>{{ Form::text("school-received-$itemId", Input::old("school-received-$itemId",
                  $fundData[($itemId-1)]->AMT_R), array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control received', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("school-utilized-$itemId", Input::old("school-utilized-$itemId",
                  $fundData[($itemId-1)]->AMT_U), array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control utilized', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("school-spillovers-$itemId", Input::old("school-spillovers-$itemId",
                  $fundData[($itemId-1)]->AMT_S), array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control spillovers', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
              </tr>
              @endforeach
              <tr>
                <th>Others</th>
                <td>{{ Form::text("others-received", Input::old("others-received", $fundData[5]->AMT_R),
                  array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control received', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("others-utilized", Input::old("others-utilized", $fundData[5]->AMT_U),
                  array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control utilized', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
                <td>{{ Form::text("others-spillovers", Input::old("others-spillovers", $fundData[5]->AMT_S),
                  array('data-parsley-type'=>"number",'data-parsley-trigger'=>'keyup',
                  'class'=>'form-control spillovers', 'data-parsley-min' => '0', 'min' => '0')) }}</td>
              </tr>
              <tr>
                <th>Total</th>
                <th class="total-received"></th>
                <th class="total-utilized"></th>
                <th class="total-spillovers"></th>
              </tr>
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop


