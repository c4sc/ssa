@include('backend.school.menu')

<li class="treeview studentGroup">
  <a>
    <i class="fa fa-th"></i> <span>ADISE</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <li class='students'><a href="{{ route('students', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Students</a></li>
    <li class='transfer-list'><a href="{{ route('transfer-list', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Transfer Students</a></li>
  </ul>
</li>
