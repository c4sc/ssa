<form role="form" method="post" data-parsley-validate  action="">
  <div class="box box-warning">
    <div class="box-body">
      <fieldset>
        <div class="form-group">
          <button id="save-student" class="btn btn-primary">Save</button>
          @if($student->id)
          @if( ! $school->confirmation())
          <a class="btn btn-info" href="{{ route('transfer-student', array($schoolCode, $student->id)) }}">Transfer</a>
          <a class="delete-student btn btn-danger"  data-toggle="modal" href="#deleteStudentModal"
            data-studentid="{{ $student->id }}"
            data-studentname="{{ $student->name }}"> Delete </a>
          @endif
          @endif
        </div>
      </fieldset>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="student-id" value="{{ @$student->id }}" />

  <div class="box box-primary box-solid">
    <div class="box-header">
      <h3 class="box-title"> {{ (@$student->name )?"Edit":"New" }} Student</h3>
    </div>
    <div class="box-body">
      <table class="table">
        <tr>
          <th>Academic year</th>
          <td>{{ (@$studentDetails->academic_year )? $studentDetails->academic_year:AcademicYear::CURRENT_YEAR }} </td>
        </tr>
        <tr>
          <th>Rural or Urban</th>
          <td class="{{ $errors->has('rural_urban') ? ' has-error' : '' }}">
            {{ Form::select('rural-urban', School::$AREA, Input::old('rural-urban', $student->rural_urban),
            array('id'=>'rural-urban', 'class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('rural-urban', '<label for="rural-urban" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>

        <tr>
          <th>Habitation</th>
          <td class="{{ $errors->has('habitation') ? ' has-error' : '' }}">
            {{ Form::select('habitation', $habitations, Input::old('habitation', $student->habitation_code),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('habitation', '<label for="habitation" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
        </td>
        </tr>
        <tr>
          <th>Student Name</th>
          <td class="{{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::text('name', Input::old('name', $student->name), array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'name')) }}
            {{ $errors->first('name', '<label for="name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr>
          <th>Father Name</th>
          <td class="{{ $errors->has('father') ? ' has-error' : '' }}">
            {{ Form::text('father', Input::old('father', $student->father), array('class'=>'form-control', 'id'=>'father',
            'data-parsley-required'=>'true')) }}
            {{ $errors->first('father', '<label for="father" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr>
          <th>Mother Name</th>
          <td class="{{ $errors->has('mother') ? ' has-error' : '' }}">
            {{ Form::text('mother', Input::old('mother', $student->mother), array('class'=>'form-control', 'id'=>'mother',
            'data-parsley-required'=>'true')) }}
            {{ $errors->first('mother', '<label for="mother" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('dob', 'has-error') }}">
          <th>Date of Birth</th>
          <td>
            <div class="input-group">
              @if($student->dob)
              {{ Form::text('dob', Input::old('dob', date("d-m-Y",strtotime($student->dob))),
              array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'dob',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @else
              {{ Form::text('dob', Input::old('dob'), array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'dob',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @endif
              {{ $errors->first('dob', '<label for="dob" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
          </td>
        </tr>
        <tr class="{{ $errors->first('gender', 'has-error') }}">
          <th>Gender</th>
          <td>
            {{ Form::select('gender', Student::$GENDER, Input::old('gender', $student->gender),
            array('class'=>'form-control', 'id'=>'gender', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('gender', '<label for="gender" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('social-category', 'has-error') }}">
          <th>Social Category</th>
          <td>
            {{ Form::select('social-category', Student::$SOCIAL_CATEGORY, Input::old('social-category', $student->social_category),
            array('class'=>'form-control', 'id'=>'social-category', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('social-category', '<label for="social-category" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
        </td>
        </tr>
        <tr class="{{ $errors->first('religion', 'has-error') }}">
          <th>Religion</th>
          <td>
            {{ Form::select('religion', Student::$RELIGION, Input::old('religion', $student->religion),
            array('class'=>'form-control', 'id'=>'religion', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('religion', '<label for="religion" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('bpl', 'has-error') }}">
          <th>BPL</th>
          <td>
            {{ Form::select('bpl', School::$CHOICE, Input::old('bpl', $student->bpl_yn),
            array('class'=>'form-control', 'id'=>'bpl', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('bpl', '<label for="bpl" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('disadvantage-group', 'has-error') }}">
          <th>Disadvantage</th>
          <td>
            {{ Form::select('disadvantage-group', School::$CHOICE, Input::old('disadvantage-group', $student->disadvantagegroup_yn),
            array('class'=>'form-control', 'id'=>'disadvantage-group', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('disadvantage-group', '<label for="disadvantage-group" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('adhar', 'has-error') }}">
          <th>Aadhaar No.</th>
          <td class="{{ $errors->has('adhar') ? ' has-error' : '' }}">
            {{ Form::text('aadhaar', Input::old('aadhaar', $student->aadhaar),
            array('data-parsley-trigger'=>"keyup",'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'data-parsley-length'=>'[12,12]','data-parsley-error-message'=>"Aadhaar
            number must be twelve digits!!", 'class'=>'form-control', 'id'=>'adhar')) }}
            {{ $errors->first('adhar', '<label for="adhar" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('admission-date', 'has-error') }}">
          <th>Admission Date</th>
          <td>
            <div class="input-group">
              @if($studentDetails->admission_date)
              {{ Form::text('admission-date', Input::old('admission-date', date("d-m-Y",strtotime($studentDetails->admission_date))),
              array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'admission-date',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @else
              {{ Form::text('admission-date', Input::old('admission-date'), array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'admission-date',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @endif
              {{ $errors->first('admission-date', '<label for="admission-date" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
          </td>
        </tr>
        <tr class="{{ $errors->first('admission-no', 'has-error') }}">
          <th>Admission No.</th>
          <td class="{{ $errors->has('admission-no') ? ' has-error' : '' }}">
            {{ Form::text('admission-no', Input::old('admission-no', $studentDetails->admission_no),
            array('class'=>'form-control', 'id'=>'admission-no', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('admission-no', '<label for="admission-no" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('class', 'has-error') }}">
          <th>Class</th>
          <td>
            {{ Form::select('class', StudentDetails::$CLASS, Input::old('class', $studentDetails->class),
            array('class'=>'form-control', 'id'=>'class', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('class', '<label for="class" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('division', 'has-error') }}">
          <th>Division</th>
          <td>
            {{ Form::text('division', Input::old('division', $studentDetails->division), array('class'=>'form-control',
            'id'=>'division', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('division', '<label for="division" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('previous-class', 'has-error') }}">
          <th>Previous Class</th>
          <td>
            {{ Form::select('previous-class', StudentDetails::$PREVIOUSCLASS, Input::old('previous-class',
            $studentDetails->previous_class), array('class'=>'form-control', 'id'=>'previous-class')) }}
            {{ $errors->first('previous-class', '<label for="previous-class" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="depends-std {{ $errors->first('previous-status', 'has-error') }}">
          <th>If class I, previous status</th>
          <td>
            {{ Form::select('previous-status', StudentDetails::$PREVIOUSSTATUS, Input::old('previous-status',
            $studentDetails->previous_status), array('class'=>'form-control', 'id'=>'previous-status')) }}
            {{ $errors->first('previous-status', '<label for="previous-status" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('previous-days', 'has-error') }}">
          <th>Days attended in previous year</th>
          <td>
            {{ Form::text('previous-days', Input::old('previous-days', $studentDetails->previous_days), array('class'=>'form-control', 'id'=>'previous-days')) }}
            {{ $errors->first('previous-days', '<label for="previous-days" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('medium', 'has-error') }}">
          <th>Medium</th>
          <td>
            {{ Form::select('medium', Master::$INSTRUCTION_MEDIUMS, Input::old('medium', $studentDetails->medium),
            array('class'=>'form-control', 'id'=>'medium', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('medium', '<label for="medium" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('disability', 'has-error') }}">
          <th>Disability if any</th>
          <td>
            {{ Form::select('disability', School::$CHOICE, Input::old('disability', $student->disability_yn),
            array('class'=>'form-control', 'id'=>'disability', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('disability', '<label for="disability" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('free-education', 'has-error') }}">
          <th>RTE Admission</th>
          <td>
            {{ Form::select('free-education', School::$CHOICE, Input::old('free-education', $studentDetails->free_education_yn),
            array('class'=>'form-control', 'id'=>'free-education', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('free-education', '<label for="free-education" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
      </table>
    </div> <!-- box-body -->
  </div> <!-- box -->

  @if($master->isGov() || $master->isGovAided())
  <div class="box box-primary box-solid">
    <div class="box-header">
      <h3 class="box-title">Facilities provided during previous Academic year(To be filled by Govt. or Aided Schools only)</h3>
    </div>

    <div class="box-body">
      <table class="table">
        <tr class="{{ $errors->first('facilitiescwsn', 'has-error') }}">
          <th>CWSN Facility</th>
          <td>
            {{ Form::select('facilitiescwsn', School::$CHOICE, Input::old('facilitiescwsn', $studentDetails->facilitiescwsn),
            array('class'=>'form-control', 'id'=>'facilitiescwsn', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('facilitiescwsn', '<label for="facilitiescwsn" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('books', 'has-error') }}">
          <th>Free Text Book</th>
          <td>
            {{ Form::select('books', School::$CHOICE, Input::old('books', $studentDetails->books_yn), array('class'=>'form-control',
            'id'=>'books', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('books', '<label for="books" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('uniforms', 'has-error') }}">
          <th>Free Uniform</th>
          <td>
            {{ Form::select('uniforms', StudentDetails::$UNIFORMSETS, Input::old('uniforms', $studentDetails->uniforms),
            array('class'=>'form-control', 'id'=>'uniforms', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('uniforms', '<label for="uniforms" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('transport', 'has-error') }}">
          <th>Transport</th>
          <td>
            {{ Form::select('transport', School::$CHOICE, Input::old('transport', $studentDetails->transport_yn),
            array('class'=>'form-control', 'id'=>'transport', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('transport', '<label for="transport" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('escort', 'has-error') }}">
          <th>Escort Facility</th>
          <td>
            {{ Form::select('escort', School::$CHOICE, Input::old('escort', $studentDetails->escort_yn),
            array('class'=>'form-control', 'id'=>'escort', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('escort', '<label for="escort" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('hostel', 'has-error') }}">
          <th>Hostel Facility</th>
          <td>
            {{ Form::select('hostel', School::$CHOICE, Input::old('hostel', $studentDetails->hostel_yn),
            array('class'=>'form-control', 'id'=>'hostel', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('hostel', '<label for="hostel" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr>
          <th>Special Training</th>
          <td class="{{ $errors->first('special-training', 'has-error') }}">
            {{ Form::select('special-training', School::$CHOICE, Input::old('special-training', $studentDetails->special_training_yn),
            array('class'=>'form-control', 'id'=>'special-training', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('special-training', '<label for="special-training" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
        <tr class="{{ $errors->first('homeless', 'has-error') }}">
          <th>Homeless</th>
          <td>
            {{ Form::select('homeless', School::$CHOICE, Input::old('homeless', $studentDetails->homeless_yn),
            array('class'=>'form-control', 'id'=>'homeless', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('homeless', '<label for="homeless" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </td>
        </tr>
      </table>
    </div> <!-- box-body -->
  </div> <!-- box -->
  @endif

  <div class="box box-warning">
    <div class="box-body">
      <fieldset>
        <div class="form-group">
          <button id="save-school" class="btn btn-primary">Save</button>
          @if($student->id)
          @if( ! $school->confirmation())
          <a class="btn btn-info" href="{{ route('transfer-student', array($schoolCode, $student->id)) }}">Transfer</a>
          <a class="delete-student btn btn-danger"  data-toggle="modal" href="#deleteStudentModal"
            data-studentid="{{ $student->id }}"
            data-studentname="{{ $student->name }}"> Delete </a>
          @endif
          @endif
        </div>
      </fieldset>
    </div>
  </div>
</form>