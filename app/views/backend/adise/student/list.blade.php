@extends('backend.layouts.adise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {

  activate('.studentGroup', '.students')

  $('.delete-student').click( function () {
    var studentId = $(this).data('studentid');
    var studentName = $(this).data('studentname');
    $("#idstudent").val( studentId );
    $("#student-name").html( studentName );
  });

  $('.edit-student').click( function (e) {
    e.preventDefault()
    var student = $(this).data('student');
    $('#studentId').val(student)
    $('#studentId').click()
  });

  $('#class').change( function (e) {
    if($(this).val() == 1) {
        $('.depends-std').show();
        $('.depends-std select, .depends-std input').attr('data-parsley-required', 'true');
    } else {
      $('.depends-std').hide();
      $('.depends-std input, .depends-std select').val('');
      $('.depends-std select, .depends-std input').attr('data-parsley-required', 'false');
    }
    // function to change previous class corresponding to current class
     fillSelect($(this).val());
    $('select[name=previous-class]');
  });

  $('#class').change();
  $('#dob').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      startDate: '-25y',
      endDate: '-4y',
      startView: 2,

  });
  $('#admission-date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: 'true',
    startDate: '-40y',
    endDate: '-0d',
  });
  function fillSelect(classId) {
    $('select[name=previous-class]').empty();
    if (classId <= 1) { // for pre-primary and 1st standard 'None' is an option
      $('select[name=previous-class]').append(new Option('None', 9));
      if (classId == 0) {
        return;
      }
    }
    var data =['Pre Primary', 1, 2, 3, 4, 5, 6, 7, 8];
    options = [];
    options.push(new Option(data[classId-1], classId-1));
    options.push(new Option(data[classId], classId));
    $('select[name=previous-class]').append(options);
  }
});
@parent
@stop

@section('content')
<div class="modal fade" id="deleteStudentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Confirm Delete Student</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete student, <span id="student-name">  </span>?</p>
      </div>
      <div class="modal-footer">
        <form method="post"
          action="{{URL::Route("adiseDeleteStudent", $master->SCHCD)}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <input type="hidden" id="idstudent" name="idstudent" value="">
          <input type="hidden" name="schoolCode" value="{{ $schoolCode }}" />
          <!--<button class="btn btn-danger">Delete Student</button> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          {{ Form::submit('Delete Student', array('class' => "btn btn-danger")) }}
        </form>
      </div>
    </div>
  </div>
</div>


<div class="col-md-5">
  <div class="box box-primary box-solid">
    <div class="box-header">
      <h3 class="box-title">Search Student</h3>
    </div>
    <div class="box-body">
      <form action="" method="get">
        <div class="row">
          <div class="col-xs-3">
            {{ Form::text('class', Input::old('class', $class), array('class'=>'form-control', 'placeholder'=>'Class')) }}
          </div>
          <div class="col-xs-3">
            {{ Form::text('division', Input::old('division', $division), array('class'=>'form-control', 'placeholder'=>'Division')) }}
          </div>
          <div class="col-xs-3">
            {{ Form::select('gender', Student::$GENDER, Input::old('gender', $gender), array('class'=>'form-control')) }}
          </div>
          <div class="col-xs-3">
            {{ Form::select('ac_year', $acYearList, Input::old('ac_year', $academicYear), array('class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group" style="margin-top:5px;">
          {{ Form::text('q', Input::old('q', $query), array('class'=>'form-control', 'placeholder'=>'Name')) }}
        </div>
        <div class="form-group">
          <button class="btn btn-info" type="submit">Search!</button>
          <button class="btn btn-primary" id="studentId" name="studentId" value="new">new student</button>
        </div>
        <div>
          {{ $studentList->appends(
          array(
            'q' => $query,
            'class' => $class,
            'division' => $division,
            'gender' => $gender,
            'ac_year' => $academicYear))->links() }}
        </div>
      </form>
    </div>
  </div>

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title"> List Students</h3>
    </div>
    <div class="box-body">
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Admission Number</th>
            <th>Date of Birth</th>
          </tr>
        </thead>
        <tbody>
        @foreach($studentList as $s)
        <tr>
          <input type="hidden" name="schoolCode" value="{{ $schoolCode }}" />
          <td><a data-student='{{ $s->id }}' class="edit-student" href="#">{{$s->name }}</a></td>
          <td>{{ $s->admission_no }}</td>
          <td>{{ date("d-m-Y",strtotime($s->dob)) }}</td>
        </tr>
        @endforeach
        </tbody>
      </table>
      <div>
        {{ $studentList->appends(
        array(
        'q' => $query,
        'class' => $class,
        'division' => $division,
        'gender' => $gender,
        'ac_year' => $academicYear))->links() }}
      </div>
    </div> <!-- box-body -->
  </div> <!-- box -->
</div>
<div class="col-md-7">
  @include('backend.adise.student.edit')
</div>
@stop

