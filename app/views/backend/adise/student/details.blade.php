@extends('backend.layouts.adise')

@section('title')
@stop

@section('patd')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="box box-primary box-solid">
      <div class="box-header">
          <h3 class="box-title"> Student Details</h3>
      </div>
      <div class="box-body">
        <table class="table">
        </tbody>
            <tr>
              <td>Student Name</td>
              <td>{{ $student->name }}</td>
            </tr>
            <tr>
              <td>Father Name</td>
              <td>{{ $student->father }}</td>
            </tr>
            <tr>
              <td>Mother Name</td>
              <td>{{ $student->mother }}</td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>{{ choiceLabel($student->gender, $gender) }}</td>
            </tr>
            <tr>
              <td>Date of Birth</td>
              <td>{{ $student->dob }}</td>
            </tr>
            <tr>
              <td>Adhaar Number</td>
              <td>{{ $student->aadhaar }}</td>
            </tr>
            <tr>
              <td>Religion</td>
              <td>{{ choiceLabel($student->religion, $religion) }}</td>
            </tr>
            <tr>
              <td>Social Category</td>
              <td>{{ choiceLabel($student->social_category, $socialcategory) }}</td>
            </tr>
            <tr>
              <td>Date of Admission</td>
              <td>{{ $studentdetails->admission_date }}</td>
            </tr>
            <tr>
              <td>Admission Number</td>
              <td>{{ $studentdetails->admission_no }}</td>
            </tr>
            <tr>
              <td>Class</td>
              <td>{{ $studentdetails->class }}</td>
            </tr>
            <tr>
              <td>Medium of Instruction</td>
              <td>{{ choiceLabel($studentdetails->medium, $mediums) }}</td>
            </tr>
          <tbody>
        </table>
      </div> <!-- box-body -->
    </div> <!-- box -->
  </div> <!-- col -->
</div>
@stop
