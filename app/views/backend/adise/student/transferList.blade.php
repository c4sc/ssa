@extends('backend.layouts.adise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop
@section('script')
$(function() {

  activate('.studentGroup', '.transfer-list');

  $('.transfer-student').click( function () {
    var studentId = $(this).data('studentid');
    var studentName = $(this).data('studentname');
    var fromSchool = $(this).data('fromschool');
    var toSchool = $(this).data('toschool');
    $("#student-id").val( studentId );
    $("#student-name").html( studentName );
  });
  $('.cancel-transfer-student').click( function () {
    var studentId = $(this).data('studentid');
    var studentName = $(this).data('studentname');
    var fromSchool = $(this).data('fromschool');
    var toSchool = $(this).data('toschool');
    $("#student-id2").val( studentId );
    $("#student-name").html( studentName );
  });
});

@stop
@section('content')
<div class="modal fade" id="transferStudentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Confirm Transfer Student</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to Transfer student <span id="student-name"></span> from School <span id="from-school"></span>
        to School {{ $master->SCHCD }} ?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('transfer-list', $master->SCHCD) }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <input type="hidden" id="student-id" name="student-id" value="">
          <input type="hidden" name="schoolCode" value="{{ $master->SCHCD }}" />
          <button class="btn btn-danger">Transfer Student</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cancelTransferStudentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Confirm Cancel Transfering Student</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to cancel Transfer student <span id="student-name"></span> from School <span
          id="from-school"></span> to School {{ $master->SCHCD }} ?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('cancel-tranfer', $master->SCHCD) }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <input type="hidden" id="student-id2" name="student-id" value="">
          <input type="hidden" name="schoolCode" value="{{ $master->SCHCD }}" />
          <button class="btn btn-danger">Cancel Transfer</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-7">
    <div class="box box-primary box-solid">
      <div class="box-header">
        <h3 class="box-title"> New Students Seeking Admission</h3>
      </div>
      <div class="box-body">
        <table class="table">
          <thead>
            <tr>
              <th colspan="2">School - {{ $master->SCHCD . '-' . $master->school->SCHNAME }}</th>
            </tr>
            <tr>
              <th>Name of Student</th>
              <th>School</th>
            </tr>
          </thead>
          <tbody>
          @foreach($studentsToBeApproved as $student)
          <tr>
            <input type="hidden" name="schoolCode" value="{{ $master->SCHCD }}" />
            <td>{{ $student->name }}</td>
            <td>{{ $student->from_school }}</td>
            <td>
              <a class="transfer-student btn btn-primary" data-toggle="modal" href="#transferStudentModal"
                data-fromschool="{{ $student->from_school }}"
                data-toschool="{{ $student->to_school }}"
                data-studentid="{{ $student->student_id }}"
                data-studentname="{{ $student->name }}">Approve Transfer</a>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div> <!-- box-body -->
    </div> <!-- box -->
  </div> <!-- col -->

  <div class="col-md-5">
    <div class="box box-primary box-solid">
      <div class="box-header">
        <h3 class="box-title"> Transfer List</h3>
      </div>
      <div class="box-body">
        <table class="table">
          @foreach($transferingStudents as $student)
          <tr>
            <th>
              {{ Form::label($student->name) }}

            </th>
            <td>
              <a class="cancel-transfer-student btn btn-danger" data-toggle="modal" href="#cancelTransferStudentModal"
                data-fromschool="{{ $student->from_school }}"
                data-toschool="{{ $student->to_school }}"
                data-studentid="{{ $student->student_id }}"
                data-studentname="{{ $student->name }}">Cancel</a>
            </td>
          </tr>
          @endforeach
        </table>
        @if( ! count($transferingStudents) > 0 )
          <h3>No pending transfers</h3>
        @endif
      </div> <!-- box-body -->
    </div> <!-- box -->
  </div> <!-- col -->

</div>
@stop

