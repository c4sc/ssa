@extends('backend.layouts.adise')

@section('title')
Transfer
@stop

@section('script')
$(function(){
  $("#block").remoteChained("#district", "/api/blocks.json");
  $("#to_school").remoteChained("#block", "/api/schools.json");
});
@stop

@section('content')
<form role="form" method="post" data-parsley-validate  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-transfer" class="btn btn-primary">Transfer</button>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Transfer Details</h3>
        </div>
        <div class="box-body">
          <table class="table">
            <tr>
              <th colspan="4">Student</th>
              <th colspan="4">{{ $student->name }}</th>
            </tr>
            <tr>
              <th colspan="4">Present School</th>
              <th colspan="4">{{ $schoolCode. "-".$school->SCHNAME }}</th>
            </tr>
          </table>
          <div class="form-group">
            <label class="control-label" for="district">Select District to Transfer</label>
          <div class="input-group">
              <th colspan="4">{{ $school->SCHCD. "-".$school->SCHNAME }}</th>
            </tr>
          </table>
          <br>
          <div class="form-group">
            <label class="control-label" for="district">Select District</label>
            {{ Form::select('district', $districts, Input::old('district', $districts),
            array('id'=>'district','class'=>'form-control')) }}
            {{ $errors->first('district',
               '<label for="village" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group">
            <label class="control-label" for="block">Select Block</label>
            {{ Form::select('block', $blocks, Input::old('block', $blocks),
            array('id'=>'block','class'=>'form-control', 'data-parsley-required' => 'required')) }}
            {{ $errors->first('district',
               '<label for="block" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group">
            <label class="control-label" for="to_school">Select School</label>
            {{ Form::select('to_school', array('Select'), null, array('id'=>'to_school','class'=>'form-control')) }}
            {{ $errors->first('to_school',
               '<label for="to_school" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-transfer" class="btn btn-primary">Transfer</button>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
