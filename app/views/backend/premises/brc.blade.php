@extends('backend.layouts.dashboard')

@section('title')
Premises
@stop


@section('script')
$(function() {
activate('.brc', '.brc-premises')
});
@stop

@section('content')
<form class="form-horizontal" method="post" action="">
  <fieldset>
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <div class="col-md-4">
              <button id="save-school" class="btn btn-primary">Save</button>
            </div>
          </div>
        </fieldset>
      </div>
    </div>
    <div class="box box-primary box-solid">
      <div class="box-header">
        <h3 class="box-title">Premises</h3>
      </div>
      <div class="box-body">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="form-group">
          <label class="col-md-4 control-label" for="constituencies">Assembly Constituency</label>
          <div class="col-md-4">
            {{ Form::select('constituencies[]', $constituencies, Input::old('constituencies[]', $blockConstituencies),
            array('size'=>5, 'multiple'=>true, 'id'=>'constituencies','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="aeos">Education Block</label>
          <div class="col-md-4">
            {{ Form::select('aeos[]', $aeos, Input::old('aeos[]', $blockAeos), array('size'=>7,
            'multiple'=>true, 'id'=>'aeos','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="villages">Village</label>
          <div class="col-md-4">
            {{ Form::select('villages[]', $villages, Input::old('villages[]', $blockVillages), array('size'=>15,
            'multiple'=>true, 'id'=>'villages','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="municipalities">Municipality</label>
          <div class="col-md-4">
            {{ Form::select('municipalities[]', $municipalities, Input::old('municipalities[]', $blockMunicipalities),
            array('size'=>5, 'multiple'=>true, 'id'=>'municipalities','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="cities">Cities</label>
          <div class="col-md-4">
            {{ Form::select('cities[]', $cities, Input::old('cities[]', $blockCities), array('size'=>5,
            'multiple'=>true, 'id'=>'cities','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="panchayaths">Panchayath</label>
          <div class="col-md-4">
            {{ Form::select('panchayaths[]', $panchayaths, Input::old('panchayaths[]', $blockPanchayaths), array('size'=>15,
            'multiple'=>true, 'id'=>'panchayaths','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="clusters">Clusters</label>
          <div class="col-md-4">
            {{ Form::select('clusters[]', $clusters, Input::old('clusters[]', $blockClusters), array('size'=>15,
            'multiple'=>true, 'id'=>'clusters','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="habitations">Habitations</label>
          <div class="col-md-4">
            {{ Form::select('habitations[]', $habitations, Input::old('habitations[]', $blockHabitations), array('size'=>15,
            'multiple'=>true, 'id'=>'habitations','class'=>'form-control')) }}
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="villages">&nbsp;</label>
          <div class="col-md-4">
            <button class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <div class="col-md-4">
              <button id="save-school" class="btn btn-primary">Save</button>
            </div>
          </div>
        </fieldset>
      </div>
    </div>
  </fieldset>
</form>
@stop




