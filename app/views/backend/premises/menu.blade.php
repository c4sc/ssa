@if(Sentry::getUser()->scope == AuthorizedController::$SCOPE_BLOCK)
<li class="treeview brc">
<a>
  <i class="fa fa-cogs"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu">
  <li class='brc-premises'><a href="{{ route('brc-premises', Sentry::getUser()->scope_id) }}"><i class="fa fa-angle-double-right"></i>
    Premises</a></li>
</ul>
</li>
<li class="treeview statusGroup">
<a>
  <i class="fa fa-info"></i> <span>Status</span>
</a>
<ul class="treeview-menu">
  <li class='status'>
  <a href="{{ route('status')}}"><span class="loginStatus">Login Status</span></a>
  </li>
</ul>
</li>
@endif


