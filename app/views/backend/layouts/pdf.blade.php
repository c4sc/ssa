<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
  SSA |
  @section('title')
  @show
</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
{{ HTML::style('assets/css/pdf.min.css'); }}
@yield('style')
{{ HTML::script('assets/javascript/pdf.min.js'); }}
</head>
<body class="skin-black">
<div id="wrapper">
  <section class="content">
    @yield('content')
  </section><!-- /.content -->
</div>
<script type="text/javascript">
@yield('script')
</script>
</body>
</html>
