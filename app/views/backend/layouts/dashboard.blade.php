<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
  SSA |
  @section('title')
  @show
</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
{{ HTML::style('assets/css/backend.min.css'); }}
@yield('style')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
{{ HTML::script('assets/javascript/backend.min.js'); }}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55912789-1', 'auto');
    ga('send', 'pageview');

</script>
<noscript>
  <div class="container">
    <div class="row vertical-center-row">
        <div class="col-lg-12">
            <div class="row ">
              <div class="col-xs-4 col-xs-offset-4" style="background:black;color:white">
                <h3>
                  JavaScript is disabled! Why you want to do so? <br/>Please enable JavaScript in your web browser!
                </h3>
              </div>
            </div>
        </div>
    </div>
  </div>
  <style type="text/css">
    .wrapper { display:none; }
    .header { opacity: 0.4;
              filter: alpha(opacity=40); /* For IE8 and earlier */
          }
    a {
      font-size: 0px;
    }
    html, body, .container {
    height: 100%;
    }
    .container {
        display: table;
        vertical-align: middle;
        opacity: 0.4;
        filter: alpha(opacity=75); 
    }
    .vertical-center-row {
        display: table-cell;
        vertical-align: middle;
    }
    
  </style>
</noscript>
</head>
<body class="skin-black fixed">
<!-- header logo: style can be found in header.less -->
<header class="header logo-header">
  <a href="{{ URL::Route('login') }}" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    SSA
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    @include('backend.layouts.menu')
    <div class="navbar-right">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
        <a class="dropdown-toggle" data-toggle="dropdown">
          <i class="glyphicon glyphicon-user"></i>
          <span>{{ Sentry::getUser()->first_name.' '.Sentry::getUser()->last_name }}<i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header bg-light-blue">
          <p>
          {{ Sentry::getUser()->first_name.' '.Sentry::getUser()->last_name }}
          </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
          <div class="pull-left">
            <a href="{{ URL::to('account') }}" class="btn btn-default btn-flat">Profile</a>
          </div>
          <div class="pull-right">
            <a href="{{ URL::to('logout') }}" class="btn btn-default btn-flat">Logout</a>
          </div>
          </li>
        </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image"></div>
        <div class="pull-left info">
          <p>Hello, {{{ Sentry::getUser()->first_name.'' }}}</p>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="{{ route('schools') }}" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="School Code..." value="{{{ Input::get('q') }}}" />
          <span class="input-group-btn">
            <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        @yield('menu')
      </ul>
      <br>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Right side column. Contains the navbar and content of the page -->
  <aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @section('title')
        @show
        <small>
          @section('main-page')
          @show
        </small>
      </h1>
      <ol class="breadcrumb">
        @section('path')
        @show
        <li class="active">
        @section('title')
        @show
        </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('frontend.notifications')
      @yield('content')
    </section><!-- /.content -->
  </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<script type="text/javascript">
@yield('script')
</script>
</body>
</html>
