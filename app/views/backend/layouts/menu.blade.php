@if((Sentry::getUser()->scope == AuthorizedController::$SCOPE_SCHOOL))
<div class="navbar-left">
  <ul class="nav navbar-nav">
    <li><a href="{{ route('udise-status', Sentry::getUser()->scope_id) }}">Udise</a></li>
    <li><a href="{{ route('students', Sentry::getUser()->scope_id) }}">Adise</a></li>
  </ul>
</div>
<!-- TODO: Use corresponding menu. -->
@elseif((Sentry::getUser()->scope == AuthorizedController::$SCOPE_BLOCK))
<div class="navbar-left">
  <ul class="nav navbar-nav">
    <li><a href="{{ route('schools') }}">Schools</a></li>
    <li><a href="{{ route('school-users') }}">Users</a></li>
  </ul>
</div>
@elseif((Sentry::getUser()->scope == AuthorizedController::$SCOPE_DISTRICT))
<div class="navbar-left">
  <ul class="nav navbar-nav">
    <li><a href="{{ route('schools') }}">Schools</a></li>
    <li><a href="{{ route('school-users') }}">Users</a></li>
  </ul>
</div>
@elseif(( Sentry::getUser()->scope == AuthorizedController::$SCOPE_STATE))
<div class="navbar-left">
  <ul class="nav navbar-nav">
    <li><a href="{{ route('schools') }}">Schools</a></li>
    <li><a href="{{ route('school-users') }}">Users</a></li>
    <li><a href="{{ route('report-comparison-enrolment') }}">Reports</a></li>
  </ul>
</div>
@endif
