@extends('backend.layouts.dashboard')

@section('main-page')
UDISE - {{ $master->SCHCD }}
@stop

@section('path')
<li><a href="{{ route('show-school', $master->SCHCD) }}"><i class="fa fa-bookmark"></i> School</a></li>
<li><a href="{{ route('udise-status', $master->SCHCD) }}">Udise</a></li>
@stop

@section('menu')
  @include('backend.udise.menu')
@stop
