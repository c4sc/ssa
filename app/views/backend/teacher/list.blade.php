@extends('backend.layouts.udise')

@section('title')
Teachers
@stop

@section('script')
$(function() {

  activate('.particularsGroup', '.teachers')

  $('.delete-teacher').click( function () {
    var teacherId = $(this).data('teacherid');
    var teacherName = $(this).data('teachername');
    $(".modal-footer #teacher-id").val( teacherId );
    $(".modal-body #teacher-name").html( teacherName );
  });
});
@stop

@section('content')

<div class="modal fade" id="deleteTeacherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Confirm Delete Teacher</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete teacher, <span id="teacher-name"></span> ?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('teacher-delete', $master->SCHCD) }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <input type="hidden" id="teacher-id" name="teacher-id" value="">
          <button class="btn btn-danger">Delete Teacher</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    @foreach ($warnings as $warning)
    <div class="alert alert-danger">
      <i class="fa fa-ban"></i>
      <b>Error!</b>
      {{ $warning->message }}
    </div>
    @endforeach
  </div>
</div>
<div class="row">
  <div class="box box-warning">
    <div class="box-body">
      @if( ! $master->school->confirmation())
      <a class="btn btn-primary" href="{{ route('new-teacher', $master->SCHCD) }}">New Teacher</a>
      @endif
      @include('backend.udise.neighborLinks')
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
        <table class="table">
          <thead>
            <th>Category</th>
            <th>Number of Teachers</th>
          </thead>
          <tbody>
          @foreach($categoryTeachers as $category)
          <tr>
            <td>{{ isset(Teacher::$TYPE_CATEGORY[$category->CATEGORY])?Teacher::$TYPE_CATEGORY[$category->CATEGORY]:'' }}</td>
            <td>{{ $category->count }}</td>
          </tr>
          @endforeach
          </tbody>
          <tr>
            <th>Total</th>
            <th>{{ $teacherCount }}</th>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title"> Teachers</h3>
      </div>
      <div class="box-body">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Class taught</th>
              <th>Category</th>
              <th>Email</th>
              <th>Mobile</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          @foreach($teachers as $teacher)
          <tr>
            <td><a href="{{ route(($master->school->confirmation()?'show-teacher':'edit-teacher'), array($master->SCHCD, $teacher->TCHCD1)) }}">
                {{ $teacher->TCHNAME }}</a></td>
            <td>{{ Teacher::$CLASSES_TAUGHT[$teacher->CLS_TAUGHT] }}</td>
            <td>{{ isset(Teacher::$TYPE_CATEGORY[$teacher->CATEGORY])?Teacher::$TYPE_CATEGORY[$teacher->CATEGORY]:'' }}</td>
            <td>{{ $teacher->EMAIL }}</td>
            <td>{{ $teacher->MOBILE }}</td>
            <td>
              @if( ! $master->school->confirmation())
              <a class="delete-teacher btn btn-danger"  data-toggle="modal" href="#deleteTeacherModal"
                data-teacherid="{{ $teacher->TCHCD1 }}"
                data-teachername="{{ $teacher->TCHNAME }}"> Delete </a>
              @endif
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div> <!-- box-body -->
    </div> <!-- box -->
  </div> <!-- col -->
</div>
@stop

