@extends('backend.layouts.udise')

@section('title')
{{ (@$teacher->TCHNAME )?"Edit":"New" }} Teacher
@stop

@section('script')
$(function() {
activate('.particularsGroup', '.teachers')
});
var dob = 0;
$('#dob')
  .datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    endDate: '-18y',
    startDate: '-57y',
    startView: 2,
  });
  $('#dob').on('ready keyup keypress blur change', function(){
    dob = parseInt($('#dob').val().split('-')[2]) + 18;
    console.log(dob);
  })
  $('#join-date').datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years", 
    autoclose: true,
    startDate: dob,
    endDate: '+0y',
    // I dont Know what is happening with startDate please help me with this @rashivkp
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />

  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> {{ (@$teacher->TCHNAME )?"Edit":"New" }} Teacher</h3>
        </div>
        <div class="box-body">

          <div class="form-group {{ $errors->has('pen') ? ' has-error' : '' }}">
            <label class="control-label" for="pen">Pen No.</label>
            {{ Form::text('pen', Input::old('pen', $teacher->PENNO), array('data-parsley-trigger'=>"keyup",'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'id'=>'pen')) }}
            {{ $errors->first('pen', '<label for="pen" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label" for="name">Name</label>
            {{ Form::text('name', Input::old('name', $teacher->TCHNAME), array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'name')) }}
            {{ $errors->first('name', '<label for="name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
            <label class="control-label" for="mobile">Mobile</label><span class="glyphicon glyphicon-phone"></span>
            {{ Form::text('mobile', Input::old('mobile', $teacher->MOBILE), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0',
            'data-parsley-maxlength'=>'10','data-parsley-required'=>'true','data-parsley-trigger'=>"keyup", 'class'=>'form-control', 'id'=>'mobile')) }}
            {{ $errors->first('mobile', '<label for="mobile" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class=" control-label" for="email">email</label>
            <div class="input-group">
              {{ Form::text('email', Input::old('email', $teacher->EMAIL), array('data-parsley-type'=>"email",'class'=>'form-control',
              'placeholder'=>'email', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('email', '<label for="email" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->first('adhar', 'has-error') }}">
            <label class="control-label" for="adhar">Aadhaar No.</label>
            {{ Form::text('adhar', Input::old('adhar', $teacher->AADHAAR),
            array('data-parsley-trigger'=>"keyup",'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[12,12]','data-parsley-error-message'=>"Aadhaar
            number must be twelve digits!!", 'class'=>'form-control', 'id'=>'adhar')) }}
            {{ $errors->first('adhar', '<label for="adhar" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('gender', 'has-error') }}">
            <label class="control-label" for="gender">Gender</label>
            {{ Form::select('gender', Teacher::$GENDER, Input::old('gender', $teacher->SEX),
            array('class'=>'form-control', 'id'=>'gender')) }}
            {{ $errors->first('gender', '<label for="gender" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('social-category', 'has-error') }}">
            <label class="control-label" for="social-category">Social Category</label>
            {{ Form::select('social-category', Teacher::$SOCIAL_CATEGORY, Input::old('social-category', $teacher->CASTE),
            array('class'=>'form-control', 'id'=>'social-category')) }}
            {{ $errors->first('social-category', '<label for="social-category" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('teacher-type', 'has-error') }}">
            <label class="control-label" for="teacher-type">Type of Teacher</label>
            {{ Form::select('teacher-type', Teacher::$TYPE_CATEGORY, Input::old('teacher-type', $teacher->CATEGORY),
            array('class'=>'form-control', 'id'=>'teacher-type')) }}
            {{ $errors->first('teacher-type', '<label for="teacher-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('appointment-nature', 'has-error') }}">
            <label class="control-label" for="appointment-nature">Nature of Appointment</label>
            {{ Form::select('appointment-nature', Teacher::$APPOINTMENT_NATURE, Input::old('appointment-nature', $teacher->POST_STATUS),
            array('class'=>'form-control', 'id'=>'appointment-nature')) }}
            {{ $errors->first('appointment-nature', '<label for="appointment-nature" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('dob', 'has-error') }}">
            <label class="control-label" for="dob">Date of Birth</label>
            <div class="input-group">
              @if($teacher->DOB)
              {{ Form::text('dob', Input::old('dob', date("d-m-Y",strtotime($teacher->DOB))),
              array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'dob',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @else
              {{ Form::text('dob', Input::old('dob'), array('data-parsley-required'=>"true",'class'=>'form-control', 'id'=>'dob',
              'data-provide'=>'datepicker', 'data-date-format'=>'dd-mm-yyyy', 'data-date-start-view'=>'2',
              'data-date-start-date'=>'')) }}
              @endif
              {{ $errors->first('dob', '<label for="dob" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
            </div>
          </div>

          <div class="form-group {{ $errors->first('join-date', 'has-error') }}">
            <label class="control-label" for="join-date">Year of joining in service</label>
            {{ Form::text('join-date', Input::old('join-date', $teacher->YOJ),
            array('data-parsley-trigger'=>"keyup",'data-parsley-required'=>"true",'data-parsley-maxlength'=>'4','data-parsley-error-message'=>"Year must be four digits!!",'class'=>'form-control','id'=>'join-date', 'data-provide'=>'datepicker')) }}
            {{ $errors->first('join-date', '<label for="join-date" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('academic', 'has-error') }}">
            <label class="control-label" for="academic">Highest Academic Qualification</label>
            {{ Form::select('academic', Teacher::$ACADEMIC_QUALIFICATION, Input::old('academic', $teacher->QUAL_ACAD), array('class'=>'form-control', 'id'=>'academic')) }}
            {{ $errors->first('academic', '<label for="academic" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('professional', 'has-error') }}">
            <label class="control-label" for="professional">Highest Professional Qualification</label>
            {{ Form::select('professional', Teacher::$PROF_QUALIFICATION, Input::old('professional', $teacher->QUAL_PROF), array('class'=>'form-control', 'id'=>'professional')) }}
            {{ $errors->first('professional', '<label for="professional" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('classes-taught', 'has-error') }}">
            <label class="control-label" for="classes-taught">Classes Taught</label>
            {{ Form::select('classes-taught', Teacher::$CLASSES_TAUGHT, Input::old('classes-taught', $teacher->CLS_TAUGHT), array('class'=>'form-control', 'id'=>'classes-taught')) }}
            {{ $errors->first('classes-taught', '<label for="classes-taught" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('appointed-subject', 'has-error') }}">
            <label class="control-label" for="appointed-subject">Appointed Subject</label>
            {{ Form::select('appointed-subject', $subjectList, Input::old('appointed-subject', $teacher->APPOINTSUB), array('class'=>'form-control', 'id'=>'appointed-subject')) }}
            {{ $errors->first('appointed-subject', '<label for="appointed-subject" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('subject1', 'has-error') }}">
            <label class="control-label">Main Subject Taught</label>
            <div class="input-group">
              <span class="input-group-addon">Subject 1</span>
              {{ Form::select('subject1', $subjectList, Input::old('subject1', $teacher->MAIN_TAUGHT1), array('class'=>'form-control', 'id'=>'subject1')) }}
              {{ $errors->first('subject1', '<label for="subject1" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group">
              <span class="input-group-addon">Subject 2</span>
              {{ Form::select('subject2', $subjectList, Input::old('subject2', $teacher->MAIN_TAUGHT2), array('class'=>'form-control', 'id'=>'subject2')) }}
              {{ $errors->first('subject2', '<label for="subject2" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group">
            <label class="control-label">Total days of training recieved in last academic year</label>
            <table class="table">
              <thead>
                <tr>
                  <th>Training by</th>
                  <th>Total Days</th>
                </tr>
              </thead>
              <tbody>
              @foreach(Teacher::$TRAINING_SOURCE as $source)
              <tr>
                <td>{{ $source }}</td>
                <td>{{ Form::text("$source-training", Input::old("$source-training", $teacher->{"TRN_$source"}),
                  array('data-parsley-trigger'=>"keyup",'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','class'=>'form-control')) }}</td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

          <div class="form-group {{ $errors->first('days-on-assignments','has-error') }}">
            <label class="control-label" for="days-on-assignments">No. of working days spent on non-teaching assignments</label>
            {{ Form::text('days-on-assignments', Input::old('days-on-assignments', $teacher->NONTCH_ASS),
            array('data-parsley-trigger'=>"keyup",'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','class'=>'form-control', 'id'=>'days-on-assignments')) }}
            {{ $errors->first('days-on-assignments', '<label for="days-on-assignments" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('maths-qualification','has-error') }}">
            <label class="control-label" for="maths-qualification">Maths/Science Studied upto</label>
            {{ Form::select('maths-qualification', Teacher::$ACADEMIC_QUALIFICATION, Input::old('maths-qualification', $teacher->MATH_UPTO), array('class'=>'form-control', 'id'=>'maths-qualification')) }}
            {{ $errors->first('maths-qualification', '<label for="maths-qualification" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('english-qualification','has-error') }}">
            <label class="control-label" for="english-qualification">English/Languages studied upto</label>
            {{ Form::select('english-qualification', Teacher::$ACADEMIC_QUALIFICATION, Input::old('english-qualification', $teacher->ENG_UPTO), array('class'=>'form-control', 'id'=>'english-qualification')) }}
            {{ $errors->first('english-qualification', '<label for="english-qualification" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('ss-qualification','has-error') }}">
            <label class="control-label" for="ss-qualification">Social Studies studied upto</label>
            {{ Form::select('ss-qualification', Teacher::$ACADEMIC_QUALIFICATION, Input::old('ss-qualification', $teacher->SCIENCEUPTO), array('class'=>'form-control', 'id'=>'ss-qualification')) }}
            {{ $errors->first('ss-qualification', '<label for="ss-qualification" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('current-school-joining-year','has-error') }}">
            <label class="control-label" for="current-school-joining-year">Working in present school since(year)</label>
            {{ Form::text('current-school-joining-year', Input::old('current-school-joining-year',
            $teacher->WORKINGSINCE), array('data-parsley-trigger'=>"keyup",'data-parsley-required'=>"true",'data-parsley-maxlength'=>'4','data-parsley-error-message'=>"Year must be four digits!!",'class'=>'form-control', 'id'=>'current-school-joining-year')) }}
            {{ $errors->first('current-school-joining-year', '<label for="current-school-joining-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('disablity-type','has-error') }}">
            <label class="control-label" for="disablity-type">Type of Disablity, if any</label>
            {{ Form::select('disablity-type', Teacher::$DISABILITY, Input::old('disablity-type', $teacher->DISABILITY_TYPE),
            array('class'=>'form-control', 'id'=>'disablity-type')) }}
            {{ $errors->first('disablity-type', '<label for="disablity-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('cwsn-training','has-error') }}">
            <label class="control-label" for='cwsn-training'>Whether trained for teaching CWSN
            </label>
            {{ Form::select('cwsn-training', Teacher::$CHOICE, Input::old('cwsn-training',
            $teacher->CWSNTRAINED_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('cwsn-training', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('ict-training','has-error') }}">
            <label class="control-label" for='ict-training'>Whether ICT training received
            </label>
            {{ Form::select('ict-training', Teacher::$CHOICE, Input::old('ict-training',
            $teacher->SUPVAR3), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('ict-training', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop


