@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.facilityGroup', '.exam-results')

  $('.form-control').keyup(function() {
    var ids = this.id.split('-')
    category = ids[0]
    gender = ids[1]
    class_id = ids[2]
    head = ids[3]
    head_sum = 0
    $('.'+gender+'-'+class_id+'-'+head).each(function(){
      head_sum += (parseInt($(this).val()) || 0);
    });
    $('.total-'+gender+'-'+class_id+'-'+head).html(head_sum)
  });

  $('form tr td input').keyup()
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <div class="form-group">
          <button id="save-school" class="btn btn-primary">Save</button>
          @include('backend.udise.neighborLinks')
        </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Examination Results (Last Academic Year)</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <?php
            $categoryRowCount = count($classes) * 2;
            ?>
            <table class="enrolment table table-bordered">
              <thead>
                <tr>
                  <th>Category</th>
                  <th>Grade</th>
                  <th>Gender</th>
                  @foreach(Result::$RESULT_HEADS as $headId => $head)
                  <th>{{ $head }}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
              @foreach(Result::$CATEGORY_LIST as $cat)
              <tr>
                <th rowspan='{{ $categoryRowCount }}'>{{ $cat['name'] }}</th>
              <?php $i = 1; ?>
              @foreach($classes as $class)
              @if($i != 1)
              <tr>
              @endif
                <th rowspan='2'>{{ $class['name'] }}</th>
                <?php $i++; $j = 1; ?>
                @foreach(array('B'=>'Boys', 'G'=>'Girls') as $genderId => $gender)
              @if($j != 1)
              <tr>
              @endif
                <th>{{ $gender }}</th>
                @foreach(Result::$RESULT_HEADS as $headId => $head)
                <td>
                  {{ Form::text("{$cat['abbr']}-$gender-{$class['id']}-$headId", Input::old("{$cat['abbr']}-$gender-{$class['id']}-$headId",
                  $resultsElementary[($headId-1)]->{"C{$class['id']}{$genderId}_{$cat['abbr']}"}),
                  array('data-parsley-type'=>'number', 'data-parsley-trigger'=>'keyup', 'class'=>"form-control $gender-{$class['id']}-$headId",
                  'id'=>"{$cat['abbr']}-$gender-{$class['id']}-$headId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                @endforeach
              </tr>
              <?php $j++; ?>
              @endforeach
              @endforeach
              @endforeach
              <tr>
                <th rowspan='{{ $categoryRowCount }}'>Total</th>
              <?php $i = 1; ?>
              @foreach($classes as $class)
              @if($i != 1)
              <tr>
              @endif
                <th rowspan='2'>{{ $class['name'] }}</th>
                <?php $i++; $j = 1; ?>
                @foreach(array('B'=>'Boys', 'G'=>'Girls') as $genderId => $gender)
              @if($j != 1)
              <tr>
              @endif
                <th>{{ $gender }}</th>
                @foreach(Result::$RESULT_HEADS as $headId => $head)
                <td class="{{ "total-$gender-{$class['id']}-$headId" }}">
                </td>
                @endforeach
              </tr>
              <?php $j++; ?>
              @endforeach
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop



