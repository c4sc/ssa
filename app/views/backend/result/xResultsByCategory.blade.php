@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.secondaryGroup', '.x-result-by-category')

  $('.passed, .appeared').keyup(function() {
    var ids = this.id.split('-')
    gender = ids[0]
    category = ids[2]
    passed = Math.abs(parseInt($('#'+gender+'-passed-'+category).val()) || 0);
    appeared = Math.abs(parseInt($('#'+gender+'-appeared-'+category).val()) || 0);
    if(passed > appeared) {
      $('#'+gender+'-passed-'+category).parent().addClass('danger')
    } else {
      $('#'+gender+'-passed-'+category).parent().removeClass('danger')
    }
  });

  $('.appeared').keyup()
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Result of class X examination for previous academic year</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <th colspan="2">Number of students appeared</th>
                  <th colspan="2">Number of students passed/qualified</th>
                </tr>
                <tr>
                  <th>Sl.No</th>
                  <th>Category</th>
                  <th>Boys</th>
                  <th>Girls</th>
                  <th>Boys</th>
                  <th>Girls</th>
                </tr>
              </thead>
              <tbody>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <tr>
                <th>{{ $categoryId }}</th>
                <th>{{ $category }}</th>
                <td>
                  {{ Form::text("boys-appeared-$categoryId",
                  Input::old("boys-appeared-$categoryId", $results[($categoryId-1)]->C10B_GEN),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'form-control appeared', 'id'=>"boys-appeared-$categoryId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                <td>
                  {{ Form::text("girls-appeared-$categoryId",
                  Input::old("girls-appeared-$categoryId", $results[($categoryId-1)]->C10G_GEN),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'form-control appeared', 'id'=>"girls-appeared-$categoryId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                <td>
                  {{ Form::text("boys-passed-$categoryId",
                  Input::old("boys-passed-$categoryId", $results[($categoryId-1)]->C10B_SC),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'form-control passed', 'id'=>"boys-passed-$categoryId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                <td>
                  {{ Form::text("girls-passed-$categoryId",
                  Input::old("girls-passed-$categoryId", $results[($categoryId-1)]->C10G_SC),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'form-control passed', 'id'=>"girls-passed-$categoryId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop


