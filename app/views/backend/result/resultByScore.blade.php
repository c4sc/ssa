@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  @if($class == 'X')
  activate('.secondaryGroup', '.result-by-score-x')
  @else
  activate('.secondaryGroup', '.result-by-score-xii')
  @endif

  $('.form-control').keyup(function() {
    var ids = this.id.split('-')
    gender = ids[0]
    category = ids[1]
    score = ids[2]
    category_sum = score_sum = 0
    $('.'+gender+'-category-'+category).each(function(){
      category_sum += (parseInt($(this).val()) || 0);
    });
    $('.'+gender+'-score-'+score).each(function(){
      score_sum += parseInt($(this).val()) || 0
    });
    $('.total-'+gender+'-score-'+score).html(score_sum)
    $('.total-'+gender+'-category-'+category).html(category_sum)
  });

  $('form tr td input').keyup()
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">
            @if($class == 'X')
            Number of students passed/qualified the Secondary School Board (Class X) examination in
            previous academic year
            @else
            Result of class XII previous year -- by percentage of marks
            @endif
          </h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Sl.No</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  @foreach(Result::$SCORE as $scoreId => $score)
                  <th>{{ $score }}</th>
                  @endforeach
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
              @foreach(Result::$CATEGORY_LIST as $categoryId => $category)
              <tr>
                <th rowspan="2">{{ $categoryId }}</th>
                <th rowspan="2">{{ $category['name'] }}</th>
                <th>Boys</th>
                @foreach(Result::$SCORE as $scoreId => $score)
                <td>
                  {{ Form::text("boys-$categoryId-$scoreId",
                  Input::old("boys-$categoryId-$scoreId", $results[($scoreId-1)]->{"C10B_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control boys-score-$scoreId boys-category-$categoryId", 'id'=>"boys-$categoryId-$scoreId",
                  'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                @endforeach
                <th class="total-boys-category-{{ $categoryId }}">&nbsp;</th>
              </tr>
              <tr>
                <th>Girls</th>
                @foreach(Result::$SCORE as $scoreId => $score)
                <td>
                  {{ Form::text("girls-$categoryId-$scoreId",
                  Input::old("girls-$categoryId-$scoreId", $results[($scoreId-1)]->{"C10G_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control girls-score-$scoreId girls-category-$categoryId",
                  'id'=>"girls-$categoryId-$scoreId",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </td>
                @endforeach
                <th class="total-girls-category-{{ $categoryId }}">&nbsp;</th>
              </tr>
              @endforeach
              <tr>
                <th rowspan="2" colspan="2">Total</th>
                <th>Boys</th>
                @foreach(Result::$SCORE as $scoreId => $score)
                <th class="total-boys-score-{{ $scoreId }}">&nbsp;</th>
                @endforeach
              </tr>
              <tr>
                <th>Girls</th>
                @foreach(Result::$SCORE as $scoreId => $score)
                <th class="total-girls-score-{{ $scoreId }}">&nbsp;</th>
                @endforeach
              </tr>
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop


