@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.secondaryGroup', '.result-by-stream')

  $('.passed, .appeared').keyup(function() {
    var ids = this.id.split('-')
    gender = ids[0]
    category = ids[2]
    stream = ids[3]
    passed = parseInt($('#'+gender+'-passed-'+category+'-'+stream).val()) || 0;
    appeared = parseInt($('#'+gender+'-appeared-'+category+'-'+stream).val()) || 0;
    if(passed > appeared) {
      $('#'+gender+'-passed-'+category+'-'+stream).parent().addClass('danger')
    } else {
      $('#'+gender+'-passed-'+category+'-'+stream).parent().removeClass('danger')
    }
  });

  $('.appeared').keyup()
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Result of class XII Board/University examination in previous academic year</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Number of students appeared</label>
            <table class="table enrolment table-bordered">
              <thead>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @foreach($streams as $key => $stream)
                <th>{{ Facility::$STREAMS[$stream] }}</th>
                @endforeach
              </thead>
              <tbody>
              @foreach(Result::$CATEGORY_LIST as $categoryId => $category)
              <tr>
                <th rowspan="2">{{ $category['name'] }}</th>
                <th>Boys</th>
                @foreach($streams as $key => $stream)
                <td>
                  {{ Form::text("boys-appeared-$categoryId-$stream",
                  Input::old("boys-appeared-$categoryId-$stream", $results[$key]->{"C10B_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'appeared form-control', 'id'=>"boys-appeared-$categoryId-$stream",'maxlength'=>'3')) }}
                </td>
                @endforeach
              </tr>
              <tr>
                <th>Girls</th>
                @foreach($streams as $key => $stream)
                <td>
                  {{ Form::text("girls-appeared-$categoryId-$stream",
                  Input::old("girls-appeared-$categoryId-$stream", $results[$key]->{"C10G_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'appeared form-control', 'id'=>"girls-appeared-$categoryId-$stream",'maxlength'=>'3')) }}
                </td>
                @endforeach
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>
          <div class="form-group">
            <label class="control-label">Number of students passed</label>
            <table class="table enrolment table-bordered">
              <thead>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                @foreach($streams as $key => $stream)
                <th>{{ Facility::$STREAMS[$stream] }}</th>
                @endforeach
              </thead>
              <tbody>
              @foreach(Result::$CATEGORY_LIST as $categoryId => $category)
              <tr>
                <th rowspan="2">{{ $category['name'] }}</th>
                <th>Boys</th>
                @foreach($streams as $key => $stream)
                <td>
                  {{ Form::text("boys-passed-$categoryId-$stream",
                  Input::old("boys-passed-$categoryId-$stream", $results[$key]->{"C12B_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'passed form-control', 'id'=>"boys-passed-$categoryId-$stream",'maxlength'=>'3')) }}
                </td>
                @endforeach
              </tr>
              <tr>
                <th>Girls</th>
                @foreach($streams as $key => $stream)
                <td>
                  {{ Form::text("girls-passed-$categoryId-$stream",
                  Input::old("girls-passed-$categoryId-$stream", $results[$key]->{"C12G_{$category['abbr']}"}),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'passed form-control', 'id'=>"girls-passed-$categoryId-$stream",'maxlength'=>'3')) }}
                </td>
                @endforeach
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop



