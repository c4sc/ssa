@extends('backend.layouts.udise')

@section('title')
District Users
@stop

@section('path')
@stop

@section('main-page')
DistrictUsers
@stop

@section('content')
@include('backend.deleteModalBox')
<div class="box box-primary">
  <div class="box-body">
    <table class="table">
      <tbody>
      <tr>
        <th> Name </th>
        <td> {{{ $user->fullName() }}} </td>
      </tr>
      <tr>
        <th> Email </th>
        <td> {{{ $user->email }}} </td>
      </tr>
      <tr>
        <th> Scope </th>
        <td> {{{ AuthorizedController::$SCOPE[$user->scope] }}} </td>
      </tr>
      <tr>
        <th> User Group</th>

        <td>
          @foreach($user->getGroups() as $group)
          {{{ $group->name }}}
          @endforeach
        </td>
      </tr>
      <tr>
        <th> District</th>
        <td> {{{ $district->DISTNAME }}} </td>
      </tr>
      </tbody>
    </table>
  </div>
  <div class="box-body">
    <a class="btn btn-warning" href="{{{ route('district-user-edit', $user->id) }}}"> Edit </a>
    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUserModal">Delete</a>
  </div>
</div>
@stop



