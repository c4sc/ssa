@extends('backend.layouts.user')

@section('title')
  @if (@$user->id)
    Edit District User
  @else
    New District User
  @endif
@stop

@section('path')
<li><a href="{{ route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('main-page')
DistrictUsers
@stop

@section('script')
$(function(){
  activate('.userGroup', '.district-user-create');
});
@stop

@section('content')
<div class="box box-primary">
  <form class="form-horizontal" method="post" action="">
    <fieldset>
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />

      <div class="box-body">
        <!-- Text input-->
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="username">Email</label>
          <div class="col-md-4">
            <input id="email" name="email" placeholder="Email" class="form-control input-md" required=""
            type="text" value="{{{ Input::old('email', $user->email) }}}" autocomplete="off">
            {{ $errors->first('email', '<label for="email" class="control-label"><i class="fa fa-times-circle-o"></i>
              :message </label>') }}

          </div>
        </div>

        @if ( ! $user->id)
        <!-- Password input-->
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="password">Password</label>
          <div class="col-md-4">
            <input id="password" name="password" placeholder="Password" class="form-control input-md"
            type="password" autocomplete="off">
            {{ $errors->first('password', '<label for="password" class="control-label"><i class="fa fa-times-circle-o"></i>
              :message </label>') }}
          </div>
        </div>
        @endif

        <!-- Select Basic -->
        <div class="form-group {{ $errors->has('user-group') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="user-group">User group</label>
          <div class="col-md-4">
            {{ Form::select('user-group', $groupList, Input::old('user-group', @$group->id), array('class'=>'form-control')) }}
            {{ $errors->first('user-group', '<label for="user-group" class="control-label"><i class="fa fa-times-circle-o"></i>
              :message </label>') }}
          </div>
        </div>

        <!-- Text input-->
        <div class="form-group {{ $errors->has('district-code') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="district-code">District Code</label>
          <div class="col-md-4">
            <input id="district-code" name="district-code" placeholder="District Code" class="form-control input-md"
            required="" type="text" value="{{{ Input::old('district-code', $user->scope_id) }}}">
            {{ $errors->first('district-code', '<label for="district-code" class="control-label"><i class="fa fa-times-circle-o"></i>
              :message </label>') }}
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="create-user"></label>
          <div class="col-md-4">
            <button id="create-user" name="create-user" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>

    </fieldset>
  </form>
</div>
@stop

