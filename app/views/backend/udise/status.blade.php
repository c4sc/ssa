@extends('backend.layouts.udise')

@section('title')
Status of Forms
@stop

@section('script')
$(function() {
  activate('.schoolGroup', '.udise-status')
});
@stop

@section('content')

@foreach ($errorList as $error)
<div class="alert alert-danger">
  <i class="fa fa-times"></i>
  <b>Error!</b>
  {{ $error->message }}
</div>
@endforeach
@foreach ($warnings as $warning)
<div class="alert alert-warning">
  <i class="fa fa-warning"></i>
  <b>Warning!</b>
  {{ $warning->message }}
</div>
@endforeach

@if ($canConfirm)
@if ($errors->confirmation->any())

@if ($errors->confirmation->has('zero-filled-in-preprimary'))
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Error!.</b> preprimary students count can't be zero.
  <a href="{{ route('enrolment-by-category', $master->SCHCD) }}">Enrolment by social category</a> form.
</div>
@endif
@if ($errors->confirmation->has('not-equal'))
@foreach ($errors->confirmation->get('not-equal') as $message)
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <b>Error!.</b> {{ $message }}.
</div>
@endforeach
@endif

@endif {{-- confirmation-errors --}}
<a class="btn btn-danger" data-toggle="modal" data-target="#confirmUdise"> Confirm Udise</a>
<div class="modal fade" id="confirmUdise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Confirm UDISE</h4>
      </div>
      <div class="modal-body">
        <p>After confriming UDISE data entry you can not update any data. <br>Are you sure to confirm ?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('udise-confirm', $master->SCHCD) }}" method="post">
          <button class="btn btn-warning">Confirm</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
@elseif (Sentry::getUser()->hasAccess('blockAdmin') && $master->school->confirmation())
<a class="btn btn-warning" data-toggle="modal" data-target="#resetUdise"> Reset Udise</a>
<div class="modal fade" id="resetUdise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title">Reset UDISE</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to reset?</p>
      </div>
      <div class="modal-footer">
        <form method="post" action="{{ route('udise-reset', $master->SCHCD) }}">
          <button class='btn btn-danger'>Reset</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif {{-- canConfirm --}}
@if ($master->school->confirmation())
<a class="btn btn-info" href="{{ route('pdf-1-download', $master->SCHCD) }}">Pdf 1</a>
<a class="btn btn-info" href="{{ route('pdf-2-download', $master->SCHCD) }}">Pdf 2</a>
@endif
<table class="table">
  <tbody>
  <?php
  $group = '';
  ?>

  @foreach ($master->getUdiseRoutesForMenu() as $route=>$link)
  @if ($group != $link['group'])
  <?php
  $group = $link['group'];
  ?>
  <tr>
    <th>
      {{ School::$UDISE_ROUTE_GROUPS[$group] }}
    </th>
  </tr>
  @endif
  <tr class="{{ ($r = findInCollection($statusOfUdiseRoutes, 'route', $route))?$r->status:'' }}">
    <td><a href="{{ route($route, $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> {{ $link['title'] }}</a></td>
  </tr>
  @endforeach
  </tbody>
</table>
@stop
