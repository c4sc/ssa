@extends('backend.layouts.udise')

@section('title')
Particulars Two
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-two')

  // pre-primary-section
  $("select[name='pre-primary-section']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-pre-primary-section" ).show('slow')
      $( ".depends-pre-primary-section select, .depends-pre-primary-section input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-pre-primary-section" ).hide('slow')
      $( ".depends-pre-primary-section input, .depends-pre-primary-section select" ).val('');
      $( ".depends-pre-primary-section select, .depends-pre-primary-section input" ).attr('data-parsley-required', 'false');
    }
  });

  // anganwadi-section
  $("select[name='anganwadi-section']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-anganwadi-section" ).show('slow');
      $( ".depends-anganwadi-section select, .depends-anganwadi-section input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-anganwadi-section" ).hide('slow');
      $( ".depends-anganwadi-section input, .depends-anganwadi-section select" ).val('');
      $( ".depends-anganwadi-section select, .depends-anganwadi-section input" ).attr('data-parsley-required', 'false');
    }
  });
  $("select[name='anganwadi-section']").change();
  $("select[name='pre-primary-section']").change();
});
@stop


@section('content')

<form role="form" method="post" action="" data-parsley-validate  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />

  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Last academic year details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label class=" control-label" >Number of   academic inspections</label>

            <div class="input-group {{ $errors->has('academic-inspections') ? ' has-error' : '' }}">
              <span class="input-group-addon">Number of  academic inspections by AEO  and  Others </span>
              {{ Form::text('academic-inspections', Input::old('academic-inspections', $schoolMaster->NOINSPECT),
              array('data-parsley-type'=>"number",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>"keyup",'class'=>'form-control', 'placeholder'=>'Total Number of  academic inspections')) }}
              {{ $errors->first('academic-inspections', '<label for="academic-inspections" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('visits-by-crc') ? ' has-error' : '' }}">
              <span class="input-group-addon">Number of visits by CRC coordinators </span>
              {{ Form::text('visits-by-crc', Input::old('visits-by-crc', $schoolMaster->VISITSCRC),
              array('data-parsley-type'=>"number",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>"keyup",'class'=>'form-control', 'placeholder'=>'Total Number of visits by CRC coordinators')) }}
              {{ $errors->first('visits-by-crc', '<label for="visits-by-crc" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('visits-by-brc') ? ' has-error' : '' }}">
              <span class="input-group-addon">Number of visits by Block level officer </span>
              {{ Form::text('visits-by-brc', Input::old('visits-by-brc', $schoolMaster->VISITSBRC),
              array('data-parsley-type'=>"number",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>"keyup",'class'=>'form-control', 'placeholder'=>'Number of visits by Block level officer')) }}
              {{ $errors->first('visits-by-brc', '<label for="visits-by-brc" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          @if($schoolMaster->hasElementary())
          <label class="control-label" >School funds excluding MDM (for elementary schools/sections)</label>
          <div class="form-group">
            <label class="control-label" >School Development Grant (under SSA)</label>
            <div class="input-group
              {{ $errors->has('school-grant-receipt') ? ' has-error' : '' }}
              {{ $errors->has('school-grant-expenditure') ? ' has-error' : '' }}">
              <span class="input-group-addon">Receipt</span>
              {{ Form::text('school-grant-receipt', Input::old('school-grant-receipt', $schoolMaster->CONTI_R),
              array('data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control', 'id' => 'school-grant-receipt')) }}
              <span class="input-group-addon">Expenditure</span>
              {{ Form::text('school-grant-expenditure', Input::old('school-grant-expenditure', $schoolMaster->CONTI_E),
              array('data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control', 'data-parsley-lte' => '#school-grant-receipt')) }}

              {{ $errors->first('school-grant-receipt', '<label for="school-grant-receipt" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('school-grant-expenditure', '<label for="school-grant-expenditure" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @if ( $master->isGov() )
              <label class="control-label" >School Maintenance Grant (Under SSA)</label>
              <div class="input-group
                {{ $errors->has('maintenance-grant-receipt') ? ' has-error' : '' }}
                {{ $errors->has('maintenance-grant-expenditure') ? ' has-error' : '' }}">
                <span class="input-group-addon">Receipt</span>
                {{ Form::text('maintenance-grant-receipt', Input::old('maintenance-grant-receipt', $schoolMaster->SCHMNTCGRANT_R), array('data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control', 'id' => 'maintenance-grant-receipt')) }}
                <span class="input-group-addon">Expenditure</span>
                {{ Form::text('maintenance-grant-expenditure', Input::old('maintenance-grant-expenditure', $schoolMaster->SCHMNTCGRANT_E),  array('data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control', 'data-parsley-lte' => '#maintenance-grant-receipt')) }}

                {{ $errors->first('maintenance-grant-receipt', '<label for="maintenance-grant-receipt" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                {{ $errors->first('maintenance-grant-expenditure', '<label for="maintenance-grant-expenditure" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            @endif
            <label class="control-label" >TLM/Teachers Grant (Under SSA)</label>
            <div class="input-group
              {{ $errors->has('teachers-grant-receipt') ? ' has-error' : '' }}
              {{ $errors->has('teachers-grant-expenditure') ? ' has-error' : '' }}">
              <span class="input-group-addon">Receipt</span>
              {{ Form::text('teachers-grant-receipt', Input::old('teachers-grant-receipt', $schoolMaster->TLM_R), array('id' => 'teachers-grant-receipt', 'data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control')) }}
              <span class="input-group-addon">Expenditure</span>
              {{ Form::text('teachers-grant-expenditure', Input::old('teachers-grant-expenditure', $schoolMaster->TLM_E),
              array('data-parsley-type'=>'number','data-parsley-required'=>"true",'class'=>'form-control', 'data-parsley-lte' => '#teachers-grant-receipt')) }}

              {{ $errors->first('teachers-grant-receipt', '<label for="teachers-grant-receipt" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('teachers-grant-expenditure', '<label for="teachers-grant-expenditure" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          @endif
        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->

    @if($schoolMaster->LOWCLASS == 1)
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> only for schools having Pre-primary sections and Anganwadi</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label>
              Pre-primary section (other than Anganwadi) attached to school
            </label>
            {{ Form::select('pre-primary-section', School::$CHOICE, Input::old('pre-primary-section',
            $schoolMaster->PPSEC_YN), array('class'=>'form-control', 'data-parsley-required'=>'required'))}}
          </div>
          <div class="form-group depends-pre-primary-section">
            <label class=" control-label" for="">If yes </label>
            <div class="input-group {{ $errors->has('pre-primary-students') ? ' has-error' : '' }}">
              <span class="input-group-addon">Total students in pre-primary sections</span>
              {{ Form::text('pre-primary-students', Input::old('pre-primary-students', $schoolMaster->PPSTUDENT),
              array('data-parsley-type'=>'digits','data-parsley-min'=>'1','class'=>'form-control', 'placeholder'=>'total students in pre-primary sections ')) }}
              {{ $errors->first('pre-primary-students', '<label for="pre-primary-students" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('pre-primary-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Total No of  pre primary teachers</span>
              {{ Form::text('pre-primary-teachers', Input::old('pre-primary-teachers', $schoolMaster->PPTEACHER),
              array('data-parsley-type'=>'digits','data-parsley-min'=>'1','class'=>'form-control', 'placeholder'=>'total pre primary teachers')) }}
              {{ $errors->first('pre-primary-teachers', '<label for="pre-primary-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group">
            <label>
              Anganwadi Centre in or adjacent to school
            </label>
            {{ Form::select('anganwadi-section', School::$CHOICE, Input::old('anganwadi-section',
            $schoolMaster->ANGANWADI_YN), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
          </div>
          <div class="form-group depends-anganwadi-section">
            <label class=" control-label">If yes </label>
            <div class="input-group {{ $errors->has('anganwadi-students') ? ' has-error' : '' }}">
              <span class="input-group-addon">a) Total students in anganwadi sections</span>
              {{ Form::text('anganwadi-students', Input::old('anganwadi-students', $schoolMaster->ANGANWADI_STU),
              array('data-parsley-type'=>'digits','data-parsley-min'=>'1','class'=>'form-control', 'placeholder'=>'total students in anganwadi sections ')) }}
              {{ $errors->first('anganwadi-students', '<label for="anganwadi-students" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('anganwadi-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Total No of  anganwadi teachers</span>
              {{ Form::text('anganwadi-teachers', Input::old('anganwadi-teachers', $schoolMaster->ANGANWADI_TCH),
              array('data-parsley-type'=>'digits','data-parsley-min'=>'1','class'=>'form-control', 'placeholder'=>'total anganwadi teachers')) }}
              {{ $errors->first('anganwadi-teachers', '<label for="anganwadi-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->
    @endif
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Staff category</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Section</th>
                  <th>Sanctioned Post</th>
                  <th>In Position</th>
                </tr>
              </thead>
              <tbody>
              @if($schoolMaster->hasPrimary())
              <tr>
                <th>Primary</th>
                <td>
                  <input type="text" class="form-control" value="{{{ Input::old('primary-teacher-sanctioned',
                  $schoolMaster->TCHSANCT_PR) }}}" name="primary-teacher-sanctioned" id="primary-teacher-sanctioned"
                  data-parsley-required="true" data-parsley-type="digits">
                  {{ $errors->first('primary-teacher-sanctioned', '<label for="primary-teacher-sanctioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}


                </td>
                <td class="{{ $errors->first('primary-teacher-inpositioned', 'has-error') }}">
                  <input type="text" class="form-control" value="{{{ Input::old('primary-teacher-inpositioned',
                  $schoolMaster->TCHPOS) }}}" name="primary-teacher-inpositioned" id="primary-teacher-inpositioned"
                  data-parsley-required="true" data-parsley-type="digits" data-parsley-lte="#primary-teacher-sanctioned">
                  {{ $errors->first('primary-teacher-inpositioned', '<label for="primary-teacher-inpositioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

                </td>
              </tr>
              @endif
              @if($schoolMaster->hasUpperPrimary())
              <tr>
                <th>Upper Primary</th>
                <td>
                  <input type="text" class="form-control" value="{{{ Input::old('up-teacher-sanctioned',
                  $schoolMaster->TCHSAN) }}}" name="up-teacher-sanctioned" id="up-teacher-sanctioned"
                  data-parsley-required="true" data-parsley-type="digits">
                  {{ $errors->first('up-teacher-sanctioned', '<label for="up-teacher-sanctioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

                </td>
                <td class="{{ $errors->first('up-teacher-inpositioned', 'has-error') }}">
                  <input type="text" class="form-control" value="{{{ Input::old('up-teacher-inpositioned',
                  $schoolMaster->TCHREGU_UPR) }}}" name="up-teacher-inpositioned" id="up-teacher-inpositioned"
                  data-parsley-required="true" data-parsley-type="digits" data-parsley-lte="#up-teacher-sanctioned">
                  {{ $errors->first('up-teacher-inpositioned', '<label for="up-teacher-inpositioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
              </tr>
              @endif
              @if($schoolMaster->hasSecondary())
              <tr>
                <th>Secondary</th>
                <td>
                  <input type="text" class="form-control" value="{{{ Input::old('secondary-teacher-sanctioned',
                  $schoolMaster->TCHSANCT_SEC) }}}" name="secondary-teacher-sanctioned"
                  id="secondary-teacher-sanctioned" data-parsley-required="true" data-parsley-type="digits">
                  {{ $errors->first('secondary-teacher-sanctioned', '<label for="secondary-teacher-sanctioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

                </td>
                <td class="{{ $errors->first('secondary-teacher-inpositioned', 'has-error') }}">
                  <input type="text" class="form-control" value="{{{ Input::old('secondary-teacher-inpositioned',
                  $schoolMaster->TCHPOSN_SEC) }}}" name="secondary-teacher-inpositioned" id="secondary-teacher-inpositioned"
                  data-parsley-required="true"  data-parsley-type="digits" data-parsley-lte="#secondary-teacher-sanctioned">
                  {{ $errors->first('secondary-teacher-inpositioned', '<label for="secondary-teacher-inpositioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
              </tr>
              @endif
              @if($schoolMaster->hasHigherSecondary())
              <tr>
                <th>Higher Secondary</th>
                <td>
                  <input type="text" class="form-control" value="{{{ Input::old('hs-teacher-sanctioned',
                  $schoolMaster->TCHSANCT_HSEC) }}}" name="hs-teacher-sanctioned" id="hs-teacher-sanctioned"
                  data-parsley-required="true" data-parsley-type="digits">
                  {{ $errors->first('hs-teacher-sanctioned', '<label for="hs-teacher-sanctioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

                </td>
                <td class="{{ $errors->first('hs-teacher-inpositioned', 'has-error') }}">
                  <input type="text" class="form-control" value="{{{ Input::old('hs-teacher-inpositioned',
                  $schoolMaster->TCHPOSN_HSEC) }}}" name="hs-teacher-inpositioned" id="hs-teacher-inpositioned"
                  data-parsley-required="true"  data-parsley-type="digits" data-parsley-lte="#hs-teacher-sanctioned">
                  {{ $errors->first('hs-teacher-inpositioned', '<label for="hs-teacher-inpositioned" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

                </td>
              </tr>
              @endif
              </tbody>
            </table>
          </div>

          <div class="form-group">
            <label class=" control-label" for="class">Contract Teachers </label>

            @if($schoolMaster->hasPrimary())
            <div class="input-group {{ $errors->has('primary-contract-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary </span>
              <input type="text" class="form-control" value="{{{ Input::old('primary-contract-teachers',
              $schoolMaster->PARAPOS) }}}" name="primary-contract-teachers" id="primary-contract-teachers"
              placeholder="No of Primary Contract Teachers" data-parsley-required="true" data-parsley-type="digits" >
              {{ $errors->first('primary-contract-teachers', '<label for="primary-contract-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif

            @if($schoolMaster->hasUpperPrimary())
            <div class="input-group {{ $errors->has('up-contract-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Upper Primary </span>
              <input type="text" class="form-control" value="{{{ Input::old('up-contract-teachers',
              $schoolMaster->TCHPARA_UPR) }}}" name="up-contract-teachers" id="up-contract-teachers" placeholder="No of
              Upper Primary Contract Teachers" data-parsley-required="true" data-parsley-type="digits">
              {{ $errors->first('up-contract-teachers', '<label for="up-contract-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif

            @if($schoolMaster->hasSecondary())
            <div class="input-group {{ $errors->has('secondary-contract-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Secondary </span>
              <input type="text" class="form-control" value="{{{ Input::old('secondary-contract-teachers',
              $schoolMaster->PARAPOSN_SEC) }}}" name="secondary-contract-teachers" id="secondary-contract-teachers"
              placeholder="No of Secondary Contract Teachers" data-parsley-required="true" data-parsley-type="digits" >
              {{ $errors->first('secondary-contract-teachers', '<label for="secondary-contract-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif

            @if($schoolMaster->hasHigherSecondary())
            <div class="input-group {{ $errors->has('hs-contract-teachers') ? ' has-error' : '' }}">
              <span class="input-group-addon">Higher Secondary </span>
              <input type="text" class="form-control" value="{{{ Input::old('hs-contract-teachers',
              $schoolMaster->PARAPOSN_HSEC) }}}" name="hs-contract-teachers" id="hs-contract-teachers" placeholder="No
              of Higher Secondary Contract Teachers" data-parsley-required="true" data-parsley-type="digits">
              {{ $errors->first('hs-contract-teachers', '<label for="hs-contract-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif
          </div>
          @if($schoolMaster->hasUpperPrimary())
          <div class="form-group">
            <label class=" control-label" >Part-time instructor positioned    as per RTE</label>
            <div class="input-group {{ $errors->has('part-time-teachers') ? ' has-error' : '' }}">
              <input type="text" class="form-control" value="{{{ Input::old('part-time-teachers',
              $schoolMaster->TCHPART_UPR) }}}" name="part-time-teachers" id="part-time-teachers" placeholder="No of .
              Part-time instructor positioned as per RTE" data-parsley-required="true" data-parsley-type="digits" >
              {{ $errors->first('part-time-teachers', '<label for="part-time-teachers" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          @endif
        </div>
      </div>
    </div> <!-- col -->
  </div><!-- row -->

  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>

</form>
@stop
