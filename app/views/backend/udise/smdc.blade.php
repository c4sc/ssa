@extends('backend.layouts.udise')

@section('title')
SMDC
@stop

@section('script')
$(function() {

  activate('.particularsGroup', '.particulars-four')

//SMDC DETAILS
  $("select[name='smdc-constituted']").on('change', function(event){
    if($(this).val() == 2) {
      $(".depends-smdc-constituted").show('slow');
      $('.depends-smdc-constituted select, .depends-smdc-constituted input').attr('data-parsley-required', 'true');
    } else {
      $('.depends-smdc-constituted select, .depends-smdc-constituted input').attr('data-parsley-required', 'false');
      $(".depends-smdc-constituted").hide('slow');
      $(".depends-smdc-constituted select, .depends-smdc-constituted input").val('');
    }
  });
  $("select[name='smdc-constituted']").change();

//ONLY SMDC
  $("select[name='only-smdc-constituted']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-only-smdc-constituted" ).show('slow');
      $( ".depends-only-smdc-constituted select, .depends-only-smdc-constituted input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-only-smdc-constituted select, .depends-only-smdc-constituted input" ).attr('data-parsley-required', 'false');
      $( ".depends-only-smdc-constituted" ).hide('slow');
      $( ".depends-only-smdc-constituted input" ).val('');
    }
  });
  $("select[name='only-smdc-constituted']").change();


//SMDC BANK ACCOUNT
  $("select[name='smdc-bank-account']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-smdc-bank-account" ).show('slow');
      $( ".depends-smdc-bank-account select, .depends-smdc-bank-account input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-smdc-bank-account" ).hide('slow');
      $( ".depends-smdc-bank-account select, .depends-smdc-bank-account input" ).val('');
      $( ".depends-smdc-bank-account select, .depends-smdc-bank-account input" ).attr('data-parsley-required', 'false');
    }
  });
  $("select[name='smdc-bank-account']").change();

//PTA COMMITTEE
  $("select[name='pta-constituted']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-pta-constituted" ).show('slow');
      $( ".depends-pta-constituted select, .depends-pta-constituted input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-pta-constituted" ).hide('slow');
      $( ".depends-pta-constituted input" ).val('');
      $( ".depends-pta-constituted select, .depends-pta-constituted input" ).attr('data-parsley-required', 'false');
    }
  });
  $("select[name='pta-constituted']").change();

});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="box box-primary box-solid">
      <div class="box-header">
        <h3 class="box-title ">Only for Secondary and Hr. Secondary stage</h3>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Details of instructional days and school hours</h3>
        </div>
        <div class="box-body">

          <div class="form-group {{ $errors->has('instructional-days') ? ' has-error' : '' }}">
            <label class=" control-label" for="instructional-days">Number of instructional days (previous academic year)</label>
            <div class="input-group {{ $errors->has('instructional-days') ? ' has-error' : '' }}">
              @if($master->hasSecondary())
              <span class="input-group-addon">Secondary</span>
              <input id="secondary-instructional-days" name="secondary-instructional-days"
              placeholder="secondary-instructional-days" data-parsley-type="number"
              data-parsley-range='[100,250]' data-parsley-required="true" value="{{{ Input::old('secondary-instructional-days', $smdc->WORKDAYS_SEC) }}}" class="form-control input-md" type="text">
              {{ $errors->first('secondary-instructional-days', '<label for="secondary-instructional-days" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasHigherSecondary())
              <span class="input-group-addon">Higher Secondary</span>
              <input id="hs-instructional-days" name="hs-instructional-days" placeholder="hs-instructional-days" data-parsley-type="number"
              data-parsley-range='[100,250]' data-parsley-required="true" value="{{{ Input::old('hs-instructional-day', $smdc->WORKDAYS_HSEC) }}}" class="form-control input-md" type="text">
              {{ $errors->first('hs-instructional-days', '<label for="hs-instructional-days" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class=" control-label" for="school-hours">School hours for children (per day)</label>
            <div class="input-group {{ $errors->has('school-hours') ? ' has-error' : '' }}">

              @if($master->hasSecondary())
              <span class="input-group-addon">Secondary</span>
              {{ Form::text('secondary-school-hours', Input::old('secondary-school-hours', $smdc->SCHHRSCHILD_SEC),
              array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-range'=>'[5,8]', 'data-parsley-required'=>"true", 'class'=>'form-control', 'placeholder'=>'secondary-school-hours')) }}
              {{ $errors->first('secondary-school-hours', '<label for="secondary-school-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasHigherSecondary())
              <span class="input-group-addon">Higher Secondary</span>
              {{ Form::text('hs-school-hours', Input::old('hs-school-hours', $smdc->SCHHRSCHILD_HSEC), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-range'=>'[5,8]', 'data-parsley-required'=>"true", 'class'=>'form-control', 'placeholder'=>'hs-school-hours')) }}
              {{ $errors->first('hs-school-hours', '<label for="hs-school-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class=" control-label" for="teacher-working-hours">Teacher working hours (per day)</label>
            <div class="input-group {{ $errors->has('teacher-working-hours') ? ' has-error' : '' }}">

              @if($master->hasSecondary())
              <span class="input-group-addon">Secondary</span>
              {{ Form::text('secondary-teacher-working-hours', Input::old('secondary-teacher-working-hours', $smdc->SCHHRSTCH_SEC), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-range'=>'[5,8]', 'data-parsley-required'=>"true", 'class'=>'form-control', 'placeholder'=>'secondary-teacher-working-hours')) }}
              {{ $errors->first('secondary-teacher-working-hours', '<label for="secondary-teacher-working-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasHigherSecondary())
              <span class="input-group-addon">Higher Secondary</span>
              {{ Form::text('hs-teacher-working-hours', Input::old('hs-teacher-working-hours', $smdc->SCHHRSTCH_HSEC), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-range'=>'[5,8]', 'data-parsley-required'=>"true", 'class'=>'form-control', 'placeholder'=>'hs-teacher-working-hours')) }}
              {{ $errors->first('hs-teacher-working-hours', '<label for="hs-teacher-working-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
            </div>
          </div>
          <div class="form-group">
            <label> Is CCE being implemented?</label>
            @if($master->hasSecondary())
            <div class="input-group {{ $errors->has('cce-implemented-secondary') ? ' has-error' : '' }}">
              <label>
                for Secondary sections
              </label>
              {{ Form::select('cce-implemented-secondary', School::$CHOICE, Input::old('cce-implemented-secondary',
              $smdc->CCESEC_YN), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
              {{ $errors->first('cce-implemented-secondary', '<label for="cce-implemented-secondary" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif
            @if($master->hasHigherSecondary())
            <div class="input-group {{ $errors->has('cce-implemented-hs') ? ' has-error' : '' }}">
              <label>
                for Higher Secondary sections
              </label>
              {{ Form::select('cce-implemented-hs', School::$CHOICE, Input::old('cce-implemented-hs',
              $smdc->CCEHSEC_YN), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
              {{ $errors->first('cce-implemented-hs', '<label for="cce-implemented-hs" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            @endif
          </div>

        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->

    @if($master->hasSecondary() || $master->hasHigherSecondary())
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">SMDC details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label>
              Whether School Management Committee (SMC) and School Management and Development Committee (SMDC) are same in the school?
            </label>
            {{ Form::select('smdc-constituted', School::$CHOICE, Input::old('smdc-constituted', $smdc->SMDC_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'required'))}}
          </div>


          <div class="depends-smdc-constituted">
            <div class="form-group">
              <label class=" control-label" for="SMDC-details'">If ‘No’, give the following details about the composition of the SMDC; </label>
              <label>
                Whether School Management and Development Committee has been constituted
              </label>
              {{ Form::select('only-smdc-constituted', School::$CHOICE, Input::old('only-smdc-constituted',
              $smdc->SMCSMDC1_YN), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
            </div>

            <div class="depends-only-smdc-constituted">
              <div class="form-group">
                <label class=" control-label" for="no-of-smdc-members'">Total number of Members in SMDC</label>
                <div class="input-group {{ $errors->has('no-of-smdc-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-smdc-members-male', Input::old('no-of-smdc-members-male', $smdc->TOT_M),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','maxlength'=>'3', 'class'=>'form-control', 'placeholder'=>'no-of-smdc-members-male')) }}
                  {{ $errors->first('no-of-smdc-members-male', '<label for="no-of-smdc-members-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-smdc-members-female', Input::old('no-of-smdc-members-female', $smdc->TOT_F),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control',
                  'placeholder'=>'no-of-smdc-members-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-smdc-members-female', '<label for="no-of-smdc-members-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">

                <label class=" control-label" for="no-of-pta-members'">Representatives of Parents/Guardians/PTA</label>
                <div class="input-group {{ $errors->has('no-of-pta-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-pta-members-male', Input::old('no-of-pta-members-male', $smdc->PARENTS_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-pta-members-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-pta-members-male', '<label for="no-of-pta-members-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-pta-members-female', Input::old('no-of-pta-members-female', $smdc->PARENTS_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-pta-members-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-pta-members-female', '<label for="no-of-pta-members-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-local-authority'">Representatives/nominees from local authority/local government/urban local body</label>
                <div class="input-group {{ $errors->has('no-of-local-authority') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-local-authority-male', Input::old('no-of-local-authority-male', $smdc->LOCAL_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-local-authority-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-local-authority-male', '<label for="no-of-local-authority-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-local-authority-female', Input::old('no-of-local-authority-female', $smdc->LOCAL_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-local-authority-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-local-authority-female', '<label for="no-of-local-authority-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-minority-members'">Member from Educationally Backward Minority Community</label>
                <div class="input-group {{ $errors->has('no-of-minority-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-minority-male', Input::old('no-of-minority-male', $smdc->EBMC_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-minority-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-minority-male', '<label for="no-of-minority-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-minority-female', Input::old('no-of-minority-female', $smdc->EBMC_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-minority-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-minority-female', '<label for="no-of-minority-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-women-group-members'">Member from any Women Group</label>
                <div class="input-group {{ $errors->has('no-of-women-group-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-women-group', Input::old('no-of-women-group', $smdc->WOMEN_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-women-group-members','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-women-group', '<label for="no-of-women-group" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-sc-or-st-members'">Member from SC/ST community</label>
                <div class="input-group {{ $errors->has('no-of-sc-or-st-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-sc-or-st-male', Input::old('no-of-sc-or-st-male', $smdc->SCST_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-sc-or-st-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-sc-or-st-male', '<label for="no-of-sc-or-st-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-sc-or-st-female', Input::old('no-of-sc-or-st-female', $smdc->SCST_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-sc-or-st-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-sc-or-st-female', '<label for="no-of-sc-or-st-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-deo-nominee-members'">Nominee of the District Education Officer (DEO)</label>
                <div class="input-group {{ $errors->has('no-of-deo-nominee-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-deo-nominee-male', Input::old('no-of-deo-nominee-male', $smdc->DEO_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-deo-nominee-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-deo-nominee-male', '<label for="no-of-deo-nominee-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-deo-nominee-female', Input::old('no-of-deo-nominee-female', $smdc->DEO_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-deo-nominee-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-deo-nominee-female', '<label for="no-of-deo-nominee-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-nominee-audit-dept-members'">Member from Audit and Accounts Department</label>
                <div class="input-group {{ $errors->has('no-of-nominee-audit-dept-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-nominee-audit-dept-male', Input::old('no-of-nominee-audit-dept-male', $smdc->AAD_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-nominee-audit-dept-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-nominee-audit-dept-male', '<label for="no-of-nominee-audit-dept-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-nominee-audit-dept-female', Input::old('no-of-nominee-audit-dept-female', $smdc->AAD_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-nominee-audit-dept-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-nominee-audit-dept-female', '<label for="no-of-nominee-audit-dept-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-rmsa-nominee-members'">Subject experts (one each from Science, Humanities and arts/Crafts/Culture) nominated by District Programme Coordinator (RMSA)y</label>
                <div class="input-group {{ $errors->has('no-of-rmsa-nominee-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-rmsa-nominee-male', Input::old('no-of-rmsa-nominee-male-male', $smdc->SUBEXP_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-rmsa-nominee-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-rmsa-nominee-male', '<label for="no-of-rmsa-nominee-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-rmsa-nominee-female', Input::old('no-of-rmsa-nominee-female', $smdc->SUBEXP_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-rmsa-nominee-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-rmsa-nominee-female', '<label for="no-of-rmsa-nominee-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-teachers-members'">Teachers (one each from Social Science, Science and Mathematics) of the school</label>
                <div class="input-group {{ $errors->has('no-of-teachers-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-teachers-male', Input::old('no-of-teachers-male-male', $smdc->TCH_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-teachers-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-teachers-male', '<label for="no-of-teachers-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-teachers-female', Input::old('no-of-teachers-female', $smdc->TCH_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-teachers-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-teachers-female', '<label for="no-of-teachers-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-vice-principal-members'">Vice-Principal/Asst. Headmaster, as member</label>
                <div class="input-group {{ $errors->has('no-of-vice-principal-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-vice-principal-male', Input::old('no-of-vice-principal-male', $smdc->AHM_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-vice-principal-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-vice-principal-male', '<label for="no-of-vice-principal-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-vice-principal-female', Input::old('no-of-vice-principal-female', $smdc->AHM_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-vice-principal-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-vice-principal-female', '<label for="no-of-vice-principal-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-principal-members'">Principal/Headmaster, as Chairperson</label>
                <div class="input-group {{ $errors->has('no-of-principal-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-principal-male', Input::old('no-of-principal-male', $smdc->HM_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-principal-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-principal-male', '<label for="no-of-principal-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-principal-female', Input::old('no-of-principal-female', $smdc->HM_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-principal-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-principal-female', '<label for="no-of-principal-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label class=" control-label" for="no-of-chairpersons-members'">Chairperson (If Principal/Headmaster is not the Chairperson)</label>
                <div class="input-group {{ $errors->has('no-of-chairpersons-members') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Male</span>
                  {{ Form::text('no-of-chairpersons-male', Input::old('no-of-chairpersons-male', $smdc->CP_M), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-chairpersons-male','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-chairpersons-male', '<label for="no-of-chairpersons-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  <span class="input-group-addon">Female</span>
                  {{ Form::text('no-of-chairpersons-female', Input::old('no-of-chairpersons-female', $smdc->CP_F), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'no-of-chairpersons-female','maxlength'=>'3')) }}
                  {{ $errors->first('no-of-chairpersons-female', '<label for="no-of-chairpersons-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group {{ $errors->has('smdc-meetings') ? ' has-error' : '' }}">
                <label class=" control-label" for="smdc-meetings">Number of meetings held by SMDC during the previous academic year</label>
                <div class="input-group">
                  {{ Form::text('smdc-meetings', Input::old('smdc-meetings', $smdc->SMDCMEETING), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'smdc-meetings','maxlength'=>'3')) }}
                  {{ $errors->first('smdc-meetings', '<label for="smdc-meetings" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
              <div class="form-group">
                <label>
                  Whether SMDC Prepare the School Improvement Plan
                </label>
                {{ Form::select('smdc-plan', School::$CHOICE, Input::old('smdc-plan', $smdc->SIP_YN), array('class'=>'form-control'))}}
              </div>
              <div class="form-group">
                <label>
                  Whether separate bank account for SMDC is being maintained
                </label>
                {{ Form::select('smdc-bank-account', School::$CHOICE, Input::old('smdc-bank-account', $smdc->BANKAC_YN),
                array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
              </div>


              <div class="depends-smdc-bank-account">
                <div class="form-group">
                  <label class=" control-label" for="smdc-bank-name'">If yes, </label>
                  <div class="input-group {{ $errors->has('smdc-bank-name') ? ' has-error' : '' }}">
                    <span class="input-group-addon">Bank name</span>
                    {{ Form::text('smdc-bank-name', Input::old('smdc-bank-name', $smdc->BANKNAME), array('class'=>'form-control', 'placeholder'=>'smdc-bank-name')) }}
                    {{ $errors->first('smdc-bank-name', '<label for="smdc-bank-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
                  </div>
                  <div class="input-group {{ $errors->has('smdc-branch-name') ? ' has-error' : '' }}">
                    <span class="input-group-addon">Branch</span>
                    {{ Form::text('smdc-branch-name', Input::old('smdc-branch-name', $smdc->BANKBRANCH), array('class'=>'form-control', 'placeholder'=>'smdc-branch-name')) }}
                    {{ $errors->first('smdc-branch-name', '<label for="smdc-branch-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                  <div class="input-group {{ $errors->has('smdc-acc-no') ? ' has-error' : '' }}">
                    <span class="input-group-addon">Account No</span>
                    {{ Form::text('smdc-acc-no', Input::old('smdc-acc-no', $smdc->BANKACNO),
                    array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','class'=>'form-control', 'placeholder'=>'smdc-acc-no')) }}
                    {{ $errors->first('smdc-acc-no', '<label for="smdc-acc-no" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                  <div class="input-group {{ $errors->has('smdc-acc-name') ? ' has-error' : '' }}">
                    <span class="input-group-addon">Account Name</span>
                    {{ Form::text('smdc-acc-name', Input::old('smdc-acc-name', $smdc->ACNAME), array('class'=>'form-control', 'placeholder'=>'smdc-acc-name')) }}
                    {{ $errors->first('smdc-acc-name', '<label for="smdc-acc-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                  <div class="input-group {{ $errors->has('smdc-ifsc-code') ? ' has-error' : '' }}">
                    <span class="input-group-addon">IFSC Code</span>
                    {{ Form::text('smdc-ifsc-code', Input::old('smdc-ifsc-code', $smdc->IFSC), array('maxlength'=>'11','class'=>'form-control', 'placeholder'=>'smdc-ifsc-code')) }}
                    {{ $errors->first('smdc-ifsc-code', '<label for="smdc-ifsc-code" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>
                  Whether the School Building Committee (SBC) has been constituted:
                </label>
                {{ Form::select('sbc-constituted', School::$CHOICE, Input::old('sbc-constituted', $smdc->SBC_YN), array('class'=>'form-control'))}}
              </div>
            </div>
          </div>


          <div class="form-group">
            <label>
              Whether the school has constituted its Academic Committee (AC)
            </label>
            {{ Form::select('ac-constituted', School::$CHOICE, Input::old('ac-constituted', $smdc->AC_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
          </div>
          <div class="form-group">
            <label>
              Whether the school has constituted its Parent-Teacher Association (PTA)
            </label>
            {{ Form::select('pta-constituted', School::$CHOICE, Input::old('pta-constituted', $smdc->PTA_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
          </div>

          <div class="depends-pta-constituted">
            <div class="form-group {{ $errors->has('pta-meetings') ? ' has-error' : '' }}">
              <label class=" control-label" for="pta-meetings">If yes, number of PTA meetings held during the last academic year:</label>
              <div class="input-group">
                {{ Form::text('pta-meetings', Input::old('pta-meetings', $smdc->PTAMEETING), array('maxlength'=>'3','data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'class'=>'form-control', 'placeholder'=>'pta-meetings')) }}
                {{ $errors->first('pta-meetings', '<label for="pta-meetings" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
          </div>

        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->
    @endif
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>

</form>
@stop
