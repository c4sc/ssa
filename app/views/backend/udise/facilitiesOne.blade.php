@extends('backend.layouts.udise')

@section('title')
Physical Facilities and Equipment
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.facilities-one')

  var building_type = ['pucca', 'partially-pucca', 'kucha', 'tent']
  var states = ['good', 'minor-repair', 'major-repair']
  @if($master->LOWCLASS < 9)
  $('.class-1-8, .good-pucca, .good-partially-pucca, .good-kucha, .good-tent, \
  .minor-repair-pucca, .minor-repair-partially-pucca, .minor-repair-kucha, .minor-repair-tent, \
  .major-repair-pucca, .major-repair-partially-pucca, .major-repair-kucha, .major-repair-tent').keyup(function() {
    total1 = 0
    $('.good-pucca, .good-partially-pucca, .good-kucha, .good-tent, \
    .minor-repair-pucca, .minor-repair-partially-pucca, .minor-repair-kucha, .minor-repair-tent, \
    .major-repair-pucca, .major-repair-partially-pucca, .major-repair-kucha, .major-repair-tent').each(function() {
      total1 += parseInt($(this).val()) || 0;
    });
    $('.class-1-8').parent().removeClass('danger')
    if(total1 != (parseInt($('.class-1-8').val()) || 0)) {
      $('.class-type-0').addClass('danger')
    }else {
      $('.class-type-0').removeClass('danger')
    }
    $('.total-state-0').val(total1);
    $('.total-class-0').val((parseInt($('.class-1-8').val()) || 0));
  });
  $('.good-pucca').keyup();
  @endif
  @if($master->hasSecondary())
  $('.class-9, .class-10, .good-secondary, .minor-repair-secondary, .major-repair-secondary').keyup(function() {
    total1 = total2 = 0
    $('.good-secondary, .minor-repair-secondary, .major-repair-secondary').each(function() {
      total1 += parseInt($(this).val()) || 0;
      $(this).parent().removeClass('danger')
    });
    $('.class-9, .class-10').each(function() {
      total2 += parseInt($(this).val()) || 0;
      $(this).parent().removeClass('danger')
    });
    if(total1 != total2) {
      $('.class-type-1').addClass('danger')
    }else {
      $('.class-type-1').removeClass('danger')
    }
    $('.total-state-1').val(total1);
    $('.total-class-1').val(total2);
  });
  $('.good-secondary').keyup()
  @endif
  @if($master->hasHigherSecondary())
  $('.class-11, .class-12, .good-higher-secondary, .minor-repair-higher-secondary, .major-repair-higher-secondary').keyup(function() {
    total1 = total2 = 0
    $('.good-higher-secondary, .minor-repair-higher-secondary, .major-repair-higher-secondary').each(function() {
      total1 += parseInt($(this).val()) || 0;
      $(this).parent().removeClass('danger')
    });
    $('.class-11, .class-12').each(function() {
      total2 += parseInt($(this).val()) || 0;
      $(this).parent().removeClass('danger')
    });
    if(total1 != total2) {
      $('.class-type-2').addClass('danger')
    }else {
      $('.class-type-2').removeClass('danger')
    }
    $('.total-state-2').val(total1);
    $('.total-class-2').val(total2);
  });
  $('.good-higher-secondary, .minor-repair-higher-secondary, .major-repair-higher-secondary').keyup()
  @endif
  //RAMP NEEDED
  $("select[name='ramp-needed']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-ramp-needed" ).show('slow')
      $( ".depends-ramp-needed input, .depends-ramp-needed select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".depends-ramp-needed" ).hide('slow')
      $( ".depends-ramp-needed input, .depends-ramp-needed select" ).val('')
      $( ".depends-ramp-needed input, .depends-ramp-needed select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='ramp-needed']").change();
  //RAMP AVAILABLE
  $("select[name='ramp-available']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".hand-rails" ).show('slow')
      $( ".hand-rails input, .hand-rails select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".hand-rails" ).hide('slow')
      $( ".hand-rails input, .hand-rails select" ).val('')
      $( ".hand-rails input, .hand-rails select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='ramp-available']").change();

//BOYS HOSTEL
  $("select[name='boys-hostel']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".residing-boys" ).show('slow')
      $( ".residing-boys input, .residing-boys select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".residing-boys" ).hide('slow')
      $( ".residing-boys input, .residing-girls select" ).val('')
      $( ".residing-boys input, .residing-boys select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='boys-hostel']").change();

//GIRLS HOSTEL
  $("select[name='girls-hostel']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".residing-girls" ).show('slow')
      $( ".residing-girls input, .residing-girls select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".residing-girls" ).hide('slow')
      $( ".residing-girls input, .residing-girls select" ).val('')
      $( ".residing-girls input, .residing-girls select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='girls-hostel']").change();

  //library
  $("select[name='library']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-library" ).show('slow')
      $( ".depends-library input, .depends-library select" ).attr('data-parsley-required', 'true')
     } else {
      $( ".depends-library" ).hide('slow')
      $( ".depends-library select" ).removeClass('required');
      $( ".depends-library input, .depends-library select" ).val('')
      $( ".depends-library input, .depends-library select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='library']").change();

  //Play ground
  $("select[name='playground']").on('change', function(event){
    if($(this).val() == 2) {
      $( ".playground-land" ).show('slow')
      $( ".playground-land input, .playground-land select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".playground-land" ).hide('slow')
      $( ".playground-land input, .playground-land select" ).val('')
      $( ".playground-land input, .playground-land select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='playground']").change();

  $("select[name='medical-checkup']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-medical-checkup" ).show('slow')
      $( ".depends-medical-checkup input, .depends-medical-checkup select" ).attr('data-parsley-required', 'true')
    } else {
      $( ".depends-medical-checkup" ).hide('slow')
      $( ".depends-medical-checkup input" ).val('')
      $( ".depends-medical-checkup input, .depends-medical-checkup select" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='medical-checkup']").change();
});
@stop

@section('content')

@if ($errors->has('hs-classes'))
<div class="row">
  <div class="col-md-12">
    <div class="alert alert-danger">
      <i class="fa fa-ban"></i>
      <b>Error!</b>
      classroom for 11, 12 is required
    </div>
  </div>
</div>
@endif

<form role="form" method="post" data-parsley-validate action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Physical Facilities and Equipment</h3>
        </div>
        <div class="box-body">
          <div class="form-group {{ $errors->has('building-status') ? ' has-error' : '' }}">
            <label class="control-label" for="building-status">Status of school building</label>
            {{ Form::select('building-status', Building::$STATUS, Input::old('building-status', $building->BLDSTATUS), array('class'=>'form-control')) }}
            {{ $errors->first('building-status', '<label for="building-status" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group">
            <label class="control-label">Details of Classrooms and availability of furniture</label>
            <table class="table">
              <thead>
                <tr>
                  <th> class/Grade </th>
                  <th> Classrooms used for instruction</th>
                  <th> Classrooms under construction </th>
                  <th> Availability of furniture </th>
                </tr>
              </thead>
              <tbody>
              @if($master->LOWCLASS < 9)
              <tr>
                <td>1-8</td>
                <td> {{ Form::text('class-1-8', Input::old('class-1-8', $building->CLROOMS),
                  array('maxlength'=>'3','class'=>'form-control class-1-8',
                  'data-parsley-type'=>'number', 'min' => '1', 'data-parsley-min' => '1')) }}</td>
                <td> {{ Form::text('class-construction-1-8', Input::old('class-construction-1-8',
                  $building->CLSUNDERCONST), array('maxlength'=>'3','class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td> {{ Form::select('furniture-1-8', Building::$FURNITURE_AVAILABILITY ,
                  Input::old('furniture-1-8', $building->FURNSTU), array('class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              @endif
              @if($master->hasSecondary())
              @foreach(range(9, 10) as $class)
              <tr>
                <td>{{ $class }}</td>
                <td> {{ Form::text("class-$class", Input::old("class-$class",
                  $building->{'TOTCLS'.$class}), array('maxlength'=>'3','class'=>"form-control class-$class",
                  'data-parsley-type'=>'number', 'min' => '1', 'data-parsley-min' => '1')) }}</td>
                <td> {{ Form::text("class-construction-$class", Input::old("class-construction-$class",
                  $building->{'CLSUCONST'.$class}), array('maxlength'=>'3','class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td> {{ Form::select("furniture-$class", Building::$FURNITURE_AVAILABILITY ,
                  Input::old("furniture-$class", $building->{'FURN_YN'.$class}), array('class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              @endforeach
              @endif
              @if($master->hasHigherSecondary())
              @foreach(range(11, 12) as $class)
              <tr>
                <td>{{ $class }}</td>
                <td> {{ Form::text("class-$class", Input::old("class-$class",
                  $building->{'TOTCLS'.$class}), array('maxlength'=>'3','class'=>"form-control class-$class",
                  'data-parsley-type'=>'number', 'min' => '1', 'data-parsley-min' => '1')) }}</td>
                <td> {{ Form::text("class-construction-$class", Input::old("class-construction-$class",
                  $building->{'CLSUCONST'.$class}), array('maxlength'=>'3','class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td> {{ Form::select("furniture-$class", Building::$FURNITURE_AVAILABILITY ,
                  Input::old("furniture-$class", $building->{'FURN_YN'.$class}), array('class'=>'form-control',
                  'data-parsley-type'=>'number', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>

          <div class="form-group {{ $errors->has('other-rooms') ? ' has-error' : '' }}">
            <label class="control-label" for="other-rooms">Total Other Rooms</label>
            {{ Form::text('other-rooms', Input::old('other-rooms', $building->OTHROOMS), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','class'=>'form-control')) }}
            {{ $errors->first('other-rooms', '<label for="other-rooms" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group">
            <label class="control-label">Details of Classrooms by Condition</label>
            <table class="table">
              <thead>
                <tr>
                  <th> type of building block </th>
                  <th> good </th>
                  <th>Need minor repair</th>
                  <th>Need major repair</th>
                </tr>
              </thead>
              <tbody>
              @foreach($sections as $section)
              <input type="hidden" class="total-class-{{ $section }}" name="total-class-{{ $section }}">
              <input type="hidden" class="total-state-{{ $section }}" name="total-state-{{ $section }}">
              @foreach(Building::$BUILDING_TYPE[$section] as $type => $suffix)
              <tr class="class-type-{{ $section }}">
                <td>{{ $type }}</td>
                <td> {{ Form::text("good-$type", Input::old("good-$type",
                  $building->{'CLGOOD'.$suffix}), array('maxlength'=>'3',
                  'data-parsley-type'=>"integer",'class'=>"form-control good-$type", 'min' => '0',
                  'data-parsley-min' => '0')) }}</td>
                <td> {{ Form::text("minor-repair-$type", Input::old("minor-repair-$type",
                  $building->{'CLMINOR'.$suffix}), array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>"form-control minor-repair-$type", 'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td> {{ Form::text("major-repair-$type", Input::old("major-repair-$type",
                  $building->{'CLMAJOR'.$suffix}), array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>"form-control major-repair-$type", 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              @endforeach
              @endforeach
              </tbody>
            </table>
          </div>

          <div class="form-group {{ $errors->has('land-available') ? ' has-error' : '' }}">
            <label>
              Land Available for Additional Classrooms
            </label>
            {{ Form::select('land-available', School::$CHOICE, Input::old('land-available', $building->LAND4CLS_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
          </div>
          <div class="form-group {{ $errors->has('hm-room') ? ' has-error' : '' }}">
            <label>
              Seperate room of Head teacher/Principal available
            </label>
            {{ Form::select('hm-room', School::$CHOICE, Input::old('hm-room', $building->HMROOM_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
          </div>

          <div class="form-group">
            <table class="table">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Boys</th>
                  <th>Girls</th>
                </tr>
              </thead>
              <tbody>
              <tr>
                <td>No. of Toilet Seats Constructed/Available</td>
                <td>{{ Form::text("boys-toilet-constructed", Input::old("boys-toilet-constructed", $building->TOILETB),
                  array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td>{{ Form::text("girls-toilet-constructed", Input::old("girls-toilet-constructed",
                  $building->TOILET_G), array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              <tr>
                <td>No. of Toilet Seats Functional</td>
                <td>{{ Form::text("boys-functional-toilets", Input::old("boys-functional-toilets",
                  $building->TOILETB_FUNC), array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
                    {{ $errors->first('boys-functional-toilets', '<label for="boys-functional-toilets" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
                <td>{{ Form::text("girls-functional-toilets", Input::old("girls-functional-toilets",
                  $building->TOILETG_FUNC), array('maxlength'=>'3','data-parsley-type'=>"integer",
                  'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
                    {{ $errors->first('girls-functional-toilets', '<label for="girls-functional-toilets" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
              </tr>
              <tr>
                <td>How many of above toilets have water available for cleaning</td>
                <td>{{ Form::text("boys-toilets-water", Input::old("boys-toilets-water", $building->TOILETWATER_B),
                array('maxlength'=>'3','data-parsley-type'=>"integer", 'class'=>'form-control'
                , 'min' => '0', 'data-parsley-min' => '0')) }}
                    {{ $errors->first('boys-toilets-water', '<label for="boys-toilets-water" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
                <td>{{ Form::text("girls-toilets-water", Input::old("girls-toilets-water", $building->TOILETWATER_G),
                array('maxlength'=>'3','data-parsley-type'=>"integer", 'class'=>'form-control'
                , 'min' => '0', 'data-parsley-min' => '0')) }}
                    {{ $errors->first('girls-toilets-water', '<label for="girls-toilets-water" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </td>
              </tr>
              <tr>
                <td>Total Urinals Available</td>
                <td>{{ Form::text("boys-urinal", Input::old("boys-urinal", $building->URINALS_B),
                array('maxlength'=>'3','data-parsley-type'=>"integer", 'class'=>'form-control',
                'min' => '0', 'data-parsley-min' => '0')) }}</td>
                <td>{{ Form::text("girls-urinal", Input::old("girls-urinal", $building->URINALS_G),
                array('maxlength'=>'3','data-parsley-type'=>"integer",
                'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}</td>
              </tr>
              </tbody>
            </table>
          </div>



        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Facilities </h3>
        </div>
        <div class="box-body">
          <div class="form-group {{ $errors->has('hand-wash') ? ' has-error' : '' }}">
            <label>
              Is the hand washing facility available near the toilet/urinals
            </label>
            {{ Form::select('hand-wash', School::$CHOICE, Input::old('hand-wash', $building->HANDWASH_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
          </div>

          <div class="form-group {{ $errors->has('cwsn-friendly-toilets') ? ' has-error' : '' }}">
            <label>
              is there any toilet that is friendly to Children with Special Needs (CWSN)
            </label>
            {{ Form::select('cwsn-friendly-toilets', School::$CHOICE, Input::old('cwsn-friendly-toilets',
            $building->TOILETD), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
          </div>

          <div class="form-group {{ $errors->has('drinking-water-source') ? ' has-error' : '' }}">
            <label class="control-label" for='drinking-water-source'>Main source of drinking water</label>
            {{ Form::select('drinking-water-source', Building::$WATER_SOURCE, Input::old('drinking-water-source',
            $building->WATER), array('class'=>'form-control', 'data-parsley-required'=>'required')) }}
            {{ $errors->first('drinking-water-source', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('is-water-facility-functional') ? ' has-error' : '' }}">
            <label>
              Whether drinking water facility is functional
            </label>
            {{ Form::select('is-water-facility-functional', School::$CHOICE, Input::old('is-water-facility-functional',
            $building->WATER_FUNC_YN), array('class'=>'form-control', 'data-parsley-required'=>'true'))}}
          </div>
          <div class="form-group {{ $errors->has('electricity-status') ? ' has-error' : '' }}">
            <label class="control-label" for='electricity-status'> Electricity connection avaialable in the school </label>
            {{ Form::select('electricity-status', Building::$ELECTRICITY_STATUS, Input::old('electricity-status',
            $building->ELECTRIC_YN), array('class'=>'form-control')) }}
            {{ $errors->first('electricity-status', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('boundary-wall') ? ' has-error' : '' }}">
            <label class="control-label" for='boundary-wall'> Type of Boundary wall</label>
            {{ Form::select('boundary-wall', Building::$BOUNDARY_WALL, Input::old('boundary-wall', $building->BNDRYWALL), array('class'=>'form-control')) }}
            {{ $errors->first('boundary-wall', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('library') ? ' has-error' : '' }}">
            <label class="control-label" for='library'>Whether school has Library</label>
            {{ Form::select('library', School::$CHOICE, Input::old('library',
            $building->LIBRARY_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('library', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="depends-library">
            <div class="form-group {{ $errors->has('library-books') ? ' has-error' : '' }}">
              <label class="control-label" for='library-books'>total books in library</label>
              {{ Form::text('library-books', Input::old('library-books', $building->BOOKINLIB),
              array('data-parsley-type'=>"integer", 'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
              {{ $errors->first('library-books', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="form-group {{ $errors->has('librarian') ? ' has-error' : '' }}">
              <label class="control-label" for='librarian'>does the school have a full-time librarian</label>
              {{ Form::select('librarian', School::$CHOICE, Input::old('librarian',
              $building->LIBRARIAN_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('librarian', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->has('subscription') ? ' has-error' : '' }}">
            <label class="control-label" for='subscription'>Does the school subscribe for news paper/magazine</label>
            {{ Form::select('subscription', School::$CHOICE, Input::old('subscription',
            $building->NEWSPAPER_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('subscription', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('playground') ? ' has-error' : '' }}">
            <label class="control-label" for='playground'>Playground</label>
            {{ Form::select('playground', School::$CHOICE, Input::old('playground',
            $building->PGROUND_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('playground', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group playground-land {{ $errors->has('playground-land') ? ' has-error' : '' }}">
            <label class="control-label" for='playground-land'>If no, whether land is available for developing playground-land</label>
            {{ Form::select('playground-land', School::$CHOICE, Input::old('playground-land',
            $building->LAND4PGROUND_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('playground-land', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group{{ $errors->has('computer-available') || $errors->has('computer-functional') ? ' has-error' : '' }}">
            <label class="control-label" >Total number of computers for teaching and learning purposes</label>
            <div class="input-group">
              <span class="input-group-addon">Available</span>
              {{ Form::text('computer', Input::old('computer', $building->COMPUTER),
              array('data-parsley-required'=>'true', 'data-parsley-type'=>'number',
              'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
              <span class="input-group-addon">Functional</span>
              {{ Form::text('computer-functional', Input::old('computer-functional', $building->TOTCOMP_FUNC),
              array('data-parsley-required'=>'true', 'data-parsley-type'=>'number',
              'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
            </div>
            {{ $errors->first('computer', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            {{ $errors->first('computer-functional', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('cal-lab') ? ' has-error' : '' }}">
            <label class="control-label" for='cal-lab'>Does the school have Computer Aided Learning (CAL) Lab</label>
            {{ Form::select('cal-lab', School::$CHOICE, Input::old('cal-lab',
            $building->CAL_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('cal-lab', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('medical-checkup') ? ' has-error' : '' }}">
            <label class="control-label" for='medical-checkup'>Whether Medical check-up of students conducted last year</label>
            {{ Form::select('medical-checkup', School::$CHOICE, Input::old('medical-checkup',
            $building->MEDCHK_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('medical-checkup', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group depends-medical-checkup {{ $errors->has('medical-checkup-frequency') ? ' has-error' : '' }}">
            <label class="control-label" for='medical-checkup-frequency'>Frequency of Medical check-up in the school
              during last year</label>
            {{ Form::text('medical-checkup-frequency', Input::old('medical-checkup-frequency',
            $building->BOX), array('class'=>'form-control', 'data-parsley-required'=>'true', 'min' => '0', 'data-parsley-min' => '0')) }}
            {{ $errors->first('medical-checkup-frequency', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('ramp-needed') ? ' has-error' : '' }}">
            <label class="control-label" for='ramp-needed'>Whether ramp for disabled children needed to access classrooms</label>
            {{ Form::select('ramp-needed', School::$CHOICE, Input::old('ramp-needed',
            $building->RAMPSNEEDED_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('ramp-needed', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>


          <div class="form-group depends-ramp-needed">
            <div class="form-group {{ $errors->first('ramp-available', ' has-error') }} ramp-available">
              <label class="control-label" for='ramp-available'>If yes, whether ramp(s) is/are available</label>
              {{ Form::select('ramp-available', School::$CHOICE, Input::old('ramp-available',
              $building->RAMPS_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('ramp-available', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="form-group hand-rails {{ $errors->has('hand-rails') ? ' has-error' : '' }}">
              <label class="control-label" for='hand-rails'>If yes, whether Hand-rails for ramp is available</label>
              {{ Form::select('hand-rails', School::$CHOICE, Input::old('hand-rails',
              $building->HANDRAILS), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('hand-rails', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group {{ $errors->has('campus-plan') ? ' has-error' : '' }}">
            <label class="control-label" for='campus-plan'>Whether measured campus plan prepared</label>
            {{ Form::select('campus-plan', School::$CHOICE, Input::old('campus-plan',
            $building->CAMPUSPLAN_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('campus-plan', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          @if ($master->hasSecondary() || $master->hasHigherSecondary())
          <div class="form-group {{ $errors->has('boys-hostel') ? ' has-error' : '' }}">
            <label class="control-label" for='boys-hostel'>Does the school have a boys hostel(s)?</label>
            {{ Form::select('boys-hostel', School::$CHOICE, Input::old('boys-hostel',
            $building->HOSTELB_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('boys-hostel', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group residing-boys {{ $errors->has('residing-boys') ? ' has-error' : '' }}">
            <label class="control-label" for='residing-boys'>if yes, total boys residing in hostel</label>
            {{ Form::text('residing-boys', Input::old('residing-boys', $building->HOSTELBOYS),
            array('data-parsley-type'=>"integer", 'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
            {{ $errors->first('residing-boys', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('girls-hostel') ? ' has-error' : '' }} ">
            <label class="control-label" for='girls-hostel'>Does the school have a girls hostel(s)?</label>
            {{ Form::select('girls-hostel', School::$CHOICE, Input::old('girls-hostel',
            $building->HOSTELG_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('girls-hostel', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group residing-girls {{ $errors->has('residing-girls') ? ' has-error' : '' }}">
            <label class="control-label" for='residing-girls'>if yes, total girls residing in hostel</label>
            {{ Form::text('residing-girls', Input::old('residing-girls', $building->HOSTELGIRLS),
            array('data-parsley-type'=>"integer", 'class'=>'form-control', 'min' => '0', 'data-parsley-min' => '0')) }}
            {{ $errors->first('residing-girls', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          @endif
        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div> <!-- row -->

  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
