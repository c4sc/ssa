@include('backend.school.menu')
<?php
$group = '';
?>

@foreach ($udiseMenu as $route=>$link)
@if ($group != $link['group'])
@if ($group != '')
</ul>
</li>
@endif
<?php
$group = $link['group'];
?>
<li class="treeview {{ $group }}Group">
<a>
  <i class="fa fa-th"></i> <span>{{ School::$UDISE_ROUTE_GROUPS[$group] }}</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  @endif
  <li class='{{ $route }}'><a href="{{ route($route, $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i>
    {{ $link['title'] }}
  </a></li>
@endforeach
