@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.mdm')

  $("select[name='mdm-status']").on('change', function(event){
    status = $(this).val()
    if( status == 2) {
      $( ".kitchen-status-group" ).show();
      $( ".kitchen-status-group select, .kitchen-status-group input" ).attr('data-parsley-required', 'true')
      $( ".mdm-source-group" ).hide();
      $( ".mdm-source-group select, .mdm-source-group input" ).attr('data-parsley-required', 'false')
    } else if( status == 3) {
      $( ".kitchen-status-group" ).hide();
      $( ".kitchen-status-group select, .kitchen-status-group input" ).attr('data-parsley-required', 'false')
      $( ".mdm-source-group" ).show();
      $( ".mdm-source-group select, .mdm-source-group input" ).attr('data-parsley-required', 'true')
    } else {
      $( ".kitchen-status-group" ).hide();
      $( ".kitchen-status-group select, .kitchen-status-group input" ).attr('data-parsley-required', 'false')
      $( ".mdm-source-group" ).hide();
      $( ".mdm-source-group select, .mdm-source-group input" ).attr('data-parsley-required', 'false')
    }
  });
  $("select[name='mdm-status']").change();
});
@stop

@section('content')

<form role="form" method="post" action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Mid day meal information</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label" for="mdm-status">Status of Mid-day Meal</label>
            {{ Form::select('mdm-status', Mdm::$MDM_STATUS, Input::old('mdm-status', $mdm->MEALSINSCH), array('class'=>'form-control')) }}
            {{ $errors->first('mdm-status', '<label for="mdm-status" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group kitchen-status-group">
            <label class="control-label" for="kitchen-status">If Provided &amp; prepared in school premises then
              status of Kitchen Shed</label>
            {{ Form::select('kitchen-status', Mdm::$KITCHEN_STATUS, Input::old('kitchen-status', $mdm->KITSHED), array('class'=>'form-control')) }}
            {{ $errors->first('kitchen-status', '<label for="kitchen-status" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group mdm-source-group">
            <label class="control-label" for="mdm-source">If Provided but not prepared in school premises,
              Provide source of MDM</label>
            {{ Form::select('mdm-source', Mdm::$MDM_SOURCE, Input::old('mdm-source', $mdm->MDM_MAINTAINER), array('class'=>'form-control')) }}
            {{ $errors->first('mdm-source', '<label for="mdm-source" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
