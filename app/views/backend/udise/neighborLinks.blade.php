<ul class="nav nav-pills navbar-right">
  @if ($neighborRoutes['previous'])
  <li><a href="{{ URL::route($neighborRoutes['previous'], $master->SCHCD) }}" class="fa fa-fast-backward"> Previous</a></li>
  @endif
  @if ($neighborRoutes['next'])
  <li><a href="{{ URL::route($neighborRoutes['next'], $master->SCHCD) }}" class="fa fa-fast-forward"> Next</a></li>
  @endif
</ul>

