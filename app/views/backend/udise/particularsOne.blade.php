@extends('backend.layouts.udise')

@section('title')
School Particulars
@stop

@section('script')
//ELEMENTARY
function hasElementary() {
    if(parseInt($('#lowest-class').val()) < 8) {
     return true;
   }
   return false;
}
//PRIMARY
function hasPrimary() {
   if(parseInt($('#lowest-class').val()) >= 5) {
     return false;
   }
   return true;
}
//UPPER PRIMARY

function hasUpperPrimary() {
  if(parseInt($('#lowest-class').val()) <= 6 && parseInt($('#highest-class').val()) >= 6 ) {
    return true;
  }
  return false;
}

//SECONDARY

function hasSecondary() {
  if(parseInt($('#lowest-class').val()) > 10 || parseInt($('#highest-class').val()) < 9 ) {
  return false;
  }
  return true;
}

//HIGHER SECONDARY


function hasHigherSecondary() {
  if(parseInt($('#highest-class').val()) >= 11) {
    return true;
  }
  return false;
}

$(function() {
  activate('.particularsGroup', '.particulars-one')

//RESIDENTIAL PRIMARY
  $("select[name='residential-primary'], select[name='residential-up'], select[name='residential-secondary'], \
    select[name='residential-hs']").on('change', function(){

    if(($("select[name='residential-primary']").val() != 1 && $("select[name='residential-up']").val() != 1) &&
      ($("select[name='residential-secondary']").val() != 1 && $("select[name='residential-hs']").val() != 1)) {
      $( ".depends-residential" ).hide('slow')
      $( ".depends-residential select, .depends-residential input" ).attr('data-parsley-required', 'false')
    } else {
      $( ".depends-residential" ).show('slow')
      $( ".depends-residential select, .depends-residential input" ).attr('data-parsley-required', 'true')
    }
  });

  // CLASS CHECKING

  //PRIMARY CLASS
  $('#lowest-class, #highest-class').keyup(function() {
    if(hasPrimary()) {
      $( ".depends-primary, .residential-primary" ).show();
      $( ".depends-primary input, .depends-primary select, .residential-primary select, .residential-primary input").attr(
        'data-parsley-required', 'true');

    }
    else {
      $( ".depends-primary, .residential-primary" ).hide();
      $( ".depends-primary input, .depends-primary select, .residential-primary select, .residential-primary input" ).val('');
      $( ".depends-primary input, .depends-primary select, .residential-primary select, .residential-primary input").attr(
        'data-parsley-required', 'false');
    }
    //UPPERPRIMARY
    if(hasUpperPrimary()) {
      $( ".residential-up" ).show();
      $( ".residential-up select, .residential-up input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".residential-up" ).hide();
      $( ".residential-up select, .residential-up input" ).val('');
      $( ".residential-up select, .residential-up input" ).attr('data-parsley-required', 'false');
    }
    //SECONDARY
    if(hasSecondary()) {
      $( ".residential-secondary" ).show();
      $( ".residential-secondary select, .residential-secondary input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".residential-secondary" ).hide();
      $( ".residential-secondary select, .residential-secondary input" ).val('');
      $( ".residential-secondary select, .residential-secondary input" ).attr('data-parsley-required', 'false');
    }
    //HIGHERSECONDARY
    if(hasHigherSecondary()) {
      $( ".residential-hs" ).show();
      $( ".residential-hs select, .residential-hs input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".residential-hs" ).hide();
      $( ".residential-hs select, .residential-hs input" ).val('');
      $( ".residential-hs select, .residential-hs input" ).attr('data-parsley-required', 'false');
    }

  //AFFILIATION BOARD CHECKING
    if(hasSecondary() || hasHigherSecondary()) {
      $( ".affiliation-board" ).show();
      $( ".affiliation-board select, .affiliation-board input" ).attr('data-parsley-required', 'true');
      $(".pre-vocational").show();
      $(".pre-vocational input, .pre-vocational select").attr('data-parsley-required', 'true');
      if(hasSecondary()) {
        $( ".board-secondary" ).show();
        $( ".board-secondary select, .board-secondary input" ).attr('data-parsley-required', 'true');
      }
      else {
        $( ".board-secondary" ).hide();
        $(".board-secondary select, .board-secondary input").val('');
        $( ".board-secondary select, .board-secondary input" ).attr('data-parsley-required', 'false');
      }

      if(hasHigherSecondary()) {
        $( ".board-hs" ).show();
        $( ".board-hs select, .board-hs input" ).attr('data-parsley-required', 'true');
      }
      else {
        $( ".board-hs" ).hide();
        $(".board-hs select, .board-hs input").val('');
        $( ".board-hs select, .board-hs input" ).attr('data-parsley-required', 'false');
      }

    }
    else {
      $( ".affiliation-board" ).hide();
      $(".affiliation-board select, .affiliation-board input").val('');
      $( ".affiliation-board select, .affiliation-board input" ).attr('data-parsley-required', 'false');
      $(".pre-vocational input, .pre-vocational select").attr('data-parsley-required', 'false');
      $( ".pre-vocational select, .pre-vocational input" ).val('');
      $( ".pre-vocational").hide();
    }

    if(hasElementary()) {
      $( ".depends-elementary" ).show();
      $( ".elementary-recognition" ).show();
      $( ".depends-elementary select, .depends-elementary input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".depends-elementary" ).hide();
      $(".depends-elementary select, .depends-elementary input").val('');
      $( ".elementary-recognition" ).hide();
      $( ".elementary-recognition select, .elementary-recognition input" ).val('');
      $( ".depends-elementary select, .depends-elementary input" ).attr('data-parsley-required', 'false');
    }

    if(hasSecondary()) {
      $( ".depends-secondary" ).show();
      $( ".secondary-recognition" ).show();
      $( ".depends-secondary select, .depends-secondary input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".depends-secondary" ).hide();
      $( ".secondary-recognition" ).hide();
      $( ".depends-secondary select, .depends-secondary input" ).val('');
      $( ".secondary-recognition input, .secondary-recognition select" ).val('');
      $( ".depends-secondary select, .depends-secondary input" ).attr('data-parsley-required', 'false');
    }

    if(hasHigherSecondary()) {
      $( ".depends-hs" ).show();
      $( ".hs-recognition" ).show();
      $( ".depends-hs select, .depends-hs input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".depends-hs" ).hide();
      $(".depends-hs select, .depends-hs input").val('');
      $( ".hs-recognition" ).hide();
      $(".hs-recognition select, .hs-recognition input").val('');
      $( ".depends-hs select, .depends-hs input" ).attr('data-parsley-required', 'false');
    }

  //UPGRADATION
    if(hasPrimary() && hasUpperPrimary()) {
      $( ".primary-up-upgradation" ).show();
      $( ".primary-up-upgradation select, .primary-up-upgradation input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".primary-up-upgradation" ).hide();
      $(".primary-up-upgradation select, .primary-up-upgradation input").val('');
      $( ".primary-up-upgradation select, .primary-up-upgradation input" ).attr('data-parsley-required', 'false');
    }

    if(hasElementary() && hasSecondary()) {
      $( ".e-s-upgradation" ).show();
      $( ".e-s-upgradation select, .e-s-upgradation input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".e-s-upgradation" ).hide();
      $(".e-s-upgradation select, .e-s-upgradation input").val('');
      $( ".e-s-upgradation select, .e-s-upgradation input" ).attr('data-parsley-required', 'false');
    }

    if(hasSecondary() && hasHigherSecondary()) {
      $( ".s-hs-upgradation" ).show();
      $( ".s-hs-upgradation select, .s-hs-upgradation input" ).attr('data-parsley-required', 'true');
    }
    else {
      $( ".s-hs-upgradation" ).hide();
      $(".s-hs-upgradation select, .s-hs-upgradation input").val('');
      $( ".s-hs-upgradation select, .s-hs-upgradation input" ).attr('data-parsley-required', 'false');
    }
    if ( ! hasSecondary() && hasUpperPrimary()) {
      $('#hs-nearby').show();
      $( "#hs-nearby select, #hs-nearby input" ).attr('data-parsley-required', 'true');
    }
    else  {
      $('#hs-nearby').hide();
      $( "#hs-nearby select, #hs-nearby input" ).attr('data-parsley-required', 'false');
      $( "#hs-nearby select, #hs-nearby input" ).val('');
    }
    if ( hasPrimary()  && ! hasUpperPrimary()) {
      $('#up-nearby').show();
      $( "#up-nearby select, #up-nearby input" ).attr('data-parsley-required', 'true');
    }
    else {
      $('#up-nearby').hide();
      $( "#up-nearby select, #up-nearby input" ).attr('data-parsley-required', 'false');
      $( "#up-nearby select, #up-nearby input" ).val('');
    }
  });

  //RURAL-URBAN SELECTION
  $("select[name='rural-urban']").on('change', function(event){
    if($(this).val() == 2) {
      $( ".panchayath" ).hide('slow');
      $( ".panchayath select" ).val('');
      $( ".municipality" ).show('slow');
      $( ".city" ).show('slow');
      $( ".panchayath select, .panchayath input" ).attr('data-parsley-required', 'false');
      $( ".municipality select, .municipality input" ).attr('data-parsley-required', 'true');
      $( ".city select, .city input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".municipality" ).hide('slow');
      $( ".municipality select" ).val('');
      $( ".city" ).hide('slow');
      $( ".panchayath" ).show('slow');
      $( ".panchayath select, .panchayath input" ).attr('data-parsley-required', 'true');
      $( ".municipality select, .municipality input" ).attr('data-parsley-required', 'false');
      $( ".city select, .city input" ).attr('data-parsley-required', 'false');
    }
  });

  $("select[name='religious-minority']").on('change', function(event){
    if($(this).val() == 2) {
      $( ".depends-religious-minority" ).hide('slow');
      $( ".depends-religious-minority select" ).val('');
      $( ".depends-religious-minority select, .depends-religious-minority input" ).attr('data-parsley-required', 'false');
    } else {
      $( ".depends-religious-minority" ).show('slow');
      $( ".depends-religious-minority select, .depends-religious-minority input" ).attr('data-parsley-required', 'true');
    }
  });

  $('#lowest-class').keyup();
  $("select[name='residential-primary'], select[name='residential-up'], select[name='residential-secondary'], \
    select[name='residential-hs']").change();
  $("select[name='religious-minority']").change();
  $("select[name='rural-urban']").change();

});
@stop

@section('content')
<form role="form" method="post" data-parsley-validate  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Basic Details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label class="col-md-4 control-label" for="">DISE Code</label>
            <span class="">
              {{ $school->SCHCD }}
            </span>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="">Academic Year</label>
            <span class="">
              {{ $schoolMaster->AC_YEAR }}
            </span>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="">School Name</label>
            <span class="">
              {{{ $school->SCHNAME }}}
            </span>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="">Block</label>
            <span class=""> {{ isset($block->id)?$block->name:'' }} </span>
          </div>
          <div class="form-group {{ $errors->first('rural_urban', ' has-error') }}">
            <label class=" control-label" for="rural_urban">Rural or Urban</label>
            {{ Form::select('rural-urban', School::$AREA, Input::old('rural-urban', $schoolMaster->RURURB), array('id'=>'rural-urban','class'=>'form-control')) }}
            {{ $errors->first('rural-urban', '<label for="rural-urban" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('village', ' has-error') }}">
            <label class=" control-label" for="village">Village</label>
            {{ Form::select('village', $villages, Input::old('village', $schoolMaster->VILCD), array('class'=>'form-control')) }}
            {{ $errors->first('village', '<label for="village" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->first('cluster', ' has-error') }}">
            <label class="control-label" for="cluster">Cluster</label>
            {{ Form::select('cluster', $clusters, Input::old('cluster', $schoolMaster->CLUCD),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('cluster', '<label for="cluster" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('habitation') ? ' has-error' : '' }}">
            <label class="control-label" for="habitation">Habitation</label>
            {{ Form::select('habitation', $habitations, Input::old('habitation', $schoolMaster->HABITCD),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('habitation', '<label for="habitation" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('pincode') ? ' has-error' : '' }}">
            <label class=" control-label" for="pincode">Pincode</label>
            <input id="pincode" name="pincode" placeholder="Pincode" data-parsley-type="integer"
            data-parsley-maxlength='6' data-parsley-validation-threshold='6' data-parsley-minlength='6'
            data-parsley-trigger='keyup' data-parsley-error-message="Pincode must be 6 digits!!!" value="{{{
            Input::old('pincode', $schoolMaster->PINCD) }}}" class="form-control input-md" data-parsley-required='true' type="text">
            {{ $errors->first('pincode', '<label for="pincode" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('edu-block') ? ' has-error' : '' }}">
            <label class=" control-label" for="edu-block">Education Block</label>
            {{ Form::select('edu-block', $eduBlocks, Input::old('edu-block', $schoolMaster->EDUBLKCD),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('edu-block', '<label for="edu-block" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('assembly-constituency') ? ' has-error' : '' }}">
            <label class=" control-label" for="assembly-constituency">Assembly Constituency</label>
            {{ Form::select('assembly-constituency', $constituencies, Input::old('assembly-constituency',
            $schoolMaster->ACONSTCD), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('assembly-constituency', '<label for="assembly-constituency" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

          </div>
          <div class="form-group municipality {{ $errors->has('municipality') ? ' has-error' : '' }}">
            <label class=" control-label" for="municipality">Municipality</label>
            {{ Form::select('municipality', $municipalities, Input::old('municipality', $schoolMaster->MUNICIPALCD), array('class'=>'form-control')) }}
            {{ $errors->first('municipality', '<label for="municipality" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group city {{ $errors->has('city') ? ' has-error' : '' }}">
            <label class=" control-label" for="city">City</label>
            {{ Form::select('city', $cities, Input::old('city', $schoolMaster->CITY), array('class'=>'form-control')) }}
            {{ $errors->first('city', '<label for="city" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

          </div>

          <div class="form-group panchayath {{ $errors->has('panchayath') ? ' has-error' : '' }}">
            <label class=" control-label" for="panchayath">Panchayath</label>
            {{ Form::select('panchayath', $panchayaths, Input::old('panchayath', $schoolMaster->PANCD), array('class'=>'form-control')) }}
            {{ $errors->first('panchayath', '<label for="panchayath" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}

          </div>

        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Contact Details</h3>
        </div>
        <div class="box-body">
          <div class="form-group {{ $errors->has('user-group') ? ' has-error' : '' }}">
            <label class=" control-label" for="std-code">Office Phone</label>
            <div class="input-group {{ $errors->has('std-code') ? ' has-error' : '' }}">
              <span class="input-group-addon">STD Code</span>
              {{ Form::text('std-code', Input::old('std-code', $schoolMaster->STDCODE1),
              array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'data-parsley-trigger'=>"keyup",'data-parsley-length'=>'[4,5]', 'class'=>'form-control', 'placeholder'=>'std-code')) }}
              {{ $errors->first('std-code', '<label for="std-code" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('phone-number') ? ' has-error' : '' }}">
              <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
              {{ Form::text('phone-number', Input::old('phone-number', $schoolMaster->PHONE1), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0',
              'data-parsley-trigger'=>"keyup",'data-parsley-length'=>'[6,7]', 'class'=>'form-control', 'placeholder'=>'phone-number')) }}
              {{ $errors->first('phone-number', '<label for="phone-number" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
              <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
              {{ Form::text('mobile', Input::old('mobile', $schoolMaster->MOBILE1), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0',
              'data-parsley-trigger'=>"keyup",'data-parsley-maxlength'=>'10', 'class'=>'form-control', 'placeholder'=>'mobile')) }}
              {{ $errors->first('mobile', '<label for="mobile" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group">
            <label class=" control-label" for="respondent">Respondent</label>
            <div class="input-group {{ $errors->has('respondent-std-code') ? ' has-error' : '' }}">
              <span class="input-group-addon">STD Code</span>
              {{ Form::text('respondent-std-code', Input::old('respondent-std-code', $schoolMaster->STDCODE2),
              array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0', 'data-parsley-trigger'=>"keyup",'data-parsley-length'=>'[4,5]', 'class'=>'form-control', 'placeholder'=>'respondent-std-code')) }}
              {{ $errors->first('respondent-std-code', '<label for="respondent-std-code" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('respondent-phone-number') ? ' has-error' : '' }}">
              <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
              {{ Form::text('respondent-phone-number', Input::old('respondent-phone-number', $schoolMaster->PHONE2),
              array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-trigger'=>"keyup",'data-parsley-length'=>'[6,7]', 'class'=>'form-control', 'placeholder'=>'respondent-phone-number')) }}
              {{ $errors->first('respondent-phone-number', '<label for="respondent-phone-number" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('respondent-mobile') ? ' has-error' : '' }}">
              <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
              {{ Form::text('respondent-mobile', Input::old('respondent-mobile', $schoolMaster->MOBILE2),
              array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-trigger'=>"keyup",'data-parsley-maxlength'=>'10', 'class'=>'form-control', 'placeholder'=>'respondent-mobile')) }}
              {{ $errors->first('respondent-mobile', '<label for="respondent-mobile" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label class=" control-label" for="email">email</label>
            <div class="input-group">
              {{ Form::text('email', Input::old('email', $schoolMaster->EMAIL),
              array('type'=>'email','data-parsley-type'=>"email", 'class'=>'form-control', 'placeholder'=>'email')) }}
              {{ $errors->first('email', '<label for="email" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->has('website') ? ' has-error' : '' }}">
            <label class=" control-label" for="website">website</label>
            <div class="input-group">
              {{ Form::text('website', Input::old('website', $schoolMaster->WEBSITE),
              array('class'=>'form-control', 'placeholder'=>'website')) }}
              {{ $errors->first('website', '<label for="website" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span for="websiteexample">Example: http://www.example.com</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h3 class="box-title">Classes</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label class=" control-label" for="class">class</label>
            <div class="input-group {{ $errors->has('lowest-class') ? ' has-error' : '' }}">
              <span class="input-group-addon">Lowest</span>
              <input type="text" class="form-control" value="{{{ Input::old('lowest-class', $schoolMaster->LOWCLASS) }}}"
              name="lowest-class" id="lowest-class" data-parsley-required='true' data-parsley-type='integer' type ="number"
              placeholder="lowest-class" data-parsley-trigger='keyup' data-parsley-range='[1,12]'>
              {{ $errors->first('lowest-class', '<label for="lowest-class" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group {{ $errors->has('highest-class') ? ' has-error' : '' }}">
              <span class="input-group-addon">Highest</span>
              <input type="text" class="form-control" value="{{{ Input::old('highest-class', $schoolMaster->HIGHCLASS) }}}"
              name="highest-class" id="highest-class" placeholder="highest-class"
              data-parsley-type="integer" data-parsley-required='true' data-parsley-gte='#lowest-class'
              data-parsley-error-message='Highest class must be between 1 and 12 and not less than lowest class!!'
              data-parsley-trigger='keyup' data-parsley-range='[1,12]' >
              {{ $errors->first('highest-class', '<label for="highest-class" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Type details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Section</th>
                  <th>School type</th>
                  <th>Managed by</th>
                </tr>
              </thead>
              <tbody>
              <tr class="depends-elementary">
                <th>Elementary</th>
                <td>
                  <div class="input-group{{ $errors->first('elementary-type', ' has-error') }}">

                    {{ Form::select('elementary-type', School::$TYPES, Input::old('elementary-type', $schoolMaster->SCHTYPE), array('class'=>'form-control')) }}
                    {{ $errors->first('elementary-type', '<label for="elementary-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
                <td>
                  <div class="input-group{{ $errors->first('elementary-management', ' has-error') }}">
                    {{ Form::select('elementary-management', School::$MANAGEMENTS, Input::old('elementary-management',
                    $schoolMaster->SCHMGT), array('class'=>'form-control')) }}
                    {{ $errors->first('elementary-management', '<label for="elementary-management" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
              </tr>
              <tr class="depends-secondary">
                <th>Secondary Stage</th>
                <td>
                  <div class="input-group{{ $errors->first('secondary-type', ' has-error') }}">

                    {{ Form::select('secondary-type', School::$TYPES, Input::old('secondary-type', $schoolMaster->SCHTYPES), array('class'=>'form-control')) }}
                    {{ $errors->first('secondary-type', '<label for="secondary-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
                <td>
                  <div class="input-group{{ $errors->first('secondary-management', ' has-error') }}">

                    {{ Form::select('secondary-management', School::$MANAGEMENTS, Input::old('secondary-management',
                    $schoolMaster->SCHMGTS), array('class'=>'form-control')) }}
                    {{ $errors->first('secondary-management', '<label for="secondary-management" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
              </tr>
              <tr class="depends-hs">
                <th>Hr. Secondary Stage</th>
                <td>
                  <div class="input-group{{ $errors->first('hs-type', ' has-error') }}">
                    {{ Form::select('hs-type', School::$TYPES, Input::old('higher-secondary-type', $schoolMaster->SCHTYPEHS), array('class'=>'form-control')) }}
                    {{ $errors->first('hs-type', '<label for="higher-secondary-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
                <td>
                  <div class="input-group{{ $errors->first('hs-management', ' has-error') }}">
                    {{ Form::select('hs-management', School::$MANAGEMENTS, Input::old('higher-secondary-management',
                    $schoolMaster->SCHMGTHS), array('class'=>'form-control')) }}
                    {{ $errors->first('hs-management', '<label for="higher-secondary-management" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class="form-group {{ $errors->has('distance-to-up') ? ' has-error' : '' }}" id="up-nearby">
            <label class=" control-label" for="distance-to-up">Distance from primary to nearest govt. / aided upper primary school</label>
            <div class="input-group">
              <input type="text" class="form-control" value="{{{ Input::old('distance-to-up', $schoolMaster->DISTP) }}}"
              name="distance-to-up" id="distance-to-up" placeholder="distance to up" data-parsley-trigger='keyup' data-parsley-type='number' min = "0" data-parsley-min = "0" >
              <span class="input-group-addon">KM</span>
            </div>
          </div>
          <div class="form-group {{ $errors->has('distance-to-secondary') ? ' has-error' : '' }}" id="hs-nearby">
            <label class=" control-label" for="distance-to-secondary">Distance from upper primary to nearest govt./aided secondary school</label>
            <div class="input-group">
              <input type="text" class="form-control" value="{{{ Input::old('distance-to-secondary', $schoolMaster->DISTU)
              }}}" name="distance-to-secondary" id="distance-to-secondary" placeholder="distance-to-secondary"
              data-parsley-trigger="keyup" data-parsley-type='number' type= "text" data-parsley-trigger='keyup' min = "0" data-parsley-min = "0">
              <span class="input-group-addon">KM</span>
            </div>
          </div>
        </div>
      </div> <!-- box-body -->
    </div> <!-- col -->

    <div class="col-md-6">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h3 class="box-title">More Details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label class="control-label" for='road'>Approachable by all weather roads
            </label>
            {{ Form::select('road', School::$CHOICE, Input::old('road', $schoolMaster->APPROACHBYROAD),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('road', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group {{ $errors->has('establishment-year') ? ' has-error' : '' }}">
            <label class=" control-label" for="establishment-year">Establishment year</label>
            {{ Form::text('establishment-year', Input::old('establishment-year', $schoolMaster->ESTDYEAR),
            array('data-parsley-trigger'=>'keyup','data-parsley-validation-threshold'=>'4',
            'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]','data-parsley-error-message'=>"Year
            must be four digits!!",'class'=>'form-control','placeholder'=>'establishment-year','id'=>'establishment-year')) }}
            {{ $errors->first('establishment-year', '<label for="establishment-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="form-group ">
            <label class=" control-label" >Year of recognition</label>
            <div class="input-group elementary-recognition {{ $errors->has('elementary-recognition') ? ' has-error' : '' }}">
              <span class="input-group-addon">Elementary</span>
              {{ Form::text('elementary-recognition', Input::old('elementary-recognition', $schoolMaster->YEARRECOG),
              array('data-parsley-trigger'=>'keyup', 'data-parsley-gte'=>'#establishment-year',
              'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]',
              'data-parsley-error-message'=>"Recognition year must be four digits and should not be less than establishment year!!",
              'class'=>'form-control',
              'placeholder'=>'elementary-recognition','id'=>'elementary-recognition')) }}
              {{ $errors->first('elementary-recognition', '<label for="elementary-recognition" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group secondary-recognition {{ $errors->has('secondary-recognition') ? ' has-error' : '' }}">
              <span class="input-group-addon">Secondary</span>
              {{ Form::text('secondary-recognition', Input::old('secondary-recognition', $schoolMaster->YEARRECOGS),
              array('data-parsley-trigger'=>'keyup','data-parsley-gte'=>"#establishment-year",'data-parsley-validation-threshold'=>'4','data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]','data-parsley-error-message'=>"Recognition year must be four digits and should not be less than establishment year!!",
              'class'=>'form-control', 'placeholder'=>'secondary-recognition','id'=>'secondary-recognition')) }}
              {{ $errors->first('secondary-recognition', '<label for="secondary-recognition" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group hs-recognition {{ $errors->has('hs-recognition') ? ' has-error' : '' }}">
              <span class="input-group-addon">Hr.Secondary</span>
              {{ Form::text('hs-recognition', Input::old('hs-recognition', $schoolMaster->YEARRECOGH),
              array('data-parsley-trigger'=>'keyup','data-parsley-gte'=>"#establishment-year",'data-parsley-validation-threshold'=>'4','data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]','data-parsley-error-message'=>"Recognition year must be four digits and should not be less than establishment year!!",'class'=>'form-control', 'placeholder'=>'hs-recognition','id'=>'hs-recognition')) }}
              {{ $errors->first('hs-recognition', '<label for="hs-recognition" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group">
            <label class=" control-label" >Year of upgradation</label>
            <div class="input-group primary-up-upgradation {{ $errors->has('primary-up-upgradation') ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary-Upper Primary</span>
              {{ Form::text('primary-up-upgradation', Input::old('primary-up-upgradation', $schoolMaster->YEARUPGRD),
              array('data-parsley-trigger'=>'keyup','data-parsley-gte'=>"#establishment-year",'data-parsley-validation-threshold'=>'4','data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]',
              'data-parsley-error-message'=>"must be four digits and should not be less than establishment year",
              'class'=>'form-control', 'placeholder'=>'primary-up-upgradation','id'=>'primary-up-upgradation')) }}
              {{ $errors->first('primary-up-upgradation', '<label for="primary-up-upgradation" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group e-s-upgradation {{ $errors->has('e-s-upgradation') ? ' has-error' : '' }}">
              <span class="input-group-addon">Elementary-Secondary</span>
              {{ Form::text('e-s-upgradation', Input::old('e-s-upgradation', $schoolMaster->YEARUPGRDS),
              array('data-parsley-trigger'=>'keyup','data-parsley-gte'=>"#establishment-year",'data-parsley-validation-threshold'=>'4','data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-length'=>'[4,4]',
              'data-parsley-error-message'=>"must be four digits and should not be less than establishment year",
              'class'=>'form-control', 'placeholder'=>'e-s-upgradation','id'=>'e-s-upgradation')) }}
              {{ $errors->first('e-s-upgradation', '<label for="e-s-upgradation" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group s-hs-upgradation {{ $errors->has('s-hs-upgradation') ? ' has-error' : '' }}">
              <span class="input-group-addon">Secondary-Hr.Secondary</span>
              {{ Form::text('s-hs-upgradation', Input::old('s-hs-upgradation', $schoolMaster->YEARUPGRDH),
              array('data-parsley-trigger'=>'keyup','data-parsley-validation-threshold'=>'4',
              'data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-gte'=>"#establishment-year",'data-parsley-length'=>'[4,4]',
              'data-parsley-error-message'=>"must be four digits and should not be less than establishment year",
              'class'=>'form-control', 'placeholder'=>'s-hs-upgradation','id'=>'s-hs-upgradation')) }}
              {{ $errors->first('s-hs-upgradation', '<label for="s-hs-upgradation" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->first('cwsn', 'has-error') }}">
            <label class="control-label" for='cwsn'>Special School for CWSN</label>
            {{ Form::select('cwsn', School::$CHOICE, Input::old('cwsn',
            $schoolMaster->CWSNSCH_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('cwsn', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->first('shift', ' has-error') }}">
            <label class="control-label" for='shift'>Shift Schools</label>
            {{ Form::select('shift', School::$CHOICE, Input::old('shift',
            $schoolMaster->SCHSHI_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('shift', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i>
              Required and should not be Yes </label>') }}
          </div>

          <div class="form-group">
            <label class=" control-label" >Residential School</label>
            <div class="input-group residential-primary {{ $errors->has('residential-primary') ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary</span>
              {{ Form::select('residential-primary', School::$CHOICE, Input::old('residential-primary', $schoolMaster->SCHRES_YN),
              array('class'=>'form-control', 'id'=>'residential-primary')) }}
              {{ $errors->first('residential-primary', '<label for="residential-primary" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group residential-up {{ $errors->has('residential-up') ? ' has-error' : '' }}">
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::select('residential-up', School::$CHOICE, Input::old('residential-up', $schoolMaster->SCHRESUPR_YN),
              array('class'=>'form-control', 'id'=>'residential-up')) }}
              {{ $errors->first('residential-up', '<label for="residential-up" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>

            <div class="input-group residential-secondary{{ $errors->has('residential-up') ? ' has-error' : '' }}">
              <span class="input-group-addon">Secondary</span>
              {{ Form::select('residential-secondary', School::$CHOICE, Input::old('residential-secondary', $schoolMaster->SCHRESSEC_YN),
              array('class'=>'form-control', 'id'=>'residential-secondary')) }}
              {{ $errors->first('residential-secondary', '<label for="residential-secondary" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>

            <div class="input-group residential-hs{{ $errors->has('residential-up') ? ' has-error' : '' }}">
              <span class="input-group-addon">Higher Secondary</span>
              {{ Form::select('residential-hs', School::$CHOICE, Input::old('residential-hs', $schoolMaster->SCHRESHSEC_YN),
              array('class'=>'form-control', 'id'=>'residential-hs')) }}
              {{ $errors->first('residential-hs', '<label for="residential-hs" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group depends-residential">
            <div class="input-group{{ $errors->first('residential-up', ' has-error') }}">
              <span class="input-group-addon">Type of Residential</span>
              {{ Form::select('residential-type', Master::$RESIDENTIAL_TYPES, Input::old('residential-type', $schoolMaster->RESITYPE),
              array('class'=>'form-control')) }}
              {{ $errors->first('residential-type', '<label for="residential-type" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group {{ $errors->first('religious-minority', ' has-error') }}">
            <label class="control-label" for='religious-minority'>Religious Minority School</label>
            {{ Form::select('religious-minority', School::$CHOICE, Input::old('religious-minority',
            $schoolMaster->RELMINORITY_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('religious-minority', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group depends-religious-minority {{ $errors->first('religious-minority-type', ' has-error') }}">
            <label class="control-label" for='religious-minority-type'>Religious Minority Type</label>
            {{ Form::select('religious-minority-type', Master::$RELIGIOUS_MINORITIES, Input::old('religious-minority-type',
            $schoolMaster->RELMINORITY_TYPE), array('class'=>'form-control')) }}
            {{ $errors->first('religious-minority-type', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group">
            <label class=" control-label" >Medium of instruction</label>
            <div class="input-group{{ $errors->first('medium-a', ' has-error') }}">
              <span class="input-group-addon">a</span>
              {{ Form::select('medium-a', Master::$INSTRUCTION_MEDIUMS_MANDATORY, Input::old('medium-a', $schoolMaster->MEDINSTR1),
              array('class'=>'form-control')) }}
              {{ $errors->first('medium-a', '<label for="medium-a" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group{{ $errors->first('medium-b', ' has-error') }}">
              <span class="input-group-addon">b</span>
              {{ Form::select('medium-b', Master::$INSTRUCTION_MEDIUMS, Input::old('medium-b', $schoolMaster->MEDINSTR2),
              array('class'=>'form-control')) }}
              {{ $errors->first('medium-b', '<label for="medium-b" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group{{ $errors->has('medium-c') ? ' has-error' : '' }}">
              <span class="input-group-addon">c</span>
              {{ Form::select('medium-c', Master::$INSTRUCTION_MEDIUMS, Input::old('medium-c', $schoolMaster->MEDINSTR3),
              array('class'=>'form-control')) }}
              {{ $errors->first('medium-c', '<label for="medium-c" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group{{ $errors->has('medium-d') ? ' has-error' : '' }}">
              <span class="input-group-addon">d</span>
              {{ Form::select('medium-d', Master::$INSTRUCTION_MEDIUMS, Input::old('medium-d', $schoolMaster->MEDINSTR4),
              array('class'=>'form-control')) }}
              {{ $errors->first('medium-d', '<label for="medium-d" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group depends-primary {{ $errors->has('primary-taught-in-mothertongue') ? ' has-error' : '' }}">
            <label class="control-label" for='primary-taught-in-mothertongue'>Are the majority of pupils taught through
            their mother tongue(s) at primary stage</label>
            {{ Form::select('primary-taught-in-mothertongue', School::$CHOICE, Input::old('primary-taught-in-mothertongue',
            $schoolMaster->MTONGUE_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('primary-taught-in-mothertongue', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group depends-primary primary-languages">
            <label class=" control-label">Languages taught at primary stage</label>
            <div class="input-group{{ $errors->has('lang-a') ? ' has-error' : '' }}">
              <span class="input-group-addon">a</span>
              {{ Form::select('lang-a', $languages, Input::old('lang-a',
              $schoolMaster->LANG1), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('lang-a', '<label for="lang-a" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group{{ $errors->has('lang-b') ? ' has-error' : '' }}">
              <span class="input-group-addon">b</span>
              {{ Form::select('lang-b', $languages, Input::old('lang-b', $schoolMaster->LANG2),
              array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('lang-b', '<label for="lang-b" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group{{ $errors->has('lang-c') ? ' has-error' : '' }}">
              <span class="input-group-addon">c</span>
              {{ Form::select('lang-c', $languages, Input::old('lang-c', $schoolMaster->LANG3),
              array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('lang-c', '<label for="lang-c" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

          <div class="form-group affiliation-board">
            <label class=" control-label" >Affiliation Board</label>
            <div class="input-group board-secondary{{ $errors->has('board-secondary') ? ' has-error' : '' }}">
              <span class="input-group-addon">Secondary</span>
              {{ Form::select('board-secondary', Master::$BOARDS, Input::old('board-secondary', $schoolMaster->BOARDSEC),
              array('class'=>'form-control')) }}
              {{ $errors->first('board-secondary', '<label for="board-secondary" class="control-label"><i class="fa fa-times-circle-o"></i>
                Required and should be state-board for gov, aided schools </label>') }}
            </div>
            <div class="input-group board-hs{{ $errors->has('board-hs') ? ' has-error' : '' }}">
              <span class="input-group-addon">Higher Secondary</span>
              {{ Form::select('board-hs', Master::$BOARDS, Input::old('board-hs', $schoolMaster->BOARDHSEC),
              array('class'=>'form-control')) }}
              {{ $errors->first('board-hs', '<label for="board-hs" class="control-label"><i class="fa fa-times-circle-o"></i>
                Required and should be state-board for gov, aided schools </label>') }}
            </div>
          </div>

          <div class="form-group {{ $errors->has('pre-vocational-course') ? ' has-error' : '' }} pre-vocational">
            <label class="control-label" for='pre-vocational-course'>Does the school offers any pre-vocational course(s)
            at secondary stage</label>
            {{ Form::select('pre-vocational-course', School::$CHOICE, Input::old('pre-vocational-course',
            $schoolMaster->PREVOCCOURSE_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('pre-vocational-course', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group {{ $errors->has('councelling') ? ' has-error' : '' }} pre-vocational">
            <label class="control-label" for='councelling'>Does the school provide educational and vocational guidance
              and counseling to students</label>
            {{ Form::select('councelling', School::$CHOICE, Input::old('councelling', $schoolMaster->EDUVOCGUIDE_YN),
            array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('councelling', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="form-group">
            <label class="control-label" >Geographical Information</label>
            <div class="input-group
              {{ $errors->has('latitude-degree') ? ' has-error' : '' }}
              {{ $errors->has('latitude-min') ? ' has-error' : '' }}
              {{ $errors->has('latitude-sec') ? ' has-error' : '' }}">
              <span class="input-group-addon">Latitude</span>
              <div class="col-xs-4">
                {{ Form::text('latitude-degree', Input::old('latitude-degree', $schoolMaster->LATDEG),
                array('data-parsley-type'=>'integer','class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              <div class="col-xs-4">
                {{ Form::text('latitude-min', Input::old('latitude-min', $schoolMaster->LATMIN),
                array('data-parsley-type'=>'integer', 'data-parsley-maxlength'=>'2','class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              <div class="col-xs-3">
                {{ Form::text('latitude-sec', Input::old('latitude-sec', $schoolMaster->LATSEC), array('data-parsley-type'=>'integer', 'data-parsley-trigger'=>"keyup", 'data-parsley-maxlength'=>'2','class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              {{ $errors->first('latitude-degree', '<label for="latitude-degree" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('latitude-min', '<label for="latitude-min" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('latitude-sec', '<label for="latitude-sec" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="input-group
              {{ $errors->has('longitude-degree') ? ' has-error' : '' }}
              {{ $errors->has('longitude-min') ? ' has-error' : '' }}
              {{ $errors->has('longitude-sec') ? ' has-error' : '' }}">
              <span class="input-group-addon">Longitude</span>
              <div class="col-xs-4">
                {{ Form::text('longitude-degree', Input::old('longitude-degree', $schoolMaster->LONDEG), array( 'data-parsley-trigger'=>"keyup", 'data-parsley-type'=>'integer','data-parsley-maxlength'=>'2','class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              <div class="col-xs-4">
                {{ Form::text('longitude-min', Input::old('longitude-min', $schoolMaster->LONMIN),
                array('data-parsley-type'=>'integer', 'data-parsley-trigger'=>"keyup",'class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              <div class="col-xs-3">
                {{ Form::text('longitude-sec', Input::old('longitude-sec', $schoolMaster->LONSEC), array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'2', 'data-parsley-trigger'=>"keyup",'class'=>'form-control', 'maxlength' => '2')) }}
              </div>
              {{ $errors->first('longitude-degree', '<label for="longitude-degree" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('longitude-min', '<label for="longitude-min" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              {{ $errors->first('longitude-sec', '<label for="longitude-sec" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div> <!-- col -->

  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
