@extends('backend.layouts.udise')

@section('title')
Particulars - Elementary
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-three');
  @if($rte->ACSTARTMNTH == null)
    $('[name=academic-session-start] option[value="6"]').attr("selected",true);
  @endif
  //SMC DETAILS
  $("select[name='smc-constituted']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-smc-constituted" ).show('slow');
      $( ".depends-smc-constituted select, .depends-smc-constituted input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-smc-constituted" ).hide('slow');
      $( ".depends-smc-constituted select, .depends-smc-constituted input" ).attr('data-parsley-required', 'false');
      $( ".depends-smc-constituted input, .depends-smc-constituted select" ).val('');
    }
  });

  //SMC BANK ACCOUNT
  $("select[name='smc-bank-account']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-smc-bank-account" ).show('slow');
      $( ".depends-smc-bank-account select, .depends-smc-bank-account input" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-smc-bank-account" ).hide('slow');
      $( ".depends-smc-bank-account input, .depends-smc-bank-account select" ).val('');
      $( ".depends-smc-bank-account select, .depends-smc-bank-account input" ).attr('data-parsley-required', 'false');
    }
  });

  // Text Book
  $("select[name='text-books-received']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-books-received" ).show('slow');
      $( ".depends-books-received input, .depends-books-received select" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-books-received" ).hide('slow');
      $( ".depends-books-received input, .depends-books-received select" ).val('');
      $( ".depends-books-received input, .depends-books-received select" ).attr('data-parsley-required', 'false');
    }
  });

  //SPECIAL TRAINING
  $("select[name='special-training']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-special-training" ).show('slow');
      $( ".depends-special-training input, .depends-special-training select" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-special-training" ).hide('slow');
      $( ".depends-special-training input, .depends-special-training select" ).val('');
      $( ".depends-special-training input, .depends-special-training select" ).attr('data-parsley-required', 'false');
    }
  });
  //CCE Implimentation

  $("select[name='cce-implemented']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".depends-cce" ).show('slow');
      $( ".depends-cce input, .depends-cce select" ).attr('data-parsley-required', 'true');
    } else {
      $( ".depends-cce" ).hide('slow');
      $( ".depends-cce input, .depends-cce select" ).val('');
      $( ".depends-cce input, .depends-cce select" ).attr('data-parsley-required', 'false');
    }
  });

  //PUPIL CUMULATIVE RECORD

  $("select[name='cce-record-maintained']").on('change', function(event){
    if($(this).val() == 1) {
      $( ".cce-record-shared" ).show('slow');
      $( ".cce-record-shared input, .cce-record-shared select" ).attr('data-parsley-required', 'true');
    } else {
      $( ".cce-record-shared" ).hide('slow');
      $( ".cce-record-shared input, .cce-record-shared select" ).val('');
      $( ".cce-record-shared input, .cce-record-shared select" ).attr('data-parsley-required', 'false');
    }
  });

  // order matters
  $("select[name='cce-implemented']").change();
  $("select[name='cce-record-maintained']").change();
  $("select[name='smc-constituted']").change();
  $("select[name='special-training']").change();
  $("select[name='text-books-received']").change();
  $("select[name='smc-bank-account']").change();
});
@stop

@section('content')

<div class="row">
  <div class="col-md-12">
    @foreach ($warnings as $warning)
    <div class="alert alert-warning">
      <i class="fa fa-ban"></i>
      <b>Warning!</b>
      {{ $warning->message }}
    </div>
    @endforeach
  </div>
</div>
<form role="form" method="post" action="" data-parsley-validate action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            <ul class="nav navbar-right">
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Details of instructional days and school hours</h3>
        </div>
        <div class="box-body">

          <div class="form-group {{ $errors->has('instructional-days') ? ' has-error' : '' }}">
            <label class=" control-label" for="instructional-days">Number of instructional days (previous academic year)</label>
            <div class="input-group {{ ($errors->has('primary-instructional-days') || $errors->has('up-instructional-days')) ? ' has-error' : '' }}">
              @if($master->hasPrimary())
              <span class="input-group-addon">Primary</span>
              <input id="primary-instructional-days" name="primary-instructional-days"
              placeholder="primary-instructional-days" data-parsley-range='[100,250]'
              data-parsley-error-message="School hours should be between 100 and 250"
              value="{{{ Input::old('primary-instructional-days', $rte->WORKDAYS_PR) }}}" class="form-control input-md"
              data-parsley-required='true' type="text"  data-parsley-min = '0' min = '0' >
              {{ $errors->first('primary-instructional-days', '<label for="primary-instructional-days" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasUpperPrimary())
              <span class="input-group-addon">Upper Primary</span>
              <input id="up-instructional-days" name="up-instructional-days" placeholder="up-instructional-days"
              data-parsley-range='[100,250]' data-parsley-error-message="School hours should be between 100 and 250"
              value="{{{ Input::old('up-instructional-days', $rte->WORKDAYS_UPR) }}}" class="form-control input-md"
              data-parsley-required='true' type="text" data-parsley-min = '0' min = '0' data-parsley-type = "number">
              {{ $errors->first('up-instructional-days', '<label for="up-instructional-days" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
            </div>
          </div>
          <div class="form-group">
            <label class=" control-label" for="school-hours">School hours for children (per day) [Eg:5.30 for 5hours 30minutes]</label>
            <div class="input-group {{ ($errors->has('primary-school-hours') || $errors->has('up-school-hours')) ? ' has-error' : '' }}">

              @if($master->hasPrimary())
              <span class="input-group-addon">Primary</span>
              {{ Form::text('primary-school-hours', Input::old('primary-school-hours', $rte->SCHHRSCHILD_PR),
              array('data-parsley-required'=>"true",'data-parsley-range'=>'[5,8]','data-parsley-error-message'=>"School
              hours should be between 5 and 8",'class'=>'form-control',
              'placeholder'=>'primary-school-hours', 'data-parsley-min' => '0', 'min' => '0', 'data-parsley-type'=>"number")) }}
              {{ $errors->first('primary-school-hours', '<label for="primary-school-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasUpperPrimary())
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::text('up-school-hours', Input::old('up-school-hours', $rte->SCHHRSCHILD_UPR),
              array('data-parsley-required'=>"true",'data-parsley-type'=>"number",'data-parsley-range'=>'[5,8]','data-parsley-error-message'=>"School
              hours should be between 5 and 8",'class'=>'form-control',
              'placeholder'=>'up-school-hours', 'data-parsley-min' => '0', 'min' => '0')) }}
              {{ $errors->first('up-school-hours', '<label for="up-school-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif

            </div>
          </div>
          <div class="form-group">
            <label class=" control-label" for="teacher-working-hours">Teacher working hours (per day)  [Eg:5.30 for 5hours 30minutes]</label>
            <div class="input-group {{ ($errors->has('primary-teacher-working-hours') ||
              $errors->has('up-teacher-working-hours')) ? ' has-error' : '' }}">

              @if($master->hasPrimary())
              <span class="input-group-addon">Primary</span>
              {{ Form::text('primary-teacher-working-hours', Input::old('primary-teacher-working-hours', $rte->SCHHRSTCH_PR),
              array('data-parsley-required'=>"true",'data-parsley-type'=>"number",'data-parsley-range'=>'[5,8]','data-parsley-error-message'=>"School
              hours should be between 5 and 8",'class'=>'form-control',
              'placeholder'=>'primary-teacher-working-hours', 'data-parsley-min' => '0', 'min' => '0')) }}
              {{ $errors->first('primary-teacher-working-hours', '<label for="primary-teacher-working-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif
              @if($master->hasUpperPrimary())
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::text('up-teacher-working-hours', Input::old('up-teacher-working-hours', $rte->SCHHRSTCH_UPR),
              array('data-parsley-required'=>"true",'data-parsley-type'=>"number",'data-parsley-range'=>'[5,8]','data-parsley-error-message'=>"School
              hours should be between 5 and 8",'class'=>'form-control',
              'placeholder'=>'up-teacher-working-hours', 'data-parsley-min' => '0', 'min' => '0')) }}
              {{ $errors->first('up-teacher-working-hours', '<label for="up-teacher-working-hours" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              @endif

            </div>
          </div>
          <div class="form-group cce-implemented{{ $errors->has('cce-implemented') ? ' has-error' : '' }}">
            <label class="control-label" for='cce-implemented'>Is CCE being implemented in school at elementary level
            </label>
            {{ Form::select('cce-implemented', School::$CHOICE, Input::old('cce-implemented',
            $rte->CCE_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('cce-implemented', '<label for="" class="control-label"><i class="fa
                fa-times-circle-o"></i> required and should be YES for government and aided schools </label>') }}
          </div>
          <div class="depends-cce">
            <div class="form-group cce-record-maintained{{ $errors->has('cce-record-maintained') ? ' has-error' : '' }}">
              <label class="control-label" for='cce-record-maintained'>If Yes,  Are pupil cumulative records being maintained</label>
              {{ Form::select('cce-record-maintained', School::$CHOICE, Input::old('cce-record-maintained',
              $rte->PCR_MAINTAINED), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('cce-record-maintained', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="form-group cce-record-shared{{ $errors->has('cce-record-shared') ? ' has-error' : '' }}">
              <label class="control-label" for='cce-record-shared'>If Yes,  Are pupil cumulative records shared with parents?</label>
              {{ Form::select('cce-record-shared', School::$CHOICE, Input::old('cce-record-shared',
              $rte->PCR_SHARED), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
              {{ $errors->first('cce-record-shared', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          @if ( $master->isPvtUnaided() )
            <div class="form-group">
              <label class=" control-label">Only for Private unaided schools </label>
              <div class="input-group {{ $errors->has('children-enrolled-in-RTE-quota-current-year') ? ' has-error' : '' }}">
                <label class=" control-label" for="children-enrolled-in-RTE-quota-current-year">Number of Children enrolled at entry level (Under 25% quota as per RTE) in current academic year</label>
                {{ Form::text('children-enrolled-in-RTE-quota-current-year',
                Input::old('children-enrolled-in-RTE-quota-current-year', $rte->WSEC25P_APPLIED),
                array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'children-enrolled-in-RTE-quota-current-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('children-enrolled-in-RTE-quota-current-year', '<label for="children-enrolled-in-RTE-quota-current-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
              <div class="input-group {{ $errors->has('children-enrolled-in-RTE-quota-previous-year') ? ' has-error' : '' }}">
                <label class=" control-label" for="children-enrolled-in-RTE-quota-previous-year">Number of Students continuing who got admission under 25% quota in previous years.</label>
                {{ Form::text('children-enrolled-in-RTE-quota-previous-year', Input::old('children-enrolled-in-RTE-quota-previous-year', $rte->WSEC25P_ENROLLED), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'children-enrolled-in-RTE-quota-previous-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('children-enrolled-in-RTE-quota-previous-year', '<label for="children-enrolled-in-RTE-quota-previous-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
          @endif
        </div> <!-- box body -->
      </div> <!-- box -->
    </div> <!-- col-md -->

    @if($master->hasElementary())
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">SMC details</h3>
        </div>
        <div class="box-body">
          <div class="form-group {{ $errors->first('smc-constituted', 'has-error') }}">
            <label class="control-label" for='smc-constituted'>Whether School Management Committee (SMC) has been constituted?</label>
            {{ Form::select('smc-constituted', School::$CHOICE, Input::old('smc-constituted',
            $rte->SMC_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('smc-constituted', '<label for="" class="control-label"><i class="fa
                fa-times-circle-o"></i> Required and should not be NO if government school </label>') }}
          </div>

          <div class="depends-smc-constituted">
            <div class="form-group">
              <label class=" control-label" for="SMC-details'">if yes Details of Members/Representatives </label>
              <label class=" control-label" for="no-of-smc-members'">Total number of Members in SMC</label>
              <div class="input-group {{ $errors->has('no-of-smc-members-male') || $errors->has('no-of-smc-members-female') ? ' has-error' : '' }}">
                <span class="input-group-addon">Male</span>
                {{ Form::text('no-of-smc-members-male', Input::old('no-of-smc-members-male', $rte->SMCMEM_M),
                array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-smc-members-male','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-smc-members-male', '<label for="no-of-smc-members-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                <span class="input-group-addon">Female</span>
                {{ Form::text('no-of-smc-members-female', Input::old('no-of-smc-members-female', $rte->SMCMEM_F),
                array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-smc-members-female','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-smc-members-female', '<label for="no-of-smc-members-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
            <div class="form-group">
              <label class=" control-label" for="no-of-pta-members'">Representatives of Parents/Guardians/PTA</label>
              <div class="input-group {{ $errors->has('no-of-pta-members-male') || $errors->has('no-of-pta-members-female') ? ' has-error' : '' }}">
                <span class="input-group-addon">Male</span>
                {{ Form::text('no-of-pta-members-male', Input::old('no-of-pta-members-male-male', $rte->SMSPARENTS_M),
                array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-pta-members-male','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-pta-members-male', '<label for="no-of-pta-members-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                <span class="input-group-addon">Female</span>
                {{ Form::text('no-of-pta-members-female', Input::old('no-of-pta-members-female', $rte->SMSPARENTS_F),
                array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-pta-members-female','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-pta-members-female', '<label for="no-of-pta-members-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
            <div class="form-group">
              <label class=" control-label" for="no-of-local-authority'">Representatives/nominees from local authority/local government/urban local body</label>
              <div class="input-group {{ $errors->has('no-of-local-authority-male') || $errors->has('no-of-local-authority-female') ? ' has-error' : '' }}">
                <span class="input-group-addon">Male</span>
                {{ Form::text('no-of-local-authority-male', Input::old('no-of-local-authority-male',
                $rte->SMCNOMLOCAL_M), array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-local-authority-male','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-local-authority-male', '<label for="no-of-local-authority-male" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                <span class="input-group-addon">Female</span>
                {{ Form::text('no-of-local-authority-female', Input::old('no-of-local-authority-female',
                $rte->SMCNOMLOCAL_F), array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'no-of-local-authority-female','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-of-local-authority-female', '<label for="no-of-local-authority-female" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
            <div class="form-group {{ $errors->has('smc-meetings') ? ' has-error' : '' }}">
              <label class=" control-label" for="smc-meetings">Number of meetings held by SMC during the previous academic year</label>
              <div class="input-group">
                {{ Form::text('smc-meetings', Input::old('smc-meetings', $rte->SMCMEETINGS),
                array('data-parsley-type'=>"integer",'class'=>'form-control',
                'placeholder'=>'smc-meetings','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('smc-meetings', '<label for="smc-meetings" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>

            <div class="form-group {{ $errors->has('smc-plan') ? ' has-error' : '' }}">
              <label class="control-label" for='smc-plan'>Whether SMC Prepare the School Development Plan?</label>
              {{ Form::select('smc-plan', School::$CHOICE, Input::old('smc-plan',
              $rte->SMCSDP_YN), array('class'=>'form-control')) }}
              {{ $errors->first('smc-plan', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>

            <div class="form-group smc-bank-account {{ $errors->has('smc-bank-account') ? ' has-error' : '' }}">
              <label class="control-label" for='smc-bank-account'>Whether separate bank account for SMC is being maintained?</label>
              {{ Form::select('smc-bank-account', School::$CHOICE, Input::old('smc-bank-account',
              $rte->SMCBANKAC_YN), array('class'=>'form-control')) }}
              {{ $errors->first('smc-bank-account', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
            <div class="depends-smc-bank-account">
              <div class="form-group">
                <label class=" control-label" for="smc-bank-name'">If yes, </label>
                <div class="input-group {{ $errors->has('smc-bank-name') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Bank name</span>
                  {{ Form::text('smc-bank-name', Input::old('smc-bank-name', $rte->SMCBANK), array(
                  'class'=>'form-control', 'placeholder'=>'smc-bank-name')) }}
                  {{ $errors->first('smc-bank-name', '<label for="smc-bank-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
                </div>
                <div class="input-group {{ $errors->has('smc-branch-name') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Branch</span>
                  {{ Form::text('smc-branch-name', Input::old('smc-branch-name', $rte->SMCBANKBRANCH), array(
                  'class'=>'form-control', 'placeholder'=>'smc-branch-name')) }}
                  {{ $errors->first('smc-branch-name', '<label for="smc-branch-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
                <div class="input-group {{ $errors->has('smc-acc-no') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Account No</span>
                  {{ Form::text('smc-acc-no', Input::old('smc-acc-no', $rte->SMCACNO),
                  array('data-parsley-type'=>"integer",'class'=>'form-control', 'placeholder'=>'smc-acc-no', 'data-parsley-min' => '0', 'min' => '0')) }}
                  {{ $errors->first('smc-acc-no', '<label for="smc-acc-no" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
                <div class="input-group {{ $errors->has('smc-acc-name') ? ' has-error' : '' }}">
                  <span class="input-group-addon">Account Name</span>
                  {{ Form::text('smc-acc-name', Input::old('smc-acc-name', $rte->SMCACNAME),
                  array('class'=>'form-control', 'placeholder'=>'smc-acc-name',)) }}
                  {{ $errors->first('smc-acc-name', '<label for="smc-acc-name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
                <div class="input-group {{ $errors->has('smc-ifsc-code') ? ' has-error' : '' }}">
                  <span class="input-group-addon">IFSC Code</span>
                  {{ Form::text('smc-ifsc-code', Input::old('smc-ifsc-code', $rte->IFSCCODE),
                  array('data-parsley-type'=>'alphanum','data-parsley-maxlength'=>'11','data-parsley-minlength'=>'11','data-parsley-error-message'=>'IFSC
                  code should be 11 characters!!','data-parsley-validation-threshold'=>'11',
                  'class'=>'form-control', 'placeholder'=>'smc-ifsc-code', 'data-parsley-min' => '0', 'min' => '0')) }}
                  {{ $errors->first('smc-ifsc-code', '<label for="smc-ifsc-code" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif

    <div class="col-md-6">
      <div class="box box-info box-solid">
        @if($master->isGov())
        <div class="box-header">
          <h3 class="box-title">Special Training</h3>
        </div>
        <div class="box-body">

          <div class="form-group {{ $errors->has('special-training') ? ' has-error' : '' }}">
            <label class="control-label" for='special-training'>Whether any child enrolled in school attending Special Training?</label>
            {{ Form::select('special-training', School::$CHOICE, Input::old('special-training',
            $rte->SPLTRNG_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('special-training', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>

          <div class="depends-special-training">
            <div class="form-group">
              <label class=" control-label" for="no-childeren-in-special-training">If 'Yes', Details of Special Training </label>
              <label class=" control-label" for="no-childeren-in-special-training">No. of children provided Special Training in current year (upto 30th Sep.) </label>
              <div class="input-group {{ $errors->has('no-boys-in-special-training') ? ' has-error' : '' }}">
                <span class="input-group-addon">Boys</span>
                {{ Form::text('no-boys-in-special-training', Input::old('no-boys-in-special-training',
                $rte->SPLTRG_CY_PROVIDED_B), array('data-parsley-type'=>'number',
                'class'=>'form-control', 'placeholder'=>'no-boys-in-special-training', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-boys-in-special-training', '<label for="no-boys-in-special-training" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>
              <div class="input-group {{ $errors->has('no-girls-in-special-training') ? ' has-error' : '' }}">
                <span class="input-group-addon">GIRLS</span>
                {{ Form::text('no-girls-in-special-training', Input::old('no-girls-in-special-training', $rte->SPLTRG_CY_PROVIDED_G), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'no-girls-in-special-training', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-girls-in-special-training', '<label for="no-girls-in-special-training" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>

            </div>
            <div class="form-group">

              <label class=" control-label" for="no-childeren-in-special-training-previous-year'">No. of children enrolled for special training in previous academic year</label>
              <div class="input-group {{ $errors->has('no-boys-in-special-training-previous-year') ? ' has-error' : '' }}">
                <span class="input-group-addon">Boys</span>
                {{ Form::text('no-boys-in-special-training-previous-year', Input::old('no-boys-in-special-training-previous-year', $rte->SPLTRG_PY_ENROLLED_B), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'no-boys-in-special-training-previous-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-boys-in-special-training-previous-year', '<label for="no-boys-in-special-training-previous-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>
              <div class="input-group {{ $errors->has('no-childeren-in-special-training-previous-year') ? ' has-error' : '' }}">
                <span class="input-group-addon">GIRLS</span>
                {{ Form::text('no-girls-in-special-training-previous-year', Input::old('no-girls-in-special-training-previous-year', $rte->SPLTRG_PY_ENROLLED_G), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'no-girls-in-special-training-previous-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-girls-in-special-training-previous-year', '<label for="no-girls-in-special-training-previous-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>

            </div>
            <div class="form-group">

              <label class=" control-label" for="no-childeren-completed-special-training-in-previous-year'">No. of children completed special training during previous academic year </label>
              <div class="input-group {{ $errors->has('no-childeren-completed-special-training-in-previous-year') ? ' has-error' : '' }}">
                <span class="input-group-addon">Boys</span>
                {{ Form::text('no-boys-completed-special-training-in-previous-year', Input::old('no-boys-completed-special-training-in-previous-year', $rte->SPLTRG_PY_PROVIDED_B), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'no-boys-completed-special-training-in-previous-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-boys-completed-special-training-in-previous-year', '<label for="no-boys-completed-special-training-in-previous-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>
              <div class="input-group {{ $errors->has('no-childeren-completed-special-training-in-previous-year') ? ' has-error' : '' }}">
                <span class="input-group-addon">GIRLS</span>
                {{ Form::text('no-girls-completed-special-training-in-previous-year', Input::old('no-girls-completed-special-training-in-previous-year', $rte->SPLTRG_PY_PROVIDED_G), array('data-parsley-type'=>'number','class'=>'form-control',
                'placeholder'=>'no-girls-completed-special-training-in-previous-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('no-girls-completed-special-training-in-previous-year', '<label for="no-girls-completed-special-training-in-previous-year" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}<br>
              </div>
            </div>


            <div class="form-group {{ $errors->has('special-training-conducted-by') ? ' has-error' : '' }}">
              <label class=" control-label" for="special-training-conducted-by">Who conducts the special training?</label>
              <div class="input-group">
                {{ Form::select('special-training-conducted-by', Rte::$SPLTRG_BY, Input::old('special-training-conducted-by', $rte->SPLTRG_BY), array('class'=>'form-control')) }}
                {{ $errors->first('special-training-conducted-by', '<label for="special-training-conducted-by" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>

            <div class="form-group {{ $errors->has('special-training-conducted-in') ? ' has-error' : '' }}">
              <label class="control-label" for="special-training-conducted-in">Special Training is conducted in?</label>
              <div class="input-group">
                {{ Form::select('special-training-conducted-in', Rte::$SPLTRG_PLACE, Input::old('special-training-conducted-in', $rte->SPLTRG_PLACE), array('class'=>'form-control')) }}
                {{ $errors->first('special-training-conducted-in', '<label for="special-training-conducted-in" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
            <div class="form-group {{ $errors->has('type-of-special-training') ? ' has-error' : '' }}">
              <label class="control-label" for="type-of-special-training">Type of training being conducted</label>
              <div class="input-group">
                {{ Form::select('type-of-special-training', Rte::$SPLTRG_TYPE, Input::old('type-of-special-training', $rte->SPLTRG_TYPE), array('class'=>'form-control')) }}
                {{ $errors->first('type-of-special-training', '<label for="type-of-special-training" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
          </div>

        </div> <!-- box-body -->
        @endif
      </div> <!-- box -->
    </div> <!-- col -->
    <div class="col-md-6">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h3 class="box-title">Text Books Details</h3>
        </div>
        <div class="box-body">
          <div class="form-group ">
            <label class=" control-label" >When does the academic session starts:</label>
            <div class="input-group {{ $errors->has('academic-session-start') ? ' has-error' : '' }}">
              <span class="input-group-addon">Month</span>
              {{ Form::select('academic-session-start', Rte::$MONTH, Input::old('academic-session-start', $rte->ACSTARTMNTH), array('class'=>'form-control')) }}
              {{ $errors->first('academic-session-start', '<label for="academic-session-start" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group {{ $errors->has('text-books-received') ? ' has-error' : '' }}">
            <label class="control-label" for='text-books-received'>Whether any text book received in current academic year (upto 30th September)</label>
            {{ Form::select('text-books-received', School::$CHOICE, Input::old('text-books-received',
            $rte->TXTBKRECD_YN), array('class'=>'form-control', 'data-parsley-required'=>'true')) }}
            {{ $errors->first('text-books-received', '<label for="" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          <div class="depends-books-received">
            <div class="form-group ">
              <label class=" control-label" >If yes, a) When was the text books received in the current academic year:</label>
              <div class="input-group {{ $errors->has('books-received-month') ? ' has-error' : '' }}">
                <span class="input-group-addon">Month</span>
                {{ Form::select('books-received-month', Rte::$MONTH, Input::old('books-received-month', $rte->TXTBKMNTH), array('class'=>'form-control')) }}
                {{ $errors->first('books-received-month', '<label for="books-received-month" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
              <div class="input-group {{ $errors->has('books-received-year') ? ' has-error' : '' }}">
                <span class="input-group-addon">Year</span>
                {{ Form::text('books-received-year', Input::old('books-received-year', $rte->TXTBKYEAR),
                array('data-parsley-type'=>'number','data-parsley-minlength'=>'4','maxlength'=>'4',
                'class'=>'form-control', 'placeholder'=>'pbooks-received-year', 'data-parsley-min' => '0', 'min' => '0')) }}
                {{ $errors->first('books-received-year', '<label for="books-received-yearx" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              </div>
            </div>
          </div>


          <div class="form-group ">
            <legend>Availability of free Text Books, Teaching Learning Equipment (TLE) and play material (in current academic year</legend>
            <label class=" control-label" >Whether complete set of free text books received</label>
            <div class="input-group {{ ($errors->has('is-primary-books-received') ||
              $errors->has('is-up-books-received')) ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary</span>
              {{ Form::select('is-primary-books-received', Material::$FREETXTBK, Input::old('is-primary-books-received',
              $materialBook->C1), array('class'=>'form-control')) }}
              {{ $errors->first('is-primary-books-received', '<label for="is-primary-books-received" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::select('is-up-books-received', Material::$FREETXTBK, Input::old('is-up-books-received',
              $materialBook->C2), array('class'=>'form-control')) }}
              {{ $errors->first('is-up-books-received', '<label for="is-up-books-received" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group ">
            <label class=" control-label" >Whether TLE available for each grade</label>
            <div class="input-group {{ ($errors->has('tle-available-primary') || $errors->has('tle-available-up')) ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary</span>
              {{ Form::select('tle-available-primary', Material::$FREETXTBK, Input::old('tle-available-primary',
              $materialTle->C1), array('class'=>'form-control')) }}
              {{ $errors->first('tle-available-primary', '<label for="tle-available-primary" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::select('tle-available-up', Material::$FREETXTBK, Input::old('tle-available-up', $materialTle->C2), array('class'=>'form-control')) }}
              {{ $errors->first('tle-available-up', '<label for="tle-available-up" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>
          <div class="form-group ">
            <label class=" control-label" >Whether play material, games and sports equipment available for each grade</label>
            <div class="input-group {{ $errors->has('play-material-available') ? ' has-error' : '' }}">
              <span class="input-group-addon">Primary</span>
              {{ Form::select('play-material-available-primary', Material::$FREETXTBK,
              Input::old('play-material-available-primary', $materialSports->C1), array('class'=>'form-control')) }}
              {{ $errors->first('play-material-available-primary', '<label for="play-material-available-primary" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
              <span class="input-group-addon">Upper Primary</span>
              {{ Form::select('play-material-available-up', Material::$FREETXTBK,
              Input::old('play-material-available-up', $materialSports->C2), array('class'=>'form-control')) }}
              {{ $errors->first('play-material-available-up', '<label for="play-material-available-up" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
            </div>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div> <!-- row -->
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>

</form>
@stop
