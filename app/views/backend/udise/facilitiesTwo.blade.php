@extends('backend.layouts.udise')

@section('title')
Facilities Two
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.facilities-two')
});
@stop

@section('content')

<form role="form" method="post" action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Physical Facilities and Equipment</h3>
        </div>
        <div class="box-body">

            <table class="table">
              <thead>
                <tr>
                  <th class="col-xs-8"> facility </th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
              @foreach($group1 as $key => $facility)
              <tr>
                <td>{{ $facility->description }}</td>
                <td>
                  {{ Form::select('facility-'.$facility->id, School::$CHOICE_NA,
                  Input::old('facility-'.$facility->id, $groupValue1[$key]->ITEMVALUE), array('class'=>'form-control','maxlength'=>'3')) }}
                  {{ $errors->first('facility-'.$facility->id,
                  '<label class="control-label"><i class="fa fa-times-circle-o"></i>correct this</label>') }}
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>


        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->

    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Facilities </h3>
        </div>
        <div class="box-body">
            <table class="table">
              <thead>
                <tr>
                  <th> working/usable condition </th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
          @foreach($group3 as $key => $facility)
              <tr>
                <td>{{ $facility->description }}</td>
                <td>
                  {{ Form::select('facility-'.$facility->id, Facility::$FACILITY_STATUS,
                  Input::old('facility-'.$facility->id, $groupValue3[$key]->ITEMVALUE), array('class'=>'form-control','maxlength'=>'3')) }}
                  {{ $errors->first('facility-'.$facility->id,
                  '<label class="control-label"><i class="fa fa-times-circle-o"></i>correct this</label>') }}
                </td>
              </tr>
          @endforeach
              </tbody>
            </table>
        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div> <!-- row -->

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Facilities </h3>
        </div>
        <div class="box-body">
          <table class="table">
            <thead>
              <tr>
                <th> Type of Laboratory </th>
                <th> Seperate room available </th>
                <th> Present condition </th>
              </tr>
            </thead>
            <tbody>
            @foreach($group2 as $key => $facility)
            <tr>
              <td>{{ $facility->description }}</td>
              <td>
                {{ Form::select('facility-'.$facility->id, School::$CHOICE_NA,
                Input::old('facility-'.$facility->id, $groupValue2[$key]->ITEMVALUE), array('class'=>'form-control','maxlength'=>'3')) }}
                {{ $errors->first('facility-'.$facility->id,
                '<label class="control-label"><i class="fa fa-times-circle-o"></i>correct this</label>') }}
              </td>
              <td>
                {{ Form::select('facility-status-'.$facility->id, Facility::$FACILITY_STATUS,
                Input::old('facility-status-'.$facility->id, $groupValue2[$key]->ITEMVALUE1), array('class'=>'form-control','maxlength'=>'3')) }}
                {{ $errors->first('facility-status-'.$facility->id,
                '<label class="control-label"><i class="fa fa-times-circle-o"></i>correct this</label>') }}
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
