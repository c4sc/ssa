@extends('backend.layouts.udise')

@section('title')
Streams Available
@stop

@section('script')
$(function() {
  activate('.secondaryGroup', '.streams-available')
});
@stop

@section('content')

<form role="form" method="post" action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Availability of Streams in the school</h3>
        </div>
        <div class="box-body">
          @foreach(Facility::$STREAMS as $streamId => $stream)
          <div class="form-group {{ $errors->has("stream-$streamId") ? ' has-error' : '' }}">
            <label class="control-label" for="{{ "stream-$streamId" }}">{{ $stream }}</label>
              {{ Form::select("stream-$streamId", School::$CHOICE_NA, Input::old("stream-$streamId",
              $streamData[($streamId-1)]->ITEMVALUE), array('class'=>'form-control', 'id'=>"stream-$streamId")) }}

              {{ $errors->first("stream-$streamId", '<label for="stream-$streamId" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
          @endforeach
        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop


