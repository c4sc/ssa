@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.secondaryGroup', '.repeaters-by-stream')

  function sum(stream_id, std, gender) {
    total = 0;
    $('.'+gender+'-'+stream_id+'-'+std).each(function() {
      total += parseInt($(this).val()) || 0;
    });
    $('.total-'+gender+'-'+stream_id+'-'+std).html(total);
    classtotal = (parseInt($('.total-boys-'+stream_id+'-'+std).html()) || 0) + (parseInt($('.total-girls-'+stream_id+'-'+std).html()) || 0);
    $('.classtotal-'+stream_id+'-'+std).html(classtotal);
  }
  @foreach($streams as $streamId)
  @foreach(range(11,12) as $class)

  sum({{$streamId}}, {{ $class }}, 'boys');
  sum({{$streamId}}, {{ $class }}, 'girls');
  $('.boys-{{$streamId}}-{{$class}}').keyup(function() {
    sum({{$streamId}}, {{ $class }}, 'boys');
  });
  $('.girls-{{$streamId}}-{{$class}}').keyup(function() {
    sum({{$streamId}}, {{ $class }}, 'girls');
  });
  @endforeach
  @endforeach
});
@stop
@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Repeaters by Stream</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th colspan="2">XI</th>
                  <th colspan="2">XII</th>
                </tr>
                <tr>
                  <th>Stream</th>
                  <th>Social Category</th>
                  <th>Boys</th>
                  <th>Girls</th>
                  <th>Boys</th>
                  <th>Girls</th>
                </tr>
              </thead>
              <tbody>

              <?php $counter = 0; ?>
              @foreach($streams as $streamId)
              <tr>
                <td rowspan="5">{{ Facility::$STREAMS[$streamId] }}</td>
              </tr>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <tr>
                <td>{{ $category }}</td>
                <td>
                  {{ Form::text("XI-boys-$streamId-$categoryId", Input::old("XI-boys-$streamId-$categoryId",
                  $streamData[$counter]->RC11_B),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$streamId-11",'maxlength'=>'3')) }}
                </td>
                <td>
                  {{ Form::text("XI-girls-$streamId-$categoryId", Input::old("XI-girls-$streamId-$categoryId",
                  $streamData[$counter]->RC11_G),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control girls-$streamId-11",'maxlength'=>'3')) }}
                </td>
                <td>
                  {{ Form::text("XII-boys-$streamId-$categoryId", Input::old("XII-boys-$streamId-$categoryId",
                  $streamData[$counter]->RC12_B),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$streamId-12",'maxlength'=>'3')) }}
                </td>
                <td>
                  {{ Form::text("XII-girls-$streamId-$categoryId", Input::old("XII-girls-$streamId-$categoryId",
                  $streamData[$counter]->RC12_G),
                  array('data-parsley-type'=>"integer", 'min' => '0', 'data-parsley-min' => '0','data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control girls-$streamId-12",'maxlength'=>'3')) }}
                </td>
              </tr>
              <?php $counter++; ?>
              @endforeach

              <tr>
                  <th></th>
                  <th>Total</th>
                  @foreach(range(11, 12) as $class)
                  <th class="total-boys-{{"$streamId-$class"}}">
                  </th>
                  <th class="total-girls-{{"$streamId-$class"}}">
                  </th>
                  @endforeach
                </tr>
                <tr>
                  <th></th>
                  <th>Class strength</th>
                  @foreach(range(11, 12) as $class)
                  <td colspan="2" class="classtotal-{{"$streamId-$class"}}"></td>
                  @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop



