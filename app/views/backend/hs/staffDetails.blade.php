@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
@stop

@section('script')
$(function() {
  activate('.fundGroup', '.staff')
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">Number of non-teaching/administrative and support staff sanctioned and in-position</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <table class="table enrolment table-bordered">
              <thead>
                <th>&nbsp;</th>
                <th>No. of sanctioned staff</th>
                <th>No. of staff in-position</th>
              </thead>
              <tbody>
              @foreach(Staff::$POSTS as $postId => $post)
              <tr>
                <th>{{ $post }}</th>
                <td>
                  <div class="input-control {{ $errors->has("sanctioned-$postId") ? ' has-error' : '' }}">
                    {{ Form::text("sanctioned-$postId", Input::old("sanctioned-$postId", $staffData[$postId-1]->TOTSAN),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                    'data-parsley-trigger'=>'keyup', 'class'=>'form-control','maxlength'=>'3',
                    'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                <td>
                  <div class="input-control {{ $errors->has("in-post-$postId") ? ' has-error' : '' }}">
                  {{ Form::text("in-post-$postId", Input::old("in-post-$postId", $staffData[$postId-1]->TOTPOS),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>'form-control','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop

