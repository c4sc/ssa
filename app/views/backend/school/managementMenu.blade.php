@if(Sentry::hasAccess('blockAdmin'))
<li class="treeview schoolGroup">
<a>
  <i class="fa fa-th"></i> <span>Schools</span>
</a>
<ul class="treeview-menu">
  @if(Sentry::hasAccess('stateAdmin'))
  <li class='new-school'><a href="{{ route('new-school') }}"><i class="fa fa-angle-double-right"></i> New</a></li>
  @endif
  <li class='schools'><a href="{{ route('schools') }}"><i class="fa fa-angle-double-right"></i> Browse</a></li>
</ul>
</li>
@endif

