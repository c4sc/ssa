@extends('backend.layouts.school')

@section('title')
  @if ($school->SCHCD)
    Edit School
  @else
    New School
  @endif
@stop

@section('script')
$(function(){

  activate('.schoolGroup', '.new-school'),

  $("#block").remoteChained("#district", "/api/blocks.json");
  $("#cluster").remoteChained("#block", "/api/clusters.json");
  $("#village").remoteChained("#block", "/api/villages.json");
  $("#panchayath").remoteChained("#block", "/api/panchayaths.json");
});
@stop


@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('main-page')
SchoolManagement
@stop

@section('content')
<div class="box box-primary">
  <form class="form-horizontal" method="post" action="">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div class="box-body">
      <fieldset>

        <div class="form-group {{ $errors->has('schoolCode') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="name">School Code</label>
          <div class="col-md-4">
            <input id="schoolCode" maxlength="11" name="schoolCode" placeholder="School Code" class="form-control input-md" required=""
            type="text" value="{{ Input::old('schoolCode', $school->SCHCD) }}"
            {{ $school->SCHCD?'disabled="disabled"':'' }}>
            {{ $errors->first('schoolCode', '<label for="schoolCode" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="name">School Name</label>
          <div class="col-md-4">
            <input id="name" name="name" placeholder="School Name" class="form-control input-md" required=""
            type="text" value="{{{ Input::old('name', $school->SCHNAME) }}}">
            {{ $errors->first('name', '<label for="name" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('rural_or_urban') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="rural_or_urban">Rural Or Urban</label>
          <div class="col-md-4">
            {{ Form::select('rural_or_urban', School::$AREA, Input::old('rural_or_urban', $school->RURURB), array('class'=>'form-control', 'id'=>'rural_or_urban')) }}
            {{ $errors->first('rural_or_urban', '<label for="rural_or_urban" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('district') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="district">District</label>
          <div class="col-md-4">
            {{ Form::select('district', $districts, Input::old('district', $school->DISTCD),
            array('class'=>'form-control', 'id'=>'district')) }}
            {{ $errors->first('district', '<label for="district" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('block') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="block">Block</label>
          <div class="col-md-4">
            {{ Form::select('block', $blocks, Input::old('block', $school->BLKCD), array('class'=>'form-control', 'id'=>'block')) }}
            {{ $errors->first('block', '<label for="block" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('village') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="village">Village</label>
          <div class="col-md-4">
            {{ Form::select('village', $villages, Input::old('village', $school->VILCD), array('class'=>'form-control', 'id'=>'village')) }}
            {{ $errors->first('village', '<label for="village" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group {{ $errors->has('cluster') ? ' has-error' : '' }}">
          <label class="col-md-4 control-label" for="cluster">Cluster</label>
          <div class="col-md-4">
            {{ Form::select('cluster', $clusters, Input::old('cluster', $school->CLUCD), array('class'=>'form-control', 'id'=>'cluster')) }}
            {{ $errors->first('cluster', '<label for="cluster" class="control-label"><i class="fa fa-times-circle-o"></i> :message </label>') }}
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="save-school"></label>
          <div class="col-md-4">
            <button id="save-school" class="btn btn-primary">Save</button>
          </div>
        </div>
      </fieldset>
    </div>

  </form>
</div>
@stop

