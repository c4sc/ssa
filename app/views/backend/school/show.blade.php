@extends('backend.layouts.dashboard')

@section('title')
School
@stop

@section('path')
@stop

@section('main-page')
Schools
@stop

@section('script')
$(function() {
  activate('.schoolGroup', '.show-school');
});
@stop

@section('menu')
@include('backend.school.menu')
@stop

@section('content')
<div class="box box-primary">
  <div class="box-body">
    <ul class="list1 list-unstyled">
      <li><span> School Code </span> {{{ $school->SCHCD }}} </li>
      <li><span>School Name</span> {{{ $school->SCHNAME }}} </li>
      <li><span>Block</span> {{{ $school->BLKCD }}} </li>
      </li>
      @if(Sentry::getUser()->scope != AuthorizedController::$SCOPE_SCHOOL)
      <li><span>Users</span>
        @if(count($users))
          @foreach($users as $user)
            <a href="{{{ route('school-user-show', $user->id) }}}">{{{ $user->email }}}</a>
          @endforeach
        @else
          No users found
        @endif
        </li>
      @endif
      </ul>
    </ul>
    <div class="box-body">
      @if(Sentry::getUser()->scope == AuthorizedController::$SCOPE_STATE && Sentry::getUser()->hasAccess('schoolAdmin'))
        <a class="btn btn-danger" href="#"> Delete School</a>
        <a class="btn btn-warning" href="{{{ route('edit-school', $school->SCHCD) }}}"> Edit </a>
      @endif
    </div>
  </div>
</div>
@stop



