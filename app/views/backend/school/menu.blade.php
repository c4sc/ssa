@if((Sentry::getUser()->scope != AuthorizedController::$SCOPE_SCHOOL))
<li class="treeview schoolGroup">
<a>
  <i class="fa fa-th"></i> <span>School</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='show-school'><a href="{{ route('show-school', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>Home</a></li>
  <li class='udise-status'><a href="{{ route('udise-status', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>UDISE</a></li>
  <li class=''><a href="{{ route('students', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>Adise</a></li>
</ul>
</li>
@endif
