@extends('backend.layouts.school')

@section('title')
 Browse School
@stop

@section('path')
@stop

@section('main-page')
Schools
@stop

@section('script')
$(function() {
  activate('.schoolGroup', '.schools');
});
@stop

@section('content')
<div class="box box-primary">
  <div class="box-body">
    <form action="" method="get" class="form-horizontal form">
      <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <input type="text" name="q" id="q" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default"
                type="submit">Search!</button>
            </span>
          </div>
        </div>
      </div>
    </form>

    @if($schoolCount)
    <table class="table">
      <thead>
        <th>School Code</th>
        <th>Name</th>
        <th>Type</th>
        <th>Management</th>
        <th>Category</th>
      </thead>
      <tbody>
      @foreach ($schools as $school)
      <?php $statusList=SchoolStatus::filterSchool($statics, $school->SCHCD); ?>
      <tr>
        <td>
          <a href="{{ URL::Route('show-school', $school->SCHCD ) }}" class="name">{{{ $school->SCHCD }}}</a>
        </td>
        <td>{{ $school->SCHNAME }}
          <div class="row-fluid" style="display:inline">
            @foreach($statusList as $title => $value)
              <div class="tick" style="diplay:inline">
                <label class="badge pull-left">{{ $value->route }}</span>
                </label>
              </div>
            @endforeach
          </div>

        </td>
        <td>{{ choiceLabel($school->SCHTYPE, School::$TYPES) }}</td>
        <td>{{ choiceLabel($school->SCHMGT, School::$MANAGEMENTS) }}</td>
        <td>{{ choiceLabel($school->SCHCAT, School::$CATEGORIES) }}</td>
      </tr>
      @endforeach
      </tbody>
    </table>

    <div>
      {{ $schools->appends(array('q' => $query))->links() }}
    </div>
    @elseif(@$query)
    <br>
    <div class='alert alert-danger'>
      No results found, please try again.
    </div>
    @endif
  </div>
</div>
@stop


