@extends('backend.layouts.report')

@section('title')
Enrolment Comparison
@stop

@section('script')
$(function() {
  activate('.reportComparativeGroup', '.report-comparison-enrolment');
});
@stop

@section('content')
<div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th rowspan="2">Block Name</th>
        <th colspan="2">Schools</th>
        <th colspan="2">Enrolment Boys (1-8)</th>
        <th colspan="2">Enrolment Girls (1-8)</th>
        <th colspan="2">Total</th>
        <th colspan="2">Classrooms</th>
      </tr>
      <tr>
        <th>{{ $pyRange }}</th>
        <th>{{ $cyRange }}</th>
        <th>{{ $pyRange }}</th>
        <th>{{ $cyRange }}</th>
        <th>{{ $pyRange }}</th>
        <th>{{ $cyRange }}</th>
        <th>{{ $pyRange }}</th>
        <th>{{ $cyRange }}</th>
        <th>{{ $pyRange }}</th>
        <th>{{ $cyRange }}</th>
      </tr>
    </thead>
    <tbody>
    <?php $currentDistrict = ''; ?>
    @for ($i=0; $i<count($currentYear); $i++)
    <tr>
      @if ($currentDistrict != $currentYear[$i]->distname)
      <?php
      $currentDistrict = $currentYear[$i]->distname;
      ?>
      <th colspan="11">{{ $currentDistrict }}</th>
    </tr>
    <tr>
      @endif
      <td>{{ $previousYear[$i]->blkname }}</td>
      <td>{{ $previousYear[$i]->num_schools }}</td>
      <td>{{ $currentYear[$i]->num_schools }}</td>
      <td>{{ $previousYear[$i]->boys_total }}</td>
      <td>{{ $currentYear[$i]->boys_total }}</td>
      <td>{{ $previousYear[$i]->girls_total }}</td>
      <td>{{ $currentYear[$i]->girls_total }}</td>
      <td>{{ $previousYear[$i]->boys_total + $previousYear[$i]->girls_total }}</td>

      <td>{{ $currentYear[$i]->boys_total + $currentYear[$i]->girls_total }}</td>
      <td>{{ $previousYear[$i]->class_rooms }}</td>
      <td>{{ $currentYear[$i]->class_rooms }}</td>
    </tr>
    @endfor
    </tbody>
  </table>
</div>
@stop

