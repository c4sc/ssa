<li class="treeview reportEnrolmentGroup">
  <a>
    <i class="fa fa-th"></i> <span>Enrolment Reports</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <li class='report-enrolment-class-conditions'><a href="{{ route('report-enrolment-class-conditions') }}"><i class="fa fa-angle-double-right"></i>
      Class Conditions</a></li>
  </ul>
</li>

<li class="treeview reportComparativeGroup">
  <a>
    <i class="fa fa-th"></i> <span>Comparative</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <li class='report-comparison-enrolment'><a href="{{ route('report-comparison-enrolment') }}">
      <i class="fa fa-angle-double-right"></i> Enrolment</a></li>
    <li class='report-comparison-enrolment-age-medium-caste'><a href="{{ route('report-comparison-enrolment-age-medium-caste') }}">
      <i class="fa fa-angle-double-right"></i> Enrolment by Age Medium Caste</a></li>
  </ul>
</li>

<li class="treeview reportSchoolsGroup">
  <a>
    <i class="fa fa-th"></i> <span>Schools</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <li class='report-schools-having-facilities'><a href="{{ route('report-schools-having-facilities') }}">
      <i class="fa fa-angle-double-right"></i> Having Facilities</a></li>
    <li class='report-schools-with-no-response'><a href="{{ route('report-schools-with-no-response') }}">
      <i class="fa fa-angle-double-right"></i> No Response In Data</a></li>
  </ul>
</li>

<li class="treeview reportDataEntryGroup">
  <a>
    <i class="fa fa-th"></i> <span>Data entry status</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <?php
    $scope = Sentry::getUser()->scope;
    $scopeId = Sentry::getUser()->scope_id;
    ?>
    @if ($scope == AuthorizedController::$SCOPE_STATE)
    <li class='report-data-entry-state'>
      <a href="{{ route('report-data-entry-state') }}"><i class="fa fa-angle-double-right"></i>
      State Level</a></li>
    @endif
    @if ($scope == AuthorizedController::$SCOPE_DISTRICT)
    <li class='report-data-entry-district'>
      <a href="{{ route('report-data-entry-district', $scopeId) }}"><i class="fa fa-angle-double-right"></i>
      District Level</a></li>
    @endif
    @if ($scope == AuthorizedController::$SCOPE_BLOCK)
    <li class='report-data-entry-block'>
      <a href="{{ route('report-data-entry-block', $scopeId) }}"><i class="fa fa-angle-double-right"></i>
      Block Level</a></li>
    @endif
  </ul>
</li>
