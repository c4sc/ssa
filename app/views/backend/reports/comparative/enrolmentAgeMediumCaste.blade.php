@extends('backend.layouts.report')

@section('title')
Enrolment by Age, Medium, Caste
@stop

@section('script')
$(function() {
  activate('.reportComparativeGroup', '.report-comparison-enrolment-age-medium-caste');
});
@stop

@section('content')
<table class="table table-bordered">
  <thead>
    <tr>
      <th rowspan="2">&nbsp;</th>
      <th colspan="2">Class I</th>
      <th colspan="2">Class II</th>
      <th colspan="2">Class III</th>
      <th colspan="2">Class IV</th>
      <th colspan="2">Class V</th>
      <th colspan="2">Class VI</th>
      <th colspan="2">Class VII</th>
      <th colspan="2">Class VIII</th>
    </tr>
    <tr>
      @for($j=1; $j<=8; $j++)
      <th>Boys</th>
      <th>Girls</th>
      @endfor
    </tr>
  </thead>
  <tbody>
  <?php $currentDistrict = '';
  $blocksCount = count($ageData);
  ?>
  @for ($i=0; $i < $blocksCount ; $i++)
  <tr>
    @if ($currentDistrict != $ageData[$i]->distname)
    <?php
    $currentDistrict = $ageData[$i]->distname;
    ?>
    <th colspan="17">{{ $currentDistrict }}</th>
  </tr>
  <tr>
    @endif
    <th colspan="17">{{ $ageData[$i]->blkname }}</th>
  </tr>
  <tr>
    <td>by Age</td>
    @for($j=1; $j<=8; $j++)
    <td>{{ $ageData[$i]->{"c{$j}_boys_total"} }}</td>
    <td>{{ $ageData[$i]->{"c{$j}_girls_total"} }}</td>
    @endfor
  </tr>
  <tr>
    <td>by Medium</td>
    @for($j=1; $j<=8; $j++)
    <td>{{ $mediumData[$i]->{"c{$j}_boys_total"} }}</td>
    <td>{{ $mediumData[$i]->{"c{$j}_girls_total"} }}</td>
    @endfor
  </tr>
  <tr>
    <td>by Caste</td>
    @for($j=1; $j<=8; $j++)
    <td>{{ $casteData[$i]->{"c{$j}_boys_total"} }}</td>
    <td>{{ $casteData[$i]->{"c{$j}_girls_total"} }}</td>
    @endfor
  </tr>
  @endfor
  </tbody>
</table>
@stop


