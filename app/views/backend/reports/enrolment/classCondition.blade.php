@extends('backend.layouts.report')

@section('title')
Enrolment Comparison - Class Conditions
@stop

@section('style')
<style>
.table th {
    text-align: center;
}
</style>
@stop

@section('script')
$(function() {
  activate('.reportEnrolmentGroup', '.report-enrolment-class-conditions');
});
@stop

@section('content')
<div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th style="vertical-align: middle" rowspan=3 colspan=3>Block Code & Block Name</th>
        <th colspan=5>Enrolment</th>
        <th colspan=12>Classrooms by Condition</th>
      </tr>
      <tr>
        <th colspan=3>Enrolment (1-8)</th>
        <th colspan=2>Enrolment (9-12)</th>
        <th colspan=4>Elementary classes 1 to 8</th>
        <th colspan=4>Secondary classes 9 & 10</th>
        <th colspan=4>Hr. Sec. classes 11 & 12</th>
      </tr>
      <tr>
        <th>Boys</th>
        <th>Girls</th>
        <th>Total (1-8)</th>
        <th>Boys</th>
        <th>Girls</th>
        <th>Total</th>
        <th>Good</th>
        <th>Major</th>
        <th>Minor</th>
        <th>Total</th>
        <th>Good</th>
        <th>Major repair</th>
        <th>Minor repair</th>
        <th>Total</th>
        <th>Good</th>
        <th>Major repair</th>
        <th>Minor repair</th>
      </tr>
    </thead>
    <tbody>
    <?php $currentDistrict = ''; ?>
    @for ($i=0; $i<count($data); $i++)
    <tr>
      @if ($currentDistrict != $data[$i]->distname)
      <?php
      $currentDistrict = $data[$i]->distname;
      ?>
      <td colspan="17">{{ $data[$i]->distcd  }} <strong>{{ $currentDistrict }}</strong></td>
    </tr>
    <tr>
      @endif
      <td>{{ $i+1 }}</td>
      <td>{{ $data[$i]->blkcd }}</td>
      <td>{{ $data[$i]->blkname }}</td>
      <td>{{ $data[$i]->boys_elementary }}</td>
      <td>{{ $data[$i]->girls_elementary }}</td>
      <td>{{ $data[$i]->boys_elementary + $data[$i]->girls_elementary }}</td>
      <td>{{ $data[$i]->boys_9_to_12 }}</td>
      <td>{{ $data[$i]->girls_9_to_12 }}</td>
      <td>{{ $data[$i]->good_elementary + $data[$i]->major_elementary +
        $data[$i]->minor_elementary }}</td>
      <td>{{ $data[$i]->good_elementary }}</td>
      <td>{{ $data[$i]->major_elementary }}</td>
      <td>{{ $data[$i]->minor_elementary }}</td>
      <td>{{ $data[$i]->good_secondary + $data[$i]->major_secondary + $data[$i]->minor_secondary }}</td>
      <td>{{ $data[$i]->good_secondary }}</td>
      <td>{{ $data[$i]->major_secondary }}</td>
      <td>{{ $data[$i]->minor_secondary }}</td>
      <td>{{ $data[$i]->good_hs + $data[$i]->major_hs + $data[$i]->minor_hs }}</td>
      <td>{{ $data[$i]->good_hs }}</td>
      <td>{{ $data[$i]->major_hs }}</td>
      <td>{{ $data[$i]->minor_hs }}</td>
    </tr>
    @endfor
    </tbody>
  </table>
</div>
@stop
