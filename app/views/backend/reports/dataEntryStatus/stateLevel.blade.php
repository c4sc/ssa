@extends('backend.layouts.report')

@section('title')
Data entry status - state level
@stop

@section('script')
$(function() {
  activate('.reportDataEntryGroup', '.report-data-entry-state');
});
@stop

@section('content')
<table class="table table-bordered">
  <thead>
    <tr>
      <th>District Name</th>
      <th>Total Schools</th>
      <th>Schools logged-in</th>
      <th>Schools saved first form</th>
    </tr>
  </thead>
  <tbody>
    @foreach($stateLevelStatistics as $district)
    <tr>
      <td><a href="{{ route('report-data-entry-district', $district->distcd) }}">{{ $district->distname }}</td>
      <td> {{ $district->total_schools }}</td>
      <td> {{ $district->schools_logged_in }}</td>
      <td> {{ $district->schools_save_first_form }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@stop

