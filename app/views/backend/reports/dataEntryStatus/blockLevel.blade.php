@extends('backend.layouts.report')

@section('title')
@parent
Data entry status - Block level
@stop

@section('script')
$(function() {
  activate('.reportDataEntryGroup', '.report-data-entry-block');
});
@stop

@section('content')
  @if($schoolCount)
    <table class="table">
      <thead>
        <th>School Code</th>
        <th>Name</th>
        <th>Type</th>
        <th>Management</th>
        <th>Category</th>
      </thead>
      <tbody>
      @foreach ($schools as $school)
      <?php $statusList=SchoolStatus::filterSchool($statics, $school->SCHCD); ?>
      <tr>
        <td>
          <a href="{{ URL::Route('show-school', $school->SCHCD ) }}" class="name">{{{ $school->SCHCD }}}</a>
        </td>
        <td>{{ $school->SCHNAME }}
          <div class="row-fluid" style="display:inline">
            @foreach($statusList as $title => $value)
              <div class="tick" style="diplay:inline">
                <label class="badge pull-left">{{ $value->route }}</span>
                </label>
              </div>
            @endforeach
          </div>

        </td>
        <td>{{ choiceLabel($school->SCHTYPE, School::$TYPES) }}</td>
        <td>{{ choiceLabel($school->SCHMGT, School::$MANAGEMENTS) }}</td>
        <td>{{ choiceLabel($school->SCHCAT, School::$CATEGORIES) }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
@stop

