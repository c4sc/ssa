@extends('backend.layouts.report')

@section('title')
@parent
Data entry status - District level
@stop

@section('script')
$(function() {
  activate('.reportDataEntryGroup', '.report-data-entry-district');
});
@stop

@section('content')
<table class="table table-bordered">
  <thead>
    <tr>
      <th>Block Name</th>
      <th>Total Schools</th>
      <th>Schools logged-in</th>
      <th>Schools saved first form</th>
    </tr>
  </thead>
  <tbody>
    @foreach($districtLevelStatistics as $block)
    <tr>
      <td> <a href = '{{ route('report-data-entry-block', $block->blkcd)}}'> {{ $block->blkname }} </a></td>
      <td> {{ $block->total_schools }}</td>
      <td> {{ $block->schools_logged_in }}</td>
      <td> {{ $block->schools_save_first_form }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@stop

