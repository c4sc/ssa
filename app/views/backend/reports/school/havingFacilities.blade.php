@extends('backend.layouts.report')

@section('title')
Number of Schools Having Facilities
@stop

@section('script')
$(function() {
  activate('.reportSchoolsGroup', '.report-schools-having-facilities');
});
@stop

@section('content')
<div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Block</th>
        <th>School</th>
        <th>Builiding</th>
        <th>School with Classroom</th>
        <th>Boys Toilet</th>
        <th>Girls Toilet</th>
        <th>Toilet for CWSN</th>
        <th>Hand wasing facility</th>
        <th>water</th>
        <th>Electricity</th>
        <th>Play Ground</th>
        <th>Ramps</th>
        <th>Medical Checkup</th>
        <th>Computer</th>
        <th>Library</th>
        <th>Kitchen Shed</th>
        <th>Class as Kitchen Shed</th>
      </tr>
    </thead>
    <tbody>
    <?php $currentDistrict = ''; ?>
    @foreach ($data as $block)
    <tr>
      @if ($block->distname != $currentDistrict)
      <?php
      $currentDistrict = $block->distname;
      ?>
      <th colspan="17">{{ $currentDistrict }}</th>
    </tr>
    <tr>
      @endif
      <td>{{ $block->blkname }}</td>
      <td>{{ $block->total_schools }}</td>
      <td>{{ $block->buildings }}</td>
      <td>{{ $block->class_rooms }}</td>
      <td>{{ $block->toilet_boys }}</td>
      <td>{{ $block->toilet_girls }}</td>
      <td>{{ $block->toilet_cwsn }}</td>
      <td>{{ $block->handwash }}</td>
      <td>{{ $block->water }}</td>
      <td>{{ $block->electric }}</td>
      <td>{{ $block->playground }}</td>
      <td>{{ $block->ramps }}</td>
      <td>{{ $block->medical_checkup }}</td>
      <td>{{ $block->computer }}</td>
      <td>{{ $block->library }}</td>
      <td>{{ $block->kitchen_shed }}</td>
      <td>{{ $block->class_as_kitchen_shed }}</td>
    </tr>
    @endforeach
    </tbody>
  </table>
</div>
@stop


