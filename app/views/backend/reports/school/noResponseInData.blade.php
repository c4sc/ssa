@extends('backend.layouts.report')

@section('title')
Number of Schools with no response in data items
@stop

@section('script')
$(function() {
  activate('.reportSchoolsGroup', '.report-schools-with-no-response');
});
@stop

@section('content')
<div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Block</th>
        <th>Total Schools</th>
        <th>School Category</th>
        <th>Rural Urban</th>
        <th>MDM Status</th>
        <th>Kitchen Device</th>
        <th>Electricity</th>
        <th>Book</th>
        <th>Playground</th>
        <th>Ramps</th>
        <th>Medical checkup</th>
        <th>Furniture for Elementary</th>
        <th>Boundary</th>
        <th>Kitchen</th>
        <th>Source of Water</th>
      </tr>
    </thead>
    <tbody>
    <?php $currentDistrict = ''; ?>
    @foreach ($data as $block)
    <tr>
      @if ($block->distname != $currentDistrict)
      <?php
      $currentDistrict = $block->distname;
      ?>
      <th colspan="15">{{ $currentDistrict }}</th>
    </tr>
    <tr>
      @endif
      <td>{{ $block->blkname }}</td>
      <td>{{ $block->total_schools }}</td>
      @if($block->total_schools > 0)
      <td>{{ $block->school_category }}</td>
      <td>{{ $block->rural_urban }}</td>
      <td>{{ $block->mdm }}</td>
      <td>{{ $block->kitchen_device }}</td>
      <td>{{ $block->electricity }}</td>
      <td>{{ $block->textbook }}</td>
      <td>{{ $block->playground }}</td>
      <td>{{ $block->ramp }}</td>
      <td>{{ $block->medical_checkup }}</td>
      <td>{{ $block->furniture_elementary }}</td>
      <td>{{ $block->boundary_wall }}</td>
      <td>{{ $block->kitchen_shed }}</td>
      <td>{{ $block->water }}</td>
      @else
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      @endif
    </tr>
    @endforeach
    </tbody>
  </table>
</div>
@stop
