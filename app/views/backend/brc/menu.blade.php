@if (isset($master->SCHCD) && Sentry::getUser()->scope == AuthorizedController::$SCOPE_BLOCK)
<li class="treeview brc-school">
<a>
  <i class="fa fa-th"></i> <span>School</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='show-school'><a href="{{ route('show-school', $master->SCHCD) }}"><i class="fa
      fa-angle-double-right"></i>Home</a></li>
  <li class='pdf-1-download'><a href="{{ route('pdf-1-download', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>Pdf 1</a></li>
  <li class='pdf-2-download'><a href="{{ route('pdf-2-download', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>Pdf 2</a></li>
</ul>
</li>
<li class="treeview particularsGroup">
<a>
  <i class="fa fa-th"></i> <span>UDISE</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='particulars-one'><a href="{{ route('particulars-one', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> School Particulars </a></li>
  <li class='particulars-two'><a href="{{ route('particulars-two', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Particulars - Schools
    with Pre Primary </a></li>
  @if($master->hasElementary())
  <li class='particulars-three'><a href="{{ route('particulars-three', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Particulars -
    Elementary</a></li>
  @endif
  @if($master->hasSecondary() || $master->hasHigherSecondary())
  <li class='particulars-four'><a href="{{ route('particulars-four', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> SMDC Details</a></li>
  <li class='facilities-two'><a href="{{ route('facilities-two', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i>
    Facilities - Secondary & Higher Secondary</a></li>
  @endif
  <li class='facilities-one'><a href="{{ route('facilities-one', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Physical facilities and
    Equipment </a></li>
  @if( $master->isGov() || $master->isGovAided())
  <li class='mdm'><a href="{{ route('mdm', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Mid day meal </a></li>
  @endif
  <li class='teachers'><a href="{{ route('teachers', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Teachers</a></li>
</ul>
</li>

<li class="treeview enrolmentGroup">
<a>
  <i class="fa fa-th"></i> <span>Enrolment</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  @if($master->LOWCLASS == 1)
  <li class='first-grade-admission'><a href="{{ route('first-grade-admission', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> First Grade
    Admission</a></li>
  @endif
  <li class="enrolment-by-category"><a href="{{ route('enrolment-by-category', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Enrolment by Social Category</a></li>
  <li class="enrolment-minority-category"><a href="{{ route('enrolment-minority-category', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Enrolment by Social Category - Minority</a></li>
  <li class="enrolment-by-medium"><a href="{{ route('enrolment-by-medium', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Enrolment by Medium of Instruction</a></li>
  </li>
</ul>
</li>

<li class="treeview enrolmentAgeGroup">
<a>
  <i class="fa fa-th"></i> <span>Enrolment by Age</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  @if($master->hasPrimary())
  <li class="enrolmentAgeLp"><a href="{{ route('enrolment-by-age', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> LP Section</a></li>
  @endif
  @if($master->hasUpperPrimary())
  <li class="enrolmentAgeUp"><a href="{{ route('enrolment-by-age-up', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> UP Section</a></li>
  @endif
  @if($master->hasSecondary())
  <li class="enrolmentAgeHs"><a href="{{ route('enrolment-by-age-hs', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Secondary</a></li>
  @endif
  @if($master->hasHigherSecondary())
  <li class="enrolmentAgeSs"><a href="{{ route('enrolment-by-age-ss', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Higher Secondary</a></li>
  @endif
</ul>
</li>

<li class="treeview repeatersGroup">
<a>
  <i class="fa fa-th"></i> <span>Repeaters</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='repeaters-by-category'><a href="{{ route('repeaters-by-category', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> By Social Category</a></li>
  <li class='repeaters-minority-category'><a href="{{ route('repeaters-minority-category', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Minority Category</a></li>
</ul>
</li>

<li class="treeview cwsnGroup">
<a>
  <i class="fa fa-th"></i> <span>Children with Special Needs</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  @if($master->hasPrimary())
  <li class='children-with-special-needs'><a href="{{ route('children-with-special-needs', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> LP Section</a></li>
  @endif
  @if($master->hasUpperPrimary())
  <li class='children-with-special-needs-up'><a href="{{ route('children-with-special-needs-up', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> UP Section</a></li>
  @endif
  @if($master->hasSecondary())
  <li class='children-with-special-needs-hs'><a href="{{ route('children-with-special-needs-hs', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Secondary</a></li>
  @endif
  @if($master->hasHigherSecondary())
  <li class='children-with-special-needs-hss'><a href="{{ route('children-with-special-needs-hss', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Higher Secondary</a></li>
  @endif
</ul>
</li>

<li class="treeview facilityGroup">
<a>
  <i class="fa fa-th"></i> <span>Facilities Provided </span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='facilities-cwsn'><a href="{{ route('facilities-cwsn', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> To (CWSN)-Children with Special Needs</a></li>
  @if($master->hasPrimary())
  <li class='facilities-to-children-primary'><a href="{{ route('facilities-to-children-primary', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> To Primary Classes</a></li>
  @endif
  @if($master->hasUpperPrimary())
  <li class='facilities-to-children-up'><a href="{{ route('facilities-to-children-up', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> To Upper Primary Classes</a></li>
  @endif
</ul>
</li>

@if($master->hasHigherSecondary() || $master->hasSecondary())
<li class="treeview secondaryGroup">
<a>
  <i class="fa fa-th"></i> <span>Secondary &amp; Higher Secondary</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  <li class='x-result-by-category'><a href="{{ route('x-result-by-category', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> X Results by category</a></li>
  <li class='result-by-score-x'><a href="{{ route('result-by-score-x', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> X Results by score</a></li>
  @if($master->hasHigherSecondary())
  <li class='streams-available'><a href="{{ route('streams-available', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Streams available</a></li>
  <li class='enrolment-by-stream'><a href="{{ route('enrolment-by-stream', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Enrolment by Stream</a></li>
  <li class='repeaters-by-stream'><a href="{{ route('repeaters-by-stream', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> Repeaters by Stream</a></li>
  <li class='result-by-score-xii'><a href="{{ route('result-by-score-xii', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> XII Results by score</a></li>
  <li class='result-by-stream'><a href="{{ route('result-by-stream', $master->SCHCD) }}">
    <i class="fa fa-angle-double-right"></i> XII Results by stream</a></li>
  @endif
</ul>
</li>
@endif

<li class="treeview fundGroup">
<a>
  <i class="fa fa-th"></i> <span>Fund and Staff</span> <small class="badge pull-right bg-green"></small>
</a>
<ul class="treeview-menu">
  @if( $master->isGov() || $master->isGovAided())
  <li class='rmsa-fund'><a href="{{ route('rmsa-fund', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> RMSA Fund</a></li>
  @endif
  <li class='staff'><a href="{{ route('staff', $master->SCHCD) }}"><i class="fa fa-angle-double-right"></i> Staff Details</a></li>
</ul>
</li>
@endif
