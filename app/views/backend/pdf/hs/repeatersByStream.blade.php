@section('script')
$(function() {

  function sumRepeatersStream(stream_id, std, gender) {
    total = 0;
    $('.rs-'+gender+'-'+stream_id+'-'+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      $('.total-rs-'+gender+'-'+stream_id+'-'+std).html(total);
    }
    classtotal = (parseInt($('.total-rs-boys-'+stream_id+'-'+std).html()) || 0) + (parseInt($('.total-rs-girls-'+stream_id+'-'+std).html()) || 0);
    if(classtotal != 0) {
      $('.classtotal-rs-'+stream_id+'-'+std).html(classtotal);
    }
  }
  @foreach($streams as $streamId)
  @foreach(range(11,12) as $class)

  sumRepeatersStream({{$streamId}}, {{ $class }}, 'boys');
  sumRepeatersStream({{$streamId}}, {{ $class }}, 'girls');
  @endforeach
  @endforeach
});

@parent
@stop

@section('content')
@if($master->hasHigherSecondary())
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="6">Repeaters by Stream</th>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <th>&nbsp;</th>
      <th colspan="2">XI</th>
      <th colspan="2">XII</th>
    </tr>
    <tr>
      <th>Stream</th>
      <th>Social Category</th>
      <th>Boys</th>
      <th>Girls</th>
      <th>Boys</th>
      <th>Girls</th>
    </tr>
  </thead>
  <tbody>

  <?php $counter = 0; ?>
  @foreach($streams as $streamId)
  <tr>
    <td rowspan="5">{{ Facility::$STREAMS[$streamId] }}</td>
  </tr>
  @foreach(Enrolment::$CATEGORY as $categoryId => $category)
  <tr>
    <td>{{ $category }}</td>
    <td class="rs-boys-{{$streamId}}-11">{{ $streamEnrolmentData[$counter]->RC11_B }}</td>
    <td class="rs-girls-{{$streamId}}-11">{{ $streamEnrolmentData[$counter]->RC11_G }}</td>
    <td class="rs-boys-{{$streamId}}-12">{{ $streamEnrolmentData[$counter]->RC12_B }}</td>
    <td class="rs-girls-{{$streamId}}-12">{{ $streamEnrolmentData[$counter]->RC12_G }}</td>
  </tr>
  <?php $counter++; ?>
  @endforeach

  <tr>
    <th></th>
    <th>Total</th>
    @foreach(range(11, 12) as $class)
    <th class="total-rs-boys-{{"$streamId-$class"}}"></th>
    <th class="total-rs-girls-{{"$streamId-$class"}}"></th>
    @endforeach
  </tr>
  <tr>
    <th></th>
    <th>Class strength</th>
    @foreach(range(11, 12) as $class)
    <td colspan="2" class="classtotal-rs-{{"$streamId-$class"}}"></td>
    @endforeach
  </tr>
  @endforeach
  </tbody>
</table>
@endif
@parent
@stop
