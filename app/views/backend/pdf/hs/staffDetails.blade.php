@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="3">Number of non-teaching/administrative and support staff sanctioned and in-position</th>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <th>No. of sanctioned staff</th>
      <th>No. of staff in-position</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Staff::$POSTS as $postId => $post)
  <tr>
    <th>{{ $post }}</th>
    <td>{{ $staffData[$postId-1]->TOTSAN }}</td>
    <td>{{ $staffData[$postId-1]->TOTPOS }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop

