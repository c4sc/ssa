@section('content')
@if($master->hasHigherSecondary())
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="2">Availability of Streams in the school</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Facility::$STREAMS as $streamId => $stream)
  <tr>
    <td>{{ $stream }}</td>
    <td>{{ choiceLabel($streamData[$streamId-1]->ITEMVALUE, $choice) }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
@endif
@parent
@stop
