@extends('backend.layouts.pdf')

@section('style')
<style>
#wrapper{
  width:750px;
  margin:0 auto;
  overflow:hidden;
}
</style>
@stop

@section('title')
DCF
@stop

@section('content')
<div class="table clearfix center-block table-pdf">
    SSA KERALA - DCF
</div>
@parent
@stop

@include('backend.pdf.udiseStyled.particularsOne')
@include('backend.pdf.udiseStyled.particularsTwo')
@include('backend.pdf.udiseStyled.particularsThree')
@include('backend.pdf.udiseStyled.smdc')
@include('backend.pdf.udiseStyled.facilitiesOne')
@include('backend.pdf.udiseStyled.facilitiesTwo')
@include('backend.pdf.udiseStyled.mdm')
@include('backend.pdf.enrolmentStyled.firstGradeAdmission')
@include('backend.pdf.enrolmentStyled.enrolmentBySocialCategory')
@include('backend.pdf.enrolmentStyled.enrolmentBySocialCategoryMinority')
@include('backend.pdf.enrolmentStyled.enrolmentByMedium')
@include('backend.pdf.enrolmentStyled.repeatersBySocialCategory')
@include('backend.pdf.enrolmentStyled.repeatersBySocialCategoryMinority')
@include('backend.pdf.enrolmentStyled.facilitiesCwsn')
@include('backend.pdf.enrolmentStyled.facilitiesToChildren')
@include('backend.pdf.result.examResults')
@include('backend.pdf.hs.streamsAvailable')
@include('backend.pdf.hs.enrolmentByStream')
@include('backend.pdf.hs.repeatersByStream')
@include('backend.pdf.hs.staffDetails')
@include('backend.pdf.result.xResultsByCategory')
@include('backend.pdf.result.resultByScore')
@include('backend.pdf.result.resultByStream')
@include('backend.pdf.rmsa.fund')
@include('backend.pdf.teacherList')
@include('backend.pdf.teacher')
