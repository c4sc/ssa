@section('script')
$(function() {
  function sumFund(head) {
    var total = 0;
    $('.'+head).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      $('.fund-total-'+head).html(total);
    }
  }
  sumFund('received');
  sumFund('utilized');
  sumFund('spillovers');
});
@parent
@stop

@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="4">Receipts and Expenditure - Under RMSA</th>
    </tr>
    <tr>
      <th>Details of school level grants</th>
      <th>Grants Recieved</th>
      <th>Grants Utilized</th>
      <th>Spillovers as on 1st April, {{ date('Y') }}</th>
    </tr>
  </thead>
  <tbody>
  <tr>
    <th>Civil Works</th>
    <td class="received">{{ $fundData[0]->AMT_R }}</td>
    <td class="utilized">{{ $fundData[0]->AMT_U }}</td>
    <td class="spillovers">{{ $fundData[0]->AMT_S }}</td>
  </tr>
  <tr>
    <th colspan="4">Annual School Grants (recurring)</th>
  </tr>
  @foreach(RmsaFund::$SCHOOL_GRANT_HEADS as $itemId => $item)
  <tr>
    <td>{{ $item }}</td>
    <td class="received">{{ $fundData[($itemId-1)]->AMT_R }}</td>
    <td class="utilized">{{ $fundData[($itemId-1)]->AMT_U }}</td>
    <td class="spillovers">{{ $fundData[($itemId-1)]->AMT_S }}</td>
  </tr>
  @endforeach
  <tr>
    <th>Others</th>
    <td class="received">{{ $fundData[5]->AMT_R }}</td>
    <td class="utilized">{{ $fundData[5]->AMT_U }}</td>
    <td class="spillovers">{{ $fundData[5]->AMT_S }}</td>
  </tr>
  <tr>
    <th>Total</th>
    <th class="fund-total-received"></th>
    <th class="fund-total-utilized"></th>
    <th class="fund-total-spillovers"></th>
  </tr>
  </tbody>
</table>
@parent
@stop
