@if( ! empty($classes))
@section('script')
$(function() {
  function elementaryResultSum(gender, class_id, head) {
    head_sum = 0
    $('.elementary-results-'+gender+'-'+class_id+'-'+head).each(function(){
      head_sum += parseInt($(this).html()) || 0
    });
    if(head_sum > 0) {
      $('.total-'+gender+'-'+class_id+'-'+head).html(head_sum)
    }
  }

  @foreach($classes as $class)
  @foreach(array('B'=>'Boys', 'G'=>'Girls') as $gender)
  @foreach(Result::$RESULT_HEADS as $headId => $head)
  elementaryResultSum('{{ $gender }}', {{ $class['id'] }}, {{ $headId }});
  @endforeach
  @endforeach
  @endforeach
});
@stop

@section('content')
<?php
$categoryRowCount = count($classes) * 2;
?>
<table class="enrolment table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan='7'>Examination Results (Last Academic Year)</th>
    </tr>
    <tr>
      <th>Category</th>
      <th>Grade</th>
      <th>Gender</th>
      @foreach(Result::$RESULT_HEADS as $headId => $head)
      <th>{{ $head }}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  @foreach(Result::$CATEGORY_LIST as $cat)
  <tr>
    <th rowspan='{{ $categoryRowCount }}'>{{ $cat['name'] }}</th>
  <?php $i = 1; ?>
  @foreach($classes as $class)
  @if($i != 1)
  <tr>
  @endif
    <th rowspan='2'>{{ $class['name'] }}</th>
    <?php $i++; $j = 1; ?>
    @foreach(array('B'=>'Boys', 'G'=>'Girls') as $genderId => $gender)
  @if($j != 1)
  <tr>
  @endif
    <th>{{ $gender }}</th>
    @foreach(Result::$RESULT_HEADS as $headId => $head)
    <td class="{{ "elementary-results-$gender-{$class['id']}-$headId" }}">
      {{ $resultsElementary[($headId-1)]->{"C{$class['id']}{$genderId}_{$cat['abbr']}"} }}
    </td>
    @endforeach
  </tr>
  <?php $j++; ?>
  @endforeach
  @endforeach
  @endforeach
  <tr>
    <th rowspan='{{ $categoryRowCount }}'>Total</th>
  <?php $i = 1; ?>
  @foreach($classes as $class)
  @if($i != 1)
  <tr>
  @endif
    <th rowspan='2'>{{ $class['name'] }}</th>
    <?php $i++; $j = 1; ?>
    @foreach(array('B'=>'Boys', 'G'=>'Girls') as $genderId => $gender)
  @if($j != 1)
  <tr>
  @endif
    <th>{{ $gender }}</th>
    @foreach(Result::$RESULT_HEADS as $headId => $head)
    <td class="{{ "total-$gender-{$class['id']}-$headId" }}">
    </td>
    @endforeach
  </tr>
  <?php $j++; ?>
  @endforeach
  @endforeach
  </tbody>
</table>
@parent
@stop
@endif
