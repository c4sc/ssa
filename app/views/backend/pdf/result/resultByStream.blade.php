@section('content')
@if($master->hasHigherSecondary())
<table class="table enrolment table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="12">Results of class XII Board/University examination in previous academic year</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <th colspan="{{ count($streams) }}">Number of students appeared</th>
      <th colspan="{{ count($streams) }}">Number of students passed</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      @foreach($streams as $key => $stream)
      <th>{{ Facility::$STREAMS[$stream] }}</th>
      @endforeach
      @foreach($streams as $key => $stream)
      <th>{{ Facility::$STREAMS[$stream] }}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  @foreach(Result::$CATEGORY_LIST as $categoryId => $category)
  <tr>
    <th rowspan="2">{{ $category['name'] }}</th>
    <th>Boys</th>
    @foreach($streams as $key => $stream)
    <td>{{ $streamResults[$key]->{"C10B_{$category['abbr']}"} }}</td>
    <td>{{ $streamResults[$key]->{"C12B_{$category['abbr']}"} }}</td>
    @endforeach
  </tr>
  <tr>
    <th>Girls</th>
    @foreach($streams as $key => $stream)
    <td>{{ $streamResults[$key]->{"C10G_{$category['abbr']}"} }}</td>
    <td>{{ $streamResults[$key]->{"C12G_{$category['abbr']}"} }}</td>
    @endforeach
  </tr>
  @endforeach
  </tbody>
</table>
@endif
@parent
@stop
