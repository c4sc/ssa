@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="5">Result of class X examination for previous academic year</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <th colspan="2">Number of students appeared</th>
      <th colspan="2">Number of students passed/qualified</th>
    </tr>
    <tr>
      <th>Category</th>
      <th>Boys</th>
      <th>Girls</th>
      <th>Boys</th>
      <th>Girls</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Enrolment::$CATEGORY as $categoryId => $category)
  <tr>
    <th>{{ $category }}</th>
    <td>{{ $xResults[($categoryId-1)]->C10B_GEN }}</td>
    <td>{{ $xResults[($categoryId-1)]->C10G_GEN }}</td>
    <td>{{ $xResults[($categoryId-1)]->C10B_SC }}</td>
    <td>{{ $xResults[($categoryId-1)]->C10G_SC }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
