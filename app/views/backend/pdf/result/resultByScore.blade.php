@section('script')
$(function() {
  $('.boys, .girls').each(function() {
    var ids = this.id.split('-')
    gender = ids[0]
    std = ids[1]
    category = ids[2]
    score = ids[3]
    category_sum = score_sum = 0
    $('.'+gender+'-'+std+'-category-'+category).each(function(){
      category_sum += parseInt($(this).html()) || 0
    });
    $('.'+gender+'-'+std+'-score-'+score).each(function(){
      score_sum += parseInt($(this).html()) || 0
    });
    if(score_sum != 0) {
      $('.total-'+gender+'-'+std+'-score-'+score).html(score_sum)
    }
    if(category_sum != 0) {
      $('.total-'+gender+'-'+std+'-category-'+category).html(category_sum)
    }
  });
});

@parent
@stop

@section('content')
@foreach($scoreResults as $key => $results)
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="7">{{ $resultLabel[$key]['label'] }}</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      @foreach(Result::$SCORE as $scoreId => $score)
      <th>{{ $score }}</th>
      @endforeach
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Result::$CATEGORY_LIST as $categoryId => $category)
  <tr>
    <th rowspan="2">{{ $category['name'] }}</th>
    <th>Boys</th>
    @foreach(Result::$SCORE as $scoreId => $score)
    <td class="boys boys-{{ $resultLabel[$key]['class'] }}-score-{{$scoreId}} boys-{{ $resultLabel[$key]['class'] }}-category-{{$categoryId}}" id="boys-{{ $resultLabel[$key]['class'] }}-{{$categoryId}}-{{$scoreId}}">
      {{ $results[($scoreId-1)]->{"C10B_{$category['abbr']}"} }}
    </td>
    @endforeach
    <th class="total-boys-{{ $resultLabel[$key]['class'] }}-category-{{ $categoryId }}">&nbsp;</th>
  </tr>
  <tr>
    <th>Girls</th>
    @foreach(Result::$SCORE as $scoreId => $score)
    <td class="girls girls-{{ $resultLabel[$key]['class'] }}-score-{{$scoreId}} girls-{{ $resultLabel[$key]['class'] }}-category-{{$categoryId}}" id="girls-{{ $resultLabel[$key]['class'] }}-{{$categoryId}}-{{$scoreId}}">
      {{ $results[($scoreId-1)]->{"C10G_{$category['abbr']}"} }}
    </td>
    @endforeach
    <th class="total-girls-{{ $resultLabel[$key]['class'] }}-category-{{ $categoryId }}">&nbsp;</th>
  </tr>
  @endforeach
  <tr>
    <th rowspan="2">Total</th>
    <th>Boys</th>
    @foreach(Result::$SCORE as $scoreId => $score)
    <th class="total-boys-{{ $resultLabel[$key]['class'] }}-score-{{ $scoreId }}">&nbsp;</th>
    @endforeach
  </tr>
  <tr>
    <th>Girls</th>
    @foreach(Result::$SCORE as $scoreId => $score)
    <th class="total-girls-{{ $resultLabel[$key]['class'] }}-score-{{ $scoreId }}">&nbsp;</th>
    @endforeach
  </tr>
  </tbody>
</table>
@endforeach
@parent
@stop


