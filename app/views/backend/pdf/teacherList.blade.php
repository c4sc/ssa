@section('content')
@if(count($teachers))
<table class="table-bordered table-pdf table">
  <thead>
    <tr>
      <th colspan="6">Teachers</th>
    </tr>
    <tr>
      <th rowspan="2" class="col-xs-2">Name</th>
      <th class="col-xs-2">Class taught</th>
      <th class="col-xs-2">Category</th>
      <th class="col-xs-2">Email</th>
      <th class="col-xs-2">Mobile</th>
      <th class="col-xs-2">Appointment</th>
    </tr>
    <tr>
      <th class="col-xs-2">Pen</th>
      <th class="col-xs-2">Year of Join</th>
      <th class="col-xs-2">Subject</th>
      <th class="col-xs-2">Sub taught</th>
      <th class="col-xs-2">ICT Trained</th>
    </tr>
  </thead>
  <tbody>
  @foreach($teachers as $teacher)
  <tr>
    <td rowspan="2">{{ $teacher->TCHNAME }}</td>
    <td>{{ Teacher::$CLASSES_TAUGHT[$teacher->CLS_TAUGHT] }}</td>
    <td>{{ choiceLabel($teacher->CATEGORY, Teacher::$TYPE_CATEGORY) }}</td>
    <td>{{ $teacher->EMAIL }}</td>
    <td>{{ $teacher->MOBILE }}</td>
    <td>{{ choiceLabel($teacher->POST_STATUS,Teacher::$APPOINTMENT_NATURE) }}</td>
  </tr>
  <tr>
    <td>{{ $teacher->PENNO }}</td>
    <td>{{ $teacher->YOJ }}</td>
    <td>{{ $teacher->APPOINTSUB }}</td>
    <td>{{ $teacher->MAIN_TAUGHT1.' , '.$teacher->MAIN_TAUGHT2 }}</td>
    <td>{{ choiceLabel($teacher->SUPVAR3, $choice) }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
@endif
@parent
@stop
