@section('content')
@if($master->hasPrimary() || $master->hasUpperPrimary())
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4">Details of instructional days and school hours</th>
  </tr>
  <tr>
    <th class="col-xs-3" colspan="1">Section</th>
    <th class="col-xs-3">No. of instructional days(previous academic year)</th>
    <th class="col-xs-3">school hours for children(per day)</th>
    <th class="col-xs-3">Teacher working hours(per day)</th>
  </tr>
  @if($master->hasPrimary())
  <tr>
    <td>{{ shapeIt('Primary ') }}</td>
    <td colspan="1">{{ shapeIt($rte->WORKDAYS_PR) }}</td>
    <td colspan="1">{{ shapeIt($rte->SCHHRSCHILD_PR) }}</td>
    <td colspan="1">{{ shapeIt($rte->SCHHRSTCH_PR) }}</td>
  </tr>
  @endif
  @if($master->hasUpperPrimary())
  <tr>
    <td>{{ shapeIt('Upper Primary ') }}</td>
    <td colspan="1">{{ shapeIt($rte->WORKDAYS_UPR) }}</td>
    <td colspan="1">{{ shapeIt($rte->SCHHRSCHILD_UPR) }}</td>
    <td colspan="1">{{ shapeIt($rte->SCHHRSTCH_UPR) }}</td>
  </tr>
  </tbody>
</table>
  @endif
@endif
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="3">{{ shapeIt('Is CCE being implemented in school at elementary level ') }}</th>
    <th class="col-xs-3" colspan="1">{{ shapeIt($choice[$rte->CCE_YN], 1) }}</th>
  </tr>
  @if($rte->CCE_YN != 2)
  <tr>
    <td colspan="3">{{ shapeIt('If Yes,  Are pupil cumulative records being maintained') }}</td>
    <td colspan="1">{{ shapeIt($choice[$rte->PCR_MAINTAINED], 1) }}</td>
  </tr>
  @if($rte->PCR_MAINTAINED != 2)
  <tr>
    <td colspan="3">{{ shapeIt('If Yes,  Are pupil cumulative records shared with parents') }}</td>
    <td colspan="1">{{ shapeIt($choice[$rte->PCR_SHARED], 1) }}</td>
  </tr>
  @endif
  @endif
  <tr>
    <th colspan="4">Only for Private unaided schools</th>
  </tr>
  <tr>
    <td colspan="3">{{ shapeIt('Number of Children enrolled at entry level (Under 25% quota as per RTE) in current
      academic year') }}</td>
    <td colspan="1">{{ shapeIt($rte->WSEC25P_APPLIED) }}</td>
  </tr>
  <tr>
    <td colspan="3">{{ shapeIt('Number of Students continuing who got admission under 25% quota in previous years.') }}</td>
    <td colspan="1">{{ shapeIt($rte->WSEC25P_ENROLLED) }}</td>
  </tr>
  </tbody>
</table>

<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4">SMC details</th>
  </tr>
  <tr>
    <td colspan="3">{{ shapeIt('Whether School Management Committee (SMC) has been constituted') }}</td>
    <td colspan="1" class="col-xs-3">{{ shapeIt($choice[$rte->SMC_YN]) }}</td>
  </tr>
  @if($rte->SMC_YN != 2)
  <tr>
    <th colspan="4">if yes Details of Members/Representatives</th>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <th colspan="1" class="col-xs-3">Male</th>
    <th colspan="1">Female</th>
  </tr>
  <tr>
    <td colspan="2">Total number of Members in SMC</td>
    <td colspan="1">{{ shapeIt($rte->SMCMEM_M) }}</td>
    <td colspan="1">{{ shapeIt($rte->SMCMEM_F) }}</td>
  </tr>
  <tr>
    <td colspan="2">Representatives of Parents/Guardians/PTA</td>
    <td colspan="1">{{ shapeIt($rte->SMSPARENTS_M)}}</td>
    <td colspan="1">{{ shapeIt($rte->SMSPARENTS_F)}}</td>
  </tr>
  <tr>
    <td colspan="2">Representatives/nominees from local authority/local government/urban local body</td>
    <td colspan="1">{{ shapeIt($rte->SMCNOMLOCAL_M) }}</td>
    <td colspan="1">{{ shapeIt($rte->SMCNOMLOCAL_F) }}</td>
  </tr>
  <tr>
    <td colspan="2">Number of meetings held by SMC during the previous academic year</td>
    <td colspan="2">{{ shapeIt($rte->SMCMEETINGS) }}</td>
  </tr>
  <tr>
    <td colspan="2">Whether SMC Prepare the School Development Plan?</td>
    <td colspan="2">{{ shapeIt($choice[$rte->SMCSDP_YN]) }}</td>
  </tr>
  <tr>
    <td colspan="2">Whether separate bank account for SMC is being maintained</td>
    <td colspan="2">{{ shapeIt($choice[$rte->SMCBANKAC_YN]) }}</td>
  </tr>
  @if($rte->SMCBANKAC_YN != 2)
  <tr>
    <td colspan="2"><strong>if yes,</strong> <span class="pull-right">Bank Name</span></td>
    <td colspan="2">{{ shapeIt($rte->SMCBANK) }}</td>
  </tr>
  <tr>
    <td colspan="2">{{ shapeIt('Branch: &nbsp;').shapeIt($rte->SMCBANKBRANCH) }}</td>
    <td colspan="2">{{ shapeIt('Account No.: &nbsp;').shapeIt($rte->SMCACNO, 15) }}</td>
  </tr>
  <tr>
    <td colspan="2">{{ shapeIt('Account Name: &nbsp;').shapeIt($rte->SMCACNAME) }}</td>
    <td colspan="2">{{ shapeIt('IFSC code: &nbsp;').shapeIt($rte->IFSCCODE, 11) }}</td>
  </tr>
  @endif
  @endif
  @if($master->isGov())
  <tr>
    <th colspan="4">Special Training</th>
  </tr>
  <tr>
    <td colspan="2">Whether any child enrolled in school attending Special Training</td>
    <td colspan="2">{{ shapeIt($choice[$rte->SPLTRNG_YN], 1) }}</td>
  </tr>
  @if($rte->SPLTRNG_YN != 2)
  <tr>
    <th colspan="4">If 'Yes', Details of Special Training</th>
  </tr>
  <tr>
    <th colspan="2">&nbsp;</th>
    <th colspan="1">Boys</th>
    <th colspan="1">Girls</th>
  </tr>
  <tr>
    <td colspan="2">No. of children provided Special Training in current year (upto 30th Sep.) </td>
    <td colspan="1">{{ $rte->SPLTRG_CY_PROVIDED_B }}</td>
    <td colspan="1">{{ $rte->SPLTRG_CY_PROVIDED_G }}</td>
  </tr>
  <tr>
    <td colspan="2">No. of children enrolled for special training in previous academic year</td>
    <td colspan="1">{{ $rte->SPLTRG_PY_ENROLLED_B }}</td>
    <td colspan="1">{{ $rte->SPLTRG_PY_ENROLLED_G }}</td>
  </tr>
  <tr>
    <td colspan="2">No. of children completed special training during previous academic year</td>
    <td colspan="1">{{ $rte->SPLTRG_PY_PROVIDED_B }}</td>
    <td colspan="1">{{ $rte->SPLTRG_PY_PROVIDED_G }}</td>
  </tr>
  <tr>
    <td colspan="2">{{ shapeIt('Who conducts the special training: &nbsp;').shapeIt($rte->SPLTRG_BY, 1) }}</td>
    <td colspan="2">{{ shapeIt('Special Training is conducted in: &nbsp;').shapeIt($rte->SPLTRG_PLACE, 1) }}</td>
  </tr>
  <tr>
    <td colspan="2">Type of training being conducted</td>
    <td colspan="2">{{ shapeIt($rte->SPLTRG_TYPE, 1) }}</td>
  </tr>
  @endif
  @endif
  <tr>
    <th colspan="4">Text Book Details</th>
  </tr>
  <tr>
    <td colspan="2">When does the academic session starts:</td>
    <td colspan="2">{{ shapeIt($rte->ACSTARTMNTH, 2) }}</td>
  </tr>
  <tr>
    <td colspan="2">Whether any text book received in current academic year (upto 30th September)</td>
    <td colspan="2">{{ shapeIt($choice[$rte->TXTBKRECD_YN], 1) }}</td>
  </tr>
  @if($rte->TXTBKRECD_YN != 2)
  <tr>
    <td colspan="2">If yes, When was the text books received in the current academic year:</td>
    <td colspan="1">{{ shapeIt('Month: &nbsp;').shapeIt($rte->TXTBKMNTH, 2) }}</td>
    <td colspan="1">{{ shapeIt('Year: &nbsp;').shapeIt($rte->TXTBKYEAR, 2) }}</td>
  </tr>
  @endif
  @if($master->hasPrimary() || $master->hasUpperPrimary())
  <tr>
    <th colspan="4">Availability of free Text Books, Teaching Learning Equipment (TLE) and play material (in current
      academic year</th>
  </tr>
  <?php
  $colspan1 = ( ! $master->hasUpperPrimary())?2:1;
  $colspan2 = ( ! $master->hasPrimary())?2:1;
  ?>
  <tr>
    <th colspan="2">&nbsp;</th>
    @if($master->hasPrimary())
    <th colspan="{{ $colspan1 }}">Primary</th>
    @endif
    @if($master->hasUpperPrimary())
    <th colspan="{{ $colspan2 }}">Upper Primary</th>
    @endif
  </tr>

  <tr>
    <td colspan="2">Whether complete set of free text books received</td>
    @if($master->hasPrimary())
    <td colspan="{{ $colspan1 }}">{{ shapeIt($choice[$materialBook->C1]) }}</td>
    @endif
    @if($master->hasUpperPrimary())
    <td colspan="{{ $colspan2 }}">{{ shapeIt($choice[$materialBook->C2]) }}</td>
    @endif
  </tr>

  <tr>
    <td colspan="2">Whether TLE available for each grade</td>
    @if($master->hasPrimary())
    <td colspan="{{ $colspan1 }}">{{ shapeIt($choice[$materialTle->C1]) }}</td>
    @endif
    @if($master->hasUpperPrimary())
    <td colspan="{{ $colspan2 }}">{{ shapeIt($choice[$materialTle->C2]) }}</td>
    @endif
  </tr>

  <tr>
    <td colspan="2">Whether play material, games and sports equipment available for each grade</td>
    @if($master->hasPrimary())
    <td colspan="{{ $colspan1 }}">{{ shapeIt($choice[$materialSports->C1]) }}</td>
    @endif
    @if($master->hasUpperPrimary())
    <td colspan="{{ $colspan2 }}">{{ shapeIt($choice[$materialSports->C2]) }}</td>
    @endif
  </tr>
  @endif

  </tbody>
</table>

@parent
@stop
