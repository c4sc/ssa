@section('content')
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="2">Basic Details</th>
  </tr>
  <tr>
    <td class="col-xs-4">DISE Code</td>
    <td class="">{{ $school->SCHCD }}</td>
  </tr>
  <tr>
    <td>School Name</td>
    <td>{{{ $school->SCHNAME }}}</td>
  </tr>
  <tr>
    <td>School Category</td>
    <td>{{ choiceLabel($master->SCHCAT, School::$CATEGORIES) }}</td>
  </tr>
  <tr>
    <td>Rural or Urban</td>
    <td>{{ $areas[$master->RURURB] }}</td>
  </tr>
  <tr>
    <td>Village</td>
    <td>{{ isset($village->id)?$village->name:'' }}</td>
  </tr>
  <tr>
    <td>Cluster</td>
    <td>{{ isset($cluster->id)?$cluster->name:'' }}</td>
  </tr>
  <tr>
    <td>Block</td>
    <td>{{ isset($block->id)?$block->name:'' }}</td>
  </tr>
  <tr>
    <td>Habitation</td>
    <td>{{ isset($habitation->id)?$habitation->name:'' }}</td>
  </tr>
  <tr>
    <td>Pincode</td>
    <td>{{ $master->PINCD }}</td>
  </tr>
  <tr>
    <td>Education Block</td>
    <td>{{ isset($eduBlock->id)?$eduBlock->name:'' }}</td>
  </tr>
  <tr>
    <td>Assembly Consistuency</td>
    <td>{{ isset($constituency->id)?$constituency->name:'' }}</td>
  </tr>
  @if($master->RURURB != 1)
  <tr>
    <td>Municipality</td>
    <td>{{ isset($municipality->id)?$municipality->name:'' }}</td>
  </tr>
  <tr>
    <td>City</td>
    <td>{{ isset($city->id)?$city->name:'' }}</td>
  </tr>
  @endif
  <tr>
    @if($master->RURURB != 2)
    <td>Panchayath</td>
    <td>{{ isset($panchayat->id)?$panchayat->name:'' }}</td>
    @endif

  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="2">Contact Details</th>
  </tr>
  <tr>
    <td class="col-xs-4">Office Phone</td>
    <td>{{ $master->STDCODE1.' - '.$master->PHONE1 }}</td>
  </tr>
  <tr>
    <td>Office Mobile</td>
    <td>{{ $master->MOBILE1 }}</td>
  </tr>
  <tr>
    <td>Respondent Phone</td>
    <td>{{ shapeIt($master->STDCODE2, 5).'<span class="pull-left"> - </span>'.shapeIt($master->PHONE2, 7, 'square clear-right') }}</td>
  </tr>
  <tr>
    <td>Respondent Mobile</td>
    <td>{{ $master->MOBILE2 }}</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>{{ $master->EMAIL }}</td>
  </tr>
  <tr>
    <td>Website</td>
    <td>{{ $master->WEBSITE }}</td>
  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="2">Class Details</th>
  </tr>
  <tr>
    <td class="col-xs-4">Category</td>
    <td>{{ isset(School::$CATEGORIES[$master->SCHCAT])?School::$CATEGORIES[$master->SCHCAT]:'' }}</td>
  </tr>
  <tr>
    <td>Low Class </td>
    <td>{{ $master->LOWCLASS }}</td>
  </tr>
  <tr>
    <td>High Class </td>
    <td>{{ $master->HIGHCLASS }}</td>
  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4">School Type and Management</th>
  </tr>
  <tr>
    <th class="col-xs-3" colspan="1">Section</th>
    <th class="col-xs-3">School Type</th>
    <th class="col-xs-3">Managed by</th>
    <th class="col-xs-3">Recognition Year</th>
  </tr>
  <tr>
    <td colspan="1">Elementary</td>
    <td colspan="1">{{ $types[$master->SCHTYPE] }}</td>
    <td colspan="1">{{ $managements[$master->SCHMGT] }}</td>
    <td colspan="1">{{ shapeIt($master->YEARRECOG, 4) }}</td>
  </tr>
  <tr>
    <th colspan="4">Distance</th>
  </tr>
  <tr>
    <td colspan="3">Distance from primary to nearest govt. / aided upper primary school</td>
    <td colspan="1">{{ shapeIt($master->DISTP, 5, 'space').shapeIt('&nbsp;KM') }} </td>
  </tr>
  <tr>
    <td colspan="3">Distance from upper primary to nearest govt./aided secondary school</td>
    <td colspan="1">{{ shapeIt($master->DISTU, 5, 'space').shapeIt('&nbsp;KM') }} </td>
  </tr>
  <tr>
    <th colspan="4">More Details</th>
  </tr>
  <tr>
    <td colspan="2">Approachable by all weather roads </td>
    <td colspan="2">{{ choiceLabel($master->APPROACHBYROAD, $choice) }} </td>
  </tr>
  <tr>
    <td colspan="2">Establishment Year</td>
    <td colspan="2">{{ shapeIt($master->ESTDYEAR, 4) }} </td>
  </tr>
  <tr>
    <th colspan="4">Year of Upgradation</th>
  </tr>
  @if ($master->hasPrimary() && $master->hasUpperPrimary())
  <tr>
    <td colspan="2">Primary to Upper Primary</td>
    <td colspan="2">{{ shapeIt($master->YEARUPGRD, 4) }}</td>
  </tr>
  @endif
  @if($master->hasUpperPrimary() && $master->hasSecondary())
  <tr>
    <td colspan="2">Upper Primary to Secondary</td>
    <td colspan="2">{{ shapeIt($master->YEARUPGRDS, 4) }}</td>
  </tr>
  @endif
  @if ($master->hasSecondary() && $master->hasHigherSecondary())
  <tr>
    <td colspan="2">Secondary to Hr.Secondary</td>
    <td colspan="2">{{ shapeIt($master->YEARUPGRDH, 4) }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="1">{{ shapeIt('Shift School :&nbsp;').shapeIt(isset($choice[$master->SCHSHI_YN])?$choice[$master->SCHSHI_YN]:'', 1) }}</td>
    <td colspan="3">{{ shapeIt('Special School for CWSN :&nbsp;').shapeIt(isset($choice[$master->CWSNSCH_YN])?$choice[$master->CWSNSCH_YN]:'', 1) }}</td>
  </tr>
  <tr>
    <th colspan="4">Residential School</th>
  </tr>
  <tr>
    @if ($master->hasPrimary())
    <?php $column = ''; ?>
    <td>{{ shapeIt('Primary: ').shapeIt(isset($choice[$master->SCHRES_YN])?$choice[$master->SCHRES_YN]:'', 1) }}</td>
    @else
    <?php $column = "<td>&nbsp;</td>"; ?>
    @endif

    @if($master->hasUpperPrimary())
    <td>{{ shapeIt('Upper Primary: ').shapeIt(isset($choice[$master->SCHRESUPR_YN])?$choice[$master->SCHRESUPR_YN]:'', 1) }}</td>
    @else
     <?php $column .= "<td>&nbsp;</td>"; ?>
    @endif

     @if($master->hasSecondary())
    <td>{{ shapeIt('Secondary: ').shapeIt(isset($choice[$master->SCHRESSEC_YN])?$choice[$master->SCHRESSEC_YN]:'', 1) }}</td>
    @else
     <?php $column .= "<td>&nbsp;</td>"; ?>
    @endif

    @if($master->hasHigherSecondary())
    <td>{{ shapeIt('Hr.Secondary: ').shapeIt(isset($choice[$master->SCHRESHSEC_YN])?$choice[$master->SCHRESHSEC_YN]:'', 1) }}</td>
     @else
     <?php $column .= "<td>&nbsp;</td>"; ?>
    @endif
    {{$column}}

  </tr>
  @if($master->SCHRES_YN != 2)
  <tr>
    <td colspan="2">Type of Residential School</td>
    <td colspan="2">{{ shapeIt($master->RESITYPE, 1) }}</td>
  </tr>
  @elseif($master->SCHRESUPR_YN != 2)
  <tr>
    <td colspan="2">Type of Residential School</td>
    <td colspan="2">{{ shapeIt($master->RESITYPE, 1) }}</td>
  </tr>
  @elseif($master->SCHRESSEC_YN != 2)
  <tr>
    <td colspan="2">Type of Residential School</td>
    <td colspan="2">{{ shapeIt($master->RESITYPE, 1) }}</td>
  </tr>
  @elseif($master->SCHRESHSEC_YN != 2)
  <tr>
    <td colspan="2">Type of Residential School</td>
    <td colspan="2">{{ shapeIt($master->RESITYPE, 1) }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="2">Religious Minority School</td>
    <td colspan="2">{{ choiceLabel($master->RELMINORITY_YN, $choice) }}</td>
  </tr>
  @if($master->RELMINORITY_YN == 1)
  <tr>
    <td colspan="2">Religious Minority Type</td>
    <td colspan="2">{{ choiceLabel($master->RELMINORITY_TYPE, $religiousMinorities) }}</td>
  </tr>
  @endif
  <tr>
    <th colspan="4">Medium of Instruction</th>
  </tr>
  <tr>
    <td>{{ shapeIt('a: &nbsp;').shapeIt(isset($mediums[$master->MEDINSTR1])?$mediums[$master->MEDINSTR1]:null, 2) }}</td>
    <td>{{ shapeIt('b: &nbsp;').shapeIt(isset($mediums[$master->MEDINSTR2])?$mediums[$master->MEDINSTR2]:null, 2) }}</td>
    <td>{{ shapeIt('c: &nbsp;').shapeIt(isset($mediums[$master->MEDINSTR3])?$mediums[$master->MEDINSTR3]:null, 2) }}</td>
    <td>{{ shapeIt('d: &nbsp;').shapeIt(isset($mediums[$master->MEDINSTR4])?$mediums[$master->MEDINSTR4]:null, 2) }}</td>
  </tr>

  @if($master->hasPrimary())
  <tr>
    <td colspan="3">Are the majority of pupils taught through their mother tongue(s) at primary stage</td>
    <td colspan="1">{{ choiceLabel($master->MTONGUE_YN, $choice) }}</td>
  </tr>
  <tr>
    <td colspan="1">Languages taught at primary stage</td>
    <td>{{ choiceLabel($master->LANG1, $languages) }}</td>
    <td>{{ choiceLabel($master->LANG2, $languages) }}</td>
    <td>{{ choiceLabel($master->LANG3, $languages) }}</td>
  </tr>
  @endif
  @if($master->hasSecondary() || $master->hasHigherSecondary())
  <tr>
    <th colspan="4">Affiliation Board</th>
  </tr>
  <tr>
    @if($master->hasSecondary())
    <td colspan="2">{{ shapeIt('Secondary: &nbsp;').shapeIt(isset($boards[$master->BOARDSEC])?$boards[$master->BOARDSEC]:'', 2) }}</td>
    @endif
    @if($master->hasHigherSecondary())
    <td colspan="2">{{ shapeIt('Higher Secondary: &nbsp;').shapeIt(isset($boards[$master->BOARDHSEC])?$boards[$master->BOARDHSEC]:'', 2) }}</td>
    @endif
  </tr>
  <tr>
    <td colspan="3">Does the school offers any pre-vocational course(s) at secondary stage</td>
    <td>{{ choiceLabel($master->PREVOCCOURSE_YN, $choice) }}</td>
  </tr>
  <tr>
    <td colspan="3">Does the school provide educational and vocational guidance and counseling to students</td>
    <td>{{ choiceLabel($master->EDUVOCGUIDE_YN, $choice) }}</td>
  </tr>
  @endif
  <tr>
    <th colspan="4">Geographical Information</th>
  </tr>
  <tr>
    <td colspan="2">{{
      shapeIt('Latitude: &nbsp;').
      shapeIt($master->LONDEG, 2).
      "<div class='pull-left'>&deg; &nbsp;</div>".
      shapeIt($master->LONMIN, 2).
      "<div class='pull-left'>&acute; &nbsp;</div>".
      shapeIt($master->LONSEC, 2).
      "<div class='pull-left'>&nbsp; N</div>"
      }}</td>
    <td colspan="2">{{
      shapeIt('Longitude: &nbsp;').
      shapeIt($master->LATDEG, 2).
      "<div class='pull-left'>&deg; &nbsp;</div>".
      shapeIt($master->LATMIN, 2).
      "<div class='pull-left'>&acute; &nbsp;</div>".
      shapeIt($master->LATSEC, 2).
      "<div class='pull-left'>&nbsp; E</div>"
      }}</td>
  </tr>
  </tbody>
</table>
@parent
@stop
