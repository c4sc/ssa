@section('content')
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4">Particulars Two</th>
  </tr>
  @if($master->LOWCLASS == 1)
  <tr>
    <th colspan="4">Only for schools having Pre-primary sections and Anganwadi</th>
  </tr>
  <tr>
    <td colspan="3">{{ shapeIt('Pre-primary section (other than Anganwadi) attached to school' ) }}</td>
    <td colspan="1" class="col-xs-3">{{ shapeIt(isset($choice[$master->PPSEC_YN])?$choice[$master->PPSEC_YN]:'') }}</td>
  </tr>
  <tr>
    <th colspan="4">If yes</th>
  </tr>
  <tr>
    <td colspan="3">Total students in pre-primary sections</td>
    <td colspan="1"> {{ $master->PPSTUDENT }}</td>
  </tr>
  <tr>
    <td colspan="3">Total No. of  pre primary teachers</td>
    <td colspan="1">{{$master->PPTEACHER}}</td>
  </tr>
  <tr>
    <td colspan="3">{{ shapeIt('Anganwadi Centre in or adjacent to school') }}</td>
    <td colspan="1" class="col-xs-3">{{ shapeIt($choice[$master->ANGANWADI_YN]) }}</td>
  </tr>
  <tr>
    <th colspan="4">If yes</th> </tr>
  <tr>
    <td colspan="3">Total students in anganwadi sections</td>
    <td colspan="1"> {{ $master->ANGANWADI_STU }}</td>
  </tr>
  <tr>
    <td colspan="3">Total No. of anganwadi teachers</td>
    <td colspan="1"> {{ $master->ANGANWADI_TCH }}</td>
  </tr>
  @endif
  <tr>
    <th colspan="4">Last academic year details</th></tr>
  <tr>
    <td colspan="3">Number of  academic inspections by AEO  and  Others </td>
    <td colspan="1">{{ $master->NOINSPECT }}</td>
  </tr>
  <tr>
    <td colspan="3" > Number of visits by CRC coordinators </td>
    <td colspan="1">{{ $master->VISITSCRC }} </td>
  </tr>
  <tr>
    <td colspan="3" > Number of visits by Block level officer </td>
    <td colspan="1">{{ $master->VISITSBRC }}</td>
  </tr>
  <tr><th colspan="4"> School funds excluding MDM (for elementary schools/sections)</th></tr>
  <tr>
    <th></th>
    <th>School Development Grant (under SSA) </th>
    <th>School Maintenance Grant (Under SSA) </th>
    <th>TLM/Teachers Grant (Under SSA)</th>
  </tr>
  <tr>
    <td>Receipt</td>
    <th>{{ $master->CONTI_R }}</th>
    <th>{{ $master->SCHMNTCGRANT_R }}</th>
    <th>{{ $master->TLM_R }}</th>
  </tr>
  <tr>
    <td>Expenditure</td>
    <th>{{ $master->CONTI_E }}</th>
    <th>{{ $master->SCHMNTCGRANT_E }}</th>
    <th>{{ $master->TLM_E }}</th>
  </tr>

  <tr><th colspan="4">Staff category</th></tr>
  <tr>
    <td colspan="2">Section</td>
    <th>Sanctioned Post</td>
    <th>In Position</th>
  </tr>
  @if($master->hasPrimary())
  <tr>
    <td colspan="2">Primary</td>
    <th>{{ $master->TCHSANCT_PR }}</th>
    <th>{{ $master->TCHPOS }}</th>
  </tr>
  @endif
  @if($master->hasUpperPrimary())
  <tr>
    <td colspan="2">Upper Primary</td>
    <th>{{ $master->TCHSAN }}</th>
    <th>{{ $master->TCHREGU_UPR }}</th>
  </tr>
  @endif
  @if($master->hassecondary())
  <tr>
    <td colspan="2">Secondary</td>
    <th>{{ $master->TCHSANCT_SEC }}</th>
    <th>{{ $master->TCHPOSN_SEC }}</th>
  </tr>
  @endif
  @if($master->hashighersecondary())
  <tr>
    <td colspan="2">Higher Secondary</td>
    <th>{{ $master->TCHSANCT_HSEC }}</th>
    <th>{{ $master->TCHPOSN_HSEC }}</th>
  </tr>
  @endif

  <tr><th colspan="4">Contract Teachers </th></tr>

  @if($master->hasPrimary())
  <tr>
    <td colspan="1">Primary</td>
    <td colspan="3">{{ $master->PARAPOS }}</td>
  </tr>
  @endif

  @if($master->hasUpperPrimary())
  <tr>
    <td colspan="1">Upper Primary</td>
    <td colspan="3">{{ $master->TCHPARA_UPR }}</td>
  </tr>
  @endif

  @if($master->hassecondary())
  <tr>
    <td colspan="1">Secondary</td>
    <td colspan="3">{{ $master->PARAPOSN_SEC }}</td>
  </tr>
  @endif

  @if($master->hashighersecondary())
  <tr>
    <td colspan="1">Higher Secondary</td>
    <td colspan="3">{{ $master->PARAPOSN_HSEC }}</td>
  </tr>
  @endif

  <tr>
    <th colspan="3">Part-time instructor positioned as per RTE</th>
    <td colspan="1">{{ $master->TCHPART_UPR }}</td>
  </tr>

  </tbody>
</table>
@parent
@stop
