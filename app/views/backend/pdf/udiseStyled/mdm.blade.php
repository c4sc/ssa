@section('content')
@if($master->isGov() || $master->isGovAided())
<table class="table table-pdf table-bordered">
  <tr>
    <th colspan="2">Mid day meal information</th>
  </tr>
  <tr>
    <td class="col-xs-8">Status of Mid-day Meal</td>
    <td class="col-xs-4">{{ choiceLabel($mdm->MEALSINSCH, Mdm::$MDM_STATUS) }}</td>
  </tr>
  @if( ! in_array($mdm->MEALSINSCH, [1, 3]))
  <tr>
    <td>If Provided &amp; prepared in school premises then status of Kitchen Shed</td>
    <td>{{ choiceLabel($mdm->KITSHED, Mdm::$KITCHEN_STATUS) }}</td>
  </tr>
  @endif
  @if( ! in_array($mdm->MEALSINSCH, [1, 2]))
  <tr>
    <td>If Provided but not prepared in school premises, Provide source of MDM</td>
    <td>
      {{ choiceLabel($mdm->MDM_MAINTAINER, Mdm::$MDM_SOURCE) }}
    </td>
  </tr>
  @endif
</table>
@endif

@parent
@stop
