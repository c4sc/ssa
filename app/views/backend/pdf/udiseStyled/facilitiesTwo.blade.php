@section('content')
@if($master->hasSecondary() || $master->hasHigherSecondary())
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th class="col-xs-8"> facility </th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
  @foreach($group1 as $key => $facility)
  <tr>
    <td>{{ $facility->description }}</td>
    <td>{{ choiceLabel($groupValue1[$key]->ITEMVALUE, $choice) }}</td>
  </tr>
  @endforeach
  </tbody>
</table>

<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="2">Facilities</th>
    </tr>
    <tr>
      <th class="col-xs-8"> working/usable condition </th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
  @foreach($group3 as $key => $facility)
  <tr>
    <td>{{ $facility->description }}</td>
    <td>{{ choiceLabel($groupValue3[$key]->ITEMVALUE, $facilityChoice) }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th> Type of Laboratory </th>
      <th> Seperate room available </th>
      <th> Present condition </th>
    </tr>
  </thead>
  <tbody>
  @foreach($group2 as $key => $facility)
  <tr>
    <td>{{ $facility->description }}</td>
    <td>{{ choiceLabel($groupValue2[$key]->ITEMVALUE, $choice) }}</td>
    <td>{{ choiceLabel($groupValue2[$key]->ITEMVALUE1, $facilityChoice) }}
    </td>
  </tr>
  @endforeach
  </tbody>
</table>
@endif

@parent
@stop
