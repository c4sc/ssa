@section('content')
@if($master->hasSecondary() || $master->hasHigherSecondary())
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4">Only for Secondary and Hr. Secondary stage</th>
  </tr>
  <tr>
    <th colspan="4">Details of instructional days and school hours</th>
  </tr>
  <tr>
    <?php
    $colSecondary = $master->hasHigherSecondary()?1:2;
    $colHs = $master->hasSecondary()?1:2;
    ?>
    <td colspan="2" class="col-xs-8">&nbsp;</td>
    @if($master->hasSecondary())
    <th colspan="{{ $colSecondary }}" class="col-xs-2">Secondary</th>
    @endif
    @if($master->hasHigherSecondary())
    <th colspan="{{ $colHs }}" class="col-xs-2">Higher Secondary</th>
    @endif
  </tr>
  <tr>
    <td colspan="2">Number of instructional days (previous academic year)</td>
    @if($master->hasSecondary())
    <td colspan="{{ $colSecondary }}">{{ $smdc->WORKDAYS_SEC }}</td>
    @endif
    @if($master->hasHigherSecondary())
    <td colspan="{{ $colHs }}">{{ $smdc->WORKDAYS_HSEC }}</td>
    @endif
  </tr>
  <tr>
    <td colspan="2">School hours for children (per day)</td>
    @if($master->hasSecondary())
    <td colspan="{{ $colSecondary }}">{{ $smdc->SCHHRSCHILD_SEC }}</td>
    @endif
    @if($master->hasHigherSecondary())
    <td colspan="{{ $colHs }}">{{ $smdc->SCHHRSCHILD_HSEC }}</td>
    @endif
  </tr>
  <tr>
    <td colspan="2">Teacher working hours (per day)</td>
    @if($master->hasSecondary())
    <td colspan="{{ $colSecondary }}">{{ $smdc->SCHHRSTCH_SEC }}</td>
    @endif
    @if($master->hasHigherSecondary())
    <td colspan="{{ $colHs }}">{{ $smdc->SCHHRSTCH_HSEC }}</td>
    @endif
  </tr>

  <tr>
    <td colspan="2"> Is CCE being implemented?</td>
    @if($master->hasSecondary())
    <td colspan="{{ $colSecondary }}">{{ choiceLabel($master->CCESEC_YN, $choice) }}</td>
    @endif
    @if($master->hasHigherSecondary())
    <td colspan="{{ $colHs }}">{{ choiceLabel($master->CCEHSEC_YN, $choice) }}</td>
    @endif
  </tr>

  <tr>
    <th colspan="4">SMDC details</th>
  </tr>
  <tr>
    <td colspan="2">Whether School Management Committee (SMC) and School Management and Development Committee (SMDC) are same in the school?</td>
    <td colspan="2">{{ choiceLabel($smdc->SMDC_YN, $choice) }}</td>
  </tr>
  @if($smdc->SMDC_YN != 1)
  <tr>
    <td colspan="4">If 'No', give the following details about the composition of the SMDC; </td>
  </tr>
  <tr>
    <td colspan="2">Whether School Management and Development Committee has been constituted</td>
    <td colspan="2">{{ choiceLabel($smdc->SMCSMDC1_YN, $choice) }}</td>
  </tr>
  @if($smdc->SMCSMDC1_YN != 2)
  <tr>
    <td colspan="2">&nbsp;</td>
    <th colspan="1">Male</th>
    <th colspan="1">Female</th>
  </tr>
  <tr>
    <td colspan="2">Total number of Members in SMDC</td>
    <td colspan="1">{{ $smdc->TOT_M }}</td>
    <td colspan="1">{{ $smdc->TOT_F }}</td>
  </tr>

  <tr>
    <td colspan="2">Representatives of Parents/Guardians/PTA</td>
    <td colspan="1">{{ $smdc->PARENTS_M }}</td>
    <td colspan="1">{{ $smdc->PARENTS_F }}</td>
  </tr>

  <tr>
    <td colspan="2">Representatives/nominees from local authority/local government/urban local body</td>
    <td colspan="1">{{ $smdc->LOCAL_M }}</td>
    <td colspan="1">{{ $smdc->LOCAL_F }}</td>
  </tr>

  <tr>
    <td colspan="2">Member from Educationally Backward Minority Community</td>
    <td colspan="1">{{ $smdc->EBMC_M }}</td>
    <td colspan="1">{{ $smdc->EBMC_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Member from any Women Group</td>
    <td colspan="1">{{ $smdc->WOMEN_M }}</td>
    <td colspan="1">{{ $smdc->WOMEN_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Member from SC/ST community</td>
    <td colspan="1">{{ $smdc->SCST_M }}</td>
    <td colspan="1">{{ $smdc->SCST_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Nominee of the District Education Officer (DEO)</td>
    <td colspan="1">{{ $smdc->DEO_M }}</td>
    <td colspan="1">{{ $smdc->DEO_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Member from Audit and Accounts Department</td>
    <td colspan="1">{{ $smdc->AAD_M }}</td>
    <td colspan="1">{{ $smdc->AAD_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Subject experts (one each from Science, Humanities and arts/Crafts/Culture) nominated by District Programme Coordinator (RMSA)y</td>
    <td colspan="1">{{ $smdc->SUBEXP_M }}</td>
    <td colspan="1">{{ $smdc->SUBEXP_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Teachers (one each from Social Science, Science and Mathematics) of the school</td>
    <td colspan="1">{{ $smdc->TCH_M }}</td>
    <td colspan="1">{{ $smdc->TCH_F }}</td>
  </tr>

  <tr>
    <td colspan="2">Vice-Principal/Asst. Headmaster, as member</td>
    <td colspan="1">{{ $smdc->AHM_M }}</td>
    <td colspan="1">{{ $smdc->AHM_F }}</td>
  </tr>
  <tr>
    <td colspan="2">Principal/Headmaster, as Chairperson</td>
    <td colspan="1">{{ $smdc->HM_M }}</td>
    <td colspan="1">{{ $smdc->HM_F }}</td>
  </tr>

  <tr>
    <td colspan="2">Chairperson (If Principal/Headmaster is not the Chairperson)</td>
    <td colspan="1">{{ $smdc->CP_M }}</td>
    <td colspan="1">{{ $smdc->CP_F }}</td>
  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <td colspan="2">Number of meetings held by SMDC during the previous academic year</td>
    <td class="col-xs-5" colspan="2">{{ $smdc->SMDCMEETING }}</td>
  </tr>
  <tr>
    <td colspan="2">Whether SMDC Prepare the School Improvement Plan</td>
    <td colspan="2">{{ choiceLabel($smdc->SIP_YN, $choice) }}</td>
  </tr>

  <tr>
    <td colspan="2">Whether separate bank account for SMDC is being maintained</td>
    <td colspan="2">{{ choiceLabel($smdc->BANKAC_YN, $choice) }}</td>
  </tr>
  @if($smdc->BANKAC_YN != 2)
  <tr>
    <td colspan="1">If yes, </td>
    <td colspan="1">Bank name</td>
    <td colspan="2">{{ $smdc->BANKNAME }}</td>
  </tr>
  <tr>
    <td colspan="2">Branch</td>
    <td colspan="2">{{ $smdc->BANKBRANCH }}</td>
  </tr>

  <tr>
    <td colspan="2">Account No</td>
    <td colspan="2">{{ $smdc->BANKACNO }}</td>
  </tr>

  <tr>
    <td colspan="2">Account Name</td>
    <td colspan="2">{{ $smdc->ACNAME }}</td>
  </tr>

  <tr>
    <td colspan="2">IFSC Code</td>
    <td colspan="2">{{ $smdc->IFSC }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="2">Whether the School Building Committee (SBC) has been constituted:</td>
    <td colspan="2">{{ choiceLabel($smdc->SBC_YN, $choice) }}</td>
  </tr>
  @endif
  @endif
  <tr>
    <td colspan="2">Whether the school has constituted its Academic Committee (AC)</td>
    <td colspan="2">{{ choiceLabel($smdc->AC_YN, $choice) }}</td>
  </tr>

  <tr>
    <td colspan="2">Whether the school has constituted its Parent-Teacher Association (PTA)</td>
    <td colspan="2">{{ choiceLabel($smdc->PTA_YN, $choice) }}</td>
  </tr>
  @if($smdc->PTA_YN != 2)
  <tr>
    <td colspan="2">If yes, number of PTA meetings held during the last academic year:</td>
    <td colspan="2">{{ $smdc->PTAMEETING }}</td>
  </tr>
  @endif
  </tbody>
</table>
@endif

@parent
@stop
