@section('content')
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th colspan="4"> Physical Facilities and Equipment</th>
  </tr>
  <tr>
    <td colspan="2">Status of School Building</td>
    <td colspan="2">{{ choiceLabel($building->BLDSTATUS, Building::$STATUS) }}</td>
  </tr>
  <tr>
    <td colspan="4">Details of Classrooms and availability of furniture</td>
  </tr>
  <tr>
    <th class="col-xs-3">Class/Grade</th>
    <th class="col-xs-3">Classrooms used for instruction</th>
    <th class="col-xs-3">Classrooms under construction</th>
    <th class="col-xs-3">Availability of furniture</th>
  </tr>
  @if($master->LOWCLASS < 9)
  <tr>
    <td>1-8</td>
    <td> {{ $building->CLROOMS }}</td>
    <td> {{ $building->CLSUNDERCONST }}</td>
    <td> {{ $building->FURNSTU }}</td>
  </tr>
  @endif
  @if($master->hasSecondary())
  @foreach(range(9, 10) as $class)
  <tr>
    <td>{{ $class }}</td>
    <td> {{ $building->{'TOTCLS'.$class} }}</td>
    <td> {{ $building->{'CLSUCONST'.$class} }}</td>
    <td> {{ $building->{'FURN_YN'.$class} }}</td>
  </tr>
  @endforeach
  @endif
  @if($master->hasHigherSecondary())
  @foreach(range(11, 12) as $class)
  <tr>
    <td>{{ $class }}</td>
    <td> {{ $building->{'TOTCLS'.$class} }}</td>
    <td> {{ $building->{'CLSUCONST'.$class} }}</td>
    <td> {{ $building->{'FURN_YN'.$class} }}</td>
  </tr>
  @endforeach
  @endif
  <tr>
    <td colspan="2">Total Other Rooms</td>
    <td colspan="2"> {{ $building->OTHROOMS }} </td>
  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th> type of building block </th>
    <th> good </th>
    <th>Need minor repair</th>
    <th>Need major repair</th>
  </tr>
  @foreach($sections as $section)
  <input type="hidden" class="total-class-{{ $section }}" name="total-class-{{ $section }}">
  <input type="hidden" class="total-state-{{ $section }}" name="total-state-{{ $section }}">
  @foreach(Building::$BUILDING_TYPE[$section] as $type => $suffix)
  <tr class="class-type-{{ $section }}">
    <td>{{ $type }}</td>
    <td>{{ $building->{'CLGOOD'.$suffix} }}</td>
    <td>{{ $building->{'CLMINOR'.$suffix} }}</td>
    <td>{{ $building->{'CLMAJOR'.$suffix} }}</td>
  </tr>
  @endforeach
  @endforeach
  <tr>
    <td colspan="2">Land Available for Additional Classrooms </td>
    <td colspan="2"> {{ choiceLabel($building->LAND4CLS_YN, $choice) }} </td>
  </tr>
  <tr>
    <td colspan="2">Seperate room of Head teacher/Principal available</td>
    <td colspan="2">{{ choiceLabel($building->HMROOM_YN, $choice) }}</td>
  </tr>
  </tbody>
</table>

<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <th class="col-xs-6" colspan="2">&nbsp;</th>
    <th class="col-xs-3">Boys</th>
    <th class="col-xs-3">Girls</th>
  </tr>
  <tr>
    <td colspan="2">No. of Toilet Seats Constructed/Available</td>
    <td>{{ $building->TOILETB }}</td>
    <td>{{ $building->TOILET_G }}</td>
  </tr>
  <tr>
    <td colspan="2">No. of Toilet Seats Functional</td>
    <td>{{ $building->TOILETB_FUNC }}</td>
    <td>{{ $building->TOILETG_FUNC }}</td>
  </tr>
  <tr>
    <td colspan="2">How many of above toilets have water available for cleaning</td>
    <td>{{ $building->TOILETWATER_B }}</td>
    <td>{{ $building->TOILETWATER_G }}</td>
  </tr>
  <tr>
    <td colspan="2">Total Urinals Available</td>
    <td>{{ $building->URINALS_B }}</td>
    <td>{{ $building->URINALS_G }}</td>
  </tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr>
    <td class="col-xs-9" colspan="3"> Is the hand washing facility available near the toilet/urinals </td>
    <td class="col-xs-3" colspan="1"> {{ choiceLabel($building->HANDWASH_YN, $choice) }} </td>
  </tr>
  <tr>
    <td colspan="3"> is there any toilet that is friendly to Children with Special Needs (CWSN) </td>
    <td colspan="1"> {{ choiceLabel($building->TOILETD, $choice) }} </td>
  </tr>
  <tr>
    <td colspan="3">Main source of drinking water</td>
    <td colspan="1"> {{ choiceLabel($building->WATER, Building::$WATER_SOURCE) }} </td>
  </tr>
  <tr>
    <td colspan="3">Whether drinking water facility is functional </td>
    <td colspan="1"> {{ choiceLabel($building->WATER_FUNC_YN, $choice) }} </td>
  </tr>
  <tr>
    <td colspan="3"> Electricity connection avaialable in the school </td>
    <td colspan="1"> {{ choiceLabel($building->ELECTRIC_YN, School::$CHOICE_NF) }} </td>
  </tr>
  <tr>
    <td colspan="3">Type of Boundary wall </td>
    <td colspan="1"> {{ choiceLabel($building->BNDRYWALL, Building::$BOUNDARY_WALL) }} </td>
  </tr>
  <tr>
    <td colspan="3">Whether school has Library </td>
    <td colspan="1"> {{ choiceLabel($building->LIBRARY_YN, $choice) }} </td>
  </tr>
  @if($building->LIBRARY_YN != 2)
  <tr>
    <td colspan="3">total books in library </td>
    <td colspan="1"> {{ $building->BOOKINLIB }} </td>
  </tr>
  <tr>
    <td colspan="3">does the school have a full-time librarian</td>
    <td colspan="1">{{ choiceLabel($building->LIBRARIAN_YN, $choice) }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="3">Does the school subscribe for news paper/magazine</td>
    <td colspan="1">{{ choiceLabel($building->NEWSPAPER_YN, $choice) }}</td>
  </tr>
  <tr>
    <td colspan="3">Playground</td>
    <td colspan="1">{{ choiceLabel($building->PGROUND_YN, $choice) }}</td>
  </tr>
  @if($building->PGROUND_YN != 1)
  <tr>
    <td colspan="3">If no, whether land is available for developing playground-land</td>
    <td colspan="1">{{ choiceLabel($building->LAND4PGROUND_YN, $choice) }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="3">Total number of computers available for teaching and learning purposes</td>
    <td colspan="1">{{ $building->COMPUTER }}</td>
  </tr>
  <tr>
    <td colspan="3">Total number of computers functional for teaching and learning purposes</td>
    <td colspan="1">{{ $building->TOTCOMP_FUNC }}</td>
  </tr>
  <tr>
    <td colspan="3">Does the school have Computer Aided Learning (CAL) Lab</td>
    <td colspan="1">{{ choiceLabel($building->CAL_YN, $choice) }}</td>
  </tr>
  <tr>
    <td colspan="3">Whether Medical check-up of students conducted last year</td>
    <td colspan="1">{{ choiceLabel($building->MEDCHK_YN, $choice) }}</td>
  </tr>
  @if($building->MEDCHK_YN == 1)
  <tr>
    <td colspan="3">Frequency of Medical check-up in the school during last year</td>
    <td colspan="1">{{ $building->BOX }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="3">Whether ramp for disabled children needed to access classrooms</td>
    <td colspan="1">{{ choiceLabel($building->RAMPSNEEDED_YN, $choice) }}</td>
  </tr>
  @if($building->RAMPSNEEDED_YN != 2)
  <tr>
    <td colspan="3">If yes, whether ramp(s) is/are available</td>
    <td colspan="1">{{ choiceLabel($building->RAMPS_YN, $choice) }}</td>
  </tr>
  @if($building->RAMPS_YN != 2)
  <tr>
    <td colspan="3">If yes, whether Hand-rails for ramp is available</td>
    <td colspan="1">{{ choiceLabel($building->HANDRAILS, $choice) }}</td>
  </tr>
  @endif
  @endif
  <tr>
    <td colspan="3">Whether measured campus plan prepared</td>
    <td colspan="1">{{ choiceLabel($building->CAMPUSPLAN_YN, $choice) }}</td>
  </tr>
  <tr>
    <td colspan="3">Does the school have a boys hostel(s)?</td>
    <td colspan="1">{{ choiceLabel($building->HOSTELB_YN, $choice) }}</td>
  </tr>
  @if($building->HOSTELB_YN != 2)
  <tr>
    <td colspan="3">if yes, total boys residing in hostel</td>
    <td colspan="1">{{ $building->HOSTELBOYS }}</td>
  </tr>
  @endif
  <tr>
    <td colspan="3">Does the school have a girls hostel(s)?</td>
    <td colspan="1">{{ choiceLabel($building->HOSTELG_YN, $choice) }}</td>
  </tr>
  @if($building->HOSTELG_YN != 2)
  <tr>
    <td colspan="3">if yes, total girls residing in hostel</td>
    <td colspan="1">{{ $building->HOSTELGIRLS }}</td>
  </tr>
  @endif
</tbody>
</table>
@parent
@stop
