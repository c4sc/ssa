@section('script')
$(function() {

  function sumEnrolmentMinority(std, gender) {
    total = 0;
    classtotal = 0;
    $('.em-'+gender+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total) {
      $('.total-em-'+gender+std).html(total);
    }
    classtotal=(parseInt($('.total-em-boys-'+std).html()) || 0) + (parseInt($('.total-em-girls-'+std).html()) || 0);
    if(classtotal) {
      $('.classtotal-em-'+std).html(classtotal);
    }
  }

  @foreach(array_merge(range($master->LOWCLASS, $master->HIGHCLASS), array("PP")) as $class)
  sumEnrolmentMinority('{{ $class }}', 'boys-');
  sumEnrolmentMinority('{{ $class }}', 'girls-');
  @endforeach
});
@parent
@stop

@section('content')

<table class="table enrolment table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="10">Enrolment in current academic session(by Social Category-Minority)</th>
    </tr>
    <tr>
      <th>Class</th>
      <th>Section</th>
      @foreach(Enrolment::$CATEGORYMINORITY as $category)
      <th>{{ $category }}</th>
      @endforeach
      <th>Total</th>
      <th>Class strength</th>
    </tr>
  </thead>
  <tbody>
  @if($master->PPSEC_YN == 1)
  <tr>
    <td rowspan="2"> Pre Primary </td>
    <td> Boys </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="em-boys-PP">
        {{ $minorityData[($categoryId-5)]->CPP_B }}
    </td>
    @endforeach
    <th class="total-em-boys-PP"></th>
    <td rowspan="2" class="classtotal-em-PP"></td>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="em-girls-PP">
      {{ $minorityData[($categoryId-5)]->CPP_G }}
    </td>
    @endforeach
    <th class="total-em-girls-PP"></th>
  </tr>
  @endif
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  <tr>
    <td rowspan="2"> {{ $class }} </td>
    <td> Boys </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="{{ "em-boys-$class" }}">
        {{ $minorityData[($categoryId-5)]->{"C{$class}_B"} }}
    </td>
    @endforeach
    <th class="total-em-boys-{{ $class }}"></th>
    <td rowspan="2" class="classtotal-em-{{$class}}"></td>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="{{ "em-girls-$class" }}">
      {{ $minorityData[($categoryId-5)]->{"C{$class}_G"} }}
    </td>
    @endforeach
    <th class="total-em-girls-{{ $class }}"></th>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
