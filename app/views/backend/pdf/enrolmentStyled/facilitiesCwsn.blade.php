@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="10">Facilities Provided to CWSN (Last Academic Year)</th>
    </tr>
    <tr>
      <th class="text-center" rowspan="2">Sl.No</th>
      <th class="text-center" rowspan="2">Type of Facility</th>
      @foreach($sections as $sectionKey)
      <th class="text-center" colspan="2">{{ CwsnFacility::$SECTIONS[$sectionKey][1] }}</th>
      @endforeach
    </tr>
    <tr>
      @foreach($sections as $section)
      <th>Boys</th>
      <th>Girls</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  @foreach(CwsnFacility::$CWSNFACILITY as $cwsnFacilityId => $cwsnFacility)
  <tr>

    <td>{{ $cwsnFacilityId }}</td>
    <td>{{ $cwsnFacility }}</td>
    @foreach($sections as $sectionKey)
    <?php
    $abbr = CwsnFacility::$SECTIONS[$sectionKey][0];
    ?>
    <td>
      {{ $cwsnFacilityData[($cwsnFacilityId-1)]->{"TOT_B{$abbr}"} }}
    </td>
    <td>
      {{ $cwsnFacilityData[($cwsnFacilityId-1)]->{"TOT_G{$abbr}"} }}
    </td>
    @endforeach
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop

