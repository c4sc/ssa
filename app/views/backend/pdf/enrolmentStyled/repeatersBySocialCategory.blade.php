@section('script')
$(function() {
  function sumRepeatersCategory(std, gender) {
    total = 0;
    classtotal = 0;
    $('.'+gender+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      e_total = $('.total-'+gender+std).html(total);
    }
    classtotal=(parseInt($('.total-rc-boys-'+std).html()) || 0) + (parseInt($('.total-rc-girls-'+std).html()) || 0);
    if(classtotal != 0) {
      $('.classtotal-rc-'+std).html(classtotal);
    }
  }
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sumRepeatersCategory({{ $class }}, 'rc-boys-');
  sumRepeatersCategory({{ $class }}, 'rc-girls-');
  @endforeach
});
@parent
@stop

@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="8">Repeaters in current academic session(by Social Category)</th>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <th>&nbsp;</th>
      @foreach(Enrolment::$CATEGORY as $category)
      <th>{{ $category }}</th>
      @endforeach
      <th>Total</th>
      <th>Class strength</th>
    </tr>
  </thead>
  <tbody>
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  <tr>
    <td rowspan="2"> {{ $class }} </td>
    <td> Boys </td>
    @foreach(Enrolment::$CATEGORY as $categoryId => $category)
    <td class="rc-boys-{{$class}}">
      {{ $repeatersData[($categoryId-1)]->{"C{$class}_B"} }}
    </td>
    @endforeach
    <th class="total-rc-boys-{{ $class }}"></th>
    <th rowspan="2" class="classtotal-rc-{{$class}}"></th>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach(Enrolment::$CATEGORY as $categoryId => $category)
    <td class="rc-girls-{{$class}}">
      {{ $repeatersData[($categoryId-1)]->{"C{$class}_G"} }}
    </td>
    @endforeach
    <th class="total-rc-girls-{{ $class }}"></th>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
