@section('content')
@foreach($childrensFacility as $key => $facilityData)
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="9">{{ $facilitiesLabel[$key] }}</th>
    </tr>
    <tr>
      <th>Sl.No</td>
      <th>Student Category</th>
      <th>Type of Facility</th>
      @foreach(Incentive::$TYPE as $type)
      <th>{{ $type }}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  <?php $loopCounter=1; ?>
  @foreach(Incentive::$CATEGORY as $catabbr =>$category)
  <tr>

    <td rowspan="2">{{ $loopCounter }} </td>
    <td rowspan="2">{{ $category }} </td>
    <td> Boys </td>
    <?php $loopCounter++; ?>
    @foreach(Incentive::$TYPE as $typeId=> $type)
    <td>
      {{ $facilityData[($typeId-1)]->{"{$catabbr}_B"} }}
    </td>
    @endforeach
  </tr>

  <tr>
    <td> Girls </td>
    @foreach(Incentive::$TYPE as $typeId => $type)
    <td>
      {{ $facilityData[($typeId-1)]->{"{$catabbr}_G"} }}
    </td>
    @endforeach
  </tr>
  @endforeach
  </tbody>
</table>
@endforeach
@parent
@stop
