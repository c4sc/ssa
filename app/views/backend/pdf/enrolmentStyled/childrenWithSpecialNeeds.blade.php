@section('script')
$(function() {
  function sumCwsn(std, gender) {
    total = 0;
    classtotal = 0;
    $('.'+gender+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      $('.total-'+gender+std).html(total);
    }
    classtotal=(parseInt($('.total-cwsn-boys-'+std).html()) || 0) + (parseInt($('.total-cwsn-girls-'+std).html()) || 0);
    if(classtotal != 0) {
      $('.classtotal-cwsn-'+std).html(classtotal);
    }
  }
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sumCwsn({{ $class }}, 'cwsn-boys-');
  sumCwsn({{ $class }}, 'cwsn-girls-');
  @endforeach
});
@parent
@stop

@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="{{ count(range($master->LOWCLASS, $master->HIGHCLASS))*2+2 }}">Children with Special needs</th>
    </tr>
    <tr>
      <th rowspan="2">Sl.No</th>
      <th>Class</th>
      @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
      <th class="text-center" colspan="2">{{ $class }}</th>
      @endforeach
    </tr>
    <tr>
      <th>Type of Impairment</th>
      @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
      <th>Boys</th>
      <th>Girls</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  @foreach(Cwsn::$DISABILITIES as $disabilityId => $disability)
  <tr>
    <td>{{ $disabilityId }}</td>
    <td>{{ $disability }}</td>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td class="cwsn-boys-{{ $class }}">
      {{ $disabilityData[($disabilityId-1)]->{"C{$class}_B"} }}
    </td>
    <td class="cwsn-girls-{{ $class }}">
      {{ $disabilityData[($disabilityId-1)]->{"C{$class}_G"} }}
    </td>
    @endforeach
  </tr>
  @endforeach
  <tr>
    <th colspan="2">Total</th>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <th class="total-cwsn-boys-{{ $class }}"></th>
    <th class="total-cwsn-girls-{{ $class }}"></th>
    @endforeach
  </tr>
  <tr>
    <th colspan="2">Class strength</th>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td colspan="2" class="classtotal-cwsn-{{$class}}"></td>
    @endforeach
  </tr>
  </tbody>
</table>
@parent
@stop

