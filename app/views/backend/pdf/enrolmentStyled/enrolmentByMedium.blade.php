@section('script')
$(function() {

  function sumEnrolmentMedium(std, gender) {
    total = 0;
    classtotal = 0;
    $('.emedium-'+gender+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      e_total = $('.total-emedium-'+gender+std).html(total);
    }
    classtotal=(parseInt($('.total-emedium-boys-'+std).html()) || 0) + (parseInt($('.total-emedium-girls-'+std).html()) || 0);
    if(classtotal != 0) {
      $('.classtotal-emedium-'+std).html(classtotal);
    }
  }
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sumEnrolmentMedium({{ $class }}, 'boys-');
  sumEnrolmentMedium({{ $class }}, 'girls-');
  @endforeach
});
@parent
@stop

@section('content')

<table class="table enrolment table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="{{ count($schoolMediums)+4 }}">Enrolment in current academic session(by Medium of Instruction)</th>
    </tr>
    <tr>
      <th>Classes</th>
      <th>&nbsp;</th>
      @foreach($schoolMediums as $medium)
      <th>{{ $medium['name'] }}</th>
      @endforeach
      <th>Total</th>
      <th>Class strength</th>
    </tr>
  </thead>
  <tbody>
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  <tr>
    <td rowspan="2">{{ $class }} </td>
    <td> Boys </td>
    @foreach($schoolMediums as $index => $medium)
    <td class="emedium-boys-{{$class}}">{{ $mediumData[$index]->{"C{$class}_B"} }}</td>
    @endforeach
    <th class="total-emedium-boys-{{ $class }}"></th>
    <td rowspan="2" class="classtotal-emedium-{{$class}}"></td>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach($schoolMediums as $index => $medium)
    <td class="emedium-girls-{{$class}}">{{ $mediumData[$index]->{"C{$class}_G"} }}</td>
    @endforeach
    <th class="total-emedium-girls-{{ $class }}"></th>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
