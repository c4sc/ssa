@section('content')
@if($master->LOWCLASS == 1)
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="3">New Admissions in Grade I</th>
    </tr>
    <tr>
      <th> &nbsp;</th>
      <th> Boys </th>
      <th> Girls </th>
    </tr>
  </thead>
  <tbody>
  @foreach($firstGradeAgeList as $age => $description)
  <tr>
    <td>{{ $description }}</td>
    <td>{{ $firstGrade->{"AGE{$age}_B"} }}</td>
    <td>{{ $firstGrade->{"AGE{$age}_G"} }}</td>
  </tr>
  @endforeach
  <tr>
    <th>Total</th>
    <td>{{ $firstGrade->TOT_B }}</td>
    <td>{{ $firstGrade->TOT_G }}</td>
  </tr>
  <tr>
    <th colspan="3"> Pre school experience in </th>
  </tr>
  <tr>
    <td>Same School</td>
    <td>{{ $firstGrade->SAMESCH_B }}</td>
    <td>{{ $firstGrade->SAMESCH_G }}</td>
  </tr>
  <tr>
    <td>Another School</td>
    <td>{{ $firstGrade->OTHERSCH_B }}</td>
    <td>{{ $firstGrade->OTHERSCH_G }}</td>
  </tr>
  <tr>
    <td>Anganwadi/ECCE centre</td>
    <td>{{ $firstGrade->ECCE_B }}</td>
    <td>{{ $firstGrade->ECCE_G }}</td>
  </tr>
  </tbody>
</table>
@endif

@parent
@stop
