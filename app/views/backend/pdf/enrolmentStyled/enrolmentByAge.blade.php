@section('content')
<table style="padding:0" class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="{{ ($master->HIGHCLASS - $master->LOWCLASS + 1)*2+1 }}">Enrolment in current academic session(by Age)- All Children</th>
    </tr>
    <tr>
      <th>Class</th>
      @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
      <th class="text-center" colspan="2">{{ $class }}</th>
      @endforeach
    </tr>
    <tr>
      <th>Age</th>
      @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
      <th>Boys</th>
      <th>Girls</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
  <tr>
    <td>&lt;5</td>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td>
      @if(in_array(4, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      {{ $ageData[0]->{"C{$class}B"} }}
      @else
      &nbsp;
      @endif
    </td>
    <td>
      @if(in_array(4, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      {{ $ageData[0]->{"C{$class}G"} }}
      @else
      &nbsp;
      @endif
    </td>
    @endforeach
  </tr>
  @foreach(range(5, 22) as $age)
  <tr>
    <td>{{ $age }}</td>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td>
      @if(in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      {{ $ageData[($age-4)]->{"C{$class}B"} }}
      @else
      &nbsp;
      @endif
    </td>
    <td>
      @if(in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      {{ $ageData[($age-4)]->{"C{$class}G"} }}
      @else
      &nbsp;
      @endif
    </td>
    @endforeach
  </tr>
  @endforeach
  <tr>
    <td>&gt;22</td>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td>
      @if(in_array(23, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      {{ $ageData[19]->{"C{$class}B"} }}
      @else
      &nbsp;
      @endif
    </td>
      @if(in_array(23, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
      <td>{{ $ageData[19]->{"C{$class}G"} }}</td>
      @else
      <td class="disabled">&nbsp;</td>
      @endif
    @endforeach
  </tr>
  <tr>
    <th>Total</th>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <input type="hidden" value="" name="total-boys-{{ $class }}">
    <input type="hidden" value="" name="total-girls-{{ $class }}">
    <th class="total-boys-{{ $class }}" data-sum="{{ $sum->{"S{$class}B"} }}">{{ $sum->{"S{$class}B"} }}</th>
    <th class="total-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}">{{ $sum->{"S{$class}G"} }}</th>
    @endforeach
  </tr>
  <tr>
    <th>Class strength</th>
    @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
    <td colspan="2" class="classtotal-{{$class}}">
      {{ ($sum->{"S{$class}B"}+$sum->{"S{$class}G"})?($sum->{"S{$class}B"}+$sum->{"S{$class}G"}):'' }}</td>
    @endforeach
  </tr>
  </tbody>
</table>

@parent
@stop

