@section('content')

<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="9">Enrolment in current academic session(by Social Category)</th>
    </tr>
    <tr>
      <th>Class</th>
      <th>No. of Division</th>
      <th>Gender</th>
      @foreach(Enrolment::$CATEGORY as $category)
      <th style="width: 100px;">{{ $category }}</th>
      @endforeach
      <th>Total</th>
      <th>Class strength</th>
    </tr>
  </thead>
  <tbody>
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  <tr>
    <td rowspan="2"> {{ $class }} </td>
    <td rowspan="2" class="col-xs-1">
        {{ $divisions->{"C{$class}_B"} }}
    </td>
    <td> Boys </td>
    @foreach(Enrolment::$CATEGORY as $categoryId => $category)
    <td class="{{ "boys-$class" }}">
      {{ $categoryData[($categoryId-1)]->{"C{$class}_B"} }}
    </td>
    @endforeach
    <td class="total-boys-{{$class}}">{{ $sum->{"S{$class}B"} }}</td>
    <td rowspan="2" class="classtotal-{{$class}}">{{ shapeIt($sum->{"S{$class}G"}+$sum->{"S{$class}B"}) }}</td>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach(Enrolment::$CATEGORY as $categoryId => $category)
    <td class="{{ "girls-$class" }}">
      {{ $categoryData[($categoryId-1)]->{"C{$class}_G"} }}
    </td>
    @endforeach
    <td class="total-girls-{{$class}}">{{ $sum->{"S{$class}G"} }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
