@section('script')
@parent
$(function() {
  function sumRepeatersMinority(std, gender) {
    total = 0;
    classtotal = 0;
    $('.'+gender+std).each(function() {
      total += parseInt($(this).html()) || 0;
    });
    if(total != 0) {
      e_total = $('.total-'+gender+std).html(total);
    }
    classtotal=(parseInt($('.total-rm-boys-'+std).html()) || 0) + (parseInt($('.total-rm-girls-'+std).html()) || 0);
    if(classtotal != 0) {
      $('.classtotal-rm-'+std).html(classtotal);
    }
  }
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sumRepeatersMinority({{ $class }}, 'rm-boys-');
  sumRepeatersMinority({{ $class }}, 'rm-girls-');
  @endforeach
});
@stop

@section('content')
<table class="table table-pdf enrolment table-bordered">
  <thead>
    <tr>
      <th colspan="10">Repeaters in current academic session(by Social Category-Minority)</th>
    </tr>
    <tr>
      <th>Class</th>
      <th>Section</th>
      @foreach(Enrolment::$CATEGORYMINORITY as $category)
      <th>{{ $category }}</th>
      @endforeach
      <th>Total</th>
      <th>Class strength</th>
    </tr>
  </thead>
  <tbody>
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  <tr>
    <td rowspan="2"> {{ $class }} </td>
    <td> Boys </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="rm-boys-{{$class}}">
      {{ $repeatersMinorityData[($categoryId-5)]->{"C{$class}_B"} }}
    </td>
    @endforeach
    <th class="total-rm-boys-{{ $class }}"></th>
    <td rowspan="2" class="classtotal-rm-{{$class}}"></td>
  </tr>
  <tr>
    <td> Girls </td>
    @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
    <td class="rm-girls-{{$class}}">
      {{ $repeatersMinorityData[($categoryId-5)]->{"C{$class}_G"} }}
    </td>
    @endforeach
    <input type="hidden" value="" name="total-rm-girls-{{ $class }}">
    <th class="total-rm-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}"></th>
  </tr>
  @endforeach
  </tbody>
</table>
@parent
@stop
