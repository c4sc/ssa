@extends('backend.layouts.pdf')

@section('style')
<style>
#wrapper{
  width: 1200px;
  margin:0 auto;
  overflow:show;
}
</style>
@stop

@include('backend.pdf.enrolmentStyled.enrolmentByAge')
@include('backend.pdf.enrolmentStyled.childrenWithSpecialNeeds')

