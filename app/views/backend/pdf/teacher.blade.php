@section('content')
<div class="page-breaker"></div>
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="2">New Teacher</th>
    </tr>
  </thead>
  <tbody>
  <tr><td class="col-xs-4">Pen No.</td><td>&nbsp;</td></tr>
  <tr><td>Name</td><td>&nbsp;</td></tr>
  <tr><td>email</td><td>&nbsp;</td></tr>
  <tr><td>Adhar No.</td><td>&nbsp;</td></tr>
  <tr><td>Gender</td><td>&nbsp;</td></tr>
  <tr><td>Social Category</td><td>&nbsp;</td></tr>
  <tr><td>Type of Teacher</td><td>&nbsp;</td></tr>
  <tr><td>Nature of Appointment</td><td>&nbsp;</td></tr>
  <tr><td>Date of Birth</td><td>&nbsp;</td></tr>
  <tr><td>Year of joining in service</td><td>&nbsp;</td></tr>
  <tr><td>Highest Academic Qualification</td><td>&nbsp;</td></tr>
  <tr><td>Highest Professional Qualification</td><td>&nbsp;</td></tr>
  <tr><td>Classes Taught</td><td>&nbsp;</td></tr>
  <tr><td>Appointed Subject</td><td>&nbsp;</td></tr>
  <tr><td>Main Subject Taught</td><td>&nbsp;</td></tr>
  <tr><td>Subject 1</td><td>&nbsp;</td></tr>
  <tr><td>Subject 2</td><td>&nbsp;</td></tr>
  <tr><td>Total days of training recieved in last academic year</td><td>&nbsp;</td></tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th class="col-xs-4">Training by</th>
      <th>Total Days</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Teacher::$TRAINING_SOURCE as $source)
  <tr>
    <td>{{ $source }}</td>
    <td>&nbsp;</td>
  </tr>
  @endforeach
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr><td class="col-xs-4">No. of working days spent on non-teaching assignments</td><td>&nbsp;</td></tr>
  <tr><td>Maths/Science Studied upto</td><td>&nbsp;</td></tr>
  <tr><td>English/Languages studied upto</td><td>&nbsp;</td></tr>
  <tr><td>Social Studies studied upto</td><td>&nbsp;</td></tr>
  <tr><td>Working in present school since(year)</td><td>&nbsp;</td></tr>
  <tr><td>Type of Disablity, if any</td><td>&nbsp;</td></tr>
  <tr><td>Whether trained for teaching CWSN </td><td>&nbsp;</td></tr>
  <tr><td>Whether ICT training received </td><td>&nbsp;</td></tr>
  </tbody>
</table>

@parent
@stop
