@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-by-medium')

  function sum(std, gender) {
    total = 0;
    classtotal = 0;
    $('.'+gender+std).each(function() {
      total += (parseInt($(this).val()) || 0);
    });
    e_total = $('.total-'+gender+std)
    e_total.html(total);

    classtotal=(parseInt($('.total-boys-'+std).html()) || 0) + (parseInt($('.total-girls-'+std).html()) || 0);
    $('.classtotal-'+std).html(classtotal);
    $('input[name="total-'+gender+std+'"]').val(total)
    if(e_total.data('sum') < total) {
      e_total.removeClass('success')
      e_total.removeClass('warning')
      e_total.addClass('danger')
      } else if (e_total.data('sum') > total) {
      e_total.removeClass('success')
      e_total.removeClass('danger')
      e_total.addClass('warning')
    }
    else {
      e_total.removeClass('danger')
      e_total.removeClass('warning')
      e_total.addClass('success')
    }
  }
  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sum({{ $class }}, 'boys-');
  sum({{ $class }}, 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sum({{ $class }}, 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sum({{ $class }}, 'girls-');
  });
  @endforeach
});
@stop


@section('content')

<form role="form" method="post"  data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Enrolment in current academic session(by Medium of Instruction)</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Classes</th>
                  <th>&nbsp;</th>
                  @foreach($mediums as $medium)
                  <th>{{ $medium['name'] }}</th>
                  @endforeach
                  <th>Total</th>
                  <th>Class strength</th>
                </tr>
              </thead>
              <tbody>
                @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
              <tr>
                <td rowspan="2">{{ $class }} </td>
                <td> Boys </td>
                @foreach($mediums as $index => $medium)
                <td>
                  <div class="input-control {{ $errors->has("$class-{$medium['id']}-boys") ? ' has-error' : '' }}">
                    {{ Form::text("$class-{$medium['id']}-boys", Input::old("$class-{$medium['id']}-boys",
                    $mediumData[$index]->{"C{$class}_B"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control boys-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                @endforeach
                <input type="hidden" value="" name="total-boys-{{ $class }}">
                <th class="total-boys-{{ $class }} {{ $errors->has("total-boys-$class") ? 'danger' : '' }}" data-sum="{{ $sum->{"S{$class}B"} }}">
                </th>
                 <td rowspan="2" class="classtotal-{{$class}}"></td>
              </tr>
              <tr>
                <td> Girls </td>
                @foreach($mediums as $index => $medium)
                <td>
                  <div class="input-control {{ $errors->has("$class-{$medium['id']}-girls") ? ' has-error' : '' }}">
                    {{ Form::text("$class-{$medium['id']}-girls", Input::old("$class-{$medium['id']}-girls",
                    $mediumData[$index]->{"C{$class}_G"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control girls-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                @endforeach
                <input type="hidden" value="" name="total-girls-{{ $class }}">
                <th class="total-girls-{{ $class }} {{ $errors->has("total-girls-$class") ? 'danger' : '' }}" data-sum="{{ $sum->{"S{$class}G"} }}">
                </th>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
