@extends('backend.layouts.udise')

@section('title')
First Grade Admission
@stop

@section('script')
$(function() {
  activate('.enrolmentGroup', '.first-grade-admission')
  function sumFirstGrade(gender) {
    total = 0;
    $('.'+gender).each(function() {
      total += (parseInt($(this).val()) || 0);
    });
    $('.total-'+gender).val(total);
    $('.total-label-'+gender).html(total);
    grand_total = (parseInt($('.total-boys').val()) || 0) + (parseInt($('.total-girls').val()) || 0);
    $('.grand-total').html(grand_total);
  }

  // checking preschool count is not greater than students count
  function validatePreschool(gender) {
    gender_total = parseInt($('.total-'+gender).val())
    total = 0;
    $('.preschool-'+gender).each(function() {
      total += parseInt($(this).val()) || 0;
    });
    if(total > gender_total) {
      $('.preschool-'+gender).parent().addClass('has-error');
    } else {
      $('.preschool-'+gender).parent().removeClass('has-error');
    }
    $('#preschool-total-'+gender).val(total)
  }

  $('.boys').keyup(function() {
    sumFirstGrade('boys');
  });
  $('.girls').keyup(function() {
    sumFirstGrade('girls');
  });
  sumFirstGrade('boys');
  sumFirstGrade('girls');
  $('.preschool-boys').keyup(function() {
    validatePreschool('boys');
  });
  $('.preschool-girls').keyup(function() {
    validatePreschool('girls');
  });
  $('#same-school-boys').keyup()
  $('#same-school-girls').keyup()
});
@stop
@section('content')

<form role="form" method="post" data-parsley-validate action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> New Admissions in Grade I</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <table class="table">
              <thead>
                <tr>
                  <th> Age</th>
                  <th> Boys </th>
                  <th> Girls </th>
                </tr>
              </thead>
              <tbody>
              @foreach($firstGradeAgeList as $age => $description)
              <tr>
                <td> {{ $description }} </td>
                <td>
                  <div class="form-group {{ $errors->has("boys-age-$age") ? ' has-error' : '' }}">
                    {{ Form::text("boys-age-$age", Input::old("boys-age-$age", $firstGrade->{"AGE{$age}_B"}),
                    array('data-parsley-type'=>"integer", 'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>'boys form-control','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                <td>
                  <div class="form-group {{ $errors->has("girls-age-$age") ? ' has-error' : '' }}">
                    {{ Form::text("girls-age-$age", Input::old("girls-age-$age", $firstGrade->{"AGE{$age}_G"}),
                    array('data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>'girls form-control','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
              </tr>
              @endforeach
              <tr>
                <th>Total</th>
                <td>
                    {{ Form::hidden("total-boys", Input::old("total-boys", $firstGrade->TOT_B),
                    array( 'class'=>'form-control total-boys')) }}
                  <strong class="total-label-boys">
                    {{ $firstGrade->TOT_B }}
                  </strong>
                </td>
                <td>
                    {{ Form::hidden("total-girls", Input::old("total-girls", $firstGrade->TOT_G),
                    array('class'=>'form-control total-girls')) }}
                  <strong class="total-label-girls">
                    {{ $firstGrade->TOT_G }}
                  </strong>
                </td>
              </tr>
              <tr>
                <th>Grand Total</th>
                <th colspan="2" class="grand-total"> </th>
              </tr>
              <tr>
                <th colspan="3">
                  Pre school experience in
                </th>
              </tr>
              <tr>
                <td>Same School</td>
                <td>
                  <div class="form-group {{ $errors->has('same-school-boys') ? ' has-error' : '' }}">
                    {{ Form::text('same-school-boys', Input::old("same-school-boys", $firstGrade->SAMESCH_B),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-boys', 'id'=>'same-school-boys','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                <td>
                  <div class="form-group {{ $errors->has('same-school-girls') ? ' has-error' : '' }}">
                    {{ Form::text('same-school-girls', Input::old('same-school-girls', $firstGrade->SAMESCH_G),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-girls', 'id'=>'same-school-girls','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
              </tr>
              <tr>
                <td>Another School</td>
                <td>
                  <div class="form-group {{ $errors->has('other-school-boys') ? ' has-error' : '' }}">
                    {{ Form::text("other-school-boys", Input::old("other-school-boys", $firstGrade->OTHERSCH_B),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-boys','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                <td>
                  <div class="form-group {{ $errors->has('other-school-girls') ? ' has-error' : '' }}">
                    {{ Form::text("other-school-girls", Input::old("other-school-girls", $firstGrade->OTHERSCH_G),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-girls','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
              </tr>
              <tr>
                <td>Anganwadi/ECCE centre</td>
                <td>
                  <div class="form-group {{ $errors->has('anganwadi-boys') ? ' has-error' : '' }}">
                    {{ Form::text("anganwadi-boys", Input::old("anganwadi-boys", $firstGrade->ECCE_B),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-boys','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                <td>
                  <div class="form-group {{ $errors->has('anganwadi-girls') ? ' has-error' : '' }}">
                    {{ Form::text("anganwadi-girls", Input::old("anganwadi-girls", $firstGrade->ECCE_G),
                    array('data-parsley-trigger'=>'keyup','data-parsley-type'=>"number" , 'data-parsley-maxlength'=>'3',
                    'class'=>'form-control preschool-girls','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
              </tr>
              <input type="hidden" name="preschool-total-boys" id="preschool-total-boys">
              <input type="hidden" name="preschool-total-girls" id="preschool-total-girls">
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
