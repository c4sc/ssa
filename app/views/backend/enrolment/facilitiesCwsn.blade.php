@extends('backend.layouts.udise')

@section('title')
Facilities to CWSN
@stop

@section('script')
$(function() {
  activate('.facilityGroup', '.facilities-cwsn')
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Facilities Provided to CWSN (Last Academic Year)</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>

                  <th class="text-center" rowspan="2">Sl.No</th>
                  <th class="text-center" rowspan="2">Type of Facility</th>
                  @foreach($sections as $sectionKey)
                  <th class="text-center" colspan="2">{{ CwsnFacility::$SECTIONS[$sectionKey][1] }}</th>
                  @endforeach
                </tr>
                <tr>
                  @foreach($sections as $section)
                  <th>Boys</th>
                  <th>Girls</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                @foreach(CwsnFacility::$CWSNFACILITY as $cwsnFacilityId => $cwsnFacility)
                <tr>
                  <td>{{ $cwsnFacilityId }}</td>
                  <td>{{ $cwsnFacility }}</td>
                  @foreach($sections as $sectionKey)
                  <?php
                  $abbr = CwsnFacility::$SECTIONS[$sectionKey][0];
                  ?>
                  <td>
                    <div class="input-control {{ $errors->has("boys-$abbr-$cwsnFacilityId") ? 'has-error' : '' }}">
                      {{ Form::text("boys-$abbr-$cwsnFacilityId", Input::old("boys-$abbr-$cwsnFacilityId",
                      $cwsnFacilityData[($cwsnFacilityId-1)]->{"TOT_B{$abbr}"}), array('data-parsley-type'=>"integer",
                      'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'form-control',
                      'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  <td>
                    <div class="input-control {{ $errors->has("girls-$abbr-$cwsnFacilityId") ? 'has-error' : '' }}">
                      {{ Form::text("girls-$abbr-$cwsnFacilityId", Input::old("girls-$abbr-$cwsnFacilityId",
                      $cwsnFacilityData[($cwsnFacilityId-1)]->{"TOT_G{$abbr}"}), array('data-parsley-type'=>"integer",
                      'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'form-control',
                      'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  @endforeach
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop

