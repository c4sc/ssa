@extends('backend.layouts.udise')

@section('title')
Enrolment - Minority
@stop

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-minority-category')

  function sum(std, gender) {
   total = 0;
   classtotal = 0;
   $('.'+gender+std).each(function() {
      total += (parseInt($(this).val()) || 0);
    });
    e_total = $('.total-'+gender+std)
    e_total.html(total);
    classtotal=(parseInt($('.total-boys-'+std).html()) || 0) + (parseInt($('.total-girls-'+std).html()) || 0);
    $('.classtotal-'+std).html(classtotal);

    $('input[name="total-'+gender+std+'"]').val(total)
    if(e_total.data('sum') < total) {
      e_total.removeClass('success')
      e_total.addClass('danger')
    }
    else {
      e_total.removeClass('danger')
      e_total.addClass('success')
    }
  }

  @foreach(array_merge(range($master->LOWCLASS, $master->HIGHCLASS), array("PP")) as $class)
  sum('{{ $class }}', 'boys-');
  sum('{{ $class }}', 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sum('{{ $class }}', 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sum('{{ $class }}', 'girls-');
  });
  @endforeach
});
@stop


@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Enrolment in current academic session(by Social Category-Minority)</h3>
        </div>
        <div class="box-body">
          <table class="table enrolment table-bordered">
            <thead>
              <tr>
                <th>Class</th>
                <th>Section</th>
                @foreach(Enrolment::$CATEGORYMINORITY as $category)
                <th>{{ $category }}</th>
                @endforeach
                <th>Total</th>
                <th>Class strength</th>
              </tr>
            </thead>
            <tbody>
            @if($master->PPSEC_YN == 1)
            <tr>
              <td rowspan="2"> PP </td>
              <td> Boys </td>
              @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("boys-PP-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("boys-PP-$categoryId", Input::old("boys-PP-$categoryId",
                  $categoryData[($categoryId-5)]->{"CPP_B"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                  'data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$class",
                  'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <input type="hidden" value="" name="total-boys-PP">
              <th class="total-boys-PP" data-sum="{{ $sum->{"SPPB"} }}"></th>
              <td rowspan="2" class="classtotal-PP"></td>
            </tr>
            <tr>
              <td> Girls </td>
              @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("girls-PP-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("girls-PP-$categoryId", Input::old("girls-PP-$categoryId",
                  $categoryData[($categoryId-5)]->{"CPP_G"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control girls-$class", 'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <input type="hidden" value="" name="total-girls-PP">
              <th class="total-girls-PP" data-sum="{{ $sum->{"SPPG"} }}"></th>
            </tr>
            @endif
            @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
            <tr>
              <td rowspan="2"> {{ $class }} </td>
              <td> Boys </td>
              @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("boys-$class-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("boys-$class-$categoryId", Input::old("boys-$class-$categoryId",
                  $categoryData[($categoryId-5)]->{"C{$class}_B"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                  'data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$class",
                  'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <input type="hidden" value="" name="total-boys-{{ $class }}">
              <th class="total-boys-{{ $class }}" data-sum="{{ $sum->{"S{$class}B"} }}"></th>
              <td rowspan="2" class="classtotal-{{$class}}"></td>
            </tr>
            <tr>
              <td> Girls </td>
              @foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("girls-$class-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("girls-$class-$categoryId", Input::old("girls-$class-$categoryId",
                  $categoryData[($categoryId-5)]->{"C{$class}_G"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control girls-$class", 'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <input type="hidden" value="" name="total-girls-{{ $class }}">
              <th class="total-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}"></th>
            </tr>
            @endforeach
            </tbody>
          </table>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
