@extends('backend.layouts.udise')

@section('title')
facilities to children
@stop

@section('script')
$(function() {
  @if($section == 'Primary')
  activate('.facilityGroup', '.facilities-to-children-primary')
  @else
  activate('.facilityGroup', '.facilities-to-children-up')
  @endif
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-9">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title">
            @if($section == 'Primary')
            Facilities provided to Children(Last academic Year,Primary Classes)
            @else
            Facilities provided to Children(Last academic year, Upper Primary classes)
            @endif
          </h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Sl.No</th>
                  <th>Student Category</th>
                  <th>Type of Facility</th>
                  @foreach(Incentive::$TYPE as $type)
                  <th>{{ $type }}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                <?php $loopCounter = 1; ?>
                @foreach(Incentive::$CATEGORY as $catabbr =>$category)
                <tr>
                  <td rowspan="2">{{ $loopCounter }} </td>
                  <td rowspan="2">{{ $category }} </td>
                  <td> Boys </td>
                  <?php $loopCounter++;?>
                  @foreach(Incentive::$TYPE as $typeId=> $type)
                  <td>
                    <div class="input-control {{ $errors->has("$catabbr-boys-$typeId") ? 'has-error' : '' }}">
                      {{ Form::text("$catabbr-boys-$typeId", Input::old("$catabbr-boys-$typeId",
                      $facilityData[($typeId-1)]->{"{$catabbr}_B"}), array('data-parsley-type'=>"integer",
                      'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                      'class'=>'form-control','maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  @endforeach
                </tr>

                <tr>
                  <td> Girls </td>
                  @foreach(Incentive::$TYPE as $typeId => $type)
                  <td>
                    <div class="input-control {{ $errors->has("$catabbr-girls-$typeId") ? 'has-error' : '' }}">
                      {{ Form::text("$catabbr-girls-$typeId", Input::old("$catabbr-girls-$typeId",
                      $facilityData[($typeId-1)]->{"{$catabbr}_G"}), array('data-parsley-type'=>"integer",
                      'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>'form-control',
                      'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  @endforeach
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
