@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop



@section('script')
$(function() {
  activate('.repeatersGroup', '.repeaters-by-category')

  @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
  sumClassMax({{ $class }}, 'boys-');
  sumClassMax({{ $class }}, 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sumClassMax({{ $class }}, 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sumClassMax({{ $class }}, 'girls-');
  });
  @endforeach
});
@stop



@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Repeaters in current academic session(by Social Category)</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Class</th>
                  <th>&nbsp;</th>
                  @foreach(Enrolment::$CATEGORY as $category)
                  <th>{{ $category }}</th>
                  @endforeach
                  <th>Total</th>
                  <th>Class strength</th>
                </tr>
              </thead>
              <tbody>
              @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
              <tr>
                <td rowspan="2"> {{ $class }} </td>
                <td> Boys </td>
                @foreach(Enrolment::$CATEGORY as $categoryId => $category)
                <td>
                  <div class="input-control {{ $errors->has("boys-$class-$categoryId") ? 'has-error' : '' }}">
                    {{ Form::text("boys-$class-$categoryId", Input::old("boys-$class-$categoryId",
                    $categoryData[($categoryId-1)]->{"C{$class}_B"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control boys-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                @endforeach
                <input type="hidden" value="" name="total-boys-{{ $class }}">
                <th class="total-boys-{{ $class }}" data-sum="{{ $sum->{"S{$class}B"} }}"></th>
                <td rowspan="2" class="classtotal-{{$class}}"></td>
              </tr>
              <tr>
                <td> Girls </td>
                @foreach(Enrolment::$CATEGORY as $categoryId => $category)
                <td>
                  <div class="input-control {{ $errors->has("girls-$class-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("girls-$class-$categoryId", Input::old("girls-$class-$categoryId",
                  $categoryData[($categoryId-1)]->{"C{$class}_G"}),
                  array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control girls-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                  </div>
                </td>
                @endforeach
                <input type="hidden" value="" name="total-girls-{{ $class }}">
                <th class="total-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}"></th>
              </tr>
              @endforeach
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop
