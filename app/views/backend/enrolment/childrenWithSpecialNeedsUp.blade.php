@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {
  activate('.cwsnGroup', '.children-with-special-needs-up')

  @foreach(range($lowClass, $highClass) as $class)
  sumClassMax({{ $class }}, 'boys-');
  sumClassMax({{ $class }}, 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sumClassMax({{ $class }}, 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sumClassMax({{ $class }}, 'girls-');
  });
  @endforeach
});
@stop
@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Children with Special needs - UP section</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th class="text-center" rowspan="2">Sl.No</th>
                  <th>Class</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <th class="text-center" colspan="2">{{ $class }}</th>
                  @endforeach
                </tr>
                <tr>
                  <th>Type of Impairment</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <th>Boys</th>
                  <th>Girls</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                @foreach(Cwsn::$DISABILITIES as $disabilityId => $disability)
                <tr>
                  <td>{{ $disabilityId }}</td>
                  <td>{{ $disability }}</td>
                  @foreach(range($lowClass, $highClass) as $class)
                  <td>
                    <div class="input-control {{ $errors->has("boys-$class-$disabilityId") ? 'has-error' : '' }}">
                      {{ Form::text("boys-$class-$disabilityId", Input::old("boys-$class-$disability",
                      $disabilityData[($disabilityId-1)]->{"C{$class}_B"}),
                      array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                      'class'=>"form-control boys-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  <td>
                    <div class="input-control {{ $errors->has("girls-$class-$disabilityId") ? 'has-error' : '' }}">
                      {{ Form::text("girls-$class-$disabilityId", Input::old("girls-$class-$disability",
                      $disabilityData[($disabilityId-1)]->{"C{$class}_G"}),
                      array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                      'class'=>"form-control girls-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    </div>
                  </td>
                  @endforeach
                </tr>
                @endforeach
                 <tr>
                  <th colspan="2">Total</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <input type="hidden" value="" name="total-boys-{{ $class }}">
                  <input type="hidden" value="" name="total-girls-{{ $class }}">
                  <th class="total-boys-{{ $class }}" data-sum="{{ $sum->{"S{$class}B"} }}"></th>
                  <th class="total-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}"></th>
                  @endforeach
                </tr>
                <tr>
                  <th colspan="2">Class strength</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <td colspan="2" class="classtotal-{{$class}}"></td>
                  @endforeach
                </tr>

                </tbody>
              </table>
            </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop

