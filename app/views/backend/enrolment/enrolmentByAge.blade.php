@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {

  activate('.enrolmentAgeGroup', '.enrolment-by-age');

  @foreach(range($lowClass, $highClass) as $class)
  sumClass({{ $class }}, 'boys-');
  sumClass({{ $class }}, 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sumClass({{ $class }}, 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sumClass({{ $class }}, 'girls-');
  });
  @endforeach
});
@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled  action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Enrolment in current academic session(by Age)- All Children</h3>
        </div>
        <div class="box-body">

          <div class="form-group">
            <label class="control-label">Count of students</label>
            <table class="table enrolment table-bordered">
              <thead>
                <tr>
                  <th>Class</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <th class="text-center" colspan="2">{{ $class }}</th>
                  @endforeach
                </tr>
                <tr>
                  <th>Age</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <th>Boys</th>
                  <th>Girls</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>&lt;5</td>
                  @foreach(range($lowClass, $highClass) as $class)
                  <td>
                    @if(in_array(4, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("boys-$class-4", Input::old("boys-$class-4", $all[0]->{"C{$class}B"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control boys-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                    @endif
                  </td>
                  <td>
                    @if(in_array(4, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("girls-$class-4", Input::old("girls-$class-4", $all[0]->{"C{$class}G"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control girls-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                    @endif
                  </td>
                  @endforeach
                </tr>
                @foreach(range(5, 22) as $age)
                <tr>
                  <td>{{ $age }}</td>
                  @foreach(range($lowClass, $highClass) as $class)
                  <td>
                    @if(in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("boys-$class-$age", Input::old("boys-$class-$age",
                    $all[($age-4)]->{"C{$class}B"}), array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                    'data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$class",'maxlength'=>'3',
                    'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                    @endif
                  </td>
                  <td>
                    @if(in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("girls-$class-$age", Input::old("girls-$class-$age",
                    $all[($age-4)]->{"C{$class}G"}), array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                    'data-parsley-trigger'=>'keyup', 'class'=>"form-control girls-$class",
                    'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                    @endif
                  </td>
                  @endforeach
                </tr>
                @endforeach
                <tr>
                  <td>&gt;22</td>
                  @foreach(range($lowClass, $highClass) as $class)
                  <td>
                    @if(in_array(23, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("boys-$class-23", Input::old("boys-$class-23", $all[19]->{"C{$class}B"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3',
                    'data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-$class",
                    'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                    @endif
                  </td>
                  <td>
                    @if(in_array(23, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1])))
                    {{ Form::text("girls-$class-23", Input::old("girls-$class-23", $all[19]->{"C{$class}G"}),
                    array('data-parsley-type'=>"integer",'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                    'class'=>"form-control girls-$class",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                    @else
                    &nbsp;
                  @endif
                  </td>
                  @endforeach
                </tr>
                <tr>
                  <th>Total</th>
                  @foreach(range($lowClass, $highClass) as $class)
                  <input type="hidden" value="" name="total-boys-{{ $class }}">
                  <input type="hidden" value="" name="total-girls-{{ $class }}">
                  <th class="total-boys-{{ $class }}" data-sum="{{ $sum->{"S{$class}B"} }}"></th>
                  <th class="total-girls-{{ $class }}" data-sum="{{ $sum->{"S{$class}G"} }}"></th>
                  @endforeach
                </tr>
                <tr>
                  <th>Class strength</th>
                   @foreach(range($lowClass, $highClass) as $class)
                   <td colspan="2" class="classtotal-{{$class}}"></td>
                  @endforeach
                </tr>
              </tbody>
            </table>
          </div>

        </div> <!-- box-body -->
      </div> <!-- box -->
    </div> <!-- col -->
  </div>


  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</form>
@stop

