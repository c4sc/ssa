@extends('backend.layouts.udise')

@section('title')
@stop

@section('path')
<li><a href="{{ URL::Route('account') }}"><i class="fa fa-dashboard"></i> Home</a></li>
@stop

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-by-category')

  function sum(std, gender) {
    total = 0;
    classtotal=0;
    $('.'+gender+std).each(function() {
      total += (parseInt($(this).val()) || 0);
    });
    $('.total-'+gender+std).html(total);
    classtotal=(parseInt($('.total-boys-'+std).html()) || 0) + (parseInt($('.total-girls-'+std).html()) || 0);
    $('.col-classtotal-'+std).html(classtotal);
    $('.class-total-'+std).val(classtotal);
  }
  @foreach(array_merge(range($master->LOWCLASS, $master->HIGHCLASS), array("PP")) as $class)
  sum('{{ $class }}', 'boys-');
  sum('{{ $class }}', 'girls-');
  $('.boys-{{$class}}').keyup(function() {
    sum('{{ $class }}', 'boys-');
  });
  $('.girls-{{$class}}').keyup(function() {
    sum('{{ $class }}', 'girls-');
  });
  @endforeach
});
$("input:text[name*=boys]").keyup( function () {
  if( parseInt(this.value ) > 0 ) {
    $('input:text[name=divisions-'+this.name.substr(-3, 1)+']').attr({'data-parsley-required':'true','data-parsley-min':'1','min':'1'});
  }
});
$("input:text[name*=girls]").click( function () {
  if( parseInt(this.value ) > 0 ) {
    $('input:text[name=divisions-'+this.name.substr(-3, 1)+']').attr({'data-parsley-required':'true','data-parsley-min':'1','min':'1'});
  }
});

@stop

@section('content')

<form role="form" method="post" data-parsley-validate data-parsley-errors-messages-disabled action="">
  <div class="row">
    <div class="box box-warning">
      <div class="box-body">
        <fieldset>
          <div class="form-group">
            <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
          </div>
        </fieldset>
      </div>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <input type="hidden" name="_route" value="{{ Route::currentRouteName() }}" />


  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary box-solid">
        <div class="box-header">
          <h3 class="box-title"> Enrolment in current academic session(by Social Category)</h3>
        </div>
        <div class="box-body">

          <table class="table enrolment table-bordered">
            <thead>
              <tr>
                <th>Class</th>
                <th>No. of Division</th>
                <th>Gender</th>
                @foreach(Enrolment::$CATEGORY as $category)
                <th>{{ $category }}</th>
                @endforeach
                <th>Total</th>
                <th>Class strength</th>
              </tr>
            </thead>
            <tbody>
            @if($master->PPSEC_YN == 1)
            <tr>
              <td rowspan="2"> Pre Primary </td>
              <td class="col-xs-1 {{ $errors->first("divisions-PP", 'danger') }} " rowspan="2">
                <div class="input-control {{ $errors->has("divisions-PP") ? 'has-error' : '' }}">
                  {{ Form::text("divisions-PP", Input::old("divisions-PP",
                  $divisions->CPP_B), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control", 'maxlength'=>'3' , 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>

              </td>
              <td> Boys </td>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("boys-PP-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("boys-PP-$categoryId", Input::old("boys-PP-$categoryId",
                  $categoryData[($categoryId-1)]->{"CPP_B"}), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control boys-PP",
                  'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <td class="total-boys-PP"></td>
              <input type="hidden" name="class-total-PP" value="" class="class-total-PP">
              <td rowspan="2" class="col-classtotal-PP"></td>
            </tr>
            <tr>
              <td> Girls </td>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("girls-PP-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("girls-PP-$categoryId", Input::old("girls-PP-$categoryId",
                  $categoryData[($categoryId-1)]->{"CPP_G"}), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup','class'=>"form-control
                  girls-PP",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <td class="total-girls-PP">
              </td>
            </tr>
            @endif
            @foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class)
            <tr>
              <td rowspan="2"> {{ $class }} </td>
              <td class="col-xs-1 {{ $errors->first("divisions-$class", 'danger') }} " rowspan="2">
                <div class="input-control {{ $errors->has("divisions-$class") ? 'has-error' : '' }}">
                  {{ Form::text("divisions-$class", Input::old("divisions-$class",
                  $divisions->{"C{$class}_B"}), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup', 'class'=>"form-control",'maxlength'=>'3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>

              </td>
              <td> Boys </td>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("boys-$class-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("boys-$class-$categoryId", Input::old("boys-$class-$categoryId",
                  $categoryData[($categoryId-1)]->{"C{$class}_B"}), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control boys-$class",'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <td class="total-boys-{{$class}}">
              </td>
              <input type="hidden" name="class-total-{{ $class }}" value="" class="class-total-{{$class}}">
              <td rowspan="2" class="col-classtotal-{{$class}}"></td>
            </tr>
            <tr>
              <td> Girls </td>
              @foreach(Enrolment::$CATEGORY as $categoryId => $category)
              <td>
                <div class="input-control {{ $errors->has("girls-$class-$categoryId") ? 'has-error' : '' }}">
                  {{ Form::text("girls-$class-$categoryId", Input::old("girls-$class-$categoryId",
                  $categoryData[($categoryId-1)]->{"C{$class}_G"}), array('data-parsley-type'=>"integer",
                  'data-parsley-maxlength'=>'3','data-parsley-trigger'=>'keyup',
                  'class'=>"form-control girls-$class",'maxlength' => '3', 'data-parsley-min' => '0', 'min' => '0')) }}
                </div>
              </td>
              @endforeach
              <td class="total-girls-{{$class}}">
              </td>
            </tr>
            @endforeach
            </tbody>
          </table>
      </div> <!-- box-body -->
    </div> <!-- box -->
  </div> <!-- col -->
</div>


<div class="row">
  <div class="box box-warning">
    <div class="box-body">
      <fieldset>
        <div class="form-group">
          <button id="save-school" class="btn btn-primary">Save</button>
            @include('backend.udise.neighborLinks')
        </div>
      </fieldset>
    </div>
  </div>
</div>
</form>
@stop
