@extends('backend.layouts.general')

{{-- Page title --}}
@section('title')
Edit Profile
@stop

@section('script')
$(function() {
  activate('.accountGroup', '.edit-profile');
});
@stop

@section('content')
<div class="form-wrapper">
  <form method="post" action="" class="form-horizontal" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="col-md-2 control-label">Email <i class='icon-asterisk'></i> </label>
      <div class="col-md-5">
        <input id="email" name="email" placeholder="Email" class="form-control input-md" required=""
        type="text" value="{{{ Input::old('email', $user->email) }}}">
        {{ $errors->first('email', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>

    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
      <label for="first_name" class="col-md-2 control-label">First Name <i class='icon-asterisk'></i> </label>
      <div class="col-md-5">
        <input class="form-control" type="text" name="first_name" id="first_name" required="" value="{{{
        Input::old('first_name', $user->first_name) }}}" />
        {{ $errors->first('first_name', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>
    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
      <label for="last_name" class="col-md-2 control-label">Last Name
        <i class='icon-asterisk'></i>
      </label>
      <div class="col-md-5">
        <input class="form-control" type="text" name="last_name" id="last_name" required="" value="{{{
        Input::old('last_name', $user->last_name) }}}" />
        {{ $errors->first('last_name', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="col-md-2 control-label">Current Password <i class='icon-asterisk'></i></label>
      <div class="col-md-5">
        <input class="form-control" type="password" name="password" id="password" />
        {{ $errors->first('password', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>
    <hr>

    <!-- Form actions -->
    <div class="form-group">
      <label class="col-md-2 control-label"></label>
      <div class="col-md-7">
        <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save</button>
      </div>
    </div>

  </form>
@stop
