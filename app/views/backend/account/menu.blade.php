<li class="treeview accountGroup">
  <a>
    <i class="fa fa-th"></i> <span>Account</span> <small class="badge pull-right bg-green"></small>
  </a>
  <ul class="treeview-menu">
    <li class='change-password'><a href="{{ route('change-password') }}"><i class="fa fa-angle-double-right"></i> Change Password</a></li>
    <li class='edit-profile'><a href="{{ route('edit-profile') }}"><i class="fa fa-angle-double-right"></i> Edit Profile</a></li>
  </ul>
</li>
