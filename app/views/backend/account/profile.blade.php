@extends('backend.layouts.general')

@section('title')
Profile
@stop

@section('main-page')
Account
@stop

@section('content')
<div>
  <div class="col-xs-12">
    <h2 class="page-header">
      <i class="fa fa-user"></i>
      {{{ $user->fullName() }}}
      <small class="pull-right">{{{ AuthorizedController::$SCOPE[$user->scope] }}}</small>
    </h2>
  </div>
  <ul class="list-unstyled">
    <li><span class="col-md-2"><strong>Email</strong></span>{{{ $user->email }}} </li>
    <li>
    <span class="col-md-2">
      <strong>User Group</strong>
    </span>
    @foreach($user->getGroups() as $group)
    {{{ $group->name }}}
    @endforeach
    </li>
  </ul>
  <br>
  <a href="{{ URL::Route('change-password') }}" class="btn btn-primary">Change Password</a>
  <a href="{{ URL::Route('edit-profile') }}" class="btn btn-primary">Edit Profile</a>
</div>
@stop
