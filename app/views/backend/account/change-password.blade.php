@extends('backend.layouts.general')

{{-- Page title --}}
@section('title')
Change your Password
@stop

@section('script')
$(function() {
  activate('.accountGroup', '.change-password');
});
@stop

{{-- Account page content --}}
@section('content')

<div class="form-wrapper">
  <form method="post" action="" class="form-horizontal" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <!-- Old Password -->
    <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
      <label for="old_password" class="col-md-2 control-label">Old Password
        <i class='icon-asterisk'></i>
      </label>
      <div class="col-md-5">
        <input class="form-control" type="password" name="old_password" id="old_password" />
        {{ $errors->first('old_password', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="col-md-2 control-label">New Password
        <i class='icon-asterisk'></i></label>
      <div class="col-md-5">
        <input class="form-control" type="password" name="password" id="password" />
        {{ $errors->first('password', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>


    <div class="form-group {{ $errors->has('password_confirm') ? ' has-error' : '' }}">
      <label for="password_confirm" class="col-md-2 control-label">Confirm New Password
        <i class='icon-asterisk'></i>
      </label>
      <div class="col-md-5">
        <input class="form-control" type="password" name="password_confirm" id="password_confirm" />
        {{ $errors->first('password_confirm', '<span class="alert-msg"><i class="icon-remove-sign"></i> :message</span>') }}
      </div>
    </div>

    <hr>

    <!-- Form actions -->
    <div class="form-group">
      <label class="col-md-2 control-label"></label>
      <div class="col-md-7">
        <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Save</button>
      </div>
    </div>

  </form>
</div>
@stop
