@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.facilityGroup', '.exam-results')
});
@parent
@stop
@include('backend.pdf.result.examResults')
