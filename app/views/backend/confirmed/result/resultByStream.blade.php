@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.secondaryGroup', '.result-by-stream')
});
@parent
@stop

@include('backend.pdf.result.resultByStream')
