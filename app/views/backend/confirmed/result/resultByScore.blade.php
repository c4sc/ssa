@extends('backend.layouts.udise')

@section('script')
$(function() {
  @if($class == 'X')
  activate('.secondaryGroup', '.result-by-score-x')
  @else
  activate('.secondaryGroup', '.result-by-score-xii')
  @endif
});
@parent
@stop

@include('backend.pdf.result.resultByScore')
