@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.secondaryGroup', '.x-result-by-category')
});
@parent
@stop

@include('backend.pdf.result.xResultsByCategory')
