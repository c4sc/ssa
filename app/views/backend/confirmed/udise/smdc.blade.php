@extends('backend.layouts.udise')

@section('title')
SMDC
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-four')
});
@parent
@stop

@include('backend.pdf.udiseStyled.smdc')
