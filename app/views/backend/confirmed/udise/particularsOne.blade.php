@extends('backend.layouts.udise')

@section('title')
School Particulars
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-one')
});
@parent
@stop

@include('backend.pdf.udiseStyled.particularsOne')
