@extends('backend.layouts.udise')

@section('title')
Particulars Two
@stop

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-two')
});
@parent
@stop

@include('backend.pdf.udiseStyled.particularsTwo')
