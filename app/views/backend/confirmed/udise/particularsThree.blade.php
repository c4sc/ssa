@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.particularsGroup', '.particulars-three')
});
@parent
@stop

@include('backend.pdf.udiseStyled.particularsThree')
