@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.particularsGroup', '.facilities-one')
});
@parent
@stop

@include('backend.pdf.udiseStyled.facilitiesOne')
