@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.particularsGroup', '.facilities-two')
});
@parent
@stop

@include('backend.pdf.udiseStyled.facilitiesTwo')
