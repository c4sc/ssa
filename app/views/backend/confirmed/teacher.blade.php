@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.particularsGroup', '.teachers')
});
@stop

@section('content')
<div class="page-breaker"></div>
<table class="table table-pdf table-bordered">
  <thead>
    <tr>
      <th colspan="2">New Teacher</th>
    </tr>
  </thead>
  <tbody>
  <tr><td class="col-xs-4">Pen No.</td><td>{{ $teacher->PENNO }}</td></tr>
  <tr><td>Name</td><td>{{ $teacher->TCHNAME }}</td></tr>
  <tr><td>email</td><td>{{ $teacher->EMAIL }}</td></tr>
  <tr><td>Adhar No.</td><td>{{ $teacher->AADHAAR }}</td></tr>
  <tr><td>Gender</td><td>{{ $teacher->SEX }}</td></tr>
  <tr><td>Social Category</td><td>{{ $teacher->CASTE }}</td></tr>
  <tr><td>Type of Teacher</td><td>{{ $teacher->CATEGORY }}</td></tr>
  <tr><td>Nature of Appointment</td><td>{{ $teacher->POST_STATUS }}</td></tr>
  <tr><td>Date of Birth</td><td>{{ $teacher->DOB }}</td></tr>
  <tr><td>Year of joining in service</td><td>{{ $teacher->YOJ }}</td></tr>
  <tr><td>Highest Academic Qualification</td><td>{{ $teacher->QUAL_ACAD }}</td></tr>
  <tr><td>Highest Professional Qualification</td><td>{{ $teacher->QUAL_PROF }}</td></tr>
  <tr><td>Classes Taught</td><td>{{ $teacher->CLS_TAUGHT }}</td></tr>
  <tr><td>Appointed Subject</td><td>{{ $teacher->APPOINTSUB }}</td></tr>
  <tr><td colspan="2">Main Subject Taught</td></tr>
  <tr><td>Subject 1</td><td>{{ $teacher->MAIN_TAUGHT1 }}</td></tr>
  <tr><td>Subject 2</td><td>{{ $teacher->MAIN_TAUGHT2 }}</td></tr>
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <thead>
    <tr><colspan="2" td>Total days of training recieved in last academic year</tr>
    <tr>
      <th class="col-xs-4">Training by</th>
      <th>Total Days</th>
    </tr>
  </thead>
  <tbody>
  @foreach(Teacher::$TRAINING_SOURCE as $source)
  <tr>
    <td>{{ $source }}</td>
    <td>{{ $teacher->{"TRN_$source"} }}</td>
  </tr>
  @endforeach
  </tbody>
</table>
<table class="table table-pdf table-bordered">
  <tbody>
  <tr><td class="col-xs-4">No. of working days spent on non-teaching assignments</td><td>{{ $teacher->NOTCH_ASS }}</td></tr>
  <tr><td>Maths/Science Studied upto</td><td>{{ $teacher->MATH_UPTO }}</td></tr>
  <tr><td>English/Languages studied upto</td><td>{{ $teacher->ENG_UPTO }}</td></tr>
  <tr><td>Social Studies studied upto</td><td>{{ $teacher->SCIENCEUPTO }}</td></tr>
  <tr><td>Working in present school since(year)</td><td>{{ $teacher->WORKINGSINCE }}</td></tr>
  <tr><td>Type of Disablity, if any</td><td>{{ choiceLabel($teacher->DISABILITY_TYPE, Teacher::$DISABILITY) }}</td></tr>
  <tr><td>Whether trained for teaching CWSN </td><td>{{ choiceLabel($teacher->CWSNTRAINED_YN, Teacher::$CHOICE) }}</td></tr>
  <tr><td>Whether ICT training received </td><td>{{ choiceLabel($teacher->SUPVAR3, Teacher::$CHOICE) }}</td></tr>
  </tbody>
</table>
@stop
