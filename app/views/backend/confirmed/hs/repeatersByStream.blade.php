@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.secondaryGroup', '.repeaters-by-stream')
});
@parent
@stop

@include('backend.pdf.hs.repeatersByStream')
