@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.secondaryGroup', '.enrolment-by-stream')
});
@parent
@stop

@include('backend.pdf.hs.enrolmentByStream')
