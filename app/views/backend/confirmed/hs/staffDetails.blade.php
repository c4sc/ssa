@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.fundGroup', '.staff')
});
@parent
@stop

@include('backend.pdf.hs.staffDetails')
