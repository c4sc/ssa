@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.secondaryGroup', '.streams-available')
});
@parent
@stop

@include('backend.pdf.hs.streamsAvailable')
