@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.fundGroup', '.rmsa-fund')
});
@parent
@stop

@include('backend.pdf.rmsa.fund')
