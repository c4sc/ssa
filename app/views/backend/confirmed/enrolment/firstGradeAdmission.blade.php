@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.enrolmentGroup', '.first-grade-admission')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.firstGradeAdmission')
