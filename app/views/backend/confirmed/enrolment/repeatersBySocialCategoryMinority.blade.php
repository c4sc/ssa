@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.repeatersGroup', '.repeaters-minority-category')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.repeatersBySocialCategoryMinority')
