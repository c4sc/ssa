@extends('backend.layouts.udise')

@section('script')
$(function() {
  @if($section == 'Primary')
  activate('.facilityGroup', '.facilities-to-children-primary')
  @else
  activate('.facilityGroup', '.facilities-to-children-up')
  @endif
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.facilitiesToChildren')
