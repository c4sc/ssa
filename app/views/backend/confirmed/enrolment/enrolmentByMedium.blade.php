@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-by-medium')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.enrolmentByMedium')
