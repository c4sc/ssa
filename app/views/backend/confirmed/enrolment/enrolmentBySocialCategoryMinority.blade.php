@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-minority-category')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.enrolmentBySocialCategoryMinority')
