@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.repeatersGroup', '.repeaters-by-category')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.repeatersBySocialCategory')
