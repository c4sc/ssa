@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.enrolmentGroup', '.enrolment-by-category')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.enrolmentBySocialCategory')
