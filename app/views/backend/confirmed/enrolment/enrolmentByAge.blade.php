@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.enrolmentAgeGroup', '.enrolmentAgeLp')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.enrolmentByAge')
