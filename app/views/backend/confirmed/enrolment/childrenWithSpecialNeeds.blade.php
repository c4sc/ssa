@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.cwsnGroup', '.children-with-special-needs')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.childrenWithSpecialNeeds')
