@extends('backend.layouts.udise')

@section('script')
$(function() {
  activate('.facilityGroup', '.facilities-cwsn')
});
@parent
@stop

@include('backend.pdf.enrolmentStyled.facilitiesCwsn')
