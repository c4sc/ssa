@if(Sentry::hasAccess('blockAdmin'))
<li class="treeview userGroup">
<a>
  <i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu">
  <li class='school-users'><a href="{{ route('school-users') }}"><i class="fa fa-angle-double-right"></i> Search</a></li>
  <li class='school-user-create'><a href="{{ route('school-user-create') }}"><i class="fa fa-angle-double-right"></i> New SchoolUser</a></li>
  @if(Sentry::hasAccess('stateAdmin'))
  <li class='block-user-create'><a href="{{ route('block-user-create') }}"><i class="fa fa-angle-double-right"></i> New BlockUser</a></li>
  <li class='district-user-create'><a href="{{ route('district-user-create') }}"><i class="fa fa-angle-double-right"></i> New DistrictUser</a></li>
  @endif
</ul>
</li>
@endif

