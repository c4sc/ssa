@extends('backend.layouts.user')

@section('title')
School Users
@stop

@section('path')
@stop

@section('main-page')
SchoolUsers
@stop

@section('script')
$(function(){
  activate('.userGroup', '.school-users');
});
@stop

@section('content')
@include('backend.deleteModalBox')
<div class="box box-primary">
  <div class="box-body">
    <table class="table">
      <tbody>
      <tr>
        <th> Name </th>
        <td> {{{ $user->fullName() }}} </td>
      </tr>
      <tr>
        <th> Email </th>
        <td> {{{ $user->email }}} </td>
      </tr>
      <tr>
        <th> Scope </th>
        <td> {{{ AuthorizedController::$SCOPE[$user->scope] }}} </td>
      </tr>
      <tr>
        <th> User Group</th>

        <td>
          @foreach($user->getGroups() as $group)
          {{{ $group->name }}}
          @endforeach
        </td>
      </tr>
      @if (isset($school->SCHCD))
      <tr>
        <th> School </th>
        <td> {{{ $school->SCHNAME }}} </td>
      </tr>
      @else
      <tr>
        <th colspan="2"> <div class="alert alert-warning">School Not found </div></th>
      </tr>
      @endif
      </tbody>
    </table>
  </div>
  <div class="box-body">
    <a class="btn btn-warning" href="{{{ route('school-user-edit', $user->id) }}}"> Edit </a>
    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteUserModal">Delete</a>
  </div>
</div>
@stop



