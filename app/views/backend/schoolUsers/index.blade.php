@extends('backend.layouts.user')

@section('title')
Users
@stop

@section('path')
@stop

@section('main-page')
Users
@stop

@section('script')
$(function(){
  activate('.userGroup', '.school-users');
});
@stop

@section('content')
<div class="box box-primary">
  <div class="box-body">
    <form action="" method="get" class="form-horizontal form">
      <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <input type="text" name="q" id="q" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-default"
                type="submit">Search!</button>
            </span>
          </div>
        </div>
      </div>
    </form>

    @if(count(@$users))
    <table class="table">
      <thead>
        <th>Name</th>
      </thead>
      <tbody>
      @foreach ($users as $user)
      <tr>
        <td>
          @if($user->scope == AuthorizedController::$SCOPE_SCHOOL)
          <a href="{{ URL::Route('school-user-show', $user->id ) }}" class="name">{{{ $user->email }}}</a>
          @elseif($user->scope == AuthorizedController::$SCOPE_BLOCK)
          <a href="{{ URL::Route('block-user-show', $user->id ) }}" class="name">{{{ $user->email }}}</a>
          @elseif($user->scope == AuthorizedController::$SCOPE_DISTRICT)
          <a href="{{ URL::Route('district-user-show', $user->id ) }}" class="name">{{{ $user->email }}}</a>
          @endif

        </td>
      </tr>
      @endforeach
      </tbody>
    </table>
    <div>
      {{ $users->appends(array('q' => $query))->links() }}
    </div>
    @elseif(@$query)
    <br>
    <div class='alert alert-danger'>
      No results found, please try again.
    </div>
    @endif
  </div>
</div>
@stop


