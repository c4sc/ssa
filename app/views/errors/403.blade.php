<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
  SSA | 403
</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>
<style type="text/css">
*{margin:0;padding:0}
html, body {
  height: 100%;
}
body {
  margin: 20% auto 0;
  background: url('{{ asset('assets/images/403.png'); }}') #E0DFD9;
  background-repeat: no-repeat;
  background-position: top center;
  color: #E0DFD9;
  text-align: center;
  padding-left: 12%;
}
h1, p, a {
  color: #382E32;
  font-family: 'Cabin', sans-serif;
}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55912789-1', 'auto');
    ga('send', 'pageview');

</script>
</head>
<body>
  <h1> 403 </h1>
  <p>Access Denied.</p>
</body>
</html>
