<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */
Route::pattern('schoolCode', '[0-9]+');
Route::pattern('blockCode', '[0-9]+');
Route::pattern('districtCode', '[0-9]+');
Route::pattern('teacherCode', '[0-9]+');

Route::get('/', array('as'=>'login', 'uses'=>'AuthController@getLogin'));
Route::post('/', array('uses'=>'AuthController@postLogin'));
Route::get('/logout', array('as'=>'logout', 'uses'=>'AuthController@getLogout'));
Route::get('/forgot-password', array('as'=>'forgot-password', 'uses'=>'AuthController@forgotPassword'));
Route::post('/forgot-password', array('uses'=>'AuthController@postForgotPassword'));
Route::get('/forgot-password-confirm/{id}', array('as'=>'forgot-password-confirm', 'uses'=>'AuthController@getForgotPasswordConfirm'));
Route::post('/forgot-password-confirm/{id}', array('uses'=>'AuthController@postForgotPasswordConfirm'));

Route::group(array('prefix'=>'account'), function() {

    Route::get('/', array('as'=>'account', 'uses'=>'ProfileController@index'));
    Route::get('/change-password', array('as'=>'change-password', 'uses'=>'ProfileController@changePassword'));
    Route::post('/change-password', array('uses'=>'ProfileController@postChangePassword'));
    Route::get('/edit', array('as'=>'edit-profile', 'uses'=>'ProfileController@editProfile'));
    Route::post('/edit', array('uses'=>'ProfileController@postEditProfile'));
});

Route::group(array('namespace'=>'Controllers\Admin', 'prefix'=>'school'), function() {

    Route::get('search/', array('as'=>'schools', 'uses'=>'SchoolController@index'));
    Route::get('status/', array('as'=>'status', 'uses'=>'SchoolStatusController@index'));
    Route::get('new/', array('as'=>'new-school', 'uses'=>'SchoolController@create'));
    Route::post('new/', array('uses'=>'SchoolController@postCreate'));
    Route::get('{schoolCode}/', array('as'=>'show-school', 'uses'=>'SchoolController@show'));

});

Route::group(array('namespace'=>'Controllers\Admin', 'before'=>'auth|school|udise_menu', 'prefix'=>'school/{schoolCode}'), function() {

    Route::get('edit/', array('as'=>'edit-school', 'uses'=>'SchoolController@edit'));
    Route::post('edit/', array('uses'=>'SchoolController@postUpdate'));

    Route::group(array('prefix'=>'pdf', 'before'=>'class_data|stream'), function() {

        Route::get('1/download/', array('as'=>'pdf-1-download', 'uses'=>'PdfController@getPdfOne'));
        Route::get('2/download/', array('as'=>'pdf-2-download', 'uses'=>'PdfController@getPdfTwo'));
        Route::get('1/', array('as'=>'pdf-1', 'uses'=>'PdfController@getPageOne'));
        Route::get('2/', array('as'=>'pdf-2', 'uses'=>'PdfController@getPageTwo'));
    });

    Route::group(array('prefix'=>'udise/'), function() {

        Route::get('/', array('as'=>'udise-status', 'uses'=>'UdiseController@getStatusOfForms'));
        Route::get('school-particulars/', array('as'=>'particulars-one', 'uses'=>'UdiseController@getParticulars'));
        Route::post('school-particulars/', array('uses'=>'UdiseController@postParticulars'));
        Route::post('reset/', array('as'=>'udise-reset', 'uses'=>'UdiseController@postReset'));
    });

    Route::group(array('prefix'=>'udise', 'before'=>'class_data'), function() {

        Route::post('confirm/', array('as'=>'udise-confirm', 'uses'=>'UdiseController@postConfirm'));

        Route::get('school-particulars-two/', array('as'=>'particulars-two', 'uses'=>'UdiseController@getParticularsTwo'));
        Route::post('school-particulars-two/', array('uses'=>'UdiseController@postParticularsTwo'));

        Route::get('school-particulars-three/', array( 'as'=>'particulars-three', 'uses'=>'UdiseController@getParticularsThree'));
        Route::post('school-particulars-three/', array( 'uses'=>'UdiseController@postParticularsThree'));

        Route::get('school-particulars-four/', array( 'as'=>'particulars-four', 'uses'=>'UdiseController@getSmdc'));
        Route::post('school-particulars-four/', array( 'uses'=>'UdiseController@postSmdc'));

        Route::get('physical-facilities/', array( 'as'=>'facilities-one', 'uses'=>'UdiseController@getFacilitiesOne'));
        Route::post('physical-facilities/', array( 'uses'=>'UdiseController@postFacilitiesOne'));

        Route::get('physical-facilities-two/', array( 'as'=>'facilities-two', 'uses'=>'UdiseController@getFacilitiesTwo'));
        Route::post('physical-facilities-two/', array( 'uses'=>'UdiseController@postFacilitiesTwo'));

        Route::get('mid-day-meal/', array('as'=>'mdm', 'uses'=>'UdiseController@getMdm'));
        Route::post('mid-day-meal/', array('uses'=>'UdiseController@postMdm'));

        // Admission & Enrolment routes
        Route::get('first-grade-admission/', array('as'=>'first-grade-admission', 'uses'=>'EnrolmentController@firstGradeAdmission'));
        Route::post('first-grade-admission/', array('uses'=>'EnrolmentController@postFirstGradeAdmission'));

        Route::get('enrolment-by-social-category/', array('as'=>'enrolment-by-category', 'uses'=>'EnrolmentController@enrolmentBySocialCategory'));
        Route::post('enrolment-by-social-category/', array('uses'=>'EnrolmentController@postEnrolmentBySocialCategory'));

        Route::get('enrolment-by-social-category-minority/', array('as'=>'enrolment-minority-category', 'uses'=>'EnrolmentController@enrolmentBySocialCategoryMinority'));
        Route::post('enrolment-by-social-category-minority/', array('uses'=>'EnrolmentController@postEnrolmentBySocialCategoryMinority'));

        Route::get('enrolment-by-age/', array('as'=>'enrolment-by-age', 'uses'=>'EnrolmentController@enrolmentByAge'));
        Route::post('enrolment-by-age/', array('uses'=>'EnrolmentController@postEnrolmentByAge'));

        Route::get('enrolment-by-age-up/', array('as'=>'enrolment-by-age-up', 'uses'=>'EnrolmentController@enrolmentByAgeUp'));
        Route::post('enrolment-by-age-up/', array('uses'=>'EnrolmentController@postEnrolmentByAgeUp'));

        Route::get('enrolment-by-medium/', array('as'=>'enrolment-by-medium', 'uses'=>'EnrolmentController@enrolmentByMedium'));
        Route::post('enrolment-by-medium/', array('uses'=>'EnrolmentController@postEnrolmentByMedium'));

        Route::get('enrolment-by-age-hs/', array('as'=>'enrolment-by-age-hs', 'uses'=>'EnrolmentController@enrolmentByAgeHs'));
        Route::post('enrolment-by-age-hs/', array('as'=>'enrolment-by-age-hs', 'uses'=>'EnrolmentController@postEnrolmentByAgeHs'));

        Route::get('enrolment-by-age-ss/', array('as'=>'enrolment-by-age-ss', 'uses'=>'EnrolmentController@enrolmentByAgeSs'));
        Route::post('enrolment-by-age-ss/', array('as'=>'enrolment-by-age-ss', 'uses'=>'EnrolmentController@postEnrolmentByAgeSs'));

        Route::get('repeaters-by-social-category/', array('as'=>'repeaters-by-category', 'uses'=>'EnrolmentController@repeatersBySocialCategory'));
        Route::post('repeaters-by-social-category/', array('as'=>'repeaters-by-category', 'uses'=>'EnrolmentController@postRepeatersBySocialCategory'));

        Route::get('repeaters-by-social-category-minority/', array('as'=>'repeaters-minority-category', 'uses'=>'EnrolmentController@repeatersBySocialCategoryMinority'));
        Route::post('repeaters-by-social-category-minority/', array('as'=>'repeaters-minority-category', 'uses'=>'EnrolmentController@postRepeatersBySocialCategoryMinority'));

        /* NEED RENEWAL METHOD */

        Route::get('children-with-special-needs/', array('as'=>'children-with-special-needs', 'uses'=>'EnrolmentController@childrenWithSpecialNeeds'));
        Route::post('children-with-special-needs/', array('as'=>'children-with-special-needs', 'uses'=>'EnrolmentController@postChildrenWithSpecialNeeds'));

//Student

//
        Route::get('children-with-special-needs-up/', array('as'=>'children-with-special-needs-up', 'uses'=>'EnrolmentController@childrenWithSpecialNeedsUp'));
        Route::post('children-with-special-needs-up/', array('as'=>'children-with-special-needs-up', 'uses'=>'EnrolmentController@postChildrenWithSpecialNeedsUp'));

        Route::get('children-with-special-needs-hs/', array('as'=>'children-with-special-needs-hs', 'uses'=>'EnrolmentController@childrenWithSpecialNeedsHs'));
        Route::post('children-with-special-needs-hs/', array('as'=>'children-with-special-needs-hs', 'uses'=>'EnrolmentController@postChildrenWithSpecialNeedsHs'));

        Route::get('children-with-special-needs-hss/', array('as'=>'children-with-special-needs-hss', 'uses'=>'EnrolmentController@childrenWithSpecialNeedsHss'));
        Route::post('children-with-special-needs-hss/', array('as'=>'children-with-special-needs-hss', 'uses'=>'EnrolmentController@postChildrenWithSpecialNeedsHss'));

        Route::get('facilities-cwsn/', array('as'=>'facilities-cwsn', 'uses'=>'EnrolmentController@facilitiesCwsn'));
        Route::post('facilities-cwsn/', array('as'=>'facilities-cwsn', 'uses'=>'EnrolmentController@postFacilitiesCwsn'));

        Route::get('facilities-to-children/primary/', array('as'=>'facilities-to-children-primary', 'uses'=>'EnrolmentController@facilitiesToChildrenPrimary'));
        Route::post('facilities-to-children/primary/', array('as'=>'facilities-to-children-primary', 'uses'=>'EnrolmentController@postFacilitiesToChildrenPrimary'));

        Route::get('facilities-to-children/up/', array('as'=>'facilities-to-children-up', 'uses'=>'EnrolmentController@facilitiesToChildrenUp'));
        Route::post('facilities-to-children/up/', array('as'=>'facilities-to-children-up', 'uses'=>'EnrolmentController@postFacilitiesToChildrenUp'));

        Route::get('exam-results/', array('before'=>'class_5_8', 'as'=>'exam-results', 'uses'=>'ResultController@getExamResultsByCategory'));
        Route::post('exam-results/', array('uses'=>'ResultController@postExamResultsByCategory'));

        Route::get('x-result-by-category/', array('before'=>'school_secondary', 'as'=>'x-result-by-category', 'uses'=>'ResultController@resultXByCategory'));
        Route::post('x-result-by-category/', array('uses'=>'ResultController@postResultXByCategory'));

        Route::get('result-by-score/x/', array('before'=>'school_secondary', 'as'=>'result-by-score-x', 'uses'=>'ResultController@resultByScoreX'));
        Route::post('result-by-score/x/', array('uses'=>'ResultController@postResultByScoreX'));

        Route::get('rmsa-fund/', array('as'=>'rmsa-fund','before'=>'school_secondary_or_hss', 'uses'=>'RmsaController@fund'));
        Route::post('rmsa-fund/', array('uses'=>'RmsaController@postFund', 'before'=>'school_secondary_or_hss',));

        Route::get('staff/', array('as'=>'staff', 'before'=>'school_secondary_or_hss', 'uses'=>'HigherSecondaryController@staffDetails'));
        Route::post('staff/', array('as'=>'staff', 'before'=>'school_secondary_or_hss','uses'=>'HigherSecondaryController@postStaffDetails'));

    });

    Route::group(array('prefix'=>'hss', 'before'=>'class_data|school_hss'), function() {
        Route::get('streams-available/', array('as'=>'streams-available', 'uses'=>'HigherSecondaryController@availableStreams'));
        Route::post('streams-available/', array('as'=>'streams-available', 'uses'=>'HigherSecondaryController@postAvailableStreams'));
    });

    Route::group(array('prefix'=>'hss', 'before'=>'class_data|school_hss|stream'), function() {

        Route::get('enrolment-by-stream/', array('as'=>'enrolment-by-stream', 'uses'=>'HigherSecondaryController@enrolmentByStream'));
        Route::post('enrolment-by-stream/', array('uses'=>'HigherSecondaryController@postEnrolmentByStream'));

        Route::get('repeaters-by-stream/', array('as'=>'repeaters-by-stream', 'uses'=>'HigherSecondaryController@repeatersByStream'));
        Route::post('repeaters-by-stream/', array('uses'=>'HigherSecondaryController@postRepeatersByStream'));

        Route::get('result-by-score/xii/', array('as'=>'result-by-score-xii', 'uses'=>'ResultController@resultByScoreXII'));
        Route::post('result-by-score/xii/', array('as'=>'result-by-score-xii', 'uses'=>'ResultController@postResultByScoreXII'));

        Route::get('result-by-stream/', array('as'=>'result-by-stream', 'uses'=>'ResultController@resultByStream'));
        Route::post('result-by-stream/', array('uses'=>'ResultController@postResultByStream'));
    });


    Route::group(array('prefix'=>'teachers', 'before'=>'class_data'), function() {

        Route::get('/', array('as'=>'teachers', 'uses'=>'TeacherController@teachers'));
        Route::get('new/', array('as'=>'new-teacher', 'uses'=>'TeacherController@newTeacher'));
        Route::post('new/', array('uses'=>'TeacherController@postNewTeacher'));
        Route::get('{teacherCode}/show/', array( 'as'=>'show-teacher', 'uses'=>'TeacherController@editTeacher'));
        Route::get('{teacherCode}/edit/', array( 'as'=>'edit-teacher', 'uses'=>'TeacherController@editTeacher'));
        Route::post('{teacherCode}/edit/', array('uses'=>'TeacherController@postUpdate'));

        Route::post('delete/', array('as'=>'teacher-delete', 'uses'=>'TeacherController@destroy'));
    });

    Route::group(array('prefix'=>'students', 'before'=>'class_data'), function() {

        // handles edit, new, update, search
        Route::get('/', array('as' => 'students', 'uses' => 'AdiseController@students'));
        Route::post('/', array('uses' => 'AdiseController@postStudent'));

        Route::post('delete/', array('uses' => 'AdiseController@destroy', 'as' => 'adiseDeleteStudent'));
    });

    Route::group(array('prefix'=>'transfer', 'before'=>'class_data'), function() {

        Route::get('list/', array('before'=>'class_data', 'as' => 'transfer-list', 'uses' => 'AdiseController@transferList'));
        Route::post('list/', array('uses' => 'AdiseController@postApproveTransfer'));
        Route::post('list/cancel',array( 'before' => 'class_data', 'as' => 'cancel-tranfer', 'uses' => 'AdiseController@postCancelTransfer'));
        Route::get('{studentId}', array('as' => 'transfer-student', 'uses' => 'AdiseController@transferStudent'));
        Route::post('{studentId}', array('uses' => 'AdiseController@postTransferStudent'));
    });

});



Route::group(array('prefix'=>'block/{blockCode}'), function() {

    Route::get('premises/', array('as'=>'brc-premises', 'uses'=>'Controllers\Admin\PremisesController@getPremises'));
    Route::post('premises/', array('uses'=>'Controllers\Admin\PremisesController@postPremises'));
});

Route::group(array('prefix'=>'user'), function() {

    Route::get('/delete/{id}', array('as'=>'user-delete', 'uses'=>'SchoolUserController@destroy'));

    Route::group(array('prefix'=>'school', 'before'=>'auth'), function() {

        Route::get('/', array('as'=>'school-users', 'uses'=>'SchoolUserController@search'));

        Route::get('/create', array('as'=>'school-user-create', 'uses'=>'SchoolUserController@create'));
        Route::post('/create', array('uses'=>'SchoolUserController@postCreate'));

        Route::get('/{id}/show', array('as'=>'school-user-show', 'uses'=>'SchoolUserController@show'));
        Route::get('/{id}/edit', array('as'=>'school-user-edit', 'uses'=>'SchoolUserController@edit'));
        Route::post('/{id}/edit', array('uses'=>'SchoolUserController@postUpdate'));
    });

    Route::group(array('prefix'=>'block', 'before'=>'auth'), function() {
        Route::get('/create', array('as'=>'block-user-create', 'uses'=>'BlockUserController@create'));
        Route::post('/create', array('uses'=>'BlockUserController@postCreate'));
        Route::get('{id}/show', array('as'=>'block-user-show', 'uses'=>'BlockUserController@show'));
        Route::get('{id}/edit', array('as'=>'block-user-edit', 'uses'=>'BlockUserController@edit'));
        Route::post('{id}/edit', array('uses'=>'BlockUserController@postUpdate'));
    });

    Route::group(array('prefix'=>'district', 'before'=>'auth'), function() {
        Route::get('/create', array('as'=>'district-user-create', 'uses'=>'DistrictUserController@create'));
        Route::post('/create', array('uses'=>'DistrictUserController@postCreate'));
        Route::get('{id}/show', array('as'=>'district-user-show', 'uses'=>'DistrictUserController@show'));
        Route::get('{id}/edit', array('as'=>'district-user-edit', 'uses'=>'DistrictUserController@edit'));
        Route::post('{id}/edit', array('uses'=>'DistrictUserController@postUpdate'));
    });
});

Route::group(array('prefix'=>'api'), function() {

    Route::get('/blocks.json', array('as'=>'api-blocks', 'uses'=>'JsonController@blocks'));
    Route::get('/clusters.json', array('as'=>'api-clusters', 'uses'=>'JsonController@clusters'));
    Route::get('/villages.json', array('as'=>'api-villages', 'uses'=>'JsonController@villages'));
    Route::get('/panchayaths.json', array('as'=>'api-panchayaths', 'uses'=>'JsonController@panchayaths'));
    Route::get('/schools.json', array('as'=>'api-schools', 'uses'=>'JsonController@schools'));
});

Route::group(array('namespace'=>'Controller\Reports', 'prefix'=>'reports'), function() {

    Route::group(array('prefix'=>'comparison'), function() {
        Route::get('enrolment', array('as'=>'report-comparison-enrolment', 'uses'=>'ComparativeController@enrolmentComparison'));
        Route::get('enrolment-age-medium-caste', array('as'=>'report-comparison-enrolment-age-medium-caste',
            'uses'=>'ComparativeController@enrolmentByAgeCasteMedium'));
    });
    Route::group(array('prefix'=>'schools'), function() {
        Route::get('having-facilities', array('as'=>'report-schools-having-facilities', 'uses'=>'SchoolController@havingFacilities'));
        Route::get('with-no-response', array('as'=>'report-schools-with-no-response', 'uses'=>'SchoolController@noReponseInData'));
    });
    Route::group(array('prefix'=>'enrolment'), function() {
        Route::get('class-conditions', array('as'=>'report-enrolment-class-conditions', 'uses'=>'EnrolmentController@classroomConditions'));
    });
    Route::group(array('prefix'=>'data-entry', 'before' => 'auth'), function() {
        Route::get('state', array('before' => 'state','as'=>'report-data-entry-state', 'uses'=>'DataEntryStatusController@stateLevel'));
        Route::get('district/{districtCode}', array('before' => 'district', 'as'=>'report-data-entry-district', 'uses'=>'DataEntryStatusController@districtLevel'));
        Route::get('block/{blockCode}', array('before' => 'block', 'as'=>'report-data-entry-block', 'uses'=>'DataEntryStatusController@blockLevel'));
    });
});

App::missing(function($exception) {
    return Response::view('errors.404', array(), 404);
});
Validator::resolver(function($translator, $data, $rules, $messages) {
    return new ExtendedValidator($translator, $data, $rules, $messages);
});

