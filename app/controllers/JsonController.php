<?php

use Illuminate\Routing\Controller;

class JsonController extends Controller {

    /**
     * display list of blocks
     * in the district
     */
    public function blocks()
    {
        $districtCode = e(Input::get('district'));
        if( $districtCode != '') {
            try {
                District::find($districtCode);
                return [''=>'']+Block::where('BLKCD', 'like', "$districtCode%")
                    ->select(DB::raw('CONCAT(BLKNAME,  " - ", BLKCD) as name'), 'BLKCD as id')
                    ->lists('name', 'id');
            } catch(ModelNotFoundException $e) {
                return '{"--":"--"}';
            }
        }
        return '{"--":"--"}';
    }

	/**
	 * Display a listing of the Villages.
	 * GET /json
	 *
	 * @return Response
	 */
	public function villages()
	{
        $block = e(Input::get('block'));
        if( $block == '') {
            return '{"--":"--"}';
        }
        return Response::download(__DIR__."/../assets/villages/$block.json");
	}

	/**
	 * Display a listing of the Clusters.
	 * GET /json
	 *
	 * @return Response
	 */
	public function clusters()
	{
        $block = e(Input::get('block'));
        if( $block == '') {
            return '{"--":"--"}';
        }
        return Response::download(__DIR__."/../assets/clusters/$block.json");
	}

	/**
	 * Display a listing of the Panchayath.
	 * GET /json
	 *
	 * @return Response
	 */
	public function panchayaths()
	{
        $block = e(Input::get('block'));
        if( $block == '') {
            return '{"--":"--"}';
        }
        return Response::download(__DIR__."/../assets/panchayaths/$block.json");
	}

	/**
	 * listing of Schools in a block under a district .
	 * GET /json
	 *
	 * @return Response
	 */


	public function schools()
	{
        $blockCode = Input::get('block');
        if ($blockCode == '') {
            return '{"--":"--"}';
        }
        $schools = new SchoolDetails;
        $schools = $schools->where('BLKCD', $blockCode);
        try {
            return [''=>'']+$schools->select(DB::raw('CONCAT(SCHNAME,  " - ", SCHCD) as name'), 'SCHCD')
                ->lists('name', 'SCHCD');
        } catch(ModelNotFoundException $e) {
            return '{"--":"--"}';
        }
	}

}
