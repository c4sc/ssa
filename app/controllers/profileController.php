<?php

class ProfileController extends AuthorizedController {

    /**
     * profile page
     *
     * @return View
     */
    function index()
    {
        $user = Sentry::getUser();
        return View::make('backend.account.profile', compact('user'));
    }

    /**
     * User change password page.
     *
     * @return View
     */
    public function changePassword()
    {
        // Get the user information
        $user = Sentry::getUser();

        // Show the page
        return View::make('backend/account/change-password', compact('user'));
    }

    /**
     * User change password form processing page.
     *
     * @return Redirect
     */
    protected function postChangePassword()
    {
        // Declare the rules for the form validation
        $rules = array(
            'old_password'     => 'required',
            'password'         => 'required|min:6',
            'password_confirm' => 'required|same:password',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // Grab the user
        $user = Sentry::getUser();

        // Check the user current password
        if ( ! $user->checkPassword(Input::get('old_password'))) {
            // Set the error message
            $this->messageBag->add('old_password', 'Your current password is incorrect.');

            // Redirect to the change password page
            return Redirect::route('change-password')->withErrors($this->messageBag);
        }

        // Update the user password
        $user->password = Input::get('password');
        $user->save();

        // Redirect to the change_password page
        return Redirect::route('change-password')->with('success', 'Password successfully updated');
    }

    /**
     * edit profile form
     *
     * @return View
     */
    public function editProfile()
    {
        $user = Sentry::getUser();
        return View::make('backend/account/edit-profile', compact('user'));
    }

    /**
     * save profile
     *
     * @return Redirect
     */
    public function postEditProfile()
    {
        $rules = array(
            'password' => 'required|between:6,32',
            'email'            => 'required|email|unique:users,email,'.Sentry::getUser()->email.',email',
        );

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::back()->withInput()->withErrors($validator);
        }

        // Grab the user
        $user = Sentry::getUser();

        // Check the user current password
        if ( ! $user->checkPassword(Input::get('password'))) {
            // Set the error message
            $this->messageBag->add('password', 'Your current password is incorrect');

            // Redirect to the edit profile page
            return Redirect::route('edit-profile')->withErrors($this->messageBag);
        }

        // Update the user email
        $user->email = e(Input::get('email'));
        $user->first_name = e(Input::get('first_name'));
        $user->last_name = e(Input::get('last_name'));
        $user->save();

        // Redirect to account
        return Redirect::route('account')->with('success', 'Profile successfully updated');
    }
}
