<?php namespace Controllers\Admin;

use AuthorizedController;
use Illuminate\Routing\Controller;
use Input;
use Lang;
use User;
use School;
use SchoolDetails;
use Master;
use District;
use Block;
use Village;
use Cluster;
use Panchayath;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Material;
use Building;
use Facility;
use Rte;
use Mdm;
use Smdc;
use Enrolment;
use FirstGrade;
use AgeEnrolment;
use Repeater;
use StreamEnrolment;
use Incentive;
use Result;
use RmsaFund;
use Staff;
use Teacher;
use Cwsn;
use CwsnFacility;
use AcademicYear;
use SchoolStatus;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class SchoolStatusController extends \AuthorizedController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $user = Sentry::getUser();
        $query = e(Input::get('q'));
        if ( ! $query)
            $query = '';

        switch($user->scope) {
            case self::$SCOPE_SCHOOL:
                return Redirect::route('show-school', $user->scope_id);
                break;
            case self::$SCOPE_BLOCK:
                $schools = SchoolStatus::where('school_code','LIKE', $user->scope_id.'%')->where('link_id','35');
                break;
            case self::$SCOPE_DISTRICT:
                $schools = SchoolStatus::where('school_code', $user->scope_id.'%')->where('link_id','35');
                break;
            case self::$SCOPE_STATE:
                $schools = SchoolDetails::where('school_code', 'LIKE', "%$query%")->where('link_id','35');
                break;
            default:
                return Redirect::route('logout');
                break;
        }

        $schools = $schools
            ->where('school_code', 'LIKE',"%$query%" )
            ->whereIn('academic_year',
                array(AcademicYear::CURRENT_YEAR, AcademicYear::PREVIOUS_YEAR))
            ->orderBy('updated_at')
            ->paginate(50);
        return View::make('backend.schoolStatus.index', compact('schools', 'query'));
	}
}
