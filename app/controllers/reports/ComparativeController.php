<?php namespace Controller\Reports;

use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use DB;
use View;
use App;
use Response;
use Log;


class ComparativeController extends AuthorizedController
{
    /**
     * Reports home page
     *
     */
    function index()
    {
        $user = Sentry::getUser();
        if($user->scope == self::$SCOPE_SCHOOL) {
            $schoolCode = $user->scope_id;
            $school = School::find($schoolCode);
        }
        $enrollments = DB::table('STEPS_ENROLMENT09')
            ->select(DB::raw('count(1) as count, ac_year, schcd'))
            ->groupBy('schcd','ac_year')
            ->get();
        return View::make('backend.reports.index', compact('user', 'school','enrollments'));
    }

    /**
     * Enrolment Comparison
     *
     */
    function enrolmentComparison()
    {
        $cyRange = Input::get('year', '');
        if ($cyRange == '' || ( ! preg_match('/^20[1-9]{2}-[1-9]{2}/', $cyRange))) {
            $cyRange = AcademicYear::CURRENT_YEAR;
        }
        $yearParts = explode("-", $cyRange);
        $py = intval($yearParts[0]-1);
        $pyRange = sprintf("%d-%s", $py, substr($yearParts[0], -2));

        $sql = 'SELECT d.distcd, d.distname, b.blkcd, b.blkname,
                    count(k.SCHCD) as num_schools,
                    sum(
                            IFNULL(c.CLROOMS,0)+IFNULL(c.TOTCLS9,0)+IFNULL(c.TOTCLS10,0)+
                            IFNULL(c.TOTCLS11,0)+IFNULL(c.TOTCLS12,0)
                    ) as class_rooms,
                    IFNULL(en.boys_total, 0) as boys_total,
                    IFNULL(en.girls_total, 0) as girls_total
                FROM STEPS_BLOCK b
                LEFT JOIN
                    STEPS_DISTRICT d
                        ON
                            d.DISTCD = LEFT(b.BLKCD, 4)
                LEFT JOIN
                    STEPS_KEYVAR k
                        ON
                            k.BLKCD = b.BLKCD
                        AND k.AC_YEAR = \'%1$s\'
                LEFT JOIN
                    STEPS_BLDEQP c
                        ON
                            k.SCHCD = c.SCHCD
                        AND c.AC_YEAR = \'%1$s\'
                LEFT OUTER JOIN
                    (SELECT b.blkcd,
                        sum(
                            IFNULL(e.c1_b,0)+IFNULL(e.c2_b,0)+IFNULL(e.c3_b,0)+
                            IFNULL(e.c4_b,0)+IFNULL(e.c5_b,0)+IFNULL(e.c6_b,0)+
                            IFNULL(e.c7_b,0)+IFNULL(e.c8_b,0)+IFNULL(e.c10_b,0)+
                            IFNULL(e.c11_b,0)+IFNULL(e.c12_b,0)
                        ) as boys_total,
                        sum(
                            IFNULL(e.c1_g,0)+IFNULL(e.c2_g,0)+IFNULL(e.c3_g,0)+
                            IFNULL(e.c4_g,0)+IFNULL(e.c5_g,0)+IFNULL(e.c6_g,0)+
                            IFNULL(e.c7_g,0)+IFNULL(e.c8_g,0)+IFNULL(e.c10_g,0)+
                            IFNULL(e.c11_g,0)+IFNULL(e.c12_g,0)
                        ) as girls_total
                    FROM STEPS_ENROLMENT09 e
                    LEFT JOIN
                        STEPS_SCHOOL s
                            ON
                                e.SCHCD = s.SCHCD
                    LEFT JOIN
                        STEPS_BLOCK b
                            ON
                                e.SCHCD = s.SCHCD
                            AND b.BLKCD = s.BLKCD
                            AND e.AC_YEAR = \'%1$s\'
                            AND ITEMIDGROUP = 1
                            AND CASTEID IN (1,2,3,4)
                    GROUP BY blkcd
                ) AS en ON en.blkcd = b.blkcd
                GROUP BY blkcd;
            ';
        $cySql = sprintf($sql, $cyRange);
        $pySql = sprintf($sql, $pyRange);
        $currentYear = DB::select($cySql);
        $previousYear = DB::select($pySql);
        return View::make('backend.reports.enrolmentComparison', compact('currentYear', 'previousYear', 'cyRange', 'pyRange'));
    }

    function enrolmentByAgeCasteMedium()
    {
        $yearRange = Input::get('year', '');
        if ($yearRange == '' || ( ! preg_match('/^20[1-9]{2}-[1-9]{2}/', $yearRange))) {
            $yearRange = AcademicYear::CURRENT_YEAR;
        }

        $ageQuery = '
            SELECT d.distname, d.distcd, b.blkcd, b.blkname,
                sum(IFNULL(c1b,0)) as c1_boys_total,
                sum(IFNULL(c2b,0)) as c2_boys_total,
                sum(IFNULL(c3b,0)) as c3_boys_total,
                sum(IFNULL(c4b,0)) as c4_boys_total,
                sum(IFNULL(c5b,0)) as c5_boys_total,
                sum(IFNULL(c6b,0)) as c6_boys_total,
                sum(IFNULL(c7b,0)) as c7_boys_total,
                sum(IFNULL(c8b,0)) as c8_boys_total,
                sum(IFNULL(c9b,0)) as c9_boys_total,
                sum(IFNULL(c10b,0)) as c10_boys_total,
                sum(IFNULL(c11b,0)) as c11_boys_total,
                sum(IFNULL(c12b,0)) as c12_boys_total,
                sum(IFNULL(c1g,0)) as c1_girls_total,
                sum(IFNULL(c2g,0)) as c2_girls_total,
                sum(IFNULL(c3g,0)) as c3_girls_total,
                sum(IFNULL(c4g,0)) as c4_girls_total,
                sum(IFNULL(c5g,0)) as c5_girls_total,
                sum(IFNULL(c6g,0)) as c6_girls_total,
                sum(IFNULL(c7g,0)) as c7_girls_total,
                sum(IFNULL(c8g,0)) as c8_girls_total,
                sum(IFNULL(c9g,0)) as c9_girls_total,
                sum(IFNULL(c10g,0)) as c10_girls_total,
                sum(IFNULL(c11g,0)) as c11_girls_total,
                sum(IFNULL(c12g,0)) as c12_girls_total
            FROM STEPS_BLOCK b
            LEFT JOIN
                STEPS_DISTRICT d
                    ON
                        d.DISTCD = LEFT(b.BLKCD, 4)
            LEFT JOIN
                STEPS_KEYVAR k
                    ON
                        k.BLKCD=b.BLKCD
                    AND k.AC_YEAR = \'%1$s\'
            LEFT JOIN
                STEPS_ENRAGEBYCASTE en
                    ON
                        en.SCHCD=k.SCHCD
                    AND en.AC_YEAR=\'%1$s\'
                    AND en.CASTEID=0
            GROUP BY blkcd;
        ';

        $mediumQuery = '
            SELECT b.blkcd, b.blkname,
                sum(IFNULL(c1_b,0)) as c1_boys_total,
                sum(IFNULL(c2_b,0)) as c2_boys_total,
                sum(IFNULL(c3_b,0)) as c3_boys_total,
                sum(IFNULL(c4_b,0)) as c4_boys_total,
                sum(IFNULL(c5_b,0)) as c5_boys_total,
                sum(IFNULL(c6_b,0)) as c6_boys_total,
                sum(IFNULL(c7_b,0)) as c7_boys_total,
                sum(IFNULL(c8_b,0)) as c8_boys_total,
                sum(IFNULL(c9_b,0)) as c9_boys_total,
                sum(IFNULL(c10_b,0)) as c10_boys_total,
                sum(IFNULL(c11_b,0)) as c11_boys_total,
                sum(IFNULL(c12_b,0)) as c12_boys_total,
                sum(IFNULL(c1_g,0)) as c1_girls_total,
                sum(IFNULL(c2_g,0)) as c2_girls_total,
                sum(IFNULL(c3_g,0)) as c3_girls_total,
                sum(IFNULL(c4_g,0)) as c4_girls_total,
                sum(IFNULL(c5_g,0)) as c5_girls_total,
                sum(IFNULL(c6_g,0)) as c6_girls_total,
                sum(IFNULL(c7_g,0)) as c7_girls_total,
                sum(IFNULL(c8_g,0)) as c8_girls_total,
                sum(IFNULL(c9_g,0)) as c9_girls_total,
                sum(IFNULL(c10_g,0)) as c10_girls_total,
                sum(IFNULL(c11_g,0)) as c11_girls_total,
                sum(IFNULL(c12_g,0)) as c12_girls_total
            FROM STEPS_BLOCK b
            LEFT JOIN
                STEPS_KEYVAR k
                    ON
                        k.BLKCD = b.BLKCD
                    AND k.AC_YEAR = \'%1$s\'
            LEFT JOIN
                STEPS_ENRMEDINSTR e
                    ON
                        k.SCHCD = e.SCHCD
                    AND e.AC_YEAR = \'%1$s\'
            GROUP BY blkcd;
        ';

        $casteQuery = '
            SELECT b.BLKCD, b.BLKNAME,
                sum(IFNULL(c1_b,0)) as c1_boys_total,
                sum(IFNULL(c2_b,0)) as c2_boys_total,
                sum(IFNULL(c3_b,0)) as c3_boys_total,
                sum(IFNULL(c4_b,0)) as c4_boys_total,
                sum(IFNULL(c5_b,0)) as c5_boys_total,
                sum(IFNULL(c6_b,0)) as c6_boys_total,
                sum(IFNULL(c7_b,0)) as c7_boys_total,
                sum(IFNULL(c8_b,0)) as c8_boys_total,
                sum(IFNULL(c9_b,0)) as c9_boys_total,
                sum(IFNULL(c10_b,0)) as c10_boys_total,
                sum(IFNULL(c11_b,0)) as c11_boys_total,
                sum(IFNULL(c12_b,0)) as c12_boys_total,
                sum(IFNULL(c1_g,0)) as c1_girls_total,
                sum(IFNULL(c2_g,0)) as c2_girls_total,
                sum(IFNULL(c3_g,0)) as c3_girls_total,
                sum(IFNULL(c4_g,0)) as c4_girls_total,
                sum(IFNULL(c5_g,0)) as c5_girls_total,
                sum(IFNULL(c6_g,0)) as c6_girls_total,
                sum(IFNULL(c7_g,0)) as c7_girls_total,
                sum(IFNULL(c8_g,0)) as c8_girls_total,
                sum(IFNULL(c9_g,0)) as c9_girls_total,
                sum(IFNULL(c10_g,0)) as c10_girls_total,
                sum(IFNULL(c11_g,0)) as c11_girls_total,
                sum(IFNULL(c12_g,0)) as c12_girls_total
            FROM STEPS_BLOCK b
            LEFT JOIN
                STEPS_KEYVAR k
                    ON
                        k.BLKCD = b.BLKCD
                    AND k.AC_YEAR=\'%1$s\'
            LEFT JOIN
                STEPS_ENROLMENT09 e
                    ON
                        k.SCHCD = e.SCHCD
                    AND e.ITEMIDGROUP=1
                    AND e.CASTEID IN (1,2,3,4)
                    AND e.AC_YEAR = \'%1$s\'
            GROUP BY BLKCD;
        ';

        $ageQuery = sprintf($ageQuery, $yearRange);
        $mediumQuery = sprintf($mediumQuery, $yearRange);
        $casteQuery = sprintf($casteQuery, $yearRange);

        $ageData = DB::select($ageQuery);
        $mediumData = DB::select($mediumQuery);
        $casteData = DB::select($casteQuery);
        return View::make('backend.reports.comparative.enrolmentAgeMediumCaste', compact('yearRange', 'ageData', 'mediumData', 'casteData'));
    }

}
