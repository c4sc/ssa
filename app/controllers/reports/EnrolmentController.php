<?php namespace Controller\Reports;

use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use DB;
use View;
use App;
use Response;
use Log;


class EnrolmentController extends AuthorizedController {
    
    function classroomConditions()
    {
        $year = Input::get('year', AcademicYear::CURRENT_YEAR);
        $query = '
        SELECT d.distcd, d.distname, b.blkcd, b.blkname,

        SUM(IFNULL(clrooms, 0)) as classrooms_8,
        SUM(IFNULL(totcls9, 0)) as classrooms_9,
        SUM(IFNULL(totcls10, 0)) as classrooms_10,
        SUM(IFNULL(totcls11, 0)) as classrooms_11,
        SUM(IFNULL(totcls12, 0)) as classrooms_12,

        SUM(
            IFNULL(clgood, 0) + IFNULL(clgood_ppu, 0) + IFNULL(clgood_kuc, 0) + 
            IFNULL(clgood_tnt, 0)
        ) as good_elementary, 
        SUM(IFNULL(clgoods, 0)) as good_secondary,
        SUM(IFNULL(clgoodhs, 0)) as good_hs,

        SUM(
            IFNULL(clmajor, 0) + IFNULL(clmajor_ppu, 0) + IFNULL(clmajor_kuc, 0) + 
            IFNULL(clmajor_tnt, 0)
        ) as major_elementary,
        SUM(IFNULL(clmajors, 0)) as major_secondary,
        SUM(IFNULL(clmajorhs, 0)) as major_hs,

        SUM(
            IFNULL(clminor, 0) + IFNULL(clminor_ppu, 0) + IFNULL(clminor_kuc, 0) + 
            IFNULL(clminor_tnt, 0)
        ) as minor_elementary,
        SUM(IFNULL(clminors, 0)) as minor_secondary,
        SUM(IFNULL(clminorhs, 0)) as minor_hs,

        IFNULL(boys_elementary, 0) as boys_elementary,
        IFNULL(boys_9_to_12, 0) as boys_9_to_12,
        IFNULL(girls_elementary, 0) as girls_elementary,
        IFNULL(girls_9_to_12, 0) as girls_9_to_12

    FROM STEPS_BLOCK b
    LEFT JOIN
        STEPS_DISTRICT d
        ON
            d.DISTCD = LEFT(b.BLKCD, 4)
    LEFT JOIN
        STEPS_KEYVAR k
            ON 
                k.BLKCD = b.BLKCD AND 
                k.AC_YEAR=\'%1$s\'
    LEFT JOIN
        STEPS_BLDEQP bld
            ON
                bld.SCHCD = k.SCHCD AND 
                bld.AC_YEAR = \'%1$s\'
    LEFT OUTER JOIN
        ( SELECT blkcd,
        SUM(
            IFNULL(C1_B, 0) + IFNULL(C2_B, 0) + IFNULL(C3_B, 0) + 
            IFNULL(C4_B, 0) + IFNULL(C5_B, 0) + IFNULL(C6_B, 0) + 
            IFNULL(C7_B, 0) + IFNULL(C8_B, 0)
        ) as boys_elementary,
        SUM(
            IFNULL(C9_B, 0) + IFNULL(C10_B, 0) + IFNULL(C11_B, 0) + 
            IFNULL(C12_B, 0)
        ) as boys_9_to_12,

        SUM(
            IFNULL(C1_G, 0) + IFNULL(C2_G, 0) + IFNULL(C3_G, 0) + 
            IFNULL(C4_G, 0) + IFNULL(C5_G, 0) + IFNULL(C6_G, 0) + 
            IFNULL(C7_G, 0) + IFNULL(C8_G, 0)
        ) as girls_elementary,
        SUM(
            IFNULL(C9_G, 0) + IFNULL(C10_G, 0) + IFNULL(C11_G, 0) + 
            IFNULL(C12_G, 0)
        ) as girls_9_to_12

        FROM STEPS_ENROLMENT09 e
        LEFT JOIN
            STEPS_SCHOOL s
                ON
                    e.SCHCD = s.SCHCD
        WHERE 
            e.AC_YEAR = \'%1$s\' AND
            ITEMIDGROUP = 1 AND 
            CASTEID IN (1,2,3,4) 
       GROUP BY blkcd
        ) AS en ON en.blkcd = b.blkcd
        GROUP BY BLKCD;
        ';
        
        $query = sprintf($query, $year);
        $data = DB::select($query);
        return View::make('backend.reports.enrolment.classCondition', compact('year','data'));
    }
}
