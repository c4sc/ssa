<?php namespace Controller\Reports;

use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use DB;
use View;
use App;
use Response;
use Log;


class SchoolController extends AuthorizedController
{
    /**
     * number of school having facilities
     */
    function havingFacilities()
    {
        $year = Input::get('year', AcademicYear::CURRENT_YEAR);
        if ($year == '') $year = AcademicYear::CURRENT_YEAR;

        $query = '
            SELECT d.distname, b.blkcd, b.blkname,
                COUNT(k.SCHCD) as total_schools,
                SUM(IF(e.bldstatus<5,1,0)) as buildings,
                SUM(IF(
                    IFNULL(e.clrooms,0)+
                    IFNULL(e.totcls9,0)+
                    IFNULL(e.totcls10,0)+
                    IFNULL(e.totcls11,0)+
                    IFNULL(e.totcls12,0)>0, 1, 0)) as class_rooms,
                SUM(IF(e.land4cls_yn=1,1,0)) as land,
                SUM(IF(e.hmroom_yn=1,1,0)) as hm_room,
                SUM(IF(e.toiletb>0,1,0)) as toilet_boys,
                SUM(IF(e.toilet_g>0,1,0)) as toilet_girls,
                SUM(IF(e.toiletd=1,1,0)) as toilet_cwsn,
                SUM(IF(e.othrooms>0,1,0)) as other_rooms,
                SUM(IF(e.handwash_yn=1,1,0)) as handwash,
                SUM(IF(e.water_func_yn=1,1,0)) as water,
                SUM(IF(e.electric_yn=1,1,0)) as electric,
                SUM(IF(e.pground_yn=1,1,0)) as playground,
                SUM(IF(e.ramps_yn=1,1,0)) as ramps,
                SUM(IF(e.medchk_yn=1,1,0)) as medical_checkup,
                SUM(IF(e.computer>0,1,0)) as computer,
                SUM(IF(e.library_yn=1,1,0)) as library,
                SUM(IF(m.kitshed=1,1,0)) as kitchen_shed,
                SUM(IF(m.kitshed=4,1,0)) as class_as_kitchen_shed
            FROM `STEPS_BLOCK` b
            LEFT JOIN STEPS_DISTRICT d
                ON
                    d.DISTCD = LEFT(b.BLKCD, 4)
            LEFT JOIN STEPS_KEYVAR k
                ON
                    k.blkcd = b.blkcd AND
                    k.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_BLDEQP e
                ON
                    e.schcd = k.schcd AND
                    e.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_MDM m
                ON
                    m.schcd = k.schcd AND
                    m.AC_YEAR = \'%1$s\'
            GROUP BY blkcd;
        ';

        $query = sprintf($query, $year);
        $data = DB::select($query);
        return View::make('backend.reports.school.havingFacilities', compact('year', 'data'));
    }

    public function noReponseInData()
    {
        $year = Input::get('year', AcademicYear::CURRENT_YEAR);
        if ($year == '') $year = AcademicYear::CURRENT_YEAR;

        $query = '
            SELECT d.distname, b.blkcd, b.blkname,
                COUNT(k.SCHCD) as total_schools,
                sum(CASE WHEN master.SCHCAT IS NULL THEN 1 ELSE 0 END) as school_category,
                SUM(CASE WHEN k.rururb IS NULL THEN 1 ELSE 0 END) as rural_urban,
                SUM(CASE WHEN m.mealsinsch IS NULL THEN 1 ELSE 0 END) as mdm,
                SUM(CASE WHEN m.kitdevgrant_yn IS NULL THEN 1 ELSE 0 END) as kitchen_device,
                SUM(CASE WHEN e.electric_yn IS NULL THEN 1 ELSE 0 END) as electricity,
                SUM(CASE WHEN r.TXTBKRECD_YN IS NULL THEN 1 ELSE 0 END) as textbook,
                SUM(CASE WHEN e.pground_yn IS NULL THEN 1 ELSE 0 END) as playground,
                SUM(CASE WHEN e.RAMPSNEEDED_YN IS NULL THEN 1 ELSE 0 END) as ramp,
                SUM(CASE WHEN e.MEDCHK_YN IS NULL THEN 1 ELSE 0 END) as medical_checkup,
                SUM(CASE WHEN e.FURNSTU IS NULL THEN 1 ELSE 0 END) as furniture_elementary,
                SUM(CASE WHEN e.BNDRYWALL IS NULL THEN 1 ELSE 0 END) as boundary_wall,
                SUM(CASE WHEN m.KITSHED IS NULL THEN 1 ELSE 0 END) as kitchen_shed,
                SUM(CASE WHEN e.water IS NULL THEN 1 ELSE 0 END) as water

            FROM `STEPS_BLOCK` b
            LEFT JOIN STEPS_DISTRICT d
                ON
                    d.DISTCD = LEFT(b.BLKCD, 4)
            LEFT JOIN STEPS_KEYVAR k
                ON
                    k.blkcd = b.blkcd AND
                    k.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_MASTER master
                ON
                    master.schcd =k.schcd AND
                    master.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_RTEINFO r
                ON
                    r.schcd =k.schcd AND
                    r.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_BLDEQP e
                ON
                    e.schcd = k.schcd AND
                    e.AC_YEAR = \'%1$s\'
            LEFT JOIN STEPS_MDM m
                ON
                    m.schcd = k.schcd AND
                    m.AC_YEAR = \'%1$s\'
            GROUP BY blkcd;
            ';

        $query = sprintf($query, $year);
        $data = DB::select($query);
        return View::make('backend.reports.school.noResponseInData', compact('year', 'data'));
    }
}

