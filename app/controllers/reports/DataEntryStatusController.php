<?php namespace Controller\Reports;

use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use DB;
use View;
use App;
use Response;
use Log;
use SchoolDetails;
use Link;

class DataEntryStatusController extends AuthorizedController
{

    /**
     * listing districts, totalschools, number of schools logged-in, number of schools saved particulars-one
     */
    public function stateLevel()
    {
        $query = '
            SELECT
                d.distcd, d.distname,
                s.total_schools,
                l.schools_logged_in,
                p.schools_save_first_form
            FROM STEPS_DISTRICT d
            LEFT OUTER JOIN
                (SELECT count(ds.schcd) as total_schools, ds.distcd
                FROM
                (SELECT distinct schcd, distcd
                    FROM STEPS_KEYVAR
                    WHERE ac_year in (\'%1$s\', \'%2$s\')
                ) as ds
                group by distcd
            ) as s ON s.distcd = d.distcd
            LEFT OUTER JOIN
                ( SELECT distcd, count(school_code) as schools_logged_in
                FROM school_status ss
                LEFT JOIN
                    STEPS_SCHOOL s
                        ON
                    ss.school_code = s.SCHCD
                WHERE
                    ss.link_id = 35 AND
                    ss.academic_year = \'%1$s\'
               GROUP BY distcd) AS l ON l.distcd= d.distcd
            LEFT OUTER JOIN
                ( SELECT distcd, count(school_code) as schools_save_first_form
                FROM school_status ss
                LEFT JOIN
                    STEPS_SCHOOL s
                        ON
                    ss.school_code = s.SCHCD
                WHERE
                    ss.link_id = 1 AND
                    ss.academic_year = \'%1$s\'
               GROUP BY distcd
                ) AS p ON p.distcd = d.distcd
            GROUP BY distcd;
        ';

        $query = sprintf($query, AcademicYear::CURRENT_YEAR, AcademicYear::PREVIOUS_YEAR);
        $stateLevelStatistics = DB::select($query);
        return View::make('backend.reports.dataEntryStatus.stateLevel', compact('stateLevelStatistics'));
    }

    public function districtLevel($districtCode)
    {
        $query = '
            SELECT
                b.blkcd, b.blkname,
                s.total_schools ,
                l.schools_logged_in,
                p.schools_save_first_form
            FROM STEPS_BLOCK b
            LEFT OUTER JOIN
                (SELECT count(ds.schcd) as total_schools, ds.blkcd
                FROM
                (SELECT distinct schcd, blkcd
                    FROM STEPS_KEYVAR
                    WHERE ac_year in (\'%2$s\', \'%3$s\') AND
                    distcd = \'%1$s\'
                ) as ds
                group by blkcd
            ) as s ON s.blkcd = b.blkcd
            LEFT OUTER JOIN
                ( SELECT blkcd, count(school_code) as schools_logged_in
                FROM school_status ss
                LEFT JOIN
                    STEPS_SCHOOL s
                    ON
                    ss.school_code = s.SCHCD AND
                    s.distcd = \'%1$s\'
                WHERE
                    ss.link_id = 35 AND
                    ss.academic_year = \'%2$s\'
                GROUP BY blkcd
                ) AS l ON l.blkcd = b.blkcd
            LEFT OUTER JOIN
                ( SELECT blkcd, count(school_code) as schools_save_first_form
                FROM school_status ss
                LEFT JOIN
                    STEPS_SCHOOL s
                    ON
                    ss.school_code = s.SCHCD AND
                    s.distcd = \'%1$s\'
                WHERE
                    ss.link_id = 1 AND
                    ss.academic_year = \'%2$s\'
                GROUP BY blkcd
                ) AS p ON p.blkcd = b.blkcd
            where left(b.blkcd, 4) = \'%1$s\'
            GROUP BY b.blkcd
            ';
        $query = sprintf($query, $districtCode, AcademicYear::CURRENT_YEAR, AcademicYear::PREVIOUS_YEAR);
        $districtLevelStatistics = DB::select($query);
        return View::make('backend.reports.dataEntryStatus.districtLevel', compact('districtLevelStatistics'));
    }

    public function blockLevel($blockCode)
    {
        $schools = SchoolDetails::select('SCHCD', 'BLKCD', 'SCHNAME', 'SCHTYPE', 'SCHMGT', 'SCHCAT')
            ->join('school_status', 'school_status.school_code', '=', 'STEPS_KEYVAR.SCHCD')
            ->where('BLKCD', '=', $blockCode)
            ->where('AC_YEAR', AcademicYear::CURRENT_YEAR)
            ->orderBy('SCHCD')
            ->groupBy('SCHCD')
            ->paginate(50);

        $vari = array();
        $schoolCount = count($schools);

        for ($i=0; $i<$schoolCount; $i++) {
            $vari[]= $schools[$i]->SCHCD;
        }

        if($schoolCount) {
            $statics = Link::select('links.route', 'school_status.school_code')
                ->join('school_status','links.id','=','school_status.link_id')
                ->where('links.category','=','U')
                ->where('school_status.academic_year','=',AcademicYear::CURRENT_YEAR)
                ->whereIn('school_status.school_code', $vari)
                ->whereIn('links.route', array('particulars-one', 'facilities-one'))
                ->get();
            }
        return View::make('backend.reports.dataEntryStatus.blockLevel',
            compact('schools', 'statics', 'schoolCount'));
    }
}
