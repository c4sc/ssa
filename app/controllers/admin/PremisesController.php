<?php namespace Controllers\Admin;

use Illuminate\Routing\Controller;
use AuthorizedController;
use Sentry;
use Validator;
use School;
use Master;
use District;
use Block;
use EducationBlock;
use Constituency;
use Municipality;
use Village;
use Cluster;
use Panchayath;
use City;
use Habitation;
use BlockVillage;
use BlockCity;
use BlockMunicipality;
use BlockConstituency;
use BlockCluster;
use BlockHabitation;
use BlockAeo;
use BlockPanchayat;
use DB;
use View;
use Input;
use Response;
use Redirect;

class PremisesController extends AuthorizedController
{
    /**
     * BRC premises
     */
    public function getPremises($blockCode)
    {
        $user = Sentry::getUser();
        if($blockCode == '' && $user->scope == self::$SCOPE_BLOCK) {
            $blockCode = $user->scope_id;
        }

        $block = Block::find($blockCode);
        $district = substr($blockCode, 0, 4);

        $villages = Village::where('VILCD', 'LIKE', "{$district}%")
            ->orderBy('VILNAME')
            ->select(DB::raw('CONCAT(VILNAME,  " - ", VILCD) as name'), 'VILCD')
            ->lists('name', 'VILCD');

        $clusters = Cluster::where('CLUCD', 'LIKE', "{$district}%")->orderBy('CLUNAME')
            ->select(DB::raw('CONCAT(CLUNAME, " - ", CLUCD) as name'), 'CLUCD')
            ->lists('name','CLUCD');

        $panchayaths = Panchayath::where('PANCD', 'LIKE', "{$district}%")->orderBy('PANNAME')
            ->select(DB::raw('CONCAT(PANNAME, " - ", PANCD) as name'), 'PANCD')
            ->lists('name', 'PANCD');

        $habitations = Habitation::where('HABCD', 'LIKE', "{$district}%")->orderBy('HABNAME')
            ->select(DB::raw('CONCAT(HABNAME, " - ", HABCD) as name'), 'HABCD')
            ->lists('name', 'HABCD');

        $aeos = EducationBlock::where('BLKCD', 'LIKE', "{$district}%")->orderBy('BLKNAME')
            ->select(DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name'), 'BLKCD')
            ->lists('name', 'BLKCD');

        $constituencies= Constituency::where('CONSTCD', 'LIKE', "{$district}%")->orderBy('CONSTNAME')
            ->select(DB::raw('CONCAT(CONSTNAME, " - ", CONSTCD) name'), 'CONSTCD')
            ->lists('name', 'CONSTCD');

        $municipalities = Municipality::where('MUNCD', 'LIKE', "{$district}%")->orderBy('MUNNAME')
            ->select(DB::raw('CONCAT(MUNNAME, " - ", MUNCD) as name'), 'MUNCD')
            ->lists('name', 'MUNCD');

        $cities = City::where('CITYCD', 'LIKE', "{$district}%")->orderBy('CITYNAME')
            ->select(DB::raw('CONCAT(CITYNAME, " - ", CITYCD) as name'), 'CITYCD')
            ->lists('name', 'CITYCD');

        $blockVillages = BlockVillage::where('BLKCD', '=', $blockCode)->lists('VILCD');
        $blockCities = BlockCity::where('BLKCD', '=', $blockCode)->lists('CITYCD');
        $blockMunicipalities = BlockMunicipality::where('BLKCD', '=', $blockCode)->lists('MUNCD');
        $blockPanchayaths = BlockPanchayat::where('BLKCD', '=', $blockCode)->lists('PANCD');
        $blockAeos = BlockAeo::where('BLKCD', '=', $blockCode)->lists('aeo_id');
        $blockClusters = BlockCluster::where('BLKCD', '=', $blockCode)->lists('CLUCD');
        $blockHabitations = BlockHabitation::where('BLKCD', '=', $blockCode)->lists('HABCD');
        $blockConstituencies = BlockConstituency::where('BLKCD', '=', $blockCode)->lists('CONSTCD');
        return View::make('backend.premises.brc', compact('cities', 'block',
            'villages', 'blockVillages',
            'constituencies', 'blockConstituencies',
            'aeos', 'blockAeos',
            'panchayaths', 'blockPanchayaths',
            'municipalities', 'blockMunicipalities',
            'clusters', 'blockClusters',
            'habitations', 'blockHabitations',
            'cities', 'blockCities'));

    }

    public function postPremises($blockCode)
    {
        $user = Sentry::getUser();
        $block = Block::find($blockCode);

        BlockVillage::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('villages', array()) as $village) {
            $data[] = array('VILCD' => $village, 'BLKCD' => $blockCode);
        }
        BlockVillage::insert($data);


        BlockCluster::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('clusters', array()) as $cluster) {
            $data[] = array('CLUCD' => $cluster, 'BLKCD' => $blockCode);
        }
        BlockCluster::insert($data);


        BlockHabitation::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('habitations', array()) as $habitation) {
            $data[] = array('HABCD' => $habitation, 'BLKCD' => $blockCode);
        }
        BlockHabitation::insert($data);

        BlockMunicipality::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('municipalities', array()) as $premise) {
            $data[] = array('MUNCD' => $premise, 'BLKCD' => $blockCode);
        }
        BlockMunicipality::insert($data);

        BlockPanchayat::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('panchayaths', array()) as $premise) {
            $data[] = array('PANCD' => $premise, 'BLKCD' => $blockCode);
        }
        BlockPanchayat::insert($data);

        BlockCity::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('cities', array()) as $premise) {
            $data[] = array('CITYCD' => $premise, 'BLKCD' => $blockCode);
        }
        BlockCity::insert($data);


        BlockAeo::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('aeos', array()) as $premise) {
            $data[] = array('aeo_id' => $premise, 'BLKCD' => $blockCode);
        }
        BlockAeo::insert($data);


        BlockConstituency::where('BLKCD', '=', $blockCode)->delete();
        $data = array();
        foreach(Input::get('constituencies', array()) as $premise) {
            $data[] = array('CONSTCD' => $premise, 'BLKCD' => $blockCode);
        }
        BlockConstituency::insert($data);

        return Redirect::route('brc-premises', $blockCode)->with('success', 'Saved');

    }
}
