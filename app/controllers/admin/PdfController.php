<?php namespace Controllers\Admin;

use AuthorizedController;
use Illuminate\Routing\Controller;
use Input;
use Lang;
use PDF;
use App;
use User;
use AcademicYear;
use School;
use Master;
use District;
use Block;
use EducationBlock;
use Constituency;
use Municipality;
use Village;
use Cluster;
use Panchayath;
use City;
use Habitation;
use Building;
use Facility;
use FacilityDetail;
use FirstGrade;
use Enrolment;
use MediumEnrolment;
use AgeEnrolment;
use Cwsn;
use CwsnFacility;
use Incentive;
use Repeater;
use RmsaFund;
use StreamEnrolment;
use Staff;
use Result;
use Mdm;
use Teacher;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Rte;
use Material;
use Smdc;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PdfController extends AuthorizedController
{
    public $choice = array(
        0 => '',
        1 => '1 - Yes',
        2 => '2 - No',
        '' => ''
    );

    public $emptyValues = array(
        0 => '',
        '' => '',
        98 => ''
    );

    function getPageOne($schoolCode)
    {
        return $this->getView(False, $schoolCode);
    }

    function getPdfOne($schoolCode)
    {
        return $this->getView(True, $schoolCode);
    }

    function getPageTwo($schoolCode)
    {
        return $this->getLandScapeView(False, $schoolCode);
    }

    function getPdfTwo($schoolCode)
    {
        return $this->getLandScapeView(True, $schoolCode);
    }

    function getView($download, $schoolCode)
    {

        $user = Sentry::getUser();
        if($schoolCode == '' && $user->scope == self::$SCOPE_SCHOOL) {
            $schoolCode = $user->scope_id;
        }
        // particulars one
        $school = School::find($schoolCode);
        $district = substr($schoolCode, 0, 4);
        $master =  Master::findRecord($schoolCode);

        $village = Village::where('VILCD', '=', $master->VILCD)
            ->first(array('VILCD as id', DB::raw('CONCAT(VILNAME, " - ", VILCD) as name')));
        $cluster = Cluster::where('CLUCD', '=', $master->CLUCD)
            ->first(array('CLUCD as id', DB::raw('CONCAT(CLUNAME, " - ", CLUCD) as name')));
        $habitation = Habitation::where('HABCD', '=', $master->HABITCD)
            ->first(array('HABCD as id', DB::raw('CONCAT(HABNAME, " - ", HABCD) as name')));
        $eduBlock = EducationBlock::where('BLKCD', '=', $master->EDUBLKCD)
            ->first(array('BLKCD as id', DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name')));
        $block = Block::where('BLKCD', '=', $master->BLKCD)
            ->first(array('BLKCD as id', DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name')));
        $constituency = Constituency::where('CONSTCD', '=', $master->ACONSTCD)
            ->first(array('CONSTCD as id', DB::raw('CONCAT(CONSTNAME, " - ", CONSTCD) as name')));
        $municipality = Municipality::where('MUNCD', '=', $master->MUNICIPALCD)
            ->first(array('MUNCD as id', DB::raw('CONCAT(MUNNAME, " - ", MUNCD) as name')));
        $city = City::where('CITYCD', '=', $master->CITY)
            ->first(array('CITYCD as id', DB::raw('CONCAT(CITYNAME, " - ", CITYCD) as name')));
        $panchayat = Panchayath::where('PANCD', '=', $master->PANCD)
            ->first(array('PANCD as id', DB::raw('CONCAT(PANNAME, " - ", PANCD) as name')));

        $religiousMinorities = Master::$RELIGIOUS_MINORITIES+$this->emptyValues;
        $languages = DB::table('STEPS_MEDINSTR')->lists('MEDINSTR_DESC', 'MEDINSTR_ID')+$this->emptyValues;;

        //particulars three
        $rte =  Rte::findRecord($schoolCode);
        // Text Book info
        $materialBook =  Material::findRecord($schoolCode, 'book');
        // TLE info
        $materialTle =  Material::findRecord($schoolCode, 'tle');
        // Sports info
        $materialSports =  Material::findRecord($schoolCode, 'sports');

        // physical facilities
        if($master->LOWCLASS < 9) {
            $sections[] = 0;
        }
        if($master->hasSecondary()) {
            $sections[] = 1;
            $sectionNames[] = 'secondary';
        }
        if($master->hasHigherSecondary()) {
            $sections[] = 2;
            $sectionNames[] = 'hs';
        }
        $building =  Building::findRecord($schoolCode);

        // physical facilities two
        if( ! empty($sectionNames)) {

            $regexp = implode ("|", $sectionNames);
            $group1 = FacilityDetail::where('group_id', '=', 1)->where('available_for', 'REGEXP', $regexp)->get();
            $group2 = FacilityDetail::where('group_id', '=', 2)->where('available_for', 'REGEXP', $regexp)->get();
            $group3 = FacilityDetail::where('group_id', '=', 3)->where('available_for', 'REGEXP', $regexp)->get();
            $groupValue1 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 1)
                ->whereIn('ITEMID', $group1->lists('facility_id'))
                ->orderBy('ITEMID')->get();

            $groupValue2 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 2)
                ->whereIn('ITEMID', $group2->lists('facility_id'))
                ->orderBy('ITEMID')->get();

            $groupValue3 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 3)
                ->whereIn('ITEMID', $group3->lists('facility_id'))
                ->orderBy('ITEMID')->get();
        }

        //mdm
        $mdm = Mdm::findRecord($schoolCode);

        //smdc
        $smdc =  Smdc::findRecord($schoolCode);

        //firstgrade
        $firstGrade = FirstGrade::findRecord($schoolCode);
        $firstGradeAgeList = array(
            4 => 'less than 5',
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 'greater than 7'
        );

        // enrolment by social category
        $categoryData = Enrolment::findRecords($schoolCode)
            ->whereIn('CASTEID', array(1, 2, 3, 4))
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('CASTEID')
            ->get();
        $divisions = Enrolment::getDivisions($schoolCode);

        // enrolment by minority
        $minorityData = Enrolment::findRecords($schoolCode)
            ->whereIn('CASTEID', array(5, 6, 7, 8, 9, 10))
            ->where('ITEMIDGROUP', '=', 2)
            ->orderBy('CASTEID')
            ->get();
        $sum = Enrolment::sum($schoolCode, 1);

        // medium enrolment
        $schoolMediums =  MediumEnrolment::getSchoolMediums($schoolCode);
        $mediumData = [];

        foreach($schoolMediums as  $index => $medium) {
            $newMedium = MediumEnrolment::findRecords($schoolCode)
                ->where('MEDIUM', '=', $medium['id'])->first();

            if( ! isset($newMedium->SEQNO)) {
                // deleting existing sequence data
                MediumEnrolment::findRecords($schoolCode)
                    ->where('SEQNO', '=', $index+1)->delete();
                // creating new medium-enrolment
                $newMedium = new MediumEnrolment;
                $newMedium->AC_YEAR = AcademicYear::CURRENT_YEAR;
                $newMedium->SCHCD = $schoolCode;
                $newMedium->MEDIUM = $medium['id'];
                $newMedium->SEQNO = $index+1;
                $newMedium->save();

            } elseif ($newMedium->SEQNO != ($index+1)) {

                // correcting sequence number
                MediumEnrolment::updateMediumEnrolment(array('SEQNO'=>$index+1), $medium['id'], $schoolCode);
            }
            $mediumData[] = $newMedium;
        }

        // repeaters by social category
        $repeatersData = Repeater::findRecords($schoolCode)
            ->whereIn('ITEMID', array(1, 2, 3, 4))
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('ITEMID')
            ->get();

        // repeater by minority
        $repeatersMinorityData = Repeater::findRecords($schoolCode)
            ->whereIn('ITEMID', array(5, 6, 7, 8, 9, 10))
            ->where('ITEMIDGROUP', '=', 2)
            ->orderBy('ITEMID')
            ->get();

        // facilities - cwsn
        $cwsnFacilityData = CwsnFacility::findRecords($schoolCode)
            ->whereIn('ITEMID', range(1, 9))
            ->orderBy('ITEMID')
            ->get();

        // facilities to children
        $childrensFacility = [];
        $facilitiesLabel = [];

        if($master->hasPrimary()) {

            $facilitiesLabel[] = 'Facilities provided to Children(Last academic Year,Primary Classes)';
            $childrensFacility[] = Incentive::findRecords($schoolCode)
                ->whereIn('INCENT_TYPE', range(1, 6))
                ->where('GRADEPRUPR', '=', 5)
                ->orderBy('INCENT_TYPE')
                ->get();
        }
        if( $master->hasUpperPrimary()) {

            $facilitiesLabel[] = 'Facilities provided to Children(Last academic Year,Upper Primary Classes)';
            $childrensFacility[] = Incentive::findRecords($schoolCode)
                ->whereIn('INCENT_TYPE', range(1, 6))
                ->where('GRADEPRUPR', '=', 5)
                ->orderBy('INCENT_TYPE')
                ->get();
        }

        // exam results of 4/5, 7/8
        $resultsElementary = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 6)
            ->whereIn('ITEMID', range(1, 4))
            ->orderBy('ITEMID')
            ->get();

        $classes = array();
        if (($master->LOWCLASS <= 5 && $master->HIGHCLASS >= 5) || ($master->LOWCLASS <= 4 && $master->HIGHCLASS >= 4)){
            $classes[] = array('id'=>'5', 'name'=>'IV/V');
        }
        if (($master->LOWCLASS <= 7 && $master->HIGHCLASS >= 7) || ($master->LOWCLASS <= 8 && $master->HIGHCLASS >= 8)){
            $classes[] = array('id'=>'8', 'name'=>'VII/VIII');
        }

        // streams available
        $streamData = Facility::findRecords($schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->where('ITEMIDGROUP', '=', 4)
            ->orderBy('ITEMID')
            ->get();

        // stream enrolment
        // repeaters by stream
        $streams = Facility::getStreamList($schoolCode);
        // repeaters and enrolment uses same row for storing values
        $streamEnrolmentData = StreamEnrolment::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', '1')
            ->whereIn('CASTEID', range(1, 4))
            ->orderBy('STREAMID')
            ->orderBy('CASTEID')
            ->get();

        // staff Details
        $staffData = Staff::findRecords($schoolCode)
            ->orderBy('DESIG_ID')
            ->get();

        // X result by category
        $xResults = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('ITEMID')
            ->get();

        // result by score
        $scoreResults = [];
        $resultLabel = [];
        if( $master->hasSecondary()) {
            $scoreResults[] = Result::findRecords($schoolCode)
                ->where('ITEMIDGROUP', '=', 3)
                ->orderBy('ITEMID')
                ->get();
            $resultLabel[] = array(
                'label' => "Number of students passed/qualified the Secondary School Board (Class X) examination in previous academic year",
                'class' => 'secondary');
        }

        if( $master->hasHigherSecondary()) {
            $scoreResults[] = Result::findRecords($schoolCode)
                ->where('ITEMIDGROUP', '=', 5)
                ->orderBy('ITEMID')
                ->get();
            $resultLabel[] = array(
                'label' => "Result of class XII previous year -- by percentage of mark",
                'class' => 'hs');
        }

        // result by stream
        if(count($streams)) {
            $streamResults = Result::findRecords($schoolCode)
                ->where('ITEMIDGROUP', '=', 4)
                ->whereIn('ITEMID', $streams)
                ->orderBy('ITEMID')
                ->get();
        }

        // rmsa fund
        $fundData = RmsaFund::findRecords($schoolCode)->orderBy('ITEMID')->get();

        //teachers
        $teachers = Teacher::where('SCHCD', '=', $school->SCHCD)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->get();

        // choice fields
        $mediums = Master::$INSTRUCTION_MEDIUMS+$this->emptyValues;
        $types = School::$TYPES+$this->emptyValues;
        $areas = School::$AREA+$this->emptyValues;
        $managements = School::$MANAGEMENTS+$this->emptyValues;
        $mediums[98] = '';
        $boards = Master::$BOARDS+$this->emptyValues;
        $facilityChoice = Facility::$FACILITY_STATUS + $this->emptyValues;
        $facilityChoice[0] = '';
        $choice = $this->choice;

        $data = compact(
                'school', 'master', 'rte', 'materialBook', 'materialTle', 'materialSports', 'building', 'sections','smdc',
                'firstGrade', 'firstGradeAgeList', 'categoryData', 'minorityData', 'sum', 'schoolMediums', 'mediumData',
                'repeatersData','repeatersMinorityData', 'cwsnFacilityData', 'facilitiesLabel', 'childrensFacility',
                'streamData', 'streamEnrolmentData', 'streamRepeatersData', 'streams', 'staffData', 'divisions',
                'resultData', 'scoreResults', 'resultLabel','xResults', 'streamResults', 'fundData', 'teachers',
                'group1', 'group2', 'group3', 'groupValue1', 'groupValue2', 'groupValue3', 'mdm',
                'district', 'village', 'cluster', 'habitation', 'eduBlock', 'block', 'constituency',
                'municipality', 'city', 'panchayat', 'religiousMinorities', 'languages',
                'facilityChoice', 'choice', 'mediums', 'boards', 'types', 'areas', 'managements',
                'classes', 'resultsElementary'
            );

        if( ! $download) {
            return View::make('backend.pdf.singlePage', $data);
        }

        $pdf = PDF::loadView('backend.pdf.singlePage', $data);
        return $pdf->setPaper('a4')->setOption('margin-top', '5')->setOption('margin-left', 0)->setOption('margin-right', 0)->stream();
    }

    /*
     * for enrolment by age and children with special needs
     */
    function getLandScapeView($download, $schoolCode)
    {

        $user = Sentry::getUser();
        if($schoolCode == '' && $user->scope == self::$SCOPE_SCHOOL) {
            $schoolCode = $user->scope_id;
        }
        // particulars one
        $school = School::find($schoolCode);
        $district = substr($schoolCode, 0, 4);
        $master =  Master::findRecord($schoolCode);

        // physical facilities
        if($master->LOWCLASS < 9) {
            $sections[] = 0;
        }
        if($master->hasSecondary()) {
            $sections[] = 1;
        }
        if($master->hasHigherSecondary()) {
            $sections[] = 2;
        }

        $sum = Enrolment::sum($schoolCode, 1);

        // age enrolment
        $ageData = AgeEnrolment::findRecords($schoolCode)
            ->where('CASTEID', '=', 0)
            ->orderBy('AGEID')
            ->get();

        $disabilityData = Cwsn::findRecords($schoolCode)
            ->whereIn('DISABILITY_TYPE', range(1, 10))
            ->orderBy('DISABILITY_TYPE')
            ->get();

        // choice fields
        $choice = $this->choice;

        $data = compact('master', 'ageData', 'choice', 'sum', 'disabilityData');

        if( ! $download) {
            return View::make('backend.pdf.secondPage', $data);
        }

        $pdf = PDF::loadView('backend.pdf.secondPage', $data);
        return $pdf->setPaper('a4')->setOrientation('landscape')->setOption('margin-top', 5)->setOption('margin-left', 0)->setOption('margin-right', 0)->stream();
    }

}
