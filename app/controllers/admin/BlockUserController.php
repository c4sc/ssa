<?php

use Illuminate\Routing\Controller;
use Cartalyst\Sentry\Users\UserExistsException;

class BlockUserController extends AuthorizedController {

    public function __construct()
    {
        // Call parent
        parent::__construct();
    }

	/**
	 * Show the form for creating a new BlockUser.
	 * GET /user/block/create
	 *
	 * @return View
	 */
	public function create()
	{
        $groupList = Group::where('scope', '=', self::$SCOPE_BLOCK)->lists('name', 'id');
        return View::make('backend.blockUsers.edit')
            ->with('user', new User)
            ->with('groupList', $groupList);
	}

	/**
	 * Store a newly created BlockUser
	 * POST /user/block/create
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user-group' => 'required',
            'block-code' => 'required|digits:6|exists:STEPS_BLOCK,BLKCD',
        );
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()){
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $user = Sentry::getUser();
        $block = Block::find(e(Input::get('block-code')));
        if( ! $user->hasAccessToBlock($block, 'stateAdmin')) {
            return Redirect::route('block-user-create')->with('error', Lang::get('admin/messages.permission_error'));
        }

        try {

            $inputs = Input::only('email', 'password');
            $inputs['activated'] = 1;
            $inputs['scope'] = self::$SCOPE_BLOCK;
            $inputs['scope_id'] = e(Input::get('block-code'));

            if ($user = Sentry::getUserProvider()->create($inputs)) {
                $group = Sentry::findGroupById(e(Input::get('user-group')));
                $user->addGroup($group);
                return Redirect::route('block-user-create')->with('success', Lang::get('admin/user.create.success'));
            }
        } catch (UserExistsException $e) {
            return Redirect::route('block-user-create')->with('error', Lang::get('admin/user.exists'));
        }

	}

    /**
     * show user
     *
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $me->hasAccessToUser($user, 'blockAdmin')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }
        $block = Block::find($user->scope_id);
        return View::make('backend.blockUsers.show', compact('user', 'block'));
    }

	/**
	 * Show the form for editing blockUser
	 * GET /user/block/{id}/edit
	 *
	 * @return View
	 */
	public function edit($id)
	{
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $me->hasAccessToUser($user, 'schoolReport')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }

        $groupList = [""=>"Select User Group"] + Group::where('scope', '=', self::$SCOPE_DISTRICT)->lists('name', 'id');
        $group = $user->getGroups()[0];

        return View::make('backend.blockUsers.edit', compact('user', 'group'))
            ->with('groupList', $groupList);
	}

	/**
	 * updates BlockUser
	 *
	 * @return Response
	 */
	public function postUpdate($id)
	{
        $me = Sentry::getUser();
        $user = Sentry::findUserById($id);
        $rules = array(
            'email' => "required|email|unique:users,email,$id",
            'user-group' => 'required',
            'block-code' => 'required|digits:6|exists:STEPS_BLOCK,BLKCD',
        );
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()){
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        if( ! $me->hasAccess('stateAdmin')) {
            return Redirect::route('block-user-edit', $id)->with('error', Lang::get('admin/messages.permission_error'));
        }

        // removing current groups
        foreach($user->getGroups() as $userGroup) {
            $user->removeGroup($userGroup);
        }

        // updating user
        $user->email = e(Input::get('email'));
        $user->scope_id = e(Input::get('block-code'));
        $group = Sentry::findGroupById(e(Input::get('user-group')));
        $user->addGroup($group);

        if ($user->save()) {
            return Redirect::route('block-user-edit', $id)->with('success', Lang::get('admin/user.update.success'));
        }
	}

}
