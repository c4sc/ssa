<?php

use Illuminate\Routing\Controller;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Groups;

class SchoolUserController extends AuthorizedController {

    public function __construct()
    {
        // Call parent
        parent::__construct();
    }

	/**
	 * Show the form for creating a new SchoolUser.
	 * GET /user/school/create
	 *
	 * @return View
	 */
	public function create()
	{
        $groupList = Group::where('scope', '=', self::$SCOPE_SCHOOL)->lists('name', 'id');
        return View::make('backend.schoolUsers.edit')
            ->with('user', new User)
            ->with('groupList', $groupList);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function search()
	{
        $query = e(Input::get('q', ''));

        $me = Sentry::getUser();
        $users = new User;

        switch($me->scope) {
            case self::$SCOPE_SCHOOL:
            case self::$SCOPE_BLOCK:
            case self::$SCOPE_DISTRICT:
                $users = $users->where('scope_id', 'like', $me->scope_id . "%");
            case self::$SCOPE_STATE:
                break;
            default:
                return Redirect::route('logout');
                break;
        }
        if ( ! $query) return View::make('backend.schoolUsers.index');

        $users = $users->where('email', 'LIKE', "%$query%")->paginate(20);
        return View::make('backend.schoolUsers.index', compact('users', 'query'));
	}

	/**
	 * Store a newly created SchoolUser
	 * POST /user/school/create
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user-group' => 'required',
            'school-code' => 'required|digits:11|exists:STEPS_KEYVAR,SCHCD',
        );
        $user = Sentry::getUser();
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }
        $school = School::find(e(Input::get('school-code')));
        if( ! $user->hasAccessToSchool($school, 'schoolAdmin')) {
            return Redirect::route('school-user-create')->with('error', Lang::get('admin/messages.permission_error'));
        }

        try {

            $inputs = Input::only('email', 'password');
            $inputs['activated'] = 1;
            $inputs['scope_id'] = e(Input::get('school-code'));
            $inputs['scope'] = self::$SCOPE_SCHOOL;

            if ($user = Sentry::getUserProvider()->create($inputs)) {
                $group = Sentry::findGroupById(e(Input::get('user-group')));
                $user->addGroup($group);
                return Redirect::route('school-user-create')->with('success', Lang::get('admin/user.create.success'));
            }
        } catch (UserExistsException $e) {
            return Redirect::route('school-user-create')->with('error', Lang::get('admin/user.exists'));
        }

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        //checking permission
        if( ! $me->hasAccessToUser($user, 'blockAdmin')) {
            return Redirect::back()->with('error', Lang::get('admin/messages.permission_error'));
        }

        //deleting user
        $user->delete();
        return Redirect::route('school-users')->with('success', Lang::get('admin/user.delete.success'));
	}

    /**
     * show user
     * GET /user/school/show
     *
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $me->hasAccessToUser($user, 'schoolReport')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }
        try {
            $school = School::findOrFail($user->scope_id);
        } catch ( Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $school = new School;
        }
        return View::make('backend.schoolUsers.show', compact('user', 'school'));
    }

	/**
	 * Show the form for editing schoolUser
	 * GET /user/school/{id}/edit
	 *
	 * @return View
	 */
	public function edit($id)
	{
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $me->hasAccessToUser($user, 'districtAdmin')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }

        //get group
        $group = $user->getGroups()[0];
        $groupList = [""=>"Select User Group"] + Group::where('scope', '=', self::$SCOPE_SCHOOL)->lists('name', 'id');
        return View::make('backend.schoolUsers.edit', compact('group', 'user'))
            ->with('groupList', $groupList);
	}

	/**
	 * updates SchoolUser
	 *
	 * @return Response
	 */
	public function postUpdate($id)
	{
        $me = Sentry::getUser();
        $user = Sentry::findUserById($id);
        $rules = array(
            //'email' => "required|email|unique:users,email,$id",
            //'user-group' => 'required',
            'school-code' => 'required|digits:11|exists:STEPS_KEYVAR,SCHCD',
            'school-pw' => 'regex:/^\S{6,24}$/'
        );
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()){
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        if( ! $me->hasAccess('districtAdmin')) {

            return Redirect::route('school-user-edit', $id)->with('error', Lang::get('admin/messages.permission_error'));
        }

        // removing current groups
        foreach($user->getGroups() as $userGroup) {
            $user->removeGroup($userGroup);
        }

        // updating user
        $user->email = e(Input::get('email'));
        $user->scope_id = e(Input::get('school-code'));
        $group = Sentry::findGroupById(e(Input::get('user-group')));

        Input::get('school-pw') ? ($user->password = Input::get('school-pw') ):null ;
        $user->addGroup($group);

        if ($user->save()) {
            return Redirect::route('school-user-edit', $id)->with('success', Lang::get('admin/user.update.success'));
        }

	}

}
