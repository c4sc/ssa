<?php namespace Controllers\Admin;

use AuthorizedController;
use Illuminate\Routing\Controller;
use Input;
use DB;
use Link;
use Lang;
use User;
use School;
use SchoolDetails;
use Master;
use District;
use Block;
use Village;
use Cluster;
use Panchayath;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Material;
use Building;
use Facility;
use Rte;
use Mdm;
use Smdc;
use Enrolment;
use FirstGrade;
use AgeEnrolment;
use Repeater;
use StreamEnrolment;
use Incentive;
use Result;
use RmsaFund;
use Staff;
use Teacher;
use Cwsn;
use CwsnFacility;
use AcademicYear;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Log;


class SchoolController extends AuthorizedController {

	/**
	 * Display a listing of the school.
	 * GET /school
	 *
	 * @return Response
	 */
	public function index()
	{
        $user = Sentry::getUser();
        $query = e(Input::get('q'));
        if ( ! $query) {
            $query = '';
        }

        switch($user->scope) {
            case self::$SCOPE_SCHOOL:
                return Redirect::route('show-school', $user->scope_id);
                break;
            case self::$SCOPE_BLOCK:
                $schools = SchoolDetails::where('BLKCD', $user->scope_id);
                break;
            case self::$SCOPE_DISTRICT:
                $schools = SchoolDetails::where('DISTCD', $user->scope_id);
                break;
            case self::$SCOPE_STATE:
                $schools = SchoolDetails::where('SCHCD', 'LIKE', "%$query%");
                break;
            default:
                return Redirect::route('logout');
                break;
        }

        $schools = $schools->where('SCHCD', 'LIKE', "%$query%")
            ->whereIn('AC_YEAR', array(AcademicYear::CURRENT_YEAR, AcademicYear::PREVIOUS_YEAR))
            ->orderBy('SCHCD')
            ->groupBy('SCHCD')
            ->paginate(50);

        $vari = array();
        $schoolCount = count($schools);

        for ($i=0; $i<$schoolCount; $i++) {
            $vari[]= $schools[$i]->SCHCD;
        }

        if($schoolCount) {
            $statics = Link::select('links.route', 'school_status.school_code')
                ->join('school_status','links.id','=','school_status.link_id')
                ->where('links.category','=','U')
                ->where('school_status.academic_year','=',AcademicYear::CURRENT_YEAR)
                ->whereIn('school_status.school_code', $vari)
                ->whereIn('links.route', array('particulars-one', 'facilities-one'))
                ->get();
        }

        return View::make('backend.school.index',
            compact('schools', 'query', 'statics', 'schoolCount'));
	}

	/**
	 * Display the specified school.
	 * GET /school/{schoolCode}
	 *
	 * @param  int  $schoolCode
	 * @return Response
	 */
	public function show($schoolCode)
    {
        try {
            static::initialize($schoolCode);
        } catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Response::view('errors.404', array(), 404);
        }
        $master = Master::findRecord($schoolCode);
        $school = SchoolDetails::findRecord($schoolCode);
        $user = Sentry::getUser();

        if( ! $user->hasAccessToSchool($school, 'schoolReport')) {
            return Redirect::route('account')->with('error', Lang::get('admin/messages.permission_error'));
        }
        $users = User::where('scope_id', $school->SCHCD)->get();

        return View::make('backend.school.show', compact('school', 'users', 'master'));
	}

	/**
	 * Show the form for editing the specified school.
	 * GET /school/{schoolCode}/edit
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function edit($schoolCode)
	{
        $school = SchoolDetails::findRecord($schoolCode);
        if( ! isset($school->SCHCD)) {
            return Redirect::route('schools')->with('error', 'School Does not exist.');
        }
        $user = Sentry::getUser();
        if( ! $user->hasAccessToSchool($school, 'schoolReport')) {
            return Redirect::back()->with('error', Lang::get('admin/messages.permission_error'));
        }

        $districts = [''=>''] + District::lists('DISTNAME', 'DISTCD');
        $districts = District::lists('DISTNAME', 'DISTCD');
        $blocks = Block::where('BLKCD', $school->BLKCD)->lists('BLKNAME', 'BLKCD');
        $villages = Village::where('VILCD', $school->VILCD)->lists('VILNAME', 'VILCD');
        $clusters = Cluster::where('CLUCD', $school->CLUCD)->lists('CLUNAME', 'CLUCD');
        return View::make('backend.school.edit', compact('school', 'districts', 'blocks'))
            ->with('districts', $districts)
            ->with('blocks', $blocks)
            ->with('villages', $villages)
            ->with('clusters', $clusters);
	}

	/**
	 * Update the specified school in storage.
	 * POST /school/{id}/edit
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function postUpdate($schoolCode)
	{
        $school = School::find($schoolCode);
        $user = Sentry::getUser();

        if( ! $school->validate(Input::all())) {
            $errors = $school->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }
        if( ! $user->hasAccessToSchool($school, 'schoolAdmin')) {
            return Redirect::route('schools')->with('error', Lang::get('admin/messages.permission_error'));
        }

        // saving school with changes
        $schoolDetails = SchoolDetails::findRecord($schoolCode);
        $data = array(
            'SCHNAME' => e(Input::get('name')),
            'DISTCD' => e(Input::get('district')),
            'DISTNAME' => District::find(e(Input::get('district')))->DISTNAME,
            'BLKCD' => e(Input::get('block')),
            'VILCD' => e(Input::get('village')),
            'CLUCD' => e(Input::get('cluster')),
            'RURURB' => e(Input::get('rural_or_urban')),
        );

        // updating school model too
        $school->SCHNAME = e(Input::get('name'));
        $school->DISTCD = e(Input::get('district'));
        $school->BLKCD = e(Input::get('block'));
        $school->VILCD = e(Input::get('village'));
        $school->save();
        $schoolDetails->updateRecord($data, $schoolCode);
        return Redirect::route('show-school', $schoolCode)->with('success', 'Saved');
	}

	/**
	 * shows form for create school
	 * GET /school/new
	 *
	 * @return View
	 */
	public function create()
	{
        $school = new School;
        $districts = [''=>''] + District::lists('DISTNAME', 'DISTCD');
        $blocks = [''=>''];
        $villages = [''=>''];
        $clusters = [''=>''];
        $panchayaths = [''=>''];
        return View::make('backend.school.edit', compact('school'))
            ->with('districts', $districts)
            ->with('blocks', $blocks)
            ->with('villages', $villages)
            ->with('clusters', $clusters)
            ->with('panchayaths', $panchayaths);
    }

	/**
	 * Create School
	 * POST /school/new
	 *
	 * @return Redirect
	 */
	public function postCreate()
	{
        $school = new School;
        $user = Sentry::getUser();

        // checking permission
        if( ! $user->hasAccess('stateAdmin')) {
            return Redirect::route('schools')->with('error', Lang::get('admin/messages.permission_error'));
        }

        // validating form
        if( ! $school->validate(Input::all())) {
            $errors = $school->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        // creating school
        $schoolDetails = new SchoolDetails;
        $schoolDetails->AC_YEAR = AcademicYear::CURRENT_YEAR;
        $schoolDetails->SCHCD = (Input::get('schoolCode') != '')?Input::get('schoolCode'):School::generateSchoolCode(e(Input::get('village')));
        $schoolDetails->SCHNAME = e(Input::get('name'));
        $schoolDetails->DISTCD = e(Input::get('district'));
        $schoolDetails->DISTNAME = District::find(e(Input::get('district')))->DISTNAME;
        $schoolDetails->BLKCD = e(Input::get('block'));
        $schoolDetails->VILCD = e(Input::get('village'));
        $schoolDetails->CLUCD = e(Input::get('cluster'));
        $schoolDetails->RURURB = e(Input::get('rural_or_urban'));

        if( ! $schoolDetails->save()) {
            return Redirect::back()->withInput()->withErrors('error', 'un expected error found');
        }
        static::initialize($schoolDetails->SCHCD, true);
        try {

            $data['email'] = $schoolDetails->SCHCD;
            $data['password'] = $schoolDetails->SCHCD;
            $data['scope_id'] = $schoolDetails->SCHCD;
            $data['activated'] = 1;
            $data['scope'] = self::$SCOPE_SCHOOL;

            if ($user = Sentry::getUserProvider()->create($data)) {
                $group = Sentry::findGroupByName('SchoolAdmin');
                $user->addGroup($group);
            }
        } catch (UserExistsException $e) {
            return Redirect::route('school-user-create')->with('success', 'School Created')->with('warning', 'user creation failed');
        }
        return Redirect::route('show-school', $schoolDetails->SCHCD)->with('success', 'School and User created successfully');
	}

    /**
     * Initializes school with previous year data
     */
    public static function initialize($schoolCode, $force = false)
    {
        try {
            $schoolDetails = SchoolDetails::select('ac_year', 'schcd')
                ->where('SCHCD', '=',$schoolCode)
                ->whereIn('AC_YEAR', array(AcademicYear::CURRENT_YEAR, AcademicYear::PREVIOUS_YEAR))
                ->orderBy('AC_YEAR', 'desc')
                ->firstOrFail();
            }
        catch(ModelNotFoundException $e) {
            //return Redirect::route('schools')->withError('School not exists');
            \App::abort(404);
           
        }

        if ($schoolDetails->ac_year == AcademicYear::CURRENT_YEAR && ( ! $force)) {
            return false;
        }

        SchoolDetails::insertDefault($schoolCode);
        School::updateFromDetails($schoolCode);
        Master::insertDefault($schoolCode);
        Material::insertDefault($schoolCode);
        Building::insertDefault($schoolCode);
        Facility::insertDefault($schoolCode);
        Rte::insertDefault($schoolCode);
        Mdm::insertDefault($schoolCode);
        Smdc::insertDefault($schoolCode);
        Enrolment::insertDefault($schoolCode);
        FirstGrade::insertDefault($schoolCode);
        AgeEnrolment::insertDefault($schoolCode);
        Repeater::insertDefault($schoolCode);
        StreamEnrolment::insertDefault($schoolCode);
        Incentive::insertDefault($schoolCode);
        Result::insertDefault($schoolCode);
        RmsaFund::insertDefault($schoolCode);
        Staff::insertDefault($schoolCode);
        Teacher::insertDefault($schoolCode);
        Cwsn::insertDefault($schoolCode);
        CwsnFacility::insertDefault($schoolCode);
        return true;
    }

}
