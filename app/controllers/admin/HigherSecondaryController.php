<?php namespace Controllers\Admin;

use AuthorizedController;
use Sentry;
use Input;
use School;
use Facility;
use StreamEnrolment;
use Enrolment;
use Master;
use AcademicYear;
use Staff;
use Result;
use SchoolStatus;
use Redirect;
use Validator;
use DB;
use App;
use View;
use Illuminate\Routing\Controller;

class HigherSecondaryController extends AuthorizedController
{

    public $choice = array(
        0 => '',
        1 => '1 - Yes',
        2 => '2 - No',
        '' => ''
    );

    public $emptyValues = array(
        0 => '',
        '' => '',
        98 => ''
    );

    public function __construct() {
        View::share('choice', $this->choice);
    }

    /**
     * Availability of Streams in the school
     *
     * @return View
     */
    public function availableStreams($schoolCode)
    {
        $master = App::make('master');

        if( ! $master->hasHigherSecondary()) {
            // not government or gov-aided
            return Redirect::route('account')->with('error', 'not permitted');
        }

        $streamData = Facility::findRecords($schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->where('ITEMIDGROUP', '=', 4)
            ->orderBy('ITEMID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.hs.streamsAvailable', compact('streamData'));
        }
        return View::make('backend.hs.streamsAvailable', compact('streamData'));
    }

    /**
     * save stream info
     *
     * @return Redirect
     */
    public function postAvailableStreams($schoolCode)
    {
        $notAvailableStreams = array();
        foreach(Facility::$STREAMS as $streamId => $stream) {
            $data = array( "ITEMVALUE" => e(Input::get("stream-$streamId")));
            Facility::updateFacility($data, $streamId, 4, $schoolCode);
            if( e(Input::get("stream-$streamId")) != 1) {
                $notAvailableStreams[] = $streamId;
            }
        }

        // clearing data of enrolment/repeaters by stream
        if(count($notAvailableStreams)) {
            $data = array(
                'EC11_B' => null,
                'EC11_G' => null,
                'EC12_B' => null,
                'EC12_G' => null,
                'RC11_B' => null,
                'RC11_G' => null,
                'RC12_B' => null,
                'RC12_G' => null,
            );
            StreamEnrolment::where('ITEMIDGROUP', '=', 1)
                ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
                ->whereIn('STREAMID', $notAvailableStreams)
                ->update($data);

            // clearing the data from result by stream
            $data = array();
            foreach(Result::$CATEGORY_LIST as $categoryId => $category) {
                $data += array(
                    "C10B_{$category['abbr']}" => null,
                    "C10G_{$category['abbr']}" => null,
                    "C12B_{$category['abbr']}" => null,
                    "C12G_{$category['abbr']}" => null,
                );
            }

            Result::where('SCHCD', '=', $schoolCode)
                ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
                ->whereIn('ITEMID', $notAvailableStreams)
                ->where('ITEMIDGROUP', '=', 4)
                ->update($data);
        }


        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('streams-available', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Enrolment by stream
     * @return View
     */
    public function enrolmentByStream($schoolCode)
    {
        $master = App::make('master');

        if( ! $master->hasHigherSecondary()) {
            // not government or gov-aided
            return Redirect::route('account')->with('error', 'not permitted');
        }

        $streams = Facility::getStreamList($schoolCode);
        $streamData = StreamEnrolment::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 1)
            ->whereIn('STREAMID', $streams)
            ->orderBy('STREAMID')
            ->orderBy('CASTEID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.hs.enrolmentByStream', compact('streams'))
                ->with('streamEnrolmentData', $streamData);
        }
        return View::make('backend.hs.enrolmentByStream', compact('streams', 'streamData'));
    }

    /**
     * save enrolment by stream
     * @return Redirect
     */
    public function postEnrolmentByStream($schoolCode)
    {
        $rules = array();
        $streams = Facility::getStreamList($schoolCode);

        foreach($streams as $streamId) {
            foreach(Enrolment::$CATEGORY as $categoryId => $category) {
                $rules += array(
                    "XI-boys-$streamId-$categoryId" => 'integer|min:0',
                    "XI-girls-$streamId-$categoryId" => 'integer|min:0',
                    "XII-boys-$streamId-$categoryId" => 'integer|min:0',
                    "XII-girls-$streamId-$categoryId" => 'integer|min:0',
                );
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach($streams as $streamId) {

            foreach(Enrolment::$CATEGORY as $categoryId => $category) {
                $data = array(
                    'EC11_B' => (Input::get("XI-boys-$streamId-$categoryId")!='')?Input::get("XI-boys-$streamId-$categoryId"):null,
                    'EC11_G' => (Input::get("XI-girls-$streamId-$categoryId")!='')?Input::get("XI-girls-$streamId-$categoryId"):null,
                    'EC12_B' => (Input::get("XII-boys-$streamId-$categoryId")!='')?Input::get("XII-boys-$streamId-$categoryId"):null,
                    'EC12_G' => (Input::get("XII-girls-$streamId-$categoryId")!='')?Input::get("XII-girls-$streamId-$categoryId"):null,
                );
                StreamEnrolment::updateStreamData($data,$streamId, $categoryId, 1, $schoolCode);
            }
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-stream', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Repeaters by stream
     * @return View
     */
    public function repeatersByStream($schoolCode)
    {
        $master = App::make('master');

        if( ! $master->hasHigherSecondary()) {
            // not government or gov-aided
            return Redirect::route('account')->with('error', 'not permitted');
        }

        $streams = Facility::getStreamList($schoolCode);
        $streamData = StreamEnrolment::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', '1')
            ->whereIn('STREAMID', $streams)
            ->orderBy('STREAMID')
            ->orderBy('CASTEID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.hs.repeatersByStream', compact('streams'))
                ->with('streamEnrolmentData', $streamData);
        }
        return View::make('backend.hs.repeatersByStream', compact('streams', 'streamData'));
    }


    /**
     * save repeaters by stream
     * @return Redirect
     */
    public function postRepeatersByStream($schoolCode)
    {
        $rules = array();
        $streams = Facility::getStreamList($schoolCode);

        foreach($streams as $streamId) {
            foreach(Enrolment::$CATEGORY as $categoryId => $category) {
                $rules += [
                    "XI-boys-$streamId-$categoryId" => 'integer|min:0',
                    "XI-girls-$streamId-$categoryId" => 'integer|min:0',
                    "XII-boys-$streamId-$categoryId" => 'integer|min:0',
                    "XII-girls-$streamId-$categoryId" => 'integer|min:0',
                ];
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach($streams as $streamId) {

            foreach(Enrolment::$CATEGORY as $categoryId => $category) {
                $data = array(
                    'RC11_B' => (Input::get("XI-boys-$streamId-$categoryId")!='')?Input::get("XI-boys-$streamId-$categoryId"):null,
                    'RC11_G' => (Input::get("XI-girls-$streamId-$categoryId")!='')?Input::get("XI-girls-$streamId-$categoryId"):null,
                    'RC12_B' => (Input::get("XII-boys-$streamId-$categoryId")!='')?Input::get("XII-boys-$streamId-$categoryId"):null,
                    'RC12_G' => (Input::get("XII-girls-$streamId-$categoryId")!='')?Input::get("XII-girls-$streamId-$categoryId"):null,
                );
                StreamEnrolment::updateStreamData($data,$streamId, $categoryId, 1, $schoolCode);
            }
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('repeaters-by-stream', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Number of non-teaching/administrative and support staff sanctioned and in-position
     * @param int $schoolCode
     * @return View
     */
    public function staffDetails($schoolCode)
    {
        $staffData = Staff::findRecords($schoolCode)
            ->orderBy('DESIG_ID')
            ->get();

        if(App::make('master')->school->confirmation()) {
            return View::make('backend.confirmed.hs.staffDetails', compact('staffData'));
        }
        return View::make('backend.hs.staffDetails', compact('staffData'));
    }

    /**
     * save staff details
     * @return Redirect
     */
    public function postStaffDetails($schoolCode)
    {
        $rules = array();

        foreach(Staff::$POSTS as $postId => $post) {
            $rules += array(
                "sanctioned-$postId" => 'integer',
                "in-post-$postId" => 'integer',
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach(Staff::$POSTS as $postId => $post) {
            $data = array(
                'TOTSAN' => (Input::get("sanctioned-$postId")!='')?Input::get("sanctioned-$postId"):null,
                'TOTPOS' => (Input::get("in-post-$postId")!='')?Input::get("in-post-$postId"):null,
            );
            Staff::updateStaff($data, $postId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('staff', $schoolCode)->with('success', 'Saved');
    }

}
