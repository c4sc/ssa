<?php namespace Controllers\Admin;

use AuthorizedController;
use Sentry;
use Input;
use Master;
use AcademicYear;
use School;
use RmsaFund;
use SchoolStatus;
use Redirect;
use Validator;
use DB;
use App;
use View;
use Illuminate\Routing\Controller;

class RmsaController extends AuthorizedController
{

    /**
     * Receipts and expenditure
     *
     * @return View
     */
    public function fund($schoolCode)
    {
        $master = App::make('master');
        if( ! ($master->isGov() || $master->isGovAided())) {
            // not higher-secondary or secondary school
            return Redirect::route('account')->with('error', 'not government or government aided');
        }

        $fundData = RmsaFund::findRecords($schoolCode)->orderBy('ITEMID')->get();
        if($master->school->confirmation()) {
            return View::make('backend.confirmed.rmsa.fund', compact('fundData'));
        }
        return View::make('backend.rmsa.fund', compact('fundData'));
    }

    public function postFund($schoolCode)
    {
        $rules = array();

        foreach(['civil', 'others'] as $head) {
            $rules += [
                "$head-received" => 'min:0|numeric',
                "$head-utilized" => 'min:0|numeric',
                "$head-spillovers" => 'min:0|numeric',
            ];
        }
        foreach(RmsaFund::$SCHOOL_GRANT_HEADS as $itemId => $item) {
            $rules += [
                "school-received-$itemId" => 'min:0|numeric',
                "school-utilized-$itemId" => 'min:0|numeric',
                "school-spillovers-$itemId" => 'min:0|numeric',
            ];
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        // saving civil and others heads
        foreach([1 => 'civil', 6 => 'others'] as $headId => $head) {
            $data = array(
                'AMT_R' => (Input::get("$head-received")!='')?Input::get("$head-received"):null,
                'AMT_U' => (Input::get("$head-utilized")!='')?Input::get("$head-utilized"):null,
                'AMT_S' => (Input::get("$head-spillovers")!='')?Input::get("$head-spillovers"):null,
            );
            RmsaFund::updateFund($data, $headId, $schoolCode);
        }

        // saving school grant heads
        foreach(RmsaFund::$SCHOOL_GRANT_HEADS as $itemId => $item) {
            $data = array(
                'AMT_R' => (Input::get("school-received-$itemId")!='')?Input::get("school-received-$itemId"):null,
                'AMT_U' => (Input::get("school-utilized-$itemId")!='')?Input::get("school-utilized-$itemId"):null,
                'AMT_S' => (Input::get("school-spillovers-$itemId")!='')?Input::get("school-spillovers-$itemId"):null,
            );
            RmsaFund::updateFund($data, $itemId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('rmsa-fund', $schoolCode)->with('success', 'Saved');
    }

}
