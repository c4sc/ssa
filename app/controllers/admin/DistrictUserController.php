<?php

use Illuminate\Routing\Controller;
use Cartalyst\Sentry\Users\UserExistsException;

class DistrictUserController extends AuthorizedController {

    public function __construct()
    {
        // Call parent
        parent::__construct();
    }

	/**
	 * Show the form for creating a new DistrictUser.
	 * GET /user/district/create
	 *
	 * @return View
	 */
	public function create()
	{
        $groupList = Group::where('scope', '=', self::$SCOPE_DISTRICT)->lists('name', 'id');
        return View::make('backend.districtUsers.edit')
            ->with('user', new User)
            ->with('groupList', $groupList);
	}

	/**
	 * Store a newly created DistrictUser
	 * POST /user/district/create
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        $me = Sentry::getUser();
        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user-group' => 'required',
            'district-code' => 'required|digits:4|exists:STEPS_DISTRICT,DISTCD',
        );
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()){
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $district = District::find(e(Input::get('district-code')));
        if( ! $me->hasAccess('stateAdmin')) {
            return Redirect::route('district-user-create')->with('error', Lang::get('admin/messages.permission_error'));
        }

        try {

            $data = Input::only('email', 'password');
            $data['activated'] = 1;
            $data['scope'] = self::$SCOPE_DISTRICT;
            $data['scope_id'] = e(Input::get('district-code'));

            if ($user = Sentry::getUserProvider()->create($data)) {
                $group = Sentry::findGroupById(e(Input::get('user-group')));
                $user->addGroup($group);
                return Redirect::route('district-user-create')->with('success', Lang::get('admin/user.create.success'));
            }
        } catch (UserExistsException $e) {
            return Redirect::route('district-user-create')->with('error', Lang::get('admin/user.exists'));
        }

	}

    /**
     * show user
     *
     * @param int $id
     * @return View
     */
    public function show($id)
    {
        $me = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $me->hasAccessToUser($user, 'districtAdmin')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }
        $district = District::find($user->scope_id);
        return View::make('backend.districtUsers.show', compact('user', 'district'));
    }

	/**
	 * Show the form for editing districtUser
	 * GET /user/district/{id}/edit
	 *
	 * @return View
	 */
	public function edit($id)
	{
        $user = Sentry::getUser();
        try {
            $user = Sentry::findUserByID($id);
        } catch(Cartalyst\Sentry\Users\UserNotFoundException $e){
            return Redirect::route('school-users')->with('error', Lang::get('admin/user.does_not_exist'));
        }

        if( ! $user->hasAccessToUser($user, 'districtAdmin')) {
            return Redirect::route('school-users')->with('error', Lang::get('admin/messages.permission_error'));
        }

        $groupList = [""=>"Select User Group"] + Group::where('scope', '=', self::$SCOPE_DISTRICT)->lists('name', 'id');
        $group = $user->getGroups()[0];

        return View::make('backend.districtUsers.edit', compact('user', 'group'))
            ->with('groupList', $groupList);
	}

	/**
	 * updates DistrictUser
	 *
	 * @return Response
	 */
	public function postUpdate($id)
	{
        $user = Sentry::findUserById($id);
        $me = Sentry::getUser();
        $rules = array(
            'email' => "required|email|unique:users,email,$id",
            'user-group' => 'required',
            'district-code' => 'required|digits:4|exists:STEPS_DISTRICT,DISTCD',
        );
        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails()){
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        if( ! $me->hasAccessToUser($user, 'districtAdmin')) {
            return Redirect::route('district-user-edit', $id)->with('error', Lang::get('admin/messages.permission_error'));
        }

        // removing current groups
        foreach($user->getGroups() as $userGroup) {
            $user->removeGroup($userGroup);
        }

        // updating user
        $user->email = e(Input::get('email'));
        $user->scope_id = e(Input::get('district-code'));
        $group = Sentry::findGroupById(e(Input::get('user-group')));
        $user->addGroup($group);

        if ($user->save()) {
            return Redirect::route('district-user-edit', $id)->with('success', Lang::get('admin/user.update.success'));
        }
	}

}
