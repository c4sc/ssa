<?php namespace Controllers\Admin;

use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use Student;
use StudentDetails;
use Transfer;
use SchoolWarning;
use District;
use Block;
use EducationBlock;
use Constituency;
use Habitation;
use Master;
use Redirect;
use Validator;
use DB;
use View;
use App;
use Route;
use SchoolStatus;
use Response;
use Log;
use Illuminate\Routing\Controller;

class AdiseController extends AuthorizedController {

    public $choice = array(
        0 => '',
        1 => '1 - Yes',
        2 => '2 - No',
        '' => ''
    );

    public $emptyValues = array(
        0 => '',
        '' => '',
        98 => ''
    );

    /**
     * students - handles edit, new, update, search
     */
    public function students($schoolCode)
    {
        $school = School::find($schoolCode);
        $master =  App::make('master');

        $query = e(Input::get('q', null));
        $class = e(Input::get('class', null));
        $division = e(Input::get('division', null));
        $gender = e(Input::get('gender', null));
        $academicYear = e(Input::get('ac_year', AcademicYear::CURRENT_YEAR));
        $studentId = e(Input::get('studentId', null));
        $students = new Student;

        if (! $academicYear) {
            $academicYear = AcademicYear::CURRENT_YEAR;
        }
        if ($studentId != 'new' && $studentId != '') {
            ($student = Student::find($studentId)) or App::abort(404);
            $studentDetails = StudentDetails::where('student_id', $studentId)->first() or App::abort(404);
        } else {
            $student = new Student;
            $studentDetails = new StudentDetails;
        }

        if ($query) {
           $students = $students->where('name', 'LIKE', "%$query%");
        }
        if ($gender) {
           $students = $students->where('gender', '=', $gender);
        }

        $students = $students->where('students.schoolcode', '=', $schoolCode)
            ->join('student_details', 'students.id', '=', 'student_details.student_id');

        // applying filters
        if ($class) {
           $students = $students->where('student_details.class', $class);
        }
        if ($division) {
           $students = $students->where('student_details.division', $division);
        }
        $students = $students->where('student_details.academic_year', $academicYear);

        $studentList = $students->select('name', 'students.id', 'students.dob', 'student_details.admission_no')
            ->paginate(50);
        $habitations = [''=>'.... Select ....']+Habitation::getList($master->school->BLKCD);
        $acYearList = array(
            '' => 'Academic Year',
            AcademicYear::CURRENT_YEAR => AcademicYear::CURRENT_YEAR,
            AcademicYear::PREVIOUS_YEAR => AcademicYear::PREVIOUS_YEAR
        );

        return View::make('backend.adise.student.list', compact('school', 'studentList',
            'schoolCode', 'query', 'class', 'division', 'gender', 'academicYear', 'studentId',
            'acYearList', 'student', 'studentDetails',  'habitations'));
    }

    public function postStudent($schoolCode)
    {
        $studentId = e(Input::get('student-id', null));
        if($studentId) {
            return $this->postUpdate($schoolCode, $studentId);
        }
        return $this->postNewStudent($schoolCode);
    }

    /**
     * Generate Student Code
     *
     * @param $date string
     * @param $schoolCode integer
     */
    public function generateStudentCode($date, $schoolCode)
    {
        $admissionYear = date('y', strtotime($date));
        $studentCode = $schoolCode.$admissionYear;
        $studentCount = Student::select(DB::raw('count(*) as count, id'))
           ->where('studentCode', 'LIKE', "$studentCode%")
           ->count();

        $studentCount++;
        $charLength = strlen((string)$studentCount);
        $lengthOfZeros = 4 - $charLength;
        $zeros = '';
        while($lengthOfZeros--) {
            $zeros .= '0';
        }
        $studentCode .= $zeros.$studentCount;
        return $studentCode;
    }

    /**
     * Save New Student
     *
     * @return Redirect
     */
    public function postNewStudent($schoolCode)
    {
        $master = App::make('master');
        $habitations = [''=>'.... Select ....']+Habitation::getList($master->school->BLKCD);
        $rules = array(
            'rural-urban' => 'required',
            'habitation' => 'required',
            'name' => 'required',
            'father' => 'required',
            'mother' => 'required',
            'gender' => 'required',
            'dob' => 'required|date',
            'gender' => 'required',
            'social-category' => 'required',
            'religion' => 'required',
            'bpl' => 'required',
            'disadvantage-group' => 'required',
            'aadhaar' => 'digits:12',
            'admission-date' => 'required',
            'admission-no' => 'required|unique:student_details,admission_no,null,id,schoolcode,'.$schoolCode,
            'class' => 'required',
            'division' => 'required',
            'previous-days' => '',
            'medium' => 'required',
            'disability' => 'required',
            'free-education' => 'required',

        );
        if(intval(e(Input::get('class'))) == 0 ){
            $rules += ['previous-class' => 'required|regex:/9/'];
        } else {
            $rules += ['previous-class' => 'required|in:'.intval(e(Input::get('class'))).','.(intval(Input::get('class'))-1)];
        }
        if ($master->isGov() || $master->isGovAided()) {
            $rules += array(
                'facilitiescwsn' => 'required',
                'books' => 'required',
                'uniforms' => 'required',
                'transport' => 'required',
                'escort' => 'required',
                'hostel' => 'required',
                'special-training' => 'required',
                'homeless' => 'required',
            );
        }
        if (Input::get('class') == 1) {
            $rules += ['previous-status' => 'required'];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withError($validator->errors());
        }

        $student = new Student;
        //Student Code
        $admissionDate = e(Input::get('admission-date'));
        $studentCode = $this->generateStudentCode($admissionDate, $schoolCode);

        //collecting data
        $student->schoolcode = $schoolCode;
        $student->rural_urban = e(Input::get('rural-urban'));
        $student->habitation_code = e(Input::get('habitation'));
        $student->name = e(Input::get('name'));
        $student->father = e(Input::get('father'));
        $student->mother = e(Input::get('mother'));
        $student->dob = date("Y-m-d", strtotime(e(Input::get('dob'))));
        $student->gender = e(Input::get('gender'));
        $student->social_category = e(Input::get('social-category'));
        $student->religion = e(Input::get('religion'));
        $student->bpl_yn = e(Input::get('bpl'));
        $student->disadvantagegroup_yn = e(Input::get('disadvantage-group'));
        $student->disability_yn = e(Input::get('disability'));
        $student->aadhaar = e(Input::get('aadhaar'));
        $student->studentcode = $studentCode;
        //Saving
        $student->save();

        $studentId = $student->id;

        $studentdetails = new StudentDetails;

        $studentdetails->schoolcode = $schoolCode;
        $studentdetails->academic_year = AcademicYear::CURRENT_YEAR;
        $studentdetails->student_id = $studentId;
        $studentdetails->admission_date = date("Y-m-d", strtotime(e(Input::get('admission-date'))));
        $studentdetails->admission_no = e(Input::get('admission-no'));
        $studentdetails->class = e(Input::get('class'));
        $studentdetails->division = e(Input::get('division'));
        $studentdetails->previous_class = e(Input::get('previous-class'));
        $studentdetails->previous_status = e(Input::get('previous-status'));
        $studentdetails->previous_days = e(Input::get('previous-days'));
        $studentdetails->medium = e(Input::get('medium'));
        $studentdetails->free_education_yn = e(Input::get('free-education'));

        if($master->isGov() || $master->isGovAided()) {
            $studentdetails->facilitiescwsn = e(Input::get('facilitiescwsn'));
            $studentdetails->books_yn = e(Input::get('books'));
            $studentdetails->uniforms = e(Input::get('uniforms'));
            $studentdetails->transport_yn = e(Input::get('transport'));
            $studentdetails->escort_yn = e(Input::get('escort'));
            $studentdetails->hostel_yn = e(Input::get('hostel'));
            $studentdetails->special_training_yn = e(Input::get('special-training'));
            $studentdetails->homeless_yn = e(Input::get('homeless'));
        }
        else {
            $studentdetails->facilitiescwsn = null;
            $studentdetails->books_yn = null;
            $studentdetails->uniforms = null;
            $studentdetails->transport_yn = null;
            $studentdetails->escort_yn = null;
            $studentdetails->hostel_yn = null;
            $studentdetails->special_training_yn = null;
            $studentdetails->homeless_yn = null;
        }
        //saving
        if( ! $studentdetails->save()) {
            return Redirect::back()->withInput()->withError('error', 'error while saving');
        }
        return Redirect::back()->with('success', 'Saved');
    }
    /**
     * Delete Student
     */
    public function destroy($schoolCode)
    {
        $studentId = Input::get('idstudent');
        $user = Sentry::getUser();
        Student::where('id', '=', $studentId)->delete();
        StudentDetails::where('student_id', '=', $studentId)->delete();
        Transfer::where('student_id', '=', $studentId)->delete();
        return Redirect::route('students', $schoolCode)->with('success', 'Deleted');
    }

    /**
     * update student
     *
     * @param $schoolCode
     * @param $studentId
     * @return Redirect
     */
    public function postUpdate($schoolCode, $studentId)
    {
        $master = App::make('master');
        $student = Student::findRecord($studentId);
        $studentdetails = StudentDetails::findRecord($studentId);
        $user =  Sentry::getUser();
        $habitations = [''=>'.... Select ....']+Habitation::getList($master->school->BLKCD);
        $rules = array(
            'rural-urban' => 'required',
            'habitation' => 'required',
            'name' => 'required',
            'father' => 'required',
            'mother' => 'required',
            'gender' => 'required',
            'dob' => 'required|date',
            'gender' => 'required',
            'social-category' => 'required',
            'religion' => 'required',
            'bpl' => 'required',
            'disadvantage-group' => 'required',
            'aadhaar' => 'digits:12|integer',
            'admission-date' => 'required',
            'admission-no' => "required|unique:student_details,admission_no,$studentdetails->admission_no,admission_no,schoolcode,$schoolCode",
            'class' => 'required',
            'division' => 'required',
            'previous-days' => '',
            'medium' => 'required',
            'disability' => 'required',
            'free-education' => 'required',

        );
        if(intval(e(Input::get('class'))) == 0 ){
            $rules += ['previous-class' => 'required|regex:/9/'];
        } else {
            $rules += ['previous-class' => 'required|in:'.intval(e(Input::get('class'))).','.(intval(Input::get('class'))-1)];
        }
        if($master->isGov() || $master->isGovAided()) {
            $rules += array(
                'facilitiescwsn' => 'required',
                'books' => 'required',
                'uniforms' => 'required',
                'transport' => 'required',
                'escort' => 'required',
                'hostel' => 'required',
                'special-training' => 'required',
                'homeless' => 'required',
            );
        }
        if (Input::get('class') == 1) {
            $rules += ['previous-status' => 'required'];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        //collecting data
        //

        $datastudent = array(
            'rural_urban' => e(Input::get('rural-urban')),
            'habitation_code' => e(Input::get('habitation')),
            'name' => e(Input::get('name')),
            'father' => e(Input::get('father')),
            'mother' => e(Input::get('mother')),
            'dob' => date("Y-m-d", strtotime(e(Input::get('dob')))),
            'gender' => e(Input::get('gender')),
            'social_category' => e(Input::get('social-category')),
            'religion' => e(Input::get('religion')),
            'bpl_yn' => e(Input::get('bpl')),
            'disadvantagegroup_yn' => e(Input::get('disadvantage-group')),
            'disability_yn' => e(Input::get('disability')),
            'aadhaar' => e(Input::get('aadhaar')),
        );
        $datastudentdetails = array(
            'academic_year' => AcademicYear::CURRENT_YEAR,
            'admission_date' => date("Y-m-d", strtotime(e(Input::get('admission-date')))),
            'admission_no' => e(Input::get('admission-no')),
            'class' => e(Input::get('class')),
            'division' => e(Input::get('division')),
            'previous_class' => e(Input::get('previous-class')),
            'previous_status' => e(Input::get('previous-status')),
            'previous_days' => e(Input::get('previous-days')),
            'medium' => e(Input::get('medium')),
            'free_education_yn' => e(Input::get('free-education')),
        );

        //Check for Govt Aided
        if($master->isGov() || $master->isGovAided()) {
            $datastudentdetails += array(
                'facilitiescwsn' => e(Input::get('facilitiescwsn')),
                'books_yn' => e(Input::get('books')),
                'uniforms' => e(Input::get('uniforms')),
                'transport_yn' => e(Input::get('transport')),
                'escort_yn' => e(Input::get('escort')),
                'hostel_yn' => e(Input::get('hostel')),
                'special_training_yn' => e(Input::get('special-training')),
                'homeless_yn' => e(Input::get('homeless')),
            );
        }
        else {
            $datastudentdetails += array(
                'facilitiescwsn' => null,
                'books_yn' => null,
                'uniforms' => null,
                'transport_yn' => null,
                'escort_yn' => null,
                'hostel_yn' => null,
                'special_training_yn' => null,
                'homeless_yn' => null,
            );
        }


        $studentAffected = Student::updateRecord($datastudent, $studentId);
        $detailsAffected = StudentDetails::updateRecord($datastudentdetails, $studentId);
        //saving
        if ( ! ($detailsAffected || $studentAffected)) {
            return Redirect::back()->withInput()->withError('error', 'error while saving');
        }
        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::back()->with('success', 'Saved');
    }
    /**
     * transfer student
     */
    public function transferStudent($schoolCode, $studentId)
    {
        //Check if that student is already transfered.
        $count = Transfer::where('student_id', $studentId)
            ->where('status', Transfer::PENDING)
            ->count() > 0;
        if($count) {
            return Redirect::route('students', array($schoolCode))
                ->withInput()->withError('Student already  on the transfer pool.');
        }
        $school = School::find($schoolCode);
        $student = Student::findRecord($studentId);
        $studentdetails = StudentDetails::findRecord($studentId);
        $districts = [''=>'....Select....'] + District::lists('DISTNAME', 'DISTCD');
        $blocks =  [''=>'....Select....'] + Block::lists('BLKNAME', 'BLKCD');

        return View::make('backend.adise.student.transfer',
            compact('student', 'studentdetails', 'school', 'districts', 'blocks'));
    }

    /**
     * save transfer
     */
    public function postTransferStudent($schoolCode, $studentId)
    {
        //collecting data
        $count = Transfer::where('student_id', $studentId)
            ->where('status', Transfer::PENDING)
            ->count() > 0;
        if($count) {
            return Redirect::back()->withInput()->withError('Student already  on the transfer pool.');
        }
        $master = App::make('master');
        $user =  Sentry::getUser();
        $transfer = new Transfer;
        $rules = array(
            'district' => 'required',
            'block' => 'required',
            'to_school' => 'required|digits:11',
        );
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }
        if(e(Input::get('to_school')) == $master->SCHCD) {
           return Redirect::back()->withInput()->withError('Cannot transfer to same school');
        }
        $transfer->student_id = $studentId;
        $transfer->transfered_by = $user->scope_id;
        $transfer->from_school = $schoolCode;
        $transfer->to_school = Input::get('to_school');
        $transfer->academic_year = AcademicYear::CURRENT_YEAR;
        $transfer->status = Transfer::PENDING;
        //Saving
        if( ! $transfer->save()) {
            return Redirect::back()->withInput()->withError('error', 'error while saving');
        }
        return Redirect::route('students', array($schoolCode, $studentId))->with('success', 'Saved');
    }

    /**
     *  Transfer List
     */
    public function transferList($schoolCode)
    {
        // student who seeks admission in the school
        $studentsToBeApproved = Transfer::where('to_school', '=', $schoolCode)
            ->where('academic_year', '=', AcademicYear::CURRENT_YEAR)
            ->where('status', '=', Transfer::PENDING)
            ->join('students', 'transfer_pool.student_id', '=', 'students.id')
            ->get();

        // student who seeks tranfer to another school
        $transferingStudents = Transfer::where('from_school', '=', $schoolCode)
            ->where('academic_year', '=', AcademicYear::CURRENT_YEAR)
            ->where('status', '=', Transfer::PENDING)
            ->join('students', 'transfer_pool.student_id', '=', 'students.id')
            ->get();

        return View::make('backend.adise.student.transferList', compact('studentsToBeApproved', 'transferingStudents'));

    }

    public function postApproveTransfer($schoolCode)
    {
        $rules = array(
            'student-id' => 'required',
            );
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }
        $studentId = Input::get('student-id');
        // Need to check the same school Criteria.
        Transfer::where('student_id', '=', $studentId)
            ->where('academic_year', '=', AcademicYear::CURRENT_YEAR)
            ->where('status', '=', Transfer::PENDING)
            ->update(array('status' => Transfer::APPROVED ));
        Student::where('id', '=', $studentId)
            ->update(array('schoolcode' => $schoolCode));
        StudentDetails::where('student_id', '=', $studentId)
            ->update(array('schoolcode' => $schoolCode));

        SchoolStatus::setStatus($schoolCode, Input::get('_route', 'transfer-list'));
        return Redirect::route('transfer-list', $schoolCode)->with('success', 'Saved');

    }

    public function postCancelTransfer($schoolCode)
    {
        $rules = array(
            'student-id' => 'required',
            );
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }
        $studentId = Input::get('student-id');
        Transfer::where('student_id', '=', $studentId)
            ->where('academic_year', '=', AcademicYear::CURRENT_YEAR)
            ->where('status', '=', Transfer::PENDING)
            ->delete();
        Student::where('id', '=', $studentId)
            ->update(array('schoolcode' => $schoolCode));
        StudentDetails::where('student_id', '=', $studentId)
            ->update(array('schoolcode' => $schoolCode));

        SchoolStatus::setStatus($schoolCode, Input::get('_route', 'transfer-list'));
        return Redirect::route('transfer-list', $schoolCode)->with('success', 'Saved');

    }
}

