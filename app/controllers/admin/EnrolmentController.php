<?php namespace Controllers\Admin;

use AuthorizedController;
use Sentry;
use Input;
use School;
use FirstGrade;
use AgeEnrolment;
use Repeater;
use Enrolment;
use Master;
use AcademicYear;
use Incentive;
use MediumEnrolment;
use Cwsn;
use CwsnFacility;
use SchoolStatus;
use Redirect;
use Validator;
use DB;
use View;
use App;
use Response;
use Illuminate\Routing\Controller;
use Log;
use SchoolWarning;

class EnrolmentController extends AuthorizedController {

    public $choice = array(
        0 => '',
        1 => '1 - Yes',
        2 => '2 - No',
        '' => ''
    );

    public $emptyValues = array(
        0 => '',
        '' => '',
        98 => ''
    );

    public $firstGradeAgeList = array(
        4 => 'less than 5',
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 'greater than 7'
    );

    /**
     * form for Grade I admission details
     * @return View
     */
    public function firstGradeAdmission($schoolCode)
    {
        $master = App::make('master');
        $firstGrade = FirstGrade::findRecord();

        if($master->LOWCLASS != 1) {
            // not government or gov-aided
            return Redirect::route('account')->with('error', 'the requested page is only for schools having grade I');
        }
        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.firstGradeAdmission', compact('firstGrade'))
                ->with('firstGradeAgeList', $this->firstGradeAgeList);
        }
        return View::make('backend.enrolment.firstGradeAdmission', compact('firstGrade'))
            ->with('firstGradeAgeList', $this->firstGradeAgeList);
    }

    public function postfirstGradeAdmission($schoolCode)
    {
        $master = App::make('master');

        $rules = array(
            'total-boys' => 'integer|min:0',
            'total-girls' => 'integer|min:0',
            'same-school-boys' => 'integer|min:0',
            'same-school-girls' => 'integer|min:0',
            'other-school-boys' => 'integer|min:0',
            'other-school-girls' => 'integer|min:0',
            'anganwadi-boys' => 'integer|min:0',
            'anganwadi-girls' => 'integer|min:0',
            'preschool-total-boys' => 'integer|min:0|max:'.Input::get('total-boys', 0),
            'preschool-total-girls' => 'integer|min:0|max:'.Input::get('total-girls', 0),
        );

        foreach($this->firstGradeAgeList as $age => $d) {
            $rules += [
                "boys-age-$age" => 'integer|min:0',
                "girls-age-$age" => 'integer|min:0'
            ];
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        $data = array(
            'TOT_B' => Input::get('total-boys')?Input::get('total-boys'):null,
            'TOT_G' => Input::get('total-girls')?Input::get('total-girls'):null,
            'SAMESCH_B' => Input::get('same-school-boys')?Input::get('same-school-boys'):null,
            'SAMESCH_G' => Input::get('same-school-girls')?Input::get('same-school-girls'):null,
            'OTHERSCH_B' => Input::get('other-school-boys')?Input::get('other-school-boys'):null,
            'OTHERSCH_G' => Input::get('other-school-girls')?Input::get('other-school-girls'):null,
            'ECCE_B' => Input::get('anganwadi-boys')?Input::get('anganwadi-boys'):null,
            'ECCE_G' => Input::get('anganwadi-girls')?Input::get('anganwadi-girls'):null,
        );
        foreach($this->firstGradeAgeList as $age => $d) {
            $data += [
                "AGE{$age}_B" => Input::get("boys-age-$age")?Input::get("boys-age-$age"):null,
                    "AGE{$age}_G" => Input::get("girls-age-$age")?Input::get("girls-age-$age"):null,
                ];
        }

        if( ! FirstGrade::updateRecord($data, $schoolCode)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }
        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('first-grade-admission', $schoolCode)->with('success', 'Saved');
    }

    /**
     * enrolment by social category
     *
     * @return View
     */

    public function enrolmentBySocialCategory($schoolCode)
    {
        $categoryData = Enrolment::findRecords($schoolCode)
            ->whereIn('CASTEID', array(1, 2, 3, 4))
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('CASTEID')
            ->get();

        $divisions = Enrolment::getDivisions($schoolCode);

        if(App::make('master')->school->confirmation()) {
            $sum = Enrolment::sum($schoolCode, 1);
            return View::make('backend.confirmed.enrolment.enrolmentBySocialCategory', compact('divisions', 'categoryData', 'sum'));
        }
        return View::make('backend.enrolment.enrolmentBySocialCategory', compact('divisions', 'categoryData'));
    }

    /**
     * save enrolment by social category form
     *
     * @return Redirect
     */
    public function postEnrolmentBySocialCategory($schoolCode)
    {
        $master = App::make('master');
        $rules = array();

        $classes = range($master->LOWCLASS, $master->HIGHCLASS);
        if($master->PPSEC_YN == 1) {
            $classes = array_merge($classes, array('PP'));
        }

        foreach($classes as $class) {

            // no. of division validation
            $maxDivision = (int)(Input::get("class-total-$class")/20);
            $min = 1;
            if (Input::get("class-total-$class") > 0 && $maxDivision == 0)  {
                $maxDivision++;
            } elseif (Input::get("class-total-$class") % 20 > 0) {
                $maxDivision++;
            } else {
                $min = 0;
            }
            if ( e(Input::get("boys-$class-1")) +  e(Input::get("boys-$class-2")) + e(Input::get("boys-$class-3")) + e(Input::get("boys-$class-4")) > 0) {
                 $rules += array(
                     "divisions-$class" => "required|integer|min:$min|max:$maxDivision",
                 );
                 
             } else {
                $rules += array(
                     "divisions-$class" => "integer|min:$min|max:5",
                 ); 
             }

            foreach(Enrolment::$CATEGORY as $categoryId => $category) {
                $rules += array(
                    "boys-$class-$categoryId" => 'integer|min:0',
                    "girls-$class-$categoryId" => 'integer|min:0',
                );
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }
        foreach(Enrolment::$CATEGORY as $categoryId => $category) {

            $data = array();
            foreach($classes as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$categoryId")!='')?Input::get("boys-$class-$categoryId"):null,
                    "C{$class}_G" =>(Input::get("girls-$class-$categoryId")!='')?Input::get("girls-$class-$categoryId"):null,
                );
            }
            Enrolment::updateEnrolment($data, $categoryId, 1, $schoolCode);
        }

        // saving divisions
        $data = array();
        $zeroDivisions = false;
        foreach($classes as $class) {

            if (Input::get("divisions-$class") < 1) $zeroDivisions = true;

            $data += array(
                "C{$class}_B" =>(Input::get("divisions-$class")!='')?Input::get("divisions-$class"):null
            );
        }
        Enrolment::updateEnrolment($data, 0, 0, $schoolCode);
        if ($zeroDivisions) {
            $warningMessage = 'enrolment data contains classes with zero students';
            SchoolWarning::addWarning($schoolCode, 'class-with-no-students', $warningMessage);
        } else {
            SchoolWarning::deleteWarning($schoolCode, 'class-with-no-students');
        }

        // copying data to medium enrolment, if there is only one medium
        $mediums =  MediumEnrolment::getSchoolMediums($schoolCode);
        if(count($mediums) == 1) {

            MediumEnrolment::findRecords($schoolCode)->delete();
            $sum = Enrolment::sum($schoolCode, 1);
            $newMedium = new MediumEnrolment;

            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                $newMedium->{"C{$class}_B"} = $sum->{"S{$class}B"};
                $newMedium->{"C{$class}_G"} = $sum->{"S{$class}G"};
            }
            $newMedium->AC_YEAR = AcademicYear::CURRENT_YEAR;
            $newMedium->SCHCD = $schoolCode;
            $newMedium->MEDIUM = $mediums[0]['id'];
            $newMedium->SEQNO = 1;
            $newMedium->save();
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-category', $schoolCode)->with('success', 'Saved');
    }

    /**
     * enrolment by social category - Minority
     *
     * @return View
     */
    public function enrolmentBySocialCategoryMinority($schoolCode)
    {
        $sum = Enrolment::sum($schoolCode, 1);

        $categoryData = Enrolment::findRecords($schoolCode)
            ->whereIn('CASTEID', array(5, 6, 7, 8, 9, 10))
            ->where('ITEMIDGROUP', '=', 2)
            ->orderBy('CASTEID')
            ->get();

        if(App::make('master')->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.enrolmentBySocialCategoryMinority', compact('sum'))
                ->with('minorityData', $categoryData);
        }
        return View::make('backend.enrolment.enrolmentBySocialCategoryMinority', compact('sum', 'categoryData'));
    }

    /**
     * save enrolment by social category Minority  form
     *
     * @return Redirect
     */
    public function postEnrolmentBySocialCategoryMinority($schoolCode)
    {
        $master = App::make('master');
        $rules = array();

        $classes = range($master->LOWCLASS, $master->HIGHCLASS);
        if($master->PPSEC_YN == 1) {
            $classes = array_merge($classes, array('PP'));
        }
        foreach($classes as $class) {
            foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category) {
                $rules += array(
                    "boys-$class-$categoryId" => 'integer|min:0',
                    "girls-$class-$categoryId" => 'integer|min:0',
                );
            }
        }

        $sum = Enrolment::sum($schoolCode, 1);
        foreach($classes as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {

            $totalError = '';
            $errors = $validator->errors();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category) {

            $data = array();
            foreach($classes as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$categoryId")!='')?Input::get("boys-$class-$categoryId"):null,
                    "C{$class}_G" => (Input::get("girls-$class-$categoryId")!='')?Input::get("girls-$class-$categoryId"):null,
                );
            }
            Enrolment::updateEnrolment($data, $categoryId, 2, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-minority-category', $schoolCode)->with('success', 'Saved');
    }

    /**
     * page for Enrolment in current academic session by age
     *
     * @return View
     */


    public function enrolmentByAge($schoolCode)
    {
        $master = App::make('master');
        $all = AgeEnrolment::findRecords($schoolCode)
            ->where('CASTEID', '=', 0)
            ->orderBy('AGEID')
            ->get();

        if( ! $master->hasPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 5) {
            $highClass = 4;
        } elseif($master->HIGHCLASS == 5) {
            $highClass = 5;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        $lowClass = $master->LOWCLASS;

        $sum = Enrolment::sum($schoolCode, 1);

        if($master->school->confirmation()) {
            $ageData = AgeEnrolment::findRecords($schoolCode)
                ->where('CASTEID', '=', 0)
                ->orderBy('AGEID')
                ->get();
            return View::make('backend.confirmed.enrolment.enrolmentByAge', compact('sum'))
                ->with('ageData', $all);
        }

        return View::make('backend.enrolment.enrolmentByAge', compact('all', 'sum', 'highClass', 'lowClass'));
    }

    public function postEnrolmentByAge($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if($master->HIGHCLASS > 5) {
            $highClass = 4;
        } elseif($master->HIGHCLASS == 5) {
            $highClass = 5;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        $lowClass = $master->LOWCLASS;
        foreach(range($lowClass, $highClass) as $class) {
            foreach(range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]) as $age) {
                $rules += array(
                    "boys-$class-$age" => 'integer|min:0',
                    "girls-$class-$age" => 'integer|min:0',
                );
            }
        }
        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "regex:/$sum_boy/",
                "total-girls-$class" => "regex:/$sum_girl/",
            );
        }
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors())
            ->withError('total students you entered does not match total number of students. Please fix them.');
        }

        foreach(range(4, 23) as $age) {
            $data = array();
            foreach(range($lowClass, $highClass) as $class) {
                if( ! in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]))) {
                    continue;
                }
                $data += array(
                    "C{$class}B" => (Input::get("boys-$class-$age")!='')?Input::get("boys-$class-$age"):null,
                    "C{$class}G" => (Input::get("girls-$class-$age")!='')?Input::get("girls-$class-$age"):null,
                );
            }
            AgeEnrolment::updateEnrolment($data, 0, $age, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-age', $schoolCode)->with('success', 'Saved');
    }

    /**
     * page for Enrolment in current academic session by age - UP Class
     *
     * @return View
     */
    public function enrolmentByAgeUp($schoolCode)
    {
        $master = App::make('master');
        $all = AgeEnrolment::findRecords($schoolCode)
            ->where('CASTEID', '=', 0)
            ->orderBy('AGEID')
            ->get();

        if(!$master->hasUpperPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 8) {
            $highClass = 7;
        } elseif($master->HIGHCLASS == 8) {
            $highClass = 8;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 5) {
            $lowClass = 5;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        $sum = Enrolment::sum($schoolCode, 1);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.enrolmentByAge', compact('sum'))
                ->with('ageData', $all);
        }

        return View::make('backend.enrolment.enrolmentByAgeUp', compact('all'))
            ->with('agelimit', AgeEnrolment::$agelimit)
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('sum', $sum);

    }

    public function postEnrolmentByAgeUp($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasUpperPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 8) {
            $highClass = 7;
        } elseif($master->HIGHCLASS == 8) {
            $highClass = 8;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 5) {
            $lowClass = 5;
        } else {
            $lowClass = $master->LOWCLASS;
        }
        foreach(range($lowClass, $highClass) as $class) {
            foreach(range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]) as $age) {
                $rules += array(
                    "boys-$class-$age" => 'integer|min:0',
                    "girls-$class-$age" => 'integer|min:0',
                );
            }
        }

        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "regex:/$sum_boy/",
                "total-girls-$class" => "regex:/$sum_girl/",
            );
        }
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach(range(9, 23) as $age) {
            $data = array();
            foreach(range($lowClass, $highClass) as $class) {
                if( ! in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]))) {
                    continue;
                }
                $data += array(
                    "C{$class}B" => (Input::get("boys-$class-$age")!='')?Input::get("boys-$class-$age"):null,
                    "C{$class}G" => (Input::get("girls-$class-$age")!='')?Input::get("girls-$class-$age"):null,
                );
            }
            AgeEnrolment::updateEnrolment($data,0, $age, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-age-up', $schoolCode)->with('success', 'Saved');
    }

    /**
     * enrolment by Medium
     *
     * @return View
     */
    public function enrolmentByMedium($schoolCode)
    {
        $master = App::make('master');
        $mediumData = [];

        $mediums =  MediumEnrolment::getSchoolMediums($schoolCode);
        foreach($mediums as  $index => $medium) {
            $newMedium = MediumEnrolment::findRecords($schoolCode)
                ->where('MEDIUM', '=', $medium['id'])->first();

            if( ! isset($newMedium->SEQNO)) {
                // deleting existing sequence data
                MediumEnrolment::findRecords($schoolCode)
                    ->where('SEQNO', '=', $index+1)->delete();
                // creating new medium-enrolment
                $newMedium = new MediumEnrolment;
                $newMedium->AC_YEAR = AcademicYear::CURRENT_YEAR;
                $newMedium->SCHCD = $schoolCode;
                $newMedium->MEDIUM = $medium['id'];
                $newMedium->SEQNO = $index+1;
                $newMedium->save();

            } elseif ($newMedium->SEQNO != ($index+1)) {

                // correcting sequence number
                MediumEnrolment::updateMediumEnrolment(array('SEQNO'=>$index+1), $medium['id'], $schoolCode);
            }
            $mediumData[] = $newMedium;
        }

        $sum = Enrolment::sum($schoolCode, 1);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.enrolmentByMedium', compact('mediumData', 'sum'))
                ->with('schoolMediums', $mediums);
        }

        return View::make('backend.enrolment.enrolmentByMedium', compact('mediumData', 'mediums', 'sum'));
    }

    /**
     * save enrolment by Medium
     *
     * @return Redirect
     */
    public function postEnrolmentByMedium($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $mediums =  MediumEnrolment::getSchoolMediums($schoolCode);

        foreach($mediums as $medium) {
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                $rules += array(
                    "$class-{$medium['id']}-boys" => 'integer|min:0',
                    "$class-{$medium['id']}-girls" => 'integer|min:0',
                );
            }
        }

        $sum = Enrolment::sum($schoolCode, 1);
        foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {

            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "regex:/$sum_boy/",
                "total-girls-$class" => "regex:/$sum_girl/",
            );
        }
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());

        }
        foreach($mediums as $medium) {
            $data = array();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                $data += array(
                    "C{$class}_B" => (Input::get("$class-{$medium['id']}-boys")!='')?Input::get("$class-{$medium['id']}-boys"):null,
                    "C{$class}_G" => (Input::get("$class-{$medium['id']}-girls")!='')?Input::get("$class-{$medium['id']}-girls"):null,
                );
            }
            MediumEnrolment::updateMediumEnrolment($data, $medium['id'], $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-medium', $schoolCode)->with('success', 'Saved');
    }

    /* page for Enrolment in current academic session by age - HS Class
     *
     * @return View
     */
    public function enrolmentByAgeHs($schoolCode)
    {
        $master = App::make('master');

        if(!$master->hasSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 10) {
            $highClass = 10;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 9) {
            $lowClass = 8;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        $sum = Enrolment::sum($schoolCode, 1);
        $all = AgeEnrolment::findRecords($schoolCode)
            ->where('CASTEID', '=', 0)
            ->orderBy('AGEID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.enrolmentByAge', compact('sum'))
                ->with('ageData', $all);
        }

        return View::make('backend.enrolment.enrolmentByAgeHs', compact('master'))
            ->with('all', $all)
            ->with('sum', $sum)
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('agelimit', AgeEnrolment::$agelimit);
    }

    public function postEnrolmentByAgeHs($schoolCode)
    {
        $master = App::make('master');
        $rules = array();

        if(!$master->hasSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 10) {
            $highClass = 10;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 9) {
            $lowClass = 8;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        foreach(range($lowClass, $highClass) as $class) {
            foreach(range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]) as $age) {
                $rules += array(
                    "boys-$class-$age" => 'integer|min:0',
                    "girls-$class-$age" => 'integer|min:0'
                );
            }
        }

        $sum = Enrolment::sum($schoolCode, 1);
        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "regex:/$sum_boy/",
                "total-girls-$class" => "regex:/$sum_girl/",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors())
            ->withError('total students you entered does not match total number of students. Please fix them.');
        }

        foreach(range(12, 23) as $age) {
            $data = array();
            foreach(range($lowClass, $highClass) as $class) {
                if( ! in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]))) {
                    continue;
                }
                $data += array(
                    "C{$class}B" => (Input::get("boys-$class-$age")!='')?Input::get("boys-$class-$age"):null,
                    "C{$class}G" => (Input::get("girls-$class-$age")!='')?Input::get("girls-$class-$age"):null,
                );
            }
            AgeEnrolment::updateEnrolment($data,0, $age, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-age-hs', $schoolCode)->with('success', 'Saved');
    }



    /**
     * page for Enrolment in current academic session by age - SS Class
     *
     * @return View
     */
    public function enrolmentByAgeSs($schoolCode)
    {
        $master = App::make('master');
        $all = AgeEnrolment::findRecords($schoolCode)
            ->where('CASTEID', '=', 0)
            ->orderBy('AGEID')
            ->get();

        if(!$master->hasHigherSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 11) {
            $highClass = 12;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 12) {
            $lowClass = 11;
        } else {
            $lowClass = $master->LOWCLASS;
        }
        $sum = Enrolment::sum($schoolCode, 1);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.enrolmentByAge', compact('sum'))
                ->with('ageData', $all);
        }
        return View::make('backend.enrolment.enrolmentByAgeSs', compact('master'))
            ->with('all', $all)
            ->with('agelimit', AgeEnrolment::$agelimit)
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('sum', $sum);
    }

    public function postEnrolmentByAgeSs($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasHigherSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 11) {
            $highClass = 12;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 12) {
            $lowClass = 11;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        foreach(range($lowClass, $highClass) as $class) {
            foreach(range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]) as $age) {
                $rules += array(
                    "boys-$class-$age" => 'integer|min:0',
                    "girls-$class-$age" => 'integer|min:0');
            }

        }

        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "regex:/$sum_boy/",
                "total-girls-$class" => "regex:/$sum_girl/",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors())
            ->withError('total students you entered does not match total number of students. Please fix them.');
        }

        foreach(range(14, 23) as $age) {
            $data = array();
            foreach(range($lowClass, $highClass) as $class) {
                if( ! in_array($age, range(AgeEnrolment::$agelimit[$class][0], AgeEnrolment::$agelimit[$class][1]))) {
                    continue;
                }
                $data += array(
                    "C{$class}B" => (Input::get("boys-$class-$age")!='')?Input::get("boys-$class-$age"):null,
                    "C{$class}G" => (Input::get("girls-$class-$age")!='')?Input::get("girls-$class-$age"):null,
                );
            }
            AgeEnrolment::updateEnrolment($data,0, $age, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('enrolment-by-age-ss', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Repeaters by social category
     *
     * @return View
     */
    public function repeatersBySocialCategory($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        $categoryData = Repeater::findRecords($schoolCode)
            ->whereIn('ITEMID', array(1, 2, 3, 4))
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('ITEMID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.repeatersBySocialCategory', compact('sum'))
                ->with('repeatersData', $categoryData);
        }
        return View::make('backend.enrolment.repeatersBySocialCategory', compact('sum'))
            ->with('categoryData', $categoryData);
    }

    /**
     * save enrolment by social category form
     *
     * @return Redirect
     */
    public function postRepeatersBySocialCategory($schoolCode)
    {
        $master = App::make('master');
        $rules = array();

        foreach(Enrolment::$CATEGORY as $categoryId => $category) {
            $sum = Enrolment::sumMinority($schoolCode, $categoryId, 1);
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                $sumBoys = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
                $sumGirls = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
                $rules += array(
                    "boys-$class-$categoryId" => "integer|min:0|max:$sumBoys",
                    "girls-$class-$categoryId" => "integer|min:0|max:$sumGirls",
                );
            }
        }

        $sum = Enrolment::sum($schoolCode, 1);
        foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
            $sumBoys = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sumGirls = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sumBoys",
                "total-girls-$class" => "integer|min:0|max:$sumGirls",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Enrolment::$CATEGORY as $categoryId => $category) {

            $data = array();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {

                $data += array(
                    "C{$class}_B" => e(Input::get("boys-$class-$categoryId")),
                    "C{$class}_G" => e(Input::get("girls-$class-$categoryId")),
                );
            }
            Repeater::updateRepeater($data, $categoryId, 1, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('repeaters-by-category', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Repeaters by social category - Minority
     *
     * @return View
     */
    public function repeatersBySocialCategoryMinority($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        $categoryData = Repeater::findRecords($schoolCode)
            ->whereIn('ITEMID', array(5, 6, 7, 8, 9, 10))
            ->where('ITEMIDGROUP', '=', 2)
            ->orderBy('ITEMID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.repeatersBySocialCategoryMinority', compact('sum'))
                ->with('repeatersMinorityData', $categoryData);
        }
        return View::make('backend.enrolment.repeatersBySocialCategoryMinority', compact('sum', 'categoryData'));
    }

    /**
     * save Repeaters by social category Minority  form
     *
     * @return Redirect
     */
    public function postRepeatersBySocialCategoryMinority($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 2);
        foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category) {
            $sum = Enrolment::sumMinority($schoolCode, $categoryId);
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                $rules += array(
                    "boys-$class-$categoryId" => 'integer|min:0|max:'. $sum->{"S{$class}B"} , 
                    "girls-$class-$categoryId" => 'integer|min:0|max:'. $sum->{"S{$class}G"} ,
                );
            }
        }
        $sum = Enrolment::sum($schoolCode, 1);
        foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Enrolment::$CATEGORYMINORITY as $categoryId => $category) {

            $data = array();
            foreach(range($master->LOWCLASS, $master->HIGHCLASS) as $class) {

                $data += array(
                    "C{$class}_B" => e(Input::get("boys-$class-$categoryId")),
                    "C{$class}_G" => e(Input::get("girls-$class-$categoryId")),
                );
            }
            Repeater::updateRepeater($data, $categoryId, 2, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('repeaters-minority-category', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Children with Special needs
     *
     * @return View
     */
    public function childrenWithSpecialNeeds($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 5) {
            $highClass = 4;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        $lowClass = $master->LOWCLASS;
        $disabilityData = Cwsn::findRecords($schoolCode)
            ->whereIn('DISABILITY_TYPE', range(1, 10))
            ->orderBy('DISABILITY_TYPE')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.childrenWithSpecialNeeds', compact('sum', 'highClass', 'lowClass', 'disabilityData'));
        }
        return View::make('backend.enrolment.childrenWithSpecialNeeds', compact('sum', 'highClass', 'lowClass', 'disabilityData'));
    }

    /**
     * save Children with Special Needs
     *
     * @return Redirect
     */
    public function postChildrenWithSpecialNeeds($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 5) {
            $highClass = 4;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        $lowClass = $master->LOWCLASS;
        foreach(range($lowClass, $highClass) as $class) {
            foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {
                $rules += array(
                    "boys-$class-$disabilityId" => 'integer|min:0',
                    "girls-$class-$disabilityId" => 'integer|min:0'
                );
            }
        }
        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($lowClass, $highClass) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {

            $data = array();
            foreach(range($lowClass, $highClass) as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$disabilityId")!='')?Input::get("boys-$class-$disabilityId"):null,
                    "C{$class}_G" => (Input::get("girls-$class-$disabilityId")!='')?Input::get("girls-$class-$disabilityId"):null,
                );
            }
            Cwsn::updateCwsn($data, $disabilityId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('children-with-special-needs', $schoolCode)->with('success', 'Saved');
    }

    /* CHILDREN WITH SPECIAL NEEDS ALL CATEGORIES*/


    /**
     * Children with Special needs UP
     *
     * @return View
     */
    public function childrenWithSpecialNeedsUp($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        if( ! $master->hasUpperPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 8) {
            $highClass = 7;
        } elseif($master->HIGHCLASS == 8) {
            $highClass = 8;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 5) {
            $lowClass = 5;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        $disabilityData = Cwsn::findRecords($schoolCode)
            ->whereIn('DISABILITY_TYPE', range(1, 10))
            ->orderBy('DISABILITY_TYPE')
            ->get();


        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.childrenWithSpecialNeeds', compact('sum', 'highClass', 'lowClass', 'disabilityData'));
        }
        return View::make('backend.enrolment.childrenWithSpecialNeedsUp', compact('sum'))
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('disabilityData', $disabilityData);
    }

    /**
     * save Children with Special Needs UP
     *
     * @return Redirect
     */
    public function postChildrenWithSpecialNeedsUp($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasUpperPrimary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 8) {
            $highClass = 7;
        } elseif($master->HIGHCLASS == 8) {
            $highClass = 8;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 5) {
            $lowClass = 5;
        } else {
            $lowClass = $master->LOWCLASS;
        }
        foreach(range($lowClass, $highClass) as $class) {
            foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {
                $rules += array(
                    "boys-$class-$disabilityId" => 'integer|min:0',
                    "girls-$class-$disabilityId" => 'integer|min:0'
                );
            }
        }
        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($lowClass, $highClass) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {

            $data = array();
            foreach(range($lowClass, $highClass) as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$disabilityId")!='')?Input::get("boys-$class-$disabilityId"):null,
                    "C{$class}_G" => (Input::get("girls-$class-$disabilityId")!='')?Input::get("girls-$class-$disabilityId"):null,
                );
            }
            Cwsn::updateCwsn($data, $disabilityId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('children-with-special-needs-up', $schoolCode)->with('success', 'Saved');
    }


    /**
     * Children with Special needs HS
     *
     * @return View
     */
    public function childrenWithSpecialNeedsHs($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        if( ! $master->hasSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 10) {
            $highClass = 10;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 9) {
            $lowClass = 8;
        } else {
            $lowClass = $master->LOWCLASS;
        }
        $disabilityData = Cwsn::findRecords($schoolCode)
            ->whereIn('DISABILITY_TYPE', range(1, 10))
            ->orderBy('DISABILITY_TYPE')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.childrenWithSpecialNeeds', compact('sum', 'highClass', 'lowClass', 'disabilityData'));
        }
        return View::make('backend.enrolment.childrenWithSpecialNeedsHs', compact('sum', 'master'))
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('disabilityData', $disabilityData);
    }

    /**
     * save Children with Special Needs HS
     *
     * @return Redirect
     */
    public function postChildrenWithSpecialNeedsHs($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);

        if( ! $master->hasSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 10) {
            $highClass = 10;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 9) {
            $lowClass = 8;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        foreach(range($lowClass, $highClass) as $class) {
            foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {
                $rules += array(
                    "boys-$class-$disabilityId" => 'integer|min:0',
                    "girls-$class-$disabilityId" => 'integer|min:0'
                );
            }
        }

        foreach(range($lowClass, $highClass) as $class) {
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($lowClass, $highClass) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {

            $data = array();
            foreach(range($lowClass, $highClass) as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$disabilityId")!='')?Input::get("boys-$class-$disabilityId"):null,
                    "C{$class}_G" => (Input::get("girls-$class-$disabilityId")!='')?Input::get("girls-$class-$disabilityId"):null,
                );
            }
            Cwsn::updateCwsn($data, $disabilityId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('children-with-special-needs-hs', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Children with Special needs HSS
     *
     * @return View
     */
    public function childrenWithSpecialNeedsHss($schoolCode)
    {
        $master = App::make('master');
        $sum = Enrolment::sum($schoolCode, 1);

        if(!$master->hasHigherSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 11) {
            $highClass = 12;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 12) {
            $lowClass = 11;
        } else {
            $lowClass = $master->LOWCLASS;
        }
        $disabilityData = Cwsn::findRecords($schoolCode)
            ->whereIn('DISABILITY_TYPE', range(1, 10))
            ->orderBy('DISABILITY_TYPE')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.childrenWithSpecialNeeds', compact('sum', 'highClass', 'lowClass', 'disabilityData'));
        }
        return View::make('backend.enrolment.childrenWithSpecialNeedsHss', compact('sum'))
            ->with('highClass', $highClass)
            ->with('lowClass', $lowClass)
            ->with('disabilityData', $disabilityData);
    }

    /**
     * save Children with Special Needs HSS
     *
     * @return Redirect
     */
    public function postChildrenWithSpecialNeedsHss($schoolCode)
    {
        $master = App::make('master');
        $rules = array();
        $sum = Enrolment::sum($schoolCode, 1);
        if( ! $master->hasHigherSecondary()){
            return Redirect::route('account')->with('error', 'not allowed');
        }
        if($master->HIGHCLASS > 11) {
            $highClass = 12;
        } else {
            $highClass = $master->HIGHCLASS;
        }
        if($master->LOWCLASS < 12) {
            $lowClass = 11;
        } else {
            $lowClass = $master->LOWCLASS;
        }

        foreach(range($lowClass, $highClass) as $class) {
            foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {
                $rules += array(
                    "boys-$class-$disabilityId" => 'integer|min:0',
                    "girls-$class-$disabilityId" => 'integer|min:0'
                );
            }
            $sum_boy = $sum->{"S{$class}B"} == '' ? '0':$sum->{"S{$class}B"};
            $sum_girl = $sum->{"S{$class}G"} == '' ? '0':$sum->{"S{$class}G"};
            $rules += array(
                "total-boys-$class" => "integer|min:0|max:$sum_boy",
                "total-girls-$class" => "integer|min:0|max:$sum_girl",
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            $totalError = '';
            $errors = $validator->errors();
            foreach(range($lowClass, $highClass) as $class) {
                if($errors->has("total-boys-$class") || $errors->has("total-girls-$class")) {
                    $totalError = 'Sorry!! Your entry exceeds the total students admitted';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        foreach(Cwsn::$DISABILITIES as $disabilityId => $disability) {

            $data = array();
            foreach(range($lowClass, $highClass) as $class) {

                $data += array(
                    "C{$class}_B" => (Input::get("boys-$class-$disabilityId")!='')?Input::get("boys-$class-$disabilityId"):null,
                    "C{$class}_G" => (Input::get("girls-$class-$disabilityId")!='')?Input::get("girls-$class-$disabilityId"):null,
                );
            }
            Cwsn::updateCwsn($data, $disabilityId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('children-with-special-needs-hss', $schoolCode)->with('success', 'Saved');
    }


    /**
     * Facilities Provided to CWSN
     *
     * @return View
     */
    public function facilitiesCwsn($schoolCode)
    {
        $master = App::make('master');
        $cwsnFacilityData = CwsnFacility::findRecords($schoolCode)
            ->whereIn('ITEMID', range(1, 9))
            ->orderBy('ITEMID')
            ->get();

        $sections = array();
        if($master->hasElementary()) {
            $sections[] = 0;
        }
        if($master->hasSecondary()) {
            $sections[] = 1;
        }
        if($master->hasHigherSecondary()) {
            $sections[] = 2;
        }

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.enrolment.facilitiesCwsn', compact('master', 'sections', 'cwsnFacilityData'));
        }
        return View::make('backend.enrolment.facilitiesCwsn', compact('master', 'sections', 'cwsnFacilityData'));
    }

    /**
     * save Facilities
     *
     * @return Redirect
     */
    public function postFacilitiesCwsn($schoolCode)
    {
        $master = App::make('master');
        $sections = array();
        if($master->hasElementary()) {
            $sections[] = 0;
        }
        if($master->hasSecondary()) {
            $sections[] = 1;
        }
        if($master->hasHigherSecondary()) {
            $sections[] = 2;
        }
        $rules = array();
        $sumAllSpecial = $sumGirls = 0;
        foreach(CwsnFacility::$CWSNFACILITY as $cwsnFacilityId => $cwsnFacility) {
            foreach($sections as $sectionKey) {
                $abbr = CwsnFacility::$SECTIONS[$sectionKey][0];
                $rules += array(
                    "boys-$abbr-$cwsnFacilityId" => 'integer|min:0',
                    "girls-$abbr-$cwsnFacilityId" => 'integer|min:0'
                );
            }
                $sumAllSpecial += (int)e(Input::get("boys-$abbr-$cwsnFacilityId"));
                $sumAllSpecial += (int)e(Input::get("girls-$abbr-$cwsnFacilityId"));
        }
        $sum = Enrolment::sumUpDisabled($schoolCode, 1);
        if ($sumAllSpecial >= $sum->total) {
            return Redirect::back()->withInput()->withError("No of special children exceeds. Please review Children with special needs");
        }
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach(CwsnFacility::$CWSNFACILITY as $cwsnFacilityId => $cwsnFacility) {

            $data = array();
            foreach($sections as $sectionKey) {
                $abbr = CwsnFacility::$SECTIONS[$sectionKey][0];
                $data += array(
                    "TOT_B$abbr" => (Input::get("boys-$abbr-$cwsnFacilityId")!='')?Input::get("boys-$abbr-$cwsnFacilityId"):null,
                    "TOT_G$abbr" => (Input::get("girls-$abbr-$cwsnFacilityId")!='')?Input::get("girls-$abbr-$cwsnFacilityId"):null,
                );
            }

            CwsnFacility::updateCwsnFacility($data, $cwsnFacilityId, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('facilities-cwsn', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Facilities Provided to Children Primary
     *
     * @return View
     */
    public function facilitiesToChildrenPrimary($schoolCode)
    {
        return $this->facilitiesToChildren('Primary', $schoolCode);
    }

    public function postFacilitiesToChildrenPrimary($schoolCode)
    {
        return $this->postFacilitiesToChildren('Primary', $schoolCode);
    }


    /**
     * Facilities Provided to Children Upper-primary
     *
     * @return View
     */
    public function facilitiesToChildrenUp($schoolCode)
    {
        return $this->facilitiesToChildren('UpperPrimary', $schoolCode);
    }

    public function postFacilitiesToChildrenUp($schoolCode)
    {
        return $this->postFacilitiesToChildren('UpperPrimary', $schoolCode);
    }

    /**
     * Facilities Provided to Children
     *
     * @return View
     */
    protected function facilitiesToChildren($section, $schoolCode)
    {
        $master = App::make('master');

        if($section == 'Primary') {
            $gradeId = 5;
            if( ! $master->hasPrimary()) {
                // Not a primary School
                return Redirect::route('account')->with('error', 'not permitted');
            }
        } elseif($section == 'UpperPrimary') {
            $gradeId = 8;
            if( ! $master->hasUpperPrimary()) {
                // Not an Upper Primary School
                return Redirect::route('account')->with('error', 'not permitted');
            }
        }

        $facilityData = Incentive::findRecords($schoolCode)
            ->whereIn('INCENT_TYPE', range(1, 6))
            ->where('GRADEPRUPR', '=', $gradeId)
            ->orderBy('INCENT_TYPE')
            ->get();

        if($master->school->confirmation()) {
            if($section == 'Primary') {
                $facilitiesLabel[] = 'Facilities provided to Children(Last academic Year,Primary Classes)';
            } else {
                $facilitiesLabel[] = 'Facilities provided to Children(Last academic Year,Upper Primary Classes)';
            }

            $childrensFacility[] = $facilityData;
            return View::make('backend.confirmed.enrolment.facilitiesToChildren',
                compact('childrensFacility', 'facilitiesLabel', 'section'));
        }
        return View::make('backend.enrolment.facilitiesToChildren', compact('section', 'facilityData'));
    }

    /**
     * save Facilities
     *
     * @return Redirect
     */
    protected function postFacilitiesToChildren($section, $schoolCode)
    {
        if($section == 'Primary') {
            $gradeId = 5;
        } elseif($section == 'UpperPrimary') {
            $gradeId = 8;
        }

        $rules = array();

        foreach(Incentive::$TYPE as $typeId => $type) {
            foreach(Incentive::$CATEGORY as $catabbr => $category) {

                $rules += array(
                    "$catabbr-boys-$typeId" => 'integer|min:0',
                    "$catabbr-girls-$typeId" => 'integer|min:0'
                );
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        foreach(Incentive::$TYPE as $typeId => $type) {

            $data = array();
            foreach(Incentive::$CATEGORY as $catabbr => $category) {
                $data += array(
                    "{$catabbr}_B" => (Input::get("$catabbr-boys-$typeId")!='')?Input::get("$catabbr-boys-$typeId"):null,
                    "{$catabbr}_G" => (Input::get("$catabbr-girls-$typeId")!='')?Input::get("$catabbr-girls-$typeId"):null,
                );
            }

            Incentive::updateIncentive($data, $typeId, $gradeId, $schoolCode);
        }

        if($section == 'Primary') {
            SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        } elseif($section == 'UpperPrimary') {
            SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        }
        return Redirect::route(Input::get('_route'), $schoolCode)->with('success', 'Saved');
    }

}
