<?php namespace Controllers\Admin;

use AuthorizedController;
use Sentry;
use Input;
use School;
use Facility;
use Result;
use StreamEnrolment;
use Master;
use AcademicYear;
use Enrolment;
use SchoolStatus;
use Redirect;
use Validator;
use DB;
use App;
use View;
use Illuminate\Routing\Controller;

class ResultController extends AuthorizedController {

    /**
     * form for collecting Results of class X examination for previous academic year - by category
     *
     * @return View
     */
    public function resultXByCategory($schoolCode)
    {
        $master = App::make('master');
        $results = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 1)
            ->orderBy('ITEMID')
            ->get();

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.result.xResultsByCategory')
                ->with('xResults', $results);
        }
        return View::make('backend.result.xResultsByCategory', compact('results'));
    }

    /**
     * save Results of class X examination for previous academic year - by category
     *
     * @return Redirect
     */
    public function postResultXByCategory($schoolCode)
    {
        // applying validations
        $rules = array();
        foreach(Enrolment::$CATEGORY as $categoryId => $category) {
            $rules += array(
                "boys-appeared-$categoryId" => 'integer|min:0',
                "girls-appeared-$categoryId" => 'integer|min:0',
                "boys-passed-$categoryId" => 'integer|min:0|max:'.Input::get("boys-appeared-$categoryId", 0),
                "girls-passed-$categoryId" => 'integer|min:0|max:'.Input::get("girls-appeared-$categoryId", 0),
            );
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        // saving data
        foreach(Enrolment::$CATEGORY as $categoryId => $category) {
            $data = array(
                'C10B_GEN' => (Input::get("boys-appeared-$categoryId")!='')?Input::get("boys-appeared-$categoryId"):null,
                'C10G_GEN' => (Input::get("girls-appeared-$categoryId")!='')?Input::get("girls-appeared-$categoryId"):null,
                'C10B_SC' => (Input::get("boys-passed-$categoryId")!='')?Input::get("boys-passed-$categoryId"):null,
                'C10G_SC' => (Input::get("girls-passed-$categoryId")!='')?Input::get("girls-passed-$categoryId"):null,
            );
            Result::updateResult($data, $categoryId, 1, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('x-result-by-category', $schoolCode)->with('success', 'Saved');
    }

    public function resultByScoreX($schoolCode)
    {
        return $this->resultByScore('X', $schoolCode);
    }

    public function postResultByScoreX($schoolCode)
    {
        return $this->postResultByScore('X', $schoolCode);
    }

    public function resultByScoreXII($schoolCode)
    {
        return $this->resultByScore('XII', $schoolCode);
    }

    public function postResultByScoreXII($schoolCode)
    {
        return $this->postResultByScore('XII', $schoolCode);
    }

    /**
     * form for collecting Results of class X, XII examination for previous academic year - by score
     *
     * @return View
     */
    protected function resultByScore($class, $schoolCode)
    {
        $master = App::make('master');
        if($class == 'X') {
            $groupId = 3;
        } elseif($class == 'XII') {
            $groupId = 5;
        }

        $results = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', $groupId)
            ->orderBy('ITEMID')
            ->get();

        if($master->school->confirmation()) {
            // result by score
            $resultLabel = [];
            $scoreResults[] = $results;
            if($class == 'X') {
                $resultLabel[] = array(
                    'label' => "Number of students passed/qualified the Secondary School Board (Class X) examination in previous academic year",
                    'class' => 'secondary');
            } else {
                $resultLabel[] = array(
                    'label' => "Result of class XII previous year -- by percentage of mark",
                    'class' => 'hs');
            }

            return View::make('backend.confirmed.result.resultByScore', compact('scoreResults', 'resultLabel', 'class'));
        }
        return View::make('backend.result.resultByScore', compact('results'))
            ->with('class', $class);
    }

    /**
     * save Results of class X examination for previous academic year - by category
     *
     * @return Redirect
     */
    protected function postResultByScore($class, $schoolCode)
    {
        if($class == 'X') {
            $groupId = 3;
        } elseif($class == 'XII') {
            $groupId = 5;
        }

        // applying validations
        $rules = array();
        foreach(Result::$SCORE as $scoreId => $score) {
            foreach(Result::$CATEGORY_LIST as $categoryId => $category) {
                $rules += array(
                    "boys-$categoryId-$scoreId" => 'integer|min:0',
                    "girls-$categoryId-$scoreId" => 'integer|min:0',
                );
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        // saving data
        foreach(Result::$SCORE as $scoreId => $score) {
            $data = array();
            foreach(Result::$CATEGORY_LIST as $categoryId => $category) {
                $data += array(
                    "C10B_{$category['abbr']}" => (Input::get("boys-$categoryId-$scoreId")!='')?Input::get("boys-$categoryId-$scoreId"):null,
                    "C10G_{$category['abbr']}" => (Input::get("girls-$categoryId-$scoreId")!='')?Input::get("girls-$categoryId-$scoreId"):null,
                );
            }
            Result::updateResult($data, $scoreId, $groupId, $schoolCode);
        }

        if($class == 'X') {
            SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        } elseif($class == 'XII') {
            SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        }
        return Redirect::route(Input::get('_route'), $schoolCode)->with('success', 'Saved');
    }

    /**
     * form for collecting Results of class XII examination for previous academic year - by category
     *
     * @return View
     */
    public function resultByStream($schoolCode)
    {
        $master = App::make('master');
        $streams = Facility::getStreamList($schoolCode);
        $results = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 4)
            ->whereIn('ITEMID', $streams)
            ->orderBy('ITEMID')
            ->get();
        if($master->school->confirmation()) {
            return View::make('backend.confirmed.result.resultByStream', compact('master', 'streams'))
                ->with('streamResults', $results);
        }
        return View::make('backend.result.resultByStream', compact('results', 'master', 'streams'));
    }

    /**
     * save Results of class XII examination for previous academic year - by category
     *
     * @return Redirect
     */
    public function postResultByStream($schoolCode)
    {
        $streams = Facility::getStreamList($schoolCode);
        // applying validations
        $rules = array();
        foreach($streams as $stream) {
            foreach(Result::$CATEGORY_LIST as $categoryId => $category) {
                $rules += array(
                    "boys-appeared-$categoryId-$stream" => 'integer|min:0',
                    "girls-appeared-$categoryId-$stream" => 'integer|min:0',
                    "boys-passed-$categoryId-$stream" => 'integer|min:0|max:'.Input::get("boys-appeared-$categoryId-$stream", 0),
                    "girls-passed-$categoryId-$stream" => 'integer|min:0|max:'.Input::get("girls-appeared-$categoryId-$stream", 0),
                );
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        // saving data
        foreach($streams as $stream) {
            $data = array();
            foreach(Result::$CATEGORY_LIST as $categoryId => $category) {
                $data += array(
                    "C10B_{$category['abbr']}" => (Input::get("boys-appeared-$categoryId-$stream")!='')?Input::get("boys-appeared-$categoryId-$stream"):null,
                    "C10G_{$category['abbr']}" => (Input::get("girls-appeared-$categoryId-$stream")!='')?Input::get("girls-appeared-$categoryId-$stream"):null,
                    "C12B_{$category['abbr']}" => (Input::get("boys-passed-$categoryId-$stream")!='')?Input::get("boys-passed-$categoryId-$stream"):null,
                    "C12G_{$category['abbr']}" => (Input::get("girls-passed-$categoryId-$stream")!='')?Input::get("girls-passed-$categoryId-$stream"):null,
                );
            }
            Result::updateResult($data, $stream, 4, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('result-by-stream', $schoolCode)->with('success', 'Saved');
    }

    /**
     * get exam results of 4/5, 7/8 seperately for general, sc, st, obc categories
     *
     * @param $schoolCode
     * @return View
     */
    function getExamResultsByCategory($schoolCode)
    {
        $master = App::make('master');
        $classes = array();
        if (($master->LOWCLASS <= 5 && $master->HIGHCLASS >= 5) || ($master->LOWCLASS <= 4 && $master->HIGHCLASS >= 4)){
            $classes[] = array('id'=>'5', 'name'=>'IV/V');
        }
        if (($master->LOWCLASS <= 7 && $master->HIGHCLASS >= 7) || ($master->LOWCLASS <= 8 && $master->HIGHCLASS >= 8)){
            $classes[] = array('id'=>'8', 'name'=>'VII/VIII');
        }

        $resultsElementary = Result::findRecords($schoolCode)
            ->where('ITEMIDGROUP', '=', 6)
            ->whereIn('ITEMID', range(1, 4))
            ->orderBy('ITEMID')
            ->get();

        // inserting missing rows
        if(count($resultsElementary) != 4) {
            $missingItems = array_diff(range(1, 4), $resultsElementary->lists('ITEMID'));
            foreach($missingItems as $item) {
                $data[] = array(
                    'ITEMID' => $item,
                    'ITEMIDGROUP' => 6,
                    'AC_YEAR' => AcademicYear::CURRENT_YEAR,
                    'SCHCD' => $schoolCode
                );
            }
            Result::unguard();
            Result::insert($data);
            Result::reguard();
            $resultsElementary = Result::findRecords($schoolCode)
                ->where('ITEMIDGROUP', '=', 6)
                ->whereIn('ITEMID', range(1, 4))
                ->orderBy('ITEMID')
                ->get();
        }

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.result.examResults', compact('resultsElementary', 'classes'));
        }
        return View::make('backend.result.examResults', compact('resultsElementary', 'classes'));
    }

    /**
     * save exam results of 4/5, 7/8 seperately for general, sc, st, obc categories
     *
     * @param $schoolCode
     * @return Redirect
     */
    function postExamResultsByCategory($schoolCode)
    {
        $master = App::make('master');
        $classes = array();
        if (($master->LOWCLASS <= 5 && $master->HIGHCLASS >= 5) || ($master->LOWCLASS <= 4 && $master->HIGHCLASS >= 4)){
            $classes[] = array('id'=>'5', 'name'=>'IV/V');
        }
        if (($master->LOWCLASS <= 7 && $master->HIGHCLASS >= 7) || ($master->LOWCLASS <= 8 && $master->HIGHCLASS >= 8)){
            $classes[] = array('id'=>'8', 'name'=>'VII/VIII');
        }

        $rules = array();
        foreach(Result::$RESULT_HEADS as $headId => $head) {
            foreach(Result::$CATEGORY_LIST as $cat) {
                foreach($classes as $class) {
                    $rules += array(
                        "{$cat['abbr']}-Boys-{$class['id']}-$headId" => 'integer|min:0',
                        "{$cat['abbr']}-Girls-{$class['id']}-$headId" => 'integer|min:0',
                    );
                }
            }
        }

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        // saving data
        foreach(Result::$RESULT_HEADS as $headId => $head) {
            $data = array();
            foreach(Result::$CATEGORY_LIST as $cat) {
                foreach($classes as $class) {
                    $b = null;
                    $g = null;
                    if (Input::get("{$cat['abbr']}-Boys-{$class['id']}-$headId")!='') {
                         $b = Input::get("{$cat['abbr']}-Boys-{$class['id']}-$headId");
                    }
                    if (Input::get("{$cat['abbr']}-Girls-{$class['id']}-$headId")!='') {
                        $g = Input::get("{$cat['abbr']}-Girls-{$class['id']}-$headId");
                    }

                    $data += array(
                        "C{$class['id']}B_{$cat['abbr']}" => $b,
                        "C{$class['id']}G_{$cat['abbr']}" => $g,
                    );
                }
            }
            Result::updateResult($data, $headId, 6, $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::back()->with('success', 'Saved');
    }

}
