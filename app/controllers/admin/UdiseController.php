<?php namespace Controllers\Admin;

use AuthorizedController;
use Illuminate\Routing\Controller;
use Input;
use Lang;
use User;
use App;
use AcademicYear;
use School;
use Master;
use District;
use Block;
use EducationBlock;
use Constituency;
use Municipality;
use Village;
use Cluster;
use Panchayath;
use City;
use Habitation;
use Building;
use Facility;
use FacilityDetail;
use Mdm;
use Teacher;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Rte;
use Material;
use Smdc;
use SchoolWarning;
use Warning;
use Route;
use SchoolStatus;
use SchoolDetails;
use Enrolment;
use DB;
use Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;

class UdiseController extends AuthorizedController {

    public $choice = array(
        0 => '',
        1 => '1 - Yes',
        2 => '2 - No',
        '' => ''
    );

    public $emptyValues = array(
        0 => '',
        '' => '',
        98 => ''
    );

    public function __construct()
    {
        View::share('choice', $this->choice);
    }
    /**
     * school Particulars form 1
     * @param int $id
     * @return View
     */
    public function getParticulars($schoolCode)
    {
        $schoolMaster =  Master::findRecord($schoolCode);
        $user = Sentry::getUser();

        if ( ! $user->hasAccessToSchool($schoolMaster->school)) {
            return Response::view('errors.403', array(), 403);
        }
        $district = substr($schoolCode, 0, 4);
        $school = School::find($schoolCode);
        $villages = [''=>'.... Select ....']+Village::getList($schoolMaster->BLKCD);
        $clusters = [''=>'.... Select ....']+Cluster::getList($schoolMaster->BLKCD);
        $habitations = [''=>'.... Select ....']+Habitation::getList($schoolMaster->BLKCD);
        $eduBlocks = [''=>'.... Select ....']+EducationBlock::getList($schoolMaster->BLKCD);
        $constituencies= [''=>'.... Select ....']+Constituency::getList($schoolMaster->BLKCD);
        $municipalities = [''=>'.... Select ....']+Municipality::getList($schoolMaster->BLKCD);
        $cities = [''=>'.... Select ....']+City::getList($schoolMaster->BLKCD);
        $panchayaths = [''=>'.... Select ....']+Panchayath::getList($schoolMaster->BLKCD);
        $languages = [''=>'.... Select ....']+DB::table('STEPS_MEDINSTR')->lists('MEDINSTR_DESC', 'MEDINSTR_ID');
        $block = Block::where('BLKCD', '=', $schoolMaster->BLKCD)
            ->first(array('BLKCD as id', DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name')));

        if($schoolMaster->school->confirmation()) {
            $areas = School::$AREA+$this->emptyValues;
            $mediums = Master::$INSTRUCTION_MEDIUMS+$this->emptyValues;
            $types = School::$TYPES+$this->emptyValues;
            $managements = School::$MANAGEMENTS+$this->emptyValues;
            $mediums[98] = '';
            $boards = Master::$BOARDS+$this->emptyValues;
            $religiousMinorities = Master::$RELIGIOUS_MINORITIES+$this->emptyValues;
            $languages = DB::table('STEPS_MEDINSTR')->lists('MEDINSTR_DESC', 'MEDINSTR_ID')+$this->emptyValues;;

            $village = Village::where('VILCD', '=', $schoolMaster->VILCD)
                ->first(array('VILCD as id', DB::raw('CONCAT(VILNAME, " - ", VILCD) as name')));
            $cluster = Cluster::where('CLUCD', '=', $schoolMaster->CLUCD)
                ->first(array('CLUCD as id', DB::raw('CONCAT(CLUNAME, " - ", CLUCD) as name')));
            $habitation = Habitation::where('HABCD', '=', $schoolMaster->HABITCD)
                ->first(array('HABCD as id', DB::raw('CONCAT(HABNAME, " - ", HABCD) as name')));
            $eduBlock = EducationBlock::where('BLKCD', '=', $schoolMaster->EDUBLKCD)
                ->first(array('BLKCD as id', DB::raw('CONCAT(BLKNAME, " - ", BLKCD) as name')));
            $constituency = Constituency::where('CONSTCD', '=', $schoolMaster->ACONSTCD)
                ->first(array('CONSTCD as id', DB::raw('CONCAT(CONSTNAME, " - ", CONSTCD) as name')));
            $municipality = Municipality::where('MUNCD', '=', $schoolMaster->MUNICIPALCD)
                ->first(array('MUNCD as id', DB::raw('CONCAT(MUNNAME, " - ", MUNCD) as name')));
            $city = City::where('CITYCD', '=', $schoolMaster->CITY)
                ->first(array('CITYCD as id', DB::raw('CONCAT(CITYNAME, " - ", CITYCD) as name')));
            $panchayat = Panchayath::where('PANCD', '=', $schoolMaster->PANCD)
                ->first(array('PANCD as id', DB::raw('CONCAT(PANNAME, " - ", PANCD) as name')));

            return View::make('backend.confirmed.udise.particularsOne', compact(
                'school', 'village', 'cluster', 'habitation', 'constituency', 'municipality',
                'city', 'block', 'panchayat', 'eduBlock',
                'areas', 'mediums', 'types', 'managements', 'boards', 'languages', 'religiousMinorities'
            ))
            ->withMaster($schoolMaster);
        }
        return View::make('backend.udise.particularsOne', compact(
            'school', 'schoolMaster',
            'villages', 'clusters', 'habitations', 'constituencies', 'municipalities',
            'cities', 'block', 'panchayaths', 'eduBlocks', 'languages'
        ));
    }

    /**
     * save school-particulars form 1
     *
     * @return Redirect
     */
    public function postParticulars($schoolCode)
    {
        $user = Sentry::getUser();
        $schoolMaster =  Master::findRecord($schoolCode);

        $lowClass = Input::get('lowest-class');
        $highClass = Input::get('highest-class');

        $rules = array(
            'rural-urban' => 'required',
            'village' => 'required',
            'cluster' => 'required',
            'habitation' => '',
            'pincode' => 'required|digits:6',
            'edu-block' => '',
            'assembly-constituency' => 'required',
            'std-code' => 'digits_between:4,5',
            'phone-number' => 'digits_between:6,7',
            'mobile' => 'digits:10',
            'respondent-std-code' => 'digits_between:4,5',
            'respondent-phone-number' => 'digits_between:6,7',
            'respondent-mobile' => 'digits:10',
            'email' => 'email',
            'website' => 'url',
            'lowest-class' => 'required|integer|min:1|max:12',
            'highest-class' => 'required|integer|min:'.e(Input::get('lowest-class', 0)).'|max:12',
            'distance-to-up' => 'sometimes|numeric|min:0',
            'distance-to-secondary' => 'sometimes|numeric|min:0',
            'road' => 'required',
            'establishment-year' => 'digits:4|integer|min:1500|max:'.(int)substr(AcademicYear::CURRENT_YEAR, 0, 4),
            'cwsn' => 'required',
            'shift' => 'required|regex:/2/',
            'religious-minority' => 'required',
            'medium-a' => 'required',
            'medium-b' => '',
            'medium-c' => '',
            'medium-d' => '',
            'latitude-degree' => 'digits:2',
            'latitude-min' => 'digits:2',
            'latitude-sec' => 'digits:2',
            'longitude-degree' => 'digits:2',
            'longitude-min' => 'digits:2',
            'longitude-sec' => 'digits:2',
        );
        //RURAL URBAN
        if (Input::get('rural-urban') == 1) {
            $rules += array(
                'panchayath' => 'required',
            );
        } else {
            $rules += array(
                'municipality' => 'required',
                'city' => 'required',
            );
        }

        if (Input::get('religious-minority') == 1) {
            $rules['religious-minority-type'] = 'required';
        }

        //CHECK FOR RESIDENTIAL CLASS
        if (Master::isPrimary($lowClass)) {
            $rules += array(
                'residential-primary' => 'required',
                'primary-taught-in-mothertongue' => 'required',
                'lang-a' => 'required',
                'lang-b' => 'required',
                'lang-c' => 'required',
                'primary-up-upgradation' => 'digits:4',
            );
        }

        if (Master::isUpperPrimary($lowClass, $highClass)) {
            $rules += array(
                'residential-up' => 'required',
            );
        }

        if (Master::isSecondary($lowClass, $highClass)) {
            $rules += array(
                'residential-secondary' => 'required',
                'pre-vocational-course' => 'required',
                'councelling' => 'required',
                'secondary-type' => 'required',
                'secondary-management' => 'required',
                'secondary-recognition' => 'digits:4|integer|min:'.e(Input::get('establishment-year')),
                'board-secondary' => 'required',
            );
            // checks if its a gov or aided school
            $govOrAided = Master::$GOV_MANAGEMENTS + [4];
            if (in_array(Input::get('secondary-management'), $govOrAided)) {
                $rules['board-secondary'] = 'required|regex:/2/';
            }
        }

        if (Master::isHigherSecondary($highClass)) {
            $rules += array(
                'residential-hs' => 'required',
                'pre-vocational-course' => 'required',
                'councelling' => 'required',
                'hs-type' => 'required',
                'hs-management' => 'required',
                'hs-recognition' => 'digits:4|integer|min:'.e(Input::get('establishment-year')),
                'board-hs' => 'required',
            );
            $govOrAided = Master::$GOV_MANAGEMENTS + [4];
            if (in_array(Input::get('hs-management'), $govOrAided)) {
                $rules['board-hs'] = 'required|regex:/2/';
            }
        }

        //CHECK FOR RESIDENCY TYPE
        if (Input::get('residential-primary') == 1) {
            $rules += array(
                'residential-type' => 'required',
            );
        }

        if (Input::get('residential-up') == 1) {
            $rules += array(
                'residential-type' => 'required',
            );
        }

        if (Input::get('residential-secondary') == 1) {
            $rules += array(
                'residential-type' => 'required',
            );
        }

        if (Input::get('residential-hs') == 1) {
            $rules += array(
                'residential-type' => 'required',
            );
        }

        //CHECK TYPE/YEAR OF RECOGNITION/UPGRADATION/MANAGEMENT etc.
        if (Master::isElementary($lowClass)) {
            $rules += array(
                'elementary-type' => 'required',
                'elementary-management' => 'required',
                'elementary-recognition' => 'digits:4|integer|min:'.e(Input::get('establishment-year')),
            );
        }

        //UPGRADATION

        if (Master::isElementary($lowClass) && Master::isSecondary($lowClass, $highClass)) {
            $rules += [
                'e-s-upgradation' => 'required|digits:4',
                ];
        }


        if (Master::isSecondary($lowClass, $highClass) && Master::isHigherSecondary($highClass)) {
            $rules += [
                's-hs-upgradation' => 'required|digits:4',
                ];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        School::where('SCHCD', '=', $schoolCode)->update(array(
            'DISTCD'=> substr($schoolCode, 0, 4),
            'VILCD' => e(Input::get('village')),
        ));
        $schoolMaster->HABITCD = abs(e(Input::get('habitation', null)));
        $lowClass = abs(e(Input::get('lowest-class')));
        $highClass = abs(e(Input::get('highest-class')));
        $data = array(
            'RURURB' => e(Input::get('rural-urban')),
            'CLUCD' => e(Input::get('cluster')),
            'VILCD' => e(Input::get('village')),
            'HABITCD' => e(Input::get('habitation')),
            'PINCD' => Input::get('pincode')?Input::get('pincode'):null,
            'EDUBLKCD' => e(Input::get('edu-block')),
            'ACONSTCD' => e(Input::get('assembly-constituency')),
            'STDCODE1' => e(Input::get('std-code')),
            'PHONE1' => e(Input::get('phone-number')),
            'MOBILE1' => e(Input::get('mobile')),
            'STDCODE2' => e(Input::get('respondent-std-code')),
            'PHONE2' => e(Input::get('respondent-phone-number')),
            'MOBILE2' => e(Input::get('respondent-mobile')),
            'EMAIL' => e(Input::get('email')),
            'WEBSITE' => e(Input::get('website')),
            'SCHCAT' => Master::getCategory(Input::get('lowest-class', 0), Input::get('highest-class', 0)),
            'LOWCLASS' => Input::get('lowest-class')?Input::get('lowest-class'):null,
            'HIGHCLASS' => Input::get('highest-class')?Input::get('highest-class'):null,
            'DISTP' => Input::get('distance-to-up')!=''?Input::get('distance-to-up'):null,
            'DISTU' => (Input::get('distance-to-secondary')!='')?Input::get('distance-to-secondary'):null,
            'APPROACHBYROAD' => e(Input::get('road')),
            'ESTDYEAR' => Input::get('establishment-year')?Input::get('establishment-year'):null,
            'CWSNSCH_YN' => Input::get('cwsn')?Input::get('cwsn'):null,
            'SCHSHI_YN' => e(Input::get('shift')),
            'MEDINSTR1' => e(Input::get('medium-a')),
            'MEDINSTR2' => e(Input::get('medium-b')),
            'MEDINSTR3' => e(Input::get('medium-c')),
            'MEDINSTR4' => e(Input::get('medium-d')),
            'RELMINORITY_YN' => e(Input::get('religious-minority')),
            'RELMINORITY_TYPE' => (e(Input::get('religious-minority'))==1)?e(Input::get('religious-minority-type')):null,
            'LATDEG' => Input::get('latitude-degree')?Input::get('latitude-degree'):null,
            'LATMIN' => Input::get('latitude-min')?Input::get('latitude-min'):null,
            'LATSEC' => Input::get('latitude-sec')?Input::get('latitude-sec'):null,
            'LONDEG' => Input::get('longitude-degree')?Input::get('longitude-degree'):null,
            'LONMIN' => Input::get('longitude-min')?Input::get('longitude-min'):null,
            'LONSEC' => Input::get('longitude-sec')?Input::get('longitude-sec'):null,
        );

        if (Master::isSecondary($lowClass, $highClass) || Master::isHigherSecondary($highClass)) {
            $data += array(
                'PREVOCCOURSE_YN' => Input::get('pre-vocational-course'),
                'EDUVOCGUIDE_YN' => Input::get('councelling'),
            );
        }
        //RURAL URBAN
        if (Input::get('rural-urban') == 1) {
            $data += array('PANCD' => e(Input::get('panchayath')));
        } else {
            $data += [
                'PANCD' => null,
                ];
        }
        if (Input::get('rural-urban') == 2) {
            $data += [
                'MUNICIPALCD' => e(Input::get('municipality')),
                'CITY' => e(Input::get('city')),
                ];
        } else {
            $data += [
                'MUNICIPALCD' => null,
                'CITY' => null,
                ];
        }
        //UPGRADATION
        if (Master::isPrimary($lowClass) && Master::isUpperPrimary($lowClass, $highClass)) {
            $data += [
                'YEARUPGRD' => Input::get('primary-up-upgradation')?Input::get('primary-up-upgradation'):null,
                ];
        } else {
            $data += [
                'YEARUPGRD' => null,
                ];
        }

        if (Master::isElementary($lowClass) && Master::isSecondary($lowClass, $highClass)) {
            $data += [
                'YEARUPGRDS' => Input::get('e-s-upgradation')?Input::get('e-s-upgradation'):null,
                ];
        } else {
            $data += [
                'YEARUPGRDS' => null,
                ];
        }
        if (Master::isSecondary($lowClass, $highClass) && Master::isHigherSecondary($highClass)) {
            $data += [
                'YEARUPGRDH' => Input::get('s-hs-upgradation')?Input::get('s-hs-upgradation'):null,
                ];
        } else {
            $data += [
                'YEARUPGRDH' => null,
                ];
        }
        //CHECK TYPE/YEAR OF RECOGNITION/UPGRADATION/MANAGEMENT etc.

        if (Master::isElementary($lowClass)) {
            $data += [
                'SCHTYPE' => e(Input::get('elementary-type')),
                'SCHMGT' => e(Input::get('elementary-management')),
                'YEARRECOG' => Input::get('elementary-recognition')?Input::get('elementary-recognition'):null,
                ];
        } else {
            $data += [
                'SCHTYPE' => null,
                'SCHMGT' => null,
                'YEARRECOG' => null,
                ];
        }
        if (Master::isSecondary($lowClass, $highClass)) {
            $data += [
                'SCHTYPES' => e(Input::get('secondary-type')),
                'SCHMGTS' => e(Input::get('secondary-management')),
                'YEARRECOGS' => Input::get('secondary-recognition')?Input::get('secondary-recognition'):null,
                ];
        }

        else {
            $data += [
                'SCHTYPES' => null,
                'SCHMGTS' => null,
                'YEARRECOGS' => null,
                ];
        }

        if (Master::isHigherSecondary($highClass)) {
            $data += [
                'SCHTYPEHS' => e(Input::get('hs-type')),
                'SCHMGTHS' => e(Input::get('hs-management')),
                'YEARRECOGH' => Input::get('hs-recognition')?Input::get('hs-recognition'):null,
                ];
        } else {
            $data += [
                'SCHTYPEHS' => null,
                'SCHMGTHS' => null,
                'YEARRECOGH' => null,
                ];
        }
        //BOARD

        if (Master::isSecondary($lowClass, $highClass) || Master::isHigherSecondary($highClass)) {

            if (Master::isSecondary($lowClass, $highClass)) {
                $data += ['BOARDSEC' => e(Input::get('board-secondary'))];
             } else {
                $data += ['BOARDSEC' => null];
             }
             if (Master::isHigherSecondary($highClass)) {
                $data += ['BOARDHSEC' => e(Input::get('board-hs'))];
            } else {
                $data += ['BOARDHSEC' => null];
            }

        } else {
            $data += [
                'BOARDSEC' => null,
                'BOARDHSEC' => null,
                ];
        }

        //CHECK FOR RESIDENTIAL CLASS

        if (Master::isPrimary($lowClass)) {
            $data += [
                'SCHRES_YN' => e(Input::get('residential-primary')),
                'MTONGUE_YN' => e(Input::get('primary-taught-in-mothertongue')),
                'LANG1' => e(Input::get('lang-a')),
                'LANG2' => e(Input::get('lang-b')),
                'LANG3' => e(Input::get('lang-c')),
                ];
        }

        else {
            $data += ['SCHRES_YN' => 2];
        }
        if (Master::isUpperPrimary($lowClass, $highClass)) {
            $data += ['SCHRESUPR_YN' => e(Input::get('residential-up'))];
        } else {
            $data += ['SCHRESUPR_YN' => 2];
        }
        if (Master::isSecondary($lowClass, $highClass)) {
            $data += ['SCHRESSEC_YN' => e(Input::get('residential-secondary'))];
        } else {
            $data += ['SCHRESSEC_YN' => 2];
        }
        if (Master::isHigherSecondary($highClass)) {
            $data += ['SCHRESHSEC_YN' => e(Input::get('residential-hs'))];
        } else {
            $data += ['SCHRESHSEC_YN' => null];
        }
        //CHECK FOR RESIDENTIAL TYPE
        if (Input::get('residential-primary') == 1) {
            $data += ['RESITYPE' => e(Input::get('residential-type'))];
        } else {
            $data += ['RESITYPE' => null];
        }
        if (Input::get('residential-up') == 1) {
            $data += ['RESITYPE' => e(Input::get('residential-type'))];
        } else {
            $data += ['RESITYPE' => null];
        }
        if (Input::get('residential-secondary') == 1) {
            $data += ['RESITYPE' => e(Input::get('residential-type'))];
        } else {
            $data += ['RESITYPE' => null];
        }
        if (Input::get('residential-hs') == 1) {
            $data += [ 'RESITYPE' => e(Input::get('residential-type'))];
        } else {
            $data += ['RESITYPE' => null];
        }
        if (Input::get('residential-primary') == 2 && Input::get('residential-up') == 2 && Input::get('residential-secondary') == 2 && Input::get('residential-hs') == 2) {
            $data += [
                'RESITYPE' => null,
                ];
        }

        $dataDetails = array(
            'SCHMGT' => $data['SCHMGT'],
            'SCHCAT' => $data['SCHCAT'],
            'SCHTYPE' => $data['SCHTYPE'],
            'RURURB' => $data['RURURB'],
            'CLUCD' => $data['CLUCD'],
            'VILCD' => $data['VILCD'],
            'PANCD' => $data['PANCD'],
            'SCHTYPES' => $data['SCHTYPES'],
            'SCHTYPEHS' => $data['SCHTYPEHS'],
            'SCHMGTS' => $data['SCHMGTS'],
            'SCHMGTHS' => $data['SCHMGTHS'],
        );

        SchoolDetails::updateRecord($dataDetails);
        if( ! $schoolMaster->year()->update($data)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }
        $schoolMaster->removeNonExistingClasses();
        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('particulars-one', $schoolCode)->with('success', 'Saved');
    }

    /**
     * school particulars form 2
     *
     * @return View
     */
    public function getParticularsTwo($schoolCode)
    {
        $user = Sentry::getUser();
        $schoolMaster =  Master::findRecord($schoolCode);

        if(App::make('master')->school->confirmation()) {
            return View::make('backend.confirmed.udise.particularsTwo')
            ->withMaster($schoolMaster);
        }
        return View::make('backend.udise.particularsTwo', compact('schoolMaster'));
    }

    /**
     * save school-particulars form 2
     *
     * @return Redirect
     */
    public function postParticularsTwo($schoolCode)
    {
        $user = Sentry::getUser();
        $school = School::find($schoolCode);
        $master = App::make('master');

        $rules = array(
            'academic-inspections' => 'required|integer|between:0,12',
            'visits-by-crc' => 'required|integer|between:0,25',
            'visits-by-brc' => 'required|integer|between:0,25',
            'part-time-teachers' => 'sometimes|required|integer|between:0, 200',
        );
        if($master->LOWCLASS == 1) {
            $rules += array(
                'pre-primary-section' => 'required',
                'anganwadi-section' => 'required',
            );
        }
        if(Input::get('pre-primary-section') == 1) {
            $rules += array(
                'pre-primary-students' => 'required|integer|min:1',
                'pre-primary-teachers' => 'required|integer|min:1',
            );
        }

        if(Input::get('anganwadi-section') == 1) {
            $rules += array(
                'anganwadi-students' => 'required|integer|min:1',
                'anganwadi-teachers' => 'required|integer|min:1',
            );
        }

        if($master->hasElementary()) {
            $rules += array(
                'school-grant-receipt' => 'required|integer|min:0',
                'school-grant-expenditure' => 'required|integer|min:0|max:'.(int)e(Input::get('school-grant-receipt')) ,
                'maintenance-grant-receipt' => 'sometimes|required|integer|min:0',
                'maintenance-grant-expenditure' => 'sometimes|required|integer|min:0|max:'.(int)e(Input::get('maintenance-grant-receipt')) ,
                'teachers-grant-receipt' => 'required|integer|min:0',
                'teachers-grant-expenditure' => 'required|integer|min:0|max:'.(int)e(Input::get('teachers-grant-receipt')) ,
            );
        }

        if($master->hasPrimary()) {
            $rules += [
                'primary-teacher-sanctioned' => 'required|integer|min:0',
                'primary-teacher-inpositioned' => 'required|integer|max:'.Input::get('primary-teacher-sanctioned', 0),
                'primary-contract-teachers' => 'required|integer|min:0',
                ];
        }
        if($master->hasUpperPrimary()) {
            $rules += [
                'up-teacher-sanctioned' => 'required|integer|min:0',
                'up-teacher-inpositioned' => 'required|integer|max:'.Input::get('up-teacher-sanctioned', 0),
                'up-contract-teachers' => 'required|integer|min:0',
                ];
        }

        if($master->hasSecondary()) {
            $rules += [
                'secondary-teacher-sanctioned' => 'required|integer|min:0',
                'secondary-teacher-inpositioned' => 'required|integer|max:'.Input::get('secondary-teacher-sanctioned', 0),
                'secondary-contract-teachers' => 'required|integer|min:0',
                ];
        }

        if($master->hasHigherSecondary()) {
            $rules += [
                'hs-teacher-sanctioned' => 'required|integer|min:0',
                'hs-teacher-inpositioned' => 'required|integer|min:0|max:'.Input::get('hs-teacher-sanctioned', 0),
                'hs-contract-teachers' => 'required|integer|min:0',
                ];
        }


        $user = Sentry::getUser();
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $school = School::find($schoolCode);
        $master =  Master::findRecord($schoolCode);
        $data =  array();

        if(Input::get('pre-primary-section', 0) == 1) {
            $data += array(
                'PPSEC_YN' => 1,
                'PPSTUDENT' =>(Input::get('pre-primary-students') != '')?Input::get('pre-primary-students'):null,
                'PPTEACHER' => (Input::get('pre-primary-teachers') != '')?Input::get('pre-primary-teachers'):null,
            );
        } else {
            $data += array(
                'PPSEC_YN' => 2,
                'PPSTUDENT' => null,
                'PPTEACHER' => null,
            );
        }
        if(Input::get('anganwadi-section', 0) == 1) {
            $data += array(
                'ANGANWADI_YN' => 1,
                'ANGANWADI_STU' => (Input::get('anganwadi-students') != '')?Input::get('anganwadi-students'):null,
                'ANGANWADI_TCH' => (Input::get('anganwadi-teachers') != '')?Input::get('anganwadi-teachers'):null,
            );
        } else {
            $data += array(
                'ANGANWADI_YN' => 2,
                'ANGANWADI_STU' => null,
                'ANGANWADI_TCH' => null,
            );
        }
        if($master->hasElementary()) {
            $data += array(
                'CONTI_R' => (Input::get('school-grant-receipt') != '')?Input::get('school-grant-receipt'):null,
                'CONTI_E' => (Input::get('school-grant-expenditure') != '')?Input::get('school-grant-expenditure'):null,
                'SCHMNTCGRANT_R' => (Input::get('maintenance-grant-receipt') != '')?Input::get('maintenance-grant-receipt'):null,
                'SCHMNTCGRANT_E' => (Input::get('maintenance-grant-expenditure') != '')?Input::get('maintenance-grant-expenditure'):null,
                'TLM_R' => (Input::get('teachers-grant-receipt') != '')?Input::get('teachers-grant-receipt'):null,
                'TLM_E' => (Input::get('teachers-grant-expenditure') != '')?Input::get('teachers-grant-expenditure'):null,
            );
        } else {
            $data += array(
                'CONTI_R' => null,
                'CONTI_E' => null,
                'SCHMNTCGRANT_R' => null,
                'SCHMNTCGRANT_E' => null,
                'TLM_R' => null,
                'TLM_E' => null,
            );
        }
        $inposition = 0;
        if($master->hasPrimary()) {
            $data += [
                'TCHSANCT_PR' => (Input::get('primary-teacher-sanctioned') != '')?Input::get('primary-teacher-sanctioned'):null,
                'TCHPOS' => (Input::get('primary-teacher-inpositioned') != '')?Input::get('primary-teacher-inpositioned'):null,
                'PARAPOS' => (Input::get('primary-contract-teachers') != '')?Input::get('primary-contract-teachers'):null,
                ];
            $inposition += Input::get('primary-teacher-inpositioned', 0);
        }
        else  {
            $data += [
                'TCHSANCT_PR' => null,
                'TCHPOS' => null,
                'PARAPOS' => null,
                ];
        }

        if($master->hasUpperPrimary()) {
            $data += [
                'TCHSAN' => (Input::get('up-teacher-sanctioned') != '')?Input::get('up-teacher-sanctioned'):null,
                'TCHREGU_UPR' => (Input::get('up-teacher-inpositioned') != '')?Input::get('up-teacher-inpositioned'):null,
                'TCHPARA_UPR' => (Input::get('up-contract-teachers') != '')?Input::get('up-contract-teachers'):null,
                    ];
            $inposition += Input::get('up-teacher-inpositioned', 0);
        }

        else  {
            $data += [

                'TCHSAN' => null,
                'TCHREGU_UPR' => null,
                'TCHPARA_UPR' => null,
                ];
        }
        if($master->hasSecondary()) {
            $data += [
                'TCHSANCT_SEC' => (Input::get('secondary-teacher-sanctioned') != '')?Input::get('secondary-teacher-sanctioned'):null,
                'TCHPOSN_SEC' => (Input::get('secondary-teacher-inpositioned') != '')?Input::get('secondary-teacher-inpositioned'):null,
                'PARAPOSN_SEC' => (Input::get('secondary-contract-teachers') != '')?Input::get('secondary-contract-teachers'):null,
                ];
            $inposition += Input::get('secondary-teacher-inpositioned', 0);
        }

        else  {
            $data += [

                'TCHSANCT_SEC' => null,
                'TCHPOSN_SEC' => null,
                'PARAPOSN_SEC' => null,
                ];
        }
        if($master->hasHigherSecondary()) {
            $data += [
                'TCHSANCT_HSEC' => (Input::get('hs-teacher-sanctioned') != '')?Input::get('hs-teacher-sanctioned'):null,
                'TCHPOSN_HSEC' => (Input::get('hs-teacher-inpositioned') != '')?Input::get('hs-teacher-inpositioned'):null,
                'PARAPOSN_HSEC' => (Input::get('hs-contract-teachers') != '')?Input::get('hs-contract-teachers'):null,
                ];
            $inposition += Input::get('hs-teacher-inpositioned', 0);
        }

        else  {
            $data += [
                'TCHSANCT_HSEC' => null,
                'TCHPOSN_HSEC' => null,
                'PARAPOSN_HSEC' => null,
                ];
        }

        $data += array(
            'NOINSPECT' => (Input::get('academic-inspections') != '')?Input::get('academic-inspections'):null,
            'VISITSCRC' => (Input::get('visits-by-crc') != '')?Input::get('visits-by-crc'):null,
            'VISITSBRC' => (Input::get('visits-by-brc') != '')?Input::get('visits-by-brc'):null,
            'TCHPART_UPR' => (Input::get('part-time-teachers') != '')?Input::get('part-time-teachers'):null,
        );


        if( ! $master->year()->update($data)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }

        $teacherCount = Teacher::where('SCHCD', '=', $school->SCHCD)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->count();
        $teacherCountWarning = '';
        if($inposition > $teacherCount) {
            $teacherCountWarning = ($inposition - $teacherCount)." teacher(s) need to be added.";
            SchoolWarning::addWarning($schoolCode, 'teachers', $teacherCountWarning);

        } elseif($inposition < $teacherCount) {
            $teacherCountWarning = ($teacherCount - $inposition)." teacher(s) need to be deleted.";
            SchoolWarning::addWarning($schoolCode, 'teachers', $teacherCountWarning);
        } else {
            SchoolWarning::deleteWarning($schoolCode, 'teachers');
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('particulars-two', $schoolCode)->with('warning', $teacherCountWarning)->with('success', 'Saved');
    }

    /**
     * school particulars form 3
     *
     * @return View
     */
    public function getParticularsThree($schoolCode)
    {
        $user = Sentry::getUser();
        $school = School::find($schoolCode);

        if( ! App::make('master')->hasElementary()) {
            // school don't have elementary section
            return Redirect::route('account')->with('error', 'Your are not permitted to access the page');
        }

        $rte =  Rte::findRecord($schoolCode);
        // Text Book info
        $materialBook =  Material::findRecord($schoolCode, 'book');
        // TLE info
        $materialTle =  Material::findRecord($schoolCode, 'tle');
        // Sports info
        $materialSports =  Material::findRecord($schoolCode, 'sports');

        $warnings = SchoolWarning::getWarningsByRoute($schoolCode, 'particulars-three');

        if(App::make('master')->school->confirmation()) {
            return View::make('backend.confirmed.udise.particularsThree',
                compact('rte','materialBook', 'materialTle', 'materialSports'));
        }
        return View::make('backend.udise.particularsThree',
            compact('rte','materialBook', 'materialTle', 'materialSports', 'warnings'));
    }

    /**
     * process school particulars form 3
     *
     * @return Redirect
     */
    public function postParticularsThree($schoolCode)
    {
        $user = Sentry::getUser();
        $master = App::make('master');
        $rules = array(
            'cce-implemented' => 'required',
            'children-enrolled-in-RTE-quota-current-year' => 'sometimes|required',
            'children-enrolled-in-RTE-quota-previous-year' => 'sometimes|required',
            'academic-session-start' => 'required',
            'smc-constituted' => 'required',
            'text-books-received' => 'required',
        );

        if (Input::get('cce-implemented') == 1) {
            $rules += array(
                'cce-record-maintained' => 'required',
            );
        }

        if (Input::get('cce-record-maintained') == 1) {
            $rules += array(
                'cce-record-shared' => 'required',
            );
        }
        if ($master->hasPrimary()) {
            $rules += array(
                'is-primary-books-received' => 'required',
                'tle-available-primary' => 'required',
                'play-material-available-primary' => 'required',
                'primary-instructional-days' => 'required|integer|min:100|max:250',
                'primary-school-hours' => 'required|numeric|min:5|max:8',
                'primary-teacher-working-hours' => 'required|numeric|min:'.e(Input::get('primary-school-hours')).'|max:8',
            );
        }

        if ($master->hasUpperPrimary()) {
            $rules += array(
                'is-up-books-received' => 'required',
                'tle-available-up' => 'required',
                'play-material-available-up' => 'required',
                'up-instructional-days' => 'required|integer|min:100|max:250',
                'up-school-hours' => 'required|numeric|min:5|max:8',
                'up-teacher-working-hours' => 'required|numeric|min:'.e(Input::get('primary-school-hours')).'|max:8',
            );
        }

        if (Input::get('smc-constituted') == 1) {

            $rules += [
                'smc-bank-account' => 'required',
                'no-of-smc-members-male' => 'required|integer|min:0',
                'no-of-smc-members-female' => 'required|integer|min:0',
                'no-of-pta-members-male' => 'required|integer|min:0',
                'no-of-pta-members-female' => 'required|integer|min:0',
                'no-of-local-authority-male' => 'required|integer|min:0',
                'no-of-local-authority-female' => 'required|integer|min:0',
                'smc-meetings' => 'required|integer|min:0',
                'smc-plan' => 'required',
                ];

            if (Input::get('smc-bank-account') == 1) {
                $rules += array(
                    'smc-bank-name' => 'required',
                    'smc-branch-name' => 'required',
                    'smc-acc-no' => 'required|digits_between:10,15',
                    'smc-acc-name' => 'required',
                    'smc-ifsc-code' => 'required|digits:11',
                );
            }

        }
        //SPECIAL TRAINING
        if($master->isGov()) {
            $rules += ['special-training' => 'required'];
            $rules['smc-constituted'] = 'required|regex:/1/';
            $rules['cce-implemented'] = 'required|regex:/1/';
        }
        if (Input::get('special-training', 0) == 1) {
            $rules += [
                'no-boys-in-special-training' => 'required|integer|min:0',
                'no-girls-in-special-training' => 'required|integer|min:0',
                'no-boys-in-special-training-previous-year' => 'required|integer|min:0',
                'no-girls-in-special-training-previous-year' => 'required|integer|min:0',
                'no-boys-completed-special-training-in-previous-year' => 'required|integer|min:0',
                'no-girls-completed-special-training-in-previous-year' => 'required|integer|min:0',
                'special-training-conducted-by' => 'required',
                'special-training-conducted-in' => 'required',
                'type-of-special-training' => 'required',
                ];

        }

        if (Input::get('text-books-received', 0) == 1) {

            $rules += [
                'books-received-month' => 'required',
                'books-received-year' => 'required|in:'.(int)substr(AcademicYear::CURRENT_YEAR, 0, 4)
                ];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $school = School::find($schoolCode);
        $master =  Master::findRecord($schoolCode);

        // collecting data to save in RTE table
        $data = array(
            'WSEC25P_APPLIED' => Input::get('children-enrolled-in-RTE-quota-current-year')?Input::get('children-enrolled-in-RTE-quota-current-year'):null,
            'WSEC25P_ENROLLED' => Input::get('children-enrolled-in-RTE-quota-previous-year')?Input::get('children-enrolled-in-RTE-quota-previous-year'):null,

            'ACSTARTMNTH' => e(Input::get('academic-session-start')),
            'SMC_YN' => Input::get('smc-constituted'),
        );
        //CCE IMPLEMENTATION
        if (Input::get('cce-implemented') == 1) {
            $data += array(
                'CCE_YN' => 1,
            );

            if (Input::get('cce-record-maintained') == 1) {
                $data += array(
                    'PCR_MAINTAINED' => 1,
                    'PCR_SHARED' => Input::get('cce-record-shared'),
                );
            }

            else {
                $data += array(
                    'PCR_MAINTAINED' => 2,
                );
            }

        }

        else {
            $data += array(
                'CCE_YN' => 2,
            );
        }

        //INSTRUCTIONAL DAYS, SCHOOL HOURS,WORKING HOURS
        if ($master->hasPrimary()) {
            $data += array(
                'WORKDAYS_PR' => e(Input::get('primary-instructional-days')),
                'SCHHRSCHILD_PR' => Input::get('primary-school-hours')?Input::get('primary-school-hours'):null,
                'SCHHRSTCH_PR' => Input::get('primary-teacher-working-hours')?Input::get('primary-teacher-working-hours'):null,
            );
        }

        else {
            $data += array(
                'WORKDAYS_PR' => null,
                'SCHHRSCHILD_PR' => null,
                'SCHHRSTCH_PR' => null,
            );
        }
        if ($master->hasUpperPrimary()) {
            $data += array(
                'WORKDAYS_UPR' => e(Input::get('up-instructional-days')),
                'SCHHRSCHILD_UPR' => Input::get('up-school-hours')?Input::get('up-school-hours'):null,
                'SCHHRSTCH_UPR' => Input::get('up-teacher-working-hours')?Input::get('up-teacher-working-hours'):null,
            );
        }

        else {
            $data += array(
                'WORKDAYS_UPR' => null,
                'SCHHRSCHILD_UPR' => null,
                'SCHHRSTCH_UPR' => null,
            );
        }
        if(Input::get('smc-constituted') == 1) {
            $data += [
                'SMCMEM_M' => Input::get('no-of-smc-members-male'),
                'SMCMEM_F' => Input::get('no-of-smc-members-female'),
                'SMSPARENTS_M' => Input::get('no-of-pta-members-male'),
                'SMSPARENTS_F' => Input::get('no-of-pta-members-female'),
                'SMCNOMLOCAL_M' => Input::get('no-of-local-authority-male'),
                'SMCNOMLOCAL_F' => Input::get('no-of-local-authority-female'),
                'SMCMEETINGS' => Input::get('smc-meetings'),
                'SMCSDP_YN' => Input::get('smc-plan')?Input::get('smc-plan'):null,
                ];
                if (Input::get('smc-bank-account', 0) == 1) {
                    $data += [
                        'SMCBANKAC_YN' => Input::get('smc-bank-account')?Input::get('smc-bank-account'):null,
                        'SMCBANK' =>e(Input::get('smc-bank-name')),
                        'SMCBANKBRANCH' =>e(Input::get('smc-branch-name')),
                        'SMCACNO' => e(Input::get('smc-acc-no')),
                        'SMCACNAME' => e(Input::get('smc-acc-name')),
                        'IFSCCODE' =>e(Input::get('smc-ifsc-code')),
                        ];
                }
                else {
                    $data += [
                        'SMCBANKAC_YN' => 2,
                        'SMCBANK' => null,
                        'SMCBANKBRANCH' => null,
                        'SMCACNO' => null,
                        'SMCACNAME' => null,
                        'IFSCCODE' => null,
                        ];
                }
        }

        else  {
            $data += [
                'SMCMEM_M' => null,
                'SMCMEM_F' => null,
                'SMSPARENTS_M' => null,
                'SMSPARENTS_F' => null,
                'SMCNOMLOCAL_M' => null,
                'SMCNOMLOCAL_F' => null,
                'SMCMEETINGS' => null,
                'SMCSDP_YN' => null,
                'SMCBANKAC_YN' => 2,
                'SMCBANK' => null,
                'SMCBANKBRANCH' => null,
                'SMCACNO' => null,
                'SMCACNAME' => null,
                'IFSCCODE' => null,
                ];
        }
        //SPECIAL TRAINING
        if($master->isGov()) {
            $data += [
                'SPLTRNG_YN' => Input::get('special-training'),
                    ];
        }

        else {
            $data += [
                'SPLTRNG_YN' => null,
                ];
        }
        if(Input::get('special-training') == 1) {
            $data += [

                'SPLTRG_CY_PROVIDED_B' => Input::get('no-boys-in-special-training')?Input::get('no-boys-in-special-training'):null,
                    'SPLTRG_CY_PROVIDED_G' => Input::get('no-girls-in-special-training')?Input::get('no-girls-in-special-training'):null,
                    'SPLTRG_PY_ENROLLED_B' => Input::get('no-boys-in-special-training-previous-year')?Input::get('no-boys-in-special-training-previous-year'):null,
                    'SPLTRG_PY_ENROLLED_G' => Input::get('no-girls-in-special-training-previous-year')?Input::get('no-girls-in-special-training-previous-year'):null,
                    'SPLTRG_PY_PROVIDED_B' => Input::get('no-boys-completed-special-training-in-previous-year')?Input::get('no-boys-completed-special-training-in-previous-year'):null,
                    'SPLTRG_PY_PROVIDED_G' => Input::get('no-girls-completed-special-training-in-previous-year')?Input::get('no-girls-completed-special-training-in-previous-year'):null,
                    'SPLTRG_BY' => e(Input::get('special-training-conducted-by')),
                    'SPLTRG_PLACE' => e(Input::get('special-training-conducted-in')),
                    'SPLTRG_TYPE' => e(Input::get('type-of-special-training')),

                    ];
        }

        else  {
            $data += [

                'SPLTRG_CY_PROVIDED_B' => null,
                'SPLTRG_CY_PROVIDED_G' => null,
                'SPLTRG_PY_ENROLLED_B' => null,
                'SPLTRG_PY_ENROLLED_G' => null,
                'SPLTRG_PY_PROVIDED_B' => null,
                'SPLTRG_PY_PROVIDED_G' => null,
                'SPLTRG_BY' => null,
                'SPLTRG_PLACE' => null,
                'SPLTRG_TYPE' => null,
                ];
        }

        if(Input::get('text-books-received') ==1) {

            $data += array(
                'TXTBKRECD_YN' => Input::get('text-books-received'),
                'TXTBKMNTH' => e(Input::get('books-received-month')),
                'TXTBKYEAR' => e(Input::get('books-received-year')),
            );
            if ($master->isGov()  || $master->isGovAided()) {
                SchoolWarning::deleteWarning($schoolCode, 'textbook-recieved-no');
            }
        } else  {

            if ($master->isGov()  || $master->isGovAided()) {
                SchoolWarning::addWarning($schoolCode, 'textbook-recieved-no', 'text-book not recieved ?');
            }
            $data += [
                'TXTBKRECD_YN' => 2,
                'TXTBKMNTH' => null,
                'TXTBKYEAR' => null,
                ];
        }

        $dataBook = array(
            'C1' => e(Input::get('is-primary-books-received')),
            'C2' => e(Input::get('is-up-books-received')),
        );

        $dataTle = array(
            'C1' => e(Input::get('tle-available-primary')),
            'C2' => e(Input::get('tle-available-up')),
        );

        $dataSports = array(
            'C1' => e(Input::get('play-material-available-primary')),
            'C2' => e(Input::get('play-material-available-up')),
        );

        Material::item('book', $school->SCHCD)->update($dataBook);
        Material::item('tle', $school->SCHCD)->update($dataTle);
        Material::item('sports', $school->SCHCD)->update($dataSports);

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        $rte =  Rte::findRecord($schoolCode);
        $rte->year()->update($data);
        return Redirect::route('particulars-three', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Smdc form
     *
     * @return View
     */
    public function getSmdc($schoolCode)
    {
        $master = App::make('master');

        if( ! ($master->hasSecondary() || $master->hasHigherSecondary())) {
            // school don't have secondary or hs section
            return Redirect::route('account')->with('error', 'not secondary or higher-secondary');
        }
        $smdc =  Smdc::findRecord($schoolCode);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.udise.smdc', compact('smdc', 'material'));
        }
        return View::make('backend.udise.smdc', compact('smdc', 'material'));
    }

    /**
     * Save Smdc details
     * @param int $id
     * @return Redirect
     */
    public function postSmdc($schoolCode)
    {
        $master =  App::make('master');
        $rules = array(
            'smdc-constituted' => 'required',
            'ac-constituted' => 'required',
            'pta-constituted' => 'required',

        );
        if($master->hasSecondary()) {
            $rules += array(
                'secondary-instructional-days' => 'integer|min:100|max:250',
                'secondary-school-hours' => 'numeric|min:5|max:8',
                'secondary-teacher-working-hours' => 'numeric|min:5|max:8',
            );
        }

        if($master->hasHigherSecondary()) {
            $rules += array(
                'hs-instructional-days' => 'integer|min:100|max:250',
                'hs-school-hours' => 'numeric|min:5|max:8',
                'hs-teacher-working-hours' => 'numeric|min:5|max:8',
            );
        }
        //CCE
        if($master->hasSecondary()) {
            $rules += array(
                'cce-implemented-secondary' => 'required',
            );
        }

        if($master->hasHigherSecondary()) {
            $rules += array(
                'cce-implemented-hs' => 'required',
            );
        }
        //SMDC

        if(Input::get('smdc-constituted') == 2 && Input::get('only-smdc-constituted') == 1) {

            $rules += [

                'only-smdc-constituted' => 'integer|min:0',
                'no-of-smdc-members-male' => 'integer|min:0',
                'no-of-smdc-members-female' => 'integer|min:0',
                'no-of-pta-members-male' => 'integer|min:0',
                'no-of-pta-members-female' => 'integer|min:0',
                'no-of-local-authority-male' => 'integer|min:0',
                'no-of-local-authority-female' => 'integer|min:0',
                'no-of-minority-male' => 'integer|min:0',
                'no-of-minority-female' => 'integer|min:0',
                'no-of-women-group' => 'integer|min:0',
                'no-of-sc-or-st-male' => 'integer|min:0',
                'no-of-sc-or-st-female' => 'integer|min:0',
                'no-of-deo-nominee-male' => 'integer|min:0',
                'no-of-deo-nominee-female' => 'integer|min:0',
                'no-of-nominee-audit-dept-male' => 'integer|min:0',
                'no-of-nominee-audit-dept-female' => 'integer|min:0',
                'no-of-rmsa-nominee-male' => 'integer|min:0',
                'no-of-rmsa-nominee-female' => 'integer|min:0',
                'no-of-teachers-male' => 'integer|min:0',
                'no-of-teachers-female' => 'integer|min:0',
                'no-of-vice-principal-male' => 'integer|min:0',
                'no-of-vice-principal-female' => 'integer|min:0',
                'no-of-principal-male' => 'integer|min:0',
                'no-of-principal-female' => 'integer|min:0',
                'no-of-chairpersons-male' => 'integer|min:0',
                'no-of-chairpersons-female' => 'integer|min:0',
                'sbc-constituted' => 'required|min:0',
                'smdc-meetings' => 'integer|min:0',
                'smdc-plan' => 'integer|min:0',
                'smdc-bank-account' => 'required|digits_between:10,15',
                ];
            if(Input::get('smdc-bank-account') == 1) {
                $rules += [
                    'smdc-bank-name' => 'required',
                    'smdc-branch-name' => 'required',
                    'smdc-acc-no' => 'required|digits_between:10,15',
                    'smdc-acc-name' => 'required',
                    'smdc-ifsc-code' => 'required|digits:11',

                ];

            }
        }
        if(Input::get('pta-constituted') == 1) {

            $rules += [
                'pta-meetings' => 'integer',
                ];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        // collecting data to save in SMDC table
        $data = array(
            'SMDC_YN' => e(Input::get('smdc-constituted')),
            'SBC_YN' => e(Input::get('sbc-constituted')),
            'AC_YN' => e(Input::get('ac-constituted')),
            'PTA_YN' => e(Input::get('pta-constituted')),
            'SMCSMDC1_YN' => e(Input::get('only-smdc-constituted')),
            'BANKAC_YN' => e(Input::get('smdc-bank-account')),
        );

        if($master->hasSecondary()) {
            $data += array(
                'WORKDAYS_SEC' => Input::get('secondary-instructional-days')?Input::get('secondary-instructional-days'):null,
                'SCHHRSCHILD_SEC' => Input::get('secondary-school-hours')?Input::get('secondary-school-hours'):null,
                'SCHHRSTCH_SEC' => Input::get('secondary-teacher-working-hours')?Input::get('secondary-teacher-working-hours'):null,
            );
        }

        else {
            $data += array(
                'WORKDAYS_SEC' => null,
                'SCHHRSCHILD_SEC' => null,
                'SCHHRSTCH_SEC' => null,
            );
        }
        if($master->hasHigherSecondary()) {
            $data += array(
                'WORKDAYS_HSEC' => Input::get('hs-instructional-days')?Input::get('hs-instructional-days'):null,
                'SCHHRSCHILD_HSEC' => Input::get('hs-school-hours')?Input::get('hs-school-hours'):null,
                'SCHHRSTCH_HSEC' => Input::get('hs-teacher-working-hours')?Input::get('hs-teacher-working-hours'):null,
            );
        }
        else {
            $data += array(
                'WORKDAYS_HSEC' => null,
                'SCHHRSCHILD_HSEC' => null,
                'SCHHRSTCH_HSEC' => null,
            );
        }
        //CCE

        if($master->hasSecondary()) {
            $data += array(
                'CCESEC_YN' => e(Input::get('cce-implemented-secondary')),
            );
        }
        else {
            $data += array(
                'CCESEC_YN' => null,
            );
        }
        if($master->hasHigherSecondary()) {
            $data += array(
                'CCEHSEC_YN' => e(Input::get('cce-implemented-hs')),
            );
        }
        else {
            $data += array(
                'CCEHSEC_YN' => null,
            );
        }
        //SMDC
        if(Input::get('only-smdc-constituted') == 1) {

            $data += [

                'TOT_M' => Input::get('no-of-smdc-members-male')?Input::get('no-of-smdc-members-male'):null,
                'TOT_F' => Input::get('no-of-smdc-members-female')?Input::get('no-of-smdc-members-female'):null,
                'PARENTS_M' => Input::get('no-of-pta-members-male')?Input::get('no-of-pta-members-male'):null,
                'PARENTS_F' => Input::get('no-of-pta-members-female')?Input::get('no-of-pta-members-female'):null,
                'LOCAL_M' => Input::get('no-of-local-authority-male')?Input::get('no-of-local-authority-male'):null,
                'LOCAL_F' => Input::get('no-of-local-authority-female')?Input::get('no-of-local-authority-female'):null,
                'EBMC_M' => Input::get('no-of-minority-male')?Input::get('no-of-minority-male'):null,
                'EBMC_F' => Input::get('no-of-minority-female')?Input::get('no-of-minority-female'):null,
                'WOMEN_M' => Input::get('no-of-women-group')?Input::get('no-of-women-group'):null,
                'SCST_M' => Input::get('no-of-sc-or-st-male')?Input::get('no-of-sc-or-st-male'):null,
                'SCST_F' => Input::get('no-of-sc-or-st-female')?Input::get('no-of-sc-or-st-female'):null,
                'DEO_M' => Input::get('no-of-deo-nominee-male')?Input::get('no-of-deo-nominee-male'):null,
                'DEO_F' => Input::get('no-of-deo-nominee-female')?Input::get('no-of-deo-nominee-female'):null,
                'AAD_M' => Input::get('no-of-nominee-audit-dept-male')?Input::get('no-of-nominee-audit-dept-male'):null,
                'AAD_F' => Input::get('no-of-nominee-audit-dept-female')?Input::get('no-of-nominee-audit-dept-female'):null,
                'SUBEXP_M' => Input::get('no-of-rmsa-nominee-male')?Input::get('no-of-rmsa-nominee-male'):null,
                'SUBEXP_F' => Input::get('no-of-rmsa-nominee-female')?Input::get('no-of-rmsa-nominee-female'):null,
                'TCH_M' => Input::get('no-of-teachers-male')?Input::get('no-of-teachers-male'):null,
                'TCH_F' => Input::get('no-of-teachers-female')?Input::get('no-of-teachers-female'):null,
                'AHM_M' => Input::get('no-of-vice-principal-male')?Input::get('no-of-vice-principal-male'):null,
                'AHM_F' => Input::get('no-of-vice-principal-female')?Input::get('no-of-vice-principal-female'):null,
                'HM_M' => Input::get('no-of-principal-male')?Input::get('no-of-principal-male'):null,
                'HM_F' => Input::get('no-of-principal-female')?Input::get('no-of-principal-female'):null,
                'CP_M' => Input::get('no-of-chairpersons-male')?Input::get('no-of-chairpersons-male'):null,
                'CP_F' => Input::get('no-of-chairpersons-female')?Input::get('no-of-chairpersons-female'):null,
                'SMDCMEETING' => e(Input::get('smdc-meetings')),
                'SIP_YN' => e(Input::get('smdc-plan')),
                ];
        }
        else {
            $data += [
                'TOT_M' => null,
                'TOT_F' => null,
                'PARENTS_M' => null,
                'PARENTS_F' => null,
                'LOCAL_M' => null,
                'LOCAL_F' => null,
                'EBMC_M' => null,
                'EBMC_F' => null,
                'WOMEN_M' => null,
                'SCST_M' => null,
                'SCST_F' => null,
                'DEO_M' => null,
                'DEO_F' => null,
                'AAD_M' => null,
                'AAD_F' => null,
                'SUBEXP_M' => null,
                'SUBEXP_F' => null,
                'TCH_M' => null,
                'TCH_F' => null,
                'AHM_M' => null,
                'AHM_F' => null,
                'HM_M' => null,
                'HM_F' => null,
                'CP_M' => null,
                'CP_F' => null,
                'SMDCMEETING' => null,
                'SIP_YN' => null,
            ];
        }

        if(Input::get('smdc-bank-account') == 1) {

            $data += [
                'BANKNAME' => e(Input::get('smdc-bank-name')),
                'BANKBRANCH' => e(Input::get('smdc-branch-name')),
                'BANKACNO' => e(Input::get('smdc-acc-no')),
                'ACNAME' => e(Input::get('smdc-acc-name')),
                'IFSC' => e(Input::get('smdc-ifsc-code')),
                ];
        }

        else {
            $data += [
                'BANKNAME' => null,
                'BANKBRANCH' => null,
                'BANKACNO' => null,
                'ACNAME' => null,
                'IFSC' => null,
            ];
        }

        if(Input::get('pta-constituted') == 1) {

            $data += [
                'PTAMEETING' => e(Input::get('pta-meetings')),
            ];
        } else {
            $data += [
                'PTAMEETING' => null,

            ];
        }

        if( ! Smdc::updateRecord($data, $schoolCode)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('particulars-four', $schoolCode)->with('success', 'Saved');
    }

    /**
     * form for Physical Facilities and Equipment
     *
     * @return View
     */
    public function getFacilitiesOne($schoolCode)
    {
        $user = Sentry::getUser();
        $master = App::make('master');
        $sections = array();

        if($master->LOWCLASS < 9) {
            $sections[] = 0;
        }
        if($master->hasSecondary()) {
            $sections[] = 1;
        }
        if($master->hasHigherSecondary()) {
            $sections[] = 2;
        }
        $building =  Building::findRecord($schoolCode);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.udise.facilitiesOne', compact('building', 'sections'));
        }
        return View::make('backend.udise.facilitiesOne', compact('building', 'sections'));
    }

    /**
     * form for Physical Facilities and Equipment
     *
     * @return View
     */
    public function postFacilitiesOne($schoolCode)
    {
        $master = App::make('master');
        $rules = array(
            'building-status' => 'required',
            'class-1-8' => 'integer|min:1',
            'class-construction-1-8' => 'integer|min:0',
            'furniture-1-8' => 'integer|min:0',
            'other-rooms' => 'integer|min:0',
            'land-available' => 'required',
            'hm-room' => 'required',
            'boys-toilet-constructed' => 'integer|min:0',
            'girls-toilet-constructed' => 'integer|min:0',
            'boys-functional-toilets' => 'integer|min:0|max:'.e(Input::get('boys-toilet-constructed', 0)),
            'girls-functional-toilets' => 'integer|min:0|max:'.e(Input::get('girls-toilet-constructed', 0)),
            'boys-toilets-water' => 'integer|min:0|max:'.e(Input::get('boys-functional-toilets', 0)),
            'girls-toilets-water' => 'integer|min:0|max:'.e(Input::get('girls-functional-toilets', 0)),
            'boys-urinal' => 'integer|min:0',
            'girls-urinal' => 'integer|min:0',
            'hand-wash' => 'required',
            'cwsn-friendly-toilets' => 'required',
            'drinking-water-source' => 'required',
            'is-water-facility-functional' => 'required',
            'electricity-status' => 'required',
            'boundary-wall' => 'required',
            'library' => 'required',
            'subscription' => 'required',
            'playground' => 'required',
            'computer' => 'required|integer|min:0',
            'computer-functional' => 'required|integer|min:0',
            'cal-lab' => 'required',
            'medical-checkup' => 'required',
            'ramp-needed' => 'required',
            'campus-plan' => 'required',
        );
        if ($master->hasSecondary() || $master->hasHigherSecondary()) {
            $rules += array(
                'boys-hostel' => 'required',
                'girls-hostel' => 'required'
            );
            $rules['residing-boys'] = 'integer|min:0|required_if:boys-hostel,1';
            $rules['residing-girls'] = 'integer|min:0|required_if:girls-hostel,1';
        }
        //RAMP
        if (Input::get('ramp-needed', 0) == 1) {
            $rules += array(
               'ramp-available' => 'required',
           );

            if (Input::get('ramp-available', 0) == 1) {
                $rules += array(
                    'hand-rails' => 'required',
                );
            }
        }
        if (Input::get('medical-checkup') == 1) {
            $rules['medical-checkup-frequency'] = 'required';
        }

        //LIBRARY
        if (Input::get('library', 0) == 1) {
            $rules += array(
                'library-books' => 'required|integer|min:0',
                'librarian' => 'required',
            );

        }

        //PLAY GROUND
        if (Input::get('playground', 0) == 2) {
            $rules += array(
                'playground-land' => 'required',
            );

        }

        $sections = array();
        $classes = array(); // elementary section is handled seperately, so this is only for secondary and hs

        if($master->LOWCLASS < 9) {
            $sections[] = 0;
        }
        if ($master->hasSecondary()) {
            $sections[] = 1;
            $classes += array(9=>9, 10=>10);
        }
        if ($master->hasHigherSecondary()) {

            $sections[] = 2;
            $classes += array(11=>11, 12=>12);
            $hsClasses = Input::get('class-11', 0) + Input::get('class-12', 0);
            if ( ! $hsClasses) {
                $rules['hs-classes'] = 'required';
            }
        }
        foreach($classes as $class) {
            $rules += [
                "class-$class" => 'integer|min:0',
                "furniture-$class" => 'integer|min:0',
                "class-construction-$class" => 'integer',
                ];
        }
        foreach($sections as $section) {
            foreach(Building::$BUILDING_TYPE[$section] as $type => $suffix) {
                $rules += [
                    "good-$type" => 'integer|min:0',
                    "minor-repair-$type" => 'integer|min:0',
                    "major-repair-$type" => 'integer|min:0',
                ];
            }
            $rules += ["total-class-$section" => "regex:/".Input::get("total-state-$section", 0)."/"];
        }

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            $totalError = '';
            foreach($sections as $section) {
                if($errors->has("total-class-$section")) {
                    $totalError = 'make sure that, the classrooms used for instruction is matches with classrooms by condition ';
                    break;
                }
            }
            return Redirect::back()->withInput()->with('error', $totalError)->withErrors($errors);
        }

        // collecting data
        $data = array(
            'BLDSTATUS' => e(Input::get('building-status')),
            'CLROOMS' => (Input::get('class-1-8')!='')?Input::get('class-1-8'):null,
            'CLSUNDERCONST' => (Input::get('class-construction-1-8')!='')?Input::get('class-construction-1-8'):null,
            'FURNSTU' =>e(Input::get('furniture-1-8')),
            'OTHROOMS' => Input::get('other-rooms')?Input::get('other-rooms'):null,
            'LAND4CLS_YN' => e(Input::get('land-available')),
            'HMROOM_YN' => e(Input::get('hm-room')),
            'TOILETB' => Input::get('boys-toilet-constructed')?Input::get('boys-toilet-constructed'):null,
            'TOILET_G' => Input::get('girls-toilet-constructed')?Input::get('girls-toilet-constructed'):null,
            'TOILETB_FUNC' => Input::get('boys-functional-toilets')?Input::get('boys-functional-toilets'):null,
            'TOILETG_FUNC' => Input::get('girls-functional-toilets')?Input::get('girls-functional-toilets'):null,
            'TOILETWATER_B' => Input::get('boys-toilets-water')?Input::get('boys-toilets-water'):null,
            'TOILETWATER_G' => Input::get('girls-toilets-water')?Input::get('girls-toilets-water'):null,
            'URINALS_B' => Input::get('boys-urinal')?Input::get('boys-urinal'):null,
            'URINALS_G' => Input::get('girls-urinal')?Input::get('girls-urinal'):null,
            'HANDWASH_YN' => e(Input::get('hand-wash')),
            'TOILETD' => e(Input::get('cwsn-friendly-toilets')),
            'WATER' => e(Input::get('drinking-water-source')),
            'WATER_FUNC_YN' => e(Input::get('is-water-facility-functional')),
            'ELECTRIC_YN' => e(Input::get('electricity-status')),
            'BNDRYWALL' => e(Input::get('boundary-wall')),
            'NEWSPAPER_YN' => e(Input::get('subscription')),
            'COMPUTER' => Input::get('computer'),
            'TOTCOMP_FUNC' => Input::get('computer-functional'),
            'CAL_YN' => e(Input::get('cal-lab')),
            'MEDCHK_YN' => e(Input::get('medical-checkup')),
            'BOX' => (Input::get('medical-checkup') == 1)?e(Input::get('medical-checkup-frequency')):null,
            'RAMPSNEEDED_YN' => e(Input::get('ramp-needed')),
            'RAMPS_YN' => e(Input::get('ramp-available')),
            'CAMPUSPLAN_YN' => e(Input::get('campus-plan')),
        );

        if (Input::get('ramp-needed') == 1) {
            if (Input::get('ramp-available') == 1) {
                $data += array(
                    'HANDRAILS' => e(Input::get('hand-rails')),
                );
            }
            else {
                $data += array(
                    'HANDRAILS'=> null,
                );
            }
        }
        //Library
        if (Input::get('library') == 1) {
            $data += array(
                'LIBRARY_YN' => e(Input::get('library')),
                'BOOKINLIB' => Input::get('library-books')?Input::get('library-books'):null,
                'LIBRARIAN_YN' => e(Input::get('librarian')),
            );
        } else {
            $data += array(
                'LIBRARY_YN' => 2,
                'BOOKINLIB' => null,
                'LIBRARIAN_YN' => null,
            );
        }
        //PLAY GROUND
        if (Input::get('playground') == 2) {
            $data += array(
                'PGROUND_YN' => 2,
                'LAND4PGROUND_YN' => e(Input::get('playground-land')),
            );
        } else {
            $data += array(
                'PGROUND_YN' => 1,
                'LAND4PGROUND_YN' => null,
            );
        }
        //BOYS & GIRLS HOSTEL
        if (Input::get('boys-hostel') == 1) {
            $data += array(
                'HOSTELB_YN' => e(Input::get('boys-hostel')),
                'HOSTELBOYS' => Input::get('residing-boys')?Input::get('residing-boys'):null,
            );

        } else {
            $data += array(
                'HOSTELB_YN' => 2,
                'HOSTELBOYS' => null,
            );
        }
        if (Input::get('girls-hostel', 0) == 1) {
            $data += array(
                'HOSTELG_YN' => e(Input::get('girls-hostel')),
                'HOSTELGIRLS' => Input::get('residing-girls')?Input::get('residing-girls'):null,
            );
        } else {
            $data += array(
                'HOSTELG_YN' => 2,
                'HOSTELGIRLS' => null,
            );
        }
        // geting data related to classes
        foreach($classes as $class) {
            $data += array(
                "TOTCLS$class" => (Input::get("class-$class")!='')?Input::get("class-$class"):null,
                "CLSUCONST$class" => (Input::get("class-construction-$class")!='')?Input::get("class-construction-$class"):null,
                "FURN_YN$class" => (Input::get("furniture-$class")!='')?Input::get("furniture-$class"):null,
            );
        }

        // geting data related to building
        foreach($sections as $section) {
            foreach(Building::$BUILDING_TYPE[$section] as $type => $suffix) {
                $data += array(
                    "CLGOOD$suffix" => e(Input::get("good-$type")),
                    "CLMINOR$suffix" => e(Input::get("minor-repair-$type")),
                    "CLMAJOR$suffix" => e(Input::get("major-repair-$type")),
                );
            }
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        if( ! Building::updateRecord($data, $schoolCode)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }
        return Redirect::route('facilities-one', $schoolCode)->with('success', 'Saved');
    }

    /**
     * form for second form of Physical Facilities
     *
     * @return View
     */
    public function getFacilitiesTwo($schoolCode)
    {
        $user = Sentry::getUser();
        $master = App::make('master');
        $school = $master->school;

        if ( ! ($master->hasSecondary() || $master->hasHigherSecondary())) {
            return Redirect::route('account')->with('error', 'not secondary or higher-secondary');
        }
        if ($master->hasSecondary()) {
            $sectionNames[] = 'secondary';
        }
        if ($master->hasHigherSecondary()) {
            $sectionNames[] = 'hs';
        }
        $regexp = implode ("|", $sectionNames);
        $group1 = FacilityDetail::where('group_id', '=', 1)->where('available_for', 'REGEXP', $regexp)->get();
        $group2 = FacilityDetail::where('group_id', '=', 2)->where('available_for', 'REGEXP', $regexp)->get();
        $group3 = FacilityDetail::where('group_id', '=', 3)->where('available_for', 'REGEXP', $regexp)->get();

        $groupValue1 = Facility::findRecords($school->SCHCD)
            ->where('ITEMIDGROUP', '=', 1)
            ->whereIn('ITEMID', $group1->lists('facility_id'))
            ->orderBy('ITEMID')->get();

        $groupValue2 = Facility::findRecords($school->SCHCD)
            ->where('ITEMIDGROUP', '=', 2)
            ->whereIn('ITEMID', $group2->lists('facility_id'))
            ->orderBy('ITEMID')->get();

        $groupValue3 = Facility::findRecords($school->SCHCD)
            ->where('ITEMIDGROUP', '=', 3)
            ->whereIn('ITEMID', $group3->lists('facility_id'))
            ->orderBy('ITEMID')->get();

        // make sure that the required rows are exists in facilities table
        for ($i=1; $i<4; $i++) {
            $group = 'group'.$i;
            $groupValue = 'groupValue'.$i;
            $facilities = $$group->lists('facility_id');
            $facilityValues = $$groupValue->lists('ITEMID');
            $missing_items = array_diff($facilities, $facilityValues);
            if ( ! empty($missing_items)) {
                foreach($missing_items as $item) {
                    $itemsToInsert[] = array(
                        'ITEMID' => $item,
                        'ITEMIDGROUP' => $i,
                        'SCHCD' => $master->SCHCD,
                        'AC_YEAR' => AcademicYear::CURRENT_YEAR
                    );
                }
            }
        }
        // inserting missing rows
        if ( ! empty($itemsToInsert)) {
            Facility::unguard();
            Facility::insert($itemsToInsert);
            Facility::reguard();
            $groupValue1 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 1)
                ->whereIn('ITEMID', $group1->lists('facility_id'))
                ->orderBy('ITEMID')->get();

            $groupValue2 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 2)
                ->whereIn('ITEMID', $group2->lists('facility_id'))
                ->orderBy('ITEMID')->get();

            $groupValue3 = Facility::findRecords($school->SCHCD)
                ->where('ITEMIDGROUP', '=', 3)
                ->whereIn('ITEMID', $group3->lists('facility_id'))
                ->orderBy('ITEMID')->get();
        }

        if($school->confirmation()) {
            $facilityChoice = Facility::$FACILITY_STATUS + $this->emptyValues;
            $facilityChoice[0] = '';

            return view::make('backend.confirmed.udise.facilitiesTwo', compact(
                'school','group1', 'group2', 'group3', 'groupValue1', 'groupValue2', 'groupValue3', 'facilityChoice'));
        }
        return View::make('backend.udise.facilitiesTwo', compact(
            'school', 'group1', 'group2', 'group3', 'groupValue1', 'groupValue2', 'groupValue3'));
    }

    /**
     * form for Physical Facilities and Equipment
     *
     * @return View
     */
    public function postFacilitiesTwo($schoolCode)
    {
        $rules = array();
        $master = App::make('master');

        if ($master->hasSecondary()) {
            $sectionNames[] = 'secondary';
        }
        if ($master->hasHigherSecondary()) {
            $sectionNames[] = 'hs';
        }
        $regexp = implode ("|", $sectionNames);
        $group1 = FacilityDetail::where('group_id', '=', 1)->where('available_for', 'REGEXP', $regexp)->get();
        $group2 = FacilityDetail::where('group_id', '=', 2)->where('available_for', 'REGEXP', $regexp)->get();
        $group3 = FacilityDetail::where('group_id', '=', 3)->where('available_for', 'REGEXP', $regexp)->get();

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }

        foreach($group1 as $facility) {
            Facility::updateFacility(
                [
                'ITEMVALUE' => e(Input::get("facility-$facility->id")),
                    ],
                    $facility->facility_id,
                    $facility->group_id,
                    $schoolCode);
        }
        foreach($group2 as $facility) {
            Facility::updateFacility(
                [
                'ITEMVALUE' => e(Input::get("facility-$facility->id")),
                    'ITEMVALUE1' => e(Input::get("facility-status-$facility->id"))
                    ],
                    $facility->facility_id,
                    $facility->group_id,
                    $schoolCode);
        }
        foreach($group3 as $facility) {
            Facility::updateFacility( [
                'ITEMVALUE' => e(Input::get("facility-$facility->id")),
                    ],
                    $facility->facility_id,
                    $facility->group_id,
                    $schoolCode);
        }

        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('facilities-two', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Mid day meal information collecting form
     *
     * @param int $id
     * @return View
     */
    public function getMdm($schoolCode)
    {
        $user = Sentry::getUser();
        $master = App::make('master');

        if( ! ($master->isGov() || $master->isGovAided())) {
            // not government or gov-aided
            return Redirect::route('account')->with('error', 'not permitted');
        }
        if( $master->isGov() ) {
            unset(Mdm::$MDM_STATUS[1]);
        }
        $mdm = Mdm::findRecord($schoolCode);

        if($master->school->confirmation()) {
            return View::make('backend.confirmed.udise.mdm', compact('mdm'));
        }
        return View::make('backend.udise.mdm', compact('mdm'));
    }

    /**
     * Save Mdm form
     *
     * @return Redirect
     */
    public function postMdm($schoolCode)
    {
        $user = Sentry::getUser();
        $master = App::make('master');
        $rules = array ();
        if ($master->isGov() ) {
            $rules += array(
            'mdm-status' => 'required|not_in:1',
            );
        }
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
        }


        //collecting data
        $data = array(
            'MEALSINSCH' => e(Input::get('mdm-status')),
            'KITSHED' => e(Input::get('kitchen-status')),
            'MDM_MAINTAINER' => e(Input::get('mdm-source')),
        );

        //saving
        if( ! Mdm::updateRecord($data, $schoolCode )) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }
        SchoolStatus::setStatus($schoolCode, Input::get('_route', ''));
        return Redirect::route('mdm', $schoolCode)->with('success', 'Saved');
    }

    /**
     * Confirm Data entry
     * @param $schoolCode
     * @return void
     */
    public function postConfirm($schoolCode)
    {

        $master = Master::findRecord($schoolCode);
        if ( ! empty(SchoolWarning::getWarningsByType($schoolCode, Warning::ERROR))) {
            return Redirect::route('udise-status', $schoolCode)->with('error', 'Please fix all errors listed');
        }

        // checking all routes have SAVED status
        $routes = $master->getUdiseRoutes();
        $savedRoutes = SchoolStatus::getStatusByRoutes($master->SCHCD, $routes, SchoolStatus::SAVED);
        if (count($routes) != count($savedRoutes) || $master->school->confirmation()) {
            return Redirect::route('udise-status', $schoolCode)->with('error', 'Please save all forms');
        }

        $errors = Enrolment::isValid($master);

        if ($errors !== false) {

            $errorMessages = new MessageBag;
            foreach ($errors['messages'] as $message) {
                $errorMessages->add($errors['type'], $message);
            }
            return Redirect::route('udise-status', $schoolCode)->withErrors($errorMessages, 'confirmation');
        }
        if($master->school->confirmation(true)) {
            return Redirect::route('udise-status', $schoolCode)->with('success', 'Confirmed successfully');
        }
        return Redirect::route('udise-status', $schoolCode)->with('error', 'unexpected error occurred');
    }

    /**
     * Reset Data entry
     * @param $schoolCode
     * @return void
     */
    public function postReset($schoolCode)
    {
        $master = App::make('master');
        $school = $master->school;
        $user = Sentry::getUser();
        if($user->scope != self::$SCOPE_SCHOOL && $user->hasAccessToSchool($school)) {
            $school->confirmation(0);
            return Redirect::back()->with('success', 'Reset done successfully');
        }
        return Redirect::back()->with('error', 'Access Denied');
    }

    public function getStatusOfForms($schoolCode)
    {
        $user = Sentry::getUser();
        $master = Master::findRecordOrFail($schoolCode, AcademicYear::CURRENT_YEAR,
            array('SCHCD', 'LOWCLASS', 'HIGHCLASS', 'SCHCAT', 'SCHTYPE', 'SCHTYPES', 'SCHTYPEHS',
            'SCHMGT', 'SCHMGTS', 'SCHMGTHS', 'PPSEC_YN'));

        $udiseRoutes = $master->getUdiseRoutes();
        $savedRoutes = SchoolStatus::getStatusByRoutes($master->SCHCD, $udiseRoutes, SchoolStatus::SAVED);
        $canConfirm = false;
        $errorList = SchoolWarning::getWarningsByType($schoolCode, Warning::ERROR);
        $errorsCount = count($errorList);
        if (count($udiseRoutes) == count($savedRoutes) && $errorsCount == 0 && (! $master->school->confirmation())) {
            $canConfirm = true;
        }

        $warnings = SchoolWarning::getWarningsByType($schoolCode, Warning::WARNING);
        $statusOfUdiseRoutes = SchoolStatus::getStatusByCategory($schoolCode, SchoolStatus::UDISE);
        return View::make('backend.udise.status', compact('statusOfUdiseRoutes', 'canConfirm', 'warnings', 'errorList'));
    }

}

