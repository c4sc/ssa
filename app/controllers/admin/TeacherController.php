<?php namespace Controllers\Admin;

use App;
use AuthorizedController;
use Sentry;
use Input;
use School;
use AcademicYear;
use Teacher;
use SchoolWarning;
use Master;
use SchoolStatus;
use Redirect;
use Validator;
use DB;
use View;
use Route;
use Illuminate\Routing\Controller;

class TeacherController extends AuthorizedController {

    public $rules = array(
        'adhar' => 'digits:12',
        'name' => 'required',
        'mobile' => 'required|digits:10',
        'email' => 'required|email',
        'gender' => 'required',
        'dob' => 'required',
        'social-category' => 'required',
        'teacher-type' => 'required',
        'appointment-nature' => 'required',
        'join-date' => 'required',
        'academic' => 'required',
        'professional' => 'required',
        'classes-taught' => 'required',
        'appointed-subject' => 'required',
        'subject1' => 'required',
        'subject2' => '',
        'days-on-assignments' => '',
        'maths-qualification' => 'required',
        'english-qualification' => 'required',
        'ss-qualification' => 'required',
        'current-school-joining-year' => 'required',
        'disablity-type' => '',
        'cwsn-training' => 'required',
        'ict-training' => 'required',
    );

    /**
     * Teachers list
     *
     * @param int $id
     * @return View
     */
    public function teachers($schoolCode)
    {
        $master =  Master::findRecord($schoolCode, AcademicYear::CURRENT_YEAR,
            array('SCHCD', 'LOWCLASS', 'HIGHCLASS', 'SCHMGT', 'SCHMGTS', 'SCHMGTHS',
            DB::raw('SUM(COALESCE(`TCHPOSN_HSEC`, 0) + COALESCE(`TCHPOSN_SEC`, 0) +
            COALESCE(`TCHREGU_UPR`, 0) + COALESCE(`TCHPOS`, 0)) as inposition')));

        $categoryTeachers = Teacher::select(DB::raw('count(*) as count, CATEGORY'))
            ->where('SCHCD', '=', $schoolCode)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->groupBy('CATEGORY')
            ->get();

        $teacherCount = 0;
        foreach($categoryTeachers as $category) {
            $teacherCount += $category->count;
        }

        if($master->inposition > $teacherCount) {
            $teacherCountWarning = ($master->inposition - $teacherCount)." teacher(s) need to be added. or you have to update in-position teacher count";
            SchoolWarning::addWarning($schoolCode, 'teachers', $teacherCountWarning);

        } elseif($master->inposition < $teacherCount) {
            $teacherCountWarning = ($teacherCount - $master->inposition)." teacher(s) need to be deleted. or you have to update in-position teacher count";
            SchoolWarning::addWarning($schoolCode, 'teachers', $teacherCountWarning);
        } else {
            SchoolWarning::deleteWarning($schoolCode, 'teachers');
        }

        // checking teachers taught-in field
        // geting not allowed taught-in
        $classesNotAllowed = array_diff(Teacher::$CLASSES_TAUGHT, Teacher::getAllowedClassesTaught($master));
        if ( ! empty($classesNotAllowed)) {

            $teachersWithInvalidClass = Teacher::where('schcd', $schoolCode)
                ->where('ac_year', AcademicYear::CURRENT_YEAR)
                ->whereIn('cls_taught', $classesNotAllowed)
                ->count();
            if ($teachersWithInvalidClass) {

                SchoolWarning::addWarning($schoolCode, 'teachers-with-invalid-class-taught',
                    $teachersWithInvalidClass.' teacher(s) have invalid class-taught value');

            } else {
                SchoolWarning::deleteWarning($schoolCode, 'teachers-with-invalid-class-taught');
            }
        } else {
            SchoolWarning::deleteWarning($schoolCode, 'teachers-with-invalid-class-taught');
        }

        $teachers = Teacher::where('SCHCD', '=', $master->SCHCD)
            ->where('AC_YEAR', '=', AcademicYear::CURRENT_YEAR)
            ->select('TCHCD1', 'CLS_TAUGHT', 'CATEGORY', 'EMAIL', 'MOBILE', 'TCHNAME')
            ->get();
        $warnings = SchoolWarning::getWarningsByRoute($schoolCode, 'teachers');
        return View::make('backend.teacher.list', compact('teachers', 'master', 'categoryTeachers', 'teacherCount', 'warnings'));
    }

    /**
     * form for creating new teacher
     *
     * @param $schoolCode
     * @return View
     */
    public function newTeacher($schoolCode)
    {
        $teacher = new Teacher;
        $teacherTypeList = DB::table('STEPS_TCHCAT')
            ->where('TCHCAT_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHCAT_DESC, " - ", TCHCAT_ID) as name'), 'TCHCAT_ID')
            ->lists('name', 'TCHCAT_ID');

        $proQualificationList = DB::table('STEPS_TCHPQUAL')
            ->where('TCHPQUAL_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHPQUAL_DESC, " - ", TCHPQUAL_ID) as name'), 'TCHPQUAL_ID')
            ->lists('name', 'TCHPQUAL_ID');

        $subjectList = DB::table('STEPS_TCHSUBTAUGHT')
            ->where('TCHSUBTAUGHT_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHSUBTAUGHT_DESC, " - ", TCHSUBTAUGHT_ID) as name'), 'TCHSUBTAUGHT_ID')
            ->lists('name', 'TCHSUBTAUGHT_ID');

        return View::make('backend.teacher.edit',
            compact('teacher', 'teacherTypeList', 'subjectList', 'proQualificationList'));
    }

    /**
     * delete teacher
     * @return void
     */
    public function destroy($schoolCode)
    {
        $teacherId = Input::get('teacher-id');
        $teacher = Teacher::deleteRecord($teacherId);
        $school = School::find($schoolCode);
        if($school->confirmation()) {
            return Redirect::route('teachers', $schoolCode)->with('error', 'the school has confirmed UDISE.');
        }
        return Redirect::route('teachers', $schoolCode)->with('success', 'Deleted');
    }

    /**
     * edit teacher
     *
     * @return View
     */
    public function editTeacher($schoolCode, $teacherId)
    {
        $teacher = Teacher::findRecord($teacherId);
        $master = App::make('master');

        $teacherTypeList = DB::table('STEPS_TCHCAT')
            ->where('TCHCAT_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHCAT_DESC, " - ", TCHCAT_ID) as name'), 'TCHCAT_ID')
            ->lists('name', 'TCHCAT_ID');

        $proQualificationList = DB::table('STEPS_TCHPQUAL')
            ->where('TCHPQUAL_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHPQUAL_DESC, " - ", TCHPQUAL_ID) as name'), 'TCHPQUAL_ID')
            ->lists('name', 'TCHPQUAL_ID');

        $subjectList = DB::table('STEPS_TCHSUBTAUGHT')
            ->where('TCHSUBTAUGHT_ID', '!=', '99')
            ->select(DB::raw('CONCAT(TCHSUBTAUGHT_DESC, " - ", TCHSUBTAUGHT_ID) as name'), 'TCHSUBTAUGHT_ID')
            ->lists('name', 'TCHSUBTAUGHT_ID');

        if(Route::currentRouteName() == 'show-teacher' || $master->school->confirmation()) {
            return View::make('backend.confirmed.teacher', compact('teacher', 'master'))
                ->with('teacherTypeList', $teacherTypeList)
                ->with('subjectList', $subjectList)
                ->with('proQualificationList', $proQualificationList);
        }
        return View::make('backend.teacher.edit', compact('teacher'))
            ->with('teacherTypeList', $teacherTypeList)
            ->with('subjectList', $subjectList)
            ->with('proQualificationList', $proQualificationList);
    }

    /**
     * Save New Teacher
     *
     * @return Redirect
     */
    public function postNewTeacher($schoolCode)
    {
        $rules = $this->rules;
        $master = App::make('master');
        $dobYear = substr(Input::get('dob'), -4);
        $joinYear = Input::get('join-date');

        foreach(Teacher::$TRAINING_SOURCE as $source) {
            $rules += ["$source-training" => 'integer'];
        }
        $rules['pen'] = 'required|size:6';
        if (($master->isGovAided() || $master->isGov()) && Input::get('appointment-nature') == 1) {
            $rules['dob'] = 'required|age_between:18,56';
            $rules['dob'] = 'required|older_than';
            $rules['join-date'] = 'required|dob_than_:'.$dobYear;
            $rules['current-school-joining-year'] = 'required|dob_than:'.$joinYear.', 0';
        }
        $rules += ['join-date' => 'require'];
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        $teacher = new Teacher;
        $yearDigit = substr(AcademicYear::CURRENT_YEAR, 2, 2);

        //collecting data
        $teacher->AC_YEAR = AcademicYear::CURRENT_YEAR;
        $teacher->SLNO = Teacher::generateSerial($schoolCode);
        $teacher->TCHCD1 = $schoolCode.$yearDigit.$teacher->SLNO;
        $teacher->SCHCD = $schoolCode; // for new school teacher-id contains schoolCode
        $teacher->PENNO = Input::get('pen') ? Input::get('pen'):null;
        $teacher->AADHAAR = Input::get('adhar') ? Input::get('adhar'):null;
        $teacher->TCHNAME = e(Input::get('name'));
        $teacher->MOBILE = e(Input::get('mobile'));
        $teacher->EMAIL = e(Input::get('email'));
        $teacher->SEX = e(Input::get('gender'));
        $teacher->DOB = date("Y-m-d", strtotime(e(Input::get('dob'))));
        $teacher->CASTE = e(Input::get('social-category'));
        $teacher->CATEGORY = e(Input::get('teacher-type'));
        $teacher->POST_STATUS = e(Input::get('appointment-nature'));
        $teacher->YOJ = e(Input::get('join-date'));
        $teacher->QUAL_ACAD = e(Input::get('academic'));
        $teacher->QUAL_PROF = e(Input::get('professional'));
        $teacher->CLS_TAUGHT = e(Input::get('classes-taught'));
        $teacher->APPOINTSUB = e(Input::get('appointed-subject'));
        $teacher->MAIN_TAUGHT1 = e(Input::get('subject1'));
        $teacher->MAIN_TAUGHT2 = e(Input::get('subject2'));
        $teacher->NONTCH_ASS = e(Input::get('days-on-assignments'));
        $teacher->MATH_UPTO = e(Input::get('maths-qualification'));
        $teacher->ENG_UPTO = e(Input::get('english-qualification'));
        $teacher->SCIENCEUPTO = e(Input::get('ss-qualification'));
        $teacher->WORKINGSINCE = e(Input::get('current-school-joining-year'));
        $teacher->DISABILITY_TYPE = e(Input::get('disablity-type'));
        $teacher->CWSNTRAINED_YN = e(Input::get('cwsn-training'));
        $teacher->SUPVAR3 = e(Input::get('ict-training'));

        foreach(Teacher::$TRAINING_SOURCE as $source) {
            $teacher->{"TRN_$source"} = e(Input::get("$source-training"));
        }

        //saving
        if( ! $teacher->save()) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }

        return Redirect::route('teachers', $schoolCode)->with('success', 'Saved');
    }

    public function postUpdate($schoolCode, $teacherId)
    {
        $rules = $this->rules;
        $master = App::make('master');

        foreach(Teacher::$TRAINING_SOURCE as $source) {
            $rules += ["$source-training" => 'integer'];
        }
        if (($master->isGovAided() || $master->isGov()) && Input::get('appointment-nature') == 1) {
            $dobYear = substr(Input::get('dob'), -4);
            $joinYear = Input::get('join-date');
            $rules['dob'] = 'required|older_than';
            $rules['join-date'] = 'required|dob_than_:'.$dobYear;
            $rules['current-school-joining-year'] = 'required|dob_than:'.$joinYear.', 0';
        }
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }

        //collecting data
        $data = array(
            'AC_YEAR' => AcademicYear::CURRENT_YEAR,
            'PENNO' => Input::get('pen') ? Input::get('pen'):null,
            'AADHAAR' => Input::get('adhar') ? Input::get('adhar'):null,
            'TCHNAME' => e(Input::get('name')),
            'MOBILE' => e(Input::get('mobile')),
            'EMAIL' => e(Input::get('email')),
            'SEX' => e(Input::get('gender')),
            'DOB' => date("Y-m-d", strtotime(e(Input::get('dob')))),
            'CASTE' => e(Input::get('social-category')),
            'CATEGORY' => e(Input::get('teacher-type')),
            'POST_STATUS' => e(Input::get('appointment-nature')),
            'YOJ' => e(Input::get('join-date')),
            'QUAL_ACAD' => e(Input::get('academic')),
            'QUAL_PROF' => e(Input::get('professional')),
            'CLS_TAUGHT' => e(Input::get('classes-taught')),
            'APPOINTSUB' => e(Input::get('appointed-subject')),
            'MAIN_TAUGHT1' => e(Input::get('subject1')),
            'MAIN_TAUGHT2' => e(Input::get('subject2')),
            'NONTCH_ASS' => e(Input::get('days-on-assignments')),
            'MATH_UPTO' => e(Input::get('maths-qualification')),
            'ENG_UPTO' => e(Input::get('english-qualification')),
            'SCIENCEUPTO' => e(Input::get('ss-qualification')),
            'WORKINGSINCE' => e(Input::get('current-school-joining-year')),
            'DISABILITY_TYPE' => e(Input::get('disablity-type')),
            'CWSNTRAINED_YN' => e(Input::get('cwsn-training')),
            'SUPVAR3' => e(Input::get('ict-training')),
        );

        foreach(Teacher::$TRAINING_SOURCE as $source) {
            $data += array("TRN_$source" => e(Input::get("$source-training")));
        }

        //saving
        if( ! Teacher::updateRecord($data, $teacherId)) {
            return Redirect::back()->withInput()->withErrors('error', 'error while saving');
        }

        return Redirect::route('teachers', $schoolCode)->with('success', 'Saved');
    }
}
