<?php

class AuthorizedController extends BaseController
{
    public static $SCOPE_SCHOOL = 'SC';
    public static $SCOPE_VILLAGE = 'VL';
    public static $SCOPE_CLUSTER= 'CL';
    public static $SCOPE_BLOCK = 'BL';
    public static $SCOPE_DISTRICT = 'DT';
    public static $SCOPE_STATE = 'ST';

    public static $SCOPE = array(
        'SC' => 'School',
        'VL' => 'Village',
        'CL' => 'Cluster',
        'BL' => 'Block',
        'DT' => 'District',
        'ST' => 'State'
    );

    public static $ROUTES = array(
        'particulars-one' => 1,
        'particulars-two' => 2,
        'particulars-three' => 3,
        'particulars-four' => 4,
        'facilities-two' => 5,
        'facilities-one' => 6,
        'mdm' => 7,
        'teachers' => 8,
        'first-grade-admission' => 9,
        'enrolment-by-category' => 10,
        'enrolment-minority-category' => 11,
        'enrolment-by-medium' => 12,
        'enrolment-by-age' => 13,
        'enrolment-by-age-up' => 14,
        'enrolment-by-age-hs' => 15,
        'enrolment-by-age-ss' => 16,
        'repeaters-by-category' => 17,
        'repeaters-minority-category' => 18,
        'children-with-special-needs' => 19,
        'children-with-special-needs-up' => 20,
        'children-with-special-needs-hs' => 21,
        'children-with-special-needs-hss' => 22,
        'facilities-cwsn' => 23,
        'facilities-to-children-primary' => 24,
        'facilities-to-children-up' => 25,
        'x-result-by-category' => 26,
        'result-by-score-x' => 27,
        'streams-available' => 28,
        'enrolment-by-stream' => 29,
        'repeaters-by-stream' => 30,
        'result-by-score-xii' => 31,
        'result-by-stream' => 32,
        'rmsa-fund' => 33,
        'staff' => 34,
        'students' => 35,
        'search-transfer' => 36,
        'student-transfer' => 37,
        'student-details' => 38,
    );

    public function __construct()
    {
        parent::__construct();
        // Apply the auth filter
        $this->beforeFilter('auth');
    }


}
