module.exports = function(grunt) {
  grunt.initConfig({

    concat: {
      options: {
        separator: ';',
      },
      js_backend: {
        src: [
          './bower_components/jquery/dist/jquery.js',
          './bower_components/AdminLTE/js/bootstrap.js',
          './bower_components/AdminLTE/js/AdminLTE/app.js',
          './bower_components/chained/jquery.chained.remote.js',
          './bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
          './bower_components/parsleyjs/dist/parsley.js',
          './bower_components/parsleyjs/src/extra/validator/comparison.js',
          './app/assets/javascript/backend.js'
        ],
        dest: './public/assets/javascript/backend.js',
      },
      js_frontend: {
        src: [
          './bower_components/jquery/dist/jquery.js',
          './bower_components/AdminLTE/js/bootstrap.js',
          './app/assets/javascript/frontend.js'
        ],
        dest: './public/assets/javascript/frontend.js',
      },
    },
    cssmin: {
      minify_backend: {
        src: [
          './bower_components/AdminLTE/css/bootstrap.css',
          './bower_components/AdminLTE/css/font-awesome.css',
          './bower_components/AdminLTE/css/ionicons.css',
          './bower_components/AdminLTE/css/AdminLTE.css',
          './bower_components/bootstrap-datepicker/css/datepicker3.css',
          './app/assets/css/backend.css'
        ],
        dest: './public/assets/css/backend.min.css'
      },
      minify_pdf: {
        src: [
          './app/assets/css/pdf.css'
        ],
        dest: './public/assets/css/pdf.min.css'
      },
      minify_frontend: {
        src: [
          './bower_components/AdminLTE/css/bootstrap.css',
          './app/assets/css/frontend.css'
        ],
        dest: './public/assets/css/frontend.min.css'
      },
    },
    uglify: {
      options: {
        mangle: false  // Use if you want the names of your functions and variables unchanged
      },
      js_backend: {
        src: './public/assets/javascript/backend.js',
        dest: './public/assets/javascript/backend.min.js',
      },
      js_pdf: {
        src: './bower_components/jquery/dist/jquery.js',
        dest: './public/assets/javascript/pdf.min.js',
      },
      js_frontend: {
        src: './public/assets/javascript/frontend.js',
        dest: './public/assets/javascript/frontend.min.js',
      },
    },
    watch: {
      js_backend: {
        files: [
          //watched files
          './bower_components/jquery/dist/jquery.js',
          './bower_components/AdminLTE/js/bootstrap.js',
          './bower_components/AdminLTE/js/AdminLTE/app.js',
          './bower_components/AdminLTE/js/AdminLTE/demo.js',
          './bower_components/parsleyjs/dist/parsley.js',
          './app/assets/javascript/backend.js',
          './app/assets/javascript/frontend.js',
          './bower_components/AdminLTE/css/bootstrap.css',
          './bower_components/AdminLTE/css/font-awesome.css',
          './bower_components/AdminLTE/css/ionicons.css',
          './bower_components/AdminLTE/css/AdminLTE.css',
          './app/assets/css/backend.css',
          './app/assets/css/pdf.css',
          './app/assets/css/frontend.css'
        ],
        tasks: ['concat','uglify', 'cssmin'],
        options: {
          livereload: true
        }
      },
    }
  });

  // Plugin loading
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Task definition
  grunt.registerTask('default', ['watch']);
};
