## SSA application

#### Requirements

* PHP >= 5.4
* node >= 0.10.*

#### Installation

1. Install following

    - PHP >= 5.4
    - node == 0.10
    - npm
    - bower
    - grunt
    - composer

    I prefer using *nvm* for installing node

2. Setup Environment

        cp bootstrap/start.example.php bootstrap/start.php

    Update the file *bootstrap/start.php* under the section *Detect The Application Environment* with your hostname.

    create .env.local.php with your credentials. sample format:
    ```
    <?php

    return array(
        'DB_HOST'      => '',
        'DB_NAME'  => '',
        'DB_USERNAME'  => '',
        'DB_PASSWORD'  => '',
    );
    ```

3. Install dependencies

        npm install
        bower install
        composer install

4. Compile assets

        grunt concat
        grunt cssmin
        grunt uglify

5. install application

        php artisan app:install

6. import sample database

7. Configure VirtualHost or Run

        php artisan serve
